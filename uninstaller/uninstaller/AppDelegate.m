//
//  AppDelegate.m
//  KensingtonWorks Uninstaller
//
//  Created by Hariharan Mahadevan on 2019/9/27.
//  Copyright © 2019 kensington. All rights reserved.
//

#import "AppDelegate.h"

// Uninstallation script that is created as a temporary file
// and then executed.
// V_20210108 new '.KensingtonWorks-for-BigSur-above.app'
NSString* const UNINST_SCRIPT = @"#!/bin/bash\n\
\n\
# Kill any active GUI process\n\
killall KensingtonWorks2 2> /dev/null\n\
\n\
# Stop TbwHelper launch-agent\n\
CONSOLE_USER=$(stat -f%Su /dev/console)\n\
su -l $CONSOLE_USER -c '/bin/launchctl unload /Library/LaunchAgents/com.kensington.trackballworks.plist' 2> /dev/null\n\
\n\
# Unload kernel extension\n\
kextunload -q '/Library/Extensions/trackballworks2.kext' 2> /dev/null\n\
rm -rf /Applications/KensingtonWorks\\ .app 2> /dev/null\n\
rm -rf /Applications/Utilities/KensingtonWorks\\ Uninstaller.app 2> /dev/null\n\
rm -rf /Library/PreferencePanes/KensingtonWorks.prefPane 2> /dev/null\n\
rm -rf /Library/Extensions/trackballworks2.kext 2> /dev/null\n\
rm -f /Library/LaunchAgents/com.kensington.trackballworks.plist 2> /dev/null\n\
rm -rf /Library/Application\\ Support/Kensington/KensingtonWorks2/KensingtonWorksHelper.app 2> /dev/null\n\
touch /Library/PreferencePanes 2> /dev/null\n\
touch /Library/Extensions 2> /dev/null\n";

/*
NSString* const UNINST_SCRIPT = @"#!/bin/bash\n\
rm -rf /Users/hari/Downloads/quicksynergy-0.9.0\n";
*/

void showWarningMessage(const char* message, const char* info)
{
    NSAlert *alert = [[NSAlert alloc] init];
    // Need to localize
    [alert addButtonWithTitle:@"Close"];
    NSString* message_ = NULL, *info_ = NULL;
    message_ = [[NSString alloc] initWithUTF8String:message];
    [alert setMessageText:message_];
    if (info) {
        info_ = [[NSString alloc] initWithUTF8String:info];
        [alert setInformativeText:info_];
    }
    [alert setAlertStyle:NSAlertStyleWarning];
    [alert runModal];
}

@interface NSFileManager (TemporaryDirectory)

- (NSString *) createTemporaryDirectory;
- (NSString *) createTemporaryFile:(NSData *)contents;

@end

@implementation NSFileManager (TemporaryDirectory)

-(NSString *) createTemporaryDirectory {
    // Create a unique directory in the system temporary directory
    NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString];
    NSString *path = [NSTemporaryDirectory() stringByAppendingPathComponent:guid];
    if (![self createDirectoryAtPath:path withIntermediateDirectories:NO attributes:nil error:nil]) {
        return nil;
    }
    return path;
}

- (NSString *)createTemporaryFile:(NSData *)contents {
    // Create a unique file in the system temporary directory
    NSString *guid = [[NSProcessInfo processInfo] globallyUniqueString];
    NSString *path = [NSTemporaryDirectory() stringByAppendingPathComponent:guid];
    if(![self createFileAtPath:path contents:contents attributes:nil]) {
        return nil;
    }
    return path;
}

@end

// *&*&*&Va_20200908
@interface NSString (ShellExecution)
- (NSString*)runAsCommand;
@end

@implementation NSString (ShellExecution)

- (NSString*)runAsCommand {
    NSPipe* pipe = [NSPipe pipe];

    NSTask* task = [[NSTask alloc] init];
    [task setLaunchPath: @"/bin/sh"];
    [task setArguments:@[@"-c", [NSString stringWithFormat:@"%@", self]]];
    [task setStandardOutput:pipe];

    NSFileHandle* file = [pipe fileHandleForReading];
    [task launch];

    return [[NSString alloc] initWithData:[file readDataToEndOfFile] encoding:NSUTF8StringEncoding];
}

@end


@interface AppDelegate ()

@property (weak) IBOutlet NSWindow *window;
@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
    [self showPrompt];
}


- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}

- (void) showPrompt
{
    NSAlert *alert = [[NSAlert alloc] init];
    // Need to localize
    [alert addButtonWithTitle:NSLocalizedString(@"uninstall", @"uninstall")];
    [alert addButtonWithTitle:NSLocalizedString(@"quit", @"quit")];
    [alert setMessageText:NSLocalizedString(@"uninstall_prompt", @"uninstall_prompt")];
    [alert setInformativeText:NSLocalizedString(@"app_title", @"app_title")];
    [alert setAlertStyle:NSAlertStyleWarning];
    NSModalResponse response = [alert runModal];
    NSLog(@"Response: %d", (int)response);
    if ((int)response == NSAlertFirstButtonReturn)
    {
        // try to uninstall tbwDKDriver
        NSString* tmpStr = [@"systemextensionsctl list | grep com.kensington.tbwDKDriver" runAsCommand]; // *&*&*&V1_20200908
        if([tmpStr containsString:(@"activated enabled")] || [tmpStr containsString:(@"activated waiting to upgrade")])
        {
            //[@"systemextensionsctl uninstall 293UQF7R4S com.kensington.tbwDKDriver" runAsCommand]; // 這招蘋果不准用(以後可能會開放)
            if([tmpStr containsString:(@"KensingtonWorks DriverKit driver 11")])
                [@"open -W /Applications/.KensingtonWorks-for-BigSur-above.app --args --uninstall" runAsCommand];
            else    
                [@"open -W /Applications/.KensingtonWorks.app --args --uninstall" runAsCommand];
        }
        // execute install script
        [self uninstall];
    }
    else
    {
        ;//NSLog(@"Quit selected");
    }
    [NSApp terminate:self];
}

- (void) uninstall
{
    // create the script as a temporary file
    NSFileManager* pFileMgr = [[NSFileManager alloc] init];
    NSString* tempScriptPath = [pFileMgr createTemporaryFile:[UNINST_SCRIPT dataUsingEncoding:NSUTF8StringEncoding]];
    //NSLog(@"Temp script path: %@", tempScriptPath);
    
    // Add +x for file owner
    NSDictionary* fileAttr = [NSDictionary dictionaryWithObjectsAndKeys: [NSNumber numberWithShort:0766], NSFilePosixPermissions, NULL];
    NSError *error = nil;
    [[NSFileManager defaultManager] setAttributes:fileAttr ofItemAtPath:tempScriptPath error:&error];
    if (error) {
        return;
    }
    
    NSString * output = nil;
    NSString * processErrorDescription = nil;
    //NSString* scriptPath = [NSString stringWithFormat:@"%@/%@", [[NSBundle mainBundle] bundlePath], @"Contents/Resources/uninst.sh"];
    //NSLog(@"Script path: %@", scriptPath);
    
    BOOL success = [self runProcessAsAdministrator:tempScriptPath
                         withArguments:[NSArray arrayWithObjects:@"-un", nil]
                         output:&output
                         errorDescription:&processErrorDescription];


    if (!success) // Process failed to run
    {
         // ...look at errorDescription
         //NSLog(@"failed");
         NSString* msg = NSLocalizedString(@"uninstall_failure", @"uninstall_failure");
         showWarningMessage(
            [msg UTF8String],
            processErrorDescription ? [processErrorDescription UTF8String] : NULL);
    }
    else
    {
         // ...process output
         //NSLog(@"success: %@", output);
         NSString* msg = NSLocalizedString(@"uninstall_success", @"uninstall_success");
         showWarningMessage([msg UTF8String], NULL);
    }
}

- (BOOL) runProcessAsAdministrator:(NSString*)scriptPath
                     withArguments:(NSArray *)arguments
                            output:(NSString **)output
                  errorDescription:(NSString **)errorDescription {

    NSString * allArgs = [arguments componentsJoinedByString:@" "];
    NSString * fullScript = [NSString stringWithFormat:@"'%@' %@", scriptPath, allArgs];

    NSDictionary *errorInfo = [NSDictionary new];
    NSString *script =  [NSString stringWithFormat:@"do shell script \"%@\" with administrator privileges", fullScript];

    NSAppleScript *appleScript = [[NSAppleScript new] initWithSource:script];
    NSAppleEventDescriptor * eventResult = [appleScript executeAndReturnError:&errorInfo];

    // Check errorInfo
    if (! eventResult)
    {
        // Describe common errors
        *errorDescription = nil;
        if ([errorInfo valueForKey:NSAppleScriptErrorNumber])
        {
            NSNumber * errorNumber = (NSNumber *)[errorInfo valueForKey:NSAppleScriptErrorNumber];
            if ([errorNumber intValue] == -128)
                *errorDescription = @"The administrator password is required to do this.";
        }

        // Set error message from provided message
        if (*errorDescription == nil)
        {
            if ([errorInfo valueForKey:NSAppleScriptErrorMessage])
                *errorDescription =  (NSString *)[errorInfo valueForKey:NSAppleScriptErrorMessage];
        }

        return NO;
    }
    else
    {
        // Set output to the AppleScript's output
        *output = [eventResult stringValue];

        return YES;
    }
}

@end
