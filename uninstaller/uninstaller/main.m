//
//  main.m
//  KensingtonWorks Uninstaller
//
//  Created by Hariharan Mahadevan on 2019/9/27.
//  Copyright © 2019 kensington. All rights reserved.
//

#import <Cocoa/Cocoa.h>
/*
void executeElevated() {
    AuthorizationRef authorizationRef;
    FILE *pipe = NULL;
    OSStatus err = AuthorizationCreate(nil,
                                       kAuthorizationEmptyEnvironment,
                                       kAuthorizationFlagDefaults,
                                       &authorizationRef);

    char *command= "/usr/bin/touch";
    char *args[] = {"/System/Library/Caches/com.stackoverflow.test", nil};

    err = AuthorizationExecuteWithPrivileges(authorizationRef,
                                             command,
                                             kAuthorizationFlagDefaults,
                                             args,
                                             &pipe);
}
*/

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
