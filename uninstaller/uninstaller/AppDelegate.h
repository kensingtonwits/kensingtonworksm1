//
//  AppDelegate.h
//  KensingtonWorks Uninstaller
//
//  Created by Hariharan Mahadevan on 2019/9/27.
//  Copyright © 2019 kensington. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>

- (void) showPrompt;
- (void) uninstall;
- (BOOL) runProcessAsAdministrator:(NSString*)scriptPath
                     withArguments:(NSArray *)arguments
                            output:(NSString **)output
                  errorDescription:(NSString **)errorDescription;

@end

