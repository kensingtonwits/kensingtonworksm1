//
//  TrackballWorks.m
//  TrackballWorks
//
//  Created by Hariharan Mahadevan on 2019/3/6.
//  Copyright © 2019 Kensington. All rights reserved.
//

#import "TrackballWorks.h"

void showWarningMessage(const char* message, const char* info)
{
    NSAlert *alert = [[NSAlert alloc] init];
    // Need to localize
    [alert addButtonWithTitle:@"Close"];
    NSString* message_ = NULL, *info_ = NULL;
    message_ = [[NSString alloc] initWithUTF8String:message];
    [alert setMessageText:message_];
    if (info) {
        info_ = [[NSString alloc] initWithUTF8String:info];
        [alert setInformativeText:info_];
    }
    [alert setAlertStyle:NSAlertStyleWarning];
    [alert runModal];
}

@implementation TrackballWorks

- (void)mainViewDidLoad
{
    self.launchButton.enabled = TRUE;
}

- (void) didSelect
{
    [self launchGUI];
}

- (IBAction)launchButtonClicked:(id)sender
{
    [self launchGUI];
}

- (void)launchGUI
{
    NSArray *selectedApps = [NSRunningApplication runningApplicationsWithBundleIdentifier:@"com.kensington.kensingtonworks2.app"];
    // Activate the running TrackballWorks app, if one is active
    bool fActivated = false;
    for (int i=0; i < [selectedApps count]; i++) {
        NSRunningApplication* app = [selectedApps objectAtIndex:i];
        // This does not restore minimized window.
        // TODO: Have to find a way to do that.
        [app activateWithOptions:(NSApplicationActivateAllWindows | NSApplicationActivateIgnoringOtherApps)];
        fActivated = true;
    }

    if (!fActivated) {
        if(![[NSWorkspace sharedWorkspace] launchApplication:@"KensingtonWorks "]) {
            // display error message
            showWarningMessage("Failed to launch KensingtonWorks!", NULL);
        }
    }
}
@end
