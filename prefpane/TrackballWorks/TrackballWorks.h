//
//  TrackballWorks.h
//  TrackballWorks
//
//  Created by Hariharan Mahadevan on 2019/3/6.
//  Copyright © 2019 Kensington. All rights reserved.
//

#import <PreferencePanes/PreferencePanes.h>

@interface TrackballWorks : NSPreferencePane

- (void)mainViewDidLoad;
- (void)didSelect;

@property (weak) IBOutlet NSButton *launchButton;

- (IBAction)launchButtonClicked:(id)sender;
- (void) launchGUI;
@end
