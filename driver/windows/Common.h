#ifndef _COMMON_H_
#define _COMMON_H_

#pragma warning(disable:4201)

#include "ntddk.h"

#pragma warning(default:4201)

#include <wdf.h>

// this is necessary to link with WDK 7 and earlier
#ifndef NTDDI_WIN8
#define NTDDI_WIN8 0x06020000L
#endif

#ifndef NonPagedPoolNx
#define NonPagedPoolNx 512
#endif

#define AppropriatePoolForOS (commonUseNxPool ? NonPagedPoolNx : NonPagedPool)

extern BOOLEAN commonUseNxPool;

void CommonInit(void);

#endif