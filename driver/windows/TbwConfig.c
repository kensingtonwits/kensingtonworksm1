#include "ntddk.h"

#include <wdf.h>

#include "tbwkernelapi.h"
#include "tbwconfig.h"
#include "wdfringbuffer.h"

#define USE_TBLIB

#pragma warning(default:4296)

// local prototypes
static BOOLEAN DetectChange(TBWKERNDATA *EventNew, TBWKERNDATA *EventOld);
static BOOLEAN DoStatus(CONFIGDATA *ConfigData, int Code, BOOLEAN State, TBWKERNDATA *Event_processed, TBWKERNDATA *Event_pdo);
static VOID DoAction(CONFIGDATA *ConfigData, ULONG ActionIndex, LONG value, TBWKERNDATA *Event_processed, TBWKERNDATA *Event_pdo, DEFERRED_ACTION_TODO *ActionsToDo);
static BOOLEAN EnqueueDeferredAction(CONFIGDATA *ConfigData, TBWKERN_ACTION *Action, LONG Value);
static BOOLEAN DequeueDeferredAction(CONFIGDATA *ConfigData, TBWKERN_ACTION *Action, LONG *Value);

#if 0
BOOLEAN ConfigEventIsValid(TBWKERNDATA *Event)
{
	return ((Event->status & TBWKERNDATA_STATUS_INVALID) == 0);
}

VOID ConfigEventValidate(TBWKERNDATA *Event, BOOLEAN Flag)
{
	if(!Flag) Event->status |= TBWKERNDATA_STATUS_INVALID;
		else Event->status &= ~TBWKERNDATA_STATUS_INVALID;
}

VOID ConfigProcessEvent(CONFIGDATA *ConfigData, TBWKERNDATA *Event, TBWKERNDATA *Event_pdo, DEFERRED_ACTION_TODO *ActionsToDo)
{
	TBWKERNDATA Event_processed;
	USHORT		delta;

	RtlZeroMemory(Event_pdo, sizeof(TBWKERNDATA));
	RtlZeroMemory(&Event_processed, sizeof(TBWKERNDATA));

	// Since we want to be able to have multiple request in-flight, we ensure that
	// the lastEvent statuses are checked and updated atomically.
	WdfSpinLockAcquire(ConfigData->lastEventLock);

	// process the activity counter
	{
		ULONG increment;
		LARGE_INTEGER time;

		increment = KeQueryTimeIncrement();
		KeQueryTickCount(&time);

		time.QuadPart = (time.QuadPart * increment)/10000;

		if(time.QuadPart - ConfigData->lastActivityCount.QuadPart > MAX_LAST_ACTIVITY)
		{
			Event_pdo->status |= TBWKERNDATA_STATUS_NEWACTIVITY;
			//DebugPrint(("[TBW] Sending activity data\n"));
			ConfigData->lastActivityCount = time;
		}
	}	

	// process status
    delta = (ConfigData->lastEventPhysical.status^Event->status);
    if (delta)
	{
		int i;

		for(i = 0; i<(sizeof(delta)*8); i++)
			if(delta&(1U<<i))
				if(DoStatus(ConfigData, 
							i, 
							(ConfigData->lastEventPhysical.status&(1U<<i))!=0,
							&Event_processed,
							Event_pdo) == FALSE) goto send_out; // Stop processing.
	}

	
	// process ON/OFF usages (typically buttons)
	Event_pdo->buttons = ConfigData->lastEventPdo.buttons;
	Event_processed.buttons = ConfigData->lastEventFiltered.buttons;
    delta = (ConfigData->lastEventPhysical.buttons^Event->buttons);
    if (delta)
	{
		int i;

		for(i = 0; i<(sizeof(delta)*8); i++)
			if(delta&(1U<<i))
				DoAction(ConfigData, 
						 kKernelActionButton1+i, 
						 (Event->buttons&(1U<<i))!=0,
						 &Event_processed,
						 Event_pdo,
						 ActionsToDo);
	}
	if(ConfigData->current.invertButtons12)
	{
		Event_processed.buttons = (Event_processed.buttons & 0xFC) | ((Event_processed.buttons & 0x02) >> 1) | ((Event_processed.buttons & 0x01) << 1);
	}
	
	// process relative value usages (typically axes)
	// Apply dominant axis
	if(ConfigData->current.dominantXY)
	{
		DebugPrint(("[TBW] snap axis\n"));

		if(Event->dx==0 && Event->dy==0) 
		{
			/*
			if(ConfigData->dominance>0) ConfigData->dominance--;
			else if(ConfigData->dominance<0) ConfigData->dominance++;
			*/
		}
		else
		{
			if(MOD(ConfigData->dominance) < 3)
			{
				if(MOD(Event->dx) > MOD(Event->dy)) 
				{ 
					if(ConfigData->dominance < 10) ConfigData->dominance++; 
				}
				else
				{
					if(ConfigData->dominance > -10) ConfigData->dominance--;
				}
			}

			//if(((MOD(Event->dx) - MOD(Event->dy)) + ConfigData->dominance) > 0) Event->dy = 0;
			//	else Event->dx = 0;

			if(ConfigData->dominance > 0) Event->dy = 0;
			else Event->dx = 0;
		}
	}
	else ConfigData->dominance = 0;

	if(ConfigData->current.slowDownXY)
	{
		DebugPrint(("[TBW] slowDown axis\n"));

		if(ConfigData->current.slowDownDivider > 0)
		{
			Event->dx += ConfigData->remainderX;
			Event->dy += ConfigData->remainderY;

			ConfigData->remainderX = Event->dx % ConfigData->current.slowDownDivider;
			ConfigData->remainderY = Event->dy % ConfigData->current.slowDownDivider;

			Event->dx /= ConfigData->current.slowDownDivider;
			Event->dy /= ConfigData->current.slowDownDivider;
		}
	}

	if(Event->dx != 0)
	{
		DoAction(ConfigData, kKernelActionXAxis, Event->dx, &Event_processed, Event_pdo, ActionsToDo);
	}

	if(Event->dy != 0)
	{
		DoAction(ConfigData, kKernelActionYAxis, Event->dy, &Event_processed, Event_pdo, ActionsToDo);
	}

	if(Event->wheel != 0)
	{
		// WATCH OUT: DoAction delivers the wheel info in global WheelCount if inertialScroll is enabled.
		DoAction(ConfigData, kKernelActionWheel, Event->wheel, &Event_processed, Event_pdo, ActionsToDo);
	}
	
	//
	// Touch information cannot be configured or disabled. It's hardcoded since it's part of the scrolling
	// mechanism. It also has immediate effect as it's not queued.
	//
	if((Event->wheelTouch != 0) && (ConfigData->lastEventPhysical.wheelTouch == 0)) ConfigData->quickStop = TRUE;
	ConfigData->quickInhibit = (Event->wheelTouch == 0) ? FALSE : TRUE;

send_out:
	{
		BOOLEAN change;

		RtlCopyMemory(&(ConfigData->lastEventPhysical), Event, sizeof(TBWKERNDATA));

		ConfigEventValidate(Event_pdo, change = DetectChange(Event_pdo, &ConfigData->lastEventPdo));
		if(change) 
		{
			Event_pdo->wheel_speed = ConfigData->WheelSpeed;
			Event_pdo->wheel_count = ConfigData->WheelCounts; // WheelCounts competes against the timer function. Unreliable info

			RtlCopyMemory(&(ConfigData->lastEventPdo), Event_pdo, sizeof(TBWKERNDATA));
		}

		ConfigEventValidate(&Event_processed, change = DetectChange(&Event_processed, &ConfigData->lastEventFiltered));
		if(change) 
		{
			RtlCopyMemory(&(ConfigData->lastEventFiltered), &Event_processed, sizeof(TBWKERNDATA));
			RtlCopyMemory(Event, &Event_processed, sizeof(TBWKERNDATA));
		}
		else
		{
			ConfigEventValidate(Event, FALSE);
		}
	}

	WdfSpinLockRelease(ConfigData->lastEventLock);

}
#endif

void ConfigFixForSanity(TBWKERN_CONFIG *Config)
{
	if((Config->wheel.Speed < 1)||(Config->wheel.Speed > 200)) Config->wheel.Speed = 3;
	if((Config->wheel.Mode != 0)&&(Config->wheel.Mode != 1)) Config->wheel.Mode = 1;
	if((Config->wheel.HistorySize < 1)||(Config->wheel.HistorySize > WHEELMAXHISTORY_SIZE)) Config->wheel.HistorySize = WHEELHISTORY_SIZE;
	if((Config->wheel.ProgressionFactor < 10)||(Config->wheel.ProgressionFactor > 99)) Config->wheel.ProgressionFactor = PROGRESSION_FACTOR;
	if((Config->wheel.Timer < 30)||(Config->wheel.Timer > 500)) Config->wheel.Timer = KERNEL_TIMER_MS;
	if((Config->wheel.Divisions < 1)||(Config->wheel.Divisions > 5)) Config->wheel.Divisions = TIMER_DIVISIONS;
	if((Config->wheel.MaxNullSteps < 1)||(Config->wheel.MaxNullSteps > 50)) Config->wheel.MaxNullSteps = MAX_NULL_STEPS;
	if((Config->slowDownDivider<=0) ||(Config->slowDownDivider>20)) Config->slowDownDivider = 5;
}

BOOLEAN ConfigLoadFromRegistry(TBWKERN_CONFIG *Config)
{
	WDFDRIVER driver;
	WDFKEY hKey;
	NTSTATUS status;
	BOOLEAN result = FALSE;

	Config->wheel.Mode = 1;
	Config->wheel.HistorySize = WHEELHISTORY_SIZE;
	Config->wheel.ProgressionFactor = PROGRESSION_FACTOR;
	Config->wheel.Timer = KERNEL_TIMER_MS;
	Config->wheel.Divisions = TIMER_DIVISIONS;
	Config->wheel.MaxNullSteps = MAX_NULL_STEPS;

	driver = WdfGetDriver();

	DebugPrint(("[TBW] Reading registry values\n"));

	status = WdfDriverOpenParametersRegistryKey(driver,
												STANDARD_RIGHTS_READ,
												WDF_NO_OBJECT_ATTRIBUTES,
												&hKey);
	if(NT_SUCCESS(status))
	{
		WDFKEY hSubkey;
		DECLARE_CONST_UNICODE_STRING(keyString, L"InertialScrollWheel");
		
		status = WdfRegistryOpenKey(hKey,
									&keyString,
									KEY_READ,
									WDF_NO_OBJECT_ATTRIBUTES,
									&hSubkey);

		if(NT_SUCCESS(status))
		{
			ULONG value;
			DECLARE_CONST_UNICODE_STRING(valMode, L"Mode");
			DECLARE_CONST_UNICODE_STRING(valHistSize, L"HistorySize");
			DECLARE_CONST_UNICODE_STRING(valProgression, L"ProgressionFactor");
			DECLARE_CONST_UNICODE_STRING(valTimer, L"Timer");
			DECLARE_CONST_UNICODE_STRING(valDivisions, L"Divisions");
			DECLARE_CONST_UNICODE_STRING(valMaxNullSteps, L"MaxNullSteps");

            Config->wheel.Mode = 1;
            if(NT_SUCCESS(WdfRegistryQueryULong(hSubkey, &valMode, &value)))
				Config->wheel.Mode = (__int32)value;
			
            Config->wheel.HistorySize = WHEELHISTORY_SIZE;
            if(NT_SUCCESS(WdfRegistryQueryULong(hSubkey, &valHistSize, &value)))
				Config->wheel.HistorySize = (__int32)value;
			
            Config->wheel.ProgressionFactor = PROGRESSION_FACTOR;
            if(NT_SUCCESS(WdfRegistryQueryULong(hSubkey, &valProgression, &value)))
				Config->wheel.ProgressionFactor = (__int32)value;
			
            Config->wheel.Timer = KERNEL_TIMER_MS;
            if(NT_SUCCESS(WdfRegistryQueryULong(hSubkey, &valTimer, &value)))
				Config->wheel.Timer = (__int32)value;

            Config->wheel.Divisions = TIMER_DIVISIONS;
            if(NT_SUCCESS(WdfRegistryQueryULong(hSubkey, &valDivisions, &value)))
				Config->wheel.Divisions = (__int32)value;
			
            Config->wheel.MaxNullSteps = MAX_NULL_STEPS;
            if(NT_SUCCESS(WdfRegistryQueryULong(hSubkey, &valMaxNullSteps, &value)))
				Config->wheel.MaxNullSteps = (__int32)value;
			
			result = TRUE;

			WdfRegistryClose(hSubkey);
		}

		WdfRegistryClose(hKey);

	}

	// Final sanity check before leaving.
	ConfigFixForSanity(Config);

	DebugPrint(("[TBW] Inertial Scroll:\n"));
	DebugPrint(("[TBW] \tMode %d\n", Config->wheel.Mode));
	DebugPrint(("[TBW] \tHistory size %d\n", Config->wheel.HistorySize));
	DebugPrint(("[TBW] \tProgression factor %d\n", Config->wheel.ProgressionFactor));
	DebugPrint(("[TBW] \tTimer %d\n", Config->wheel.Timer));
	DebugPrint(("[TBW] \tDivisions %d\n", Config->wheel.Divisions));
	DebugPrint(("[TBW] \tMax Null Steps %d\n", Config->wheel.MaxNullSteps));

	return result;
}

BOOLEAN ConfigSet(CONFIGDATA *ConfigData, TBWKERN_CONFIG *Config)
{
	BOOLEAN result = FALSE;
	
	// sanity check
	ConfigFixForSanity(Config);

	if(Config->Size == sizeof(TBWKERN_CONFIG))
	{
		RtlCopyMemory(&ConfigData->current, Config, Config->Size);

		result = TRUE;
	}

	return result;
}

void ConfigSetDefaults(CONFIGDATA *ConfigData)
{
	int i;

	RtlZeroMemory(&ConfigData->current, sizeof(TBWKERN_CONFIG));

	ConfigFixForSanity(&ConfigData->current);

	for(i = kKernelActionButton1; i <= kKernelActionButton5; i++)
		ConfigData->current.action[i].code = (char) (kKernelCommandButton1+(i-kKernelActionButton1)); // TBW2 FIX

	ConfigData->current.action[kKernelActionXAxis].code = kKernelCommandMoveX;
	ConfigData->current.action[kKernelActionYAxis].code = kKernelCommandMoveY;
	ConfigData->current.action[kKernelActionWheel].code = kKernelCommandMoveWheel;

    // Slow pointer settings
    ConfigData->bSlowPointer = 0;
    ConfigData->uSlowdownDivider = 5;
    // Single axis movement flag
    ConfigData->bLockPointerAxis = FALSE;
	// Default axis switch threshhold to 12 mouse points
	ConfigData->sDominanceSwitchThreshold = 12;

    // Default values for the inertial scroll parameters

    // Vertical scroll
    const float DEFAULT_SCROLL_ATTRITION_FACTOR = 0.5;
    ConfigData->scrollParamsVert.bInvert = FALSE;
    ConfigData->scrollParamsVert.bInertialScroll = FALSE;
    ConfigData->scrollParamsVert.fAttritionFactor = DEFAULT_SCROLL_ATTRITION_FACTOR;
    ConfigData->scrollParamsVert.fVelocity = 0.0;
    ConfigData->scrollParamsVert.lTotalDistance = 0;
    ConfigData->scrollParamsVert.timeStart.QuadPart = 0;

    // Horizontal scroll
    ConfigData->scrollParamsHorz.bInvert = FALSE;
    ConfigData->scrollParamsHorz.bInertialScroll = FALSE;
    ConfigData->scrollParamsHorz.fAttritionFactor = DEFAULT_SCROLL_ATTRITION_FACTOR;
    ConfigData->scrollParamsHorz.fVelocity = 0.0;
    ConfigData->scrollParamsHorz.lTotalDistance = 0;
    ConfigData->scrollParamsHorz.timeStart.QuadPart = 0;

    ConfigSetDefaultButtonsConfig(ConfigData);
}

// Set default button actions, which is for left button & right button
void ConfigSetDefaultButtonsConfig(CONFIGDATA* pConfigData)
{
#ifdef USE_TBLIB
    UNREFERENCED_PARAMETER(pConfigData);
#else 
    TBWBUTTONSCONFIG cfg = { 0 };
    cfg.cbSize = sizeof(TBWBUTTONSCONFIG);
    cfg.nActions = 2;

    // left click
    cfg.actions[0].eventMask = 0x00001;
    cfg.actions[0].actionButton = 0x00001;

    // right click
    cfg.actions[1].eventMask = 0x00002;
    cfg.actions[1].actionButton = 0x00002;

    ConfigSetButtonsConfig(
        pConfigData,
        &cfg);
#endif
}

NTSTATUS ConfigSetButtonsConfig(CONFIGDATA* pConfigData, TBWBUTTONSCONFIG* pButtonsConfig)
{
#ifdef USE_TBLIB
    UNREFERENCED_PARAMETER(pConfigData);
    UNREFERENCED_PARAMETER(pButtonsConfig);
#else
    static const UINT32 MIN_BUTTONACTIONS_COUNT = 32;

    if (pButtonsConfig->cbSize != sizeof(TBWBUTTONSCONFIG))
    {
        DebugPrint(("[TBW] TBWBUTTONSCONFIG.cbSize(%d) != sizeof(TBWBUTTONSCONFIG)\n", pButtonsConfig->cbSize));
        return STATUS_INVALID_PARAMETER;
    }

    pConfigData->buttonsConfig = *pButtonsConfig;

#ifdef DBG
    DebugPrint(("[TBW] TBWBUTTONSCONFIG:-\n"));
    DebugPrint(("\t chordMask: 0x%04x, nActions: %d\n", pConfigData->buttonsConfig.chordMask, pConfigData->buttonsConfig.nActions));
    for (size_t i = 0; i < pConfigData->buttonsConfig.nActions; i++)
    {
        TBWBUTTONACTION* pButton = &(pConfigData->buttonsConfig.actions[i]);
        DebugPrint(("\t action %d - maskIn: 0x%04x, maskOut: 0x%04x\n", i+1, pButton->eventMask, pButton->actionButton));
    }
#endif
#endif
    return STATUS_SUCCESS;
}

NTSTATUS ConfigInit(WDFDEVICE Device, CONFIGDATA *ConfigData)
{
	WDF_OBJECT_ATTRIBUTES attributes;
	NTSTATUS status = STATUS_SUCCESS;

	RtlZeroMemory(ConfigData, sizeof(CONFIGDATA));

	ConfigSetDefaults(ConfigData);

    /*
	WDF_OBJECT_ATTRIBUTES_INIT(&attributes);
	attributes.ParentObject = Device;
	status = WdfSpinLockCreate(&attributes, &ConfigData->lastEventLock);
	if(!NT_SUCCESS(status)) return status;

	WDF_OBJECT_ATTRIBUTES_INIT(&attributes);
	attributes.ParentObject = Device;
	status = WdfSpinLockCreate(&attributes, &ConfigData->WheelCountsLock);
	if(!NT_SUCCESS(status)) return status;
	*/

    WDF_OBJECT_ATTRIBUTES_INIT(&attributes);
	attributes.ParentObject = Device;
	status = WdfRingBufferCreate(&attributes, &ConfigData->DeferredActionBuffer, DEFERRED_ACTION_SIZE, sizeof(DEFERRED_ACTION), TRUE);

	return status;
}

void ConfigDeinit(WDFDEVICE Device, CONFIGDATA *ConfigData)
{
    UNREFERENCED_PARAMETER(Device);
    UNREFERENCED_PARAMETER(ConfigData);

    DebugPrint(("[TBW] ConfigDeinit\n"));
}
#if 0
BOOLEAN ConfigProcessActionsToDo(CONFIGDATA *ConfigData, DEFERRED_ACTION_TODO *ActionsToDo)
{
	unsigned int i;
	BOOLEAN result = FALSE;

	for(i=0; i < ActionsToDo->count; i++)
	{
		result = EnqueueDeferredAction(ConfigData, &(ActionsToDo->action[i].action), ActionsToDo->action[i].Value);
		if(result == FALSE) break;
	}

	return result;
}

BOOLEAN ConfigDeferredEvent(CONFIGDATA *ConfigData, TBWKERNDATA *Event)
{
	// TO DO
	// implement the logic for deferred actions.
	// dequeue, check the actionCode, if it's not ended, requeue always 2 actions (value and 0)
    UNREFERENCED_PARAMETER(ConfigData);
    UNREFERENCED_PARAMETER(Event);
	return FALSE;
}

// internal functions
BOOLEAN EnqueueDeferredAction(CONFIGDATA *ConfigData, TBWKERN_ACTION *Action, LONG Value)
{
	DEFERRED_ACTION action;

	RtlCopyMemory(&action.action, Action, sizeof(TBWKERN_ACTION));
	action.Value = Value;

	return (WdfRingBufferEnqueue(ConfigData->DeferredActionBuffer, &action, sizeof(DEFERRED_ACTION)) == STATUS_SUCCESS);
}

BOOLEAN DequeueDeferredAction(CONFIGDATA *ConfigData, TBWKERN_ACTION *Action, LONG *Value)
{
	DEFERRED_ACTION action;
	BOOLEAN result = FALSE;

	if(NT_SUCCESS(WdfRingBufferDequeue(ConfigData->DeferredActionBuffer, &action, sizeof(DEFERRED_ACTION))))
	{
		RtlCopyMemory(Action, &action.action, sizeof(TBWKERN_ACTION));
		*Value = action.Value;
		result = TRUE;
	}

	return result;
}

BOOLEAN DetectChange(TBWKERNDATA *EventNew, TBWKERNDATA *EventOld)
{
	BOOLEAN result = FALSE;

	if(EventNew->buttons^EventOld->buttons) result = TRUE;
	if(EventNew->dx) result = TRUE;
	if(EventNew->dy) result = TRUE;
	if(EventNew->wheel) result = TRUE;
	if(EventNew->status & TBWKERNDATA_STATUS_NEWACTIVITY) result = TRUE;

	return result;
}

BOOLEAN DoStatus(CONFIGDATA *ConfigData, int Code, BOOLEAN State, TBWKERNDATA *Event_processed, TBWKERNDATA *Event_pdo)
{
    UNREFERENCED_PARAMETER(ConfigData);
    UNREFERENCED_PARAMETER(State);
	switch(Code)
	{
	case CONFIG_STATUS_DATA_INVALID:
	case CONFIG_STATUS_DATA_PASSTHROUGH:
		RtlZeroMemory(Event_pdo, sizeof(TBWKERNDATA));
		return FALSE;
		break;

	case CONFIG_STATUS_DATA_PASSTOPDO:
		RtlCopyMemory(Event_pdo, Event_processed, sizeof(TBWKERNDATA));
		RtlZeroMemory(Event_processed, sizeof(TBWKERNDATA));
		return FALSE;
		break;

	case CONFIG_STATUS_DATA_DISABLE:
		RtlZeroMemory(Event_processed, sizeof(TBWKERNDATA));
		RtlZeroMemory(Event_pdo, sizeof(TBWKERNDATA));
		return FALSE;
		break;

	default:
		return TRUE;
	}
}

VOID DoAction(CONFIGDATA *ConfigData, ULONG ActionIndex, LONG Value, TBWKERNDATA *Event_processed, TBWKERNDATA *Event_pdo, DEFERRED_ACTION_TODO *ActionsToDo)
{
    UNREFERENCED_PARAMETER(ActionsToDo);
	TBWKERN_CONFIG *cfg = &(ConfigData->current);
	
	InterlockedIncrement(&(ConfigData->cached.action[ActionIndex].activeCode));

	if(Value == 0) cfg = &(ConfigData->cached);
	else RtlCopyMemory(&(ConfigData->cached.action[ActionIndex]), &(cfg->action[ActionIndex]), sizeof(TBWKERN_ACTION));

	// TO DO
	// parse the rate and repetition values and enqueue a deferred action if necessary

	{
		UCHAR cmdCode = cfg->action[ActionIndex].code;

		if((cmdCode > kKernelCommandButtonStart)&&(cmdCode < kKernelCommandButtonEnd)) 
		{
			switch(cfg->action[ActionIndex].repetitions)
			{
				case kRepetitionsToggle:
					if(MOD(Value) > cfg->action[ActionIndex].threshold)
					{
						DebugPrint(("[TBW] ClickLock i:%x l:%x ",Event_processed->buttons,ConfigData->lastEventFiltered.buttons));
						Event_processed->buttons &= ~(1U<<(cmdCode-kKernelCommandButton1));
						Event_processed->buttons |= (~ConfigData->lastEventFiltered.buttons)&(1U<<(cmdCode-kKernelCommandButton1));
						DebugPrint(("o:%x ",Event_processed->buttons));
					}
					break;

				case kRepetitionsToggleUsermode:
					if(MOD(Value) > cfg->action[ActionIndex].threshold)
					{
						Event_processed->buttons &= ~(1U<<(cmdCode-kKernelCommandButton1));
						Event_pdo->buttons &= ~(1U<<(cmdCode-kKernelCommandButton1));
						Event_pdo->buttons |= (~ConfigData->lastEventPdo.buttons)&(1U<<(cmdCode-kKernelCommandButton1));
					}
					break;

				default:
					if(MOD(Value) > cfg->action[ActionIndex].threshold)
						Event_processed->buttons |= (1U<<(cmdCode-kKernelCommandButton1));
					else
						Event_processed->buttons &= ~(1U<<(cmdCode-kKernelCommandButton1));
					break;
			}
		}
		else
		{
			switch(cmdCode)
			{
			case kKernelCommandMoveXInvert:
				Value = -Value;
			case kKernelCommandMoveX:
				Event_processed->dx += (short) Value;
				break;

			case kKernelCommandMoveYInvert:
				Value = -Value;
			case kKernelCommandMoveY:
				Event_processed->dy += (short) Value;
				break;

			case kKernelCommandMoveWheelInvert:
				Value = -Value;
			case kKernelCommandMoveWheel:
				Event_processed->wheel += (short) (Value*cfg->wheel.Speed);
				break;

			case kKernelCommandMoveWheelWithInertiaInvert:
				Value =-Value;
			case kKernelCommandMoveWheelWithInertia:
				// update counters

				InterlockedExchangeAdd(&(ConfigData->WheelCounts), Value*cfg->wheel.Speed*(WHEEL_TICK_MULTIPLIER));
				Event_processed->wheel = 0;
				break;

			case kKernelCommandForwardToUserspace:
				if((ActionIndex <= kKernelActionButton16)/*&&(ActionIndex >= kKernelActionButton1)*/)
				{
					if(MOD(Value) > cfg->action[ActionIndex].threshold)
					{
						Event_pdo->buttons |= (1U<<(ActionIndex-kKernelActionButton1));
					}
					else Event_pdo->buttons &= ~(1U<<(ActionIndex-kKernelActionButton1));
				}
				else
				{
					switch(ActionIndex)
					{
					case kKernelActionXAxis:
						Event_pdo->dx += (short) Value;
						break;

					case kKernelActionYAxis:
						Event_pdo->dy += (short) Value;
						break;

					case kKernelActionWheel:
						Event_pdo->wheel += (short) Value;
						break;
					}
				}
				break;
			}
		}
	}
}
#endif