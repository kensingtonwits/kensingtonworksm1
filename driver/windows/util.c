#include "util.h"

#include "debug.h"

void GetWdfIoTargetDeviceObjectName(WDFIOTARGET target, PUNICODE_STRING name)
{
    if (target) {
        PDEVICE_OBJECT devObj = WdfIoTargetWdmGetTargetDeviceObject(target);
        if (devObj) {
            *name = devObj->DriverObject->DriverName;
        }
    }
}

void DumpHidCaps(PHIDP_PREPARSED_DATA pPreparsedData)
{
    /*
    typedef struct _HIDP_CAPS
    {
        USAGE    Usage;
        USAGE    UsagePage;
        USHORT   InputReportByteLength;
        USHORT   OutputReportByteLength;
        USHORT   FeatureReportByteLength;
        USHORT   Reserved[17];

        USHORT   NumberLinkCollectionNodes;

        USHORT   NumberInputButtonCaps;
        USHORT   NumberInputValueCaps;
        USHORT   NumberInputDataIndices;

        USHORT   NumberOutputButtonCaps;
        USHORT   NumberOutputValueCaps;
        USHORT   NumberOutputDataIndices;

        USHORT   NumberFeatureButtonCaps;
        USHORT   NumberFeatureValueCaps;
        USHORT   NumberFeatureDataIndices;
    } HIDP_CAPS, *PHIDP_CAPS;
    */

    HIDP_CAPS caps = { 0 };

    RtlZeroMemory(&caps, sizeof(HIDP_CAPS));
    if (NT_SUCCESS(HidP_GetCaps(pPreparsedData, &caps)))
    {
        DebugPrint(("[TBW] HID Caps:-\n"));

        DebugPrint(("[TBW] \tUsage: %d\n", (int)caps.Usage));
        DebugPrint(("[TBW] \tUsagePage: %d\n", (int)caps.UsagePage));

        DebugPrint(("[TBW] \tInputReportByteLength: %d\n", (int)caps.InputReportByteLength));
        DebugPrint(("[TBW] \tOutputReportByteLength: %d\n", (int)caps.OutputReportByteLength));
        DebugPrint(("[TBW] \tFeatureReportByteLength: %d\n", (int)caps.FeatureReportByteLength));

        DebugPrint(("[TBW] \tNumberLinkCollectionNodes: %d\n", (int)caps.NumberLinkCollectionNodes));

        DebugPrint(("[TBW] \tNumberInputButtonCaps: %d\n", (int)caps.NumberInputButtonCaps));
        DebugPrint(("[TBW] \tNumberInputValueCaps: %d\n", (int)caps.NumberInputValueCaps));
        DebugPrint(("[TBW] \tNumberInputDataIndices: %d\n", (int)caps.NumberInputDataIndices));

        DebugPrint(("[TBW] \tNumberOutputButtonCaps: %d\n", (int)caps.NumberOutputButtonCaps));
        DebugPrint(("[TBW] \tNumberOutputValueCaps: %d\n", (int)caps.NumberOutputValueCaps));
        DebugPrint(("[TBW] \tNumberOutputDataIndices: %d\n", (int)caps.NumberOutputDataIndices));

        DebugPrint(("[TBW] \tNumberFeatureButtonCaps: %d\n", (int)caps.NumberFeatureButtonCaps));
        DebugPrint(("[TBW] \tNumberFeatureValueCaps: %d\n", (int)caps.NumberFeatureValueCaps));
        DebugPrint(("[TBW] \tNumberFeatureDataIndices: %d\n", (int)caps.NumberFeatureDataIndices));
    }
    else
    {
        DebugPrint(("[TBW] Error parsing the HID report descriptor\n"));
    }
}

void DumpValueCaps(PHIDP_PREPARSED_DATA pPreparsedData)
{
    HIDP_CAPS caps = { 0 };

    RtlZeroMemory(&caps, sizeof(HIDP_CAPS));
    if (HidP_GetCaps(pPreparsedData, &caps) == HIDP_STATUS_SUCCESS)
    {
        HIDP_VALUE_CAPS aValueCaps[32];
        USHORT usValueCapsLength = caps.NumberInputValueCaps;
        if (HidP_GetValueCaps(
            HidP_Input,
            aValueCaps,
            &usValueCapsLength,
            pPreparsedData) == HIDP_STATUS_SUCCESS)
        {
            for (USHORT i = 0; i < usValueCapsLength; i++)
            {
                PHIDP_VALUE_CAPS pCaps = &aValueCaps[i];
                pCaps;
                DebugPrint(("[TBW] VALUE %d CAPS:-\n", i));
                /*
                typedef struct _HIDP_VALUE_CAPS
                {
                USAGE    UsagePage;
                UCHAR    ReportID;
                BOOLEAN  IsAlias;

                USHORT   BitField;
                USHORT   LinkCollection;   // A unique internal index pointer

                USAGE    LinkUsage;
                USAGE    LinkUsagePage;

                BOOLEAN  IsRange;
                BOOLEAN  IsStringRange;
                BOOLEAN  IsDesignatorRange;
                BOOLEAN  IsAbsolute;

                BOOLEAN  HasNull;        // Does this channel have a null report   union
                UCHAR    Reserved;
                USHORT   BitSize;        // How many bits are devoted to this value?

                USHORT   ReportCount;    // See Note below.  Usually set to 1.
                USHORT   Reserved2[5];

                ULONG    UnitsExp;
                ULONG    Units;

                LONG     LogicalMin,       LogicalMax;
                LONG     PhysicalMin,      PhysicalMax;

                union {
                    struct {
                        USAGE    UsageMin,         UsageMax;
                        USHORT   StringMin,        StringMax;
                        USHORT   DesignatorMin,    DesignatorMax;
                        USHORT   DataIndexMin,     DataIndexMax;
                    } Range;

                    struct {
                        USAGE    Usage,            Reserved1;
                        USHORT   StringIndex,      Reserved2;
                        USHORT   DesignatorIndex,  Reserved3;
                        USHORT   DataIndex,        Reserved4;
                    } NotRange;
                    };
                } HIDP_VALUE_CAPS, *PHIDP_VALUE_CAPS;
                */

                DebugPrint(("[TBW] \tUsagePage: %u\n", (unsigned)pCaps->UsagePage));
                DebugPrint(("[TBW] \tReportID: %u\n", (unsigned)pCaps->ReportID));

                DebugPrint(("[TBW] \tBitField: 0x%X\n", (unsigned)pCaps->BitField));
                DebugPrint(("[TBW] \tLinkCollection: %u\n", (unsigned)pCaps->LinkCollection));

                DebugPrint(("[TBW] \tLinkUsage: %u\n", (unsigned)pCaps->LinkUsage));
                DebugPrint(("[TBW] \tLinkUsagePage: %u\n", (unsigned)pCaps->LinkUsagePage));

                DebugPrint(("[TBW] \tIsRange: %d\n", (int)pCaps->IsRange));
                DebugPrint(("[TBW] \tIsStringRange: %d\n", (int)pCaps->IsStringRange));
                DebugPrint(("[TBW] \tIsDesignatorRange: %d\n", (int)pCaps->IsDesignatorRange));
                DebugPrint(("[TBW] \tIsAbsolute: %d\n", (int)pCaps->IsAbsolute));

                DebugPrint(("[TBW] \tHasNull: %d\n", (int)pCaps->HasNull));
                DebugPrint(("[TBW] \tBitSize: %d\n", (int)pCaps->BitSize));
                DebugPrint(("[TBW] \tReportCount: %u\n", (unsigned)pCaps->ReportCount));

                DebugPrint(("[TBW] \tUnitsExp: %u\n", pCaps->UnitsExp));
                DebugPrint(("[TBW] \tUnits: %u\n", pCaps->Units));

                DebugPrint(("[TBW] \tLogicalMin: %d, LogicalMax: %d\n", 
                    (int)pCaps->LogicalMin, (int)pCaps->LogicalMax));
                DebugPrint(("[TBW] \tPhysicalMin: %d, PhysicalMax: %d\n",
                    (int)pCaps->PhysicalMin, (int)pCaps->PhysicalMax));

                DebugPrint(("[TBW] \tRange UsageMin: %u, UsageMax: %u\n", (unsigned)pCaps->Range.UsageMin, (unsigned)pCaps->Range.UsageMax));
                DebugPrint(("[TBW] \tRange StringMin: %u, StringMax: %u\n", (unsigned)pCaps->Range.StringMin, (unsigned)pCaps->Range.StringMax));
                DebugPrint(("[TBW] \tRange DesignatorMin: %u, DesignatorMax: %u\n", (unsigned)pCaps->Range.DesignatorMin, (unsigned)pCaps->Range.DesignatorMax));
                DebugPrint(("[TBW] \tRange DataIndexMin: %u, DataIndexMax: %u\n", (unsigned)pCaps->Range.DataIndexMin, (unsigned)pCaps->Range.DataIndexMax));

            }

        }
    }
}

void DumpButtonCaps(PHIDP_PREPARSED_DATA pPreparsedData)
{
    HIDP_CAPS caps = { 0 };

    RtlZeroMemory(&caps, sizeof(HIDP_CAPS));
    if (HidP_GetCaps(pPreparsedData, &caps) == HIDP_STATUS_SUCCESS)
    {
        HIDP_BUTTON_CAPS aButtonCaps[32] = { 0 };
        USHORT usButtonCapsLength = caps.NumberInputButtonCaps;
        if (HidP_GetButtonCaps(
            HidP_Input,
            aButtonCaps,
            &usButtonCapsLength,
            pPreparsedData) == HIDP_STATUS_SUCCESS)
        {
            for (USHORT i = 0; i < usButtonCapsLength; i++)
            {
                DebugPrint(("[TBW] BUTTON %d CAPS:-\n", i));
                /*
                typedef struct _HIDP_BUTTON_CAPS
                {
                USAGE    UsagePage;
                UCHAR    ReportID;
                BOOLEAN  IsAlias;

                USHORT   BitField;
                USHORT   LinkCollection;   // A unique internal index pointer

                USAGE    LinkUsage;
                USAGE    LinkUsagePage;

                BOOLEAN  IsRange;
                BOOLEAN  IsStringRange;
                BOOLEAN  IsDesignatorRange;
                BOOLEAN  IsAbsolute;

                ULONG    Reserved[10];
                union {
                struct {
                USAGE    UsageMin,         UsageMax;
                USHORT   StringMin,        StringMax;
                USHORT   DesignatorMin,    DesignatorMax;
                USHORT   DataIndexMin,     DataIndexMax;
                } Range;
                struct  {
                USAGE    Usage,            Reserved1;
                USHORT   StringIndex,      Reserved2;
                USHORT   DesignatorIndex,  Reserved3;
                USHORT   DataIndex,        Reserved4;
                } NotRange;
                };

                } HIDP_BUTTON_CAPS, *PHIDP_BUTTON_CAPS;
                */
                PHIDP_BUTTON_CAPS pCaps = &aButtonCaps[i];
                pCaps;
                DebugPrint(("[TBW] \tUsagePage: %u\n", (unsigned)pCaps->UsagePage));
                DebugPrint(("[TBW] \tReportID: %u\n", (unsigned)pCaps->ReportID));

                DebugPrint(("[TBW] \tBitField: 0x%X\n", (unsigned)pCaps->BitField));
                DebugPrint(("[TBW] \tLinkCollection: %u\n", (unsigned)pCaps->LinkCollection));

                DebugPrint(("[TBW] \tLinkUsage: %u\n", (unsigned)pCaps->LinkUsage));
                DebugPrint(("[TBW] \tLinkUsagePage: %u\n", (unsigned)pCaps->LinkUsagePage));

                DebugPrint(("[TBW] \tIsRange: %d\n", (int)pCaps->IsRange));
                DebugPrint(("[TBW] \tIsStringRange: %d\n", (int)pCaps->IsStringRange));
                DebugPrint(("[TBW] \tIsDesignatorRange: %d\n", (int)pCaps->IsDesignatorRange));
                DebugPrint(("[TBW] \tIsAbsolute: %d\n", (int)pCaps->IsAbsolute));


                DebugPrint(("[TBW] \tRange UsageMin: %u, UsageMax: %u\n", (unsigned)pCaps->Range.UsageMin, (unsigned)pCaps->Range.UsageMax));
                DebugPrint(("[TBW] \tRange StringMin: %u, StringMax: %u\n", (unsigned)pCaps->Range.StringMin, (unsigned)pCaps->Range.StringMax));
                DebugPrint(("[TBW] \tRange DesignatorMin: %u, DesignatorMax: %u\n", (unsigned)pCaps->Range.DesignatorMin, (unsigned)pCaps->Range.DesignatorMax));
                DebugPrint(("[TBW] \tRange DataIndexMin: %u, DataIndexMax: %u\n", (unsigned)pCaps->Range.DataIndexMin, (unsigned)pCaps->Range.DataIndexMax));
            }
        }
    }
}

UINT32 numberOfSetBits(UINT32 i)
{
    i = i - ((i >> 1) & 0x55555555);
    i = (i & 0x33333333) + ((i >> 2) & 0x33333333);
    return (((i + (i >> 4)) & 0x0F0F0F0F) * 0x01010101) >> 24;
}
