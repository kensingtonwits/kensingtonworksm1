/*
*/

#ifndef _H_tbwmain
#define _H_tbwmain

#pragma warning(disable:4201)

#include "ntddk.h"

#pragma warning(default:4201)

#include <wdf.h>

#define NTSTRSAFE_LIB
#include <ntstrsafe.h>

#include <kbdmou.h>
#include <initguid.h>
#include <devguid.h>
#include <hidpddi.h>

#include "tbwconfig.h"
#include "tbwkernelapi.h"
#include "wdfringbuffer.h"
#include "debug.h"
#include "..\shared\trackball.h"

#define TBW_POOL_TAG (ULONG) 'kWbT'

#define MAXREPORTLENGTH					8
#define USEDREQUESTSBUFFER_SIZE			5
#define DATATOSENDBUFFER_SIZE			32 


typedef WDFREQUEST NEWREQUEST;

typedef struct _DEVICE_EXTENSION
{
    WDFDEVICE WdfDevice;
	CONNECT_DATA UpperConnectData;

    //
    // Number of creates sent down
    //
    LONG EnableCount;

	//
	// Context field
	//
    IN PVOID CallContext;

	//
	// Configuration fields
	//
#ifdef ENABLE_CHORDING_TIMER
	WDFTIMER		timerDetectChord;
    BOOLEAN         bChordTimerStarted;
#endif
	WDFTIMER		timerButtonDebounce;
	BOOLEAN         bDounceTimerStarted;

	WDFTIMER		timerTiltButton;
	BOOLEAN         bTiltButtonTimerStarted;

    WDFSPINLOCK		HandleEventLock;

	WDFIOTARGET		ReadIoTarget;
	PFILE_OBJECT	FileObject;

	WDFDEVICE		pdohDevice;

	USHORT			deviceVersion;

	// counters
	UINT32          uReportMaxSize;
	BOOLEAN			HidReaderStarted;
	ULONG			IoCount;

	// surprise removal
	BOOLEAN			deviceRemoved;
	WDFQUEUE		deferredRequestsQueue;

    // HID preparsed data
    PHIDP_PREPARSED_DATA    hidPreparsedData;
    HIDP_CAPS               hidCaps;            // HidP_GetCaps()
    ULONG                   nMaxUsagesLength;   // HidP_MaxUsageListLength()

    ULONG                   nPrevUsageLength; // number of elements used in the above array

    // Combined USAGE array for current, previous, make & break arrays
    // (see HidP_UsageListDifference). Length of this array would be
    // sizeof(USAGE)*nMaxUsagesLength*4. We use this approach to minimize
    // repeated allocation of multiple fragments from the non-paged pool.
    PUSAGE                  pUsageMem;

    // These pointers are initialized to offset into the combined 
    // USAGE array above that we allocate dynamically.
    PUSAGE                  pCurUsage;
    PUSAGE                  pPrevUsage;
    PUSAGE                  pUsageBreak;
    PUSAGE                  pUsageMake;

	UINT32                  uLastButtonState;

    WDFTIMER                timerInertialScroll;
#ifdef SCROLL_WITH_TRACKBALL
	WDFTIMER                timerAutoScroll;
#endif
	USHORT                  usVendorId;
    USHORT                  usProductId;
    WCHAR                   szProductName[127];
    WCHAR                   szSerialNumber[127];
	BOOLEAN					bHasACPanButtons;

    // Special handling for Slimblade as it's button 3 & 4 are not
    // included in its HID Report descriptor and therefore standard HID
    // parser cannot determine change in its state.
    // So we do it the manual way and for this we need to store the
    // last button state so that we can compare that against current HID
    // report's button state byte.
    BYTE                    bSlimbladeLastButtonState;

    // button state for manual HID button report parsers
    UINT32                  uManualParserLastButtonState;

    // Button Up events to be suppressed from forwarding.
    // This can happen when we detect a chording event, in which
    // case we emulate the chord button press and release by
    // sending two events to the user module. This happens
    // when buttons that comprise the chording event is depressed. Subsequently,
    // when these buttons are released, we have to suppress their buton up
    // events, so that the system input queue or the user module does not 
    // receive additional button up events.
    UINT32                  uButtonUpEventsToSuppress;

	//CONFIGDATA		configData;
	// cross-platform trackball library
	TBLIBCONFIG				tblibConfig;
} DEVICE_EXTENSION, *PDEVICE_EXTENSION;

WDF_DECLARE_CONTEXT_TYPE_WITH_NAME(DEVICE_EXTENSION, FilterGetData)

typedef struct _REQUEST_EXTENSION
{
	WDFMEMORY		Memory;
} REQUEST_EXTENSION, *PREQUEST_EXTENSION;

WDF_DECLARE_CONTEXT_TYPE_WITH_NAME(REQUEST_EXTENSION, RequestGetData)

/*
 * Function that dispatches a button event either to the user module or to
 * HID stack.
 */
//void DispatchButtonEvent(PDEVICE_EXTENSION pDevExt, UINT32 uDownMask, UINT32 uUpMask);
//void PostButtonEvent(PDEVICE_EXTENSION pDevExt, UINT32 uDownMask, UINT32 uUpMask);


#endif
