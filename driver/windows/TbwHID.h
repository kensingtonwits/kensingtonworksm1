

#ifndef _H_tbwhid
#define _H_tbwhid

#ifdef __cplusplus
extern "C" {
#endif

#include <ntddk.h>
#include <ntddmou.h>
#include <wdf.h>
#include <hidpddi.h>

#define kVID_KENSINGTON	0x047D	// 1149		Kensington
#define kPID_NONE		0x0000	// 0		No device
#define kPID_EXPERT		0x1020	// 4128		Expert 7
#define kPID_SLIMBLADE	0x2041	// 8257		SlimBlade
#define kPID_ORBIT		0x1022	// 4130		Orbit
#define kPID_EAGLE		0x2048	// 8264		Eagle
#define kPID_KESTREL	0x8002	//			Kestrel
#define kPID_EXPERTW	0x8018	//			Expert wireless dongle
#define kPID_EXPERTB	0x8019  //			Expert wireless ble

#define kVID_TEST		0xfeed	// test data
#define kPID_TEST       0xbeef  // 

/*
 * Get device information such as USB product id & its firmware version.
 */
	NTSTATUS HidGetDeviceInfo(WDFDEVICE Device, USHORT* pusVendorID, USHORT* pusProductID, USHORT *pusVersionNumber);

/*
 * Returns the HID Report Descriptor for the give device as preparsed data
 * in second argument. Caller has to free this using ExFreePool().
 * This can be used as argument in other HidP_* calls to parse the input reports.
 */
NTSTATUS HidGetPreparsedData(WDFDEVICE hDevice, PHIDP_PREPARSED_DATA* ppHidPreparsedData, ULONG* pulSize);

/*
 * Returns the name of the product in szName. Ensure that szName can hold at least
 * 127 wide chars.
 */
NTSTATUS HidGetProductString(WDFDEVICE hDevice, WCHAR szName[], size_t nChars);

/*
 * Returns the product serial number string embedded in the device.
 */
NTSTATUS HidGetSerialNumberString(WDFDEVICE hDevice, WCHAR szSerialNumber[], size_t nChars);

#ifdef __cplusplus
}
#endif

#endif