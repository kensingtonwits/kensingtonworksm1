
#ifndef _H_tbwconfig
#define _H_tbwconfig

#ifdef __cplusplus
extern "C" {
#endif

#pragma warning(disable:4201)

#include "ntddk.h"

#pragma warning(default:4201)

#include <wdf.h>

#include "tbwkernelapi.h"
#include "tbwhid.h"
#include "wdfringbuffer.h"
#include "../shared/trackball.h"
#include "debug.h"

#define INTEGER_TICK				120
#define WHEELHISTORY_SIZE			4
#define WHEELMAXHISTORY_SIZE		20
#define DEFERRED_ACTION_SIZE		10
#define DEFERRED_ACTION_TODO_MAX	3
#define WHEEL_TICK_MULTIPLIER		1000
#define MAX_NULL_STEPS				3
//#define PROGRESSION_FACTOR			90
#define TIMER_DIVISIONS				1
#define KERNEL_TIMER_MS				70
#define MAX_LAST_ACTIVITY			500

#define MOD(a) (a < 0 ? -a : a)

typedef struct _INERTIAL_SCROLL_PARAMS
{
    // Whether to invert scroll wheel direction
    BOOLEAN                 bInvert;

    // Inertial scroll flag
    BOOLEAN                 bInertialScroll;

    // Attrition factor that is used to decay the inertial scroll value
    float                   fAttritionFactor;

    // Timer tick count when scroll motion was detected. This is used
    // to compute the scroll velocity which will be used to generate
    // the quantum of the inertial scroll movement amount
    LARGE_INTEGER           timeStart;

    // Total distance travelled since scroll was started
    LONG                    lTotalDistance;

    // Inertial scroll velocity that will be computed from the start time
    // and total distance travelled
    float                   fVelocity;
} SCROLL_WHEEL_CONFIG;

// Used for caching button events for chord processing
typedef struct _BUTTONEVENT {
    UINT32 uDownMask;
    UINT32 uUpMask;
} BUTTONEVENT;

#define CHORD_BUTTON_EVENTS_CACHE_SIZE    16

typedef struct _CONFIGDATA
{
	TBWKERN_CONFIG	current;
	TBWKERN_CONFIG	cached;
#if 0
    /*
	struct 
	{
		unsigned short	fChordPhysicalButtons;
		unsigned short	fChordLogicalButtons;
		unsigned short	fChordToRelease;
		BOOLEAN			fChordDone;
		BOOLEAN			fPendingChordTimer;
	} chord;
    */
	WDFSPINLOCK		lastEventLock;
	TBWKERNDATA		lastEventPhysical;
	TBWKERNDATA		lastEventFiltered;
	TBWKERNDATA		lastEventPdo;

	WDFSPINLOCK		WheelCountsLock;
	UCHAR			WheelHistoryIdx;
	LONG			WheelHistory[WHEELMAXHISTORY_SIZE];
	LONG			WheelSpeed;
	LONG			WheelCounts;
#endif

	LONG			NextValueToSend;
	LONG			NextValueRemainder;
	LONG			NullSteps;

	BOOLEAN			quickInhibit;
	BOOLEAN			quickStop;

    // Locked-axis movement implementation
    UINT32          bLockPointerAxis;
	SHORT			dominance;
	SHORT			sDominanceSwitchThreshold;

    // Slow pointer implementation
    UINT32          bSlowPointer;
    UINT32          uSlowdownDivider;
	SHORT			remainderX;
	SHORT			remainderY;

	LARGE_INTEGER	lastActivityCount;

	WDFRINGBUFFER	DeferredActionBuffer;

    SCROLL_WHEEL_CONFIG scrollParamsVert;
    SCROLL_WHEEL_CONFIG scrollParamsHorz;

    TBWBUTTONSCONFIG    buttonsConfig;

    // ***** BEGIN CHORDING SUPPORT MEMBERS ***** //
    // Used to accumulate bitmask of currently depressed buttons that is then
    // used to determine if the combination of button presses is a chord 
    // button press.
    UINT32              dwCurChordEventsMask;
    // Number of events cached in aButtonEventsCache
    UINT32              nChordButtonEvents;
    // Cache for storing button events used to determine if the user
    // pressed a combination of buttons resulting a chording event.
    BUTTONEVENT         aChordButtonEventsCache[CHORD_BUTTON_EVENTS_CACHE_SIZE];
    // ***** END CHORDING SUPPORT MEMBERS ***** //

} CONFIGDATA, *PCONFIGDATA;

#define CONFIG_STATUS_DATA_INVALID		1
#define CONFIG_STATUS_DATA_PASSTHROUGH	2
#define CONFIG_STATUS_DATA_PASSTOPDO	3
#define CONFIG_STATUS_DATA_DISABLE		4

typedef struct _DEFERRED_ACTION
{
	TBWKERN_ACTION action;
	LONG Value;
} DEFERRED_ACTION, *PDEFERRED_ACTION;

typedef struct _DEFERRED_ACTION_TODO
{
	DEFERRED_ACTION action[DEFERRED_ACTION_TODO_MAX];
	ULONG	count;
} DEFERRED_ACTION_TODO, *PDEFERRED_ACTION_TODO;

BOOLEAN ConfigEventIsValid(TBWKERNDATA *Event);
VOID ConfigEventValidate(TBWKERNDATA *Event, BOOLEAN Flag);
VOID ConfigProcessEvent(CONFIGDATA *ConfigData, TBWKERNDATA *Event, TBWKERNDATA *Event_pdo, DEFERRED_ACTION_TODO *ActionsToDo);
BOOLEAN ConfigSet(CONFIGDATA *ConfigData, TBWKERN_CONFIG *Config);
void ConfigSetDefaults(CONFIGDATA *ConfigData);
BOOLEAN ConfigLoadFromRegistry(TBWKERN_CONFIG *Config);
NTSTATUS ConfigInit(WDFDEVICE Device, CONFIGDATA *ConfigData);
void ConfigDeinit(WDFDEVICE Device, CONFIGDATA *ConfigData);


BOOLEAN ConfigProcessActionsToDo(CONFIGDATA *ConfigData, DEFERRED_ACTION_TODO *ActionsToDo);
BOOLEAN ConfigDeferredEvent(CONFIGDATA *ConfigData, TBWKERNDATA *Event);
void ConfigSetDefaultButtonsConfig(CONFIGDATA* pConfigData);
NTSTATUS ConfigSetButtonsConfig(CONFIGDATA* pConfigData, TBWBUTTONSCONFIG* pButtonsConfig);

#ifdef __cplusplus
}
#endif

#endif