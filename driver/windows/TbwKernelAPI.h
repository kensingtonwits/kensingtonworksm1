//
// TbwKernelAPI.h
// 
//

//==============================================================================

#ifndef _h_tbwkernelapi
#define _h_tbwkernelapi

#ifdef __cplusplus
extern "C" {
#endif

#define SCROLL_WITH_TRACKBALL

// IOCTL control codes

#define IOCTL_WRITECONFIG		CTL_CODE(FILE_DEVICE_UNKNOWN, 0x800+1, METHOD_BUFFERED, FILE_ANY_ACCESS) 
#define IOCTL_GETDEVICETYPE		CTL_CODE(FILE_DEVICE_UNKNOWN, 0x800+2, METHOD_BUFFERED, FILE_ANY_ACCESS) 

#pragma pack(push)
#pragma pack(1)

/*
 * Get driver information such as its version, etc
 */
typedef struct _TRACKBALL_DRIVER_INFO {
    UINT32 cbSize;                 // structure size
    /*
    Version is packed in to a DWORD in the following format:
        +--------+--------+--------+--------+
        | MAJOR  | MINOR  | PATCH  |    0   |
        +--------+--------+--------+--------+
    */
    UINT32      uVersion;               // Driver version
} TRACKBALL_DRIVER_INFO;
#define IOCTL_GETDDRIVERINFO	CTL_CODE(FILE_DEVICE_UNKNOWN, 0x800+3, METHOD_BUFFERED, FILE_ANY_ACCESS)

#define IOCTL_GETDEVICEVERSION	CTL_CODE(FILE_DEVICE_UNKNOWN, 0x800+4, METHOD_BUFFERED, FILE_ANY_ACCESS) 

/*
Set/get slow pointer flag.

Parameters:
    uint32_t boolean flag.
        0 - disable slow pointer
        1 - enable slow pointer
*/
#define IOCTL_SETSLOWPOINTER	CTL_CODE(FILE_DEVICE_UNKNOWN, 0x800+5, METHOD_BUFFERED, FILE_ANY_ACCESS)
#define IOCTL_GETSLOWPOINTER	CTL_CODE(FILE_DEVICE_UNKNOWN, 0x800+6, METHOD_BUFFERED, FILE_ANY_ACCESS)

/*
Set/get single axis movement flag.

Parameters:
    uint32_t boolean flag.
        0 - disable single axis movement
        1 - enable single axis movement
*/
#define IOCTL_SETSINGLEAXISMOVT	CTL_CODE(FILE_DEVICE_UNKNOWN, 0x800+7, METHOD_BUFFERED, FILE_ANY_ACCESS) 
#define IOCTL_GETSINGLEAXISMOVT	CTL_CODE(FILE_DEVICE_UNKNOWN, 0x800+8, METHOD_BUFFERED, FILE_ANY_ACCESS) 

/*
Set/get slow pointer divider factor.

Parameters:
   uint32_t slow pointer divider factor
*/
#define IOCTL_SETSLOWPOINTERDIVIDER	CTL_CODE(FILE_DEVICE_UNKNOWN, 0x800+9, METHOD_BUFFERED, FILE_ANY_ACCESS) 
#define IOCTL_GETSLOWPOINTERDIVIDER	CTL_CODE(FILE_DEVICE_UNKNOWN, 0x800+10, METHOD_BUFFERED, FILE_ANY_ACCESS) 

typedef enum _tagSCROLL_WHEEL_TYPE {
    SCROLL_WHEEL_VERT = 0,
    SCROLL_WHEEL_HORZ
} SCROLL_WHEEL_TYPE;

typedef struct _SCROLL_PARAMS {
    __int8  ScrollWheelType;    // SCROLL_WHEEL_TYPE
    __int8  bInvertWheel;       // Invert wheel direction?
    __int8  bInertialScroll;    // enable/disable inertial scroll
    __int8  reserved;           // align to 32-bit boundary
    float   fAttritionFactor;   // valid value has to be > 0. 0 will be ignored.
} SCROLL_PARAMS;

/*
 Set scroll wheel properties.

 Parameters:
    SCROLL_PARAMS. Set the ScrollWheelType member to the appropriate scroll 
    wheel type properties for which you wish to set. Refer to SCROLL_PARAMS 
    structure above for details of scroll wheel properties that can be 
    customized.
*/
#define IOCTL_SETSCROLLPARAMS CTL_CODE(FILE_DEVICE_UNKNOWN, 0x800+11, METHOD_BUFFERED, FILE_ANY_ACCESS) 

/*
 Get scroll wheel properties.

 Parameters:
    SCROLL_PARAMS. Set the ScrollWheelType member to the appropriate scroll
    wheel type properties for which you wish to retrieve. Refer to SCROLL_PARAMS
    structure above for details of scroll wheel properties that can be
    queried.
*/
#define IOCTL_GETSCROLLPARAMS CTL_CODE(FILE_DEVICE_UNKNOWN, 0x800+12, METHOD_BUFFERED, FILE_ANY_ACCESS) 

/*
Get information about the physical device.

Parameters:
    Pointer to TRACKBALL_DEVICE_INFO.
*/
typedef struct _TRACKBALL_DEVICE_INFO {
    USHORT      cbSize;                 // structure size
    USHORT      usProductId;            // USB product ID
    USHORT      usVersion;              // Device version
    WCHAR       szName[127];            // null terminated string
    WCHAR       szSerialNumber[127];    // null terminated string
} TRACKBALL_DEVICE_INFO;
#define IOCTL_GETDEVICEINFO CTL_CODE(FILE_DEVICE_UNKNOWN, 0x800+13, METHOD_BUFFERED, FILE_ANY_ACCESS) 

// Scroll action direction
#define SCROLL_DIRECTION_UP     1
#define SCROLL_DIRECTION_DOWN   2
#define SCROLL_DIRECTION_LEFT   3
#define SCROLL_DIRECTION_RIGHT  4

// Button actions
#define BUTTON_ACTION_CLICK     0
#define BUTTON_ACTION_SCROLL    1

typedef struct _TbwButtonAction {
    UINT32 buttonMask;
    BYTE action;    // use button action macros above
    union {
        struct {
            UINT32 buttonMask;
        } ClickActionParams;
        struct {
            BYTE direction; // use scroll direction macros above
            BYTE lines;
        } ScrollActionParams;
    } u;
} TBWBUTTONACTION;

typedef struct _TbwButtonsConfig {
    UINT32      cbSize;
    UINT32      chordMask;
	UINT32      chordMsks[16];
    UINT32      nButtons;
    TBWBUTTONACTION buttons[32];
} TBWBUTTONSCONFIG;

/*
Configure butto parameters

Parameters:
    Pointer to TBWBUTTONSCONFIG- 
*/
#define IOCTL_SETBUTTONSCONFIG  CTL_CODE(FILE_DEVICE_UNKNOWN, 0x800+14, METHOD_BUFFERED, FILE_ANY_ACCESS) 

typedef struct TbwEmulateDeviceAction {
    UINT32  cbSize;         // structure size (provision for future enhancement)
    UINT32  uAction;        // See action types below
    union {
        BYTE    aParam[256];    // generic 256 byte array of parameters
        struct {
            UINT32 mask;      // mask for button click
            UINT32 dir;       // 1 - down, 2 - up, 3 - down & up
        } ButtonClick;
    } u;
} TBWEMULATEDEVICEACTION;

// device action types

// Emulate button click
//  aParam[0] - button mask, including chording masks
//  aParam[1] - direction
//                  1 - down
//                  2 - up
//                  3 - down & up, in sequence
#define DA_BUTTONCLICK  1

/*
Emulate device action

Parameters:
    Pointer to TBWEMULATEDEVICEACTION
*/
#define IOCTL_EMULATEDEVICEACTION CTL_CODE(FILE_DEVICE_UNKNOWN, 0x800+15, METHOD_BUFFERED, FILE_ANY_ACCESS) 

/*
Register helper app

Parameters:
    None
*/
#define IOCTL_REGISTERHELPER CTL_CODE(FILE_DEVICE_UNKNOWN, 0x800+16, METHOD_BUFFERED, FILE_ANY_ACCESS) 

#pragma pack(pop)

#define DRIVER_BUILD	3

// Device Types
// Better if consecutive numbers since this value can be used as an index
typedef enum {
	TBW_TYPE_UNKNOWN =				0,
	TBW_TYPE_EXPERT =				1,
	TBW_TYPE_ORBIT =				2,
	TBW_TYPE_EAGLE	=				3,
	TBW_TYPE_SLIMBLADE_NOCHORD =	4,
	TBW_TYPE_SLIMBLADE =			5,
	TBW_TYPE_KESTREL =				6,
	TBW_TYPE_EXPERT_W =				7,
	TBW_TYPE_EXPERT_BLE =			8,
	TBW_TYPE_MAX =					9
} TBW_TYPE;

#define kKernelMaxControls 32

enum {
		kKernelActionButton1 = 0,
		kKernelActionButton2,
		kKernelActionButton3,
		kKernelActionButton4,
		kKernelActionButton5,
		kKernelActionButton6,
		kKernelActionButton7,
		kKernelActionButton8,
		kKernelActionButton9,
		kKernelActionButton10,
		kKernelActionButton11,
		kKernelActionButton12,
		kKernelActionButton13,
		kKernelActionButton14,
		kKernelActionButton15,
		kKernelActionButton16,
		
		kKernelActionXAxis,
		kKernelActionYAxis,
		kKernelActionWheel,

		kKernelActionCount
};

#define kTWChord1 kKernelActionButton9
#define kTWChord2 kKernelActionButton10

enum {
		kKernelCommandNone = 0,

		kKernelCommandButtonStart,
		kKernelCommandButton1,
		kKernelCommandButton2,
		kKernelCommandButton3,
		kKernelCommandButton4,
		kKernelCommandButton5,
		kKernelCommandButton6,
		kKernelCommandButton7,
		kKernelCommandButton8,
		kKernelCommandButton9,
		kKernelCommandButton10,
		kKernelCommandButton11,
		kKernelCommandButton12,
		kKernelCommandButton13,
		kKernelCommandButton14,
		kKernelCommandButton15,
		kKernelCommandButton16,
		kKernelCommandButtonEnd,

		kKernelCommandMoveX,
		kKernelCommandMoveXInvert,
		kKernelCommandMoveY,
		kKernelCommandMoveYInvert,
		kKernelCommandMoveWheel,
		kKernelCommandMoveWheelWithInertia,
		kKernelCommandMoveWheelInvert,
		kKernelCommandMoveWheelWithInertiaInvert,

		kKernelCommandForwardToUserspace = 50,

		kKernelCommandCount
};

#define kRepetitionsToggle			(__int8) -1
#define kRepetitionsToggleUsermode	(__int8) -2

#pragma pack(push)
#pragma pack(1)

typedef struct TbwKernAction {

	__int8 code;		// command
	__int32 threshold;  // threshold that the value has to go over to be 1 (for buttons is zero)
	__int8 rate;		// repetition rate:
						// 0 means no repetition
						// -1 means use the value of the usage
	__int8 repetitions;	// 0 means no repetition
						// 1 means play down/up on down
						// -1 infinite
						// -2 means toggle mode
    
    __int8 reserved;    // to align activeCode to 64bit boundary

    long activeCode;	// Every time DoAction executes, activeCode gets incremented.
						// If it's a timed action, activeCode propagates in the deferred actions queue.
						// When a new action gets to be executed in the timer, we check if 
						// activeCode matches the saved one. If not we discard the action and move to the
						// next one.

} TBWKERN_ACTION, *PTBWKERN_ACTION;

typedef struct TbwKernWheel {

	__int32 Speed;
	__int32 Mode;
	__int32 HistorySize;
	__int32 ProgressionFactor;
	__int32 Timer;
	__int32 Divisions;
	__int32 MaxNullSteps;

} TBWKERN_WHEEL, *PTBWKERN_WHEEL;

#define TBWKERN_CONFIGURATION_VER1	1
typedef struct TbwKernConfigurationV1 {

	__int32 Size;

	__int8 invertButtons12;
	__int8 dominantXY;
	__int8 slowDownXY;

    __int8 reserved1;   // align to 32-bit boundary

	__int16 slowDownDivider;

    __int16 reserved2;  // align to 32-bit boundary

	TBWKERN_WHEEL wheel;

	TBWKERN_ACTION action[kKernelMaxControls];
	
} TBWKERN_CONFIG_V1, *PTBWKERN_CONFIG_V1;

// Event status values
#define TBWKERNDATA_STATUS_INVALID			0x01U
#define TBWKERNDATA_STATUS_WHEELTOUCHED		0x02U
#define TBWKERNDATA_STATUS_NEWACTIVITY		0x04U
#define TBWKERNDATA_STATUS_BUTTONACTIVITY   0x08U

#define TBWKERN_CONFIGURATION_VER	TBWKERN_CONFIGURATION_VER1
#define TBWKERN_CONFIG				TBWKERN_CONFIG_V1
#define PTBWKERN_CONFIG				PTBWKERN_CONFIG_V1

typedef struct TbwKernData {
	unsigned short	buttons;			
	short			dx;
	short			dy;
	short			wheel;
	short			wheelH;
	unsigned short	wheelTouch;
	unsigned short	status;

	// additional info
	unsigned long	report_length;
	long			wheel_speed; // last 1s sum
	long			wheel_count; // last 100ms sum
    LARGE_INTEGER   time;
    unsigned long   buttonDownMask;
    unsigned long   buttonUpMask;
} TBWKERNDATA, *PTBWKERNDATA;

#pragma pack(pop)

// {4F1FD8B4-05A8-4E91-B42C-8942B49AE606}
DEFINE_GUID(GUID_BUS_KNSTBW, 0x4f1fd8b4, 0x5a8, 0x4e91, 0xb4, 0x2c, 0x89, 0x42, 0xb4, 0x9a, 0xe6, 0x6);
#define TBW_DEVICE_ID L"{4F1FD8B4-05A8-4E91-B42C-8942B49AE606}\\KNSTBW\0"

// {1A629312-9256-4ED3-83CB-F5FE65F5E03A}
DEFINE_GUID(GUID_DEVINTERFACE_KNSTBW, 0x1a629312, 0x9256, 0x4ed3, 0x83, 0xcb, 0xf5, 0xfe, 0x65, 0xf5, 0xe0, 0x3a);

/*
tbwkern 1.x PDO device id
// {AF111662-3D80-43ac-B39B-D7B15ACB0B54}
DEFINE_GUID(GUID_BUS_KNSTBW, 0x4F1FD8B4, 0x05A8, 4E91, 0xb3, 0x9b, 0xd7, 0xb1, 0x5a, 0xcb, 0xb, 0x54);
#define TBW_DEVICE_ID L"{AF111662-3D80-43ac-B39B-D7B15ACB0B54}\\KNSTBW\0"

// {1D0785A7-8CF1-46d9-9643-6A3A99EB21A0}
DEFINE_GUID(GUID_DEVINTERFACE_KNSTBW, 0x1d0785a7, 0x8cf1, 0x46d9, 0x96, 0x43, 0x6a, 0x3a, 0x99, 0xeb, 0x21, 0xa0);
*/

#ifdef __cplusplus
}
#endif

#endif