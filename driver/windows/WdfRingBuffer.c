#include "common.h"
#include "wdfringbuffer.h"

static VOID OnDestroy(IN WDFOBJECT Object);
static VOID OnCleanup(IN WDFOBJECT Object);
static BOOLEAN IsFull(IN WDFRINGBUFFER rb);

NTSTATUS WdfRingBufferCreate(WDF_OBJECT_ATTRIBUTES *attributes, 
							 WDFRINGBUFFER *object, 
							 ULONG NumElements, 
							 ULONG cbSize,
							 BOOLEAN synchronized)
{
	NTSTATUS result;
	WDFRINGBUFFER_CONTEXT *ringbuffer_context;
	
	//attributes.EvtCleanupCallback = OnCleanup;
	//attributes.EvtDestroyCallback = OnDestroy;

	WDF_OBJECT_ATTRIBUTES_SET_CONTEXT_TYPE(attributes, WDFRINGBUFFER_CONTEXT);
	result = WdfObjectCreate(attributes, object);
	if(!NT_SUCCESS(result)) return result;

	ringbuffer_context = GetRingBufferContext(*object);
	RtlZeroMemory(ringbuffer_context, sizeof(WDFRINGBUFFER_CONTEXT));

	{
		WDF_OBJECT_ATTRIBUTES contextAttrib;

		if(synchronized)
		{
			WDF_OBJECT_ATTRIBUTES_INIT(&contextAttrib);
			contextAttrib.ParentObject = *object;
			result = WdfSpinLockCreate(&contextAttrib, &ringbuffer_context->lock_enqueue);
			if(!NT_SUCCESS(result)) return result;

			WDF_OBJECT_ATTRIBUTES_INIT(&contextAttrib);
			contextAttrib.ParentObject = *object;
			result = WdfSpinLockCreate(&contextAttrib, &ringbuffer_context->lock_dequeue);
			if(!NT_SUCCESS(result)) return result;
		}

		WDF_OBJECT_ATTRIBUTES_INIT(&contextAttrib);
		contextAttrib.ParentObject = *object;
		result = WdfMemoryCreate(&contextAttrib, AppropriatePoolForOS, 0, cbSize*(NumElements+1), &ringbuffer_context->buffer, NULL);
		if(!NT_SUCCESS(result)) return result;

		ringbuffer_context->ElementSize = cbSize;
		ringbuffer_context->NumElements = NumElements+1;
		ringbuffer_context->Head = ringbuffer_context->Tail = 0L;
		ringbuffer_context->synchronized = synchronized;
	}

	return result;

}

NTSTATUS WdfRingBufferEnqueue(IN WDFRINGBUFFER rb, void * element, ULONG cbSize)
{
	WDFRINGBUFFER_CONTEXT *rbc = GetRingBufferContext(rb);
	unsigned long head;
	char * SelEl;
	NTSTATUS result = STATUS_NO_MORE_ENTRIES;

	if(cbSize > rbc->ElementSize) return STATUS_BUFFER_OVERFLOW;

	if(rbc->synchronized) WdfSpinLockAcquire(rbc->lock_enqueue);

	head = rbc->Head;

	if(!IsFull(rb))
	{
		char * buff = WdfMemoryGetBuffer(rbc->buffer, NULL);

		SelEl = buff + (head*rbc->ElementSize); 

		RtlCopyMemory(SelEl, element, cbSize);

		head = (head == rbc->NumElements-1) ? 0 : (head+1);

		rbc->Head = head;

		result = STATUS_SUCCESS;
	}

	if(rbc->synchronized) WdfSpinLockRelease(rbc->lock_enqueue);

	return result;
}

NTSTATUS WdfRingBufferDequeue(IN WDFRINGBUFFER rb, void * element, ULONG cbSize)
{
	WDFRINGBUFFER_CONTEXT *rbc = GetRingBufferContext(rb);
	unsigned long int head, tail;
	char * SelEl;
	NTSTATUS result = STATUS_NO_MORE_ENTRIES;

	if(cbSize < rbc->ElementSize) return STATUS_BUFFER_TOO_SMALL;

	if(rbc->synchronized) WdfSpinLockAcquire(rbc->lock_dequeue);

	head = rbc->Head;
	tail = rbc->Tail;

	if(tail != head)
	{
		char * buff = WdfMemoryGetBuffer(rbc->buffer, NULL);

		SelEl = buff+tail*rbc->ElementSize;

		RtlCopyMemory(element, SelEl, cbSize);

		tail = (tail == rbc->NumElements-1) ? 0 : (tail+1);

		rbc->Tail = tail;

		result = STATUS_SUCCESS;
	}

	if(rbc->synchronized) WdfSpinLockRelease(rbc->lock_dequeue);

	return result;
}

NTSTATUS WdfRingBufferEnqueueAndDequeue(IN WDFRINGBUFFER rb, void * element, ULONG cbSize)
{
	WDFRINGBUFFER_CONTEXT *rbc = GetRingBufferContext(rb);
	unsigned long head, tail;
	char * SelEl;
	NTSTATUS result = STATUS_NO_MORE_ENTRIES;

	if(rbc->synchronized == FALSE) return STATUS_INVALID_PARAMETER; // Atomic enqueue/dequeue makes sense only if synchronized

	if(cbSize != rbc->ElementSize) return STATUS_BUFFER_OVERFLOW;

	WdfSpinLockAcquire(rbc->lock_enqueue);
	WdfSpinLockAcquire(rbc->lock_dequeue);

	head = rbc->Head;
	tail = rbc->Tail;

	if(tail != head)
	{
		// Enqueue
		if(!IsFull(rb))
		{
			char * buff = WdfMemoryGetBuffer(rbc->buffer, NULL);

			SelEl = buff + (head*rbc->ElementSize); 

			RtlCopyMemory(SelEl, element, cbSize);

			head = (head == rbc->NumElements-1) ? 0 : (head+1);

			rbc->Head = head;

			result = STATUS_SUCCESS;
		}

		// Dequeue
		head = rbc->Head;
		tail = rbc->Tail;

		if(tail != head)
		{
			char * buff = WdfMemoryGetBuffer(rbc->buffer, NULL);

			SelEl = buff+tail*rbc->ElementSize;

			RtlCopyMemory(element, SelEl, cbSize);

			tail = (tail == rbc->NumElements-1) ? 0 : (tail+1);

			rbc->Tail = tail;

			result = STATUS_SUCCESS;
		}
	}
	else result = STATUS_SUCCESS;

	WdfSpinLockRelease(rbc->lock_dequeue);
	WdfSpinLockRelease(rbc->lock_enqueue);

	return result;
}

BOOLEAN IsFull(IN WDFRINGBUFFER rb)
{
	WDFRINGBUFFER_CONTEXT *rbc = GetRingBufferContext(rb);
	unsigned long int head, tail;

	head = rbc->Head;
	tail = rbc->Tail;

	if(head < tail)	
		return ((tail-head)<2) ? TRUE : FALSE;
	if(head > tail) 
		return ((rbc->NumElements - (head-tail))<2) ? TRUE : FALSE;

	return FALSE;
}


VOID OnDestroy(IN WDFOBJECT Object)
{
    UNREFERENCED_PARAMETER(Object);
}

VOID OnCleanup(IN WDFOBJECT Object)
{
    UNREFERENCED_PARAMETER(Object);
}