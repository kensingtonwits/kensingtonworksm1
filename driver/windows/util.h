#pragma once
#include <ntddk.h>
#include <wdf.h>
#include <hidpddi.h>

/*
 * Given an WDFIOTARGET, returns the driver object name that it
 * points to.
 */
void GetWdfIoTargetDeviceObjectName(WDFIOTARGET target, PUNICODE_STRING name);

/*
 * Dumpts HID_CAPS structure given the HID preparsed report descriptor.
 */
void DumpHidCaps(PHIDP_PREPARSED_DATA pPreparsedData);

void DumpButtonCaps(PHIDP_PREPARSED_DATA pPreparsedData);

void DumpValueCaps(PHIDP_PREPARSED_DATA pPreparsedData);

// Returns number of bits set in a 32-bit value
UINT32 numberOfSetBits(UINT32 i);
