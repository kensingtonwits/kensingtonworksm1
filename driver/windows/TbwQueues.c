#include "ntddk.h"
#include <wdf.h>

#include "wdfringbuffer.h"
#include "tbwqueues.h"

NTSTATUS QueuesService(WDFQUEUE Queue, WDFSPINLOCK Lock, WDFRINGBUFFER RingBuffer, UCHAR *buffer, )
/*
{
	NTSTATUS status;
	WDFREQUEST ioReadReq;

	if(NT_SUCCESS(status = WdfIoQueueRetrieveNextRequest(Queue, &ioReadReq)))
	{
		if(NT_SUCCESS(status = WdfRingBufferDequeue(Ringbuffer, Report, Size)))
		{		
			WDFMEMORY memory;

			if(NT_SUCCESS(status = WdfRequestRetrieveOutputMemory(ioReadReq, &memory)))
			{
				if(NT_SUCCESS(WdfMemoryCopyFromBuffer(memory, 0, Report, Size)))
				{
					WdfRequestCompleteWithInformation(ioReadReq, STATUS_SUCCESS, Size);
				}
			}
		}
	}

	return status;
}
*/
{
	ULONG	requestsWaiting = 0L;

	WdfSpinLockAcquire(Lock);

	WdfIoQueueGetState(Queue, &requestsWaiting, NULL);

	if(requestsWaiting > 0)
	{
		do
		{
			if(NT_SUCCESS(WdfRingBufferDequeue(
		}
		while(requestsWaiting > 0);
	}
	