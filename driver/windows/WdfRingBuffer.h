/* RingBuffer.h */

#ifndef WDFRINGBUFFER_H
#define WDFRINGBUFFER_H

#pragma warning(disable:4201)

#include "ntddk.h"

#pragma warning(default:4201)

#include <wdf.h>

typedef WDFOBJECT WDFRINGBUFFER;

typedef struct _WDFRINGBUFFER_CONTEXT {

	WDFSPINLOCK	lock_enqueue;
	WDFSPINLOCK	lock_dequeue;
	BOOLEAN	synchronized;

	unsigned int ElementSize;
	unsigned long int NumElements;
	unsigned long int Tail;
	unsigned long int Head;
	
	WDFMEMORY buffer;
} WDFRINGBUFFER_CONTEXT, *PWDFRINGBUFFER_CONTEXT;

WDF_DECLARE_CONTEXT_TYPE_WITH_NAME(WDFRINGBUFFER_CONTEXT, GetRingBufferContext);

NTSTATUS WdfRingBufferCreate(WDF_OBJECT_ATTRIBUTES *attributes, 
							 WDFRINGBUFFER *object, 
							 ULONG NumElements, 
							 ULONG cbSize,
							 BOOLEAN synchronized);

NTSTATUS WdfRingBufferEnqueue(IN WDFRINGBUFFER, void * element, ULONG cbSize);
NTSTATUS WdfRingBufferDequeue(IN WDFRINGBUFFER, void * element, ULONG cbSize);
NTSTATUS WdfRingBufferEnqueueAndDequeue(IN WDFRINGBUFFER rb, void * element, ULONG cbSize);
#endif