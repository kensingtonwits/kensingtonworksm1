#include "tbwpdo.h"

#include "tbwconfig.h"
#include "tbwmain.h"
#include "wdfringbuffer.h"
#include "tbwkernelapi.h"
#include "TbwHID.h"
#include "../shared/trackball.h"
#include "./version.h"

#define USE_TBLIB

#define IOCTLInputBufferTooSmall(parameters, size) (parameters.Parameters.DeviceIoControl.InputBufferLength < size)

static VOID ServiceQueues(PPDO_DEVICE_EXTENSION PdoData);
static NTSTATUS SetUint32Parameter(IN WDFREQUEST Request, UINT32* param);
static NTSTATUS GetUint32Parameter(IN WDFREQUEST Request, UINT32 param);
static NTSTATUS GetDeviceInfo(PDEVICE_EXTENSION pDevExt, IN WDFREQUEST Request, size_t* pReturnedBytes);
static NTSTATUS SetButtonsConfig(PDEVICE_EXTENSION pDevExt, IN WDFREQUEST Request);
//static NTSTATUS EmulateDeviceAction(PDEVICE_EXTENSION pDevExt, IN WDFREQUEST Request);
static NTSTATUS RegisterHelper(WDFDEVICE pdoDevice, IN WDFREQUEST Request);

EVT_WDF_IO_QUEUE_IO_DEVICE_CONTROL	EvtIoDeviceControl;
EVT_WDF_IO_QUEUE_IO_READ			EvtPdoIoRead;
EVT_WDF_FILE_CLOSE					EvtPdoFileClose;

VOID PdoEnqueueEventAndService(WDFDEVICE PdoDevice, TBWKERNDATA *Event_pdo)
{
	PDO_DEVICE_EXTENSION *pdoData = PdoGetData(PdoDevice);

    // timestamp the report
    ULONG increment;
    increment = KeQueryTimeIncrement();
    KeQueryTickCount(&Event_pdo->time);
    Event_pdo->time.QuadPart = (Event_pdo->time.QuadPart * increment) / 10000;

    // queue it up
    WdfRingBufferEnqueue(pdoData->DataToSend, (UCHAR *) Event_pdo, sizeof(TBWKERNDATA));

    // tell queue handler to service the queue flushing its contents
	ServiceQueues(pdoData);
}

BOOLEAN IsHelperConected(WDFDEVICE pdohDevice)
{
    PPDO_DEVICE_EXTENSION pdoData = PdoGetData(pdohDevice);
    return pdoData->pfoHelper != NULL;
}

VOID ServiceQueues(PPDO_DEVICE_EXTENSION PdoData)
{
	TBWKERNDATA dequeuedEvent;
	ULONG requestsWaiting;
	WDFREQUEST request;

	WdfSpinLockAcquire(PdoData->ServiceQueuesLock);

	WdfIoQueueGetState(PdoData->ParkedReadRequestsQueue, &requestsWaiting, NULL);

	if(requestsWaiting > 0)
	{
		do
		{
			if(NT_SUCCESS(WdfRingBufferDequeue(PdoData->DataToSend, &dequeuedEvent, sizeof(TBWKERNDATA))))
			{
				if(NT_SUCCESS(WdfIoQueueRetrieveNextRequest(PdoData->ParkedReadRequestsQueue, &request)))
				{
					UCHAR *buff;
					size_t buffSize;

					if(NT_SUCCESS(WdfRequestRetrieveOutputBuffer(request, sizeof(TBWKERNDATA), &buff, &buffSize)))
					{
						// check buffSize
						RtlCopyMemory(buff, &dequeuedEvent, sizeof(TBWKERNDATA));
					}

					WdfRequestCompleteWithInformation(request, STATUS_SUCCESS, sizeof(TBWKERNDATA));
				}
			}
			else break;

			WdfIoQueueGetState(PdoData->ParkedReadRequestsQueue, &requestsWaiting, NULL);
		}
		while(requestsWaiting > 0);
	}
	
	WdfSpinLockRelease(PdoData->ServiceQueuesLock);
}

// Queue handlers.
VOID EvtPdoIoRead(IN WDFQUEUE Queue, IN WDFREQUEST Request, IN size_t Length)
{
    UNREFERENCED_PARAMETER(Length);
	WDFDEVICE				pdohDevice = WdfIoQueueGetDevice(Queue);
	PPDO_DEVICE_EXTENSION	pdoData = PdoGetData(pdohDevice);

	WdfRequestForwardToIoQueue(Request, pdoData->ParkedReadRequestsQueue);
	ServiceQueues(pdoData);
}

VOID pdoEvtIoDeviceControl(IN WDFQUEUE Queue, IN WDFREQUEST Request, IN size_t OutputBufferLength, IN size_t InputBufferLength, IN ULONG IoControlCode)
{
    NTSTATUS				status = STATUS_SUCCESS;
	WDFDEVICE				pdohDevice = WdfIoQueueGetDevice(Queue);
	PPDO_DEVICE_EXTENSION	pdoData = PdoGetData(pdohDevice);
	PDEVICE_EXTENSION		filterExt = FilterGetData(pdoData->Parent);
	WDF_REQUEST_PARAMETERS	params;
    size_t					returnedBytes = 0L;

    UNREFERENCED_PARAMETER(Queue);
    UNREFERENCED_PARAMETER(OutputBufferLength);
    UNREFERENCED_PARAMETER(InputBufferLength);

	WDF_REQUEST_PARAMETERS_INIT(&params);
	WdfRequestGetParameters(Request, &params);

	switch(IoControlCode)
	{
	case IOCTL_GETDDRIVERINFO:
		{
            TRACKBALL_DRIVER_INFO* pDriverInfo = NULL;

			DebugPrint(("[TBW] IOCTL: IOCTL_GETDDRIVERINFO: sizeof(TRACKBALL_DRIVER_INFO): %d\n", sizeof(TRACKBALL_DRIVER_INFO)));

			status = WdfRequestRetrieveOutputBuffer(Request, sizeof(TRACKBALL_DRIVER_INFO), &pDriverInfo, &returnedBytes);

			if(NT_SUCCESS(status)) 
			{
                pDriverInfo->cbSize = sizeof(TRACKBALL_DRIVER_INFO);
                pDriverInfo->uVersion = (((UINT32)VERSION_MAJOR) << 24)
                    | (((UINT32)VERSION_MINOR) << 16)
                    | (((UINT32)VERSION_PATCH) << 8)
                    | 0;
                DebugPrint(("[TBW] IOCTL: IOCTL_GETDDRIVERINFO version=0x%08x returnedBytes=%d\n", pDriverInfo->uVersion, returnedBytes));
				returnedBytes = sizeof(TRACKBALL_DRIVER_INFO);
			}
			else
			{
				DebugPrint(("[TBW] IOCTL: IOCTL_GETDDRIVERINFO (Retrieve input buffer failed with status %x\n", status));
			}
		}
		break;

    case IOCTL_SETSLOWPOINTER:
        DebugPrint(("[TBW] IOCTL: IOCTL_SETSLOWPOINTER\n"));
#ifdef USE_TBLIB
		{
			UINT32 bEnable = FALSE;
			status = SetUint32Parameter(Request, &bEnable);
			TBSetSlowPointer(&filterExt->tblibConfig, (BOOLEAN)bEnable);
		}
#else
        status = SetUint32Parameter(Request, &(filterExt->configData.bSlowPointer));
		if (NT_SUCCESS(status))
			returnedBytes = sizeof(UINT32);
#endif
        break;

    case IOCTL_GETSLOWPOINTER:
        DebugPrint(("[TBW] IOCTL: IOCTL_GETSLOWPOINTER\n"));
#ifdef USE_TBLIB
		status = GetUint32Parameter(Request, (UINT32)TBGetSlowPointer(&filterExt->tblibConfig));
#else
		status = GetUint32Parameter(Request, filterExt->configData.bSlowPointer);
#endif
		if (NT_SUCCESS(status))
            returnedBytes = sizeof(UINT32);
        break;

    case IOCTL_SETSINGLEAXISMOVT:
        DebugPrint(("[TBW] IOCTL: IOCTL_SETSINGLEAXISMOVT\n"));
#ifdef USE_TBLIB
		{
			UINT32 bEnable = FALSE;
			status = SetUint32Parameter(Request, &bEnable);
			TBSetLockedPointerAxis(&filterExt->tblibConfig, (BOOLEAN)bEnable);
		}
#else
		status = SetUint32Parameter(Request, &(filterExt->configData.bLockPointerAxis));
#endif
        if (NT_SUCCESS(status))
            returnedBytes = sizeof(UINT32);
        break;

    case IOCTL_GETSINGLEAXISMOVT:
        DebugPrint(("[TBW] IOCTL: IOCTL_GETSINGLEAXISMOVT\n"));
#ifdef USE_TBLIB
		status = GetUint32Parameter(Request, (UINT32)TBGetLockedPointerAxis(&filterExt->tblibConfig));
#else
		status = GetUint32Parameter(Request, filterExt->configData.bLockPointerAxis);
#endif
        if (NT_SUCCESS(status))
            returnedBytes = sizeof(UINT32);
        break;

    case IOCTL_SETSLOWPOINTERDIVIDER:
        DebugPrint(("[TBW] IOCTL: IOCTL_SETSLOWPOINTERDIVIDER\n"));
#ifdef USE_TBLIB
		status = STATUS_SUCCESS;
#else
        status = SetUint32Parameter(Request, &(filterExt->configData.uSlowdownDivider));
#endif
        if (NT_SUCCESS(status))
            returnedBytes = sizeof(UINT32);
        break;

    case IOCTL_GETSLOWPOINTERDIVIDER:
        DebugPrint(("[TBW] IOCTL: IOCTL_GETSLOWPOINTERDIVIDER\n"));
#ifdef USE_TBLIB
		status = STATUS_SUCCESS;
#else
		status = GetUint32Parameter(Request, filterExt->configData.uSlowdownDivider);
#endif
        if (NT_SUCCESS(status))
            returnedBytes = sizeof(UINT32);
        break;

    case IOCTL_SETSCROLLPARAMS:
        {
            SCROLL_PARAMS *pScrollParams = NULL;
            SCROLL_WHEEL_CONFIG* pScrollConfig = NULL;
            size_t bufSize = 0;

            DebugPrint(("[TBW] IOCTL: IOCTL_SETSCROLLPARAMS\n"));

            status = WdfRequestRetrieveInputBuffer(Request, sizeof(SCROLL_PARAMS), &pScrollParams, &bufSize);

            if (!NT_SUCCESS(status))
            {
                DebugPrint(("[TBW] \tWdfRequestRetrieveInputBuffer failed, rc: 0x%x\n", status));
                break;
            }

            if (bufSize < sizeof(SCROLL_PARAMS))
            {
                DebugPrint(("[TBW] \t(Buffer too small)\n"));
                status = STATUS_BUFFER_TOO_SMALL;
                break;
            }

            DebugPrint(("[TBW] \tscroll wheel type: %d\n", pScrollParams->ScrollWheelType));
#ifdef USE_TBLIB
			UNREFERENCED_PARAMETER(pScrollConfig);
			TBSetScrollParams(
				&filterExt->tblibConfig,
				pScrollParams->ScrollWheelType == SCROLL_WHEEL_VERT ? TBLIB_SCROLL_WHEEL_VERT : TBLIB_SCROLL_WHEEL_HORZ,
				(double)1,
				(BOOLEAN)pScrollParams->bInvertWheel,
				(BOOLEAN)pScrollParams->bInertialScroll
			);
			returnedBytes = sizeof(SCROLL_PARAMS);
#else
            if (pScrollParams->ScrollWheelType == SCROLL_WHEEL_VERT)
			{
                pScrollConfig = &(filterExt->configData.scrollParamsVert);
            }
			else if (pScrollParams->ScrollWheelType == SCROLL_WHEEL_HORZ)
			{
                pScrollConfig = &(filterExt->configData.scrollParamsHorz);
            }

            if (pScrollConfig)
			{
                DebugPrint(("[TBW] \tinvert wheel: %d\n", pScrollParams->bInvertWheel));
                DebugPrint(("[TBW] \tinertial scroll: %d\n", pScrollParams->bInertialScroll));
                pScrollConfig->bInvert = pScrollParams->bInvertWheel;
                pScrollConfig->bInertialScroll = pScrollParams->bInertialScroll ? 1 : 0;
                if (pScrollParams->fAttritionFactor > 0)
				{
                    pScrollConfig->fAttritionFactor = pScrollParams->fAttritionFactor;
                }
                returnedBytes = sizeof(SCROLL_PARAMS);
            }
#endif
        }
        break;

    case IOCTL_GETSCROLLPARAMS:
        {
            SCROLL_PARAMS *pScrollParams = NULL;
            SCROLL_WHEEL_CONFIG* pScrollConfig = NULL;
			double speed = 0;
			BOOLEAN bInvert = FALSE, bInertial = FALSE;

            DebugPrint(("[TBW] IOCTL: IOCTL_GETSCROLLPARAMS\n"));
         
            status = WdfRequestRetrieveOutputBuffer(Request, sizeof(SCROLL_PARAMS), &pScrollParams, &returnedBytes);
            if (!NT_SUCCESS(status))
            {
                DebugPrint(("[TBW] IOCTL: IOCTL_GETSCROLLPARAMS (Retrieve output buffer failed with status %x)\n", status));
                break;
            }
#ifdef USE_TBLIB
			UNREFERENCED_PARAMETER(pScrollConfig);
			TBGetScrollParams(
				&filterExt->tblibConfig,
				pScrollParams->ScrollWheelType == SCROLL_WHEEL_VERT ? TBLIB_SCROLL_WHEEL_VERT : TBLIB_SCROLL_WHEEL_HORZ,
				&speed,
				&bInvert,
				&bInertial);
			returnedBytes = sizeof(SCROLL_PARAMS);
#else
            if (pScrollParams->ScrollWheelType == SCROLL_WHEEL_VERT)
            {
                pScrollConfig = &(filterExt->configData.scrollParamsVert);
            }
            else if (pScrollParams->ScrollWheelType == SCROLL_WHEEL_HORZ)
            {
                pScrollConfig = &(filterExt->configData.scrollParamsHorz);
            }
            else
            {
                DebugPrint(("[TBW] IOCTL: IOCTL_GETSCROLLPARAMS unknown Scroll wheel type: %d\n", pScrollParams->ScrollWheelType));
                status = STATUS_INVALID_PARAMETER;
                break;
            }

            if (pScrollConfig)
            {
                pScrollParams->bInertialScroll = pScrollConfig->bInertialScroll ? 1 : 0;
                pScrollParams->fAttritionFactor = pScrollConfig->fAttritionFactor;
                returnedBytes = sizeof(SCROLL_PARAMS);
            }
#endif
            DebugPrint(("[TBW] IOCTL: IOCTL_GETSCROLLPARAMS returnedBytes=%d\n", returnedBytes));
        }
        break;

    case IOCTL_GETDEVICEINFO:
        DebugPrint(("[TBW] IOCTL: IOCTL_GETDEVICEINFO\n"));
        status = GetDeviceInfo(filterExt, Request, &returnedBytes);
        break;

    case IOCTL_SETBUTTONSCONFIG:
        DebugPrint(("[TBW] IOCTL: IOCTL_SETBUTTONSCONFIG\n"));
        status = SetButtonsConfig(filterExt, Request);
        break;

    case IOCTL_EMULATEDEVICEACTION:
        DebugPrint(("[TBW] IOCTL_EMULATEDEVICEACTION\n"));
//        status = EmulateDeviceAction(filterExt, Request);
        break;

    case IOCTL_REGISTERHELPER:
        DebugPrint(("[TBW] IOCTL_REGISTERHELPER\n"));
		status = RegisterHelper(pdohDevice, Request);
#ifdef USE_TBLIB
		TBSetHelperConnected(&filterExt->tblibConfig, 1);
#endif
        break;

    default:
		status = STATUS_INVALID_DEVICE_REQUEST;
	}

    WdfRequestCompleteWithInformation(Request, status, returnedBytes);

    return;
}

VOID EvtPdoFileClose(__in WDFFILEOBJECT FileObject)
{
	WDFDEVICE pdohDevice = WdfFileObjectGetDevice(FileObject);
	PPDO_DEVICE_EXTENSION pdoData = PdoGetData(pdohDevice);
	PDEVICE_EXTENSION filterExt = FilterGetData(pdoData->Parent);

    PFILE_OBJECT fileObj = WdfFileObjectWdmGetFileObject(FileObject);

    if (pdoData->pfoHelper == fileObj)
    {
        DebugPrint(("[TBW] EvtPdoFileClose - Helper client\n"));
        pdoData->pfoHelper = NULL;
#ifdef USE_TBLIB
		TBSetHelperConnected(&filterExt->tblibConfig, 0);
#else
		ConfigSetDefaults(&(filterExt->configData));
#endif
    }
    else
    {
        DebugPrint(("[TBW] EvtPdoFileClose - not helper client\n"));
    }
}

VOID PdoClose(WDFDEVICE PdoDevice)
{
	PDO_DEVICE_EXTENSION *pdoData;
	
	if(PdoDevice)
	{
		pdoData = PdoGetData(PdoDevice);

		WdfSpinLockAcquire(pdoData->ServiceQueuesLock);

		WdfIoQueuePurge(pdoData->ParkedReadRequestsQueue, WDF_NO_EVENT_CALLBACK, WDF_NO_CONTEXT);

		WdfSpinLockRelease(pdoData->ServiceQueuesLock);
	}
}

#define MAX_ID_LEN 128

NTSTATUS PdoCreate(WDFDEVICE Device, ULONG InstanceNo, WDFDEVICE *PPdoDevice)
{
    NTSTATUS                    status;
    PWDFDEVICE_INIT             pDeviceInit = NULL;
    PPDO_DEVICE_EXTENSION       pdoData = NULL;
    WDFDEVICE                   hChild = NULL;
    WDF_OBJECT_ATTRIBUTES       pdoAttributes;
    WDF_DEVICE_PNP_CAPABILITIES pnpCaps;
    WDF_IO_QUEUE_CONFIG         ioQueueConfig;
    WDF_DEVICE_STATE            deviceState;
	WDF_FILEOBJECT_CONFIG		fileObjectConfig;
	WDFQUEUE					ioReadQueue;

    DECLARE_CONST_UNICODE_STRING(deviceId, TBW_DEVICE_ID ); 
    DECLARE_CONST_UNICODE_STRING(hardwareId, TBW_DEVICE_ID ); 
    DECLARE_CONST_UNICODE_STRING(deviceLocation, L"Kensington TrackballWorks Driver\0" );
    DECLARE_UNICODE_STRING_SIZE(buffer, MAX_ID_LEN);
	DECLARE_CONST_UNICODE_STRING(SDDL_DEVOBJ_ALL,L"D:P(A;;GA;;;SY)(A;;GA;;;BA)(A;;GA;;;WD)");

	*PPdoDevice = NULL;

    //
    // Allocate a WDFDEVICE_INIT structure and set the properties
    // so that we can create a device object for the child.
    //
    pDeviceInit = WdfPdoInitAllocate(Device);

    if (pDeviceInit == NULL) 
	{
        status = STATUS_INSUFFICIENT_RESOURCES;
        goto Cleanup;
    }

    status = WdfPdoInitAssignRawDevice(pDeviceInit, &GUID_DEVCLASS_UNKNOWN); 
    if (!NT_SUCCESS(status)) 
	{
        goto Cleanup;
    }

    //
    // Since keyboard is secure device, we must protect ourselves from random
    // users sending ioctls and creating trouble.
    //
    status = WdfDeviceInitAssignSDDLString(pDeviceInit, &SDDL_DEVOBJ_ALL);
    if (!NT_SUCCESS(status)) 
	{
        goto Cleanup;
    }

    status = WdfPdoInitAssignDeviceID(pDeviceInit, &deviceId);
    if (!NT_SUCCESS(status)) 
	{
        goto Cleanup;
    }

    //
    // For RAW PDO, there is no need to provide BusQueryHardwareIDs
    // and BusQueryCompatibleIDs IDs unless we are running on
    // Windows 2000.
    //
    #pragma prefast(suppress:28159, "FALSE positive from PFD for downlevel OS's")        
    if (!IoIsWdmVersionAvailable(1, 0x20)) 
	{
        //
        // On Win2K, we must provide a HWID for the device to get enumerated.
        // Since we are providing a HWID, we will have to provide a NULL inf
        // to avoid the "found new device" popup and get the device installed silently.
        //
        status = WdfPdoInitAddHardwareID(pDeviceInit, &hardwareId);
        if (!NT_SUCCESS(status)) 
		{
            goto Cleanup;
        }
    }

    //
    // We could be enumerating more than one children if the filter attaches
    // to multiple instances of keyboard, so we must provide a
    // BusQueryInstanceID. If we don't, system will throw CA bugcheck.
    //
    status =  RtlUnicodeStringPrintf(&buffer, L"%02d", InstanceNo);
    if (!NT_SUCCESS(status)) 
	{
        goto Cleanup;
    }

    status = WdfPdoInitAssignInstanceID(pDeviceInit, &buffer);
    if (!NT_SUCCESS(status)) 
	{
        goto Cleanup;
    }

    status = RtlUnicodeStringPrintf(&buffer, L"TrackballWorks %02d", InstanceNo );
    if (!NT_SUCCESS(status)) 
	{
        goto Cleanup;
    }

    status = WdfPdoInitAddDeviceText(pDeviceInit, &buffer, &deviceLocation, 0x409);
    if (!NT_SUCCESS(status)) 
	{
        goto Cleanup;
    }

    WdfPdoInitSetDefaultLocale(pDeviceInit, 0x409);

    //
    // Initialize the attributes to specify the size of PDO device extension.
    // All the state information private to the PDO will be tracked here.
    //

	WDF_FILEOBJECT_CONFIG_INIT(&fileObjectConfig, WDF_NO_EVENT_CALLBACK, EvtPdoFileClose, WDF_NO_EVENT_CALLBACK);
	WdfDeviceInitSetFileObjectConfig(pDeviceInit, &fileObjectConfig, WDF_NO_OBJECT_ATTRIBUTES);

    WDF_OBJECT_ATTRIBUTES_INIT_CONTEXT_TYPE(&pdoAttributes, PDO_DEVICE_EXTENSION);

    status = WdfDeviceCreate(&pDeviceInit, &pdoAttributes, &hChild);
    if (!NT_SUCCESS(status)) 
	{
        goto Cleanup;
    }

    //
    // Get the device context.
    //
    pdoData = PdoGetData(hChild);

	RtlZeroMemory(pdoData, sizeof(PDO_DEVICE_EXTENSION));

    pdoData->InstanceNo = InstanceNo;
	pdoData->Parent = Device;

	WDF_IO_QUEUE_CONFIG_INIT_DEFAULT_QUEUE(&ioQueueConfig, WdfIoQueueDispatchParallel);
    ioQueueConfig.EvtIoDeviceControl = pdoEvtIoDeviceControl;
	ioQueueConfig.EvtIoRead = EvtPdoIoRead;
	ioQueueConfig.DefaultQueue = TRUE;
    status = WdfIoQueueCreate(hChild, &ioQueueConfig, WDF_NO_OBJECT_ATTRIBUTES, &ioReadQueue);
    if (!NT_SUCCESS(status)) goto Cleanup;

	WDF_IO_QUEUE_CONFIG_INIT(&ioQueueConfig, WdfIoQueueDispatchManual);
    status = WdfIoQueueCreate(hChild, &ioQueueConfig, WDF_NO_OBJECT_ATTRIBUTES, &pdoData->ParkedReadRequestsQueue);
    if (!NT_SUCCESS(status)) goto Cleanup;

	// Create Spinlock for Service Queues synchronization
	{
		WDF_OBJECT_ATTRIBUTES attributes;

		WDF_OBJECT_ATTRIBUTES_INIT(&attributes);
		attributes.ParentObject = hChild;
		status = WdfSpinLockCreate(&attributes, &pdoData->ServiceQueuesLock);

		if(!NT_SUCCESS(status))
		{
			DebugPrint(("[TBW] WdfSpinLockCreate failed 0x%x\n", status));
			return status;
		}
	}

	// Create ring buffer. This will serve as main message queue.
	{
		WDF_OBJECT_ATTRIBUTES attributes;

		WDF_OBJECT_ATTRIBUTES_INIT(&attributes);
		attributes.ParentObject = hChild;
		status = WdfRingBufferCreate(&attributes, &pdoData->DataToSend, PDO_DATATOSENDBUFFER_SIZE, sizeof(TBWKERNDATA), TRUE);
		if(!NT_SUCCESS(status))
		{
			DebugPrint(("[TBW] WdfRingBufferCreate failed 0x%x\n", status));
			return status;
		}
	}


    //
    // Set some properties for the child device.
    //
    WDF_DEVICE_PNP_CAPABILITIES_INIT(&pnpCaps);

    pnpCaps.Removable         = WdfTrue;
    pnpCaps.SurpriseRemovalOK = WdfTrue;
    pnpCaps.NoDisplayInUI     = WdfTrue;

    pnpCaps.Address  = InstanceNo;
    pnpCaps.UINumber = InstanceNo;

    WdfDeviceSetPnpCapabilities(hChild, &pnpCaps);

    //
    // TODO: In addition to setting NoDisplayInUI in DeviceCaps, we
    // have to do the following to hide the device. Following call
    // tells the framework to report the device state in
    // IRP_MN_QUERY_DEVICE_STATE request.
    //
    WDF_DEVICE_STATE_INIT(&deviceState);
    deviceState.DontDisplayInUI = WdfTrue;
    WdfDeviceSetDeviceState(hChild, &deviceState);

    //
    // Tell the Framework that this device will need an interface so that
    // application can find our device and talk to it.
    //
	DebugPrint(("[TBW] WdfDeviceCreateDeviceInterface\n"));
    status = WdfDeviceCreateDeviceInterface(hChild, &GUID_DEVINTERFACE_KNSTBW, NULL);

    if (!NT_SUCCESS (status)) goto Cleanup;

    status = WdfFdoAddStaticChild(Device, hChild);
    if (!NT_SUCCESS(status)) 
	{
        goto Cleanup;
    }

	*PPdoDevice = hChild;

	DebugPrint(("[TBW] CreateRawPdo succeeded\n"));

    return STATUS_SUCCESS;

Cleanup:

	//WdfVerifierDbgBreakPoint();

    //
    // Call WdfDeviceInitFree if you encounter an error while initializing
    // a new framework device object. If you call WdfDeviceInitFree,
    // do not call WdfDeviceCreate.
    //
    if (pDeviceInit != NULL) 
	{
        WdfDeviceInitFree(pDeviceInit);
    }

    if(hChild) 
	{
        WdfObjectDelete(hChild);
    }

    return status;
}

// Set a 32-bit value supplied by the userspace module into the variable pointed
// to by 'param'
NTSTATUS SetUint32Parameter(IN WDFREQUEST Request, UINT32* param)
{
    UINT32* enable = 0;
    size_t bufSize = 0;

    // Get the value supplied by userspace call
    NTSTATUS status = WdfRequestRetrieveInputBuffer(Request, sizeof(UINT32), &enable, &bufSize);
    if (!NT_SUCCESS(status))
    {
        DebugPrint(("[TBW]: SetUint32Parameter - WdfRequestRetrieveInputBuffer failed, rc: 0x%x\n", status));
        return status;
    }

    if (bufSize < sizeof(UINT32))
    {
        DebugPrint(("[TBW]: SetUint32Parameter - buffer too small\n"));
        return STATUS_BUFFER_TOO_SMALL;
    }

    DebugPrint(("[TBW]: SetUint32Parameter enable: %d\n", *enable));
    *param = *enable ? 1: 0;

    return STATUS_SUCCESS;
}

// Return a 32bit value in 'param' to the userspace module making the IOCTL request
NTSTATUS GetUint32Parameter(IN WDFREQUEST Request, UINT32 param)
{
    UINT32* enabled;
    size_t returnedBytes = 0;

    DebugPrint(("[TBW]: GetUint32Parameter\n"));
    NTSTATUS status = WdfRequestRetrieveOutputBuffer(Request, sizeof(UINT32), &enabled, &returnedBytes);
    if (!NT_SUCCESS(status))
    {
        DebugPrint(("[TBW]: GetUint32Parameter - WdfRequestRetrieveOutputBuffer failed, rc: 0x%x\n", status));
        return status;
    }

    *enabled = param;
    DebugPrint(("[TBW] IOCTL: GetUint32Parameter value=%d\n", param));

    return STATUS_SUCCESS;
}

NTSTATUS GetDeviceInfo(PDEVICE_EXTENSION pDevExt, IN WDFREQUEST Request, size_t* pReturnedBytes)
{
    TRACKBALL_DEVICE_INFO* pDI = { 0 };
    size_t returnedBytes = 0;

    DebugPrint(("[TBW]: GetDeviceInfo\n"));
    NTSTATUS status = WdfRequestRetrieveOutputBuffer(Request, sizeof(TRACKBALL_DEVICE_INFO), &pDI, &returnedBytes);
    if (!NT_SUCCESS(status))
    {
        DebugPrint(("[TBW]: GetDeviceInfo - WdfRequestRetrieveOutputBuffer failed, rc: 0x%x\n", status));
        return status;
    }

    pDI->usProductId = pDevExt->usProductId;
    pDI->usVersion = pDevExt->deviceVersion;
    RtlStringCbCopyW(pDI->szName, sizeof(pDI->szName), pDevExt->szProductName);
    RtlStringCbCopyW(pDI->szSerialNumber, sizeof(pDI->szSerialNumber), pDevExt->szSerialNumber);

    *pReturnedBytes = returnedBytes;

    return STATUS_SUCCESS;
}

NTSTATUS SetButtonsConfig(PDEVICE_EXTENSION pDevExt, IN WDFREQUEST Request)
{
    TBWBUTTONSCONFIG* pButtonsConfig = NULL;
    size_t bufSize = 0;

    UNREFERENCED_PARAMETER(pDevExt);

    // Get the value supplied by userspace call
    NTSTATUS status = WdfRequestRetrieveInputBuffer(Request, sizeof(TBWBUTTONSCONFIG), &pButtonsConfig, &bufSize);
    if (!NT_SUCCESS(status)/* && status != STATUS_BUFFER_TOO_SMALL*/)
    {
        DebugPrint(("[TBW]: SetButtonsAction - WdfRequestRetrieveInputBuffer failed, rc: 0x%x\n", status));
        return status;
    }
#ifdef USE_TBLIB
    // Convert TBWBUTTONSCONFIG to TBLIBBUTTONSCONFIG
    // Physically copying the structure elements one-by-one helps eliminate any 
    // structure element alignment difference between user mode and driver mode
    // code, even between the seemingly similarly declared structures, but owing
    // to compiler optimizations required for kernel mode code.
    TBLIBBUTTONSCONFIG2 tbButtonsConfig = { 0 };
    tbButtonsConfig.cbSize = sizeof(tbButtonsConfig);
    tbButtonsConfig.chordMask = pButtonsConfig->chordMask;
	//&*&*&*G1_ADD
	memcpy(tbButtonsConfig.chordMsks, pButtonsConfig->chordMsks, MAX_CHORD_BUTTONS_NO * sizeof(uint32_t));
	//&*&*&*G2_ADD
    for (size_t i = 0; i < pButtonsConfig->nButtons; i++)
    {
        const TBWBUTTONACTION* pButtonAction = &pButtonsConfig->buttons[i];
        tbButtonsConfig.buttons[i].buttonMask = pButtonAction->buttonMask;
        if (pButtonAction->action == BUTTON_ACTION_CLICK)
        {
            tbButtonsConfig.buttons[i].action = TBLIB_BUTTON_ACTION_CLICK;
            tbButtonsConfig.buttons[i].u.ClickActionParams.buttonMask = pButtonAction->u.ClickActionParams.buttonMask;
        }
        else if (pButtonAction->action == BUTTON_ACTION_SCROLL)
        {
            tbButtonsConfig.buttons[i].action = TBLIB_BUTTON_ACTION_SCROLL;
            // scroll direction macros map directly between TbwKernelAPI.h & trackball.h
            tbButtonsConfig.buttons[i].u.ScrollActionParams.direction =
                pButtonAction->u.ScrollActionParams.direction;
            tbButtonsConfig.buttons[i].u.ScrollActionParams.lines =
                pButtonAction->u.ScrollActionParams.lines;
        }
    }
    tbButtonsConfig.nButtons = pButtonsConfig->nButtons;
	TBSetButtonsConfig(&pDevExt->tblibConfig, &tbButtonsConfig);
	return STATUS_SUCCESS;
#else
    return ConfigSetButtonsConfig(&pDevExt->configData, pButtonsConfig);
#endif
}

#if 0
NTSTATUS EmulateDeviceAction(PDEVICE_EXTENSION pDevExt, IN WDFREQUEST Request)
{
    TBWEMULATEDEVICEACTION* pActionParam = NULL;
    size_t bufSize = 0;

    NTSTATUS status = WdfRequestRetrieveInputBuffer(Request, sizeof(TBWEMULATEDEVICEACTION), &pActionParam, &bufSize);
    if (!NT_SUCCESS(status))
    {
        DebugPrint(("[TBW]: EmulateDeviceAction - WdfRequestRetrieveInputBuffer failed, rc: 0x%x\n", status));
        return status;
    }

    switch (pActionParam->uAction)
    {
    case DA_BUTTONCLICK:
        {
            UINT32 uButtonMask = (UINT32)pActionParam->u.ButtonClick.mask;
            UINT32 uDir = (UINT32)pActionParam->u.ButtonClick.dir;  // 1 - down, 2 - up, 3 - both
            DebugPrint(("[TBW]: EmulateDeviceAction - button click, mask: 0x%04x, dir: %u\n", uButtonMask, uDir));
            if (uDir == 1)
            {
                // DOWN
                //DispatchButtonEvent(pDevExt, uButtonMask, 0);
				PostButtonEvent(pDevExt, uButtonMask, 0);
            }
            else if (uDir == 2)
            {
                // UP
                //DispatchButtonEvent(pDevExt, 0, uButtonMask);
				PostButtonEvent(pDevExt, 0, uButtonMask);
            }
            else if (uDir == 3)
            {
                // DOWN & UP
                //DispatchButtonEvent(pDevExt, uButtonMask, 0);
                //DispatchButtonEvent(pDevExt, 0, uButtonMask);
				PostButtonEvent(pDevExt, uButtonMask, 0);
				PostButtonEvent(pDevExt, 0, uButtonMask);
            }
            else
            {
                DebugPrint(("[TBW]: EmulateDeviceAction - invalid direction specified\n"));
                status = STATUS_INVALID_PARAMETER;
            }
        }
        break;

    default:
        {
            status = STATUS_INVALID_PARAMETER;
        }
        break;
    }
    return status;
}
#endif

NTSTATUS RegisterHelper(WDFDEVICE pdohDevice, IN WDFREQUEST Request)
{
    PPDO_DEVICE_EXTENSION   pdoData = PdoGetData(pdohDevice);

    DebugPrint(("[TBW] Helper app registered\n"));
    UNREFERENCED_PARAMETER(Request);

    pdoData->pfoHelper = IoGetCurrentIrpStackLocation(WdfRequestWdmGetIrp(Request))->FileObject;

    return STATUS_SUCCESS;
}
