#ifndef _DEBUG_H_
#define _DEBUG_H_

// A few useful defines

#if DBG

#define TRAP() DbgBreakPoint()

#define DebugPrint(_x_) DbgPrint _x_

#else   // DBG

#define TRAP()

#define DebugPrint(_x_)

#endif

#define MIN(_A_,_B_) (((_A_) < (_B_)) ? (_A_) : (_B_))

#endif