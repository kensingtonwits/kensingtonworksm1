
#include "tbwmain.h"
#include "common.h"
#include "wdfringbuffer.h"
#include "tbwconfig.h"
#include "tbwhid.h"
#include "tbwpdo.h"
#include "tbwkernelapi.h"
#include "util.h"
#include "version.h"

#define USE_TBLIB

DRIVER_INITIALIZE DriverEntry;

EVT_WDF_DRIVER_DEVICE_ADD					EvtDeviceAdd;
EVT_WDF_DEVICE_PREPARE_HARDWARE				EvtPrepareHardware;
EVT_WDF_DEVICE_RELEASE_HARDWARE             EvtReleaseHardware;
EVT_WDF_TIMER								EvtTimerInertialScroll;
#ifdef SCROLL_WITH_TRACKBALL
EVT_WDF_TIMER								EvtTimerAutoScroll;
#endif
#ifdef ENABLE_CHORDING_TIMER
EVT_WDF_TIMER								EvtTimerDetectChord;
#endif
EVT_WDF_TIMER								EvtTimerButtonDebounce;
EVT_WDF_TIMER								EvtTimerTiltButton;
EVT_WDF_IO_QUEUE_IO_INTERNAL_DEVICE_CONTROL EvtIoInternalDeviceControl;
EVT_WDF_DEVICE_FILE_CREATE					EvtFileCreate;
EVT_WDF_FILE_CLOSE							EvtFileClose;
EVT_WDF_DEVICE_QUERY_REMOVE					EvtQueryRemove;

#ifdef ALLOC_PRAGMA
#pragma alloc_text (INIT, DriverEntry)
//#pragma alloc_text (PAGE, EvtDeviceAdd)
//#pragma alloc_text (PAGE, EvtPrepareHardware)
//#pragma alloc_text (PAGE, EvtTimer)
//#pragma alloc_text (PAGE, EvtChordTimer)
//#pragma alloc_text (PAGE, EvtIoInternalDeviceControl)
//#pragma alloc_text (PAGE, ExtIoRead)
//#pragma alloc_text (PAGE, EvtIoReadCompletion)
//#pragma alloc_text (PAGE, EvtFileCreate)
//#pragma alloc_text (PAGE, EvtFileClose)
#endif

#pragma warning(push)
#pragma warning(disable:4055) // type case from PVOID to PSERVICE_CALLBACK_ROUTINE
#pragma warning(disable:4152) // function/data pointer conversion in expression

const uint32_t PAN_LEFT_BUTTON_MASK = 0x01u << 30;   // bit 30
const uint32_t PAN_RIGHT_BUTTON_MASK = 0x01u << 31;  // bit 31
const uint32_t PAN_BUTTON_STATE_MASK = 0xC0000000u;  // bits 30 & 31


static NTSTATUS ReadRequestFormatAndSend(WDFDEVICE Device, NEWREQUEST Request);
static VOID EvtIoReadCompletion(IN WDFREQUEST Request, IN WDFIOTARGET Target, PWDF_REQUEST_COMPLETION_PARAMS CompletionParams, IN WDFCONTEXT Context);
static NTSTATUS StartHidReader(WDFDEVICE Device, ULONG Length);
static NTSTATUS RepostHidReader(WDFDEVICE Device, WDFREQUEST Request);

/*
 * The following three functions take an HID input report and process any
 * embedded button, pointer or scroll wheel event embedded in them
 * respectively.
 *
 * If the report does not contain events of any of the types that the
 * functions handle, then it is ignored.
 *
 * A typical usage pattern for these methods would be to pass an incoming
 * report to all these functions (in any order) and let them do their work
 * if the report contains any data that's relevant to their individual role.
 */
static void HIDReportHandleButtonEvents(PDEVICE_EXTENSION pDevExt, PCHAR pReport, ULONG ulReportLength);
static void HIDReportHandlePointerEvents(PDEVICE_EXTENSION pDevExt, PCHAR pReport, ULONG ulReportLength);
static void HIDReportHandleWheelEvents(PDEVICE_EXTENSION pDevExt, PCHAR pReport, ULONG ulReportLength);
static void HIDReportHandlePanEvents(PDEVICE_EXTENSION pDevExt, PCHAR pReport, ULONG ulReportLength);

#ifndef USE_TBLIB
/*
Applies slow pointer metrics to dx & dy if it's enabled. If it's not enabled
does nothing.
*/
static void ApplySlowPointer(PDEVICE_EXTENSION, LONG* dx, LONG* dy);

/*
Applies single axis movement metrics, if it's enabled. If it's not enabled
does nothing.
*/
static void ApplySingleAxisMovement(PDEVICE_EXTENSION, LONG* dx, LONG* dy);

/*
Applies inertial scroll processing to wheel movement.
*/
static void ApplyCustomScrollConfig(PDEVICE_EXTENSION pDevExt, SCROLL_WHEEL_CONFIG* pScrollConfig, LONG* plWheel);
/*
 * Based on what bits are set in down/up mask arguments, does chord pre-processing.
 * If chording is defined and some bits in mask arguments were used to detect or
 * generate chording event, these bits are removed from the mask arguments. Hence
 * passing arguments by reference.
 */
static void DetectChords(PDEVICE_EXTENSION pDevExt, UINT32* pDownMask, UINT32* pUpMask);

/*
 * Function to play back the cached chord detect events.
 */
static void PostCachedChordDetectEvents(PDEVICE_EXTENSION pDeviceExt);

/*
 * Function returns a boolean indicating if the button event in mask parameter
 * is to be passed to the to the user module.
 *
 * TRUE return value indicates that the event has to be passed to the user
 * module and FALSE means event is to tbe sent to the HID stack.
 */
static BOOLEAN IsUserModuleButtonEvent(PDEVICE_EXTENSION pDevExt, UINT32 maskIn, UINT32* maskOut);

#endif
/*
 * Timer interval (milliseconds) for introducing inertial scroll wheel events.
 */
//const LONG INERTIAL_SCROLL_TIMER_INTERVAL = 70;

/*
 * Timer interval (milliseconds) for detecting chord events
 */
//const LONG CHORD_DETECT_TIMER_INTERVAL = 80;

/*
 * Post pointer movement event to mouse class driver
 */
static void PostPointerEvent(PDEVICE_EXTENSION pDevExt, LONG dx, LONG dy);

/*
 * Post button up/down event to mouse class driver from HID Report bitmask.
 */
static void PostButtonEvent(
    PDEVICE_EXTENSION pDevExt,
    UINT32 uDownMask,
    UINT32 uUpMask
    );

/*
 * Post mouse wheel event to mouse class driver
 */
static void PostWheelEvent(PDEVICE_EXTENSION pDeviceExt, SHORT wheel, LONG units);

/*
 * Fix for SlimBlade report which puts button 3 & 4 status in
 * byte 5.
 */
static void GetButtonStateSlimBlade(PDEVICE_EXTENSION pDevExt, PUCHAR pReport, UINT32* pDownMask, UINT32* pUpMask);

/*
 * Function to parse button states of Vertical Trackball manually.
 * This is required as Vertical Trackball firmware wrongly reports only
 * 5 buttons wheras the device has 7 buttons. So the last two buttons
 * will not be parsed by standard HID parser.
 */
static void GetButtonStateVerticalTrackball(PDEVICE_EXTENSION pDevExt, PUCHAR pReport, UINT32* pDownMask, UINT32* pUpMask);

ULONG InstanceNo = 0;

/* tblib support routines - end */
void tblibPostButtonEvent(void* ref, uint32_t buttonState, uint32_t uDownMask, uint32_t uUpMask);
void tblibPostCursorEvent(void* ref, int dx, int dy);
void tblibPostWheelEvent(void* ref, int wheel, int offset);
void tblibEnqueueButtonEvent(void* ref, uint32_t uDownState, uint32_t uUpState);
uint32_t tblibStartTimer(void* ref, uint32_t id, uint32_t timeout);
void tblibStopTimer(void* ref, uint32_t id);

/* tblib support routines - end */
NTSTATUS DriverEntry(IN PDRIVER_OBJECT DriverObject, IN PUNICODE_STRING RegistryPath)
{
    WDF_DRIVER_CONFIG               config;
    NTSTATUS                        status;

    DebugPrint(("[TBW] Kensington TrackballWorks Kernel module\n"));
    DebugPrint(("[TBW] Version: %s, Built %s %s\n", _FILEVERSION_STR, __DATE__, __TIME__));

    CommonInit();

    WDF_DRIVER_CONFIG_INIT(&config, EvtDeviceAdd);

    status = WdfDriverCreate(DriverObject, RegistryPath, WDF_NO_OBJECT_ATTRIBUTES, &config, WDF_NO_HANDLE);
	if (!NT_SUCCESS(status)) DebugPrint( ("[TBW] WdfDriverCreate failed with status 0x%x\n", status));

    return status;
}

// Callbacks
NTSTATUS EvtDeviceAdd(IN WDFDRIVER Driver, IN PWDFDEVICE_INIT DeviceInit)
{
    WDF_OBJECT_ATTRIBUTES   deviceAttributes;
    NTSTATUS                status;
    WDFDEVICE               hDevice;
    PDEVICE_EXTENSION       filterExt;
    WDF_IO_QUEUE_CONFIG     ioQueueConfig;
	WDFQUEUE				queue;
	WDF_FILEOBJECT_CONFIG	fileConfig;

    UNREFERENCED_PARAMETER(Driver);

    PAGED_CODE();

	DebugPrint(("[TBW] EvtDeviceAdd\n"));

	//WdfVerifierDbgBreakPoint();

    WdfFdoInitSetFilter(DeviceInit);

	WdfDeviceInitSetDeviceType(DeviceInit, FILE_DEVICE_MOUSE);

	// Configure PnP callbacks for late initialization
	{
		WDF_PNPPOWER_EVENT_CALLBACKS pnpPowerCallbacks;

		WDF_PNPPOWER_EVENT_CALLBACKS_INIT(&pnpPowerCallbacks);
		pnpPowerCallbacks.EvtDevicePrepareHardware = EvtPrepareHardware;
        pnpPowerCallbacks.EvtDeviceReleaseHardware = EvtReleaseHardware;
		pnpPowerCallbacks.EvtDeviceQueryRemove = EvtQueryRemove;

		WdfDeviceInitSetPnpPowerEventCallbacks(DeviceInit, &pnpPowerCallbacks);
	}

    WDF_OBJECT_ATTRIBUTES_INIT_CONTEXT_TYPE(&deviceAttributes, DEVICE_EXTENSION);

	WDF_FILEOBJECT_CONFIG_INIT(&fileConfig, EvtFileCreate, EvtFileClose, WDF_NO_EVENT_CALLBACK);
	WdfDeviceInitSetFileObjectConfig(DeviceInit, &fileConfig, WDF_NO_OBJECT_ATTRIBUTES);

    status = WdfDeviceCreate(&DeviceInit, &deviceAttributes, &hDevice);
    if (!NT_SUCCESS(status))
	{
        DebugPrint(("[TBW] WdfDeviceCreate failed with status code 0x%x\n", status));
        return status;
    }

    filterExt = FilterGetData(hDevice);
	RtlZeroMemory(filterExt, sizeof(DEVICE_EXTENSION));

	filterExt->WdfDevice = hDevice;

#ifndef USE_TBLIB
	{
		status = ConfigInit(hDevice, &(filterExt->configData));

		if(!NT_SUCCESS(status))
		{
			DebugPrint(("[TBW] ConfigInitDefault failed 0x%x\n", status));
			return status;
		}

		// load additional defaults from the registry
		ConfigLoadFromRegistry(&(filterExt->configData.current));
	}
#endif

	// Create Spinlock for HandleEvent
	{
		WDF_OBJECT_ATTRIBUTES attributes;

		WDF_OBJECT_ATTRIBUTES_INIT(&attributes);
		attributes.ParentObject = hDevice;
		status = WdfSpinLockCreate(&attributes, &filterExt->HandleEventLock);

		if(!NT_SUCCESS(status))
		{
			DebugPrint(("[TBW] WdfSpinLockCreate failed 0x%x\n", status));
			return status;
		}
	}

	// Configure the default queue for the IoRead
	{
		WDF_IO_QUEUE_CONFIG_INIT(&ioQueueConfig, WdfIoQueueDispatchParallel);
		ioQueueConfig.EvtIoInternalDeviceControl = EvtIoInternalDeviceControl;

		status = WdfIoQueueCreate(hDevice, &ioQueueConfig, WDF_NO_OBJECT_ATTRIBUTES, &queue);
		if (!NT_SUCCESS(status))
		{
		   DebugPrint( ("[TBW] WdfIoQueueCreate failed 0x%x\n", status));
		   return status;
		}

		status = WdfDeviceConfigureRequestDispatching(hDevice, queue, WdfRequestTypeDeviceControlInternal);
		if (!NT_SUCCESS(status))
		{
		   return status;
		}
	}

    // Create inertial scroll timer. This timer won't be started now. It'll be
    // started when wheel motion is detected. It'll also terminate itself
    // when after constant application of the inertial attrition factor results
    // in a 0 scroll wheel result.
    {
        WDF_TIMER_CONFIG timerConfig = { 0 };
        WDF_OBJECT_ATTRIBUTES  timerAttributes = { 0 };

        WDF_TIMER_CONFIG_INIT_PERIODIC(
            &timerConfig,
            EvtTimerInertialScroll,
            INERTIAL_SCROLL_TIMER_INTERVAL);
        timerConfig.AutomaticSerialization = TRUE;

        WDF_OBJECT_ATTRIBUTES_INIT(&timerAttributes);
        timerAttributes.ParentObject = hDevice;

        status = WdfTimerCreate(
            &timerConfig,
            &timerAttributes,
            &filterExt->timerInertialScroll);

        if (!NT_SUCCESS(status))
        {
            DebugPrint(("[TBW] Creating inertial scroll timer failed 0x%x\n", status));
            return status;
        }

#ifdef SCROLL_WITH_TRACKBALL
		// Create auto scroll timer.
		WDF_TIMER_CONFIG_INIT_PERIODIC(
			&timerConfig,
			EvtTimerAutoScroll,
			AUTO_SCROLL_TIMER_INTERVAL);

		timerConfig.AutomaticSerialization = TRUE;
		WDF_OBJECT_ATTRIBUTES_INIT(&timerAttributes);
		timerAttributes.ParentObject = hDevice;

		status = WdfTimerCreate(
			&timerConfig,
			&timerAttributes,
			&filterExt->timerAutoScroll);

		if (!NT_SUCCESS(status))
		{
			DebugPrint(("[TBW] Creating auto scroll timer failed 0x%x\n", status));
			return status;
		}
		filterExt->tblibConfig.bAutoScrollTimerStarted = FALSE;

#endif
    }

#ifdef ENABLE_CHORDING_TIMER
	// Create the timer for chording.
	{
		WDF_TIMER_CONFIG timerConfig;
		WDF_OBJECT_ATTRIBUTES  timerAttributes;

		WDF_TIMER_CONFIG_INIT(&timerConfig, EvtTimerDetectChord);
		timerConfig.AutomaticSerialization = TRUE;

		WDF_OBJECT_ATTRIBUTES_INIT(&timerAttributes);
		timerAttributes.ParentObject = hDevice;

		status = WdfTimerCreate(
            &timerConfig,
            &timerAttributes,
            &filterExt->timerDetectChord);

		if (!NT_SUCCESS(status))
		{
			DebugPrint(("[TBW] Creating timer for chords failed 0x%x\n", status));
			return status;
		}
	}
#endif
	// Create the timer for button debounce.
	{
		WDF_TIMER_CONFIG timerConfig;
		WDF_OBJECT_ATTRIBUTES  timerAttributes;

		WDF_TIMER_CONFIG_INIT(&timerConfig, EvtTimerButtonDebounce);
		timerConfig.AutomaticSerialization = TRUE;

		WDF_OBJECT_ATTRIBUTES_INIT(&timerAttributes);
		timerAttributes.ParentObject = hDevice;

		status = WdfTimerCreate(
			&timerConfig,
			&timerAttributes,
			&filterExt->timerButtonDebounce);

		if (!NT_SUCCESS(status))
		{
			DebugPrint(("[TBW] Creating timer for debounce failed 0x%x\n", status));
			return status;
		}
	}

	// Create the timer for tilt buttons.
	{
		WDF_TIMER_CONFIG timerConfig;
		WDF_OBJECT_ATTRIBUTES  timerAttributes;

		WDF_TIMER_CONFIG_INIT(&timerConfig, EvtTimerTiltButton);
		timerConfig.AutomaticSerialization = TRUE;

		WDF_OBJECT_ATTRIBUTES_INIT(&timerAttributes);
		timerAttributes.ParentObject = hDevice;

		status = WdfTimerCreate(
			&timerConfig,
			&timerAttributes,
			&filterExt->timerTiltButton);

		if (!NT_SUCCESS(status))
		{
			DebugPrint(("[TBW] Creating timer for tilt buttons failed 0x%x\n", status));
			return status;
		}
	}

	// Create the Pdo channel for upper layer communication
	{
		status = PdoCreate(hDevice, ++InstanceNo, &filterExt->pdohDevice);
        if (!NT_SUCCESS(status))
        {
            DebugPrint(("[TBW] Error creating PDO, status: 0x%x\n", status));
            return status;
        }
	}

	TBInit(
		&filterExt->tblibConfig,
		(void*)filterExt,
		tblibPostButtonEvent,
		tblibPostCursorEvent,
		tblibPostWheelEvent,
		tblibEnqueueButtonEvent,
		tblibStartTimer,
		tblibStopTimer);

    return status;
}

NTSTATUS EvtPrepareHardware(WDFDEVICE Device, WDFCMRESLIST Resources, WDFCMRESLIST ResourcesTranslated)
{
    UNREFERENCED_PARAMETER(ResourcesTranslated);
    UNREFERENCED_PARAMETER(Resources);

    PDEVICE_EXTENSION filterExt;
    NTSTATUS status = STATUS_SUCCESS;

    PAGED_CODE();

    DebugPrint(("[TBW] EvtPrepareHardware\n"));

    filterExt = FilterGetData(Device);

    // Get device information
    status = HidGetDeviceInfo(Device, &(filterExt->usVendorId), &(filterExt->usProductId), &(filterExt->deviceVersion));
    if (!NT_SUCCESS(status))
    {
        DebugPrint(("[TBW] Error getting device info, status: %0x\n", status));
        return status;
    }

	filterExt->bHasACPanButtons = FALSE;
	if (filterExt->usVendorId == kVID_KENSINGTON)
	{
		switch (filterExt->usProductId)
		{
			case 32872: // wireless vertical trackball
			case 32873: // wireless vertical trackball
			case 32925: // wired vertical trackball
				filterExt->bHasACPanButtons = TRUE;
				break;
			default:
				break;
		}
	}

    ULONG ulDescriptorSize = 0;
    status = HidGetPreparsedData(
        Device,
        &filterExt->hidPreparsedData,
        &ulDescriptorSize);
    if (!NT_SUCCESS(status))
    {
        DebugPrint(("[TBW] Error retrieving HIDPREPARSEDATA, status: %0x\n", status));
        return status;
    }

#ifdef DBG
    //DumpHidCaps(filterExt->hidPreparsedData);
    //DumpButtonCaps(filterExt->hidPreparsedData);
    //DumpValueCaps(filterExt->hidPreparsedData);
#endif

    // initialize HID capability structures
    status = HidP_GetCaps(filterExt->hidPreparsedData, &filterExt->hidCaps);
    if (!NT_SUCCESS(status))
    {
        DebugPrint(("[TBW] Error retrieving HIDCAPS from HID preparsed data, status: %0x\n", status));
        ExFreePool(filterExt->hidPreparsedData);
        filterExt->hidPreparsedData = NULL;
        return status;
    }

    filterExt->uReportMaxSize = filterExt->hidCaps.InputReportByteLength;

    // Read product id, firmware version, USB product name & serial number
    // Both szProductName & szSerialNumber string buffers are of the same length
    size_t cbName = sizeof(filterExt->szProductName) / sizeof(filterExt->szProductName[0]);
    HidGetProductString(Device, filterExt->szProductName, cbName);
    HidGetSerialNumberString(Device, filterExt->szSerialNumber, cbName);

    filterExt->nMaxUsagesLength = HidP_MaxUsageListLength(
        HidP_Input,
        HID_USAGE_PAGE_BUTTON,
        filterExt->hidPreparsedData);

    DebugPrint(("[TBW] %ws, Id: %d, Version: %d, Report Size: %d, MaxUsagesLen: %d, Serialnumber: %ws\n",
        filterExt->szProductName,
        filterExt->usProductId,
        filterExt->deviceVersion,
        filterExt->uReportMaxSize,
        filterExt->nMaxUsagesLength,
        filterExt->szSerialNumber));

    filterExt->pUsageMem = NULL;
    filterExt->pCurUsage = NULL;
    filterExt->pPrevUsage = NULL;
    filterExt->pUsageBreak = NULL;
    filterExt->pUsageMake = NULL;

    SIZE_T cbUsageMem = sizeof(USAGE)*filterExt->nMaxUsagesLength * 4;
    filterExt->pUsageMem = (PUSAGE)ExAllocatePoolWithTag(
        NonPagedPoolNx,
        cbUsageMem,
        'TBW');

    if (!filterExt->pUsageMem)
    {
        DebugPrint(("[TBW] Error allocating memory for processing USAGES\n"));
        ExFreePool(filterExt->hidPreparsedData);
        return STATUS_INSUFFICIENT_RESOURCES;
    }

    RtlZeroMemory(filterExt->pUsageMem, cbUsageMem);
    filterExt->pCurUsage = filterExt->pUsageMem;
    filterExt->pPrevUsage = filterExt->pUsageMem + filterExt->nMaxUsagesLength;
    filterExt->pUsageBreak = filterExt->pUsageMem + filterExt->nMaxUsagesLength * 2;
    filterExt->pUsageMake = filterExt->pUsageMem + filterExt->nMaxUsagesLength * 3;

	return status;
}

NTSTATUS EvtReleaseHardware(WDFDEVICE hDevice, WDFCMRESLIST ResourcesTranslated)
{
    UNREFERENCED_PARAMETER(hDevice);
    UNREFERENCED_PARAMETER(ResourcesTranslated);

    PAGED_CODE();

    NTSTATUS status = STATUS_SUCCESS;
    PDEVICE_EXTENSION filterExt = NULL;

    DebugPrint(("[TBW] EvtReleaseHardware\n"));

    filterExt = FilterGetData(hDevice);

    if (filterExt->pUsageMem)
    {
        DebugPrint(("[TBW] EvtReleaseHardware - relesing combined USAGE processing buffer.\n"));
        ExFreePool(filterExt->pUsageMem);
        filterExt->pUsageMem = NULL;
    }

    if (filterExt->hidPreparsedData)
    {
        DebugPrint(("[TBW] EvtReleaseHardware - releasing HID preparsedData.\n"));
        ExFreePool(filterExt->hidPreparsedData);
        filterExt->hidPreparsedData = NULL;
    }

#ifndef USE_TBLIB
    ConfigDeinit(hDevice, &filterExt->configData);
#endif

    return status;
}

NTSTATUS EvtQueryRemove(WDFDEVICE Device)
{
	PDEVICE_EXTENSION filterExt = FilterGetData(Device);

	DebugPrint(("[TBW] EvtQueryRemove\n"));

	PdoClose(filterExt->pdohDevice);

	return STATUS_SUCCESS;
}

VOID EvtFileCreate(IN WDFDEVICE Device, IN WDFREQUEST Request, IN WDFFILEOBJECT FileObject)
{
	PDEVICE_EXTENSION filterExt;
	WDF_REQUEST_SEND_OPTIONS requestSendOptions;

    UNREFERENCED_PARAMETER(FileObject);

	filterExt = FilterGetData(Device);

	// Check for opening mode
	if(WdfRequestGetRequestorMode(Request) == UserMode)
	{
		DebugPrint(("[TBW] attempt of open from usermode rejected\n"));

		WdfRequestComplete(Request, STATUS_ACCESS_DENIED);
		return;
	}

	// Fail open attempts upon deletion
	if(filterExt->deviceRemoved)
	{
		DebugPrint(("[TBW] received open upon deletion\n"));

		WdfRequestComplete(Request, STATUS_DELETE_PENDING);
		return;
	}

	WDF_REQUEST_SEND_OPTIONS_INIT(&requestSendOptions, WDF_REQUEST_SEND_OPTION_SYNCHRONOUS);
	WdfRequestFormatRequestUsingCurrentType(Request);
	WdfRequestSend(Request, WdfDeviceGetIoTarget(Device), &requestSendOptions);

	{
		WDFIOTARGET ioTarget;
		DEVICE_OBJECT *nextDevice;
		WDF_IO_TARGET_OPEN_PARAMS ioTargetOpenParams;

		if(filterExt->ReadIoTarget == NULL)
		{
			filterExt->ReadIoTarget = WdfDeviceGetIoTarget(Device);

			// Create and open a new remote ioTarget based on the same FILE_OBJECT
			if(!NT_SUCCESS(WdfIoTargetCreate(Device, WDF_NO_OBJECT_ATTRIBUTES, &ioTarget)))
			{
				DebugPrint(("[TBW] IoTargetCreate failed.\n"));
			}
			else
			{
				nextDevice = WdfDeviceWdmGetAttachedDevice(Device);
				WDF_IO_TARGET_OPEN_PARAMS_INIT_EXISTING_DEVICE(&ioTargetOpenParams, nextDevice);
				filterExt->FileObject = IoGetCurrentIrpStackLocation(WdfRequestWdmGetIrp(Request))->FileObject;
				ioTargetOpenParams.TargetFileObject = filterExt->FileObject;

				DebugPrint(("[TBW] Opening file object: 0x%p, name: %wZ\n", filterExt->FileObject,
                    &filterExt->FileObject->FileName));

				if(!NT_SUCCESS(WdfIoTargetOpen(ioTarget, &ioTargetOpenParams)))
				{
					DebugPrint(("[TBW] IoTargetOpen failed.\n"));
				}
				else
				{
                    UNICODE_STRING devNameDefault = { 0 }, devNameRemote = { 0 };
                    GetWdfIoTargetDeviceObjectName(filterExt->ReadIoTarget, &devNameDefault);
                    GetWdfIoTargetDeviceObjectName(ioTarget, &devNameRemote);
                    DebugPrint(("[TBW] EvtFileCreate - default IoTarget: %wZ, remote IoTarget: %wZ\n",
                        &devNameDefault, &devNameRemote));

                    filterExt->ReadIoTarget = ioTarget;

					if(!NT_SUCCESS(StartHidReader(Device, filterExt->uReportMaxSize)))
					{
						DebugPrint(("[TBW] StartHidReader failed.\n"));
					}
				}
			}
		}
	}

	WdfRequestComplete(Request, STATUS_SUCCESS);

	DebugPrint(("[TBW] TestEvtFileCreate req:%x, dev:%x, fo:%x\n",Request, Device, FileObject));
}

VOID EvtFileClose(IN WDFFILEOBJECT FileObject)
{
	WDFDEVICE			device;
	PDEVICE_EXTENSION	filterExt;
	PFILE_OBJECT		fileObj = WdfFileObjectWdmGetFileObject(FileObject);

	device = WdfFileObjectGetDevice(FileObject);
	filterExt = FilterGetData(device);

	DebugPrint(("[TBW] EvtFileClose (fo = %x)\n", FileObject));

	if(fileObj == filterExt->FileObject)
	{
		DebugPrint(("[TBW] Deleting IoTarget\n"));
		if(filterExt->ReadIoTarget)	WdfObjectDelete(filterExt->ReadIoTarget);
		filterExt->ReadIoTarget = NULL;
		filterExt->FileObject = NULL;
	}
}

VOID EvtTimerInertialScroll(IN WDFTIMER timer)
{
    WDFDEVICE hDevice = WdfTimerGetParentObject(timer);

    PDEVICE_EXTENSION pDevExt = FilterGetData(hDevice);

#ifdef USE_TBLIB
	TBHandleTimeOut(&pDevExt->tblibConfig, TIMER_ID_INERTIAL_SCROLL);
#else
	XSTATE_SAVE SaveState;
	NTSTATUS status;
	SCROLL_WHEEL_CONFIG* pScrollConfig = &(pDevExt->configData.scrollParamsVert);

    status = KeSaveExtendedProcessorState(XSTATE_MASK_LEGACY, &SaveState);
    if (!NT_SUCCESS(status)) {
        DebugPrint(("[TBW] EvtTimerInertialScroll - error saving processor state, status: 0x%x\n", status));
        WdfTimerStop(timer, FALSE);
        return;
    }

    WdfSpinLockAcquire(pDevExt->HandleEventLock);

    __try {

        // compute velocity
        if (pScrollConfig->fVelocity == 0)
        {
            LARGE_INTEGER now;
            // This was now.QuadPart, which works on x64, but does not on x86
            KeQueryTickCount(&now.u);

            // absolute time elapsed in 100 nanoseconds
            LARGE_INTEGER elapsed;
            elapsed.QuadPart = (now.QuadPart - pScrollConfig->timeStart.QuadPart) * KeQueryTimeIncrement();
			DebugPrint(("[TBW] inertial scroll timer - calculating initial velocity, elapsed: %llu\n", elapsed.QuadPart));

            // Compute velocity as wheel units per second
            pScrollConfig->fVelocity = (float)pScrollConfig->lTotalDistance / ((float)(elapsed.QuadPart) / (10 * 1000 * 1000));

            // Reset timeStart & lTotalDistance to 0 so that if a mouse wheel
            // movement comes in while inertial wheel timer is active, velocity &
            // totalDistance will be reset from ApplyCustomScrollConfig().
            // These attributes are used only to compute the scroll velocity
            // and once that's done, they are no longer necessary.
            pScrollConfig->timeStart.QuadPart = 0;
            pScrollConfig->lTotalDistance = 0;
        }
        else
        {
            // decay velocity using the attrition factor
            pScrollConfig->fVelocity *= pScrollConfig->fAttritionFactor;
            DebugPrint(("[TBW] inertial scroll timer - calculating attritional velocity\n"));
        }

        int decimal = (int)pScrollConfig->fVelocity;
        int fraction = (int) (pScrollConfig->fVelocity * 100) - decimal * 100;
        DebugPrint(("[TBW] \tvelocity: %d.%d\n", decimal, MOD(fraction)));

        if (MOD(pScrollConfig->fVelocity) < 1)
        {
            DebugPrint(("[TBW] stopping inertial scroll timer\n"));
            WdfTimerStop(pDevExt->timerInertialScroll, FALSE);
            pScrollConfig->fVelocity = 0;
        }
        else
        {
            PostWheelEvent(pDevExt, (LONG)pScrollConfig->fVelocity);
        }
    }
    __finally {
        WdfSpinLockRelease(pDevExt->HandleEventLock);
        KeRestoreExtendedProcessorState(&SaveState);
    }
#endif
}

#ifdef SCROLL_WITH_TRACKBALL
VOID EvtTimerAutoScroll(IN WDFTIMER timer)
{
	WDFDEVICE hDevice = WdfTimerGetParentObject(timer);

	PDEVICE_EXTENSION pDevExt = FilterGetData(hDevice);

	TBHandleTimeOut(&pDevExt->tblibConfig, TIMER_ID_AUTO_SCROLL);
}

#endif

#ifdef ENABLE_CHORDING_TIMER
VOID EvtTimerDetectChord(IN WDFTIMER Timer)
{
    WDFDEVICE hDevice = NULL;
	PDEVICE_EXTENSION pDevExt = NULL;

    hDevice = WdfTimerGetParentObject(Timer);
	pDevExt = FilterGetData(hDevice);

    DebugPrint(("[TBW] EvtChordTimer\n"));

    WdfSpinLockAcquire(pDevExt->HandleEventLock);
#ifdef USE_TBLIB
	TBHandleTimeOut(&pDevExt->tblibConfig, TIMER_ID_CHORD);
#else
    CONFIGDATA* pConfigData = &pDevExt->configData;
    pDevExt->bChordTimerStarted = FALSE;

    IsHelperConected(pDevExt->pdohDevice);

    if (IsHelperConected(pDevExt->pdohDevice))
    {
        UINT32 cBitsInChordMask = numberOfSetBits(pConfigData->dwCurChordEventsMask);
        if (cBitsInChordMask > 1)
        {
            DebugPrint(("[TBW] EvtChordTimer - chord action (0x%02x) detected sending to user module\n",
                pConfigData->dwCurChordEventsMask));

            TBWKERNDATA kd = { 0 };
            kd.buttonDownMask = pConfigData->dwCurChordEventsMask;
            kd.status |= TBWKERNDATA_STATUS_BUTTONACTIVITY;
            PdoEnqueueEventAndService(pDevExt->pdohDevice, &kd);

            kd.buttonDownMask = 0;
            kd.buttonUpMask = pConfigData->dwCurChordEventsMask;
            PdoEnqueueEventAndService(pDevExt->pdohDevice, &kd);

            pDevExt->uButtonUpEventsToSuppress = pConfigData->dwCurChordEventsMask;
        }
        else if (cBitsInChordMask == 1)
        {
            DebugPrint(("[TBW] EvtChordTimer - single button cached chord action (0x%02x) needs handling\n",
                pConfigData->dwCurChordEventsMask));
            PostCachedChordDetectEvents(pDevExt);
        }
    }

    // Reset chording cache attributes
    pConfigData->dwCurChordEventsMask = 0;
    pConfigData->nChordButtonEvents = 0;
#endif
    WdfSpinLockRelease(pDevExt->HandleEventLock);
}
#endif

VOID EvtTimerButtonDebounce(IN WDFTIMER Timer)
{
	WDFDEVICE hDevice = NULL;
	PDEVICE_EXTENSION pDevExt = NULL;

	hDevice = WdfTimerGetParentObject(Timer);
	pDevExt = FilterGetData(hDevice);

	DebugPrint(("[TBW] EvtTimerButtonDebounce\n"));

	WdfSpinLockAcquire(pDevExt->HandleEventLock);
#ifdef USE_TBLIB
	TBHandleTimeOut(&pDevExt->tblibConfig, TIMER_ID_DEBOUNCE);
#else
	CONFIGDATA* pConfigData = &pDevExt->configData;
	pDevExt->bChordTimerStarted = FALSE;

	IsHelperConected(pDevExt->pdohDevice);

	if (IsHelperConected(pDevExt->pdohDevice))
	{
		UINT32 cBitsInChordMask = numberOfSetBits(pConfigData->dwCurChordEventsMask);
		if (cBitsInChordMask > 1)
		{
			DebugPrint(("[TBW] EvtChordTimer - chord action (0x%02x) detected sending to user module\n",
				pConfigData->dwCurChordEventsMask));

			TBWKERNDATA kd = { 0 };
			kd.buttonDownMask = pConfigData->dwCurChordEventsMask;
			kd.status |= TBWKERNDATA_STATUS_BUTTONACTIVITY;
			PdoEnqueueEventAndService(pDevExt->pdohDevice, &kd);

			kd.buttonDownMask = 0;
			kd.buttonUpMask = pConfigData->dwCurChordEventsMask;
			PdoEnqueueEventAndService(pDevExt->pdohDevice, &kd);

			pDevExt->uButtonUpEventsToSuppress = pConfigData->dwCurChordEventsMask;
		}
		else if (cBitsInChordMask == 1)
		{
			DebugPrint(("[TBW] EvtChordTimer - single button cached chord action (0x%02x) needs handling\n",
				pConfigData->dwCurChordEventsMask));
			PostCachedChordDetectEvents(pDevExt);
		}
	}

	// Reset chording cache attributes
	pConfigData->dwCurChordEventsMask = 0;
	pConfigData->nChordButtonEvents = 0;
#endif
	WdfSpinLockRelease(pDevExt->HandleEventLock);
}

VOID EvtTimerTiltButton(IN WDFTIMER Timer)
{
	WDFDEVICE hDevice = NULL;
	PDEVICE_EXTENSION pDevExt = NULL;

	hDevice = WdfTimerGetParentObject(Timer);
	pDevExt = FilterGetData(hDevice);

	DebugPrint(("[TBW] EvtTimerTiltButton\n"));

	WdfSpinLockAcquire(pDevExt->HandleEventLock);
#ifdef USE_TBLIB
	TBHandleTimeOut(&pDevExt->tblibConfig, TIMER_ID_TILT_BUTTON);
#endif
	WdfSpinLockRelease(pDevExt->HandleEventLock);
}

VOID EvtIoInternalDeviceControl(
								IN WDFQUEUE      Queue,
								IN WDFREQUEST    Request,
								IN size_t        OutputBufferLength,
								IN size_t        InputBufferLength,
								IN ULONG         IoControlCode
								)
{
	PDEVICE_EXTENSION           filterExt;
    PCONNECT_DATA               connectData;
    NTSTATUS					status = STATUS_SUCCESS;
	ULONG_PTR					info = 0;
    WDFDEVICE					hDevice;
    size_t                      length;

    UNREFERENCED_PARAMETER(OutputBufferLength);
    UNREFERENCED_PARAMETER(InputBufferLength);

    PAGED_CODE();

    hDevice = WdfIoQueueGetDevice(Queue);
    filterExt = FilterGetData(hDevice);

	DebugPrint(("[TBW] Internal IOCTL code %x\n", IoControlCode));

    switch (IoControlCode)
	{
	    case IOCTL_INTERNAL_MOUSE_CONNECT:

			DebugPrint(("[TBW] IOCTL_INTERNAL_MOUSE_CONNECT\n"));

			if (filterExt->UpperConnectData.ClassService != NULL)
			{
				status = STATUS_SHARING_VIOLATION;
				break;
			}

			status = WdfRequestRetrieveInputBuffer(Request, sizeof(CONNECT_DATA), &connectData, &length);
			if(!NT_SUCCESS(status)){
				DebugPrint(("[TBW] WdfRequestRetrieveInputBuffer failed %x\n", status));
				break;
			}

			filterExt->UpperConnectData = *connectData;

			DebugPrint(("[TBW] ConnectData ClassDeviceObject %x, ClassService %x\n",
													filterExt->UpperConnectData.ClassDeviceObject,
													filterExt->UpperConnectData.ClassService));

			//
			// Hook into the report chain.  Everytime a mouse packet is reported to
			// the system, MouFilter_ServiceCallback will be called

			break;

		case IOCTL_INTERNAL_MOUSE_DISCONNECT:
			DebugPrint(("[TBW] IOCTL_INTERNAL_MOUSE_DISCONNECT\n"));
			status = STATUS_NOT_IMPLEMENTED;
			break;

		case IOCTL_INTERNAL_MOUSE_ENABLE:
			DebugPrint(("[TBW] IOCTL_INTERNAL_MOUSE_ENABLE\n"));
			status = STATUS_NOT_SUPPORTED;
			break;

		case IOCTL_INTERNAL_MOUSE_DISABLE:
			DebugPrint(("[TBW] IOCTL_INTERNAL_MOUSE_ENABLE\n"));
	        status = STATUS_NOT_SUPPORTED;
	        break;

		case IOCTL_MOUSE_QUERY_ATTRIBUTES:
			DebugPrint(("[TBW] IOCTL_MOUSE_QUERY_ATTRIBUTES\n"));
            {
				MOUSE_ATTRIBUTES *buff = NULL;
				NTSTATUS s;

                s = WdfRequestRetrieveOutputBuffer(Request, sizeof(MOUSE_ATTRIBUTES), &buff, NULL);
				if(NT_SUCCESS(s))
				{
                    buff->MouseIdentifier = WHEELMOUSE_HID_HARDWARE;
                    buff->NumberOfButtons = 32;
                    buff->InputDataQueueLength = 32;
					info = sizeof(MOUSE_ATTRIBUTES);
				}
				status = s;
			}
            break;

		default:
			break;
	}

	WdfRequestCompleteWithInformation(Request, status, info);
}

#pragma warning(pop)

NTSTATUS ReadRequestFormatAndSend(WDFDEVICE Device, NEWREQUEST Request)
{
	NTSTATUS status = STATUS_SUCCESS;
	PDEVICE_EXTENSION filterExt = FilterGetData(Device);
	REQUEST_EXTENSION *reqContext = RequestGetData(Request);
	UCHAR *buff = NULL;
	size_t buffSize = 0L;

	if(filterExt->ReadIoTarget)
	{
		buff = WdfMemoryGetBuffer(reqContext->Memory, &buffSize);
		if(buff) RtlZeroMemory(buff, buffSize);

		status = WdfIoTargetFormatRequestForRead(filterExt->ReadIoTarget, Request, reqContext->Memory, NULL, NULL);
		if(NT_SUCCESS(status))
		{
			WdfRequestSetCompletionRoutine(Request, EvtIoReadCompletion, filterExt);

			if(WdfRequestSend(Request, filterExt->ReadIoTarget, WDF_NO_SEND_OPTIONS) == FALSE)
			{
				KdBreakPoint();
				status = WdfRequestGetStatus(Request);
				DebugPrint(("[TBW] ReadRequestFormatAndSend failed with status %x", status));
			}
		}
	}
	else
	{
		status = STATUS_INTERNAL_ERROR;
	}

	return status;
}

NTSTATUS StartHidReader(WDFDEVICE Device, ULONG Length)
{
	PDEVICE_EXTENSION		filterExt;
	WDFMEMORY				memory = NULL;
	NTSTATUS				status = STATUS_SUCCESS;
	WDFREQUEST				request = (WDFREQUEST) 0xff;
	WDF_OBJECT_ATTRIBUTES	attributes;
	REQUEST_EXTENSION		*reqContext = NULL;

	DebugPrint(("[TBW] Starting HID reader (WdfDevice %x, Report Length %x)\n", Device, Length));

	filterExt = FilterGetData(Device);

	if(Length > 0)
	{
		if(filterExt->ReadIoTarget)
		{
			//WdfVerifierDbgBreakPoint();

			WDF_OBJECT_ATTRIBUTES_INIT_CONTEXT_TYPE(&attributes, REQUEST_EXTENSION);
			status = WdfRequestCreate(&attributes, filterExt->ReadIoTarget, &request);

			if(NT_SUCCESS(status))
			{
				DebugPrint(("[TBW] WdfRequestCreate succeeded. req:%x\n", request));

				WDF_OBJECT_ATTRIBUTES_INIT(&attributes);
				attributes.ParentObject = request;
				status = WdfMemoryCreate(&attributes, AppropriatePoolForOS, TBW_POOL_TAG, Length, &memory, NULL);
			}
			else
			{
				DebugPrint(("[TBW] WdfCreateRequest for ReadIoTarget %x failed with code %x\n", filterExt->ReadIoTarget, status));
			}

			if(NT_SUCCESS(status))
			{
				reqContext = RequestGetData(request);
				reqContext->Memory = memory;

				DebugPrint(("[TBW] ReadRequestCreate succeded. Request %x Memobj %x\n", request, memory));
			}
			else DebugPrint(("[TBW] WdfMemoryCreate of %d bytes failed with code %x\n", Length, status));
		}
	}
	else DebugPrint(("[TBW] Requested HidReader with 0 length buffer. Failing.\n"));

	if(!NT_SUCCESS(status)) return status;

	return ReadRequestFormatAndSend(Device, request);
}

NTSTATUS RepostHidReader(WDFDEVICE Device, WDFREQUEST Request)
{
	NTSTATUS status;
	WDF_REQUEST_REUSE_PARAMS params;

	WDF_REQUEST_REUSE_PARAMS_INIT(&params, WDF_REQUEST_REUSE_NO_FLAGS, STATUS_SUCCESS);

	status = WdfRequestReuse(Request, &params);

	if(!NT_SUCCESS(status)) return status;

	return ReadRequestFormatAndSend(Device, Request);
}

VOID EvtIoReadCompletion(IN WDFREQUEST Request, IN WDFIOTARGET Target, PWDF_REQUEST_COMPLETION_PARAMS CompletionParams, IN WDFCONTEXT Context)
{
    UNREFERENCED_PARAMETER(Target);

	PDEVICE_EXTENSION filterExt = (PDEVICE_EXTENSION) Context;
	size_t size;
	ULONG_PTR	reportLength;
	REQUEST_EXTENSION *reqContext;
    //DebugPrint(("[TBW] EvtIoReadCompletion - HID report len: %d\n", CompletionParams->IoStatus.Information));

	reqContext = RequestGetData(Request);

	if(CompletionParams->IoStatus.Status == STATUS_SUCCESS)
	{
		UCHAR *report;

		reportLength = CompletionParams->IoStatus.Information;

        report = WdfMemoryGetBuffer(reqContext->Memory, &size);
        if(report)
		{
            if (filterExt->hidPreparsedData && filterExt->pUsageMem)
            {
                HIDReportHandleButtonEvents(filterExt, (PCHAR)report, (ULONG)reportLength);
                HIDReportHandlePointerEvents(filterExt, (PCHAR)report, (ULONG)reportLength);
                HIDReportHandleWheelEvents(filterExt, (PCHAR)report, (ULONG)reportLength);
				if (filterExt->bHasACPanButtons)
				{
					HIDReportHandlePanEvents(filterExt, (PCHAR)report, (ULONG)reportLength);
				}
                
            }

			if(!NT_SUCCESS(RepostHidReader(filterExt->WdfDevice, Request)))
			{
				DebugPrint(("[TBW] RepostHidReader failed.\n"));
			}

		}
	}
	else
	{
		DebugPrint(("[TBW] EvtIoReadCompletion failed with status %x\n", CompletionParams->IoStatus.Status));

		// Here is important to do something.
		// If the status is disconnected: set a disconnected flag and fail all the reads that are parked.
		// If something else, substitute with a zero report and go as usual. This way we make sure that
		// if something happens (read error) the state of the mouse is not held indefinitely.
	}
}

void HIDReportHandleButtonEvents(PDEVICE_EXTENSION pDevExt, PCHAR pReport, ULONG ulReportLength)
{
    UINT32 uButtonDownMask = 0,
        uButtonUpMask = 0;
    static const USHORT SLIMBLADE_PRODUCT_ID = 0x2041;
    static const ULONG SLIMBLADE_REPORT_LENGTH = 6;

    static const USHORT VERTICALTB_USB_PRODUCT_ID = 0x8068;
    static const USHORT VERTICALTB_BT_PRODUCT_ID = 0x8069;
    static const ULONG VERTICALTB_REPORT_LENGTH = 7;

	//&*&*&*G1_ADD, filtering non-button reports
	ULONG nCurUsageLength = pDevExt->nMaxUsagesLength;
	NTSTATUS status = HidP_GetUsages(
		HidP_Input,
		HID_USAGE_PAGE_BUTTON,
		0,
		pDevExt->pCurUsage,
		&nCurUsageLength,
		pDevExt->hidPreparsedData,
		pReport,
		ulReportLength);
	if (status != HIDP_STATUS_SUCCESS)
	{
		//DebugPrint(("[TBW] Not button event\n"));
		return;
	}
	// Check for redundant empty button event HID report, as that is
	// generated by Wireless Trackball. If so, ignore it.
	//if (nCurUsageLength == 0 && pDevExt->nPrevUsageLength == 0)
	//{
	//	DebugPrint(("[TBW] Button event usageCount=0 & prevUsageCount = 0\n"));
	//	return;
	//}

	//&*&*&*G2_ADD

    // SlimBlade HID Report Descriptor does not define buttons 3 & 4. Therefore
    // standard HID parser below cannot decipher its state changes. So we need
    // special handling for it.
    if (pDevExt->usProductId == SLIMBLADE_PRODUCT_ID
        && ulReportLength == SLIMBLADE_REPORT_LENGTH)
    {
        GetButtonStateSlimBlade(pDevExt, (PUCHAR)pReport, &uButtonDownMask, &uButtonUpMask);
        if (uButtonDownMask == 0 && uButtonUpMask == 0)
        {
            //DebugPrint(("[TBW] HandleButtonEvents(Slimblade) - no buttons set\n"));
            return;
        }
    }
	DebugPrint(("[TBW] ====="));
	DebugPrint(("[TBW] HIDReportHandleButtonEvents: report[0~4] = 0x%02x - 0x%02x, 0x%02x, 0x%02x, 0x%02x", pReport[0], pReport[1], pReport[2], pReport[3], pReport[4]));
	if (pDevExt->tblibConfig.fHelperConnected)
	{
		//&*&*&*G1_ADD, filtering redundant reports
		if ((uint32_t)pReport[1] == pDevExt->tblibConfig.curPhysButtonState)
		{
			DebugPrint(("[TBW] redundant report"));
			return;
		}
		//&*&*&*G2_ADD	
		TBProcessButtonEvent(&pDevExt->tblibConfig, pReport[1]);
	}
	else
	{
		TBLOG(("[TBW] Helper isn't connected.\n"));
		uint32_t buttonState = pReport[1];
		TBLIBCONFIG* pConfig = &pDevExt->tblibConfig;
		if ((buttonState == pConfig->lastPhysButtonState))
		{
			TBLOG(("[TBW] HIDReportHandleButtonEvents - buttonState == pConfig->lastPhysButtonState"));
			return;
		}

		TBLOG(("[TBW] HIDReportHandleButtonEvents - BS: 0x%04x  last: 0x%04x\n", buttonState, pConfig->lastPhysButtonState));
		uint32_t uDownMask = (pConfig->lastPhysButtonState ^ buttonState) & buttonState;
		uint32_t uUpMask = (pConfig->lastPhysButtonState ^ buttonState) & pConfig->lastPhysButtonState;

		pConfig->lastPhysButtonState = buttonState;

		//TBLOG(("[TBW] Posting button event to HID stack\n"));
		PostButtonEvent(
			pDevExt,
			uDownMask,
			uUpMask);

	}
#if 0

    /*
    else if ((pDevExt->usProductId == VERTICALTB_USB_PRODUCT_ID
            || pDevExt->usProductId == VERTICALTB_BT_PRODUCT_ID)
        && ulReportLength == VERTICALTB_REPORT_LENGTH)
    {
        GetButtonStateVerticalTrackball(pDevExt, (PUCHAR)pReport, &uButtonDownMask, &uButtonUpMask);
        if (uButtonDownMask == 0 && uButtonUpMask == 0)
        {
            //DebugPrint(("[TBW] HandleButtonEvents(Slimblade) - no buttons set\n"));
            return;
        }
    }
    */
    else
    {
        // Standard HID parser.

        ULONG nCurUsageLength = pDevExt->nMaxUsagesLength;
        NTSTATUS status = HidP_GetUsages(
            HidP_Input,
            HID_USAGE_PAGE_BUTTON,
            0,
            pDevExt->pCurUsage,
            &nCurUsageLength,
            pDevExt->hidPreparsedData,
            pReport,
            ulReportLength);
        if (status != HIDP_STATUS_SUCCESS)
        {
            //DebugPrint(("[TBW] Not button event\n"));
            return;
        }

        // Check for redundant empty button event HID report, as that is
        // generated by Wireless Trackball. If so, ignore it.
        if (nCurUsageLength == 0 && pDevExt->nPrevUsageLength == 0)
        {
            //DebugPrint(("[TBW] Button event usageCount=0 & prevUsageCount = 0\n"));
            return;
        }

        //DebugPrint(("[TBW] button event, curr-usage-count: %d, prev-usage-count: %d\n", ulCurUsageLength,  pDevExt->ulPrevUsageLength));

        RtlZeroMemory(pDevExt->pUsageBreak, sizeof(USAGE)*pDevExt->nMaxUsagesLength);
        RtlZeroMemory(pDevExt->pUsageMake, sizeof(USAGE)*pDevExt->nMaxUsagesLength);
        // find the difference between the two lists
        status = HidP_UsageListDifference(
            pDevExt->pPrevUsage,    // Previous
            pDevExt->pCurUsage,                 // Current
            pDevExt->pUsageBreak,            // Previous - Current
            pDevExt->pUsageMake,             // Current - Previous
            max(nCurUsageLength, pDevExt->nPrevUsageLength));

        if (status != HIDP_STATUS_SUCCESS)
        {
            DebugPrint(("[TBW] HidP_UsageListDifference failed, rc: 0x08x\n", status));
            return;
        }

        // Deal with aUsageMake -- which refers to buttons that have been depressed.
        size_t cDownButtons = 0, cUpButtons = 0;
        for (size_t i = 0; i < pDevExt->nMaxUsagesLength; i++)
        {
            USAGE usage = *(pDevExt->pUsageMake + i);
            if (!usage)
            {
                cDownButtons = i;
                break;
            }
            //DebugPrint(("[TBW] button %d down(i = %d)\n", usage, i));
            //event.buttons|= (0x01 << (usage - 1));
            uButtonDownMask |= (0x01 << (usage - 1));
        }

        // deal with aUsageBreak -- buttons that have been released
        for (size_t i = 0; i < pDevExt->nMaxUsagesLength; i++)
        {
            USAGE usage = *(pDevExt->pUsageBreak + i);
            if (!usage)
            {
                cUpButtons = i;
                break;
            }
            //DebugPrint(("[TBW] button %d up(i = %d)\n", usage, i));
            uButtonUpMask |= (0x01 << (usage - 1));
        }

        // Store the current button HID report parsed results in context so that
        // we can compare this with the next incoming HID report for any
        // differences.
        RtlCopyMemory(pDevExt->pPrevUsage, pDevExt->pCurUsage, sizeof(USAGE)*pDevExt->nMaxUsagesLength);
        pDevExt->nPrevUsageLength = nCurUsageLength;

        if (cDownButtons == 0 && cUpButtons == 0)
        {
            DebugPrint(("[TBW] HandleButtonEvents - no buttons set\n"));
            // We used to store this even when cDownButtons & cUpButtons were zero.
            // Not sure if it has any effect.
            //RtlCopyMemory(pDevExt->pPrevUsage, pDevExt->pCurUsage, sizeof(USAGE)*pDevExt->nMaxUsagesLength);
            //pDevExt->nPrevUsageLength = nCurUsageLength;
            return;
        }
    }
#ifndef USE_TBLIB
    DebugPrint(("[TBW] Buttons(1) DOWN: 0x%04x, UP: 0x%04x\n", uButtonDownMask, uButtonUpMask));
    if (IsHelperConected(pDevExt->pdohDevice))
    {
        // DetectChords will take out the bits that are used to detect chording
        // evets from the button mask second and third argument.
        DetectChords(pDevExt, &uButtonDownMask, &uButtonUpMask);
    }
    DebugPrint(("[TBW] Buttons(2) DOWN: 0x%04x, UP: 0x%04x\n", uButtonDownMask, uButtonUpMask));

    if (uButtonDownMask || uButtonUpMask)
    {
        UINT32 maskOut = 0;
        if (IsUserModuleButtonEvent(
            pDevExt,
            uButtonDownMask ? uButtonDownMask : uButtonUpMask,
            &maskOut))
        {
            DebugPrint(("[TBW] Posting button event to user module\n"));
            TBWKERNDATA kd = { 0 };
            kd.buttonDownMask = uButtonDownMask;
            kd.buttonUpMask = uButtonUpMask;
            kd.status = TBWKERNDATA_STATUS_BUTTONACTIVITY;
            PdoEnqueueEventAndService(pDevExt->pdohDevice, &kd);
        }
        else
        {
            DebugPrint(("[TBW] Posting button event to HID stack\n"));
            PostButtonEvent(
                pDevExt,
                uButtonDownMask ? maskOut : 0,
                uButtonUpMask ? maskOut : 0);
        }
    }

    // Store the current button HID report parsed results in context so that
    // we can compare this with the next incoming HID report for any
    // differences.
    //RtlCopyMemory(pDevExt->pPrevUsage, pDevExt->pCurUsage, sizeof(USAGE)*pDevExt->nMaxUsagesLength);
    //pDevExt->nPrevUsageLength = nCurUsageLength;
#else
	// tblib

	TBProcessButtonSplitEvent(&pDevExt->tblibConfig, uButtonDownMask, uButtonUpMask);

#endif
#endif
}

void HIDReportHandlePointerEvents(PDEVICE_EXTENSION pDevExt, PCHAR pReport, ULONG ulReportLength)
{
    LONG dX = 0, dY = 0;
    if (HidP_GetScaledUsageValue(
        HidP_Input,
        HID_USAGE_PAGE_GENERIC,
        0,
        HID_USAGE_GENERIC_X,
        &dX,
        pDevExt->hidPreparsedData,
        pReport,
        ulReportLength) != HIDP_STATUS_SUCCESS ||
        HidP_GetScaledUsageValue(
            HidP_Input,
            HID_USAGE_PAGE_GENERIC,
            0,
            HID_USAGE_GENERIC_Y,
            &dY,
            pDevExt->hidPreparsedData,
            pReport,
            ulReportLength) != HIDP_STATUS_SUCCESS)
    {
        //DebugPrint(("[TBW] Not pointer event\n"));
        return;
    }

    // Only handle valid mouse movement events. That is pointer movement
    // events with either dX or dY having non-zero values.
    if (dX || dY)
    {
#ifndef USE_TBLIB
        ApplySlowPointer(pDevExt, &dX, &dY);
        ApplySingleAxisMovement(pDevExt, &dX, &dY);
        PostPointerEvent(pDevExt, dX, dY);
#else
		TBProcessPointerEvent(&pDevExt->tblibConfig, dX, dY);
#endif
    }
}

void HIDReportHandleWheelEvents(PDEVICE_EXTENSION pDevExt, PCHAR pReport, ULONG ulReportLength)
{
    LONG lWheel = 0;
    if (HidP_GetScaledUsageValue(
        HidP_Input,
        HID_USAGE_PAGE_GENERIC,
        0,
        HID_USAGE_GENERIC_WHEEL,
        &lWheel,
        pDevExt->hidPreparsedData,
        pReport,
        ulReportLength) != HIDP_STATUS_SUCCESS)
    {
        //DebugPrint(("[TBW] Not wheel event\n"));
        return;
    }

    // Only handle non-zero wheel events. Expert Wireless Trackball sends
    // empty wheel events with every button press. This need not be
    // propagated up the device stack.
    if (lWheel != 0)
    {
        WdfSpinLockAcquire(pDevExt->HandleEventLock);
        DebugPrint(("[TBW] Wheel event: %d\n", lWheel));
#ifndef USE_TBLIB
        // TODO: determine correct scroll wheel, vert or horz
        SCROLL_WHEEL_CONFIG* pScrollConfig = &(pDevExt->configData.scrollParamsVert);

        // Invert scroll wheel if configured
        if (pScrollConfig->bInvert)
            lWheel *= -1;

        // essentially applies wheel move multiplication factor, aggregrates
        // total distance moved & kicks off the inertial scroll timer.
        ApplyCustomScrollConfig(pDevExt, &pDevExt->configData.scrollParamsVert, &lWheel);
        PostWheelEvent(pDevExt, lWheel);
#else
		TBProcessWheelEvent(&pDevExt->tblibConfig, TBLIB_SCROLL_WHEEL_VERT, (int)lWheel);
#endif
        WdfSpinLockRelease(pDevExt->HandleEventLock);
    }
}

void HIDReportHandlePanEvents(PDEVICE_EXTENSION pDevExt, PCHAR pReport, ULONG ulReportLength)
{
	LONG lPan = 0;
	if (HidP_GetScaledUsageValue(
		HidP_Input,
		HID_USAGE_PAGE_CONSUMER,
		0,
		HID_USAGE_CONSUMER_AC_PAN,
		&lPan,
		pDevExt->hidPreparsedData,
		pReport,
		ulReportLength) != HIDP_STATUS_SUCCESS)
	{
		//DebugPrint(("[TBW] Not pan event\n"));
		return;
	}

	
	DebugPrint(("[TBW] AC Pan event: %ld\n", lPan));
	if (lPan != 0)
	{
		if (pDevExt->tblibConfig.fHelperConnected)
		{
			TBProcessACPanEvent(&pDevExt->tblibConfig, lPan);
		}
		else
		{
			uint32_t panButtonState = lPan < 0 ? PAN_LEFT_BUTTON_MASK :
				(lPan > 0 ? PAN_RIGHT_BUTTON_MASK : 0);

			uint32_t buttonState = (pDevExt->tblibConfig.lastPhysButtonState & ~PAN_BUTTON_STATE_MASK) | panButtonState;

			pDevExt->tblibConfig.lastPhysButtonState = buttonState;
			PostWheelEvent(
				pDevExt,
				MOUSE_HWHEEL,
				lPan);

		}

	}
	
    
}

#ifndef USE_TBLIB
// Applies slow pointer logic, if it's enabled, reducing the dx & dy values by
// an appropriate factor.
void ApplySlowPointer(PDEVICE_EXTENSION pDevExt, LONG* pDX, LONG* pDY)
{
    if (!pDevExt->configData.bSlowPointer || pDevExt->configData.uSlowdownDivider == 0)
        return;

    // DebugPrint(("[TBW] ApplySlowPointer\n"));

    LONG dx = *pDX, dy = *pDY, slowDownDivider = (LONG)pDevExt->configData.uSlowdownDivider;

    dx += pDevExt->configData.remainderX;
    dy += pDevExt->configData.remainderY;

    pDevExt->configData.remainderX = (SHORT) dx % slowDownDivider;
    pDevExt->configData.remainderY = (SHORT) dy % slowDownDivider;

    dx /= slowDownDivider;
    dy /= slowDownDivider;

    *pDX = dx; *pDY = dy;
}

// Applies single axis movement logic, if it's enabled and changes the supplied
// dx & dy co-ords accordingly.
void ApplySingleAxisMovement(PDEVICE_EXTENSION pDevExt, LONG* pDX, LONG* pDY)
{
    LONG dx = *pDX, dy = *pDY;

	const SHORT DOMINANCE_SWITCH_THRESHOLD = pDevExt->configData.sDominanceSwitchThreshold;

	if (!pDevExt->configData.bLockPointerAxis)
    {
        pDevExt->configData.dominance = 0;
        return;
    }

	//DebugPrint(("[TBW] lock pointer axis(BF), %d, %d, dominance: %d\n", dx, dy, pDevExt->configData.dominance));
	ULONG mdx = MOD(dx);
	ULONG mdy = MOD(dy);

    if (mdx > mdy)
    {
        if (pDevExt->configData.dominance < DOMINANCE_SWITCH_THRESHOLD) pDevExt->configData.dominance++;
    }
    else if (mdy > mdx)
    {
        if (pDevExt->configData.dominance > (-1* DOMINANCE_SWITCH_THRESHOLD)) pDevExt->configData.dominance--;
    }

    //if(((MOD(Event->dx) - MOD(Event->dy)) + pDevExt->configData.dominance) > 0) Event->dy = 0;
    //	else Event->dx = 0;

    if (pDevExt->configData.dominance > 0)
        dy = 0;
    else
        dx = 0;

	//DebugPrint(("[TBW] lock pointer axis(AF), %d, %d, dominance: %d\n", dx, dy, pDevExt->configData.dominance));
	*pDX = dx, *pDY = dy;
}

// Apply scroll customizations such as wheel invert, inertial scroll, etc.
void ApplyCustomScrollConfig(PDEVICE_EXTENSION pDevExt, SCROLL_WHEEL_CONFIG* pScrollConfig, LONG* plWheel)
{
    if (pScrollConfig->bInertialScroll)
    {
        // Check for direction change midway through processing inertial scroll.
        // This relies on the elementary principle that XOR of two values of same
        // data types will be less than zero if they are of opposite signs.
        if (pScrollConfig->lTotalDistance != 0
            && ((*plWheel) ^ pScrollConfig->lTotalDistance) < 0)
        {
            // DebugPrint(("[TBW] scroll direction change!\n"));
            pScrollConfig->lTotalDistance = 0;
        }

        // timestamp the start time
        if (pScrollConfig->timeStart.QuadPart == 0) {
            KeQueryTickCount(&pScrollConfig->timeStart);
            pScrollConfig->fVelocity = 0;
        }

        // accumulate total distance travelled
        pScrollConfig->lTotalDistance += *plWheel;

        DebugPrint(("[TBW] inertial scroll total distance: %d\n", pScrollConfig->lTotalDistance));

        WdfTimerStart(
            pDevExt->timerInertialScroll,
            WDF_REL_TIMEOUT_IN_MS(INERTIAL_SCROLL_TIMER_INTERVAL)
        );
    }
}

void DetectChords(PDEVICE_EXTENSION pDevExt, UINT32* pDownMask, UINT32* pUpMask)
{
    CONFIGDATA* pConfigData = &pDevExt->configData;
    TBWBUTTONSCONFIG* pButtonsCfg = &pConfigData->buttonsConfig;

    UINT32 uButtonDownMask = *pDownMask,
        uButtonUpMask = *pUpMask;

    DebugPrint(("[TBW] DetectChords chordMask: 0x%04x, dwCurChordEventsMask: 0x%04x, DOWN: 0x%04x, UP: 0x%04x\n",
        pButtonsCfg->chordMask,
        pConfigData->dwCurChordEventsMask,
        uButtonDownMask,
        uButtonUpMask));

    if (uButtonDownMask)
    {
        // first check if chord processing is necessary
        if (pButtonsCfg->chordMask
            && (uButtonDownMask & pButtonsCfg->chordMask)
            && pConfigData->nChordButtonEvents < CHORD_BUTTON_EVENTS_CACHE_SIZE)
        {
            // User has defined chording action
            UINT32 chordButtons = uButtonDownMask & pButtonsCfg->chordMask;

            // Accumulate chording buttons to the current mask
            pConfigData->dwCurChordEventsMask |= chordButtons;
            // Clear the chord buttons from the buttons mask.
            uButtonDownMask &= ~chordButtons;

            // Store the button in the array for handling when timer runs out
            BUTTONEVENT be = { 0 };
            be.uDownMask = chordButtons;
            pConfigData->aChordButtonEventsCache[pConfigData->nChordButtonEvents++] =
                be;

            DebugPrint(("[TBW] DetectChords - button DOWN event 0x%04x cached\n", chordButtons));

            // if timer is not started, start it
            if (!pDevExt->bChordTimerStarted)
            {
                DebugPrint(("[TBW] Starting chord timer\n"));
                WdfTimerStart(
                    pDevExt->timerDetectChord,
                    WDF_REL_TIMEOUT_IN_MS(CHORD_DETECT_TIMER_INTERVAL));
                pDevExt->bChordTimerStarted = TRUE;
            }
        }
    }

    if (uButtonUpMask)
    {
        if (uButtonUpMask && pDevExt->uButtonUpEventsToSuppress)
        {
            DebugPrint(("[TBW] DetectChords - suppress mask: 0x%08x\n", pDevExt->uButtonUpEventsToSuppress));
            // Remove any UP events to be suppressed from the UP mask
            UINT32 uUpdatedButtonUpMask = uButtonUpMask & ~pDevExt->uButtonUpEventsToSuppress;
            // Update the suppress mask removing bits that were suppressed
            pDevExt->uButtonUpEventsToSuppress &= ~uButtonUpMask;
            uButtonUpMask = uUpdatedButtonUpMask;
            DebugPrint(("[TBW] DetectChords - suppressed button UP mask: 0x%08x, suppress-mask: 0x%08x\n", uButtonUpMask, pDevExt->uButtonUpEventsToSuppress));
        }

        if (pConfigData->dwCurChordEventsMask & uButtonUpMask
            && pConfigData->nChordButtonEvents < CHORD_BUTTON_EVENTS_CACHE_SIZE)
        {
            UINT32 chordButtons = pConfigData->dwCurChordEventsMask & uButtonUpMask;
            uButtonUpMask &= ~chordButtons;

            // store the event in the cache. Timer routine will process it
            BUTTONEVENT be = { 0 };
            be.uUpMask = chordButtons;
            pConfigData->aChordButtonEventsCache[pConfigData->nChordButtonEvents++] =
                be;
            DebugPrint(("[TBW] DetectChords - button UP event 0x%04x cached\n", chordButtons));
        }
    }

    *pDownMask = uButtonDownMask;
    *pUpMask = uButtonUpMask;
}

void PostCachedChordDetectEvents(PDEVICE_EXTENSION pDeviceExt)
{
    CONFIGDATA* pConfigData = &pDeviceExt->configData;
    DebugPrint(("[TBW] PostChordDetectEvents - %d cached events to be posted\n",
        pConfigData->nChordButtonEvents));
    for (UINT32 i = 0; i < pConfigData->nChordButtonEvents; i++)
    {
        BUTTONEVENT* pBE = &pConfigData->aChordButtonEventsCache[i];
        UINT32 maskOut = 0;
        if (IsUserModuleButtonEvent(
            pDeviceExt,
            pBE->uDownMask ? pBE->uDownMask : pBE->uUpMask,
            &maskOut))
        {
            TBWKERNDATA kd = { 0 };
            kd.buttonDownMask = pBE->uDownMask;
            kd.buttonUpMask = pBE->uUpMask;
            kd.status = TBWKERNDATA_STATUS_BUTTONACTIVITY;
            PdoEnqueueEventAndService(pDeviceExt->pdohDevice, &kd);
        }
        else
        {
            PostButtonEvent(
                pDeviceExt,
                pConfigData->aChordButtonEventsCache[i].uDownMask ? maskOut : 0,
                pConfigData->aChordButtonEventsCache[i].uUpMask ? maskOut : 0
            );
        }
    }
}

BOOLEAN IsUserModuleButtonEvent(PDEVICE_EXTENSION pDevExt, UINT32 maskIn, UINT32* maskOut)
{
    CONFIGDATA* pConfigData = &pDevExt->configData;

    if (!IsHelperConected(pDevExt->pdohDevice)) {
        // If helper is connected, maskOut defaults to maskIn, which will ensure
        // that default button 1~5 (Windows only handles buttons 1~5 by default)
        // events are handled by the Windows input subsystem.
        *maskOut = maskIn;
        return FALSE;
    }

    // cycle through the button config array for primitives
    for (size_t i = 0; i < pConfigData->buttonsConfig.nActions; i++)
    {
        TBWBUTTONACTION* pButton = &(pConfigData->buttonsConfig.actions[i]);
        if (pButton->eventMask == maskIn)
        {
            //DebugPrint(("[TBW] Button event to be pased to HID stack\n"));
            if (maskOut)
            {
                *maskOut = (UINT32) pButton->actionButton;
            }
            return FALSE;
        }
    }
    //DebugPrint(("[TBW] Button event to be passed to user module\n"));
    return TRUE;
}
#endif

// Queues the pointer event with the upper layer driver so that it flows
// up to userspace through the HID subsystem.
void PostPointerEvent(PDEVICE_EXTENSION pDevExt, LONG dx, LONG dy)
{
	MOUSE_INPUT_DATA data = { 0 };
	LONG dataConsumed = 0;
	KIRQL oldIrql = 0;

	if (!pDevExt->UpperConnectData.ClassService)
	{
		DebugPrint(("[TBW] PostPointerEvent - mouclass service callback = NULL\n"));
		return;
	}

	RtlZeroMemory(&data, sizeof(MOUSE_INPUT_DATA));

	data.Flags = MOUSE_MOVE_RELATIVE;
	data.LastX = dx;
	data.LastY = dy;

	//DebugPrint(("[TBW] Pointer event: %d, %d\n", dx, dy));

	KeRaiseIrql(DISPATCH_LEVEL, &oldIrql);
	(*(PSERVICE_CALLBACK_ROUTINE)pDevExt->UpperConnectData.ClassService)(
		pDevExt->UpperConnectData.ClassDeviceObject,
		&data,
		&data + 1,
		&dataConsumed
		);
	KeLowerIrql(oldIrql);
}

// Note that down & up masks are as in our HID report. That is bit is set
// in uDownMask for the respective button's depress event and in uUpMask for the
// button's up event.
void PostButtonEvent(
    PDEVICE_EXTENSION pDevExt,
    UINT32 uDownMask,
    UINT32 uUpMask)
{
    USHORT aMouseButtonDownFlags[] = {
        MOUSE_LEFT_BUTTON_DOWN,
        MOUSE_RIGHT_BUTTON_DOWN,
        MOUSE_MIDDLE_BUTTON_DOWN,
        MOUSE_BUTTON_4_DOWN,
        MOUSE_BUTTON_5_DOWN
    };
    USHORT aMouseButtonUpFlags[] = {
        MOUSE_LEFT_BUTTON_UP,
        MOUSE_RIGHT_BUTTON_UP,
        MOUSE_MIDDLE_BUTTON_UP,
        MOUSE_BUTTON_4_UP,
        MOUSE_BUTTON_5_UP
    };

    MOUSE_INPUT_DATA data = { 0 };
    LONG dataConsumed = 0;
    KIRQL oldIrql = 0;

    if (!pDevExt->UpperConnectData.ClassService)
    {
        DebugPrint(("[TBW] PostButtonEvent - mouclass service callback = NULL\n"));
        return;
    }

    RtlZeroMemory(&data, sizeof(MOUSE_INPUT_DATA));

    DebugPrint(("[TBW] PostButtonEvent - DOWN: 0x%04x, UP: 0x%04x\n", uDownMask, uUpMask));

    // down flags
    for (size_t i = 0; uDownMask && i < 5; i++)
    {
        if (uDownMask & (0x01 << i))
        {
            data.ButtonFlags |= aMouseButtonDownFlags[i];
        }
    }

    // up flags
    for (size_t i = 0; uUpMask && i < 5; i++)
    {
        if (uUpMask & (0x01 << i))
        {
            data.ButtonFlags |= aMouseButtonUpFlags[i];
        }
    }

	pDevExt->tblibConfig.curButtonState |= uDownMask;
	pDevExt->tblibConfig.curButtonState &= ~uUpMask;

    DebugPrint(("[TBW] Posting button event to HID stack, ButtonFlags: 0x%04x\n", data.ButtonFlags));

    KeRaiseIrql(DISPATCH_LEVEL, &oldIrql);
    (*(PSERVICE_CALLBACK_ROUTINE)pDevExt->UpperConnectData.ClassService)(
        pDevExt->UpperConnectData.ClassDeviceObject,
        &data,
        &data + 1,
        &dataConsumed
        );
    KeLowerIrql(oldIrql);
}

void PostWheelEvent(PDEVICE_EXTENSION pDevExt, SHORT wheel, LONG units)
{
    MOUSE_INPUT_DATA data = { 0 };
    KIRQL oldIrql = 0;
    LONG dataConsumed = 0;

    if (!pDevExt->UpperConnectData.ClassService)
    {
        DebugPrint(("[TBW] PostWheelEvent - mouclass service callback = NULL\n"));
        return;
    }

    RtlZeroMemory(&data, sizeof(MOUSE_INPUT_DATA));
    data.ButtonFlags = wheel;
    //data.ButtonData = (SHORT) (Value/(MOD(Value)))*INTEGER_TICK;
    data.ButtonData = (SHORT) units*INTEGER_TICK;

    //DebugPrint(("[TBW] PostWheelEvent wheel: %s, units: %ld\n", wheel == MOUSE_WHEEL ? "MOUSE_WHEEL" : "MOUSE_HWHEEL", units));

    KeRaiseIrql(DISPATCH_LEVEL, &oldIrql);
    (*(PSERVICE_CALLBACK_ROUTINE)pDevExt->UpperConnectData.ClassService)(
        pDevExt->UpperConnectData.ClassDeviceObject,
        &data,
        &data + 1,
        &dataConsumed
        );
    KeLowerIrql(oldIrql);
}


// Only to be called for slimblade and ASSUMES that pReport is 6 bytes long!
void GetButtonStateSlimBlade(PDEVICE_EXTENSION pDevExt, PUCHAR pReport, UINT32* pDownMask, UINT32* pUpMask)
{
    // Slimblade incorrectly reports buton 3 & 4 status in byte 5 instead
    // of byte 1. Also, it uses bit 0 & 1 for the button staus bits. Fix
    // this by shifting these two bits to the left and ORing the result
    // into byte 1, the actual button status byte.
    pReport[1] |= (pReport[5] & 0x03) << 2;
    pReport[5] = 0;

    BYTE lastState = pDevExt->bSlimbladeLastButtonState;
    BYTE curState = pReport[1];

    if (curState ^ lastState)
    {
        // button states have changed
        if (curState > lastState)
        {
            // buttons pressed
            *pDownMask = (UINT32)curState ^ lastState;
            *pUpMask = 0;
        }
        else
        {
            // buttons released
            *pDownMask = 0;
            *pUpMask = (UINT32)(lastState ^ curState);

        }

        //DebugPrint(("[TBW] Slimblade buttons DOWN: 0x%04x UP: 0x%04x \n",
        //    *pDownMask, *pUpMask));
        pDevExt->bSlimbladeLastButtonState = curState;
    }
}

// Only to be called for Ergo Vertical Trackball and ASSUMES that pReport is 7 bytes long!
void GetButtonStateVerticalTrackball(PDEVICE_EXTENSION pDevExt, PUCHAR pReport, UINT32* pDownMask, UINT32* pUpMask)
{
    // Vertical Trackball HID descriptor reports only 5 buttons, whereas the 
    // device has 7 buttons. So we parse the button state manually.
    UINT32 lastState = pDevExt->uManualParserLastButtonState;
    UINT32 curState = pReport[1];

    if (curState ^ lastState)
    {
        // button states have changed
        if (curState > lastState)
        {
            // buttons pressed
            *pDownMask = curState ^ lastState;
            *pUpMask = 0;
        }
        else
        {
            // buttons released
            *pDownMask = 0;
            *pUpMask = lastState ^ curState;

        }

        //DebugPrint(("[TBW] Vertical TB buttons DOWN: 0x%04x UP: 0x%04x \n",
        //    *pDownMask, *pUpMask));
        pDevExt->uManualParserLastButtonState = curState;
    }
}

#if 0
void DispatchButtonEvent(PDEVICE_EXTENSION pDevExt, UINT32 uButtonDownMask, UINT32 uButtonUpMask)
{
    DebugPrint(("[TBW] Dispatching button event DOWN: 0x%04x, UP: 0x%04x\n", uButtonDownMask, uButtonUpMask));
#ifdef USE_TBLIB
	TBProcessButtonSplitEvent(&pDevExt->tblibConfig, uButtonDownMask, uButtonUpMask);
#else
    UINT32 maskOut = 0;
    if (IsUserModuleButtonEvent(
        pDevExt,
        uButtonDownMask ? uButtonDownMask : uButtonUpMask,
        &maskOut))
    {
        TBWKERNDATA kd = { 0 };
        kd.buttonDownMask = uButtonDownMask;
        kd.buttonUpMask = uButtonUpMask;
        kd.status = TBWKERNDATA_STATUS_BUTTONACTIVITY;
        PdoEnqueueEventAndService(pDevExt->pdohDevice, &kd);
    }
    else
    {
        //DebugPrint(("[TBW] Posting button event to HID stack\n"));
        PostButtonEvent(
            pDevExt,
            uButtonDownMask ? maskOut : 0,
            uButtonUpMask ? maskOut : 0);
    }
#endif
}
#endif

/**** tblib callbacks ****/

void tblibPostButtonEvent(void* ref, uint32_t buttonState, uint32_t uDownMask, uint32_t uUpMask)
{
	PDEVICE_EXTENSION pDevExt = (PDEVICE_EXTENSION)ref;
	UNREFERENCED_PARAMETER(pDevExt);
	UNREFERENCED_PARAMETER(buttonState);

	PostButtonEvent(pDevExt, uDownMask, uUpMask);
}

void tblibPostCursorEvent(void* ref, int dx, int dy)
{
	PDEVICE_EXTENSION pDevExt = (PDEVICE_EXTENSION)ref;
	PostPointerEvent(pDevExt, (LONG)dx, (LONG)dy);
}

void tblibPostWheelEvent(void* ref, int wheel, int offset)
{
	UNREFERENCED_PARAMETER(wheel);
	PDEVICE_EXTENSION pDevExt = (PDEVICE_EXTENSION)ref;
    
    // sanity check
    if (wheel != TBLIB_SCROLL_WHEEL_VERT && wheel != TBLIB_SCROLL_WHEEL_HORZ)
        return;

    PostWheelEvent(
        pDevExt,
        wheel == TBLIB_SCROLL_WHEEL_VERT ? MOUSE_WHEEL : MOUSE_HWHEEL,
        (LONG)offset);
}

void tblibEnqueueButtonEvent(void* ref, uint32_t uDownState, uint32_t uUpState)
{
	PDEVICE_EXTENSION pDevExt = (PDEVICE_EXTENSION)ref;
	TBWKERNDATA kd = { 0 };
	kd.buttonDownMask = uDownState;
	kd.buttonUpMask = uUpState;
	kd.status = TBWKERNDATA_STATUS_BUTTONACTIVITY;
	PdoEnqueueEventAndService(pDevExt->pdohDevice, &kd);
}

uint32_t tblibStartTimer(void* ref, uint32_t id, uint32_t timeout)
{
	UNREFERENCED_PARAMETER(id);
	UNREFERENCED_PARAMETER(timeout);
	PDEVICE_EXTENSION pDevExt = (PDEVICE_EXTENSION)ref;
	UNREFERENCED_PARAMETER(pDevExt);

	
	//&*&*&*G1_MOD
	//return (uint32_t)WdfTimerStart(
	//	id == TIMER_ID_CHORD ? pDevExt->timerDetectChord : pDevExt->timerInertialScroll,
	//	WDF_REL_TIMEOUT_IN_MS(timeout));
	WDFTIMER timer;
	switch (id)
	{
		case TIMER_ID_DEBOUNCE:
			timer = pDevExt->timerButtonDebounce;
			pDevExt->bDounceTimerStarted = TRUE;
			DebugPrint(("[TBW] TIMER_ID_DEBOUNCE started\n"));
			break;
		case TIMER_ID_TILT_BUTTON:
			timer = pDevExt->timerTiltButton;
			pDevExt->bTiltButtonTimerStarted = TRUE;
			DebugPrint(("[TBW] TIMER_ID_TILT_BUTTON started\n"));
			break;
#ifdef ENABLE_CHORDING_TIMER
		case TIMER_ID_CHORD:
			timer = pDevExt->timerDetectChord;
			pDevExt->bChordTimerStarted = TRUE;
			DebugPrint(("[TBW] TIMER_ID_CHORD\n"));
			break;
#endif
		case TIMER_ID_INERTIAL_SCROLL:
			timer = pDevExt->timerInertialScroll;
			DebugPrint(("[TBW] TIMER_ID_INERTIAL_SCROLL\n"));
			break;
		case TIMER_ID_AUTO_SCROLL:
			if (pDevExt->tblibConfig.bAutoScrollTimerStarted)
				return TRUE;
			timer = pDevExt->timerAutoScroll;
			pDevExt->tblibConfig.bAutoScrollTimerStarted = TRUE;
			DebugPrint(("[TBW] TIMER_ID_AUTO_SCROLL\n"));
			break;
		default:
			return FALSE;
	}

	DebugPrint(("[TBW] Starting timer: %d, timeout: %d\n", id, timeout));
	return (uint32_t)WdfTimerStart(
		timer,
		WDF_REL_TIMEOUT_IN_MS(timeout)
	);
	//&*&*&*G2_MOD
}

void tblibStopTimer(void* ref, uint32_t id)
{
	UNREFERENCED_PARAMETER(id);
	PDEVICE_EXTENSION pDevExt = (PDEVICE_EXTENSION)ref;
	UNREFERENCED_PARAMETER(pDevExt);
	DebugPrint(("[TBW] Stopping timer: %d\n", id));
	//&*&*&*G1_MOD
	//WdfTimerStop(
	//	id == TIMER_ID_CHORD ? pDevExt->timerDetectChord : pDevExt->timerInertialScroll,
	//	FALSE);
	WDFTIMER timer;
	switch (id)
	{
		case TIMER_ID_DEBOUNCE:
			timer = pDevExt->timerButtonDebounce;
			pDevExt->bDounceTimerStarted = FALSE;
			break;
		case TIMER_ID_TILT_BUTTON:
			timer = pDevExt->timerTiltButton;
			pDevExt->bTiltButtonTimerStarted = FALSE;
			break;
#ifdef ENABLE_CHORDING_TIMER
		case TIMER_ID_CHORD:
			timer = pDevExt->timerDetectChord;
			pDevExt->bChordTimerStarted = FALSE;
			break;
#endif
		case TIMER_ID_INERTIAL_SCROLL:
			timer = pDevExt->timerInertialScroll;
			break;
		case TIMER_ID_AUTO_SCROLL:
			timer = pDevExt->timerAutoScroll;
			pDevExt->tblibConfig.bAutoScrollTimerStarted = FALSE;
			break;
		default:
			return;
	}
	WdfTimerStop(
		timer,
		FALSE
	);
	//&*&*&*G2_MOD

}
