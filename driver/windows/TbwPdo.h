#ifndef _H_tbwpdo
#define _H_tbwpdo

#pragma warning(disable:4201)

#include "ntddk.h"

#pragma warning(default:4201)

#include <wdf.h>
#include "wdfringbuffer.h"
#include "tbwkernelapi.h"

#include "debug.h"

#define PDO_DATATOSENDBUFFER_SIZE			32

typedef struct _PDO_DEVICE_EXTENSION
{
    WDFDEVICE		WdfDevice;

    WDFRINGBUFFER	DataToSend;
	WDFQUEUE		ParkedReadRequestsQueue;
	WDFSPINLOCK		ServiceQueuesLock;

	ULONG			InstanceNo;
	WDFDEVICE		Parent;

	WDFSPINLOCK		QueueLock;

    // File object opened & held by TbwHelper.exe.
    // This set to NULL indicates that TbwHelper.exe is not connected.
    PFILE_OBJECT    pfoHelper;

} PDO_DEVICE_EXTENSION, *PPDO_DEVICE_EXTENSION;

WDF_DECLARE_CONTEXT_TYPE_WITH_NAME(PDO_DEVICE_EXTENSION, PdoGetData)

/*
 * Create the PDO that userspace modules will use to configure the device.
 */
NTSTATUS PdoCreate(WDFDEVICE Device, ULONG InstanceNo, WDFDEVICE *PPdoDevice);

/*
 * Destroy the PDO, created earlier throuth PdoCreate.
 */
VOID PdoClose(WDFDEVICE PdoDevice);

/*
 * Enqueue an event to be sent to TbwHelper userspace module
 */
VOID PdoEnqueueEventAndService(WDFDEVICE PdoDevice, TBWKERNDATA *Event_pdo);

/*
 * Returns a BOOLEAN indicating if TbwHelper.exe userspace module is running
 * and registered with us.
 */
BOOLEAN IsHelperConected(WDFDEVICE PdoDevice);

#endif