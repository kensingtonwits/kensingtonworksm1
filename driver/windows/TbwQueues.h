#ifndef _H_tbwqueues
#define _H_tbwqueues

#ifdef __cplusplus
extern "C" {
#endif

#include <wdf.h>
#include "wdfringbuffer.h"

NTSTATUS QueuesEnqueueReportAndService(WDFQUEUE Queue, WDFRINGBUFFER Ringbuffer, UCHAR *Report, size_t Size);
NTSTATUS QueuesService(WDFQUEUE Queue, WDFRINGBUFFER Ringbuffer, UCHAR *Report, size_t Size);

#ifdef __cplusplus
}
#endif

#endif