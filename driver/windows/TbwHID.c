#include <ntddk.h>
#include <wdf.h>
#include <hidpddi.h>
#include <hidclass.h>

#include "tbwkernelapi.h"
#include "tbwconfig.h"
#include "tbwhid.h"
#include "util.h"

#include "debug.h"

/*
 * A wrapper that makes it easy to call the underlying HID driver's IOCTLs to
 * get/set device specific information.
 */
static NTSTATUS HidQueryIoCtl(WDFDEVICE Device, ULONG IoctlCode, UCHAR *OutBuffer, size_t Size, ULONG_PTR *BytesReturned);

NTSTATUS HidQueryIoCtl(WDFDEVICE Device, ULONG IoctlCode, UCHAR *OutBuffer, size_t Size, ULONG_PTR *BytesReturned)
{
	WDF_MEMORY_DESCRIPTOR memOutputDescriptor;
	WDF_MEMORY_DESCRIPTOR *pMemOutputDescriptor = &memOutputDescriptor;

	if(OutBuffer)
		WDF_MEMORY_DESCRIPTOR_INIT_BUFFER(&memOutputDescriptor, OutBuffer, (ULONG)Size);
	else
		pMemOutputDescriptor = NULL;

	return WdfIoTargetSendIoctlSynchronously(WdfDeviceGetIoTarget(Device), NULL, IoctlCode, NULL, pMemOutputDescriptor, NULL, BytesReturned);
}


NTSTATUS HidGetDeviceInfo(WDFDEVICE Device, USHORT* pusVendorID, USHORT* pusProductID, USHORT *pusVersionNumber)
{
	NTSTATUS                    status = 0;
    HID_COLLECTION_INFORMATION  info = { 0 };
	
	status = HidQueryIoCtl(
        Device, 
        IOCTL_HID_GET_COLLECTION_INFORMATION, 
        (UCHAR *) &info, 
        sizeof(HID_COLLECTION_INFORMATION), 
        NULL);

    if (!NT_SUCCESS(status))
    {
        DebugPrint(("[TBW] HidGetDeviceType - HidQueryIoctl failed with status: 0x%x\n", status));
        return status;
    }

    if (pusProductID)
        *pusProductID = info.ProductID;

	if (pusVendorID)
		*pusVendorID = info.VendorID;

	if (pusVersionNumber)
        *pusVersionNumber = info.VersionNumber;

#if 0
    if (DeviceType)
    {
        *DeviceType = TBW_TYPE_UNKNOWN;

        if (info.VendorID == kVID_KENSINGTON)
        {
            switch (info.ProductID)
            {
            case kPID_EXPERT:
                *DeviceType = TBW_TYPE_EXPERT;
                status = STATUS_SUCCESS;

                DebugPrint(("[TBW] Trackball type: Expert\n"));
                break;

            case kPID_SLIMBLADE:
                if (info.VersionNumber >= 0x0107)
                {
                    *DeviceType = TBW_TYPE_SLIMBLADE;
                    status = STATUS_SUCCESS;

                    DebugPrint(("[TBW] Trackball type: Slimblade\n"));
                }
                else
                {
                    *DeviceType = TBW_TYPE_SLIMBLADE_NOCHORD;
                    status = STATUS_SUCCESS;

                    DebugPrint(("[TBW] Trackball type: Slimblade fw105\n"));
                }
                break;

            case kPID_ORBIT:
                *DeviceType = TBW_TYPE_ORBIT;
                status = STATUS_SUCCESS;

                DebugPrint(("[TBW] Trackball type: Orbit\n"));
                break;

            case kPID_EAGLE:
                *DeviceType = TBW_TYPE_EAGLE;
                status = STATUS_SUCCESS;

                DebugPrint(("[TBW] Trackball type: Eagle\n"));
                break;

            case kPID_KESTREL:
                *DeviceType = TBW_TYPE_KESTREL;
                status = STATUS_SUCCESS;

                DebugPrint(("[TBW] Trackball type: Kestrel\n"));
                break;

            case kPID_EXPERTW:
                *DeviceType = TBW_TYPE_EXPERT_W;
                status = STATUS_SUCCESS;

                DebugPrint(("[TBW] Trackball type: Expert wireless (dongle)\n"));
                break;

            case kPID_EXPERTB:
                *DeviceType = TBW_TYPE_EXPERT_BLE;
                status = STATUS_SUCCESS;

                DebugPrint(("[TBW] Trackball type: Expert wireless (ble)\n"));
                break;
            }
        }
    }
#endif
    return status;
}

NTSTATUS HidGetPreparsedData(WDFDEVICE hDevice, PHIDP_PREPARSED_DATA* ppHidPreparsedData, ULONG* pulSize)
{
    NTSTATUS                    status = 0;
    HID_COLLECTION_INFORMATION  info = { 0 };
    PHIDP_PREPARSED_DATA        preparsedData = NULL;

    status = HidQueryIoCtl(
        hDevice,
        IOCTL_HID_GET_COLLECTION_INFORMATION,
        (UCHAR *)&info,
        sizeof(HID_COLLECTION_INFORMATION),
        NULL);

    if (!NT_SUCCESS(status))
    {
        DebugPrint(("[TBW] HidGetPreparsedData - HidQueryIoctl failed with status: 0x%x\n", status));
        return status;
    }

    //DebugPrint(("[TBW] HidGetPreparsedData - report descriptor size: %d\n", info.DescriptorSize));

    // Allocate memory to hold the pre-parsed data
    preparsedData = (PHIDP_PREPARSED_DATA)ExAllocatePoolWithTag(
        NonPagedPoolNx,
        info.DescriptorSize,
        'TBW');

    if (preparsedData == NULL)
    {
        status = STATUS_INSUFFICIENT_RESOURCES;
        goto FreeAndExit;
    }

    status = HidQueryIoCtl(hDevice,
        IOCTL_HID_GET_COLLECTION_DESCRIPTOR,
        (UCHAR*)preparsedData,
        info.DescriptorSize,
        NULL);

    if (!NT_SUCCESS(status))
    {
        DebugPrint(("[TBW] HidGetPreparsedData - IOCTL_HID_GET_COLLECTION_DESCRIPTOR failed 0x%x\n", status));
        goto FreeAndExit;
    }

    *ppHidPreparsedData = preparsedData;
    *pulSize = (ULONG)info.DescriptorSize;

    return STATUS_SUCCESS;

FreeAndExit:
    if (preparsedData != NULL) {
        ExFreePool(preparsedData);
        preparsedData = NULL;
    }

    return status;
}

/*
Returns the name of the product in szName. Ensure that szName can hold at least
127 wide chars.
*/
NTSTATUS HidGetProductString(WDFDEVICE hDevice, WCHAR szName[], size_t nChars)
{
    NTSTATUS status = 0;
    status = HidQueryIoCtl(
        hDevice,
        IOCTL_HID_GET_PRODUCT_STRING,
        (UCHAR *)szName,
        nChars,
        NULL);

    if (!NT_SUCCESS(status))
    {
        DebugPrint(("[TBW] HidGetProductString - HidQueryIoctl failed with status: 0x%x\n", status));
        return status;
    }

    return status;
}

/*
Returns the product serial number string embedded in the device.
*/
NTSTATUS HidGetSerialNumberString(WDFDEVICE hDevice, WCHAR szSerialNumber[], size_t nChars)
{
    NTSTATUS status = 0;
    status = HidQueryIoCtl(
        hDevice,
        IOCTL_HID_GET_SERIALNUMBER_STRING,
        (UCHAR *)szSerialNumber,
        nChars,
        NULL);

    if (!NT_SUCCESS(status))
    {
        DebugPrint(("[TBW] HidGetSerialNumberString - HidQueryIoctl failed with status: 0x%x\n", status));
        return status;
    }

    return status;
}
