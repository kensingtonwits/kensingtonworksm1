//
//  trackball.h
//
//  Created by Hariharan Mahadevan on 2019/5/13.
//  Copyright © 2019 Kensington. All rights reserved.
//
#pragma once

//#define _USER_MODE

//#include "debug.h"

#ifdef _TBLIB_USER_MODE

    // user mode

    #ifdef __APPLE__

    #include <_types/_uint64_t.h>
    #include <_types/_uint32_t.h>
    #include <_types/_uint16_t.h>
    #include <_types/_uint8_t.h>

    #define TBLOG(_x_) printf _x_

    #elif _WINDOWS
    // TODO
    #endif // #ifdef __APPLE__

#else   //

    // kernel mode
    #ifdef __APPLE__
    #include <TargetConditionals.h>
    #if !TARGET_OS_DRIVERKIT
        #include <IOKit/IOLib.h>
    #else
        #include <DriverKit/DriverKit.h>
    #endif
    #include <math.h>

    #ifdef DEBUG
    #include "tblog.h"
    #define TBLOG(_x_) TBLogDebug _x_
    #else
    #define TBLOG(_x_)
    #endif //__APPLE__

    #else   // WINDOWS

	#include <ntddk.h>
	#include <wdf.h>


	#ifdef DBG
    #include "debug.h"
    #define TBLOG(_x_) DebugPrint(_x_)
    #else
    #define TBLOG(_x_)
    #endif

    #endif  // #ifdef __APPLE__

#endif // #ifdef _USER_MODE


#ifdef __APPLE__
    #include "KWDriverInterface.h"
#else
    #include "TbwKernelAPI.h"

	// Windows driver doesn't know about C standard fixed width types.
	// So provide type definitions for them (copied from stdint.h)
	typedef signed char        int8_t;
	typedef short              int16_t;
	typedef int                int32_t;
	typedef long long          int64_t;
	typedef unsigned char      uint8_t;
	typedef unsigned short     uint16_t;
	typedef unsigned int       uint32_t;
	typedef unsigned long long uint64_t;
#endif

#define INERTIAL_SCROLL_TIMER_INTERVAL		70
#define AUTO_SCROLL_TIMER_INTERVAL			300
#define MAX_AUTO_SCROLL_COUNT				50
#define	CHORD_DETECT_TIMER_INTERVAL			50  //100	//80
#define CHORD_DETECT_TIMEOUT				50  //100
#define PROGRESSION_FACTOR					80
#define CHORD_DEBOUNCE_TIMEOUT              100 //50
#define TILT_BUTTON_TIMEOUT1                450
#define TILT_BUTTON_TIMEOUT2                200

/*
    Post button click event to OS stack.
    Parameters:
        void* ref
            callback ref
        uint32_t buttonState
            button click bit mask
 */
typedef void (*PFN_POSTBUTTONEVENT)(void* ref, uint32_t buttonState, uint32_t uDownMask, uint32_t uUpMask);

/*
    Post cursor movement event to OS stack.
    Parameters:
        void* ref
            callback ref
        int dx
            x-axis relative movement co-ords
        int dy
            y-axis relative movement co-ords
 
 */
typedef void (*PFN_POSTCURSOREVENT)(void* ref, int dx, int dy);

/*
    Post wheel movement to OS stack.
    Parameters:
        void* ref
            callback ref
        int wheel:
            0 - vert
            1 - horiz
        int offset
            wheel offset
 */
typedef void (*PFN_POSTWHEELEVENT)(void* ref, int wheel, int offset);

/*
    Enqueue button event to be sent to helper module
    Parameters:
        void* ref
            callback ref
        uint32_t uDownState
            button down state bit mask
        uint32_t uUpState
            button up state bit mask
 */
typedef void (*PFN_ENQUEUEBUTTONEVENT)(void* ref, uint32_t uDownState, uint32_t uUpState);

/*
    Start OS specific timer routine.
    Parameters:
        void* ref
            callback ref
        uint32_t id
            timer id
        uint32_t timeout
            timeout in milliseconds
    Return:
        uint32_t
            boolean success indicator
 */
typedef uint32_t (*PFN_STARTTIMER)(void* ref, uint32_t id, uint32_t timeout);

typedef void (*PFN_STOPTIMER)(void* ref, uint32_t id);

// OSX does not have LARGE_INTEGER, which is a Windows specific type.
// Essentially it's a 64-bit data type, which can be nativelyd defined
// in 64-bit mode. But on 32-bit, it becomes a structure of uint32_t
// low part and signed int32_t high part.
#ifdef __APPLE__
typedef union _LARGE_INTEGER {
  struct {
    uint32_t LowPart;
    uint32_t  HighPart;
  };
  struct {
    uint32_t LowPart;
    uint32_t HighPart;
  } u;
  uint64_t QuadPart;    // Though the struct is INTEGER, we're not interested
                        // in its sign bit as we use this only for caclulating
                        // relative time. So in OSX we declare the components
                        // using uint64_t.
} LARGE_INTEGER, *PLARGE_INTEGER;
#endif

/*
 * Return current system clock ticks since last bootup in pTime.
 */
typedef void (*PFN_GETSYSTEMCLOCKTICKS)(LARGE_INTEGER* pTime);

/*
 * Convert system clock ticks into nano seconds by using system clock's 
 * base frequency.
 */
typedef void (*PFN_SYSTEMCLOCKTICKSTONANOSECS)(LARGE_INTEGER pTime, LARGE_INTEGER* nanosecs);

// Used for caching button events for chord processing
typedef struct _TBLIBBUTTONEVENT {
    uint32_t uDownMask;
    uint32_t uUpMask;
} TBLIBBUTTONEVENT;

// Maximum number of button events that can be cached for chord
// processing. 16 is not an arbitrary number. It's computed based
// on what is humanly possible to depress a sequence of buttons
// that would constitute a chord event within the chord processing
// timeout (~70 milliseconds) and the maximum number of buttons
// on the trackball (<=8).
#define CHORD_BUTTON_EVENTS_CACHE_SIZE      16

#define MAX_CHORD_BUTTONS_NO	16

#define TIMER_ID_CHORD              1
#define TIMER_ID_INERTIAL_SCROLL    2
#define TIMER_ID_AUTO_SCROLL		3
#define TIMER_ID_DEBOUNCE           4
#define TIMER_ID_TILT_BUTTON        5


#ifdef __APPLE__
#ifndef BOOLEAN
typedef uint32_t BOOLEAN;
#endif
#endif

#ifndef TRUE
#define TRUE    1
#endif

#ifndef FALSE
#define FALSE   0
#endif

// Scroll wheel types
#define TBLIB_SCROLL_WHEEL_VERT   1
#define TBLIB_SCROLL_WHEEL_HORZ   2

#define TBLIB_SCROLL_DIRECTION_UP     1
#define TBLIB_SCROLL_DIRECTION_DOWN   2
#define TBLIB_SCROLL_DIRECTION_LEFT   3
#define TBLIB_SCROLL_DIRECTION_RIGHT  4
#ifdef SCROLL_WITH_TRACKBALL
#define TBLIB_SCROLL_DIRECTION_TRACKBALL	5
#define TBLIB_SCROLL_DIRECTION_TRACKAUTO	6
#endif
// Button action definitions
#define TBLIB_BUTTON_ACTION_CLICK     0
#define TBLIB_BUTTON_ACTION_SCROLL    1
#define TBLIB_BUTTON_ACTION_DRAG      2
#define TBLIB_BUTTON_ACTION_X		  99


#ifdef __APPLE__
#define TBLIB_MAX_BUTTONS             4
#else
#define TBLIB_MAX_BUTTONS             5
#endif

// Important that we force structure alignment as header file is shared
// between user mode and kernel mode.
#pragma pack(push, 1)
typedef struct _TBLIBBUTTONACTION2 {
    uint32_t buttonMask;
    uint8_t action;
    union {
        struct {
            uint32_t buttonMask;
            uint32_t _modifiers;
        } ClickActionParams;
        struct {
            uint8_t direction; // ScrollDirection
            uint8_t lines;
        } ScrollActionParams;
    } u;
} TBLIBBUTTONACTION2;

typedef struct _TBLIBBUTTONSCONFIG2 {
    uint32_t      cbSize;
    uint32_t      chordMask;
	uint32_t      chordMsks[MAX_CHORD_BUTTONS_NO];
    uint32_t      nButtons;
    TBLIBBUTTONACTION2 buttons[32];
} TBLIBBUTTONSCONFIG2;

typedef struct _TBLIBSCROLL_WHEEL_CONFIG
{
    int                     Wheel;  // SCROLL_WHEEL_VERT or SCROLL_WHEEL_HORZ
	int						Offset;

    double                  fSpeed;

    // Whether to invert scroll wheel direction
    BOOLEAN                 bInvert;

    // Inertial scroll flag
    BOOLEAN                 bInertialScroll;

    // Attrition factor that is used to decay the inertial scroll value
    double                  fAttritionFactor;
    // Inertial scroll velocity that will be computed from the start time
    // and total distance travelled
    double                  fVelocity;
    // Timer tick count when scroll motion was detected. This is used
    // to compute the scroll velocity which will be used to generate
    // the quantum of the inertial scroll movement amount
    LARGE_INTEGER           timeStart;

    // Total distance travelled since scroll was started
    long                    lTotalDistance;
    // Number of scroll wheel messages(in the same direction) received
    long					nMessages;
    // Calculated scroll speed, scaled to units of 100.
    // This will be adjusted down by 80% for every timer message until it falls
    // below 50 when the inertial scroll will stop.
    long					lScrollSpeed;

} TBLIBSCROLL_WHEEL_CONFIG;

// stores all settings pertaining to TrackballWorks
typedef struct _TBLIBCONFIG {

    // first argument for all callbacks
    void* refCallback;
    // callback functions
    PFN_POSTBUTTONEVENT pfnPostButtonEvent;
    PFN_POSTCURSOREVENT pfnPostCursorEvent;
    PFN_POSTWHEELEVENT pfnPostWheelEvent;
    PFN_ENQUEUEBUTTONEVENT pfnEnqueueButtonEvent;
    PFN_STARTTIMER pfnStartTimer;
    PFN_STOPTIMER  pfnStopTimer;
    
    uint32_t    fHelperConnected;
    
    // for button depress/release detection logic
    uint32_t    curButtonState;   // current reported button state
    uint32_t    curPhysButtonState;  // current trackball button state
    uint32_t    lastPhysButtonState;
    //uint32_t    lastVirtButtonState;

    uint32_t    curActionButtonState;
	uint32_t    lastActionButtonState;
	BOOLEAN     bDragging;
    uint32_t    draggingActionButtonState;

    // ACPan button state. Following bits are used:
    //
    //      bit 0 - pan left button state
    //      bit 1 - pan right button state
    // 
    // bit == 1, button is down, bit == 0, button is up
    //uint32_t    lastPanPhysButtonState;

    TBLIBBUTTONSCONFIG2 buttonsConfig;

    BOOLEAN     bChordDebounceTimerStarted;
    BOOLEAN     bTiltButtonTimerStarted;
    uint32_t    uTiltButtonState;
	//int32_t		iChordDebounceCount;
	//BOOLEAN     bChordDebounceDone;
    // chording support
    //BOOLEAN     bChordTimerStarted;
    uint32_t    uChordDebounceButton;
    uint32_t    uButtonUpEventsToSuppress;
    uint32_t    uCurChordEventsMask;
    uint32_t    nChordButtonEvents;
	BOOLEAN		bChording;
	uint32_t	uSupplementButtonState;
	TBLIBBUTTONEVENT aChordButtonEventsCache[CHORD_BUTTON_EVENTS_CACHE_SIZE];
    
    // Single-axis (locked axis) pointer movement properties
    BOOLEAN     fLockPointerAxis;
	int         dominance;

    // Slow pointer movement properties
    BOOLEAN     fSlowPointer;
    uint32_t    uSlowdownDivider;
    int         remainderX;
	int         remainderY;

#ifdef SCROLL_WITH_TRACKBALL
	BOOLEAN     fScrollwTrackball;
	BOOLEAN		fAuto;
	BOOLEAN		bAutoScrollTimerStarted;
	BOOLEAN		bTracking;
	uint32_t    uScrolwDivider;
	uint32_t	uScrollCounter;
#endif

    // scroll wheel properties
    TBLIBSCROLL_WHEEL_CONFIG scrollParamsVert;
    TBLIBSCROLL_WHEEL_CONFIG scrollParamsHorz;
    TBLIBSCROLL_WHEEL_CONFIG* pCurInertialScrollConfig;
    
    double      dPointerSpeed;

} TBLIBCONFIG, *PTBLIBCONFIG;
#pragma pack(pop)

/*
Initialize the Trackball subsystem
*/

#ifndef max
#define max(a,b) (((a) > (b)) ? (a) : (b))
#endif

#ifdef _WIN32
EXTERN_C
#endif
void TBInit(
    TBLIBCONFIG* pConfig,
    void* refCallback,
    PFN_POSTBUTTONEVENT,
    PFN_POSTCURSOREVENT,
    PFN_POSTWHEELEVENT,
    PFN_ENQUEUEBUTTONEVENT,
    PFN_STARTTIMER,
    PFN_STOPTIMER);

/*
 * Set button configuration
 */
#ifdef _WIN32
EXTERN_C
#endif
void TBSetButtonsConfig(TBLIBCONFIG* pConfig, const TBLIBBUTTONSCONFIG2* pButtonsConfig);

/*
 * Process button press/release event.
 *
 * Parameters:
 *	buttonState - the button depress/release bitmask, where each bit represents
		the state the corresponding button's deperessed/released state. When a
		a bit is set, the button is considered depressed and when the bit 
		becomes 0, it is considered released.
 */
#ifdef _WIN32
EXTERN_C
#endif
void TBProcessButtonEvent(TBLIBCONFIG* pConfig, uint32_t buttonState);

/*
 * Process button depress/release event.
 *
 * Same as the above function, except that the depress/release button states
 * are specified as separate arguments.
 *
 * Parameters:
 *	uDownMask - Bitmask corresponding to the buttons that are depressed.
 *	uUpMask   - Bitmask corresponding to the buttons that are released.
 *
 * For both the bitmasks, calls to the function should only set bitmasks for
 * any new buttons whose state has changed. That is if button 2 is depressed
 * and consequently uDownMask is set to 0x02 and subsequently, button 1 is 
 * depressed, while button 2 is still depressed, second call should set 
 * uDownMask to 0x01 and NOT 0x03. Similarly for uUpMask as well.
 *
 * In other words, bitmasks representing button states should always specify
 * change in button state or 0 if there's no change.
 *
 * This is the core function that processes button state changes and
 * TBProcessButtonEvent above, calls this function after converting the unified
 * button state bitmask into distinct DOWN & UP state bitmasks.
 */
#ifdef _WIN32
EXTERN_C
#endif
void TBProcessButtonSplitEvent(TBLIBCONFIG* pConfig, uint32_t uDownMask, uint32_t uUpMask);

/*
 * Process relative cursor movement event
 */
#ifdef _WIN32
EXTERN_C
#endif
void TBProcessPointerEvent(TBLIBCONFIG* pConfig, int dx, int dy);

/*
 * Process scroll wheel event
 *
 * Parameters:
 *  int wheel:
 *      1 - vertical wheel
 *      2 - horizontal wheel
 *  int offset
 *      wheel offset
 */
#ifdef _WIN32
EXTERN_C
#endif
void TBProcessWheelEvent(TBLIBCONFIG* pConfig, int wheel, int offset);

/*
 * Process ACPan Left/Right event.
 *
 * Parameters:
 *  int offset
 *      < 0 - pan left units
 *      > 1 - pan right units
 *        0 - pan null event
 */
#ifdef _WIN32
EXTERN_C
#endif
void TBProcessACPanEvent(TBLIBCONFIG* pConfig, int offset);


/*
 * Handle timer timeout
 */
#ifdef _WIN32
EXTERN_C
#endif
void TBHandleTimeOut(TBLIBCONFIG* pConfig, uint32_t timerId);


/*
 * Set's the helper connected/disconnected flag.
 *
 * Returns:
 *  Previous setting for the helperConnected flag.
 */
#ifdef _WIN32
EXTERN_C
#endif
uint32_t TBSetHelperConnected(TBLIBCONFIG* pConfig, uint32_t fConnected);

/*
 * Enable/disable locked pointer axis movement
 */
#ifdef _WIN32
EXTERN_C
#endif
void TBSetLockedPointerAxis(TBLIBCONFIG* pConfig, BOOLEAN bEnable);

/*
 * Return current locked pointer axis movement setting.
 */
#ifdef _WIN32
EXTERN_C
#endif
BOOLEAN TBGetLockedPointerAxis(TBLIBCONFIG* pConfig);

/*
 * Enable/disable slow pointer movement
 */
#ifdef _WIN32
EXTERN_C
#endif
void TBSetSlowPointer(TBLIBCONFIG* pConfig, BOOLEAN bEnable);

/*
 * Return current slow pointer movement setting.
 */
#ifdef _WIN32
EXTERN_C
#endif
BOOLEAN TBGetSlowPointer(TBLIBCONFIG* pConfig);

#ifdef SCROLL_WITH_TRACKBALL
/*
 * Enable/disable slow pointer movement
 */
#ifdef _WIN32
EXTERN_C
#endif
void TBSetScrollwTrackball(TBLIBCONFIG* pConfig, BOOLEAN bEnable);

/*
 * Return current slow pointer movement setting.
 */
#ifdef _WIN32
EXTERN_C
#endif
BOOLEAN TBGetScrollwTrackball(TBLIBCONFIG* pConfig);
#endif

/*
 * Enable/disable inertial scroll.
 */
#ifdef _WIN32
EXTERN_C
#endif
void TBSetScrollParams(TBLIBCONFIG* pConfig, int Wheel, double fSpeed, BOOLEAN bInvert, BOOLEAN bInertial);

/*
 * Return current inertial scroll status.
 */
#ifdef _WIN32
EXTERN_C
#endif
void TBGetScrollParams(TBLIBCONFIG* pConfig, int Wheel, double* pfSpeed, BOOLEAN* pbInvert, BOOLEAN* pbInerial);

/*
 * Get pointer speed, which is a double that is used to multiply
 * the dx,dy movements to generate the effective dx,dy co-ords.
 */
#ifdef _WIN32
EXTERN_C
#endif
double TBGetPointerSpeed(TBLIBCONFIG* pConfig);

/*
 * Set pointer speed multiplying factor.
 */
#ifdef _WIN32
EXTERN_C
#endif
void TBSetPointerSpeed(TBLIBCONFIG* pConfig, double speed);
