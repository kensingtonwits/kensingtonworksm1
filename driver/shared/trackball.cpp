//
//  trackball.cpp
//  tbwhelper
//
//  Created by Hariharan Mahadevan on 2019/5/13.
//  Copyright © 2019 Kensington. All rights reserved.
//

// #define _USER_MODE
#include "trackball.h"
#ifdef __APPLE__
#else
#include "wdm.h"
#endif

#define LOG_PREFIX "trackball.cpp " " v.1.3.0 "

#ifdef _USER_MODE
#include <cstdio>
#include <cstring>
using namespace std;
#endif

// Internal helper functions
static void SetDefaultButtonsConfig(TBLIBCONFIG* pConfig);
//static void DetectChords(TBLIBCONFIG* pConfig, uint32_t* pDownMask, uint32_t* pUpMask);
static void HandleChordDebounceTimeOut(TBLIBCONFIG* pConfig);
static void HandleTiltButtonTimeOut(TBLIBCONFIG* pConfig);
static void ProcessButtonEvent(TBLIBCONFIG* pConfig);
//static void HandleDetectChordsTimeOut(TBLIBCONFIG* pConfig);
static bool ProcessChordingButtons(TBLIBCONFIG* pConfig);
static uint32_t GetChordButtons(TBLIBCONFIG* pConfig, uint32_t button, int *n);
static bool TranslateButtonMask(TBLIBCONFIG* pConfig, uint32_t uMaskIn, uint32_t* puMaskOut);
static bool IsHelperConnected(TBLIBCONFIG* pConfig);
static uint32_t numberOfSetBits(uint32_t u);
static void PostCachedChordDetectEvents(TBLIBCONFIG* pConfig);
static void HandleInertialScrollTimeOut(TBLIBCONFIG* pConfig);
//static void HandleInertialScrollUsingAccel(TBLIBCONFIG* pConfig, TBLIBSCROLL_WHEEL_CONFIG* pWheel);
static void HandleInertialScrollUsingDistance(TBLIBCONFIG* pConfig, TBLIBSCROLL_WHEEL_CONFIG* pWheel);
static void ApplyLockedPointerAxis(TBLIBCONFIG* pConfig, int* pDX, int* pDY);
static void ApplySlowPointer(TBLIBCONFIG* pConfig, int* pDX, int* pDY);
#ifdef SCROLL_WITH_TRACKBALL
static void HandleAutoScrollTimeOut(TBLIBCONFIG* pConfig);
static void ApplyScrollwTrackball(TBLIBCONFIG* pConfig, int* pDX, int* pDY);
#endif
static void ApplyCustomScrollConfig(TBLIBCONFIG* pConfig, TBLIBSCROLL_WHEEL_CONFIG* pScrollConfig, int* piWheel);
static TBLIBBUTTONACTION2* GetButtonAction(TBLIBCONFIG* pConfig, uint32_t uMaskIn);
static void DumpButtonsConfig(TBLIBCONFIG*);
static void TakeScrollAction(TBLIBCONFIG* pConfig, TBLIBBUTTONACTION2* pButtonAction, int offset);

#define MOD(a) (a < 0 ? -a : a)

#define MAX_BUTTON_NO	32
#define CHORDING_EVENT_NO	1


// Timer interval for inserting inertial scroll events
//#define INERTIAL_SCROLL_TIMEOUT     70
//#define PROGRESSION_FACTOR			80

const uint32_t PAN_LEFT_BUTTON_MASK = 0x01u << 30;   // bit 30
const uint32_t PAN_RIGHT_BUTTON_MASK = 0x01u << 31;  // bit 31
const uint32_t PAN_BUTTON_STATE_MASK = 0xC0000000u;  // bits 30 & 31

//---- API Implementation  **/

void TBInit(
    TBLIBCONFIG* pConfig,
    void* refCallback,
    PFN_POSTBUTTONEVENT pfnButton,
    PFN_POSTCURSOREVENT pfnCursor,
    PFN_POSTWHEELEVENT pfnWheel,
    PFN_ENQUEUEBUTTONEVENT pfnEnqueue,
    PFN_STARTTIMER pfnStartTimer,
    PFN_STOPTIMER pfnStopTimer)
{
#ifdef __APPLE__
    // TODO: Apple specific initialization
#else
    // TODO: Windows specific initialization
#endif

    pConfig->fLockPointerAxis = FALSE;
	pConfig->dominance = 0;

    pConfig->fSlowPointer = FALSE;
    pConfig->uSlowdownDivider = 5;
    pConfig->remainderX = 0;
	pConfig->remainderY = 0;

#ifdef SCROLL_WITH_TRACKBALL
	pConfig->fScrollwTrackball = FALSE;
	pConfig->fAuto = FALSE;
	pConfig->bTracking = FALSE;
	pConfig->uScrolwDivider = 1;
    pConfig->bAutoScrollTimerStarted = FALSE; // default value
#endif

    pConfig->bDragging = FALSE;
    
    // Vertical scroll
    const double DEFAULT_SCROLL_ATTRITION_FACTOR = 0.5;
    
    pConfig->scrollParamsVert.Wheel = TBLIB_SCROLL_WHEEL_VERT;
    pConfig->scrollParamsVert.fSpeed = 1.0;
    pConfig->scrollParamsVert.bInvert = FALSE;
    pConfig->scrollParamsVert.bInertialScroll = FALSE;
    pConfig->scrollParamsVert.fAttritionFactor = DEFAULT_SCROLL_ATTRITION_FACTOR;
    pConfig->scrollParamsVert.fVelocity = 0.0;
    pConfig->scrollParamsVert.lTotalDistance = 0;
	pConfig->scrollParamsVert.nMessages = 0;
	pConfig->scrollParamsVert.lScrollSpeed = 0;
    pConfig->scrollParamsVert.timeStart.QuadPart = 0;

    // Horizontal scroll
    pConfig->scrollParamsHorz.Wheel = TBLIB_SCROLL_WHEEL_HORZ;
    pConfig->scrollParamsHorz.fSpeed = 1.0;
    pConfig->scrollParamsHorz.bInvert = FALSE;
    pConfig->scrollParamsHorz.bInertialScroll = FALSE;
    pConfig->scrollParamsHorz.fAttritionFactor = DEFAULT_SCROLL_ATTRITION_FACTOR;
    pConfig->scrollParamsHorz.fVelocity = 0.0;
    pConfig->scrollParamsHorz.lTotalDistance = 0;
	pConfig->scrollParamsHorz.nMessages = 0;
	pConfig->scrollParamsHorz.lScrollSpeed = 0;
	pConfig->scrollParamsHorz.timeStart.QuadPart = 0;

    SetDefaultButtonsConfig(pConfig);
    
    pConfig->refCallback = refCallback;
    pConfig->pfnPostButtonEvent = pfnButton;
    pConfig->pfnPostCursorEvent = pfnCursor;
    pConfig->pfnPostWheelEvent = pfnWheel;
    pConfig->pfnEnqueueButtonEvent = pfnEnqueue;
    pConfig->pfnStartTimer = pfnStartTimer;
    pConfig->pfnStopTimer = pfnStopTimer;
    
    pConfig->dPointerSpeed = 1.0;

	pConfig->fHelperConnected = 0;
	pConfig->bChordDebounceTimerStarted = false;
    pConfig->bTiltButtonTimerStarted = false;
    pConfig->uTiltButtonState = 0;
	pConfig->uChordDebounceButton = 0;
	//&*&*&*G1_ADD
	pConfig->uCurChordEventsMask = 0;
	pConfig->bChording = false;
	//&*&*&*G2_ADD
	pConfig->curPhysButtonState = 0;
	pConfig->lastPhysButtonState = 0;
	pConfig->curButtonState = 0;
}

/*
 * Set button configuration
 */
void TBSetButtonsConfig(TBLIBCONFIG* pConfig, const TBLIBBUTTONSCONFIG2* pButtonsConfig)
{
    pConfig->buttonsConfig = *pButtonsConfig;
    DumpButtonsConfig(pConfig);

    /* HARD CODE buttons config to generate button events */
#if 0
    TBLOG(("TBSetButtonsConfig - hard coding Pan Left/Right to generate button events\n"));
    pConfig->buttonsConfig.panLeft.op = PAN_OP_GENERATE_BUTTON_EVENT;
    pConfig->buttonsConfig.panLeft.lines = 1;
    pConfig->buttonsConfig.panLeft.buttonMask = (uint32_t)0x1 << 30;
    pConfig->buttonsConfig.panRight.op = PAN_OP_GENERATE_BUTTON_EVENT;
    pConfig->buttonsConfig.panRight.lines = 1;
    pConfig->buttonsConfig.panRight.buttonMask = (uint32_t)0x1 << 31;
#endif
}

bool check_bits(uint32_t bits)
{
	// check if only one bit is set
    return bits && !(bits & (bits-1));
}

bool IschordActionOn(TBLIBCONFIG* pConfig, uint32_t button)
{
    if((button & pConfig->buttonsConfig.chordMask) == 0)
        return FALSE;
    else
        return TRUE;
#if 0
    TBLIBBUTTONACTION2 *pbuttonAction;
    
    uint32_t rmMask = pConfig->buttonsConfig.chordMask & (~button);
    TBLOG(("[TBW] IschordActionOn - (button,chordMask,rmMask) = (0x%04x, 0x%04x, 0x%04x)\n", button, pConfig->buttonsConfig.chordMask, rmMask));
    
    if(rmMask == 0)
        return FALSE;
    
    uint32_t bitmask = 0x01;
    for (size_t i = 0; i < TBLIB_MAX_BUTTONS; i++)
    {
        TBLOG(("[TBW] btnMask - 0x%04x\n", button | bitmask));
        if(check_bits(bitmask & rmMask))
        {
            pbuttonAction = GetButtonAction(pConfig, button | bitmask);
            if(pbuttonAction)
            {
                TBLOG(("[TBW] chordAction - 0x%04x\n", pbuttonAction->action));
                if(pbuttonAction->action == BUTTON_ACTION_NOACTION)
                    return FALSE;
                else
                    return TRUE;
            }
        }
        bitmask <<= 1;
    }
    
    return TRUE;
#endif
}

uint32_t GetChordButtons(TBLIBCONFIG* pConfig, uint32_t button, int *n)
{
	*n = 0;
	//if (button == 0) return 0;
	if ((button & pConfig->buttonsConfig.chordMask) == 0)
		return 0;

	for (int i = 0; i < MAX_CHORD_BUTTONS_NO; i++)
	{
		TBLOG(("[TBW] GetChordButtons - chordMsks[%d] = 0x%08x\n", i, pConfig->buttonsConfig.chordMsks[i]));
		if (pConfig->buttonsConfig.chordMsks[i] == 0)
			return 0;
		if (((button & pConfig->buttonsConfig.chordMsks[i]) > 0) &&
			((button & ~pConfig->buttonsConfig.chordMsks[i]) == 0))
		{
			TBLOG(("[TBW] GetChordButtons - chrd %d: chordButtons 0x%08x\n", i, pConfig->buttonsConfig.chordMsks[i]));
			*n = i;
			return pConfig->buttonsConfig.chordMsks[i];
		}
	}


	return 0;
}


//* Filter redundant report before call TBProcessButtonEvent()
//* Debounce timer may call TBProcessButtonEvent() with the same buttonstate
void TBProcessButtonEvent(TBLIBCONFIG* pConfig, uint32_t buttonState)
{

    TBLOG(("[TBW] \n"));
    TBLOG(("[TBW] TBProcessButtonEvent - BS: 0x%04x  last: 0x%04x  dbb: 0x%04x\n", buttonState, pConfig->lastPhysButtonState, pConfig->uChordDebounceButton));
    //DumpButtonsConfig(pConfig);
    
    pConfig->curPhysButtonState = buttonState;

	if (!ProcessChordingButtons(pConfig))
		return;
	TBLOG(("[TBW] TBProcessButtonEvent - curActionButtonState, lastActionButtonState = 0x%04x, 0x%04x", pConfig->curActionButtonState, pConfig->lastActionButtonState));
	ProcessButtonEvent(pConfig);
    pConfig->uChordDebounceButton = 0;
    return;
}


// button allocation
// bits 0 ~ 15: normal mouse buttons
// bits 16 ~ 29: chording buttons mapping
// bits 30 ~ 31: AC Pan buttons
#define ChordButtonsMappingMask  0x3fff0000u
// Only one chording-buttons can be triggered

// update ActionButtonState
// return false => ask the caller to stop further processing and return;
// return true => ask the caller to go on next process
// return value: pConfig->curActiveButtonState & lastActionButtonState
// pConfig->curPhysButtonState & pConfig->lastPhysButtonState will not be changed
bool ProcessChordingButtons(TBLIBCONFIG* pConfig)
{
	TBLOG(("[TBW] ProcessChordingButtons - buttonsConfig.chordMask = 0x%04x", pConfig->buttonsConfig.chordMask));

	if (pConfig->buttonsConfig.chordMask == 0)
	{
		pConfig->lastActionButtonState = pConfig->curActionButtonState;
		pConfig->curActionButtonState = pConfig->curPhysButtonState;
		return true;
	}

	uint32_t chordbuttons = 0;
	int		n = 0;

	if (!pConfig->bChording)
	{
		// handling chord-debouncing
		pConfig->uSupplementButtonState = 0;

		if (pConfig->uChordDebounceButton)
		{
			TBLOG(("[TBW] uChordDebounceButton = 0x%04x", pConfig->uChordDebounceButton));

			if (pConfig->bChordDebounceTimerStarted)
			{
				TBLOG(("[TBW] stop debounce timer"));
				pConfig->pfnStopTimer(pConfig->refCallback, TIMER_ID_DEBOUNCE);
				pConfig->bChordDebounceTimerStarted = false;
			}

		}
		else if (pConfig->curPhysButtonState > 0)
		{   // pConfig->uChordDebounceButton == 0  && buttonState > 0
			TBLOG(("[TBW] uChordDebounceButton = 0, (0x%04x, 0x%04x)", pConfig->curPhysButtonState, pConfig->lastPhysButtonState));

			if ((numberOfSetBits(pConfig->curPhysButtonState) == 1) && (pConfig->lastPhysButtonState == 0))
			{   // single button down
				TBLOG(("[TBW] single button down"));
				{
					if (GetChordButtons(pConfig, pConfig->curPhysButtonState, &n))
					{   // chord candidate
						TBLOG(("[TBW] chord candidate"));
						if (pConfig->pfnStartTimer && !pConfig->bChordDebounceTimerStarted)
						{
							pConfig->uChordDebounceButton = pConfig->curPhysButtonState;
							TBLOG(("[TBW] Qstarting chording debounce timer %d", pConfig->curPhysButtonState));
							pConfig->pfnStartTimer(
								pConfig->refCallback, TIMER_ID_DEBOUNCE, CHORD_DEBOUNCE_TIMEOUT);
							pConfig->bChordDebounceTimerStarted = true;
							return false; // pass this button event now
						}
					}

				}
			} // if((numberOfSetBits(pConfig->curPhysButtonState) == 1) && (pConfig->lastPhysButtonState == 0))

		}// end of if (pConfig->uChordDebounceButton)

		// checking chording event
		TBLOG(("[TBW] checking chording event"));
		if (pConfig->uChordDebounceButton > 0)
		{	// debouncing done
			TBLOG(("[TBW] pConfig->uChordDebounceButton > 0"));
			if ((pConfig->curPhysButtonState & pConfig->uChordDebounceButton) == 0)
			{	// uChordDebounceButton down and up - not chording event
				// process uChordDebounceButton down event and up event
				TBLOG(("[TBW] process uChordDebounceButton down event and up event"));
				// add down event to be processed in button down events processing
				pConfig->uSupplementButtonState = pConfig->uChordDebounceButton;
				//pConfig->uChordDebounceButton = 0;
				//pConfig->curActionButtonState = pConfig->curPhysButtonState;
				//return true;
			}
			else
			{	// uChordDebounceButton == 1, not changed
				chordbuttons = GetChordButtons(pConfig, pConfig->uChordDebounceButton, &n);
				if (numberOfSetBits(chordbuttons) > 1)
				{
					if (pConfig->curPhysButtonState == chordbuttons)
					{	// chording
						pConfig->uCurChordEventsMask = chordbuttons;
						pConfig->bChording = true;
						pConfig->lastActionButtonState = 0;  //chording precondition: == buttonstate == 0 
						// mapping chordbuttons to a button in bits 16~28  and hiding them
						pConfig->curActionButtonState = (pConfig->curPhysButtonState & ~chordbuttons) | (0x01u << (n + 16));
						TBLOG(("[TBW] chording: 0x%x", chordbuttons));
						TBLOG(("[TBW] curActionButtonState = 0x%x, last = 0x%x", pConfig->curActionButtonState, pConfig->lastActionButtonState));
						pConfig->uChordDebounceButton = 0;
						return true;
					}
				}
			}
		}
		else
		{	// pConfig->uChordDebounceButton == 0
			TBLOG(("[TBW] pConfig->uChordDebounceButton == 0"));
			if ((pConfig->lastPhysButtonState == 0) && (pConfig->curPhysButtonState > 0))
			{
				chordbuttons = GetChordButtons(pConfig, pConfig->curPhysButtonState, &n);
				TBLOG(("[TBW] chordbuttons = 0x%08x", chordbuttons));
				if (numberOfSetBits(chordbuttons) > 1)
				{
					if (pConfig->curPhysButtonState == chordbuttons)
					{
						pConfig->uCurChordEventsMask = chordbuttons;
						pConfig->bChording = true;
						pConfig->lastActionButtonState = 0;  //chording precondition: == buttonstate == 0
						// mapping chordbuttons to a button in bits 16~28  and hiding them
						pConfig->curActionButtonState = (pConfig->curPhysButtonState & ~chordbuttons) | (0x01u << (n + 16));
						TBLOG(("[TBW] uCurChordEventsMask = 0x%x", chordbuttons));
						TBLOG(("[TBW] curActionButtonState = 0x%x, last = 0x%x", pConfig->curActionButtonState, pConfig->lastActionButtonState));
						return true;
					}
				}
			}
		}
		// no chording event
		TBLOG(("[TBW] no chording event"));
		pConfig->bChording = false;
		pConfig->uChordDebounceButton = 0;
		pConfig->lastActionButtonState = pConfig->curActionButtonState;
		pConfig->curActionButtonState = pConfig->curPhysButtonState;
		return true;
		//TBLOG(("[TBW] -- ProcessChordingButtons: uCurChordEventsMask = 0x%x", pConfig->uCurChordEventsMask));
		// end of checking chording event

	}
	else
	{	// chording
		TBLOG(("[TBW] chording: uCurChordEventsMask = %d", pConfig->uCurChordEventsMask));
		if ((pConfig->curPhysButtonState & pConfig->uCurChordEventsMask) == 0)
		{	// chording buttons released
			pConfig->lastActionButtonState = pConfig->curActionButtonState;
			pConfig->curActionButtonState = pConfig->curPhysButtonState;// & ~pConfig->uCurChordEventsMask;
			// end of chording
			//stopchording = true;
			pConfig->bChording = false;
			TBLOG(("[TBW] to stop chording"));
			return true;
		}
		else
		{	// mapping chordbuttons to a button in bits 16~29  and hiding them
			pConfig->lastActionButtonState = pConfig->curActionButtonState;
			//chordbuttons = GetChordButtons(pConfig, pConfig->uCurChordEventsMask, &n);
			//pConfig->curActionButtonState = (pConfig->curPhysButtonState & ~chordbuttons) | (0x01u << (n + 16));
			pConfig->curActionButtonState = (pConfig->curActionButtonState & ChordButtonsMappingMask) | (pConfig->curPhysButtonState & (~pConfig->uCurChordEventsMask));
			return true;
		}

	}// end of if (!pConfig->bChording)

	//return true;
}

// Process ActionButtonState
void ProcessButtonEvent(TBLIBCONFIG* pConfig)
{
	uint32_t buttonclick;
	uint32_t buttonState = pConfig->curActionButtonState; //pConfig->curPhysButtonState;
	//uint32_t lastActionButtonState = pConfig->lastActionButtonState;
	uint32_t uDownMask = (pConfig->lastActionButtonState ^ buttonState) & buttonState;
	uint32_t uUpMask = (pConfig->lastActionButtonState ^ buttonState) & pConfig->lastActionButtonState;
	if (pConfig->uSupplementButtonState)
	{	// add the deferred button down
		uDownMask |= pConfig->uSupplementButtonState;
		uUpMask |= pConfig->uSupplementButtonState;
		//pConfig->uSupplementButtonState = 0;
	}
	//uUpMask |= pConfig->uChordDebounceButton;
	TBLIBBUTTONACTION2* pButtonActions[MAX_BUTTON_NO] = { 0 }; // chording mapping bits : bits 16~29
	TBLIBBUTTONACTION2	buttonActionX;
	buttonActionX.action = TBLIB_BUTTON_ACTION_X;


	TBLOG(("[TBW] ProcessButtonEvent: curPhys = 0x%x, curButton = 0x%x, lastphyButton = 0x%x", pConfig->curPhysButtonState, pConfig->curButtonState, pConfig->lastPhysButtonState));
	TBLOG(("[TBW] ProcessButtonEvent: curAction = 0x%x, lastActionButton = 0x%x, Dn = 0x%x Up = 0x%x", pConfig->curActionButtonState, pConfig->lastActionButtonState, uDownMask, uUpMask));
	TBLOG(("[TBW] ProcessButtonEvent: uCurChordEventsMask = 0x%x", pConfig->uCurChordEventsMask));

	pConfig->lastPhysButtonState = pConfig->curPhysButtonState;

	if (((uUpMask | uDownMask) == 0) && (pConfig->uTiltButtonState == 0))
	{
		TBLOG(("[TBW] ProcessButtonEvent: No action buttons' state changed"));
		return;
	}

	//&*&*&*G1_ADD_20210330
	//uint32_t chordbuttons = 0;
	uint32_t i, btnMask, btns;
//	bool	 stopchording = false;
	bool	bScrollaction = false;

	// button down events processing
	TBLOG(("[TBW] ProcessButtonEvent: process button down events 0x%08x", uDownMask));
	// get button actions
	btnMask = 0x01u;
	for (i = 0; i < MAX_BUTTON_NO; i++, btnMask <<= 1) // max number of buttons = 32
	{
		pButtonActions[i] = nullptr;
		
		if (uDownMask & btnMask)
		{
			btns = btnMask;
			if (i >= 16 && i <= 29)
			{	// mapped chording button
				if (pConfig->buttonsConfig.chordMsks[i - 16] > 0)
				{
					btns = pConfig->buttonsConfig.chordMsks[i - 16];
				}
			}

			pButtonActions[i] = GetButtonAction(pConfig, btns);
			if (pButtonActions[i] == nullptr)
			{
				buttonActionX.buttonMask = btns; // keep the action button(s)
				pButtonActions[i] = &buttonActionX;
			}
		}
			
	}
	// perform button actions
	btnMask = 0x01u;
	for (i = 0; i < MAX_BUTTON_NO; i++, btnMask <<= 1)
	{
		btns = btnMask;
		if (pButtonActions[i] != nullptr)
		{
			if (i >= 16 && i <= 29)
			{	// chording buttons
				btns = pConfig->buttonsConfig.chordMsks[i - 16];
			}

			// Driver-handled actions:
			// TBLIB_BUTTON_ACTION_SCROLL
			// TBLIB_BUTTON_ACTION_CLICK w/o modifiers
			// TBLIB_BUTTON_ACTION_DRAG w/o modifiers
			TBLOG(("[TBW] ProcessButtonEvent: pButtonAction"));
			if (pButtonActions[i]->action == TBLIB_BUTTON_ACTION_SCROLL)
			{
				TBLOG(("[TBW] ProcessButtonEvent: TakeScrollAction"));
				TakeScrollAction(pConfig, pButtonActions[i], 1);
				bScrollaction = true;
				goto nextdnbit;
			}

			buttonclick = pButtonActions[i]->u.ClickActionParams.buttonMask;

			if (pButtonActions[i]->action == TBLIB_BUTTON_ACTION_CLICK)
			{
				TBLOG(("[TBW] Button click event - 0x%x", buttonclick));
				if (pButtonActions[i]->u.ClickActionParams._modifiers > 0)
				{
					pConfig->pfnEnqueueButtonEvent(pConfig->refCallback, btns, 0);

				}
				else {
					uint32_t uDown = buttonclick;
					uint32_t uUp = 0;
					buttonclick |= pConfig->curButtonState;
					pConfig->pfnPostButtonEvent(pConfig->refCallback, buttonclick, uDown, uUp);
				}
				goto nextdnbit;
			}
			else if (pButtonActions[i]->action == TBLIB_BUTTON_ACTION_DRAG)
			{
				TBLOG(("[TBW] Button drag event - 0x%x", buttonclick));
				if (pConfig->bDragging)
				{
					buttonclick = 0;
					pConfig->draggingActionButtonState = 0;
					pConfig->bDragging = false;
				}
				else
				{
					pConfig->draggingActionButtonState = buttonclick;
					pConfig->bDragging = true;
				}


				if (pButtonActions[i]->u.ClickActionParams._modifiers > 0)
				{
					// with modifiers => handling by Helper
					pConfig->pfnEnqueueButtonEvent(pConfig->refCallback, btns, 0);
				}
				else
				{
					uint32_t uDown = buttonclick;
					uint32_t uUp = 0;
					buttonclick |= pConfig->curButtonState;
					pConfig->pfnPostButtonEvent(pConfig->refCallback, buttonclick, uDown, uUp);
				}

				TBLOG(("[TBW] Button 0x%x darg %s", buttonclick, pConfig->bDragging ? "ON" : "OFF"));

				goto nextdnbit;
			} // if (pButtonAction->action == TBLIB_BUTTON_ACTION_DRAG)

			// send button event to Helper

			TBLOG(("[TBW] ProcessButtonEvent: Button depress X event (0x%08x, 0x%08x)\n", btns, 0));
			pConfig->pfnEnqueueButtonEvent(pConfig->refCallback, btns, 0);
		nextdnbit:
			pButtonActions[i] = nullptr;
		} // if (pButtonAction[i])
	}// for (i = 0; i < MAX_BUTTON_NO; i++)


	if (!bScrollaction)
	{
		if (uDownMask)
		{
			// disable Track scroll & auto scroll
			if (pConfig->fScrollwTrackball || pConfig->fAuto)
			{
				TBLOG(("\t[TBW]ProcessButtonEvent - disable Track scroll & auto scroll\n"));
				pConfig->pfnStopTimer(pConfig->refCallback, TIMER_ID_AUTO_SCROLL);
				pConfig->bAutoScrollTimerStarted = false;
				pConfig->fScrollwTrackball = FALSE;
				pConfig->fAuto = FALSE;
			}
		}
	}// end of if (!bScrollaction)

//	if (!stopchording && pConfig->bChording)
//	{	// chording action just done
//		return;
//	}


	// process button up events
	TBLOG(("[TBW] ProcessButtonEvent: process button up events 0x%08x", uUpMask));

	// get button actions
	btnMask = 0x01u;
	for (i = 0; i < MAX_BUTTON_NO; i++, btnMask <<= 1) // max number of buttons = 32
	{
		pButtonActions[i] = nullptr;

		if (uUpMask & btnMask)
		{
			btns = btnMask;
			if (i >= 16 && i <= 29)
			{	// chording buttons
				btns = pConfig->buttonsConfig.chordMsks[i - 16];
			}

			pButtonActions[i] = GetButtonAction(pConfig, btns);
			if (pButtonActions[i] == nullptr)
			{
				buttonActionX.buttonMask = btns;
				pButtonActions[i] = &buttonActionX;
			}
		}

	}
	// perform button actions
	btnMask = 0x01u;
	for (i = 0; i < MAX_BUTTON_NO; i++, btnMask <<= 1)
	{
		if (pConfig->uSupplementButtonState)
		{
			pConfig->uSupplementButtonState = 0;
			// delay
			#ifdef __APPLE__
            TBLOG(("[TBW] IO delay......................................................."));
            //for (int i = 0; i< 1000000000; i++);
                //TBLOG(("[TBW] IO delay......................................................."));
			//IOLog("[TBW] IO delay.......................................................");
			IODelay(10000);
			#else
			//DbgPrint("[TBW] IO delay................");
			LARGE_INTEGER Timeout; // Units of 100 ns 
			Timeout.HighPart = 0;
			Timeout.LowPart = 100; // 10ms
			KeDelayExecutionThread(KernelMode, FALSE, &Timeout);
			#endif
		}
		btns = btnMask;
		if (pButtonActions[i])
		{
			if (i >= 16 && i <= 29)
			{	// chording buttons
				btns = pConfig->buttonsConfig.chordMsks[i - 16];
				pConfig->bChording = false;
				pConfig->uCurChordEventsMask = 0;
			}

			// Driver-handled actions:
			// TBLIB_BUTTON_ACTION_SCROLL
			// TBLIB_BUTTON_ACTION_CLICK w/o modifiers
			// TBLIB_BUTTON_ACTION_DRAG w/o modifiers
			TBLOG(("[TBW] ProcessButtonEvent: pButtonAction"));
			if (pButtonActions[i]->action == TBLIB_BUTTON_ACTION_SCROLL)
			{
				TBLOG(("[TBW] ProcessButtonEvent: TakeScrollAction - button up"));
				//TakeScrollAction(pConfig, pButtonAction[i], 1);
				continue;
			}

			buttonclick = pButtonActions[i]->u.ClickActionParams.buttonMask;

			if (pButtonActions[i]->action == TBLIB_BUTTON_ACTION_CLICK)
			{
				TBLOG(("[TBW] Button click event - 0x%x", buttonclick));
				if (pButtonActions[i]->u.ClickActionParams._modifiers > 0)
				{
					pConfig->pfnEnqueueButtonEvent(pConfig->refCallback, 0, btns);

				}
				else {
					uDownMask = 0;
					uUpMask = buttonclick;
					buttonclick = pConfig->curButtonState & ~buttonclick;
					pConfig->pfnPostButtonEvent(pConfig->refCallback, buttonclick, uDownMask, uUpMask);
				}
				continue;
			}
			else if (pButtonActions[i]->action == TBLIB_BUTTON_ACTION_DRAG)
			{
				//TBLOG(("[TBW] Button drag up event - 0x%x", buttonclick));
				continue;
			} // if (pButtonAction->action == TBLIB_BUTTON_ACTION_DRAG)

			// send button event to Helper

			TBLOG(("[TBW] ProcessButtonEvent: Button release X event (0x%08x, 0x%08x)\n", 0, btns));
			pConfig->pfnEnqueueButtonEvent(pConfig->refCallback, 0, btns);
		}
	}// for (i = 0; i < MAX_BUTTON_NO; i++)


	return;
}


//void TBProcessButtonSplitEvent(TBLIBCONFIG* pConfig, uint32_t uDownMask, uint32_t uUpMask)
//{

//}

void TBProcessPointerEvent(TBLIBCONFIG* pConfig, int dx, int dy)
{
    //TBLOG(( "TBProcessPointerEvent %d - %d", dx, dy);
//&*&*&*G1_DEL_20201224
//    if (dx == 0 && dy == 0)
//        return; // nothing to do
//&*&*&*G2_DEL_20201224
    
#ifdef SCROLL_WITH_TRACKBALL
	ApplyScrollwTrackball(pConfig, &dx, &dy);
#endif

    ApplySlowPointer(pConfig, &dx, &dy);
    ApplyLockedPointerAxis(pConfig, &dx, &dy);

    if (!pConfig->fSlowPointer && pConfig->dPointerSpeed != 1.0) {
        // if slow pointer is not enabled, multiply by speed factor
        //TBLOG(("\t[PCE] applying pointer speed\n"));
        dx = (int)((double)dx * pConfig->dPointerSpeed);
        dy = (int)((double)dy * pConfig->dPointerSpeed);
    }
    //TBLOG(("\t[PCE] Sending cursor event, dx: %d, dy: %d\n", dx, dy));
    pConfig->pfnPostCursorEvent(pConfig->refCallback, dx, dy);
}

void TBProcessWheelEvent(TBLIBCONFIG* pConfig, int wheel, int offset)
{
    //TBLOG(( "TBProcessWheelEvent- wheel: %d, offset: %d\n", wheel, offset);
    
    // TODO: determine correct scroll wheel, vert or horz
    TBLIBSCROLL_WHEEL_CONFIG* pScrollConfig =
        wheel == TBLIB_SCROLL_WHEEL_VERT ? &pConfig->scrollParamsVert : &pConfig->scrollParamsHorz;

#ifdef __APPLE__
    // apply scaling factor first
    offset = (int)((double)offset * pScrollConfig->fSpeed);
#endif

    // Invert scroll wheel if configured
    if (pScrollConfig->bInvert)
        offset *= -1;
    
    // essentially applies wheel move multiplication factor, aggregrates
    // total distance moved & kicks off the inertial scroll timer.
    ApplyCustomScrollConfig(pConfig, pScrollConfig, &offset);

    pConfig->pfnPostWheelEvent(pConfig->refCallback, wheel, offset);
}

void TBProcessACPanEvent(TBLIBCONFIG* pConfig, int offset)
{
    TBLOG(( "[TBW] TBProcessACPanEvent : offset: %d\n", offset));
    
    uint32_t panButtonState = offset < 0 ? PAN_LEFT_BUTTON_MASK :
        (offset > 0 ? PAN_RIGHT_BUTTON_MASK : 0);
    //uint32_t panButtonState = offset > 0 ? PAN_LEFT_BUTTON_MASK :
    //(offset < 0 ? PAN_RIGHT_BUTTON_MASK : 0);

    // It's important that we preserve the current states of other buttons
    // when we form the effective button mask to be sent to ProcessButtonEvent
    // so that any (though unlikely) button states that are pressed when the
    // pan buttons are pressed are correctly processed.
    
    // Emulate DOWN & UP events synchronously. This is okay as keeping the
    // pan button pressed, would keep generating the events and releasing
    // the button would generate a scroll event with 0 offset. And when scroll
    // offset is 0, we don't process those events.
    if(offset != 0)
    {
        // monitoring continue AC pan events to detect tilt button up
        if (pConfig->pfnStartTimer)
        {
            uint32_t timeout;
            
            if(pConfig->uTiltButtonState != panButtonState)
                timeout = TILT_BUTTON_TIMEOUT1;
            else
                timeout = TILT_BUTTON_TIMEOUT2;
            TBLOG(("[TBW] starting tilt button timer 0x%04x", timeout));
            pConfig->uTiltButtonState = panButtonState;
            pConfig->pfnStartTimer(
                pConfig->refCallback, TIMER_ID_TILT_BUTTON, timeout);
			pConfig->bTiltButtonTimerStarted = true;
            
        }
    }
    if((offset != 0) || (pConfig->lastPhysButtonState & PAN_BUTTON_STATE_MASK))
    {
        TBProcessButtonEvent(
            pConfig,
            (pConfig->lastPhysButtonState & ~PAN_BUTTON_STATE_MASK) | panButtonState);
    }else
    {
#if 0  // ignore offset == 0 events
        pConfig->lastPhysButtonState &= ~PAN_BUTTON_STATE_MASK;
        pConfig->curPhysButtonState &= ~PAN_BUTTON_STATE_MASK;
#endif
    }
}

void TBHandleTimeOut(TBLIBCONFIG* pConfig, uint32_t timerId)
{
	TBLOG(("[TBW] TBHandleTimeOut id - %d\n", timerId));

	switch(timerId)
	{
#ifdef ENABLE_CHORDING_TIMER
		case TIMER_ID_CHORD:
			HandleDetectChordsTimeOut(pConfig);
			break;
#endif
		case TIMER_ID_INERTIAL_SCROLL:
			HandleInertialScrollTimeOut(pConfig);
			break;
		case TIMER_ID_AUTO_SCROLL:
			HandleAutoScrollTimeOut(pConfig);
			break;
		case TIMER_ID_DEBOUNCE:
			HandleChordDebounceTimeOut(pConfig);
			break;
        case TIMER_ID_TILT_BUTTON:
            HandleTiltButtonTimeOut(pConfig);
			break;
		default:
			TBLOG(("[TBW] unknown timerId %d", timerId));
	}

}

uint32_t TBSetHelperConnected(TBLIBCONFIG* pConfig, uint32_t fConnected)
{
    uint32_t ret = pConfig->fHelperConnected;
    pConfig->fHelperConnected = fConnected ? 1 : 0;
    return ret;
}

void TBSetLockedPointerAxis(TBLIBCONFIG* pConfig, BOOLEAN bEnable)
{
    if (pConfig->fLockPointerAxis != bEnable)
    {
        pConfig->fLockPointerAxis = bEnable;
        pConfig->dominance = 0;
    }
}

/*
 * Return current locked pointer axis movement setting.
 */
BOOLEAN TBGetLockedPointerAxis(TBLIBCONFIG* pConfig)
{
    return pConfig->fLockPointerAxis;
}

void TBSetSlowPointer(TBLIBCONFIG* pConfig, BOOLEAN bEnable)
{
    if (pConfig->fSlowPointer != bEnable)
    {
        pConfig->fSlowPointer = bEnable;
        pConfig->remainderX = 0;
        pConfig->remainderY = 0;
    }
}

#ifdef SCROLL_WITH_TRACKBALL
void TBSetScrollwTrackball(TBLIBCONFIG* pConfig, BOOLEAN bEnable)
{
	//if (pConfig->fScrollwTrackball != bEnable)
	{
		pConfig->fScrollwTrackball = bEnable;
		pConfig->remainderX = 0;
		pConfig->remainderY = 0;
		pConfig->dominance = 0;
	}
}

BOOLEAN TBGetScrollwTrackball(TBLIBCONFIG* pConfig)
{
	return pConfig->fScrollwTrackball;
}

#endif

/*
 * Return current slow pointer movement setting.
 */
BOOLEAN TBGetSlowPointer(TBLIBCONFIG* pConfig)
{
    return pConfig->fSlowPointer;
}
/*
 2.24 * 100 = 224
 224 - (int)2.24*100 = 224-200 = 24
*/

void TBSetScrollParams(TBLIBCONFIG* pConfig, int Wheel, double fSpeed, BOOLEAN bInvert, BOOLEAN bInertial)
{
    TBLIBSCROLL_WHEEL_CONFIG* pWheelConfig =
        Wheel == TBLIB_SCROLL_WHEEL_VERT ? &pConfig->scrollParamsVert : &pConfig->scrollParamsHorz;
    
    pWheelConfig->fSpeed = fSpeed;
    pWheelConfig->bInvert = bInvert;
    pWheelConfig->bInertialScroll = bInertial;

    // TODO: check for inertial scroll timer & kill it, if active??
    TBLOG(("TBSetScrollParams - wheel: %d, speed: %d.%d, invert: %u, inertial: %u\n",
        Wheel,
        (int)fSpeed,
        (int)((fSpeed*100)-((int)fSpeed*100)),
        bInvert,
        bInertial));
}

void TBGetScrollParams(TBLIBCONFIG* pConfig, int Wheel, double* pfSpeed, BOOLEAN* pbInvert, BOOLEAN* pbInertial)
{
    TBLIBSCROLL_WHEEL_CONFIG* pWheelConfig =
        Wheel == TBLIB_SCROLL_WHEEL_VERT ? &pConfig->scrollParamsVert : &pConfig->scrollParamsHorz;

    TBLOG(("TBGetScrollParams - wheel: %d, speed: %d.%d, invert: %u, inertial: %u\n",
        pWheelConfig->Wheel,
        (int)pWheelConfig->fSpeed,
        (int)((pWheelConfig->fSpeed*100)-((int)pWheelConfig->fSpeed*100)),
        pWheelConfig->bInvert,
        pWheelConfig->bInertialScroll));

    *pfSpeed = pWheelConfig->fSpeed;
    *pbInvert = pWheelConfig->bInvert;
    *pbInertial = pWheelConfig->bInertialScroll;
}

double TBGetPointerSpeed(TBLIBCONFIG* pConfig)
{
    TBLOG(("TBgetPointerSpeed - cur speed: %.2f\n", pConfig->dPointerSpeed));
    return pConfig->dPointerSpeed;
}

void TBSetPointerSpeed(TBLIBCONFIG* pConfig, double speed)
{
    if (speed > 0)
    {
        TBLOG(("TBSetPointerSpeed - speed: %.2f\n", speed));
        pConfig->dPointerSpeed = speed;
    }
    else
    {
        TBLOG(("TBSetPointerSpeed - not setting as speed == 0\n"));
    }
}

// ************** IMPLEMENTATION HELPERS ****************** //

void SetDefaultButtonsConfig(TBLIBCONFIG* pConfig)
{
#ifdef _WIN32
    RtlZeroMemory(&pConfig->buttonsConfig, sizeof(TBLIBBUTTONSCONFIG2));
#else
    memset(&pConfig->buttonsConfig, 0, sizeof(TBLIBBUTTONSCONFIG2));
#endif
    pConfig->buttonsConfig.cbSize = sizeof(TBLIBBUTTONSCONFIG2);
    pConfig->buttonsConfig.chordMask = 0x0;
    for (size_t i = 0; i < TBLIB_MAX_BUTTONS; i++) {
        TBLIBBUTTONACTION2& btnAction = pConfig->buttonsConfig.buttons[i];
        btnAction.action = TBLIB_BUTTON_ACTION_CLICK;
        btnAction.buttonMask = (uint32_t)0x1 << i;
        btnAction.u.ClickActionParams.buttonMask = (uint32_t)0x1 << i;
    }

    // Pan Left
    {
        TBLIBBUTTONACTION2& btnAction = pConfig->buttonsConfig.buttons[TBLIB_MAX_BUTTONS];
        btnAction.action = TBLIB_BUTTON_ACTION_SCROLL;
        btnAction.buttonMask = PAN_LEFT_BUTTON_MASK;
        btnAction.u.ScrollActionParams.direction = TBLIB_SCROLL_DIRECTION_LEFT;
        btnAction.u.ScrollActionParams.lines = 1;
    }

    // Pan Right
    {
        TBLIBBUTTONACTION2& btnAction = pConfig->buttonsConfig.buttons[TBLIB_MAX_BUTTONS + 1];
        btnAction.action = TBLIB_BUTTON_ACTION_SCROLL;
        btnAction.buttonMask = PAN_RIGHT_BUTTON_MASK;
        btnAction.u.ScrollActionParams.direction = TBLIB_SCROLL_DIRECTION_RIGHT;
        btnAction.u.ScrollActionParams.lines = 1;
    }

    pConfig->buttonsConfig.nButtons= TBLIB_MAX_BUTTONS+2; // +2 to account for 2 ACPan buttons
    
    // defaults for pan left/right buttons
    /*
    pConfig->buttonsConfig.panLeft.op = PAN_OP_LEFT;
    pConfig->buttonsConfig.panRight.op = PAN_OP_RIGHT;
    */
    /*
    pConfig->buttonsConfig.panLeft.op = PAN_OP_GENERATE_BUTTON_EVENT;
    pConfig->buttonsConfig.panLeft.buttonMask = (uint32_t)0x1 << 30;
    pConfig->buttonsConfig.panRight.op = PAN_OP_GENERATE_BUTTON_EVENT;
    pConfig->buttonsConfig.panRight.buttonMask = (uint32_t)0x1 << 31;
    */
    DumpButtonsConfig(pConfig);
}


/*
*/
bool IsHelperConnected(TBLIBCONFIG* pConfig)
{
    return pConfig->fHelperConnected != 0;
}

/*
*/
#if 0
bool TranslateButtonMask(TBLIBCONFIG* pConfig, uint32_t uMaskIn, uint32_t* puMaskOut)
{
#if 1
    TBLIBBUTTONACTION2* pAction = GetButtonAction(pConfig, uMaskIn);
    if (pAction) {
        if (pAction->action == BUTTON_ACTION_CLICK) {
            *puMaskOut = pAction->u.ClickActionParams.buttonMask;
            return true;
        }
    }
#else
    for (uint32_t i=0; i<pConfig->buttonsConfig.nButtons; i++) {
        TBLIBBUTTONACTION2& btnConfig = pConfig->buttonsConfig.buttons[i];
        if (btnConfig.buttonMask == uMaskIn) {
            if (BUTTON_ACTION_CLICK == btnConfig.action) {
                *puMaskOut = btnConfig.u.ClickActionParams.buttonMask;
                return true;
            }
        }
    }
#endif
    return false;
}
#endif 

// Returns the number of set bits in a 32-bit value
// So given the following inputs, will return the corresponding
// value against it.
//
//  0x08 => 1   bit 3
//  0x0A => 2   bits 1 & 3
//  0x0B => 3   bits 0, 1 & 3
//  0x0F => 4   bits 0, 1, 2 & 3
//
uint32_t numberOfSetBits(uint32_t i)
{
	i = i - ((i >> 1) & 0x55555555);
	i = (i & 0x33333333) + ((i >> 2) & 0x33333333);
	return (((i + (i >> 4)) & 0x0F0F0F0F) * 0x01010101) >> 24;
}

#ifdef ENABLE_CHORDING_TIMER
// Returns the number of set bits in a 32-bit value
// So given the following inputs, will return the corresponding
// value against it.
//
//  0x08 => 1   bit 3
//  0x0A => 2   bits 1 & 3
//  0x0B => 3   bits 0, 1 & 3
//  0x0F => 4   bits 0, 1, 2 & 3
//
uint32_t numberOfSetBits(uint32_t i)
{
    i = i - ((i >> 1) & 0x55555555);
    i = (i & 0x33333333) + ((i >> 2) & 0x33333333);
    return (((i + (i >> 4)) & 0x0F0F0F0F) * 0x01010101) >> 24;
}

// Post the cached chording events, from which we failed to detect a
// predefined chording event, back into the system. Depending on the
// button event mask we either send it to the OS or we queue it up
// to be delivered to the helper (if one is connected).
void PostCachedChordDetectEvents(TBLIBCONFIG* pConfig)
{
    TBLOG(("PostChordDetectEvents - %d cached events to be posted\n",
        pConfig->nChordButtonEvents));

    uint32_t buttonState = 0;
    for (uint32_t i = 0; i < pConfig->nChordButtonEvents; i++)
    {
        TBLIBBUTTONEVENT* pBE = &pConfig->aChordButtonEventsCache[i];
        TBLOG(("\t[PCDE] %d. DOWN: 0x%08x, UP: 0x%08x\n",
            i+1,
            pBE->uDownMask,
            pBE->uUpMask));
        uint32_t maskOut = 0;
        if (TranslateButtonMask(
            pConfig,
            pBE->uUpMask ? pBE->uUpMask : pBE->uDownMask,
            &maskOut))
        {
            if (pBE->uUpMask)
                buttonState &= ~maskOut;
            else
                buttonState |= maskOut;

            TBLOG(("\t[PCDE] posting to system, buttonState 0x%08x\n", buttonState));
            if (pConfig->pfnPostButtonEvent)
            {
                pConfig->pfnPostButtonEvent(
                    pConfig->refCallback,
                    buttonState,
					pBE->uDownMask ? maskOut : 0,
					pBE->uUpMask ? maskOut : 0);
            }
        }
        else
        {
            // check if this key is bound to a scroll action
            TBLIBBUTTONACTION2* pButtonAction = GetButtonAction(pConfig, pBE->uDownMask);
            if (pButtonAction && pButtonAction->action == TBLIB_BUTTON_ACTION_SCROLL) {
                TBLOG(("\t[PCDE] sending scroll event\n"));
                TakeScrollAction(pConfig, pButtonAction, 1);
            } else {
                TBLOG(("\t[PCDE] sending to helper\n"));
                if (pConfig->pfnEnqueueButtonEvent)
                {
                    pConfig->pfnEnqueueButtonEvent(
                        pConfig->refCallback,
                        pBE->uDownMask,
                        pBE->uUpMask);
                }
            }
        }
    }
}
#endif
                          
#if 0
static void HandleInertialScrollUsingAccel(TBLIBCONFIG* pConfig, TBLIBSCROLL_WHEEL_CONFIG* pScrollConfig)
{
	TBLOG(("HandleInertialScrollUsingAccel - [HISTA]\n"));
	// compute velocity
	if (pScrollConfig->fVelocity == 0)
	{
		LARGE_INTEGER now;
#ifdef __APPLE__
		clock_get_uptime((uint64_t*)&now.QuadPart); // now.QuadPart is in nano seconds
#else
		// This was now.QuadPart, which works on x64, but does not on x86
		KeQueryTickCount(&now.u);   // in 100 nanoseconds
#endif

// Clock uptime units are different in OSX and Windows
// Windows stores them as 100 nanosecond values whereas OS X stores them
// as nanosecond values.
		LARGE_INTEGER elapsed;
#ifdef __APPLE__
		// absolute time elapsed in nanoseconds
		elapsed.QuadPart = (now.QuadPart - pScrollConfig->timeStart.QuadPart);
		pScrollConfig->fVelocity = (double)pScrollConfig->lTotalDistance / ((double)(elapsed.QuadPart) / (1000 * 1000 * 1000));
#else
		// absolute time elapsed in 100 nanoseconds
		elapsed.QuadPart = (now.QuadPart - pScrollConfig->timeStart.QuadPart) * KeQueryTimeIncrement();
		pScrollConfig->fVelocity = (double)pScrollConfig->lTotalDistance / ((double)(elapsed.QuadPart) / (10 * 1000 * 1000));
#endif

		// Compute velocity as wheel units per second
		//pScrollConfig->fVelocity = (double)pScrollConfig->lTotalDistance / ((double)(elapsed.QuadPart) / (10 * 1000 * 1000));

		// Reset timeStart & lTotalDistance to 0 so that if a mouse wheel
		// movement comes in while inertial wheel timer is active, velocity &
		// totalDistance will be reset from ApplyCustomScrollConfig().
		// These attributes are used only to compute the scroll velocity
		// and once that's done, they are no longer necessary.
		pScrollConfig->timeStart.QuadPart = 0;
		pScrollConfig->lTotalDistance = 0;
		pScrollConfig->nMessages = 0;

		TBLOG(("\t[HISTA] initial velocity: %f\n", pScrollConfig->fVelocity));
	}
	else
	{
		// decay velocity using the attrition factor
		pScrollConfig->fVelocity *= pScrollConfig->fAttritionFactor;
		TBLOG(("\t[HISTA] attritional velocity: %f\n", pScrollConfig->fVelocity));
	}

	//int decimal = (int)pScrollConfig->fVelocity;
	//int fraction = (int) (pScrollConfig->fVelocity * 100) - decimal * 100;
	//TBLOG(("[HIST] \tvelocity: %d.%d\n", decimal, MOD(fraction)));

	if (MOD(pScrollConfig->fVelocity) < 1)
	{
		TBLOG(("\t[HISTA] stopping timer\n"));
		//WdfTimerStop(pDevExt->timerInertialScroll, FALSE);
		pConfig->pfnStopTimer(pConfig->refCallback, TIMER_ID_INERTIAL_SCROLL);
		pScrollConfig->fVelocity = 0;
		pConfig->pCurInertialScrollConfig = NULL;
	}
	else
	{
		TBLOG(("\t[HISTA] posting inertial wheel offset: %d\n", (int)pScrollConfig->fVelocity));
		pConfig->pfnPostWheelEvent(
			pConfig->refCallback,
			pScrollConfig->Wheel,
			(int)pScrollConfig->fVelocity);
		pConfig->pfnStartTimer(pConfig->refCallback, TIMER_ID_INERTIAL_SCROLL, INERTIAL_SCROLL_TIMEOUT);
	}
}
#endif

void HandleInertialScrollUsingDistance(TBLIBCONFIG* pConfig, TBLIBSCROLL_WHEEL_CONFIG* pScrollConfig)
{
	TBLOG(("HandleInertialScrollUsingDistance - [HISTD]\n"));

	TBLOG(("\t[HISTD] lDistance: %ld, nMessages: %ld\n", pScrollConfig->lTotalDistance, pScrollConfig->nMessages));

	const uint32_t MIN_MESSAGES_FOR_INERTIAL_SCROLL = 5;
	if (pScrollConfig->nMessages < MIN_MESSAGES_FOR_INERTIAL_SCROLL)
	{
		TBLOG(("\t[HISTD] min messages for inertial scroll not received, stopping timer\n"));
		pConfig->pfnStopTimer(pConfig->refCallback, TIMER_ID_INERTIAL_SCROLL);
		pConfig->bAutoScrollTimerStarted = false;
		pScrollConfig->lTotalDistance = 0;
		pScrollConfig->nMessages = 0;
		pConfig->pCurInertialScrollConfig = NULL;
		return;
	}

	if (pScrollConfig->lScrollSpeed == 0)
	{
		// Compute scroll speed, in a scale of 100s.
		// We'll bring it down to device nominal scale in the end before posting
		// any messages to the HID queue.
		pScrollConfig->lScrollSpeed = (pScrollConfig->lTotalDistance*100) / pScrollConfig->nMessages;
		TBLOG(("\t[HISTD] scroll speed calculated: %ld\n", pScrollConfig->lScrollSpeed));
	}
	else
	{
		pScrollConfig->lScrollSpeed = (pScrollConfig->lScrollSpeed*PROGRESSION_FACTOR)/100;
		TBLOG(("\t[HISTD] decelerated scroll speed: %ld\n", pScrollConfig->lScrollSpeed));
	}

	TBLOG(("\t[HISTD] scroll speed: %ld\n", pScrollConfig->lScrollSpeed));
	if (MOD(pScrollConfig->lScrollSpeed) < 50)
	{
		TBLOG(("\t[HISTD] stopping timer\n"));
		pConfig->pfnStopTimer(pConfig->refCallback, TIMER_ID_INERTIAL_SCROLL);
		pConfig->bAutoScrollTimerStarted = false;
		pScrollConfig->lTotalDistance = 0;
		pScrollConfig->nMessages = 0;
		pScrollConfig->lScrollSpeed = 0;
	}
	else
	{
		long lSpeed = pScrollConfig->lScrollSpeed / 100;
		long lRemainder = pScrollConfig->lScrollSpeed % 100;
		if (MOD(lRemainder) >= 50)
			lSpeed = pScrollConfig->lScrollSpeed > 0 ? lSpeed+1 : lSpeed-1;
		TBLOG(("\t[TBW] posting wheel event: %ld\n", lSpeed));
		pConfig->pfnPostWheelEvent(
			pConfig->refCallback,
			pScrollConfig->Wheel,
			(int)lSpeed);
#ifdef __APPLE__ // IOTimerEventSource has no periodic function so this function should be called repeatedly
		pConfig->pfnStartTimer(pConfig->refCallback, TIMER_ID_INERTIAL_SCROLL, INERTIAL_SCROLL_TIMER_INTERVAL);
#endif
	}
}

void HandleInertialScrollTimeOut(TBLIBCONFIG* pConfig)
{
    TBLOG(("[TBW] HandleInertialScrollTimeOut - [HIST]\n"));
    TBLIBSCROLL_WHEEL_CONFIG* pScrollConfig = pConfig->pCurInertialScrollConfig;
    if (!pScrollConfig)
    {
        TBLOG(("\t[HIST] - cur TBLIBSCROLL_WHEEL_CONFIG == NULL\n"));
        return;
    }

	HandleInertialScrollUsingDistance(pConfig, pScrollConfig);
}

void HandleChordDebounceTimeOut(TBLIBCONFIG* pConfig)
{
    TBLOG(( "[TBW] HandleChordDebounceTimeOut: curPhysButtonState = 0x%x", pConfig->curPhysButtonState));

	pConfig->bChordDebounceTimerStarted = false;
	
    if (IsHelperConnected(pConfig))
    {
        TBProcessButtonEvent(pConfig, pConfig->curPhysButtonState);
    }

    
}

void HandleTiltButtonTimeOut(TBLIBCONFIG* pConfig)
{
    TBLOG(( "[TBW] HandleTiltButtonTimeOut: TiltButtonState = 0x%x", pConfig->uTiltButtonState));

    //pConfig->bChordDebounceTimerStarted = false;
    
    if (IsHelperConnected(pConfig))
    {
        // emulate tilt button up event
		uint32_t  buttonstate = pConfig->curPhysButtonState & (~pConfig->uTiltButtonState);
		pConfig->uTiltButtonState = 0;
		pConfig->bTiltButtonTimerStarted = FALSE;
        TBProcessButtonEvent(pConfig, buttonstate);
    }

    
}

#ifdef SCROLL_WITH_TRACKBALL
void HandleAutoScrollTimeOut(TBLIBCONFIG* pConfig)
{
	//TBLOG(("[TBW] HandleAutoScrollTimeOut - [HIST]\n"));
	TBLIBSCROLL_WHEEL_CONFIG* pScrollConfig = pConfig->pCurInertialScrollConfig;
	if (!pScrollConfig)
	{
		TBLOG(("\t[HIST] - cur TBLIBSCROLL_WHEEL_CONFIG == NULL\n"));
		return;
	}

	if (pConfig->fScrollwTrackball)
	{
		if (!pConfig->bTracking)
		{
			TBLOG(("[TBW] HandleAutoScrollTimeOut - %d - %d\n", pScrollConfig->Wheel, pScrollConfig->Offset));

			pConfig->pfnPostWheelEvent(
				pConfig->refCallback,
				pScrollConfig->Wheel,
				pScrollConfig->Offset);
			if (++pConfig->uScrollCounter > MAX_AUTO_SCROLL_COUNT)
			{
				pConfig->pfnStopTimer(pConfig->refCallback, TIMER_ID_AUTO_SCROLL);
				pConfig->bAutoScrollTimerStarted = false;
				pConfig->uScrollCounter = 0;
#ifdef __APPLE__ // for macOS should be return here or timer will be re-executed
                return;
#endif
			}


		}
		else
		{
			TBLOG(("[TBW] Tracking.\n"));
			pConfig->bTracking = FALSE;  // reset tracking flag
		}
	}
	else
	{
		TBLOG(("\t[TBW] stop auto scroll timer.\n"));
		pConfig->pfnStopTimer(pConfig->refCallback, TIMER_ID_AUTO_SCROLL);
		pConfig->bAutoScrollTimerStarted = false;
	}
#ifdef __APPLE__ // IOTimerEventSource has no periodic function so this function should be called repeatedly
    pConfig->pfnStartTimer(pConfig->refCallback, TIMER_ID_AUTO_SCROLL, AUTO_SCROLL_TIMER_INTERVAL);
#endif
}

#endif

void ApplyLockedPointerAxis(TBLIBCONFIG* pConfig, int* pDX, int* pDY)
{
    int dx = *pDX, dy = *pDY;

    if (!pConfig->fLockPointerAxis)
        return;

    TBLOG(("ApplyLockedPointerAxis - [ALPA]\n"));
	TBLOG(("\t[ALPA] lock pointer axis(BF), dominance: %d, (dx, dy): (%d, %d)\n", pConfig->dominance, dx, dy));
    if (MOD(dx) > MOD(dy))
    {
        if (pConfig->dominance < 10) pConfig->dominance++;
    }
    else
    {
        if (pConfig->dominance > -10) pConfig->dominance--;
    }

    if (pConfig->dominance > 0)
        dy = 0;
    else
        dx = 0;

    TBLOG(("\t[ALPA] lock pointer axis(AF), dominance: %d, (dx, dy): (%d, %d)\n", pConfig->dominance, dx, dy));
    *pDX = dx;
    *pDY = dy;
}

// Applies slow pointer logic, if it's enabled, reducing the dx & dy values by
// an appropriate factor.
void ApplySlowPointer(TBLIBCONFIG* pConfig, int* pDX, int* pDY)
{
    if (!pConfig->fSlowPointer || pConfig->uSlowdownDivider == 0)
        return;

    TBLOG(("ApplySlowPointer - [ASP]\n"));

    int dx = *pDX, dy = *pDY, slowDownDivider = (int)pConfig->uSlowdownDivider;

    dx += pConfig->remainderX;
    dy += pConfig->remainderY;

    pConfig->remainderX = (int) dx % slowDownDivider;
    pConfig->remainderY = (int) dy % slowDownDivider;

    dx /= slowDownDivider;
    dy /= slowDownDivider;

    *pDX = dx; *pDY = dy;
}

#ifdef SCROLL_WITH_TRACKBALL
// Applies scroll with trackball.
void ApplyScrollwTrackball(TBLIBCONFIG* pConfig, int* pDX, int* pDY)
{

	if (!pConfig->fScrollwTrackball || pConfig->uScrolwDivider == 0)
		return;

	//TBLOG(( "[TBW] ApplyScrollwTrackball - %d %d", *pDX, *pDY);

	TBLIBSCROLL_WHEEL_CONFIG* pScrollConfig;
	int wheel, offset;
	int dx = *pDX, dy = *pDY;
    int ScrolwDivider = max(11 - (int)pConfig->uScrolwDivider, 1);

	dx += pConfig->remainderX;
	dy += pConfig->remainderY;

	pConfig->remainderX = (int)dx % ScrolwDivider;
	pConfig->remainderY = (int)dy % ScrolwDivider;

	dx /= ScrolwDivider;
	dy /= ScrolwDivider;


	if (MOD(dx) > MOD(dy))
	{
		if (pConfig->dominance < 10) pConfig->dominance++;
	}
	else
	{
		if (pConfig->dominance > -10) pConfig->dominance--;
	}

	if (pConfig->dominance > 0)
	{
		//dy = 0;
		pScrollConfig = &pConfig->scrollParamsHorz;
		offset = dx;
		wheel = TBLIB_SCROLL_WHEEL_HORZ;
	}
	else
	{
		//dx = 0;
		pScrollConfig = &pConfig->scrollParamsVert;
		offset = dy;
		wheel = TBLIB_SCROLL_WHEEL_VERT;

	}

#ifdef __APPLE__
	// apply scaling factor first
	offset = (int)((double)offset * pScrollConfig->fSpeed);
#endif

	// Invert scroll wheel if configured
	if (!pScrollConfig->bInvert)
		offset *= -1;

	// essentially applies wheel move multiplication factor, aggregrates
	// total distance moved & kicks off the inertial scroll timer.
	//ApplyCustomScrollConfig(pConfig, pScrollConfig, &offset);

	//TBLOG((  " ApplyScrollwTrackball - %d - %d\n", wheel, offset);

	pConfig->pfnPostWheelEvent(pConfig->refCallback, wheel, offset);

	*pDX = 0; 
	*pDY = 0;

	if (pConfig->fAuto)
	{	// Auto scrolling enabled
		if (offset != 0)
		{
			pConfig->bTracking = TRUE;  // in track scrolling

			if (!pConfig->bAutoScrollTimerStarted)
			{   
				//if (abs(pConfig->dominance) > 5)
                if((pConfig->dominance > 5) || (pConfig->dominance < -5)) // *&*&*&_Va_20200523 another solution to replace abs api
				{	// start timer and configure scrolling parameters 
					pConfig->pCurInertialScrollConfig = pScrollConfig;
					pConfig->pCurInertialScrollConfig->Wheel = wheel;
					pConfig->pCurInertialScrollConfig->Offset = offset > 0? (11 - ScrolwDivider) : (ScrolwDivider - 11);	// offset;
					pConfig->pfnStartTimer(pConfig->refCallback, TIMER_ID_AUTO_SCROLL, AUTO_SCROLL_TIMER_INTERVAL);
					pConfig->bAutoScrollTimerStarted = true;
					pConfig->uScrollCounter = 0;
				}

			}
			else
			{
				if ((pConfig->pCurInertialScrollConfig->Offset * offset < 0) ||
					(pConfig->pCurInertialScrollConfig->Wheel != wheel ))
				{
					//wheel direction of offset direction changed 
					TBLOG(("[TBW] Hold auto scroll"));
					pConfig->pfnStopTimer(pConfig->refCallback, TIMER_ID_AUTO_SCROLL);
					pConfig->bAutoScrollTimerStarted = false;
					pConfig->dominance = 0;		
					//pConfig->uScrollCounter = 0;
				}
			}
		}
	}


}
#endif // SCROLL_WITH_TRACKBALL



// Apply scroll customizations such as wheel invert, inertial scroll, etc.
void ApplyCustomScrollConfig(TBLIBCONFIG* pConfig, TBLIBSCROLL_WHEEL_CONFIG* pScrollConfig, int* piWheel)
{
    if (pScrollConfig->bInertialScroll)
    {
        // Check for direction change midway through processing inertial scroll.
        // This relies on the elementary principle that XOR of two values of same
        // data types will be less than zero if they are of opposite signs.
        if (pScrollConfig->lTotalDistance != 0
            && ((*piWheel) ^ pScrollConfig->lTotalDistance) < 0)
        {
            // TBLOG(("[ACSC] scroll direction change!\n"));
            pScrollConfig->lTotalDistance = 0;
			pScrollConfig->nMessages = 0;
			pScrollConfig->lScrollSpeed = 0;
        }

        // accumulate total distance travelled
        pScrollConfig->lTotalDistance += *piWheel;
		pScrollConfig->nMessages++;
		pScrollConfig->lScrollSpeed = 0;

        //TBLOG(( "[ACSC] inertial scroll total distance: %ld, # messages: %ld", pScrollConfig->lTotalDistance, pScrollConfig->nMessages);

		pConfig->pfnStartTimer(pConfig->refCallback, TIMER_ID_INERTIAL_SCROLL, INERTIAL_SCROLL_TIMER_INTERVAL);// INERTIAL_SCROLL_TIMEOUT);
        pConfig->pCurInertialScrollConfig = pScrollConfig;
    }
}

static TBLIBBUTTONACTION2* GetButtonAction(TBLIBCONFIG* pConfig, uint32_t uMaskIn)
{
    for (uint32_t i = 0; i < pConfig->buttonsConfig.nButtons; i++) {
        TBLIBBUTTONACTION2& btnConfig = pConfig->buttonsConfig.buttons[i];
        if (btnConfig.buttonMask == uMaskIn) {
            TBLOG(("[TBW] GetButtonAction - buttonMask 0x%08x, action: %d\n", uMaskIn, btnConfig.action));
            return &btnConfig;
        }
    }
    return nullptr;
}

void DumpButtonsConfig(TBLIBCONFIG* pConfig)
{
    TBLOG(("[TBW] Buttons Config - chord mask: 0x%08x, nButtons: %d\n",
        pConfig->buttonsConfig.chordMask, pConfig->buttonsConfig.nButtons));
	TBLOG(("[TBW] Buttons Config - chord msks: 0x%08x,  0x%08x\n",
		pConfig->buttonsConfig.chordMsks[0], pConfig->buttonsConfig.chordMsks[1]));
    for (size_t i = 0; i < pConfig->buttonsConfig.nButtons; i++)
    {
        TBLIBBUTTONACTION2& btnAction = pConfig->buttonsConfig.buttons[i];
        TBLOG(("[TBW]\tButtonMask: 0x%08x, action: %d\n", btnAction.buttonMask, btnAction.action));
        if (btnAction.action == TBLIB_BUTTON_ACTION_CLICK) {
            TBLOG(("[TBW]\tCLICK - mask: 0x%04x, modifiers: 0x%04x\n",
            btnAction.u.ClickActionParams.buttonMask, btnAction.u.ClickActionParams._modifiers));
        } else if (btnAction.action == TBLIB_BUTTON_ACTION_SCROLL) {
            TBLOG(("[TBW]\tSCROLL - direction: 0x%d, lines: %d\n", btnAction.u.ScrollActionParams.direction, btnAction.u.ScrollActionParams.lines));
        } else if (btnAction.action == TBLIB_BUTTON_ACTION_DRAG) {
                TBLOG(("[TBW]\tDRAG - mask: 0x%04x, modifiers: 0x%04x\n",
                       btnAction.u.ClickActionParams.buttonMask, btnAction.u.ClickActionParams._modifiers));
            }
    }
}

/**
 * Since we need to handle Scroll from two different places, we abstract
 * this into an independent internal function.
 */
static void TakeScrollAction(TBLIBCONFIG* pConfig, TBLIBBUTTONACTION2* pButtonAction, int offset)
{
    TBLOG(( "\t[TBW][SCROLLACTION] scroll button, direction: %d\n", pButtonAction->u.ScrollActionParams.direction));
    int absOffset = MOD(offset);
    uint8_t direction = pButtonAction->u.ScrollActionParams.direction;
    int realOffset = absOffset;
    
#ifdef __APPLE__
    if (direction == TBLIB_SCROLL_DIRECTION_RIGHT || direction == TBLIB_SCROLL_DIRECTION_UP)
#else
    if (direction == TBLIB_SCROLL_DIRECTION_LEFT || direction == TBLIB_SCROLL_DIRECTION_DOWN)
#endif
        realOffset = absOffset * -1;


    //int realOffset = pButtonAction->u.ScrollActionParams.direction == TBLIB_SCROLL_DIRECTION_LEFT ? absOffset * -1 : absOffset;
    int speed = pButtonAction->u.ScrollActionParams.lines > 0 ? (int)pButtonAction->u.ScrollActionParams.lines : 1;
    realOffset *= speed;

#ifdef SCROLL_WITH_TRACKBALL
	if (direction == TBLIB_SCROLL_DIRECTION_TRACKBALL)
	{
		pConfig->fAuto = FALSE;
		pConfig->fScrollwTrackball = !pConfig->fScrollwTrackball;
		pConfig->uScrolwDivider = pButtonAction->u.ScrollActionParams.lines;
		TBLOG(( "\t[TBW][SCROLLACTION] fScrollwTrackball= %s\n", pConfig->fScrollwTrackball? "TRUE" : "FALSE"));
		return;
	}else if (direction == TBLIB_SCROLL_DIRECTION_TRACKAUTO)
	{
		pConfig->fAuto = !pConfig->fAuto;
		pConfig->fScrollwTrackball = pConfig->fAuto;
		if (pConfig->fAuto)
		{
			TBLOG(( "\t[TBW][SCROLLACTION] fAuto on\n"));
			pConfig->bTracking = FALSE;
			pConfig->uScrolwDivider = pButtonAction->u.ScrollActionParams.lines;
			pConfig->uScrollCounter = 0;
		}
		else
		{
			TBLOG(( "\t[TBW][SCROLLACTION] fAuto off\n"));
			pConfig->pfnStopTimer(pConfig->refCallback, TIMER_ID_AUTO_SCROLL);
			pConfig->bAutoScrollTimerStarted = false;
		}
		return;
	}
#endif

    pConfig->pfnPostWheelEvent(
        pConfig->refCallback,
        direction == TBLIB_SCROLL_DIRECTION_LEFT || direction == TBLIB_SCROLL_DIRECTION_RIGHT
        ? TBLIB_SCROLL_WHEEL_HORZ : TBLIB_SCROLL_WHEEL_VERT,
        realOffset);
}

