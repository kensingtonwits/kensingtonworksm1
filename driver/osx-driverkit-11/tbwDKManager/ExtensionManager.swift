//
//  ExtensionDelegate.swift

//

import Foundation
import SystemExtensions
import os.log

class ExtensionManager : NSObject, OSSystemExtensionRequestDelegate {
    
    static let shared = ExtensionManager()
    var forceReplace = false // Thanks Karabiner and 高山先生
    
    func activate(forceReplace: Bool) {
        self.forceReplace = forceReplace
        let activationRequest = OSSystemExtensionRequest.activationRequest(forExtensionWithIdentifier: "com.kensington.tbwDKDriver", queue: .main)
        
        activationRequest.delegate = self
        os_log(.info, log: .default, "ExtensionManager - activationRequest.")
        OSSystemExtensionManager.shared.submitRequest(activationRequest)
        os_log("ExtensionManager - activation of com.kensington.tbwDKDriver is requested");
    }
    
    func deactivate() {
        let request = OSSystemExtensionRequest.deactivationRequest(forExtensionWithIdentifier: "com.kensington.tbwDKDriver", queue: .main)
        request.delegate = self
        OSSystemExtensionManager.shared.submitRequest(request)
        os_log("ExtensionManager - deactivation of com.kensington.tbwDkDriver is requested");
    }
    
    func request(_ request: OSSystemExtensionRequest, actionForReplacingExtension existing: OSSystemExtensionProperties, withExtension ext: OSSystemExtensionProperties) -> OSSystemExtensionRequest.ReplacementAction {
        os_log("ExtensionManager - sysex actionForReplacingExtension %@ %@", existing, ext)
        let existingVersion = existing.bundleVersion
        let extVersion = ext.bundleVersion

        if forceReplace {
            os_log("ExtensionManager - %s will be force replaced to %s forcely", request.identifier, extVersion);
            //print("\(request.identifier) will be force replaced to \(extVersion) forcely")
            return .replace
        }

        if extVersion.compare(existingVersion, options: .numeric) == .orderedDescending {
            os_log("ExtensionManager - %s will be replaced to %s from %s", request.identifier,  extVersion, existingVersion);
            //print("\(request.identifier) will be replaced to \(extVersion) from \(existingVersion)")
            return .replace
        }

        os_log("ExtensionManager - request of %s is canceled because newer version %s is already installed", request.identifier, existingVersion);
        //print("request of \(request.identifier) is canceled because newer version (\(existingVersion)) is already installed")
        return .cancel
    }
    
    func requestNeedsUserApproval(_ request: OSSystemExtensionRequest) {
        os_log("ExtensionManager - sysex needsUserApproval")
        
    }
    
    func request(_ request: OSSystemExtensionRequest, didFinishWithResult result: OSSystemExtensionRequest.Result) {
        os_log("ExtensionManager - sysex didFinishWithResult %@", result.rawValue)
        switch result {
        case .completed:
            os_log("ExtensionManager - request of %s is completed", request.identifier)
        case .willCompleteAfterReboot:
            os_log("ExtensionManager - request of %s requires reboot", request.identifier)
            break
        @unknown default:
            break
        }
        exit(0)
    }
    
    func request(_ request: OSSystemExtensionRequest, didFailWithError error: Error) {
        os_log("ExtensionManager - sysex didFailWithError %@", error.localizedDescription)
        exit(0)
    }
}
