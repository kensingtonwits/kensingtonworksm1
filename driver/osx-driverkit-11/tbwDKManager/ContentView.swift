//
//  ContentView.swift
//  tbwDKManager
//
//  Created by ginolin on 2020/7/21.
//  Copyright © 2020 Kensington. All rights reserved.
//

import SwiftUI
import SystemExtensions

struct ContentView: View {
    var body: some View {
        VStack {
            Text("tbwDKDriver Manager")
            //HStack {
            //    Button() { // I dont need the UI!!
            //        Text("Activate")
            //    }
            //    Button() {
            //        Text("Deactivate")
            //    }
            //}
        }
        .frame(maxWidth: .infinity, maxHeight: .infinity)
    }
}


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
