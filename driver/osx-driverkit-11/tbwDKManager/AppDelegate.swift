//
//  AppDelegate.swift
//  tbwDKManager
//
//  Created by ginolin on 2020/7/23.
//  Copyright © 2020 Kensington. All rights reserved.
//

import Cocoa
import SwiftUI

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    var window: NSWindow!
    

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Create the SwiftUI view that provides the window contents.
        //NSApp.setActivationPolicy(.accessory);
        NSApp.setActivationPolicy(.prohibited); // not showing icon on the Dock
        for argument in CommandLine.arguments {
            if argument == "--install" {
                NSLog("tbwDKManagerM - I got install command!! Y");
                ExtensionManager.shared.activate(forceReplace: false)
                return
            } else if argument == "--forceActivate" {
                NSLog("tbwDKManagerM - I got forceActivate command!! Y");
                ExtensionManager.shared.activate(forceReplace: true)
                return
            } else if argument == "--uninstall" {
                NSLog("tbwDKManagerM - I got uninstall command!! Y");
                ExtensionManager.shared.deactivate()
                return
            }
        }
        /*
        let contentView = ContentView()

        // Create the window and set the content view. 
        window = NSWindow(
            contentRect: NSRect(x: 0, y: 0, width: 480, height: 300),
            styleMask: [.titled, .closable, .miniaturizable, .resizable, .fullSizeContentView],
            backing: .buffered, defer: false)
        window.center()
        window.setFrameAutosaveName("Main Window")
        window.contentView = NSHostingView(rootView: contentView)
        window.makeKeyAndOrderFront(nil)*/
        exit(0);
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

