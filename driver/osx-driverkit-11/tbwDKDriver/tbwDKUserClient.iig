#ifndef tbwDKUserClient_h
#define tbwDKUserClient_h

#include <Availability.h>
#include <DriverKit/IOUserClient.iig>
#include <DriverKit/IOTimerDispatchSource.iig>
#include "KWDriverInterface.h"
#include "tbwDKPointer.iig"
#include "tbwDKKeyboard.iig"


class tbwDKUserClient: public IOUserClient
{
    //static const IOUserClientMethodDispatch sMethods[kKWDriverSentinel];
    
public:
    virtual bool init() override;
    virtual void free() override;

    virtual kern_return_t Start(IOService* provider) override;
    virtual kern_return_t Stop(IOService* provider) override;
    virtual kern_return_t CopyClientMemoryForType(uint64_t type, uint64_t *options, IOMemoryDescriptor **memory) override;

    virtual kern_return_t ExternalMethod(uint64_t selector,
                                         IOUserClientMethodArguments* arguments,
                                         const IOUserClientMethodDispatch* dispatch,
                                         OSObject* target,
                                         void* reference) override;
    
 
    //virtual void DetectChordsHandler(OSAction * action, uint64_t time) TYPE(IOTimerDispatchSource::TimerOccurred);
    virtual void InertialScrollHandler(OSAction * action, uint64_t time) TYPE(IOTimerDispatchSource::TimerOccurred);
    virtual void AutoScrollHandler(OSAction * action, uint64_t time) TYPE(IOTimerDispatchSource::TimerOccurred);
    virtual void ChordDebounceHandler(OSAction * action, uint64_t time) TYPE(IOTimerDispatchSource::TimerOccurred);
    virtual void TiltButtonHandler(OSAction * action, uint64_t time) TYPE(IOTimerDispatchSource::TimerOccurred);
    
    
    // void notifyAsyncResult(OSAsyncReference64, IOReturn) LOCALONLY;
    //External Methods:
    IOReturn getDeviceInfo(IOUserClientMethodArguments* arguments) LOCALONLY;
    IOReturn registerHelper(IOUserClientMethodArguments* arguments) LOCALONLY;
    IOReturn deregisterHelper(IOUserClientMethodArguments* arguments) LOCALONLY;
    IOReturn getSlowPointer(IOUserClientMethodArguments* arguments) LOCALONLY;
    IOReturn setSlowPointer(IOUserClientMethodArguments* arguments) LOCALONLY;
    IOReturn setTBWSINGLEAXISMOVEMENTPARMS(IOUserClientMethodArguments* arguments) LOCALONLY;
    IOReturn getTBWSINGLEAXISMOVEMENTPARMS(IOUserClientMethodArguments* arguments) LOCALONLY;
    IOReturn setScrollWheelParams(IOUserClientMethodArguments* arguments) LOCALONLY;
    IOReturn getScrollWheelParams(IOUserClientMethodArguments* arguments) LOCALONLY;
    IOReturn getNextEventAsync(IOUserClientMethodArguments *arguments) LOCALONLY;
    IOReturn setButtonsConfig(IOUserClientMethodArguments *arguments) LOCALONLY;
    IOReturn emulateDeviceAction(IOUserClientMethodArguments *arguments) LOCALONLY;
    IOReturn getPointerParams(IOUserClientMethodArguments* arguments) LOCALONLY;
    IOReturn setPointerParams(IOUserClientMethodArguments* arguments) LOCALONLY;
    IOReturn postKeystroke(IOUserClientMethodArguments* arguments) LOCALONLY;
    IOReturn postPointerClick(IOUserClientMethodArguments* arguments) LOCALONLY;
 
    

    bool getPointer(tbwDKPointer* pointer) LOCALONLY;
    
    void postPointerEvent(int dx, int dy, uint32_t buttonState, uint32_t btnEvent) LOCALONLY;
    void postWheelEvent(int wheel, int units) LOCALONLY;
    

private:
    
    bool trackballStart(void) LOCALONLY;
    
    void enqueueTbwDriverEvent(TBWDRIVEREVENT* pEvent) LOCALONLY;
    // dequeue an event from the top of the queue and send it to
    // user mode app (if one is registered).
    void dequeueNextTbwDriverEvent(void) LOCALONLY;
    
    // sets default buttons config, which is maskIn & maskOut for all
    // buttons are the same.
    void setDefaultButtonsConfig(void) LOCALONLY;
    
    bool setDevicePointerAccelerationRate(uint64_t rate) LOCALONLY;
    
    void setDeviceParamProperties(const char * key, const char * value) LOCALONLY;
    
    //void postEvent(unsigned int keyCode, bool goingDown) LOCALONLY;
    

    

    //*** Begin callbacks for TB library ***//

    static void sTBPostButtonEvent(void* ref, uint32_t buttonState, uint32_t uDownMask, uint32_t uUpMask) LOCALONLY;
    //{ reinterpret_cast<tbwDKUserClient*>(ref)->tbPostButtonEvent(buttonState); }
    
    void tbPostButtonEvent(uint32_t buttonState) LOCALONLY;

    static void sTBPostCursorEvent(void* ref, int dx, int dy) LOCALONLY;
    //{ reinterpret_cast<tbwDKUserClient*>(ref)->tbPostCusrorEvent(dx, dy); }
    
    void tbPostCusrorEvent(int dx, int dy) LOCALONLY;

    static void sTBPostWheelEvent(void* ref, int wheel, int offset) LOCALONLY;
    //{ reinterpret_cast<tbwDKUserClient*>(ref)->tbPostWheelEvent(wheel, offset); }
    
    void tbPostWheelEvent(int wheel, int offset) LOCALONLY;

    static void sTBEnqueueButtonEvent(void* ref, uint32_t uDownState, uint32_t uUpState) LOCALONLY;
    //{ reinterpret_cast<tbwDKUserClient*>(ref)->tbEnqueueButtonEvent(uDownState, uUpState); }
    
    void tbEnqueueButtonEvent(uint32_t uDownState, uint32_t uUpState) LOCALONLY;

    static uint32_t sTBStartTimer(void* ref, uint32_t id, uint32_t timeOut) LOCALONLY;
    //{ return reinterpret_cast<tbwDKUserClient*>(ref)->tbStartTimer(id, timeOut); }
    
    uint32_t tbStartTimer(uint32_t id, uint32_t timeOut) LOCALONLY;
     
    static void sTBStopTimer(void* ref, uint32_t id) LOCALONLY;
    //{ reinterpret_cast<tbwDKUserClient*>(ref)->tbStopTimer(id); }
    
    void tbStopTimer(uint32_t id) LOCALONLY;
    
    
    
    //*** End TB library callbacks ***//
};

#endif
