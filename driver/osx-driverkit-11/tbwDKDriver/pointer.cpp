//
//  ServiceHIDPointer.cpp
//  trackball2
//
//  Created by Hariharan Mahadevan on 2019/1/25.
//  Copyright © 2019 Kensington. All rights reserved.
//

#include "pointer.hpp"
#include <os/log.h>
//#include <IOKit/hidsystem/IOHIDParameter.h>
#include "util.h"

OSDefineMetaClassAndStructors(TrackballWorks2Pointer, IOHIPointing)

TrackballWorks2Pointer* TrackballWorks2Pointer::withProvider(IOService* provider)
{
    TrackballWorks2Pointer* self = new TrackballWorks2Pointer();

    OSDictionary* dict = OSDictionary::withCapacity(1);
    dict->setObject(kIOHIDVirtualHIDevice, kOSBooleanTrue);
    dict->setObject("HIDDefaultBehavior", kOSBooleanTrue);

    if (!self || !self->init(dict))
    {
        if (self)
        {
            TBLogError("TrackballWorks2Pointer - error initializing\n");
            dict->release();
            self->release();
        }
        return NULL;
    }
    dict->release();

    // attach it ourselves
    if (!self->attach(provider))
    {
        TBLogError("TrackballWorks2Pointer - error attaching\n");
        self->release();
        return NULL;
    }

    // start it
    if (!self->start(provider))
    {
        TBLogError("TrackballWorks2Pointer - error starting\n");
        self->release();
        return NULL;
    }

    TBLogDebug("TrackballWorks2Pointer successfuly started\n");
    return self;
}

#if 0
void TrackballWorks2Pointer::free()
{
    TBLogDebug("TrackballWorks2Pointer::free\n");
    super::free();
}

void TrackballWorks2Pointer::detach(IOService* provider)
{
    TBLogDebug("TrackballWorks2Pointer::detach\n");
    super::detach(provider);
}

bool TrackballWorks2Pointer::terminate( IOOptionBits options /*= 0*/ )
{
    TBLogDebug("TrackballWorks2Pointer::terminate - entry\n");
    auto ret = super::terminate(options);
    TBLogDebug("TrackballWorks2Pointer::terminate - entry\n");
    return ret;
}

bool TrackballWorks2Pointer::finalize( IOOptionBits options )
{
    TBLogDebug("TrackballWorks2Pointer::finalize\n");
    return super::finalize(options);
}
#endif

bool TrackballWorks2Pointer::start(IOService* provider)
{
    if (!super::start(provider)) {
        return false;
    }
    _curButtonState = 0;
    return true;
}

void TrackballWorks2Pointer::stop(IOService* provider)
{
    TBLogDebug("TrackballWorks2Pointer::stop");
    super::stop(provider);
}

OSData* TrackballWorks2Pointer::copyAccelerationTable()
{
    OSObject* obj = copyProperty("HIDPointerAccelerationTable",
                       gIOServicePlane,
                       kIORegistryIterateRecursively|kIORegistryIterateParents);
    OSData* accelTable = OSDynamicCast(OSData, obj);
    if (!accelTable) {
        TBLogDebug("KWDriverHIDPointer - pointer acceleration table == NULL\n");
        OSSafeReleaseNULL(obj);
        return NULL;
    }
    TBLogDebug("KWDriverHIDPointer - returning pointer acceleration table\n");
    return accelTable;
}

IOItemCount TrackballWorks2Pointer::buttonCount()
{
    return 0x2;
}

IOFixed TrackballWorks2Pointer::resolution()
{
    return 0x1900000;
}

IOReturn TrackballWorks2Pointer::setParamProperties( OSDictionary * dict )
{
    TBLogDebug("TrackballWorks2Pointer::setParamProperties\n");
    //printDictionaryKeys(dict, "TrackballWorks2Pointer");

    return super::setParamProperties(dict);
}

void TrackballWorks2Pointer::dispatchButtonEvent(UInt32 buttonState)
{
    dispatchPointerEvent(0, 0, buttonState);
}

void TrackballWorks2Pointer::dispatchCursorEvent(int dx, int dy)
{
    dispatchPointerEvent(dx, dy, _curButtonState);
}

void TrackballWorks2Pointer::dispatchPointerEvent(int dx, int dy, UInt32 buttonState)
{
    AbsoluteTime timestamp;
    clock_get_uptime(&timestamp);

    dispatchRelativePointerEvent(dx, dy, buttonState, timestamp);
    _curButtonState = buttonState;
}

void TrackballWorks2Pointer::dispatchScrollEvent(short unitsAxis1, short unitsAxis2, short unitsAxis3)
{
    AbsoluteTime timestamp;
    clock_get_uptime(&timestamp);

    TBLogDebug("\tTrackballWorks2Pointer - wheel event axis1: %hd, axis2: %hd, axis3: %hd\n", unitsAxis1, unitsAxis2, unitsAxis3);
    dispatchScrollWheelEvent(unitsAxis1, unitsAxis2, unitsAxis3, timestamp);
}
