//
//  keymap.h
//  tbwDKManager
//
//  Created by ginolin on 2020/9/9.
//  Copyright © 2020 Kensington. All rights reserved.
//

#ifndef keymap_h
#define keymap_h

#include "usb_hid_keys.h"
#include "hid_report/keyboard_input.hpp"

 
const uint8_t keymap[] = {
    KEY_A,          // kVK_ANSI_A                   #x00
    KEY_S,          // kVK_ANSI_S                   #x01
    KEY_D,          // kVK_ANSI_D                   #x02
    KEY_F,          // kVK_ANSI_F                   #x03
    KEY_H,          // kVK_ANSI_H                   #x04
    KEY_G,          // kVK_ANSI_G                   #x05
    KEY_Z,          // kVK_ANSI_Z                   #x06
    KEY_X,          // kVK_ANSI_X                   #x07
    KEY_C,          // kVK_ANSI_C                   #x08
    KEY_V,          // kVK_ANSI_V                   #x09
    0,              // kVK_ISO_Section              #x0A
    KEY_B,          // kVK_ANSI_B                   #x0B
    KEY_Q,          // kVK_ANSI_Q                   #x0C
    KEY_W,          // kVK_ANSI_W                   #x0D
    KEY_E,          // kVK_ANSI_E                   #x0E
    KEY_R,          // kVK_ANSI_R                   #x0F
    KEY_Y,          // kVK_ANSI_Y                   #x10
    KEY_T,          // kVK_ANSI_T                   #x11
    KEY_1,          // kVK_ANSI_1                   #x12
    KEY_2,          // kVK_ANSI_2                   #x13
    KEY_3,          // kVK_ANSI_3                   #x14
    KEY_4,          // kVK_ANSI_4                   #x15
    KEY_6,          // kVK_ANSI_6                   #x16
    KEY_5,          // kVK_ANSI_5                   #x17
    KEY_EQUAL,      // VK_ANSI_Equal                #x18
    KEY_9,          // kVK_ANSI_9                   #x19
    KEY_7,          // kVK_ANSI_7                   #x1A
    KEY_MINUS,      // kVK_ANSI_Minus               #x1B
    KEY_8,          // kVK_ANSI_8                   #x1C
    KEY_0,          // kVK_ANSI_0                   #x1D
    KEY_RIGHTBRACE, // kVK_ANSI_RightBracket        #x1E
    KEY_O,          // kVK_ANSI_O                   #x1F
    KEY_U,          // kVK_ANSI_U                   #x20
    KEY_LEFTBRACE,  // kVK_ANSI_LeftBracket         #x21
    KEY_I,          // kVK_ANSI_I                   #x22
    KEY_P,          // kVK_ANSI_P                   #x23
    KEY_RETURN,     // kVK_Return                   #x24
    KEY_L,          // kVK_ANSI_L                   #x25
    KEY_J,          // kVK_ANSI_J                   #x26
    KEY_APOSTROPHE, // kVK_ANSI_Quote               #x27
    KEY_K,          // kVK_ANSI_K                   #x28
    KEY_SEMICOLON,  // kVK_ANSI_Semicolon           #x29
    KEY_BACKSLASH,  // kVK_ANSI_Backslash           #x2A
    KEY_COMMA,      // kVK_ANSI_Comma               #x2B
    KEY_SLASH,      // kVK_ANSI_Slash               #x2C
    KEY_N,          // kVK_ANSI_N                   #x2D
    KEY_M,          // kVK_ANSI_M                   #x2E
    KEY_DOT,        // kVK_ANSI_Period              #x2F
    KEY_TAB,        // kVK_Tab                      #x30
    KEY_SPACE,      // kVK_Space                    #x31
    KEY_GRAVE,      // kVK_ANSI_Grave               #x32
    KEY_DELETE,     // kVK_Delete                   #x33
    0,              // x34
    KEY_ESC,        // kVK_Escape                   #x35
    KEY_RIGHTMETA,  // kVK_RightCommand             #x36
    KEY_LEFTMETA,   // kVK_Command                  #x37
    KEY_LEFTSHIFT,  // kVK_Shift                    #x38
    KEY_CAPSLOCK,   // kVK_CapsLock                 #x39
    KEY_LEFTALT,    // kVK_Option                   #x3A
    KEY_LEFTCTRL,   // kVK_Control                  #x3B
    KEY_RIGHTSHIFT, // kVK_RightShift               #x3C
    KEY_RIGHTALT,   // kVK_RightOption              #x3D
    KEY_RIGHTCTRL,  // kVK_RightControl             #x3E
    0x03,           // kVK_Function                 #x3F
    KEY_F17,        // kVK_F17                      #x40
    KEY_KPDOT,      // kVK_ANSI_KeypadDecimal       #x41
    0,              // x42
    KEY_KPASTERISK, // kVK_ANSI_KeypadMultiply      #x43
    0,              // x44
    KEY_KPPLUS,     // kVK_ANSI_KeypadPlus          #x45
    0,              // x46
    KEY_NUMLOCK,    // kVK_ANSI_KeypadClear         #x47
    KEY_VOLUMEUP,   // kVK_VolumeUp                 #x48
    KEY_VOLUMEDOWN, // kVK_VolumeDown               #x49
    KEY_MUTE,       // kVK_Mute                     #x4A
    KEY_KPSLASH,    // kVK_ANSI_KeypadDivide        #x4B
    KEY_KPENTER,    // kVK_ANSI_KeypadEnter         #x4C
    0,              // x4D
    KEY_KPMINUS,    // kVK_ANSI_KeypadMinus         #x4E
    KEY_F18,        // kVK_F18                      #x4F
    KEY_F19,        // kVK_F19                      #x50
    KEY_KPEQUAL,    // kVK_ANSI_KeypadEquals        #x51
    KEY_KP0,        // kVK_ANSI_Keypad0             #x52
    KEY_KP1,        // kVK_ANSI_Keypad1             #x53
    KEY_KP2,        // kVK_ANSI_Keypad2             #x54
    KEY_KP3,        // kVK_ANSI_Keypad3             #x55
    KEY_KP4,        // kVK_ANSI_Keypad4             #x56
    KEY_KP5,        // kVK_ANSI_Keypad5             #x57
    KEY_KP6,        // kVK_ANSI_Keypad6             #x58
    KEY_KP7,        // kVK_ANSI_Keypad7             #x59
    KEY_F20,        // kVK_F20                      #x5A
    KEY_KP8,        // kVK_ANSI_Keypad8             #x5B
    KEY_KP9,        // kVK_ANSI_Keypad9             #x5C
    0,              // kVK_JIS_Yen                  #x5D
    0,              // kVK_JIS_Underscore           #x5E
    0,              // kVK_JIS_KeypadComma          #x5F
    KEY_F5,         // kVK_F5                       #x60
    KEY_F6,         // kVK_F6                       #x61
    KEY_F7,         // kVK_F7                       #x62
    KEY_F3,         // kVK_F3                       #x63
    KEY_F8,         // kVK_F8                       #x64
    KEY_F9,         // kVK_F9                       #x65
    0,              // kVK_JIS_Eisu                 #x66
    KEY_F11,        // kVK_F11                      #x67
    0,              // kVK_JIS_Kana                 #x68
    KEY_F13,        // kVK_F13                      #x69
    KEY_F16,        // kVK_F16                      #x6A
    KEY_F14,        // kVK_F14                      #x6B
    0,              // x6c
    KEY_F10,        // kVK_F10                      #x6D
    0,              // x6e
    KEY_F12,        // kVK_F12                      #x6F
    0,              // x70
    KEY_F15,        // kVK_F15                      #x71
    KEY_HELP,       // kVK_Help                     #x72
    KEY_HOME,       // kVK_Home                     #x73
    KEY_PAGEUP,     // kVK_PageUp                   #x74
    KEY_DELETE,     // kVK_ForwardDelete            #x75
    KEY_F4,         // kVK_F4                       #x76
    KEY_END,        // kVK_End                      #x77
    KEY_F2,         // kVK_F2                       #x78
    KEY_PAGEDOWN,   // kVK_PageDown                 #x79
    KEY_F1,         // kVK_F1                       #x7A
    KEY_LEFT,       // kVK_LeftArrow                #x7B
    KEY_RIGHT,      // kVK_RightArrow               #x7C
    KEY_DOWN,       // kVK_DownArrow                #x7D
    KEY_UP,         // kVK_UpArrow                  #x7E
};


#endif /* keymap_h */
