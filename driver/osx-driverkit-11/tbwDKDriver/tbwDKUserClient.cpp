#include "tbwDKUserClient.h"
#include "IOBufferMemoryDescriptorUtility.hpp"
#include "tbwDKKeyboard.h"
#include "tbwDKPointer.h"
#include <DriverKit/DriverKit.h>
#include <DriverKit/OSCollections.h>
#include <DriverKit/IODispatchQueue.h>
#include <HIDDriverKit/IOHIDEventServiceKeys.h>
#include <time.h>
#include "tbwDKDriver.h"
#include "KWDriverInterface.h"
#include "tbwDKUserClient_Method.h"
#include "version.hpp"
#include "hid_report/keyboard_input.hpp"
#include "hid_report/pointing_input.hpp"
#include "../../shared/trackball.h"
#include "circularqueue.h"
#include "keymap.h"
#include "tblog.h"

#define LOG_PREFIX "tbwDKUserClient " " v.1.0.0 "

#undef  super
#define super IOUserClient

namespace  {
/*
kVK_Command                   = 0x37,
kVK_Shift                     = 0x38,
kVK_CapsLock                  = 0x39,
kVK_Option                    = 0x3A,
kVK_Control                   = 0x3B,
*/

    void dumpButtonsConfig(const TBWBUTTONSCONFIG* pCfg)
    {
        TBLogDebug("Buttons config:-");
        TBLogDebug("tchordMask: 0x%08x, nButtons: %d", pCfg->chordMask, pCfg->nButtons);
        for (uint32_t i=0; i<pCfg->nButtons; i++) {
            const TBWBUTTONACTION& action = pCfg->buttons[i];
            TBLogDebug("IN Mask: 0x%08x",
                pCfg->buttons[i].buttonMask);
            if (action.action == BUTTON_ACTION_CLICK) {
                TBLogDebug("CLICK - OUT Mask: 0x%08x", action.u.ClickActionParams.buttonMask);
            } else if (action.action == BUTTON_ACTION_SCROLL) {
                TBLogDebug("SCROLL - direction: %d, lines: %d",
                    action.u.ScrollActionParams.direction,
                    action.u.ScrollActionParams.lines);
            } else if (action.action == BUTTON_ACTION_DRAG) {
                TBLogDebug("DRAG - OUT Mask: 0x%08x", action.u.ClickActionParams.buttonMask);
            } else if (action.action == BUTTON_ACTION_NOACTION) {
                TBLogDebug("NOACTION - 0x%04x", action.action);
            }
        }
    }
}


struct tbwDKUserClient_IVars {

    TBLIBCONFIG         tbwConfig;
    TBWBUTTONSCONFIG    buttonsConfig;

    //tbwDKService   *service;
    tbwDKKeyboard  *keyboard;
    tbwDKPointer   *pointer;
    uint32_t        buttonstate;
    tbwDKDriver    *driver;
    IOLock         *deviceLock;
    IODispatchQueue            *dispatchQueue;
    IOTimerDispatchSource      *timerChordDebounce;
    OSAction                   *actionChordDebounce;
    IOTimerDispatchSource      *timerTiltButton;
    OSAction                   *actionTiltButton;
    //IOTimerDispatchSource      *timerDetectChords;
    //OSAction                   *actionDetectChords;
    IOTimerDispatchSource      *timerInertialScroll;
    OSAction                   *actionInertialScroll;
    IOTimerDispatchSource      *timerAutoScroll;
    OSAction                   *actionAutoScroll;
    CircularQueue              *notifyEventQueue;
    IOBufferMemoryDescriptor   *notifyEventDescriptor;
//    IOMemoryMap                *notifyEventMap;
    OSAction                   *notifyEventAction;
    IOUserClientAsyncArgumentsArray     asyncData;

};

#define _service                (ivars->service)
#define _keyboard               (ivars->keyboard)
#define _pointer                (ivars->pointer)
#define _buttonstate            (ivars->buttonstate)
#define _driver                 (ivars->driver)
#define _deviceLock             (ivars->deviceLock)
#define _tbwConfig              (ivars->tbwConfig)
#define _buttonsConfig          (ivars->buttonsConfig)
#define _dispatchQueue          (ivars->dispatchQueue)
#define _timerChordDebounce    (ivars->timerChordDebounce)
#define _timerTiltButton        (ivars->timerTiltButton)
//#define _timerDetectChords      (ivars->timerDetectChords)
#define _timerInertialScroll    (ivars->timerInertialScroll)
#define _timerAutoScroll        (ivars->timerAutoScroll)
#define _notifyEventQueue       (ivars->notifyEventQueue)
#define _notifyEventDescriptor  (ivars->notifyEventDescriptor)
//#define _notifyEventMap         (ivars->notifyEventMap)
#define _actionChordDebounce   (ivars->actionChordDebounce)
#define _actionTiltButton       (ivars->actionTiltButton)
//#define _actionDetectChords     (ivars->actionDetectChords)
#define _actionInertialScroll   (ivars->actionInertialScroll)
#define _actionAutoScroll       (ivars->actionAutoScroll)
#define _notifyEventAction      (ivars->notifyEventAction)
#define _asyncData              (ivars->asyncData)

#define DISPATCH_TIME_FOREVER (~0ull)

bool tbwDKUserClient::init() {
  TBLogDebug(" init");
    
  if (!super::init()) {
    return false;
  }

  ivars = IONewZero(tbwDKUserClient_IVars, 1);
  if (ivars == nullptr) {
    return false;
  }

  return true;
}

void tbwDKUserClient::free() {
  TBLogDebug(" free");
    
    OSSafeReleaseNULL(_keyboard);
    OSSafeReleaseNULL(_pointer);
    OSSafeReleaseNULL(_timerChordDebounce);
    OSSafeReleaseNULL(_actionChordDebounce);
    OSSafeReleaseNULL(_timerTiltButton);
    OSSafeReleaseNULL(_actionTiltButton);
    //OSSafeReleaseNULL(_timerDetectChords);
    //OSSafeReleaseNULL(_actionDetectChords);
    OSSafeReleaseNULL(_timerInertialScroll);
    OSSafeReleaseNULL(_actionInertialScroll);
    OSSafeReleaseNULL(_timerAutoScroll);
    OSSafeReleaseNULL(_actionAutoScroll);
    OSSafeReleaseNULL(_dispatchQueue);
    OSSafeReleaseNULL(_notifyEventAction);
    OSSafeReleaseNULL(_notifyEventDescriptor);
    
    IOSafeDeleteNULL(ivars, tbwDKUserClient_IVars, 1);
#if 0
    if (_deviceLock) { // *&*&*&Va_20200902 crash when device unplugged
        IOLockFree(_deviceLock);
        _deviceLock = 0;
    }
#endif
  super::free();
}

kern_return_t IMPL(tbwDKUserClient, Start) {
    TBLogDebug(" Start");
        
    kern_return_t   ret;
  
    ret = Start(provider, SUPERDISPATCH);
    if (ret != kIOReturnSuccess) {
      TBLogDebug(" Start failed");
      return ret;
    
    }
    
    _driver = OSDynamicCast(tbwDKDriver, provider);
    
    /////////////
    memset(&_tbwConfig, 0, sizeof(TBLIBCONFIG));
    
    _deviceLock = IOLockAlloc();
    
    if (!_deviceLock) {
        TBLogDebug("failed to create deviceLock");
        goto bailout;
    }
    
    _notifyEventDescriptor = nullptr;
    _notifyEventQueue = nullptr;

     TBLogDebug(" Start 1");
    
    ret = CopyDispatchQueue("Default", &_dispatchQueue);
    
    if (ret != kIOReturnSuccess) {
        TBLogDebug("failed to do CopyDispatchQueue");
        goto bailout;
    }

    _notifyEventQueue = CircularQueue::withElementSize(sizeof(TBWDRIVEREVENT), 64);
    if (_notifyEventQueue == nullptr) {
        TBLogDebug("failed to create circular queue\n");
        goto bailout;
    }
    
    
    //TBLogDebug(" Start 2");
    // setup timers
    ret = IOTimerDispatchSource::Create(_dispatchQueue, &_timerChordDebounce);
    
    if(ret != kIOReturnSuccess) {
        TBLogDebug("failed to create timerChordDebounce : %x", ret);
            goto bailout;
    }
    
    ret = CreateActionChordDebounceHandler(sizeof(void *), &_actionChordDebounce);
    
    if(ret != kIOReturnSuccess) {
        TBLogDebug("failed to create actionChordDebounce : %x", ret);
            goto bailout;
    }
    
    ret = _timerChordDebounce->SetHandler(_actionChordDebounce);

    if(ret != kIOReturnSuccess) {
        TBLogDebug("failed to SetHandler : %x", ret);
            goto bailout;
    }
    
    ret = IOTimerDispatchSource::Create(_dispatchQueue, &_timerTiltButton);
    
    if(ret != kIOReturnSuccess) {
        TBLogDebug("failed to create timerTiltButton : %x", ret);
            goto bailout;
    }
    
    ret = CreateActionTiltButtonHandler(sizeof(void *), &_actionTiltButton);
    
    if(ret != kIOReturnSuccess) {
        TBLogDebug("failed to create actionTiltButton : %x", ret);
            goto bailout;
    }
    
    ret = _timerTiltButton->SetHandler(_actionTiltButton);

    if(ret != kIOReturnSuccess) {
        TBLogDebug("failed to SetHandler : %x", ret);
            goto bailout;
    }
    
#if 0
    ret = IOTimerDispatchSource::Create(_dispatchQueue, &_timerDetectChords);
    
    if(ret != kIOReturnSuccess) {
        TBLogDebug("failed to create _timerDetectChords : %x", ret);
            goto bailout;
    }
    
    ret = CreateActionDetectChordsHandler(sizeof(void *), &_actionDetectChords);
    
    if(ret != kIOReturnSuccess) {
        TBLogDebug("failed to create actionDetectChords : %x", ret);
            goto bailout;
    }
    
    ret = _timerDetectChords->SetHandler(_actionDetectChords);

    if(ret != kIOReturnSuccess) {
        TBLogDebug("failed to SetHandler : %x", ret);
            goto bailout;
    }
#endif
    
    //TBLogDebug(" Start 3");
    
    ret = IOTimerDispatchSource::Create(_dispatchQueue, &_timerInertialScroll);
     
     if(ret != kIOReturnSuccess) {
         TBLogDebug("failed to create _timerInertialScroll : %x", ret);
             goto bailout;
     }
    
    ret = CreateActionInertialScrollHandler(sizeof(void *), &_actionInertialScroll);
    
    if(ret != kIOReturnSuccess) {
        TBLogDebug("failed to create actionInertialScroll : %x", ret);
            goto bailout;
    }
     
     ret = _timerInertialScroll->SetHandler(_actionInertialScroll);

     if(ret != kIOReturnSuccess) {
         TBLogDebug("failed to SetHandler : %x", ret);
             goto bailout;
     }
    
    ret = IOTimerDispatchSource::Create(_dispatchQueue, &_timerAutoScroll);
     
     if(ret != kIOReturnSuccess) {
         TBLogDebug("failed to create _timerAutoScroll : %x", ret);
             goto bailout;
     }
     
    ret = CreateActionAutoScrollHandler(sizeof(void *), &_actionAutoScroll);
    
    if(ret != kIOReturnSuccess) {
        TBLogDebug("failed to create actionAutoScroll : %x", ret);
            goto bailout;
    }
    
     ret = _timerAutoScroll->SetHandler(_actionAutoScroll);

     if(ret != kIOReturnSuccess) {
         TBLogDebug("failed to SetHandler : %x", ret);
             goto bailout;
     }
    
    trackballStart();

    TBLogDebug("TBInit");
    // Initialize XPlatform Trackball library
    TBInit(
        &_tbwConfig,
        this,
        tbwDKUserClient::sTBPostButtonEvent,
        tbwDKUserClient::sTBPostCursorEvent,
        tbwDKUserClient::sTBPostWheelEvent,
        tbwDKUserClient::sTBEnqueueButtonEvent,
        tbwDKUserClient::sTBStartTimer,
        tbwDKUserClient::sTBStopTimer);

    setDefaultButtonsConfig();
 
  return kIOReturnSuccess;
    
bailout:
    //if (_timerDetectChords) _timerDetectChords->release();
    //if (_timerInertialScroll) _timerInertialScroll->release();
    //if (_timerAutoScroll) _timerAutoScroll->release();
    //_timerAutoScroll = nullptr;

    //_timerDetectChords = nullptr;
    //_timerInertialScroll = nullptr;
    //if (_notifyEventQueue) CircularQueue::destroy(_notifyEventQueue);
    //_notifyEventQueue = nullptr;
    //if (_workLoop) _workLoop->release();
    //_workLoop = nullptr;
    //if (_keyboard) { RELEASE_CHILD_DEVICE(_keyboard); }
    //if (_pointer) { RELEASE_CHILD_DEVICE(_pointer); }
    //if (_deviceLock) IOLockFree(_deviceLock);
    //_deviceLock = nullptr;
    return kIOReturnError;
}

#define ShouldCancel(s) (s ? 1 : 0)
#define DoCancel(s,h) {if(s){(s)->Cancel(h);}}

kern_return_t IMPL(tbwDKUserClient, Stop) {
    
    TBLogDebug(" Stop");
    
    kern_return_t ret = kIOReturnSuccess;
    __block _Atomic uint32_t cancelCount = 0;

    cancelCount += ShouldCancel (_timerChordDebounce);
    cancelCount += ShouldCancel (_timerTiltButton);
    //cancelCount += ShouldCancel (_timerDetectChords);
    cancelCount += ShouldCancel (_timerInertialScroll);
    cancelCount += ShouldCancel (_timerAutoScroll);

    void (^finalize)(void) = ^{
        if (__c11_atomic_fetch_sub(&cancelCount, 1U, __ATOMIC_RELAXED) <= 1) {
            IOReturn status;
            status = Stop(provider, SUPERDISPATCH);
        }
    };

    if (!cancelCount) {
        ret = Stop(provider, SUPERDISPATCH);
    } else {
        DoCancel (_timerChordDebounce, finalize);
        DoCancel (_timerTiltButton, finalize);
        //DoCancel (_timerDetectChords, finalize);
        DoCancel (_timerInertialScroll, finalize);
        DoCancel (_timerAutoScroll, finalize);
    }

  return ret;
}

kern_return_t IMPL(tbwDKUserClient, CopyClientMemoryForType) //(uint64_t type, uint64_t *options, IOMemoryDescriptor **memory)
{
    kern_return_t res;
    if (type == 0)
    {
        IOBufferMemoryDescriptor* buffer = nullptr;
        res = IOBufferMemoryDescriptor::Create(kIOMemoryDirectionInOut, 128 /* capacity */, 8 /* alignment */, &buffer);
        if (res != kIOReturnSuccess)
        {
            os_log(OS_LOG_DEFAULT, "tbwDKUserClient::CopyClientMemoryForType(): IOBufferMemoryDescriptor::Create failed: 0x%x", res);
        }
        else
        {
            _notifyEventDescriptor = buffer;
            _notifyEventDescriptor->retain();
            
            *memory = buffer; // returned with refcount 1
            TBLogDebug("CopyClientMemoryForType(): IOBufferMemoryDescriptor Created");
            
            // test
            IOAddressSegment range;
            if(buffer->GetAddressRange(&range) == kIOReturnSuccess)
            {
                TBWDRIVEREVENT *pev = (TBWDRIVEREVENT *) range.address;
                pev->time = 0x12345;
            }
             

        }
    }
    else
    {
        res = this->CopyClientMemoryForType(type, options, memory, SUPERDISPATCH);
    }
    return res;
}


bool tbwDKUserClient::trackballStart(void)
{

    //kern_return_t   ret;
    
    TBLogDebug(" trackballStart");
    
    
    // create virtual keyboard
    if (!_keyboard) {
      IOService* client;

      TBLogDebug(" create tbwDKKeyboardProperties");
      auto kr = Create(this, "tbwDKKeyboardProperties", &client);
      if (kr != kIOReturnSuccess) {
        TBLogDebug(" keyboard Create failed: 0x%x", kr);
        goto bailout;
      }

      _keyboard = OSDynamicCast(tbwDKKeyboard, client);
      if (!_keyboard) {
        client->release();
        goto bailout;
      }

    }
    
    // create virtual pointer
    if (!_pointer) {
      IOService* client;

      auto kr = Create(this, "tbwDKPointerProperties", &client);
      if (kr != kIOReturnSuccess) {
        TBLogDebug(" pointer Create failed: 0x%x", kr);
        goto bailout;
      }

      _pointer = OSDynamicCast(tbwDKPointer, client);
      if (!_pointer) {
        client->release();
        goto bailout;
      }

    }

    

    
    return true;
    
bailout:

    
        return false;
}

bool tbwDKUserClient::getPointer(tbwDKPointer* pointer)
{
    if(_pointer != nullptr)
    {
        pointer = _pointer;
        return true;
    }else
    {
        pointer = nullptr;
        return false;
    }
    
}

kern_return_t tbwDKUserClient::ExternalMethod(uint64_t selector,
                                              IOUserClientMethodArguments* arguments,
                                              const IOUserClientMethodDispatch* dispatch,
                                              OSObject* target,
                                              void* reference) {

    IOReturn kr = kIOReturnError;
    //TBLogDebug(" ExternalMethod - %lld", selector);
    
    //bool extra = false;
    
    switch(KWDriverRequestCode(selector))
    {
            
        case KWDriverRequestCode::kKWDriverGetDeviceInfo:
            //TBLogDebug(" kKWDriverGetDeviceInfo");
            kr = getDeviceInfo(arguments);
            return kr;
            
        case KWDriverRequestCode::kKWDriverRegisterHelper:
            //TBLogDebug(" kKWDriverRegisterHelper");
            kr = registerHelper(arguments);
            return kr;
 
        case KWDriverRequestCode::kKWDriverDeregisterHelper:
            //TBLogDebug(" kKWDriverRegisterHelper");
            kr = deregisterHelper(arguments);
            return kr;
            
        case KWDriverRequestCode::kKWDriverGetSlowPointer:
            //TBLogDebug(" kKWDriverGetSlowPointer");
            kr = getSlowPointer(arguments);
            return kr;
            
        case KWDriverRequestCode::kKWDriverSetSlowPointer:
            //TBLogDebug(" kKWDriverSetSlowPointer");
            kr = setSlowPointer(arguments);
            return kr;
            
        case KWDriverRequestCode::kKWDriverSetTBWSINGLEAXISMOVEMENTPARMS:
            //TBLogDebug(" kKWDriverSetTBWSINGLEAXISMOVEMENTPARMS");
            kr = setTBWSINGLEAXISMOVEMENTPARMS(arguments);
            return kr;
            
        case KWDriverRequestCode::kKWDriverGetTBWSINGLEAXISMOVEMENTPARMS:
            //TBLogDebug(" kKWDriverGetTBWSINGLEAXISMOVEMENTPARMS");
            kr = getTBWSINGLEAXISMOVEMENTPARMS(arguments);
            return kr;
            
        case KWDriverRequestCode::kKWDriverSetScrollWheelParams:
            //TBLogDebug(" kKWDriversSetScrollWheelParams");
            kr = setScrollWheelParams(arguments);
            return kr;
                        
        case KWDriverRequestCode::kKWDriverGetScrollWheelParams:
            //TBLogDebug(" kKWDriverGetScrollWheelParams");
            kr = getScrollWheelParams(arguments);
            return kr;
            
        case KWDriverRequestCode::kKWDriverGetNextEventAsync:
            //TBLogDebug(" kKWDriverGetNextEventAsync");
            kr = getNextEventAsync(arguments);
            return kr;
                        
        case KWDriverRequestCode::kKWDriverSetButtonsConfig:
            //TBLogDebug(" kKWDriverSetButtonsConfig");
            kr = setButtonsConfig(arguments);
            return kr;
            
        case KWDriverRequestCode::kKWDriverEmulateDeviceAction:
            //TBLogDebug(" kKWDriverEmulateDeviceAction");
            kr = emulateDeviceAction(arguments);
            return kr;
            
            
        case KWDriverRequestCode::kKWDriverGetPointerParams:
            //TBLogDebug(" kKWDriverGetPointerParams");
            kr = getPointerParams(arguments);
            return kr;
            
        case KWDriverRequestCode::kKWDriverSetPointerParams:
            //TBLogDebug(" kKWDriverSetPointerParams");
            kr = setPointerParams(arguments);
            return kr;
            
        case KWDriverRequestCode::kKWDriverPostKeystroke:
            //TBLogDebug(" kKWDriverPostKeystroke");
            kr = postKeystroke(arguments);
            return kr;
            
        case KWDriverRequestCode::kKWDriverPostPointerClick:
            //TBLogDebug(" kKWDriverPostPointerClick");
            kr = postPointerClick(arguments);
            return kr;
            
        default:
            //TBLogDebug(" extra methods");
            //extra = true;
            return kIOReturnUnsupported;
    }

    
    return kIOReturnBadArgument;
}


IOReturn tbwDKUserClient::getDeviceInfo(IOUserClientMethodArguments* arguments)
{
    OSData *dout;
    TBWDEVICEINFO devinfo;
    
    TBLogDebug(" getDeviceInfo ");
    
    if(_driver == nullptr)
        return kIOReturnError;
    
    _driver->getDeviceInfo(&devinfo);
    TBLogDebug("name = %{public}s", devinfo.szName);
    
    dout = OSDataCreate((void *)&devinfo, sizeof(TBWDEVICEINFO));
    arguments->structureOutput = dout;
    
    return kIOReturnSuccess;
}

IOReturn tbwDKUserClient::registerHelper(IOUserClientMethodArguments* arguments)
{
  
    TBLogDebug(" registerHelper ");
    
    //if(_tbwConfig == nullptr)
    //    return kIOReturnError;
    
    IOLockLock(_deviceLock);
    if(_tbwConfig.fHelperConnected == 1) {
        TBLogDebug(" Attempt to register helper when one is already registered! ");
        return kIOReturnBusy;
    }
    
    
    TBSetHelperConnected(&_tbwConfig, 1);

    IOLockUnlock(_deviceLock);
 
    TBLogDebug("Helper registered");
    
    return kIOReturnSuccess;
}

IOReturn tbwDKUserClient::deregisterHelper(IOUserClientMethodArguments* arguments)
{
    TBLogDebug("deregisterHelper");
    IOLockLock(_deviceLock);

    if (_tbwConfig.fHelperConnected == 1)
    {
        
        TBSetHelperConnected(&_tbwConfig, 0);
        if (_notifyEventDescriptor)
            _notifyEventDescriptor->release();
        _notifyEventDescriptor = nullptr;
        _notifyEventQueue->clear();
        
    }
    else
    {
        TBLogDebug("Helper deregistration request when it is not connected");
    }

    IOLockUnlock(_deviceLock);
    
    TBLogDebug("Helper deregistered");

    return kIOReturnSuccess;
}

// kKWDriverGetSlowPointer
//{
//    TrackballWorks2Service::sGetSlowPointer,
//    0,
//    0,
//    0,
//    sizeof(TBWSLOWPOINTERPARAMS)},
IOReturn tbwDKUserClient::getSlowPointer(IOUserClientMethodArguments* arguments)
{
    OSData *dout;
    TBWSLOWPOINTERPARAMS para = {0};
    
    para.enable = TBGetSlowPointer(&_tbwConfig);    ////_tbwConfig.fSlowPointer;
    TBLogDebug("getSlowPointer - status: %{public}s", (para.enable ? "ENABLE": "DISABLE"));

    dout = OSDataCreate((void *)&para, sizeof(TBWSLOWPOINTERPARAMS));
    arguments->structureOutput = dout;  //?? will change the setting of modifierKey
    return kIOReturnSuccess;

}

// kKWDriverSetSlowPointer
//{
//    TrackballWorks2Service::sSetSlowPointer,
//    0,
//    sizeof(TBWSLOWPOINTERPARAMS),
//    0,
//    0},

IOReturn tbwDKUserClient::setSlowPointer(IOUserClientMethodArguments* arguments)
{

    OSData *din = arguments->structureInput;
    TBWSLOWPOINTERPARAMS *ppara = (TBWSLOWPOINTERPARAMS*) din->getBytesNoCopy();
    os_log(OS_LOG_DEFAULT, "setSlowPointer - status: %s, modifier: %llu", (ppara->enable ? "ENABLE": "DISABLE"),  ppara->modifierKey);

    TBSetSlowPointer(&_tbwConfig, ppara->enable ? TRUE : FALSE);
    return kIOReturnSuccess;
}

IOReturn tbwDKUserClient::setTBWSINGLEAXISMOVEMENTPARMS(IOUserClientMethodArguments* arguments)
{
    OSData *din = arguments->structureInput;
    TBWSINGLEAXISMOVEMENTPARMS *ppara = (TBWSINGLEAXISMOVEMENTPARMS*)din->getBytesNoCopy();

    os_log(OS_LOG_DEFAULT, "setTBWSINGLEAXISMOVEMENTPARMS - enable: %llu, modifier: %llu", ppara->enable, ppara->modifierKey);

    TBSetLockedPointerAxis(&_tbwConfig, ppara->enable ? TRUE : FALSE);
    return kIOReturnSuccess;
}

IOReturn tbwDKUserClient::getTBWSINGLEAXISMOVEMENTPARMS(IOUserClientMethodArguments* arguments)
{
    OSData *dout;
    TBWSINGLEAXISMOVEMENTPARMS para = {0};
     
    para.enable = TBGetLockedPointerAxis(&_tbwConfig);
    //os_log(OS_LOG_DEFAULT, "getTBWSINGLEAXISMOVEMENTPARMS - status: %s", (para.enable ? "ENABLED" : "DISABLED"));
    
    dout = OSDataCreate((void *)&para, sizeof(TBWSINGLEAXISMOVEMENTPARMS));
    arguments->structureOutput = dout;  //?? will change the setting of modifierKey
    return kIOReturnSuccess;
}

#define SANITIZE_WHEEL_ARG(args)    if (args->wheel != WHEEL_VERT && args->wheel != WHEEL_HORZ) { \
                                        os_log(OS_LOG_DEFAULT, "Invalid scroll wheel argument: %d\n", args->wheel); \
                                        return kIOReturnBadArgument; \
                                    }

IOReturn tbwDKUserClient::setScrollWheelParams(IOUserClientMethodArguments* arguments)
{
    OSData *din = arguments->structureInput;
    TBWSCROLLWHEELPARAMS *ppara = (TBWSCROLLWHEELPARAMS*)din->getBytesNoCopy();

    SANITIZE_WHEEL_ARG(ppara)
    
    double speed = ppara->speed;
    os_log(OS_LOG_DEFAULT, " speed: %d.%d, invert: %llu, inertial: %llu\n",
        (int)speed,
        (int)((speed*100)-((int)speed*100)),
        ppara->invert,
        ppara->inertial);

    TBSetScrollParams(
        &_tbwConfig,
        ppara->wheel,
        ppara->speed,
        ppara->invert ? TRUE : FALSE,
        ppara->inertial ? TRUE : FALSE);

    return kIOReturnSuccess;
}


IOReturn tbwDKUserClient::getScrollWheelParams(IOUserClientMethodArguments* arguments)
{
    OSData *dout;
    TBWSCROLLWHEELPARAMS para;
    
    double speed = 0;
    BOOLEAN bInertial = FALSE;
    BOOLEAN bInvert = FALSE;
    TBGetScrollParams(&_tbwConfig, para.wheel, &speed, &bInvert, &bInertial);

    para.speed = speed;
    para.invert = bInvert ? 1 : 0;
    para.inertial = bInertial ? 1 : 0;

    os_log(OS_LOG_DEFAULT, " wheel: %d, speed: %F, invert: %llu, inertial: %llu\n",
           para.wheel,
           para.speed,
           para.invert,
           para.inertial
        );
    
    dout = OSDataCreate((void *)&para, sizeof(TBWSCROLLWHEELPARAMS));
    arguments->structureOutput = dout;
    
    return kIOReturnSuccess;
}


IOReturn tbwDKUserClient::getNextEventAsync(IOUserClientMethodArguments *arguments)
{
    IOReturn ret;
    
    //os_log(OS_LOG_DEFAULT, "getNextEventAsync - buf: %p - %p - %p, size: %d\n",
    //                            (void*)arguments->scalarInput[1],
    //                            (void*)arguments->scalarInput[2],
    //                            (void *)_notifyEventDescriptor,
    //                            (uint32_t)arguments->scalarInput[0]);
    
    if(_notifyEventDescriptor == 0)
        return kIOReturnError;
    
    
    if(arguments->completion)
    {
        IOLockLock(_deviceLock);
        _notifyEventAction = arguments->completion;
        _notifyEventAction->retain();
        
        /*
        if (arguments->scalarInput) {
            
            IOAddressSegment range;
            if(_notifyEventDescriptor->GetAddressRange(&range) == kIOReturnSuccess)
            {
                TBWDRIVEREVENT *pev = (TBWDRIVEREVENT *) range.address;
                os_log(OS_LOG_DEFAULT, "ev %d  %d  0x%llx ", pev->buttonDownMask, pev->buttonUpMask, pev->time);
            }
        }
         */
        dequeueNextTbwDriverEvent();
        IOLockUnlock(_deviceLock);
        ret = kIOReturnSuccess;
    }
    else
    {
        ret = kIOReturnError;
    }
    
    
    return ret;

 
}

IOReturn tbwDKUserClient::setButtonsConfig(IOUserClientMethodArguments *arguments)
{
    TBLogDebug(" setButtonsConfig");
    OSData *din = arguments->structureInput;
    TBWBUTTONSCONFIG *pButtonsConfig = (TBWBUTTONSCONFIG*)din->getBytesNoCopy();
    
    IOLockLock(_deviceLock);
    
    _buttonsConfig = *pButtonsConfig;
    TBLIBBUTTONSCONFIG2 newConfig = {0};
    newConfig.cbSize = sizeof(TBLIBBUTTONSCONFIG2);
    newConfig.chordMask = pButtonsConfig->chordMask;
    newConfig.nButtons = pButtonsConfig->nButtons;

    memcpy(newConfig.chordMsks, pButtonsConfig->chordMsks, MAX_CHORD_BUTTONS_NO * sizeof(uint32_t)); // *&*&*&V1_20210427
    for (size_t i=0; i<pButtonsConfig->nButtons; i++) {
        newConfig.buttons[i].buttonMask = pButtonsConfig->buttons[i].buttonMask;
        if (BUTTON_ACTION_CLICK == pButtonsConfig->buttons[i].action) {
            newConfig.buttons[i].action = TBLIB_BUTTON_ACTION_CLICK;
            newConfig.buttons[i].u.ClickActionParams.buttonMask = pButtonsConfig->buttons[i].u.ClickActionParams.buttonMask;
            newConfig.buttons[i].u.ClickActionParams._modifiers = pButtonsConfig->buttons[i].u.ClickActionParams._modifiers;
        } else if (BUTTON_ACTION_SCROLL == pButtonsConfig->buttons[i].action) {
            newConfig.buttons[i].action = TBLIB_BUTTON_ACTION_SCROLL;
            newConfig.buttons[i].u.ScrollActionParams.direction =
                pButtonsConfig->buttons[i].u.ScrollActionParams.direction;
            newConfig.buttons[i].u.ScrollActionParams.lines =
                pButtonsConfig->buttons[i].u.ScrollActionParams.lines;
        } else if (BUTTON_ACTION_DRAG == pButtonsConfig->buttons[i].action) {
            newConfig.buttons[i].action = TBLIB_BUTTON_ACTION_DRAG;
            newConfig.buttons[i].u.ClickActionParams.buttonMask = pButtonsConfig->buttons[i].u.ClickActionParams.buttonMask;
            newConfig.buttons[i].u.ClickActionParams._modifiers = pButtonsConfig->buttons[i].u.ClickActionParams._modifiers;
        }
    }
    TBSetButtonsConfig(&_tbwConfig, &newConfig);

    dumpButtonsConfig(pButtonsConfig);

    IOLockUnlock(_deviceLock);

    return kIOReturnSuccess;
}


IOReturn tbwDKUserClient::emulateDeviceAction(IOUserClientMethodArguments *arguments)
{
    OSData *din = arguments->structureInput;
    TBWEMULATEDEVICEACTION *pAction = (TBWEMULATEDEVICEACTION*)din->getBytesNoCopy();
    
    IOLockLock(_deviceLock);

    IOReturn ir = kIOReturnInvalid;

    if (pAction->uAction == DA_BUTTONCLICK)
    {
        if (pAction->u.ButtonClick.mask != 0)
        {
            os_log(OS_LOG_DEFAULT, "emulating button click, mask: 0x%04x\n", pAction->u.ButtonClick.mask);
            // button down
            TBProcessButtonEvent(&_tbwConfig, pAction->u.ButtonClick.mask);
            // button up
            TBProcessButtonEvent(&_tbwConfig, 0);

            ir = kIOReturnSuccess;
        }
        else
        {
            os_log(OS_LOG_DEFAULT, "bad argument - button mask == 0\n");
            ir = kIOReturnBadArgument;
        }

    }
    IOLockUnlock(_deviceLock);

    return ir;
}

IOReturn tbwDKUserClient::getPointerParams(IOUserClientMethodArguments* arguments)
{
    OSData *dout;
    TBWPOINTERPARAMS para;

    para.flags = POINTER_PARAM_SPEED|POINTER_PARAM_ACCELERATION;
    para.speed = TBGetPointerSpeed(&_tbwConfig);
    para.acceleration = 0;

    dout = OSDataCreate((void *)&para, sizeof(TBWSCROLLWHEELPARAMS));
    arguments->structureOutput = dout;
    
    return kIOReturnSuccess;
}

// kKWDriverSetPointerParams handler
IOReturn tbwDKUserClient::setPointerParams(IOUserClientMethodArguments* arguments)
{
    OSData *din = arguments->structureInput;
    TBWPOINTERPARAMS *ppara = (TBWPOINTERPARAMS*)din->getBytesNoCopy();
    
    os_log(OS_LOG_DEFAULT, "setPointerParams - flags: 0x%04x, speed: %d.%d, accel: %llu\n",
        (uint32_t)ppara->flags,
        (int)ppara->speed,
        (int)((ppara->speed*100)-((int)ppara->speed*100)),
        ppara->acceleration);

    if (ppara->flags & POINTER_PARAM_SPEED)
        TBSetPointerSpeed(&_tbwConfig, ppara->speed);
    if (ppara->flags & POINTER_PARAM_ACCELERATION)
        setDevicePointerAccelerationRate(ppara->acceleration);

    return kIOReturnSuccess;
}


IOReturn tbwDKUserClient::postKeystroke(IOUserClientMethodArguments* arguments)
{
    OSData *din = arguments->structureInput;
    TBWPOSTKEYSTROKE *p = (TBWPOSTKEYSTROKE*)din->getBytesNoCopy();
    size_t numKeys = fmin((size_t)p->count, sizeof(p->keystrokes)/sizeof(p->keystrokes[0]));
    TBLogDebug("postKeystroke numKeys = %lu", numKeys);
    
    IOReturn ret = kIOReturnError;
	
	for (size_t i=0; i<numKeys; i++)
    {
        auto& ks = p->keystrokes[i];
        TBLogDebug("#: %lu keyCode: 0x%04x, dir: %lld", i, (unsigned)ks.keyCode, ks.down); //
        
        //_keyboard->postEvent((unsigned)ks.keyCode, ks.down ? true : false);
        hid_report::keyboard_input kInput;
        
        if(ks.down != 0)
        {   size_t j = i;
            BOOLEAN bcontinue = false;
            kInput.keys.insert(keymap[ks.keyCode]);
            
            do{
                switch(keymap[ks.keyCode])
                {   case KEY_LEFTMETA:
                    case KEY_RIGHTMETA:
                    case KEY_LEFTSHIFT:
                    case KEY_RIGHTSHIFT:
                    case KEY_LEFTALT:
                    case KEY_RIGHTALT:
                    case KEY_LEFTCTRL:
                    case KEY_RIGHTCTRL:
                        if(bcontinue)
                        {
                            kInput.keys.insert(keymap[ks.keyCode]);
                            TBLogDebug("+: %lu keyCode: 0x%04x, dir: %lld", i, (unsigned)ks.keyCode, ks.down);
                            i = j;
                            bcontinue = false;
                        }
                        if(++j < numKeys)
                        {
                            ks = p->keystrokes[j];
                            if(ks.down != 0)
                                bcontinue = true;
                        }
                        break;
                    default:
                        bcontinue = false;
                        break;
                        
                }
            }while(bcontinue);
            
        }
        
        IOMemoryDescriptor* memory = nullptr;
        ret = IOBufferMemoryDescriptorUtility::createWithBytes(&kInput, sizeof(hid_report::keyboard_input), &memory);
        if (ret == kIOReturnSuccess)
        {
            ret = _keyboard->postReport(memory);
            OSSafeReleaseNULL(memory);
        }else
        {
            return ret;
        }
        
    }



    return ret;      //kIOReturnSuccess;
}

IOReturn tbwDKUserClient::postPointerClick(IOUserClientMethodArguments* arguments)
{
    OSData *din = arguments->structureInput;
    TBWPOSTPOINTERCLICK *p = (TBWPOSTPOINTERCLICK*)din->getBytesNoCopy();

    os_log(OS_LOG_DEFAULT, "postPointerClick - buttonState: 0x%x", p->button);
    
    if (!_pointer)
        return kIOReturnError;
    
    // only called by Helper (virtual button)
    //_tbwConfig.lastVirtButtonState = p->button;
    
    tbPostButtonEvent(p->button);
    
    return kIOReturnSuccess;
}

/////////////////////

void IMPL(tbwDKUserClient, ChordDebounceHandler)
{
    TBLogDebug(" ChordDebounceHandler");
    TBHandleTimeOut(&_tbwConfig, TIMER_ID_DEBOUNCE);
    
    return;
}

void IMPL(tbwDKUserClient, TiltButtonHandler)
{
    TBLogDebug(" TiltButtonHandler");
    TBHandleTimeOut(&_tbwConfig, TIMER_ID_TILT_BUTTON);
    
    return;
}

#if 0
void IMPL(tbwDKUserClient, DetectChordsHandler)
{
    TBLogDebug(" DetectChordsHandler");
    TBHandleTimeOut(&_tbwConfig, TIMER_ID_CHORD);
    
    return;
}
#endif

void IMPL(tbwDKUserClient, InertialScrollHandler)
{
    TBLogDebug(" InertialScrollHandler");
    TBHandleTimeOut(&_tbwConfig, TIMER_ID_INERTIAL_SCROLL);
    
    return;
}

void IMPL(tbwDKUserClient, AutoScrollHandler)
{
    
    os_log(OS_LOG_DEFAULT, "========" " AutoScrollHandler");
    TBHandleTimeOut(&_tbwConfig, TIMER_ID_AUTO_SCROLL);
    
    return;
}


// callbacks for TB library

void tbwDKUserClient::sTBPostButtonEvent(void* ref, uint32_t buttonState, uint32_t uDownMask, uint32_t uUpMask)
{ reinterpret_cast<tbwDKUserClient*>(ref)->tbPostButtonEvent(buttonState); }

void tbwDKUserClient::tbPostButtonEvent(uint32_t buttonState)
{
    TBLogDebug("tbPostButtonEvent buttons: 0x%x", buttonState);
	/* *&*&*&V1_20210511 The M1 double-clicking problem is gone since 11.2.3.
	With new algorithm (left key and right key keeps their own functions), if below method (post button
	event by original driver.cpp) is used, double click will be abnormal (first up event and second 
	up event almost happen at the same time when chording keys are assigned.
	We got to go back the origin method.
	*/
#if 0 // for fixing Apple M1 chip compatibility issue of Double Click invalid
    //_buttonstate = buttonState;
    _tbwConfig.curButtonState = buttonState;  // report to system
	_driver->postButtonEvent(0, 0, buttonState, 0, false);
    return;
#else
	
    hid_report::pointing_input report;

    report.x = 0;
    report.y = 0;
    report.buttons.set_buttons(buttonState);
    _buttonstate = buttonState;
    
    _tbwConfig.curButtonState = buttonState;
    
    IOMemoryDescriptor* memory = nullptr;

    auto kr = IOBufferMemoryDescriptorUtility::createWithBytes(&report, sizeof(hid_report::pointing_input), &memory);
    if (kr == kIOReturnSuccess) {
      kr = _pointer->postReport(memory);
      OSSafeReleaseNULL(memory);
    }

#endif

    
}

void tbwDKUserClient::sTBPostCursorEvent(void* ref, int dx, int dy)
{ reinterpret_cast<tbwDKUserClient*>(ref)->tbPostCusrorEvent(dx, dy); }

void tbwDKUserClient::tbPostCusrorEvent(int dx, int dy)
{
    
    TBLogDebug(" tbPostCusrorEvent (%d, %d)", dx, dy);
    hid_report::pointing_input report;

    report.x = dx;  // >> 16;
    report.y = dy;  // >> 16;
    report.buttons.set_buttons(_buttonstate);
    
    IOMemoryDescriptor* memory = nullptr;
    
    auto kr = IOBufferMemoryDescriptorUtility::createWithBytes(&report, sizeof(hid_report::pointing_input), &memory);
   
    if (kr == kIOReturnSuccess) {
      kr = _pointer->postReport(memory);
      OSSafeReleaseNULL(memory);
    }
}

void tbwDKUserClient::sTBPostWheelEvent(void* ref, int wheel, int offset)
{ reinterpret_cast<tbwDKUserClient*>(ref)->tbPostWheelEvent(wheel, offset); }

void tbwDKUserClient::tbPostWheelEvent(int wheel, int offset)
{
    TBLogDebug("tbPostWheelEvent wheel (%d, %d)", wheel, offset);
    
    hid_report::pointing_input report;

    if (wheel == TBLIB_SCROLL_WHEEL_VERT)
    {
        report.horizontal_wheel = 0;
        report.vertical_wheel = (offset > 0)? 1 : -1;  //offset; //workaround for speed
    }
    else if (wheel == TBLIB_SCROLL_WHEEL_HORZ)
        {
            report.horizontal_wheel =  (offset > 0)? 1 : -1;   //offset; //workaround for speed
            report.vertical_wheel = 0;
        }
          
    //workaround for speed
    IOMemoryDescriptor* memory = nullptr;
    auto kr = IOBufferMemoryDescriptorUtility::createWithBytes(&report, sizeof(hid_report::pointing_input), &memory);

    for(int i = 0; i < abs(offset); i++)
    {
        if (kr == kIOReturnSuccess) {
           _pointer->postReport(memory);
        }
    }
    if(kr)
        OSSafeReleaseNULL(memory);
}

void tbwDKUserClient::sTBEnqueueButtonEvent(void* ref, uint32_t uDownState, uint32_t uUpState)
{ reinterpret_cast<tbwDKUserClient*>(ref)->tbEnqueueButtonEvent(uDownState, uUpState); }

void tbwDKUserClient::tbEnqueueButtonEvent(uint32_t uDownState, uint32_t uUpState)
{
    //LOG_FUNCNAME();
    TBLogDebug("tbEnqueueButtonEvent Dn = 0x%x, Up = 0x%x\n", uDownState, uUpState);
    TBWDRIVEREVENT de = {0};
    de.cb = sizeof(de);
    de.flags = TBW_BUTTON_EVENT;
    de.buttonDownMask = uDownState;
    de.buttonUpMask = uUpState;
    enqueueTbwDriverEvent(&de);
}

uint32_t tbwDKUserClient::sTBStartTimer(void* ref, uint32_t id, uint32_t timeOut)
{ return reinterpret_cast<tbwDKUserClient*>(ref)->tbStartTimer(id, timeOut); }

uint32_t tbwDKUserClient::tbStartTimer(uint32_t timerId, uint32_t timeOutMS)
{
    TBLogDebug("tbStartTimer - %d", timerId);
    kern_return_t   ret = kIOReturnSuccess;
    //auto deadline = mach_absolute_time() + timeOutMS * kMillisecondScale;
	uint64_t currentTime = clock_gettime_nsec_np(CLOCK_MONOTONIC_RAW);
    
    switch (timerId) {
        case TIMER_ID_CHORD:
            //ret = _timerDetectChords->WakeAtTime(0, deadline, 0);
            break;

        case TIMER_ID_INERTIAL_SCROLL:
            //ret = _timerInertialScroll->WakeAtTime(0, deadline, 0);
            ret = _timerInertialScroll->WakeAtTime(kIOTimerClockMonotonicRaw, currentTime + timeOutMS*kMillisecondScale, 100000000/10);
            break;
        case TIMER_ID_AUTO_SCROLL:
            _tbwConfig.bAutoScrollTimerStarted = TRUE;
            //ret = _timerAutoScroll->WakeAtTime(0, deadline, 0);
            ret = _timerAutoScroll->WakeAtTime(kIOTimerClockMonotonicRaw, currentTime + timeOutMS*kMillisecondScale, 100000000/10);
            break;
        case TIMER_ID_DEBOUNCE:
            _tbwConfig.bChordDebounceTimerStarted = TRUE;
            //ret = _timerChordDebounce->WakeAtTime(0, deadline, 0);
            //ret = _timerChordDebounce->WakeAtTime(kIOTimerClockMonotonicRaw, deadline, 2000000000);
             _timerChordDebounce->WakeAtTime(kIOTimerClockMonotonicRaw, currentTime + timeOutMS*kMillisecondScale, 100000000/10);
            break;
        case TIMER_ID_TILT_BUTTON:
            _tbwConfig.bTiltButtonTimerStarted = TRUE;
            //ret = _timerTiltButton->WakeAtTime(0, deadline, 0);
            ret = _timerTiltButton->WakeAtTime(kIOTimerClockMonotonicRaw, currentTime + timeOutMS*kMillisecondScale, 100000000/10);
            break;
            
    }
    if (ret != kIOReturnSuccess) {
        os_log(OS_LOG_DEFAULT, " WakeAtTime error");
        return 0;
    }
    
    return timerId;
}


void tbwDKUserClient::sTBStopTimer(void* ref, uint32_t id)
{ reinterpret_cast<tbwDKUserClient*>(ref)->tbStopTimer(id); }

void tbwDKUserClient::tbStopTimer(uint32_t timerId)
{

    TBLogDebug(" tbStopTimer - %d", timerId);
    kern_return_t   ret = kIOReturnSuccess;
    
    switch (timerId)
    {
        case TIMER_ID_CHORD:
            //ret = _timerDetectChords->WakeAtTime(0, DISPATCH_TIME_FOREVER, 0);
            break;
        case TIMER_ID_INERTIAL_SCROLL:
            ret = _timerInertialScroll->WakeAtTime(0, DISPATCH_TIME_FOREVER, 0);
            break;
        case TIMER_ID_AUTO_SCROLL: // vannes20200519
            _tbwConfig.bAutoScrollTimerStarted = FALSE;
            ret = _timerAutoScroll->WakeAtTime(0, DISPATCH_TIME_FOREVER, 0);
            break;
        case TIMER_ID_DEBOUNCE:
            _tbwConfig.bChordDebounceTimerStarted = FALSE;
            ret = _timerChordDebounce->WakeAtTime(0, DISPATCH_TIME_FOREVER, 0);
            break;
        case TIMER_ID_TILT_BUTTON:
            _tbwConfig.bTiltButtonTimerStarted = FALSE;
            ret = _timerTiltButton->WakeAtTime(0, DISPATCH_TIME_FOREVER, 0);
            break;
    }
    if (ret != kIOReturnSuccess) {
        os_log(OS_LOG_DEFAULT, "tbStopTimer WakeAtTime:0x%x", ret);
    }
}

// end TB library callbacks


// internal method -- mutex is expected to have been acquired!
void tbwDKUserClient::enqueueTbwDriverEvent(TBWDRIVEREVENT* pEvent) // called by button-event
{
    os_log(OS_LOG_DEFAULT,  "enqueueTbwDriverEvent - DownMask: 0x%04x, UpMask: 0x%04x\n", pEvent->buttonDownMask, pEvent->buttonUpMask);
    //if (_user == nullptr) {
    //    TBLogDebug("tbwDKService::enqueueTbwDriverEvent - userClient == NULL\n");
    //    return;
    //}
  
    _notifyEventQueue->enqueue((void*)pEvent);

    // try and dequeue next event from the top of the queue
    dequeueNextTbwDriverEvent();
}

// internal method -- mutex is expected to have been acquired!
void tbwDKUserClient::dequeueNextTbwDriverEvent()
{
    TBLogDebug(" dequeueNextTbwDriverEvent");

     
    if(!_notifyEventAction || !_notifyEventDescriptor)
        return;
    
        
    TBWDRIVEREVENT ev = {0};
    if (_notifyEventQueue->dequeue((void*)&ev)) {
        // event was unqueued, post it to the userClient buffer
        os_log(OS_LOG_DEFAULT,  "dequeueNextTbwDriverEvent : down 0x%x, up 0x%x, flag 0x%x ", ev.buttonDownMask, ev.buttonUpMask, ev.flags);

        // copy event into shared buffer
             
        IOAddressSegment range;
        if(_notifyEventDescriptor->GetAddressRange(&range) == kIOReturnSuccess)
        {
            TBWDRIVEREVENT *pev = (TBWDRIVEREVENT*) range.address;
            memcpy(pev, &ev, sizeof(TBWDRIVEREVENT));
        }

        //os_log(OS_LOG_DEFAULT, "dequeueNextTbwDriverEvent - notifying async result");
        
        AsyncCompletion(_notifyEventAction, kIOReturnSuccess, _asyncData, 2);
        
    } else {
        //os_log(OS_LOG_DEFAULT, "dequeueNextTbwDriverEvent - failed dequeue event from circular queue\n");
    }
    
}


void tbwDKUserClient::setDefaultButtonsConfig()
{
    TBLogDebug("setDefaultButtonsConfig\n");
    
        memset(&_buttonsConfig, 0, sizeof(TBWBUTTONSCONFIG));
        _buttonsConfig.cbSize = sizeof(TBWBUTTONSCONFIG);
        _buttonsConfig.chordMask = 0x0;
        const size_t MAX_BUTTONS = 5;
        for (size_t i=0; i<MAX_BUTTONS; i++) {
            TBWBUTTONACTION& btnAction = _buttonsConfig.buttons[i];
            btnAction.action = BUTTON_ACTION_CLICK;
            btnAction.buttonMask =
            btnAction.u.ClickActionParams.buttonMask = (uint32_t)0x1 << i;
        }
        // ACPan Left
        {
            TBWBUTTONACTION& btnAction = _buttonsConfig.buttons[MAX_BUTTONS];
            btnAction.action = BUTTON_ACTION_SCROLL;
            btnAction.u.ScrollActionParams.direction = SCROLL_DIRECTION_LEFT;
            btnAction.u.ScrollActionParams.lines = 1;
        }
    
        // ACPan Right
        {
            TBWBUTTONACTION& btnAction = _buttonsConfig.buttons[MAX_BUTTONS+1];
            btnAction.action = BUTTON_ACTION_SCROLL;
            btnAction.u.ScrollActionParams.direction = SCROLL_DIRECTION_RIGHT;
            btnAction.u.ScrollActionParams.lines = 1;
        }
        _buttonsConfig.nButtons = MAX_BUTTONS+1;
    
}

bool tbwDKUserClient::setDevicePointerAccelerationRate(uint64_t rate)
{
    
    bool ret = false;
    OSDictionary* dict = OSDictionary::withCapacity(1);
    OSNumber* value = OSNumber::withNumber(rate, 64);

    if (dict && value)
    {
        os_log(OS_LOG_DEFAULT, " setting device accel rate to: %llu\n", rate);
        
        
        dict->setObject("HIDMouseAcceleration", value);
        SetProperties(dict);
        
            ret = true;
        
    }
    OSSafeReleaseNULL(value);
    OSSafeReleaseNULL(dict);

    
    return ret;
}

//void tbwDKUserClient::postEvent(unsigned int keyCode, bool goingDown)
//{
    
//}


void tbwDKUserClient::setDeviceParamProperties(const char * key, const char * value)
{


//exit:
    //OSSafeReleaseNULL(properties);
}

void tbwDKUserClient::postPointerEvent(int dx, int dy, uint32_t buttonState, uint32_t btnEvent)
{
    if (!_pointer)
        return;

    TBLogDebug("[TBW] tbwDKUserClient::postPointerEvent button state 0x%x %d", buttonState, btnEvent);
    IOLockLock(_deviceLock);
    TBProcessPointerEvent(&_tbwConfig, dx, dy);
    if(btnEvent)
    {
    	TBProcessButtonEvent(&_tbwConfig, buttonState);
    }
    IOLockUnlock(_deviceLock);
}

/**
 * wheel == 1 => vertical scroll
 * wheel == 2 => horizontal scroll
 */
void tbwDKUserClient::postWheelEvent(int wheel, int units)
{
    if (!_pointer)
        return;

    // validate wheel argument
    if (wheel != TBLIB_SCROLL_WHEEL_VERT && wheel != TBLIB_SCROLL_WHEEL_HORZ)
        return;
    
    //TBLogDebug("postWheelEvent wheel: 0x%x : %d", wheel, units);

    IOLockLock(_deviceLock);
    if (wheel == TBLIB_SCROLL_WHEEL_HORZ) {
        TBProcessACPanEvent(&_tbwConfig, units);
    } else {
        TBProcessWheelEvent(
            &_tbwConfig,
            TBLIB_SCROLL_WHEEL_VERT,
            units);
    }
    IOLockUnlock(_deviceLock);
}
