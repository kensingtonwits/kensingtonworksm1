#!/bin/bash

# check for root privileges
if [ "$EUID" -ne 0 ]; then
	echo "Please run as root"
    exit
fi

cp -R ./build/Debug/trackballworks2.kext /Library/Extensions
if [ $? -ne 0 ]; then
    exit 1
fi

# refresh kext cache
kextcache -i /
if [ $? -ne 0 ]; then
    exit 1
fi
echo "Driver copied and kext cache refreshed!"

