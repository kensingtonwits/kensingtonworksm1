#!/bin/bash
#
# To be run from ${workspaceFolder} and expects the pacakge contents
# to be stored as per the following folder structure:-
#
#   ${workspaceFolder}
#     +- osx
#        +- package
#           +- content
#              +- Library
#              |  +- Extensions
#              |     +- trackballworks2.kext
#              +- usr
#                 +-local
#                     +- bin
#                        +- trackballworks2agent
#

if [ ! -d ./osx/package/content ]; then
    echo "Please run this script from the workspaceRoot folder"
    exit 1
fi

pushd ./osx/package > /dev/null

# Driver & agent archive source folder names
DIST_FOLDER=../../dist
PACKAGE_NAME=TrackballWorks2Installer.pkg


# build component list
#pkgbuild --analyze --root $CONTENT_FOLDER trackballworks2.plist
#rc=$?; if [[ $rc != 0 ]]; then exit $rc; fi
# pkgbuild --analyze --root ./$AGENT_ARCHIVE trackballworks2agent.plist
# rc=$?; if [[ $rc != 0 ]]; then exit $rc; fi

# create distribution definition
pkgbuild --root ./content \
    --component-plist trackballworks2.plist \
    --identifier com.kensington.trackballworks2 \
    --scripts ./Scripts \
    trackballworks2.pkg
rc=$?; if [[ $rc != 0 ]]; then exit $rc; fi
#pkgbuild --root ./$AGENT_ARCHIVE --component-plist trackballworks2agent.plist --identifier com.kensington.trackballworks2agent trackballworks2agent.pkg
#rc=$?; if [[ $rc != 0 ]]; then exit $rc; fi
#productbuild --synthesize --package trackballworks2.pkg Distribution.xml
#rc=$?; if [[ $rc != 0 ]]; then exit $rc; fi

if [ ! -d $DIST_FOLDER ]; then
    mkdir $DIST_FOLDER
elif [ -f $DIST_FOLDER/$PACKAGE_NAME ]; then
    rm $DIST_FOLDER/$PACKAGE_NAME
fi

productbuild --distribution ./Distribution.xml --resources ./Resources --package-path . $PACKAGE_NAME
rc=$?; if [[ $rc != 0 ]]; then exit $rc; fi

mv $PACKAGE_NAME $DIST_FOLDER
rm trackballworks2.pkg

popd > /dev/null
