#!/bin/bash

# Script to install Trackball driver kext

function error_exit
{
    echo "$1" 1>&2
    exit 1
}

# check if we're being run as root
function check_root
{
    if [ "$EUID" -ne 0 ]; then
        echo "Please run as root"
        exit
    fi
}

check_root	# has to be run as root

# define source & target folders
DRIVER=trackballworks2.kext
SOURCE_DIR=/Volumes/SharedFolders/Home/Library/Developer/Xcode/DerivedData/TrackballWorks2-devexkcprdwiryhcjjgkkcflevum/Build/Products/Debug/
TARGET_DIR=/Library/Extensions

# remove existing driver
rm -rf $TARGET_DIR/$DRIVER || error_exit "Error removing existing drivers"

# copy new drivers from Host
cp -R $SOURCE_DIR/$DRIVER $TARGET_DIR || error_exit "Error copying new drivers"

# change new driver ownership
chown -R root:wheel $TARGET_DIR/$DRIVER || error_exit "Error changing newly copied driver ownership"

# touch TARGET_DIR so that driver catalog cache is refreshed
# touch $TARGET_DIR || error_exit "Error touching $TARGET_DIR"
kextcache -v -i / || error_exit "Error refreshing IO Catalog for $TARGET_DIR"
sudo shutdown -r now

echo "Done :>)"
