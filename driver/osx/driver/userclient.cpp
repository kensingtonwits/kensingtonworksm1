//
//  ServiceUserClient.cpp
//  trackball2
//
//  Created by Hariharan Mahadevan on 2019/1/25.
//  Copyright © 2019 Kensington. All rights reserved.
//

#include "userclient.hpp"
#include "tblog.h"
#include "service.hpp"

OSDefineMetaClassAndStructors(TrackballWorks2UserClient, IOUserClient)

TrackballWorks2UserClient* TrackballWorks2UserClient::withProvider(IOService* provider)
{
    TrackballWorks2UserClient* self = new TrackballWorks2UserClient();
    
    if (!self || !self->init(NULL))
    {
        if (self)
        {
            TBLogError("TrackballWorks2UserClient - error initializing\n");
            self->release();
        }
        return NULL;
    }
    
    // attach it ourselves
    if (!self->attach(provider))
    {
        TBLogError("TrackballWorks2UserClient - error attaching\n");
        self->release();
        return NULL;
    }
    
    // start it
    if (!self->start(provider))
    {
        TBLogError("TrackballWorks2UserClient - error starting\n");
        self->release();
        return NULL;
    }
    
    TBLogDebug("TrackballWorks2UserClient successfuly started\n");
    return self;

}

const IOExternalMethodDispatch TrackballWorks2UserClient::sMethods[] = {

    // kKWDriverGetDeviceInfo
    {
        TrackballWorks2Service::sGetDeviceInfo,
        0,
        0,
        0,
        sizeof(TBWDEVICEINFO)
    },
    
    // kKWDriverRegisterHelper
    {
        TrackballWorks2Service::sRegisterHelper,
        1,
        0,
        0,
        0
    },
    
    // kKWDriverDeregisterHelper,
    {
        TrackballWorks2Service::sDeregisterHelper,
        0,
        0,
        0,
        0
    },
    
    // kKWDriverGetSlowPointer
    {
        TrackballWorks2Service::sGetSlowPointer,
        0,
        0,
        0,
        sizeof(TBWSLOWPOINTERPARAMS)},

    // kKWDriverSetSlowPointer
    {
        TrackballWorks2Service::sSetSlowPointer,
        0,
        sizeof(TBWSLOWPOINTERPARAMS),
        0,
        0},

    // kKWDriverGetTBWSINGLEAXISMOVEMENTPARMS
    {
        TrackballWorks2Service::sGetTBWSINGLEAXISMOVEMENTPARMS,
        0,
        0,
        0,
        sizeof(TBWSINGLEAXISMOVEMENTPARMS)},

    // kKWDriverSetTBWSINGLEAXISMOVEMENTPARMS
    {
        TrackballWorks2Service::sSetTBWSINGLEAXISMOVEMENTPARMS,
        0,
        sizeof(TBWSINGLEAXISMOVEMENTPARMS),
        0,
        0
    },

    // kKWDriverGetScrollWheelParams,
    {
        TrackballWorks2Service::sGetScrollWheelParams,
        0,
        0,
        0,
        sizeof(TBWSCROLLWHEELPARAMS)
    },

    // kKWDriverSetScrollWheelParams,
    {
        TrackballWorks2Service::sSetScrollWheelParams,
        0,
        sizeof(TBWSCROLLWHEELPARAMS),
        0,
        0
    },

    // kKWDriverGetNextEventAsync
    //
    // Use IOCononectCallScalarMethod
    // Args:
    //  input - 2 elements
    //              0 = sizeof(TBWDRIVEREVENT)
    //              1 = pointer to TBWDRIVEREVENT*
    {
        TrackballWorks2Service::sGetNextEventAsync,
        2,
        0,
        0,
        0
    },
    
    // kKWDriverSetButtonsConfig
    //
    // Use IOConnectCallStructMethod.
    // Args:
    //  structureInput - TBWBUTTONSCONFIG*
    {
        TrackballWorks2Service::sSetButtonsConfig,
        0,
        sizeof(TBWBUTTONSCONFIG),
        0,
        0,
    },
    
    // kKWDriverEmulateDeviceAction
    //
    // Args
    //  structureInput = TBWEMULATEDEVICEACTION*
    {
        TrackballWorks2Service::sEmulateDeviceAction,
        0,
        sizeof(TBWEMULATEDEVICEACTION),
        0,
        0
    },
    
    // kKWDriverGetPointerParams,
    //
    // Use IOConnectCallStructMethod.
    // Args:
    //  structureOutput = TBWPOINTERPARAMS*
    {
        TrackballWorks2Service::sGetPointerParams,
        0,
        0,
        0,
        sizeof(TBWPOINTERPARAMS)
    },
    
    // kKWDriverSetPointerParams
    //
    // Use IOConnectCallStructMethod.
    // Args:
    //  structureInput = TBWPOINTERPARAMS*
    {
        TrackballWorks2Service::sSetPointerParams,
        0,
        sizeof(TBWPOINTERPARAMS),
        0,
        0
    },

    // kKWDriverPostKeystroke,
    //
    // Use IOConnectCallStructMethod.
    // Args:
    //  structureInput = TBWPOSTKEYSTROKE*
    {
        TrackballWorks2Service::sPostKeystroke,
        0,
        sizeof(TBWPOSTKEYSTROKE),
        0,
        0
    },
    
    // kKWDriverPostPointerClick,
    //
    // Use IOConnectCallStructMethod.
    // Args:
    //  structureInput = TBWPOSTPOINTERCLICK*
    {
        TrackballWorks2Service::sPostPointerClick,
        0,
        sizeof(TBWPOSTPOINTERCLICK),
        0,
        0
    }

};

bool TrackballWorks2UserClient::initWithTask(task_t owningTask, void* securityToken, UInt32 type, OSDictionary* properties)
{
    TBLogDebug("TrackballWorks2UserClient::initWithTask");
    if (!owningTask) {
        TBLogDebug("TrackballWorks2UserClient::initWithTask - owningTask == NULL");
        return false;
    }
    
    if (!super::initWithTask(owningTask, securityToken , type, properties)) {
        TBLogDebug("TrackballWorks2UserClient::initWithTask - super call failed");
        return false;
    }
    
    _task = owningTask;
    _taskIsAdmin = false;
    IOReturn ret = clientHasPrivilege(securityToken, kIOClientPrivilegeAdministrator);
    if (ret == kIOReturnSuccess)
    {
        // _taskIsAdmin = true;
    }
    
    TBLogDebug("TrackballWorks2UserClient::initWithTask - success");
    return true;
}


bool TrackballWorks2UserClient::start(IOService *provider)
{
    if (!super::start(provider)) {
        TBLogError("TrackballWorks2UserClient::start - parent class start failed");
        return false;
    }
    
    _service = OSDynamicCast(TrackballWorks2Service, provider);
    if (!_service) {
        TBLogError("TrackballWorks2UserClient - error getting parent driver object");
        return false;
    }
    
    TBLogDebug("TrackballWorks2UserClient::start - done");
    return true;
}

void TrackballWorks2UserClient::stop(IOService* provider)
{
    TBLogDebug("TrackballWorks2UserClient::stop");
    _service->clearHelperUserClient();
    super::stop(provider);
}

IOReturn TrackballWorks2UserClient::clientClose()
{
    terminate();
    return kIOReturnSuccess;
}

IOReturn TrackballWorks2UserClient::externalMethod(uint32_t selector,
                                IOExternalMethodArguments* arguments,
                                IOExternalMethodDispatch* dispatch/*=0*/,
                                OSObject* target/*=0*/,
                                void* reference/*=0*/)
{
    TBLogDebug("TrackballWorks2UserClient::externalMethod, selector is: %d", selector);
    // Ensure the requested control selector is within range
    if (selector >= kKWDriverSentinel)
        return kIOReturnUnsupported;
    
    dispatch = (IOExternalMethodDispatch*)&sMethods[selector];
    
    target = _service;
    reference = (void*)this;
    return super::externalMethod(selector, arguments, dispatch, target, reference);
};

void TrackballWorks2UserClient::notifyAsyncResult(OSAsyncReference64 asyncRef, IOReturn ret)
{
    IOReturn result = sendAsyncResult64(asyncRef, ret, NULL, 0);
    if (result != kIOReturnSuccess) {
        TBLogError("TrackballWorks2UserClient::notifyAsyncResult -- error: 0x%x\n", result);
    } else {
        TBLogDebug("TrackballWorks2UserClient::notifyAsyncResult - success\n");
    }
}

IOReturn TrackballWorks2UserClient::sRegisterHelper(
    OSObject* target,
    void* reference,
    IOExternalMethodArguments* arguments)
{
    return reinterpret_cast<TrackballWorks2UserClient*>(target)->registerHelper(
    );
}

IOReturn TrackballWorks2UserClient::registerHelper()
{
    TBLogDebug("TrackballWorks2UserClient::registerHelper, always return success");
    return kIOReturnSuccess;
}

IOReturn TrackballWorks2UserClient::sDeregisterHelper(
    OSObject* target,
    void* reference,
    IOExternalMethodArguments* arguments)
{
    return reinterpret_cast<TrackballWorks2UserClient*>(target)->deregisterHelper(
    );
}

IOReturn TrackballWorks2UserClient::deregisterHelper()
{
    return kIOReturnSuccess;
}

