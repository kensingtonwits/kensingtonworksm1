//
//  KWDriverService.hpp
//  trackball2
//
//  Created by Hariharan Mahadevan on 2019/1/24.
//  Copyright © 2019 Kensington. All rights reserved.
//

#pragma once

#include <IOKit/IOService.h>
#include <IOKit/IOUserClient.h>

#include "KWDriverInterface.h"
#include "circularqueue.h"
#include "trackball.h"

class com_kensington_TrackballWorks2;

class TrackballWorks2Keyboard;
class TrackballWorks2Pointer;
class TrackballWorks2UserClient;

class TrackballWorks2Service : public IOService {

    OSDeclareDefaultStructors(TrackballWorks2Service)

    typedef IOService super;

    TrackballWorks2Keyboard* _keyboard = { nullptr };
    TrackballWorks2Pointer* _pointer = { nullptr };
    TrackballWorks2UserClient* _userClient = { nullptr };

    com_kensington_TrackballWorks2* _driver = { nullptr };

    // "HIDMouseAcceleration" HID parameter controls Tracking Speed and
    // varies from 0 - 196608.
    // Default 98304
    // uint64_t _pointerAccelerationRate = { 0 };

    // "HIDMouseScrollAcceleration" HID parameter controls Scrolling speed
    // and ranges from 0 - 327680.
    // Default 49152
    //float _scrollSpeed = { 1.0 };

    IOWorkLoop* _workLoop = { nullptr };

    //IOTimerEventSource* _timerDetectChords   = { nullptr };
    IOTimerEventSource* _timerInertialScroll = { nullptr };
#ifdef SCROLL_WITH_TRACKBALL
    IOTimerEventSource* _timerAutoScroll     = { nullptr }; // *&*&*&V1_20200518
#endif
    IOTimerEventSource* _timerDebounce       = { nullptr }; // *&*&*&V1_20200914
    IOTimerEventSource* _timerTiltButton     = { nullptr }; // *&*&*&Va_20210119

    IOLock* _deviceLock = { nullptr };
    OSAsyncReference64  _notifyEventAsyncRef;
    IOMemoryDescriptor* _notifyEventDescriptor;
    CircularQueue*      _notifyEventQueue;

    TBWBUTTONSCONFIG    _buttonsConfig;
    
public:
    TBLIBCONFIG         _tbwConfig; // *&*&*&V1_20201204
    // Create an instance of this class and attach it to
    // the supplied provider.
    //
    // Returns NULL on failure.
    static TrackballWorks2Service* withProvider(IOService* provider);

    //*** Begin UserClient IOCTL request handlers  ***//

    // UserClient object directly refers to these methods in its
    // dispatch table which is why these are as public methods.
    // These methods in turn call into the DriverService
    // object instance through the 'reference' argument and then invoke the
    // object method of the same name, minus the leading 's' and the resulting
    // label properly 'camelcase'd.

    static IOReturn sGetDeviceInfo(
        OSObject* target,
        void* reference,
        IOExternalMethodArguments* arguments);
    IOReturn getDeviceInfo(TBWDEVICEINFO* pInfo);

    static IOReturn sRegisterHelper(
        OSObject* target,
        void* reference,
        IOExternalMethodArguments* arguments);
    IOReturn registerHelper(TrackballWorks2UserClient* userClient, uint64_t id);

    static IOReturn sDeregisterHelper(
        OSObject* target,
        void* reference,
        IOExternalMethodArguments* arguments);
    IOReturn deregisterHelper(TrackballWorks2UserClient* userClient);

    static IOReturn sSetSlowPointer(OSObject* target,
                                    void* reference,
                                    IOExternalMethodArguments* arguments);
    IOReturn setSlowPointer(const TBWSLOWPOINTERPARAMS* args);

    static IOReturn sGetSlowPointer(OSObject* target,
                                    void* reference,
                                    IOExternalMethodArguments* arguments);
    IOReturn getSlowPointer(TBWSLOWPOINTERPARAMS* args);

    static IOReturn sSetTBWSINGLEAXISMOVEMENTPARMS(OSObject* target,
                                           void* reference,
                                           IOExternalMethodArguments* arguments);
    IOReturn setTBWSINGLEAXISMOVEMENTPARMS(const TBWSINGLEAXISMOVEMENTPARMS* args);

    static IOReturn sGetTBWSINGLEAXISMOVEMENTPARMS(OSObject* target,
                                           void* reference,
                                           IOExternalMethodArguments* arguments);
    IOReturn getTBWSINGLEAXISMOVEMENTPARMS(TBWSINGLEAXISMOVEMENTPARMS* args);

    static IOReturn sSetScrollWheelParams(OSObject* target,
                                          void* reference,
                                          IOExternalMethodArguments* arguments);
    IOReturn setScrollWheelParams(const PTBWSCROLLWHEELPARAMS pParams);

    static IOReturn sGetScrollWheelParams(OSObject* target,
                                          void* reference,
                                          IOExternalMethodArguments* arguments);
    IOReturn getScrollWheelParams(PTBWSCROLLWHEELPARAMS pParams);

    static IOReturn sGetNextEventAsync(OSObject* target,
                                       void* reference,
                                       IOExternalMethodArguments* arguments);
    IOReturn getNextEventAsync(IOExternalMethodArguments* args);

    static IOReturn sSetButtonsConfig(OSObject* target,
                                      void* reference,
                                      IOExternalMethodArguments* arguments);
    IOReturn setButtonsConfig(const TBWBUTTONSCONFIG* pButtonsConfig);

    static IOReturn sEmulateDeviceAction(OSObject* target,
                                         void* reference,
                                         IOExternalMethodArguments* arguments);
    IOReturn emulateDeviceAction(const TBWEMULATEDEVICEACTION* pAction);

    static IOReturn sGetPointerParams(OSObject* target,
                                      void* reference,
                                      IOExternalMethodArguments* arguments);
    IOReturn getPointerParams(TBWPOINTERPARAMS* p);

    static IOReturn sSetPointerParams(OSObject* target,
                                      void* reference,
                                      IOExternalMethodArguments* arguments);
    IOReturn setPointerParams(const TBWPOINTERPARAMS* p);

    static IOReturn sPostKeystroke(OSObject* target,
                                      void* reference,
                                      IOExternalMethodArguments* arguments);
    IOReturn postKeystroke(const TBWPOSTKEYSTROKE*);

    static IOReturn sPostPointerClick(OSObject* target,
                                      void* reference,
                                      IOExternalMethodArguments* arguments);
    IOReturn postPointerClick(const TBWPOSTPOINTERCLICK*);
    //*** End UserClient IOCTL request handlers  ***//

private:
    OSObject* getDriver();

    static void sTimerFn(OSObject* owner, IOTimerEventSource* sender);
    void timerFn(IOTimerEventSource* sender);

    bool setDevicePointerAccelerationRate(uint64_t rate);

    bool parseHIDCapabilities();

    // queue up a TBWDRIVEREVENT to the end of the circular queue for
    // sending to the usermode app.
    void enqueueTbwDriverEvent(TBWDRIVEREVENT* pEvent);

    // dequeue an event from the top of the queue and send it to
    // user mode app (if one is registered).
    void dequeueNextTbwDriverEvent();

    // sets default buttons config, which is maskIn & maskOut for all
    // buttons are the same.
    void setDefaultButtonsConfig();

    // Save current TBWBUTTONSCONFIG
    void saveCurButtonsConfig();
    // restore last user-set buttons config from IORegistry, if one is
    // found. If none exists, this method does nothing.
    void restoreLastButtonsConfig();

    //*** Begin callbacks for TB library ***//
    static void sTBPostButtonEvent(void* ref, uint32_t buttonState, uint32_t uDownMask, uint32_t uUpMask) // 看起來是沒用到
    { reinterpret_cast<TrackballWorks2Service*>(ref)->tbPostButtonEvent(buttonState); }
    void tbPostButtonEvent(uint32_t buttonState);

    static void sTBPostCursorEvent(void* ref, int dx, int dy) // 看起來沒用到
    { reinterpret_cast<TrackballWorks2Service*>(ref)->tbPostCusrorEvent(dx, dy); }
    void tbPostCusrorEvent(int dx, int dy);

    static void sTBPostWheelEvent(void* ref, int wheel, int offset) // 看起來沒用到
    { reinterpret_cast<TrackballWorks2Service*>(ref)->tbPostWheelEvent(wheel, offset); }
    void tbPostWheelEvent(int wheel, int offset);

    static void sTBEnqueueButtonEvent(void* ref, uint32_t uDownState, uint32_t uUpState)
    { reinterpret_cast<TrackballWorks2Service*>(ref)->tbEnqueueButtonEvent(uDownState, uUpState); }
    void tbEnqueueButtonEvent(uint32_t uDownState, uint32_t uUpState);

    static uint32_t sTBStartTimer(void* ref, uint32_t id, uint32_t timeOut)
    { return reinterpret_cast<TrackballWorks2Service*>(ref)->tbStartTimer(id, timeOut); }
    uint32_t tbStartTimer(uint32_t id, uint32_t timeOut);

    static void sTBStopTimer(void* ref, uint32_t id)
    { reinterpret_cast<TrackballWorks2Service*>(ref)->tbStopTimer(id); }
    void tbStopTimer(uint32_t id);
    //*** End TB library callbacks ***//

public:
    void registerHelperUserClient(TrackballWorks2UserClient* pUserClient);
    void clearHelperUserClient();

    virtual bool start(IOService* provider) override;
    virtual void stop(IOService* provider) override;

    void postPointerEvent(int dx, int dy, UInt32 buttonState, bool btnEvent, bool ptrEvent);
    //void postSpecialPointerEvent(int dx, int dy);
    /**
     * int wheel
     *  1 - vert wheel
     *  2 - horizontal wheel
     * int distance
     *  wheel units to scroll
     */
    void postWheelEvent(int wheel, int units);
    uint32_t getHelperConnectedStatus(); // *&*&*&Va_20200918    
};
