//
//  DriverUSB2.cpp
//  driver
//
//  Created by Hariharan Mahadevan on 2019/2/20.
//  Copyright © 2019 Kensington. All rights reserved.
//

#include <AssertMacros.h>
#include "driver.hpp"
#include <IOKit/hid/IOHIDInterface.h>
#include <IOKit/hid/IOHIDKeys.h>
#include <IOKit/hidsystem/IOHIDTypes.h>

#include "tblog.h"
#include "util.h"
#include "service.hpp"

#define WIRED_VERTICAL_TRACKBALL_PRODUCT_ID    0x809D // *&*&*&V1_20210120

// IOHIDEventDriver.h
enum {
    kMouseButtons   = 0x1,
    kMouseXYAxis    = 0x2,
    kBootMouse      = (kMouseXYAxis | kMouseButtons)
};

enum {
    kBootProtocolNone   = 0,
    kBootProtocolKeyboard,
    kBootProtocolMouse
};


enum {
    kIOHandleChildVendorMessageReport,
    kIOHandlePrimaryVendorMessageReport
};
#define GetReportType( type )                                               \
    ((type <= kIOHIDElementTypeInput_ScanCodes) ? kIOHIDReportTypeInput :   \
    (type <= kIOHIDElementTypeOutput) ? kIOHIDReportTypeOutput :            \
    (type <= kIOHIDElementTypeFeature) ? kIOHIDReportTypeFeature : -1)

#define GET_AXIS_COUNT(usage) (usage-kHIDUsage_GD_X+ 1)
#define GET_AXIS_INDEX(usage) (usage-kHIDUsage_GD_X)

#define kDefaultAbsoluteAxisRemovalPercentage           15
#define kDefaultPreferredAxisRemovalPercentage          10

#define kHIDUsage_MFiGameController_LED0                0xFF00

#define SET_NUMBER(key, num) do { \
    tmpNumber = OSNumber::withNumber(num, 32); \
    if (tmpNumber) { \
    kbEnableEventProps->setObject(key, tmpNumber); \
    tmpNumber->release(); \
    } \
}while (0);

// IOHIDEventDriver.h

// borrowed from xnu source/libkern/OSBase.h as this is not in any importable headers
#define AbsoluteTime_to_scalar(x)	(*(uint64_t *)(x))

/* t1 < = > t2 */
#define CMP_ABSOLUTETIME(t1, t2)				\
    (AbsoluteTime_to_scalar(t1) >				\
        AbsoluteTime_to_scalar(t2)? (int)+1 :	\
    (AbsoluteTime_to_scalar(t1) <				\
        AbsoluteTime_to_scalar(t2)? (int)-1 : 0))
// end borrowed from xnu source/libkern/OSBase.h


// All our recognized HID input reports are 4 bytes long
#define INPUT_REPORT_LENGTH  4

OSDefineMetaClassAndStructors(com_kensington_TrackballWorks2, IOHIDEventDriver)

// IOHIDEventDriver.h

#define _led                            _reserved->led
#define _keyboard                       _reserved->keyboard
#define _scroll                         _reserved->scroll
#define _relative                       _reserved->relative
#define _multiAxis                      _reserved->multiAxis
#define _gameController                 _reserved->gameController
#define _digitizer                      _reserved->digitizer
#define _unicode                        _reserved->unicode
#define _absoluteAxisRemovalPercentage  _reserved->absoluteAxisRemovalPercentage
#define _preferredAxisRemovalPercentage _reserved->preferredAxisRemovalPercentage
#define _lastReportTime                 _reserved->lastReportTime
#define _vendorMessage                  _reserved->vendorMessage
#define _biometric                      _reserved->biometric
#define _accel                          _reserved->accel
#define _gyro                           _reserved->gyro
#define _compass                        _reserved->compass
#define _temperature                    _reserved->temperature
#define _sensorProperty                 _reserved->sensorProperty
#define _orientation                    _reserved->orientation
#define _workLoop                       _reserved->workLoop
#define _commandGate                    _reserved->commandGate

// end IOHIDEventDriver.h

bool com_kensington_TrackballWorks2::init(OSDictionary* dictionary)
{
    TBLogDebug("[TBW] com_kensington_TrackballWorks2::init\n");

    if (!super::init(dictionary))
        return false;

    _reserved = IONew(ExpansionData, 1);
    if (!_reserved)
        return false;

    bzero(_reserved, sizeof(ExpansionData));

    _preferredAxisRemovalPercentage = kDefaultPreferredAxisRemovalPercentage;

    return true;
}

void com_kensington_TrackballWorks2::free()
{
    TBLogDebug("[TBW] com_kensington_TrackballWorks2::free\n");

    OSSafeReleaseNULL(_relative.elements);
    OSSafeReleaseNULL(_multiAxis.elements);
    OSSafeReleaseNULL(_digitizer.transducers);
    OSSafeReleaseNULL(_digitizer.touchCancelElement);
    OSSafeReleaseNULL(_digitizer.deviceModeElement);
    OSSafeReleaseNULL(_scroll.elements);
    OSSafeReleaseNULL(_led.elements);
    OSSafeReleaseNULL(_keyboard.elements);
    OSSafeReleaseNULL(_unicode.legacyElements);
    OSSafeReleaseNULL(_unicode.gesturesCandidates);
    OSSafeReleaseNULL(_unicode.gestureStateElement);
    OSSafeReleaseNULL(_gameController.elements);
    OSSafeReleaseNULL(_vendorMessage.childElements);
    OSSafeReleaseNULL(_vendorMessage.primaryElements);
    OSSafeReleaseNULL(_vendorMessage.pendingEvents);
    OSSafeReleaseNULL(_supportedElements);
    OSSafeReleaseNULL(_biometric.elements);
    OSSafeReleaseNULL(_accel.elements);
    OSSafeReleaseNULL(_gyro.elements);
    OSSafeReleaseNULL(_compass.elements);
    OSSafeReleaseNULL(_temperature.elements);
    OSSafeReleaseNULL(_sensorProperty.reportInterval);
    OSSafeReleaseNULL(_sensorProperty.reportLatency);
    OSSafeReleaseNULL(_sensorProperty.sniffControl);
    OSSafeReleaseNULL(_orientation.elements);

    if (_reserved) {
        IODelete(_reserved, ExpansionData, 1);
        _reserved = NULL;
    }

    OSSafeReleaseNULL(_service);
    super::free();
}

/*
 We need to determine the device type here
 */
bool com_kensington_TrackballWorks2::handleStart(IOService *provider)
{
    LOG_FUNCNAME();

    bool res = super::handleStart(provider);
    if (!res)
    {
        TBLogError("com_kensington_TrackballWorks2 - Superclass handleStart failed\n");
        return res;
    }


    // begin code copied from IOHIDFamily/IOHIDEventDriver.h

    _interface = OSDynamicCast( IOHIDInterface, provider );
    if ( !_interface )
        return false;

    UInt32      bootProtocol    = 0;
    OSArray *elements = _interface->createMatchingElements();
    bool result = false;
    if ( elements ) {
        if ( parseElements ( elements, bootProtocol )) {
            result = true;
        }
    }
    OSSafeReleaseNULL(elements);

    // end code copied from IOHIDFamily/IOHIDEventDriver.h

    TBLogDebug("[TBW] com_kensington_TrackballWorks2 - relative.elements: %d, keyboard.elements: %d\n",
        _relative.elements ? _relative.elements->getCount() : 0,
        _keyboard.elements ? _keyboard.elements->getCount(): 0);
    
    _productID = 0;
    _vendorID  = 0; //*&*&*&Va_20200922
   // _interfaceNumber = 0; // unused
    _service = NULL;

    //OSNumber* number = OSDynamicCast(OSNumber, getProperty("ProductID"));
    // *&*&*&Va_20200922 we need vid&pid
    OSNumber* numberVid = OSDynamicCast(OSNumber, getProperty("VendorID"));
    OSNumber* numberPid = OSDynamicCast(OSNumber, getProperty("ProductID"));
    if (numberVid && numberPid)
    {
        TBLogDebug("[TBW] com_kensington_TrackballWorks2::handleStart() - Kensington device is plugged.\n");
        _vendorID  = numberVid->unsigned16BitValue();
        _productID = numberPid->unsigned16BitValue();
        if(_vendorID == 1149)
        {
            switch(_productID)
            {
                case 32872: // Ergo Vertical Wireless Trackball
                case 32873: // Ergo Vertical Wireless Trackball (BT)
                case 32925: // wired vertical trackball
                    // HW has AC pan buttons
                    _bhasACPanButtons = true;
                    break;
                default:
                    // HW doesn't have AC Pan buttons
                    break;
            }
        }
    }

    // Ignore interfaces without any HID relative motion elements
    bool fLoadService = _relative.elements && _relative.elements->getCount();
    
    if (fLoadService) {
        // Assuming relative elements are present, apply additional matching attributes filter
        fLoadService = matchAddlAttributes(provider);
    }

    // If both filters returned true, we can safely load the helper service
    if (fLoadService)
    {
        // create service, only for interface 0
        TBLogDebug("[TBW] com_kensington_TrackballWorks2::start - instantiating Trackball2Workservice\n");
        _service = TrackballWorks2Service::withProvider(this);
        if (!_service)
        {
            TBLogError("[TBW] com_kensington_TrackballWorks2 - Error initializing TrackballService\n");
            return false;
        }
    }
    else
    {
        TBLogDebug("[TBW] com_kensington_TrackballWorks2::start - NOT instantiating Trackball2Workservice as interface nos don't match\n");
    }
    return res;
}

void com_kensington_TrackballWorks2::handleStop(IOService *provider)
{
    TBLogInfo("com_kensington_TrackballWorks2 -handleStop - product id: %d, interface: %u\n", (int)_productID, _interfaceNumber);
    super::handleStop(provider);
}

void com_kensington_TrackballWorks2::handleInterruptReport(
                                                              AbsoluteTime                timeStamp,
                                                              IOMemoryDescriptor *        report,
                                                              IOHIDReportType             reportType,
                                                              UInt32                      reportID)
{
    TBLogDebug("[TBW] com_kensington_TrackballWorks2 - ::hIR, report Id: %u, type: %d, len: %llu\n",
               reportID,
               (int)reportType,
               report->getLength());
   
    if (!readyForReports() || reportType!= kIOHIDReportTypeInput)
    {
        TBLogDebug("[TBW] com_kensington_TrackballWorks2 - ::hIR, in the readyForReports()\n");
        return;
    }

    if (!_service) {
        TBLogDebug("[TBW] com_kensington_TrackballWorks2 - ::hIR, in the not-service\n");
        super::handleInterruptReport(timeStamp, report, reportType, reportID); // 開機不會進
        return;
    }

    _lastReportTime = mach_continuous_time();

    static const uint16_t SLIMBLADE_PRODUCT_ID = 0x2041;
    static const uint32_t SLIMBLADE_REPORT_LENGTH = 5;
    // Special handling for SlimBlade as its button 3 & 4 status bits are
    // set in byte 5 bits 0 & 1 respectively.
    if (SLIMBLADE_PRODUCT_ID == _productID && SLIMBLADE_REPORT_LENGTH == report->getLength()) {
        IOMemoryMap* mapping = report->createMappingInTask(kernel_task,
                                                           0,
                                                           kIOMapAnywhere);
        TBLogDebug("[TBW] com_kensington_TrackballWorks2 - ::hIR, in the product-id-compare\n");
        if (mapping)
        {
            UInt8* pReport = reinterpret_cast<UInt8*>(mapping->getAddress());
            /*
            TBLogDebug("\tSlimBlade report - %02x %02x %02x %02x %02x\n",
                       pReport[0],
                       pReport[1],
                       pReport[2],
                       pReport[3],
                       pReport[4]
                       );
            //*/
            pReport[0] |= (pReport[4] & 0x03) << 2;
            pReport[4] = 0;
            uint32_t buttonState = (uint32_t) pReport[0];
            int dx = (int) (SInt8)pReport[1];
            int dy = (int) (SInt8)pReport[2];
            if(_service->_tbwConfig.fHelperConnected)
                _service->postPointerEvent(dx, dy, buttonState, true, true);
            else // 這裡不僅要做座標也要做button(而且一定要是有處理過的button)
            {
                dispatchRelativePointerEvent(timeStamp, dx, dy, buttonState);
            }
            mapping->release();
        }
    }
    else if (_productID == WIRED_VERTICAL_TRACKBALL_PRODUCT_ID)
    {
        TBLogDebug("[TBW] com_kensington_TrackballWorks2 - ::hIR, Got VTB\n");
        IOMemoryMap* mapping = report->createMappingInTask(kernel_task,
                                                           0,
                                                           kIOMapAnywhere);
        if (mapping)
        {
            UInt8* pReport = reinterpret_cast<UInt8*>(mapping->getAddress());
            /*
            TBLogDebug("\t[TBW] com_kensington_TrackballWorks2 - ::hIR, VTB report - %02x %02x %02x %02x %02x\n",
                       pReport[0],
                       pReport[1],
                       pReport[2],
                       pReport[3],
                       pReport[4]
                       );
            //*/
            uint32_t buttonState = (uint32_t) pReport[0];
            int dx = (int) (SInt8)pReport[1];
            int dy = (int) (SInt8)pReport[3];
            if(_service->_tbwConfig.fHelperConnected)
                _service->postPointerEvent(dx, dy, buttonState, true, true);
            else
            {
                dispatchRelativePointerEvent(timeStamp, dx, dy, buttonState);
            }
            mapping->release();
        }                                               
    }
    else
    {
        handleRelativeReport(timeStamp, reportID);
    }
    handleScrollReport(timeStamp, reportID);
}

// begin IOHIDEventDriver.h

//====================================================================================================
// com_kensington_TrackballWorks2::parseRelativeElement
//====================================================================================================
bool com_kensington_TrackballWorks2::parseRelativeElement(IOHIDElement * element)
{
    UInt32 usagePage    = element->getUsagePage();
    UInt32 usage        = element->getUsage();
    bool   store        = false;
    TBLogDebug("[TBW] com_kensington_TrackballWorks2 - parseRelativeElement\n");
    switch ( usagePage ) {
        case kHIDPage_GenericDesktop:
            switch ( usage ) {
                case kHIDUsage_GD_X:
                case kHIDUsage_GD_Y:
                    if ((element->getFlags() & kIOHIDElementFlagsRelativeMask) == 0)
                        break;
                    _bootSupport &= ~kMouseXYAxis;
                    store = true;
                    break;
            }
            break;
    }

    __Require_Quiet(store, exit);

    if ( !_relative.elements ) {
        _relative.elements = OSArray::withCapacity(4);
        __Require(_relative.elements, exit);
    }

    _relative.elements->setObject(element);

exit:
    return store;
}


//====================================================================================================
// com_kensington_TrackballWorks2::parseScrollElement
//====================================================================================================
bool com_kensington_TrackballWorks2::parseScrollElement(IOHIDElement * element)
{
    UInt32 usagePage    = element->getUsagePage();
    UInt32 usage        = element->getUsage();
    bool   store        = false;
    TBLogDebug("[TBW] com_kensington_TrackballWorks2 - parseScrollElement\n");
    switch ( usagePage ) {
        case kHIDPage_GenericDesktop:
            switch ( usage ) {
                case kHIDUsage_GD_Dial:
                case kHIDUsage_GD_Wheel:
                case kHIDUsage_GD_Z:

                    if ((element->getFlags() & (kIOHIDElementFlagsNoPreferredMask|kIOHIDElementFlagsRelativeMask)) == 0) {
                        calibrateCenteredPreferredStateElement(element, _preferredAxisRemovalPercentage);
                    }

                    store = true;
                    break;
            }
            break;
        case kHIDPage_Consumer:
            switch ( usage ) {
                case kHIDUsage_Csmr_ACPan:
                    TBLogDebug("[TBW] com_kensington_TrackballWorks2 - parseScrollElement - got ACPan!!\n");
                    
#if 0 // *&*&*&V1_20200922, to filter unuseful messages of mouse, but doesn't work
                    if(_bhasACPanButtons)
                    {
                        store = true;
                    }
#else
                    store = true;
#endif
                break;
            }
            break;
    }

    __Require_Quiet(store, exit);

    if ( !_scroll.elements ) {
        _scroll.elements = OSArray::withCapacity(4);
        __Require(_scroll.elements, exit);
    }

    _scroll.elements->setObject(element);

exit:
    return store;
}

#if 0
//====================================================================================================
// com_kensington_TrackballWorks2::setRelativeProperties
//====================================================================================================
void com_kensington_TrackballWorks2::setRelativeProperties()
{
    OSDictionary * properties = OSDictionary::withCapacity(4);

    __Require(properties, exit);
    __Require(_relative.elements, exit);

    properties->setObject(kIOHIDElementKey, _relative.elements);

    setProperty("RelativePointer", properties);

exit:
    OSSafeReleaseNULL(properties);
}


//====================================================================================================
// com_kensington_TrackballWorks2::setScrollProperties
//====================================================================================================
void com_kensington_TrackballWorks2::setScrollProperties()
{
    OSDictionary * properties = OSDictionary::withCapacity(4);

    __Require(properties, exit);
    __Require(_scroll.elements, exit);

    properties->setObject(kIOHIDElementKey, _scroll.elements);

    setProperty("Scroll", properties);

exit:
    OSSafeReleaseNULL(properties);
}


//====================================================================================================
// com_kensington_TrackballWorks2::setAccelProperties
//====================================================================================================
void com_kensington_TrackballWorks2::setAccelProperties()
{
    OSDictionary *properties = OSDictionary::withCapacity(1);

    __Require(properties, exit);
    __Require(_accel.elements, exit);

    properties->setObject(kIOHIDElementKey, _accel.elements);

    setProperty("Accel", properties);

exit:
    OSSafeReleaseNULL(properties);
}

//====================================================================================================
// com_kensington_TrackballWorks2::setAccelerationProperties
//====================================================================================================
void com_kensington_TrackballWorks2::setAccelerationProperties()
{
#if !TARGET_OS_EMBEDDED
    bool        pointer             = false;
    OSArray     *deviceUsagePairs   = getDeviceUsagePairs();

    if (deviceUsagePairs && deviceUsagePairs->getCount()) {
        for (UInt32 index=0; index < deviceUsagePairs->getCount(); index++) {
            OSDictionary *pair = OSDynamicCast(OSDictionary, deviceUsagePairs->getObject(index));

            if (pair) {
                OSNumber *number = OSDynamicCast(OSNumber, pair->getObject(kIOHIDDeviceUsagePageKey));
                if (number) {
                    UInt32 usagePage = number->unsigned32BitValue();
                    if (usagePage != kHIDPage_GenericDesktop) {
                        continue;
                    }
                }

                number = OSDynamicCast(OSNumber, pair->getObject(kIOHIDDeviceUsageKey));
                if (number) {
                    UInt32 usage = number->unsigned32BitValue();
                    if (usage == kHIDUsage_GD_Mouse) {
                        if (!getProperty(kIOHIDPointerAccelerationTypeKey))
                            setProperty(kIOHIDPointerAccelerationTypeKey, kIOHIDMouseAccelerationType);

                        if (_scroll.elements) {
                            if (!getProperty(kIOHIDScrollAccelerationTypeKey))
                                setProperty(kIOHIDScrollAccelerationTypeKey, kIOHIDMouseScrollAccelerationKey);
                        }

                        return;
                    } else if (usage == kHIDUsage_GD_Pointer) {
                        pointer = true;
                    }
                }
            }
        }

        // this is a pointer only device
        if (pointer) {
            if (!getProperty(kIOHIDPointerAccelerationTypeKey))
                setProperty(kIOHIDPointerAccelerationTypeKey, kIOHIDPointerAccelerationKey);

            if (_scroll.elements) {
                if (!getProperty(kIOHIDScrollAccelerationTypeKey))
                    setProperty(kIOHIDScrollAccelerationTypeKey, kIOHIDScrollAccelerationKey);
            }
        }
    }
#endif /* TARGET_OS_EMBEDDED */
}
#endif

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// com_kensington_TrackballWorks2::calibrateCenteredPreferredStateElement
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
void com_kensington_TrackballWorks2::calibrateCenteredPreferredStateElement(IOHIDElement * element, SInt32 removalPercentage)
{
    UInt32 mid      = element->getLogicalMin() + ((element->getLogicalMax() - element->getLogicalMin()) / 2);
    UInt32 satMin   = element->getLogicalMin();
    UInt32 satMax   = element->getLogicalMax();
    UInt32 diff     = ((satMax - satMin) * removalPercentage) / 200;
    UInt32 dzMin    = mid - diff;
    UInt32 dzMax    = mid + diff;
    satMin          += diff;
    satMax          -= diff;
    TBLogDebug("[TBW] com_kensington_TrackballWorks2 - calibrateCenteredPreferredStateElement\n");
    element->setCalibration(-1, 1, satMin, satMax, dzMin, dzMax);
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// com_kensington_TrackballWorks2::calibrateJustifiedPreferredStateElement
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
void com_kensington_TrackballWorks2::calibrateJustifiedPreferredStateElement(IOHIDElement * element, SInt32 removalPercentage)
{
    UInt32 satMin   = element->getLogicalMin();
    UInt32 satMax   = element->getLogicalMax();
    UInt32 diff     = ((satMax - satMin) * removalPercentage) / 200;
    satMin          += diff;
    satMax          -= diff;
    TBLogDebug("[TBW] com_kensington_TrackballWorks2 - calibrateJustifiedPreferredStateElement\n");
    element->setCalibration(0, 1, satMin, satMax);
}

//====================================================================================================
// com_kensington_TrackballWorks2::parseElements
//====================================================================================================
bool com_kensington_TrackballWorks2::parseElements ( OSArray* elementArray, UInt32 bootProtocol)
{
    OSArray *   pendingElements         = NULL;
    OSArray *   pendingButtonElements   = NULL;
    bool        result                  = false;
    UInt32      count, index;
    TBLogDebug("[TBW] com_kensington_TrackballWorks2 - parseElements\n");
    if ( bootProtocol == kBootProtocolMouse )
        _bootSupport = kBootMouse;

    __Require_Action(elementArray, exit, result = false);

    _supportedElements = elementArray;
    _supportedElements->retain();

    for ( index=0, count=elementArray->getCount(); index<count; index++ ) {
        IOHIDElement *  element     = NULL;

        element = OSDynamicCast(IOHIDElement, elementArray->getObject(index));
        if ( !element )
            continue;

        if ( element->getReportID() != 0 )
            _multipleReports = true;

        if ( element->getType() == kIOHIDElementTypeCollection )
            continue;

        if ( element->getUsage() == 0 )
            continue;

        if (parseRelativeElement(element) ||
            parseScrollElement(element)
            ) {
            result = true;
            continue;
        }

        if (element->getUsagePage() == kHIDPage_Button) {
#if !TARGET_OS_EMBEDDED
            IOHIDElement *  parent = element;
            while ((parent = parent->getParentElement()) != NULL) {
                if (parent->getUsagePage() == kHIDPage_Consumer) {
                    break;
                }
            }
            if (parent != NULL) {
                continue;
            }
#endif
            if ( !pendingButtonElements ) {
                pendingButtonElements = OSArray::withCapacity(4);
                __Require_Action(pendingButtonElements, exit, result = false);
            }
            pendingButtonElements->setObject(element);
            continue;
        }

        if ( !pendingElements ) {
            pendingElements = OSArray::withCapacity(4);
            __Require_Action(pendingElements, exit, result = false);
        }

        pendingElements->setObject(element);
    }

    _digitizer.native = (_digitizer.transducers && _digitizer.transducers->getCount() != 0);

    if( pendingElements ) {
        for ( index=0, count=pendingElements->getCount(); index<count; index++ ) {
            IOHIDElement *  element     = NULL;

            element = OSDynamicCast(IOHIDElement, pendingElements->getObject(index));
            if ( !element )
                continue;

            //if ( parseDigitizerTransducerElement(element) )
            //    result = true;
        }
    }

    if( pendingButtonElements ) {
    TBLogDebug("[TBW] com_kensington_TrackballWorks2::parseElements(), count=%d\n", pendingButtonElements->getCount());
        for ( index=0, count=pendingButtonElements->getCount(); index<count; index++ ) {
            IOHIDElement *  element = NULL;

            element = OSDynamicCast(IOHIDElement, pendingButtonElements->getObject(index));
            if ( !element )
                continue;

            if (_relative.elements && _relative.elements->getCount()) {
                _relative.elements->setObject(element);
            } else if ( _gameController.capable ) {
                _gameController.elements->setObject(element);
            } else if ( _multiAxis.capable ) {
                _multiAxis.elements->setObject(element);
            /*
            } else if ( _digitizer.transducers && _digitizer.transducers->getCount() ) {
                DigitizerTransducer * transducer = OSDynamicCast(DigitizerTransducer, _digitizer.transducers->getObject(0));
                if ( transducer )
                    transducer->elements->setObject(element);
            */
            } else {
                if ( !_relative.elements ) {
                    _relative.elements = OSArray::withCapacity(4);
                }
                if ( _relative.elements ) {
                    _relative.elements->setObject(element);
                }
            }
        }
    }

    //processDigitizerElements();
    //processGameControllerElements();
    //processMultiAxisElements();
    //processUnicodeElements();

    //setRelativeProperties();
    //setDigitizerProperties();
    //setGameControllerProperties();
    //setMultiAxisProperties();
    //setScrollProperties();
    //setLEDProperties();
    //setKeyboardProperties();
    //setUnicodeProperties();
    //setAccelerationProperties();
    //setVendorMessageProperties();
    //setBiometricProperties();
    //setAccelProperties();
    //setGyroProperties();
    //setCompassProperties();
    //setTemperatureProperties();
    //setSensorProperties();
    //setDeviceOrientationProperties();


exit:

    if ( pendingElements )
        pendingElements->release();

    if ( pendingButtonElements )
        pendingButtonElements->release();

    return result || _bootSupport;
}

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// IOHIDEventDriver::setButtonState
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
static inline void setButtonState(UInt32 * state, UInt32 bit, UInt32 value)
{
    UInt32 buttonMask = (1 << bit);

    if ( value != 0 )
        (*state) |= buttonMask;
    else
        (*state) &= ~buttonMask;
}

//====================================================================================================
// com_kensington_TrackballWorks2::handleRelativeReport
//====================================================================================================
bool com_kensington_TrackballWorks2::handleRelativeReport(AbsoluteTime timeStamp, UInt32 reportID)
{
    bool        handled     = false;
    bool        btn_event   = false;
    bool        ptr_event   = false;
    SInt32      dX          = 0;
    SInt32      dY          = 0;
    UInt32      buttonState = 0;
    UInt32      index, count;

    __Require_Quiet(!_relative.disabled, exit);

    __Require_Quiet(_relative.elements, exit);
    TBLogDebug("[TBW] com_kensington_TrackballWorks2::handleRelativeReport()\n");
    for (index=0, count=_relative.elements->getCount(); index<count; index++) {
        IOHIDElement *  element;
        AbsoluteTime    elementTimeStamp;
        UInt32          usagePage, usage;
        bool            elementIsCurrent;

        element = OSDynamicCast(IOHIDElement, _relative.elements->getObject(index));
        if ( !element )
            continue;

        elementTimeStamp = element->getTimeStamp();
        elementIsCurrent = (element->getReportID()==reportID) && (CMP_ABSOLUTETIME(&timeStamp, &elementTimeStamp)==0);
        TBLogDebug("[TBW] com_kensington_TrackballWorks2::handleRelativeReport() - elementIsCurrent: %d\n", elementIsCurrent);

        // relative report, set flag
        handled |= elementIsCurrent;

        usagePage   = element->getUsagePage();
        usage       = element->getUsage();

        switch ( usagePage ) {
            case kHIDPage_GenericDesktop:
                switch ( element->getUsage() ) {
                    case kHIDUsage_GD_X:
                    {
                        TBLogDebug("[TBW] com_kensington_TrackballWorks2::handleRelativeReport() - kHIDUsage_GD_X, dX=%d\n", dX);
                        if (handled)
                        {
                            ptr_event = true;
                            dX = elementIsCurrent ? element->getValue() : 0;
                        }
                    }
                    break;
                    case kHIDUsage_GD_Y:
                    {
                        TBLogDebug("[TBW] com_kensington_TrackballWorks2::handleRelativeReport() - kHIDUsage_GD_Y, dY=%d\n", dY);
                        if (handled)
                        {
                            ptr_event = true;
                            dY = elementIsCurrent ? element->getValue() : 0;
                        }
                    }
                    break;
                }
            break;
            case kHIDPage_Button:
            {
                TBLogDebug("[TBW] com_kensington_TrackballWorks2::handleRelativeReport() - kHIDPage_Button\n");
#if 0                
                if (handled)
                {
                    btn_event = true;
                    TBLogDebug("[TBW] com_kensington_TrackballWorks2::handleRelativeReport() - usage:%d, element->getValue() is:%d\n",
                    usage, element->getValue());
                    setButtonState(&buttonState, (usage - 1), element->getValue());
                    TBLogDebug("[TBW] com_kensington_TrackballWorks2::handleRelativeReport() - now buttonState is:%d\n", buttonState);
                    if(buttonState == _service->_tbwConfig.curPhysButtonState) // vannes
                    {
                        TBLogDebug("[TBW] com_kensington_TrackballWorks2::handleRelativeReport() - redundant report\n");
                        btn_event = false;
                    }
                }
#else
                TBLogDebug("[TBW] com_kensington_TrackballWorks2::handleRelativeReport() - usage:%d, element->getValue() is:%d\n",
                    usage, element->getValue());
                setButtonState(&buttonState, (usage - 1), element->getValue());
                TBLogDebug("[TBW] com_kensington_TrackballWorks2::handleRelativeReport() - now buttonState is:%d\n", buttonState);
                if (handled)
                {
                    btn_event = true;
                    //if(buttonState == _service->_tbwConfig.curPhysButtonState)
                    //{
                    //    TBLogDebug("[TBW] com_kensington_TrackballWorks2::handleRelativeReport() - redundant report\n");
                    //    btn_event = false;
                    //}
                }
#endif
            }
            break;
        }
    }

    // If not a relative report (handled == false), exit
    __Require_Quiet(handled, exit);
    if(buttonState == _service->_tbwConfig.curPhysButtonState)
    {
        TBLogDebug("[TBW] com_kensington_TrackballWorks2::handleRelativeReport() - redundant report\n");
        btn_event = false;
    }

    // Proecss relative report. Could be either dx, dy or button events
    if(_service->_tbwConfig.fHelperConnected)
    {
        _service->postPointerEvent(dX, dY, buttonState, btn_event, ptr_event);
    }
    else
    {
        TBLogDebug("[TBW] getHelperConnectedStatus - service is not connected \n");
        dispatchRelativePointerEvent(timeStamp, dX, dY, buttonState);
    }
    return true;

exit:
    return false;
}


//====================================================================================================
// IOHIDEventDriver::handleScrollReport
//====================================================================================================
bool com_kensington_TrackballWorks2::handleScrollReport(AbsoluteTime timeStamp, UInt32 reportID)
{
    IOFixed     scrollVert  = 0;
    IOFixed     scrollHoriz = 0;
    bool        handled = false;

    UInt32      index, count;

    __Require_Quiet(_scroll.elements, exit);
    TBLogDebug("[TBW] com_kensington_TrackballWorks2::handleScrollReport - start\n");
    for (index=0, count=_scroll.elements->getCount(); index<count; index++) {
        IOHIDElement *  element;
        AbsoluteTime    elementTimeStamp;
        UInt32          usagePage, usage;

        element = OSDynamicCast(IOHIDElement, _scroll.elements->getObject(index));
        if ( !element )
            continue;

        if ( element->getReportID() != reportID )
            continue;

        elementTimeStamp = element->getTimeStamp();
        if ( CMP_ABSOLUTETIME(&timeStamp, &elementTimeStamp) != 0 )
            continue;

        usagePage   = element->getUsagePage();
        usage       = element->getUsage();

        switch ( usagePage ) {
            case kHIDPage_GenericDesktop:
                switch ( usage ) {
                    case kHIDUsage_GD_Wheel:
                    case kHIDUsage_GD_Dial:
                    {
                        scrollVert = ((element->getFlags() & kIOHIDElementFlagsWrapMask) ? element->getValue(kIOHIDValueOptionsFlagRelativeSimple) : element->getValue()) /*<< 16*/;
                        TBLogDebug("[TBW] TrackballWorks2::handleScrollReport - scroll event, offset: %d\n",
                                   scrollVert);
                        if (_service->_tbwConfig.fHelperConnected)
                        {
                            if(scrollVert != 0)//This means direction: 1 or -1, if zero we don't handle 
                                _service->postWheelEvent(1, scrollVert);
                        }
                        else
                            TBLogDebug("[TBW] TrackballWorks2::handleScrollReport - service is not connected \n");

                        handled = true;
                    }
                        break;
                    case kHIDUsage_GD_Z:
                        // Not sure how to handle this
                        scrollHoriz = ((element->getFlags() & kIOHIDElementFlagsWrapMask) ? element->getValue(kIOHIDValueOptionsFlagRelativeSimple) : element->getValue()) /*<< 16*/;
                        handled = true;
                        break;
                    default:
                        break;
                }
                break;
            case kHIDPage_Consumer:
                switch ( usage ) {
                    case kHIDUsage_Csmr_ACPan:
                        scrollHoriz = (element->getValue()) /*<< 16*/;
                        TBLogDebug("[TBW] TrackballWorks2::handleScrollReport - ACPan event, offset: %d\n",
                                   scrollHoriz);
                        if(_service->_tbwConfig.fHelperConnected)                        
                            _service->postWheelEvent(2, scrollHoriz);
                        else
                            TBLogDebug("[TBW] kHIDPage_Consumer: service is not connected \n");
                            
                        handled = true;
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }
    }
    if(!handled)
        TBLogDebug("[TBW] TrackballWorks2::handleScrollReport - It isn't handled.\n");

    __Require_Quiet(handled, exit); // vannes20210405


    //if (_service->getHelperConnectedStatus() == 0)
    if (!_service->_tbwConfig.fHelperConnected) // when service is not connected
        dispatchScrollWheelEvent(timeStamp, scrollVert, -scrollHoriz, 0);

exit:
    return handled;
}

/**
 Enumerate the AddlMatchAttributes dictionary and for each key in there
 match with value from the current recursing to ancestors IORegistryEntry
 values.
 */
bool com_kensington_TrackballWorks2::matchAddlAttributes(IOService* provider)
{
    bool fMatch = true;
    /*
    OSDictionary* driverProps = dictionaryWithProperties();
    if (driverProps) {
        printDictionaryKeys(driverProps, "TrackballWorks2: ");
        driverProps->release();
    }
    */
    TBLogDebug("[TBW] com_kensington_TrackballWorks2::matchAddlAttributes - EMPTY AddlMatchAttributes\n");   
    const char* ADDL_MATCH_ATTRS_NAME = "AddlMatchAttributes";
    OSDictionary* osnAddlMatchKeys = OSDynamicCast(OSDictionary, getProperty(ADDL_MATCH_ATTRS_NAME));

    if (!osnAddlMatchKeys) {
        TBLogDebug("[TBW] com_kensington_TrackballWorks2::matchAddlAttributes - EMPTY AddlMatchAttributes\n");
        return true;
    }
    
    OSCollectionIterator * mcoll = OSCollectionIterator::withCollection (osnAddlMatchKeys);
    OSSymbol * mkey;
    OSString * ioClass;
    unsigned int i = 0;
    
    mcoll->reset ();
    
    mkey = OSDynamicCast (OSSymbol, mcoll->getNextObject ());
    
    while (mkey) {
        const char* MATCH_KEY = mkey->getCStringNoCopy();
    TBLogDebug("[TBW] com_kensington_TrackballWorks2::matchAddlAttributes - matching values for \"%s\"n",
            MATCH_KEY);

        OSObject* obj = osnAddlMatchKeys->getObject(mkey);
        OSNumber* number = OSDynamicCast(OSNumber, obj);
        OSString* strValue = OSDynamicCast(OSString, obj);
        OSBoolean* boolValue = OSDynamicCast(OSBoolean, obj);
        OSObject* providerObj = copyProperty(MATCH_KEY,
                 gIOServicePlane,
                 kIORegistryIterateRecursively|kIORegistryIterateParents);
        if (number) {
            int plistValue = (int)number->unsigned32BitValue();
            OSNumber* osnProviderValue = OSDynamicCast(OSNumber, providerObj);

            int providerValue = -1;
            // values for the specified key exists in both Info.plist and provider IORegistryEntry
            // So these two should match. If they don't, we won't create the secondary virtual
            // device object (TrackballWorks2Service).
            if (osnProviderValue) {
                providerValue = (int)osnProviderValue->unsigned32BitValue();
            }

            if (plistValue != providerValue) {
                TBLogDebug("\t[TBW] com_kensington_TrackballWorks2: values DO NOT match, plist: %d, Provider: %d\n",
                    plistValue, providerValue);
                fMatch = false;
                break;
            } else {
                TBLogDebug("\t[TBW] com_kensington_TrackballWorks2 - values DO match, plist: %d, Provider: %d\n",
                    plistValue, providerValue);
            }
        } else if (strValue) {
            const char* plistValue = strValue->getCStringNoCopy();
            OSString* osnProviderValue = OSDynamicCast(OSString, providerObj);
            
            const char* providerValue = "";
            if (osnProviderValue) {
                providerValue = osnProviderValue->getCStringNoCopy();
            }
            
            if (::strcmp(plistValue, providerValue) != 0) {
                TBLogDebug("\t[TBW] com_kensington_TrackballWorks2 - values DO NOT match, plist: %s, Provider: %s\n",
                    plistValue, providerValue);
                fMatch = false;
                break;
            } else {
                TBLogDebug("\t[TBW] com_kensington_TrackballWorks2 - values DO match, plist: %s, Provider: %s\n",
                    plistValue, providerValue);

            }
        } else if (boolValue) {
            bool plistValue = boolValue->getValue();
            OSBoolean* osnProviderValue = OSDynamicCast(OSBoolean, providerObj);

            bool providerValue = false;
            // values for the specified key exists in both Info.plist and provider IORegistryEntry
            // So these two should match. If they don't, we won't create the secondary virtual
            // device object (TrackballWorks2Service).
            if (osnProviderValue) {
                providerValue =osnProviderValue->getValue();
            }

            if (plistValue != providerValue) {
                TBLogDebug("\t[TBW] com_kensington_TrackballWorks2 - values DO NOT match, plist: %d, Provider: %d\n",
                    plistValue, providerValue);
                fMatch = false;
                break;
            } else {
                TBLogDebug("\t[TBW] com_kensington_TrackballWorks2 - values DO match, plist: %d, Provider: %d\n",
                    plistValue, providerValue);
            }
        } else {
            TBLogDebug("\t[TBW] com_kensington_TrackballWorks2 - Unknown key type\n");
        }
        
        mkey = (OSSymbol *) mcoll->getNextObject ();
    }
    mcoll->release();
    
    return fMatch;
}
