#include <IOKit/IOLib.h>
#include "DriverUSB.hpp"
#include "service.hpp"
#include "tblog.h"
#include "util.h"
#include "hardware.h"

// All our recognized HID input reports are 4 bytes long
#define INPUT_REPORT_LENGTH  4

OSDefineMetaClassAndStructors(com_kensington_TrackballWorks2USB, IOUSBHostHIDDevice)


bool com_kensington_TrackballWorks2USB::handleStart(IOService* provider)
{
    bool res = super::handleStart(provider);
    if (!res)
    {
        TBLogError("[TBW] Superclass handleStart failed\n");
        return res;
    }
    
    _interfaceNumber = -1;
    _numInputReportElements = 0;
    _service = NULL;
    _ft = FirmwareType::kFTUnknown;
    
    _ft = getFirmwareType(this, "idProduct");
    if (_ft == FirmwareType::kFTUnknown)
    {
        TBLogError("[TBW] KWDriver - unknown firmware type");
        return false;
    }

    TBLogInfo("[TBW] KWDriver - firmware type: %d", _ft);
    
    OSNumber* interfaceNumber = OSDynamicCast(OSNumber, getProperty("bInterfaceNumber"));
    if (interfaceNumber)
    {
        _interfaceNumber = (int)interfaceNumber->unsigned32BitValue();
        TBLogDebug("[TBW] KWDriverUSB::start - interfaceNumber: %d\n", _interfaceNumber);
    }
    
    // Read number of input report elements stored in in the I/O registry,
    // which tells us the kind of device (and hence the kind of HID reports)
    // that we're dealing with. Kensington trackball (and presenter) devices now
    // only consiste of two kinds of firmware. One legacy firmware that only
    // sends one report (and hence the InputReportElements will be 1) and the
    // latter generation devices that send three HID reports for each interrupt.
    // In the handleReport method, we'll use this value to cast the input byte
    // array to our own HIDReport* structure matching the device's report format.
    OSObject* obj = getProperty("InputReportElements");
    OSArray* inputReportElements = OSDynamicCast(OSArray, obj);
    if (inputReportElements)
    {
        // _numInputReportElements = inputReportElements->getCount();
        TBLogDebug("[TBW] KWDriverUSB::InputReportElements count: %d\n", _numInputReportElements);
    }
    else
    {
        TBLogDebug("[TBW] KWDriver - input report elements could not be cast to array");
    }

    if (interfaceNumber->unsigned32BitValue() == 0) {
        // create service, only for interface 0
        _service = TrackballWorks2Service::withProvider(this);
        if (!_service)
        {
            TBLogError("Error initializing TrackballService\n");
            return false;
        }
    }
    
    return res;
}


void com_kensington_TrackballWorks2USB::detach(IOService* provider)
{
    TBLogDebug("[TBW] com_kensington_TrackballWorks2USB::detach - entry\n");
    super::detach(provider);
    TBLogDebug("[TBW] com_kensington_TrackballWorks2USB::detach - exit\n");
    //_service->release();
}

void com_kensington_TrackballWorks2USB::handleStop(IOService* provider)
{
    TBLog("[TBW] KWDriverUSB::handleStop - interface: %d\n", _interfaceNumber);
    
    super::handleStop(provider);
}

IOReturn com_kensington_TrackballWorks2USB::handleReport(
                                     IOMemoryDescriptor* report,
                                     IOHIDReportType reportType/* = kIOHIDReportTypeInput*/,
                                     IOOptionBits options/*=0*/)
{
    //IOReturn res = IOHIDDevice::handleReport(report, reportType, options);
    //TBLog("KWDriverUSB::handleReport(%d) - report len: %d, reportType: %d, options: %04x\n", _interfaceNumber,
    //      report->getLength(), reportType, options);
    bool fHandled = false;
    if (report && report->getLength() > 0 && reportType == kIOHIDReportTypeInput)
    {
        IOMemoryMap* mapping = report->createMappingInTask(kernel_task,
                                                           0,
                                                           kIOMapAnywhere);
        if (mapping)
        {
            UInt8* report_bytes = reinterpret_cast<UInt8*>(mapping->getAddress());
            if (FirmwareType::kFTExpertMouse == _ft) {
                fHandled = handleExpertMouseFirwareReport(report_bytes, report->getLength());
            } else if (FirmwareType::kFTLegacy == _ft) {
                fHandled = handleLegacyFirmwareReport(report_bytes, report->getLength());
            } else {
                // do nothing!
            }
            
            mapping->release();
        }
    }
    if (!fHandled) {
        return IOHIDDevice::handleReport(report, reportType, options);
    }

    return KERN_SUCCESS;
}

bool com_kensington_TrackballWorks2USB::handleLegacyFirmwareReport(UInt8* report, IOByteCount len)
{
    HIDReport1* hidr = reinterpret_cast<HIDReport1*>(report);
    TBLogDebug("[TBW] KWDriver(tid: %llu) - HID report1: buttons: %x, dx: %d, dy: %d, wheel: %d\n",
               thread_tid(current_thread()),
               hidr->buttons, hidr->x, hidr->y, hidr->wheel);

    _service->postPointerEvent(hidr->x, hidr->y, hidr->buttons, true);

    if (hidr->wheel)
        _service->postWheelEvent(hidr->wheel);
    
    return true;
}

bool com_kensington_TrackballWorks2USB::handleExpertMouseFirwareReport(UInt8* report, IOByteCount len)
{
    bool fHandled = false;
    
    HIDReport2* hidr = reinterpret_cast<HIDReport2*>(report);
    if (hidr->reportId == 0x01)
    {
        TBLogDebug("\t[TBW] KWDriver - HID report2 - buttons: %x, wheel: %d",
                   (unsigned)hidr->u.ButtonReport.buttons,
                   (int)hidr->u.ButtonReport.wheel);

        _service->postPointerEvent(0, 0, hidr->u.ButtonReport.buttons, true);

        if (hidr->u.ButtonReport.wheel) {
            _service->postWheelEvent(hidr->u.ButtonReport.wheel);
        }

        fHandled = true;
    }
    else if (hidr->reportId == 0x02)
    {
        TBLogDebug("\t[TBW] KWDriver - HID report2 - x: %d, y: %d",
                   (int)hidr->u.PointerReport.x,
                   (int)hidr->u.PointerReport.y);
        
        _service->postPointerEvent(hidr->u.PointerReport.x, hidr->u.PointerReport.y, 0, true);
        fHandled = true;
    }
    else if (hidr->reportId == 0x03)
    {
        
    }
    
    return fHandled;
}
