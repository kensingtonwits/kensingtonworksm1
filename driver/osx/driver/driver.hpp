//
//  DriverUSB2.hpp
//  driver
//
//  Created by Hariharan Mahadevan on 2019/2/20.
//  Copyright © 2019 Kensington. All rights reserved.
//

#pragma once

#include <IOKit/hidevent/IOHIDEventDriver.h>
#include <IOKit/hidsystem/IOHIDTypes.h>
#include <IOKit/hidevent/IOHIDEventService.h>
#include <IOKit/hid/IOHIDInterface.h>

#include "service.hpp"

class com_kensington_TrackballWorks2 : public IOHIDEventDriver
{
    OSDeclareDefaultStructors(com_kensington_TrackballWorks2)
    
    typedef IOHIDEventDriver super;
    
    uint16_t _productID;        // product id
    uint16_t _vendorID;         // vendor id
    uint32_t _interfaceNumber;    // unused
    TrackballWorks2Service* _service;
    
    // IOHIDEventDriver.h

    IOHIDInterface *            _interface;
    void *                      _reservedVoid[3] __unused;
    
    OSArray *                   _supportedElements;
    
    UInt32                      _bootSupport;
    bool                        _multipleReports;
    bool                        _authenticatedDevice;
    bool                        _reservedBool   __unused;
    UInt32                      _reservedUInt32 __unused;
    bool                        _reservedBool1  __unused;
    bool                        _bhasACPanButtons = false; // *&*&*&Va_20200922 add
    
    struct ExpansionData {
        SInt32  absoluteAxisRemovalPercentage;
        SInt32  preferredAxisRemovalPercentage;
        
        struct {
            OSArray *           elements;
        } led;
        
        struct {
            OSArray *           elements;
        } scroll;
        
        struct {
            OSArray *           elements;
            bool                disabled;
        } relative;
        
        struct {
            OSArray *           elements;
            UInt8               bootMouseData[4];
            bool                appleVendorSupported;
        } keyboard;
        
        struct {
            IOHIDElement *      deviceModeElement;
            OSArray *           transducers;
            IOHIDElement *      touchCancelElement;
            bool                native;
            bool                collectionDispatch;
            IOFixed             centroidX;
            IOFixed             centroidY;
        } digitizer;
        
        struct {
            OSArray *           elements;
            
            UInt32              capable;
            UInt32              sendingReportID;
            IOFixed             axis[6];
            UInt32              buttonState;
            IOOptionBits        options;
            bool                disabled;
        } multiAxis;
        
        struct {
            IOHIDElement *      gestureStateElement;
            OSArray *           legacyElements;
            OSArray *           gesturesCandidates;
        } unicode;
        
        struct {
            bool                extended;
            bool                formFitting;
            OSArray *           elements;
            UInt32              capable;
            UInt32              sendingReportID;
            
            struct {
                IOFixed up;
                IOFixed down;
                IOFixed left;
                IOFixed right;
            } dpad;
            
            struct {
                IOFixed x;
                IOFixed y;
                IOFixed a;
                IOFixed b;
            }face;
            
            struct {
                IOFixed x;
                IOFixed y;
                IOFixed z;
                IOFixed rz;
            } joystick;
            
            struct {
                IOFixed l1;
                IOFixed l2;
                IOFixed r1;
                IOFixed r2;
            } shoulder;
            
        } gameController;
        
        struct {
            OSArray *           childElements;
            OSArray *           primaryElements;
            OSArray *           pendingEvents;
        } vendorMessage;
        
        struct {
            OSArray *           elements;
        } biometric;
        
        struct {
            OSArray *           elements;
        } accel;
        
        struct {
            OSArray *           elements;
        } gyro;
        
        struct {
            OSArray *           elements;
        } compass;
        
        struct {
            OSArray *           elements;
        } temperature;
        
        struct {
            IOHIDElement *      reportInterval;
            IOHIDElement *      reportLatency;
            IOHIDElement *      sniffControl;
        } sensorProperty;
        
        struct {
            OSArray *           elements;
        } orientation;
        
        UInt64  lastReportTime;
        
        IOWorkLoop *            workLoop;
        IOCommandGate *         commandGate;
    };
    
    ExpansionData *             _reserved;
    // IOHIDEventDriver.h
    
private:
    // IOHIDEventDriver.h
    bool                    parseElements(OSArray * elementArray, UInt32 bootProtocol);
    bool                    parseRelativeElement(IOHIDElement * element);
    bool                    parseScrollElement(IOHIDElement * element);
    
    static void             calibrateJustifiedPreferredStateElement(IOHIDElement * element, SInt32 removalPercentage);
    static void             calibrateCenteredPreferredStateElement(IOHIDElement * element, SInt32 removalPercentage);

    bool                    handleRelativeReport(AbsoluteTime timeStamp, UInt32 reportID);
    bool                    handleScrollReport(AbsoluteTime timeStamp, UInt32 reportID);
    // IOHIDEventDriver.h
    
    bool                    matchAddlAttributes(IOService* provider);
    
    
public:
    virtual bool init(OSDictionary * dictionary = 0) override;
    virtual void free() override;
    
    virtual bool handleStart(IOService* provider) override;
    virtual void handleStop(IOService* provider) override;

    virtual void handleInterruptReport(AbsoluteTime timeStamp,
                                       IOMemoryDescriptor* report,
                                       IOHIDReportType reportType,
                                       UInt32 reportID) override;
};
