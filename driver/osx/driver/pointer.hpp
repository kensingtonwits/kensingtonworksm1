//
//  ServiceHIDPointer.hpp
//  trackball2
//
//  Created by Hariharan Mahadevan on 2019/1/25.
//  Copyright © 2019 Kensington. All rights reserved.
//

#pragma once

#include <IOKit/hidsystem/IOHIPointing.h>

class TrackballWorks2Pointer : public IOHIPointing {

    OSDeclareDefaultStructors(TrackballWorks2Pointer)

    typedef IOHIPointing super;

    UInt32  _curButtonState;
public:
    static TrackballWorks2Pointer* withProvider(IOService* provider);

#if 0
    virtual void free() override;
    virtual void detach(IOService* provider) override;
    virtual bool terminate( IOOptionBits options = 0 ) override;
    virtual bool finalize( IOOptionBits options ) override;
#endif

    virtual bool start(IOService* provider) override;
    virtual void stop(IOService* provider) override;

    virtual OSData *    copyAccelerationTable() override;
    virtual IOItemCount buttonCount() override;
    virtual IOFixed     resolution() override;
    virtual IOReturn    setParamProperties( OSDictionary * dict ) override;

    void dispatchButtonEvent(UInt32 buttonState);
    void dispatchCursorEvent(int dx, int dy);

    void dispatchPointerEvent(int dx, int dy, UInt32 buttonState);
    /**
     * unitAxis1 - Vertical axis
     * unitAxis2 - Horizontal axis (pan)
     * unitAxis3 - Z axis?
     */
    void dispatchScrollEvent(short unitsAxis1, short unitsAxis2, short unitsAxis3);
};
