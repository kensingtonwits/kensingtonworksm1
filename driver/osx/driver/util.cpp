//
//  util.c
//  trackball2
//
//  Created by Hariharan Mahadevan on 1/22/19.
//  Copyright © 2019 Kensington. All rights reserved.
//

#include "util.h"
#include "tblog.h"

OSString* getDriverVersion(IOService* provider)
{
    OSString* version = OSDynamicCast(OSString,
                                      provider->getProperty("CFBundleShortVersionString"));
    OSString* build = OSDynamicCast(OSString,
                                    provider->getProperty("CFBundleVersion"));

    if (version && build)
    {
        char szVersion[32] = {0};
        ::strncpy(szVersion, version->getCStringNoCopy(), version->getLength());
        ::strncat(szVersion, build->getCStringNoCopy(), build->getLength());
        
        return OSString::withCString(szVersion);
    }
    
    TBLogError("Error reading version information from plist\n");
    
    return OSString::withCString("");
}

static void
getCStringForObject(OSObject *inObj, char *outStr, size_t outStrLen)
{
    char * buffer;
    unsigned int    len, i;
    
    if ( (NULL == inObj) || (NULL == outStr))
        return;
    
    char * objString = (char *) (inObj->getMetaClass())->getClassName();
    
    if ((0 == strncmp(objString, "OSString", sizeof("OSString"))) ||
        (0 == strncmp(objString, "OSSymbol", sizeof("OSSymbol"))))
        strlcpy(outStr, ((OSString *)inObj)->getCStringNoCopy(), outStrLen);
    
    else if (0 == strncmp(objString, "OSData", sizeof("OSData"))) {
        len = ((OSData *)inObj)->getLength();
        buffer = (char *)((OSData *)inObj)->getBytesNoCopy();
        if (buffer && (len > 0)) {
            for (i=0; i < len; i++) {
                outStr[i] = buffer[i];
            }
            outStr[len] = 0;
        }
    }
}

//*********************************************************************************
// printDictionaryKeys
//
// Print the keys for the given dictionary and selected contents.
//*********************************************************************************
void printDictionaryKeys (const OSDictionary * inDictionary, const char * inMsg)
{
    OSCollectionIterator * mcoll = OSCollectionIterator::withCollection (inDictionary);
    OSSymbol * mkey;
   
    mcoll->reset ();
    mkey = OSDynamicCast (OSSymbol, mcoll->getNextObject ());
    
    TBLogDebug("Printing %s dictionary contents\n", inMsg);
    while (mkey) {
        OSObject* obj = inDictionary->getObject(mkey);
        OSNumber* number = OSDynamicCast(OSNumber, obj);
        OSString* strValue = OSDynamicCast(OSString, obj);
        OSBoolean* boolValue = OSDynamicCast(OSBoolean, obj);
        if (number) {
            TBLogDebug("\tnum: %s = %llu\n", mkey->getCStringNoCopy(), number->unsigned64BitValue());
        } else if (strValue) {
            TBLogDebug("\tstr: %s = %s\n", mkey->getCStringNoCopy(), strValue->getCStringNoCopy());
        } else if (boolValue) {
            TBLogDebug("\tbool: %s = %s\n", mkey->getCStringNoCopy(), boolValue->getValue() ? "TRUE" : "FALSE");
        } else {
            TBLogDebug("\tUnknown type: key: %s\n", mkey->getCStringNoCopy ());
        }
        
        mkey = (OSSymbol *) mcoll->getNextObject ();
    }
    
    mcoll->release ();
}
