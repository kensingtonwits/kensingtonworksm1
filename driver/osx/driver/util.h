	//
//  util.h
//  trackball2
//
//  Created by Hariharan Mahadevan on 1/22/19.
//  Copyright © 2019 Kensington. All rights reserved.
//

#pragma once

#include <IOKit/IOService.h>
#include <libkern/c++/OSString.h>
//#include "hardware.h"

/*
  Returns the driver version as an OString instance, with
  a reference count of 1.

  Args:
    provider - Driver instance
 
  Returns:
    Three quadrant version string as a OString object. If version
    properties could not be read from the plist file, returns an
    empty string.
    
  Notes:
    Caller has to free the returned OSString.
 
   THIS DOESN'T WORK AS VERSION IS STORED IN THE PLIST FILE OUTSIDE
   OF THE DRIVER MATCH PERSONALITY DICTIONARY. DRIVER CODE ONLY GETS
   THE PERSONALITY DICTIONARY FOR THE MATCHING CODE. WE HAVE TO FIGURE
   OUT A DIFFERENT WAY TO MAKE THIS WORK.
 */
OSString* getDriverVersion(IOService* provider);

// Print the keys in a dictionary
void printDictionaryKeys (const OSDictionary * inDictionary, const char * inMsg);
