//
//  DriverService.cpp
//  trackball2
//
//  Created by Hariharan Mahadevan on 2019/1/24.
//  Copyright © 2019 Kensington. All rights reserved.
//

#include <IOKit/hidsystem/IOHIDDescriptorParser.h>
#include "service.hpp"
#include "keyboard.hpp"
#include "pointer.hpp"
#include "userclient.hpp"
#include "driver.hpp"

#include "tblog.h"

OSDefineMetaClassAndStructors(TrackballWorks2Service, IOService)

#define ABS(x) (x >= 0 ? x : x*-1)

#define RELEASE_CHILD_DEVICE(driver)        driver->stop(this); \
                                            driver->release(); \
                                            driver = nullptr;

/*
kVK_Command                   = 0x37,
kVK_Shift                     = 0x38,
kVK_CapsLock                  = 0x39,
kVK_Option                    = 0x3A,
kVK_Control                   = 0x3B,
*/

void dumpButtonsConfig(const TBWBUTTONSCONFIG* pCfg)
{
    TBLogDebug("[TBW] TrackballWorks2Service - Buttons config:-\n");
    TBLogDebug("\t[TBW] TrackballWorks2Service - chordMask: 0x%08x, nButtons: %d\n", pCfg->chordMask, pCfg->nButtons);
    for (uint32_t i=0; i<pCfg->nButtons; i++) {
        const TBWBUTTONACTION& action = pCfg->buttons[i];
        TBLogDebug("\t\t[TBW] TrackballWorks2Service -IN Mask: 0x%08x\n",
            pCfg->buttons[i].buttonMask);
        if (action.action == BUTTON_ACTION_CLICK) {
            TBLogDebug("\t\t[TBW] TrackballWorks2Service -CLICK - OUT Mask: 0x%08x\n", action.u.ClickActionParams.buttonMask);
        } else if (action.action == BUTTON_ACTION_SCROLL) {
            TBLogDebug("\t\t[TBW] TrackballWorks2Service -SCROLL - direction: %d, lines: %d\n",
                action.u.ScrollActionParams.direction,
                action.u.ScrollActionParams.lines);
        } else if (action.action == BUTTON_ACTION_DRAG) {
                TBLogDebug("[TBW] TrackballWorks2Service - CLICK - OUT Mask: 0x%08x", action.u.ClickActionParams.buttonMask);
        }
    }
}

TrackballWorks2Service* TrackballWorks2Service::withProvider(IOService *provider)
{
    TBLogDebug("[TBW] TrackballWorks2Service - withProvider\n");
    TrackballWorks2Service* self = new TrackballWorks2Service();
    if (!self || !self->init(NULL))
    {
        if (self)
        {
            TBLogError("[TBW] TrackballWorks2Service - error initializing\n");
            self->release();
        }
        return NULL;
    }

    // attach it ourselves
    if (!self->attach(provider))
    {
        TBLogError("[TBW] TrackballWorks2Service - error attaching\n");
        self->release();
        return NULL;
    }

    // start it
    if (!self->start(provider))
    {
        TBLogError("[TBW] TrackballWorks2Service - error starting\n");
        self->release();
        return NULL;
    }

    TBLogDebug("[TBW] TrackballWorks2Service successfuly started\n");
    return self;
}

OSObject* TrackballWorks2Service::getDriver()
{
    return _driver;
}

void TrackballWorks2Service::timerFn(IOTimerEventSource* sender)
{
    TBLogDebug("[TBW] TrackballWorks2Service - timerFn: thread: %llu\n", thread_tid(current_thread()));
    /*
    postWheelEvent(1);
    if (++_timerScrolls < 5) {
        TBLogDebug("TrackballWorks2Service - queing another timer event\n");
        _timerEventSource->setTimeoutMS(1000);
    } else {
        TBLogDebug("TrackballWorks2Service - MAX timer events reached, not queing another timer event\n");
    }
    */
    // Can't use switch-case here, not digit
    //if (sender == _timerDetectChords)
    //    TBHandleTimeOut(&_tbwConfig, TIMER_ID_CHORD);
    //else if (sender == _timerInertialScroll)
    if (sender == _timerInertialScroll)
        TBHandleTimeOut(&_tbwConfig, TIMER_ID_INERTIAL_SCROLL);
    else if(sender == _timerAutoScroll) // vannes20200518
        TBHandleTimeOut(&_tbwConfig, TIMER_ID_AUTO_SCROLL);
    else if (sender == _timerDebounce) // *&*&*&_V1_20200914
        TBHandleTimeOut(&_tbwConfig, TIMER_ID_DEBOUNCE);
    else if (sender == _timerTiltButton) // *&*&*&V1_20210119
        TBHandleTimeOut(&_tbwConfig, TIMER_ID_TILT_BUTTON);

}

// External method, called from TrackballWorks2UserClient
void TrackballWorks2Service::registerHelperUserClient(TrackballWorks2UserClient* pUserClient)
{
    IOLockLock(_deviceLock);
    TBLogDebug("[TBW] TrackballWorks2Service - registerHelperUserClient\n");
    if (_userClient == nullptr)
    {
        TBLogDebug("[TBW] TrackballWorks2Service - Helper registered\n");
        _userClient = pUserClient;
        TBSetHelperConnected(&_tbwConfig, 1);
    }
    else
    {
        TBLogDebug("[TBW] TrackballWorks2Service::registerHelper - _userclient != nullptr\n");
    }
    IOLockUnlock(_deviceLock);
}

// External method, called from TrackballWorks2UserClient
void TrackballWorks2Service::clearHelperUserClient()
{
    IOLockLock(_deviceLock);
    TBLogDebug("[TBW] TrackballWorks2Service - clearHelperUserClient\n");
    if (_userClient != nullptr)
    {
        _userClient = nullptr;
        TBSetHelperConnected(&_tbwConfig, 0);
        if (_notifyEventDescriptor)
            _notifyEventDescriptor->release();
        _notifyEventDescriptor = nullptr;
        _notifyEventQueue->clear();
        TBLogDebug("[TBW] TrackballWorks2Service - Helper deregistered\n");
    }
    else
    {
        TBLogInfo("[TBW] TrackballWorks2Service - Helper deregistration request when userclient == nullptr\n");
    }

    IOLockUnlock(_deviceLock);
}

bool TrackballWorks2Service::start(IOService *provider)
{
    LOG_FUNCNAME();
    TBLogDebug("[TBW] TrackballWorks2Service - start\n");
    _keyboard = NULL;
    _pointer = NULL;
    _userClient = NULL;
    _driver = NULL;

    // "HIDMouseAcceleration" HID parameter controls Tracking Speed and
    // varies from 0 - 196608.
    // Default 98304
    //_pointerAccelerationRate = 0;

    // "HIDMouseScrollAcceleration" HID parameter controls Scrolling speed
    // and ranges from 0 - 327680.
    // Default 49152

    //_scrollSpeed = 1.0;

    memset(_notifyEventAsyncRef, 0, sizeof(OSAsyncReference64));
    _notifyEventDescriptor = nullptr;
    _notifyEventQueue = nullptr;

    if (!super::start(provider))
    {
        TBLogError("[TBW] Error starting TrackballWorks2Service superclass\n");
        return false;
    }

    _driver = OSDynamicCast(com_kensington_TrackballWorks2, provider);
    if (!_driver) {
        TBLogError("[TBW] TrackballWorks2Service::start - could not cast provider to DriverUSB or DriverBT");
        goto bailout;
    }

    _keyboard = TrackballWorks2Keyboard::withProvider(this);
    if (!_keyboard) {
        TBLogError("[TBW] TrackballWorks2Service::start - error instantiating ServiceHIDKeyboard\n");
        goto bailout;
    }

    _pointer = TrackballWorks2Pointer::withProvider(this);
    if (!_pointer) {
        TBLogError("[TBW] TrackballWorks2Service::start - error instantiating TrackballWorks2Pointer\n");
        goto bailout;
    }

    _deviceLock = IOLockAlloc();
    if (!_deviceLock) {
       TBLogError("[TBW] TrackballWorks2Service::start - failed to create mutex\n");
       goto bailout;
    }

    _workLoop = getWorkLoop();  // reuse base class IOService workloop
    if (!_workLoop) {
        TBLogError("[TBW] TrackballWorks2Service::start - failed to get workloop\n");
        goto bailout;
    }
    _workLoop->retain();

    _notifyEventQueue = CircularQueue::withElementSize(sizeof(TBWDRIVEREVENT), 64);
    if (_notifyEventQueue == nullptr) {
        TBLogError("[TBW] TrackballWorks2Service::start - failed to create circular queue\n");
        goto bailout;
    }

    // setup a tile-button timer
    _timerTiltButton = IOTimerEventSource::timerEventSource(
        this,
        TrackballWorks2Service::sTimerFn);
    // setup a chord timer (20210119 disabled)
    //_timerDetectChords = IOTimerEventSource::timerEventSource(
    //    this,
    //    TrackballWorks2Service::sTimerFn);
    // setup a inertial timer    
    _timerInertialScroll = IOTimerEventSource::timerEventSource(
        this,
        TrackballWorks2Service::sTimerFn);
    // setup a button-debounce timer
    _timerDebounce = IOTimerEventSource::timerEventSource( // *&*&*&Va_20200914
        this,
        TrackballWorks2Service::sTimerFn);
#ifdef SCROLL_WITH_TRACKBALL // setup an auto-scroll timer
    _timerAutoScroll = IOTimerEventSource::timerEventSource( // *&*&*&Va_20200518
        this,
        TrackballWorks2Service::sTimerFn);
    // allocate-fail-handling
    if (!_timerInertialScroll || !_timerAutoScroll
#else
    if (!_timerInertialScroll
#endif
        || !_timerDebounce   // *&*&*&V1_20200914
        || !_timerTiltButton // *&*&*&V1_20210119
    )
    {
        TBLogError("[TBW] TrackballWorks2Service::start - failed to create timers\n");
        goto bailout;
    }

    _workLoop->addEventSource(_timerTiltButton); // *&*&*&V1_20210119
    //_workLoop->addEventSource(_timerDetectChords);
    _workLoop->addEventSource(_timerInertialScroll);
#ifdef SCROLL_WITH_TRACKBALL
    _workLoop->addEventSource(_timerAutoScroll); // *&*&*&Va_20200518
#endif
    _workLoop->addEventSource(_timerDebounce); // *&*&*&Va_20200914 for button-debounce timer
 
    setProperty("IOUserClientClass", "TrackballWorks2UserClient");
    registerService();

    // Initialize XPlatform Trackball library
    TBInit(
        &_tbwConfig,
        this,
        TrackballWorks2Service::sTBPostButtonEvent,
        TrackballWorks2Service::sTBPostCursorEvent,
        TrackballWorks2Service::sTBPostWheelEvent,
        TrackballWorks2Service::sTBEnqueueButtonEvent,
        TrackballWorks2Service::sTBStartTimer,
        TrackballWorks2Service::sTBStopTimer);

    setDefaultButtonsConfig();

    TBLogDebug("\t[TBW] sucess\n");
    return true;

bailout:
    if (_timerTiltButton)       _timerTiltButton->release(); // *&*&*&V1_20210119
    //if (_timerDetectChords)     _timerDetectChords->release();
    if (_timerInertialScroll)   _timerInertialScroll->release();
    if (_timerDebounce)         _timerDebounce->release(); // *&*&*&V1_20200914
#ifdef SCROLL_WITH_TRACKBALL // *&*&*&V1_20200518
    if (_timerAutoScroll)       _timerAutoScroll->release();
    _timerAutoScroll = nullptr;
#endif
    // release timer-series
    _timerTiltButton     = nullptr;
    //_timerDetectChords   = nullptr;
    _timerInertialScroll = nullptr;
    _timerDebounce       = nullptr;
    // release queue
    if (_notifyEventQueue) CircularQueue::destroy(_notifyEventQueue);
    _notifyEventQueue = nullptr;
    if (_workLoop) _workLoop->release();
    _workLoop = nullptr;
    // release keyboard&pointing-service
    if (_keyboard) { RELEASE_CHILD_DEVICE(_keyboard); }
    if (_pointer) { RELEASE_CHILD_DEVICE(_pointer); }
    // release mutex
    if (_deviceLock) IOLockFree(_deviceLock);
    _deviceLock = nullptr;

    return false;
}

void TrackballWorks2Service::stop(IOService* provider)
{
    TBLogDebug("[TBW] TrackballWorks2Service::stop\n");

    IOLockLock(_deviceLock);

    if (_notifyEventDescriptor)
        _notifyEventDescriptor->release();
    _notifyEventDescriptor = nullptr;

    if (_notifyEventQueue)
        CircularQueue::destroy(_notifyEventQueue);
    _notifyEventQueue = nullptr;

    //if (_timerDetectChords) {
    //    _workLoop->removeEventSource(_timerDetectChords);
    //    _timerDetectChords->release();
    //    _timerDetectChords = nullptr;
    //}

    if (_timerInertialScroll) {
        _workLoop->removeEventSource(_timerInertialScroll);
        _timerInertialScroll->release();
        _timerInertialScroll = nullptr;
    }
#ifdef SCROLL_WITH_TRACKBALL
    if (_timerAutoScroll) {
        _workLoop->removeEventSource(_timerAutoScroll);
        _timerAutoScroll->release();
        _timerAutoScroll = nullptr;
    }
#endif
    // *&*&*&Va_20200914
    if (_timerDebounce) {
        _workLoop->removeEventSource(_timerDebounce);
        _timerDebounce->release();
        _timerDebounce = nullptr;
    }
    // *&*&*&V1_20210119
    if (_timerTiltButton) {
        _workLoop->removeEventSource(_timerTiltButton);
        _timerTiltButton->release();
        _timerTiltButton = nullptr;
    }

    if (_workLoop) {
        _workLoop->release();
        _workLoop = nullptr;
    }
    IOLockUnlock(_deviceLock);
    IOLockFree(_deviceLock); _deviceLock = nullptr;

    _pointer->detach(this);
    _pointer->release();
    _pointer = nullptr;

    _keyboard->detach(this);
    _keyboard->release();
    _keyboard = nullptr;

    super::stop(provider);

    TBLogDebug("[TBW] TrackballWorks2Service::stop - exit\n");
}

IOReturn TrackballWorks2Service::sGetDeviceInfo(
    OSObject* target,
    void* reference,
    IOExternalMethodArguments* arguments)
{
    return reinterpret_cast<TrackballWorks2Service*>(target)->getDeviceInfo(
        reinterpret_cast<TBWDEVICEINFO*>(arguments->structureOutput)
    );
}

static uint32_t getNumericProperty(IOService* provider, const char* property, uint32_t def)
{
    TBLogDebug("[TBW] TrackballWorks2Service - getNumericProperty\n");
    uint32_t ret = def;
    OSObject* obj = provider->copyProperty(property,
                             gIOServicePlane,
                             kIORegistryIterateRecursively|kIORegistryIterateParents);
    OSNumber* osNumber = OSDynamicCast(OSNumber, obj);
    if (osNumber)
    {
        ret = osNumber->unsigned32BitValue();
    }
    OSSafeReleaseNULL(obj);
    return ret;
}

static void getStringProperty(IOService* provider, const char* property, char* out, size_t outLen, const char* def)
{
    TBLogDebug("[TBW] TrackballWorks2Service - getStringProperty\n");
    memset(out, 0, outLen*sizeof(char));
    if (def) {
        ::strncpy(out, def, min(strlen(def), outLen-1));
    }

    OSObject* obj = provider->copyProperty(property,
                             gIOServicePlane,
                             kIORegistryIterateRecursively|kIORegistryIterateParents);
    OSString* osString = OSDynamicCast(OSString, obj);
    if (osString)
    {
        const char* src = osString->getCStringNoCopy();
        if (src) {
            strncpy(out, src, min(strlen(src), outLen-1));
        }
    }
    OSSafeReleaseNULL(obj);
}

IOReturn TrackballWorks2Service::getDeviceInfo(TBWDEVICEINFO* pDevInfo)
{
    LOG_FUNCNAME();
    TBLogDebug("[TBW] TrackballWorks2Service - getDeviceInfo\n");
    pDevInfo->usProductId = getNumericProperty(_driver, "ProductID", 0);
    pDevInfo->usVersion = getNumericProperty(_driver, "VersionNumber", 0);
    getStringProperty(
        _driver,
        "Product",
        pDevInfo->szName,
        sizeof(pDevInfo->szName)/sizeof(pDevInfo->szName[0]),
        nullptr);
    return kIOReturnSuccess;
}

IOReturn TrackballWorks2Service::sRegisterHelper(
    OSObject* target,
    void* reference,
    IOExternalMethodArguments* arguments)
{
    LOG_FUNCNAME();
    TBLogDebug("[TBW] TrackballWorks2Service - sRegisterHelper\n");
    return reinterpret_cast<TrackballWorks2Service*>(target)->registerHelper(
        reinterpret_cast<TrackballWorks2UserClient*>(reference), arguments->scalarInput[0]
    );
}

IOReturn TrackballWorks2Service::registerHelper(TrackballWorks2UserClient* userClient, uint64_t id)
{
    LOG_FUNCNAME();
    TBLogDebug("[TBW] TrackballWorks2Service - registerHelper\n");
    if (_userClient != nullptr) {
        TBLogError("Attempt to register helper when one is already registered\n");
        return kIOReturnBusy;
    }

    registerHelperUserClient(userClient);
    return kIOReturnSuccess;
}

IOReturn TrackballWorks2Service::sDeregisterHelper(
    OSObject* target,
    void* reference,
    IOExternalMethodArguments* arguments)
{
    LOG_FUNCNAME();
    TBLogDebug("[TBW] TrackballWorks2Service - sDeregisterHelper\n");
    return reinterpret_cast<TrackballWorks2Service*>(target)->deregisterHelper(
        reinterpret_cast<TrackballWorks2UserClient*>(reference)
    );
}

IOReturn TrackballWorks2Service::deregisterHelper(TrackballWorks2UserClient* userClient)
{
    LOG_FUNCNAME();
    TBLogDebug("[TBW] TrackballWorks2Service - deregisterHelper\n");
    if (_userClient != userClient) {
        TBLogError("[TBW] Helper deregister request from invalid process\n");
        return kIOReturnInvalid;
    }

    clearHelperUserClient();

    return kIOReturnSuccess;
}

void TrackballWorks2Service::sTimerFn(OSObject *owner, IOTimerEventSource *sender)
{
    TrackballWorks2Service* driver = OSDynamicCast(TrackballWorks2Service, owner);
    TBLogDebug("TrackballWorks2Service - TimerFn - tid: %llu\n", thread_tid(current_thread()));
    if (driver) {
        driver->timerFn(sender);
    }
}

IOReturn TrackballWorks2Service::sGetSlowPointer(OSObject* target,
                                              void* reference,
                                              IOExternalMethodArguments* arguments)
{
    LOG_FUNCNAME();
    TBLogDebug("[TBW] TrackballWorks2Service::sGetSlowPointer");
    auto me = (TrackballWorks2Service*)target;
    me->getSlowPointer((TBWSLOWPOINTERPARAMS*)arguments->structureOutput);
    return kIOReturnSuccess;
}

IOReturn TrackballWorks2Service::sSetSlowPointer(OSObject* target,
                                              void* reference,
                                              IOExternalMethodArguments* arguments)
{
    LOG_FUNCNAME();
    TBLogDebug("[TBW] TrackballWorks2Service::sSetSlowPointer");
    auto me = (TrackballWorks2Service*)target;
    me->setSlowPointer((const TBWSLOWPOINTERPARAMS*)arguments->structureInput);
    return kIOReturnSuccess;
}

IOReturn TrackballWorks2Service::sSetTBWSINGLEAXISMOVEMENTPARMS(OSObject* target,
                                                     void* reference,
                                                     IOExternalMethodArguments* arguments)
{
    LOG_FUNCNAME();
    TBLogDebug("[TBW] TrackballWorks2Service::sSetTBWSINGLEAXISMOVEMENTPARMS");
    auto me = (TrackballWorks2Service*)target;
    me->setTBWSINGLEAXISMOVEMENTPARMS((const TBWSINGLEAXISMOVEMENTPARMS*)arguments->structureInput);
    return kIOReturnSuccess;
}

IOReturn TrackballWorks2Service::sGetTBWSINGLEAXISMOVEMENTPARMS(OSObject* target,
                                                     void* reference,
                                                     IOExternalMethodArguments* arguments)
{
    LOG_FUNCNAME();
    TBLogDebug("[TBW] TrackballWorks2Service::sGetTBWSINGLEAXISMOVEMENTPARMS");
    auto me = (TrackballWorks2Service*)target;
    me->getTBWSINGLEAXISMOVEMENTPARMS((TBWSINGLEAXISMOVEMENTPARMS*)arguments->structureOutput);
    return kIOReturnSuccess;
}

IOReturn TrackballWorks2Service::sSetScrollWheelParams(OSObject* target,
                                void* reference,
                                IOExternalMethodArguments* arguments)
{
    return ((TrackballWorks2Service*)target)->setScrollWheelParams(
         (const PTBWSCROLLWHEELPARAMS)arguments->structureInput);
}


#define SANITIZE_WHEEL_ARG(args)    if (args->wheel != WHEEL_VERT && args->wheel != WHEEL_HORZ) { \
                                        TBLogDebug("\t[TBW] Invalid scroll wheel argument: %d\n", args->wheel); \
                                        return kIOReturnBadArgument; \
                                    }

IOReturn TrackballWorks2Service::setScrollWheelParams(const PTBWSCROLLWHEELPARAMS pParams)
{
    LOG_FUNCNAME();
    SANITIZE_WHEEL_ARG(pParams)

    double speed = pParams->speed;
    TBLogDebug("\t[TBW] TrackballWorks2Service - setScrollWheelParams - speed: %d.%d, invert: %llu, inertial: %llu\n",
        (int)speed,
        (int)((speed*100)-((int)speed*100)),
        pParams->invert,
        pParams->inertial);

    TBSetScrollParams(
        &_tbwConfig,
        pParams->wheel,
        pParams->speed,
        pParams->invert ? TRUE : FALSE,
        pParams->inertial ? TRUE : FALSE);

    return kIOReturnSuccess;
}

IOReturn TrackballWorks2Service::sGetScrollWheelParams(OSObject* target,
                                void* reference,
                                IOExternalMethodArguments* arguments)
{
    return ((TrackballWorks2Service*)target)->getScrollWheelParams(
         (PTBWSCROLLWHEELPARAMS)arguments->structureOutput);
}


IOReturn TrackballWorks2Service::getScrollWheelParams(PTBWSCROLLWHEELPARAMS pParams)
{
    LOG_FUNCNAME();

    SANITIZE_WHEEL_ARG(pParams)
    TBLogDebug("[TBW] TrackballWorks2Service::getScrollWheelParams");
    double speed = 0;
    BOOLEAN bInertial = FALSE;
    BOOLEAN bInvert = FALSE;
    TBGetScrollParams(&_tbwConfig, pParams->wheel, &speed, &bInvert, &bInertial);

    pParams->speed = speed;
    pParams->invert = bInvert ? 1 : 0;
    pParams->inertial = bInertial ? 1 : 0;

    TBLogDebug("\t[TBW] wheel: %d, speed: %F, invert: %llu, inertial: %llu\n",
        pParams->wheel,
        pParams->speed,
        pParams->invert,
        pParams->inertial
        );

    return kIOReturnSuccess;
}

IOReturn TrackballWorks2Service::sGetNextEventAsync(
    OSObject *target,
    void *reference,
    IOExternalMethodArguments *arguments)
{
    TrackballWorks2Service* service = reinterpret_cast<TrackballWorks2Service*>(target);
    return service->getNextEventAsync(arguments);
}

IOReturn TrackballWorks2Service::sSetButtonsConfig(
    OSObject *target,
    void *reference,
    IOExternalMethodArguments *arguments)
{
    LOG_FUNCNAME();
    TBLogDebug("[TBW] TrackballWorks2Service::sSetButtonsConfig");
    TrackballWorks2Service* service = reinterpret_cast<TrackballWorks2Service*>(target);
    return service->setButtonsConfig(reinterpret_cast<const TBWBUTTONSCONFIG*>(arguments->structureInput));
}

IOReturn TrackballWorks2Service::sEmulateDeviceAction(
    OSObject* target,
    void* reference,
    IOExternalMethodArguments* arguments)
{
    TBLogDebug("[TBW] TrackballWorks2Service::sEmulateDeviceAction");
    TrackballWorks2Service* service = reinterpret_cast<TrackballWorks2Service*>(target);
    return service->emulateDeviceAction(
        reinterpret_cast<const TBWEMULATEDEVICEACTION*>(arguments->structureInput)
    );
}

IOReturn TrackballWorks2Service::emulateDeviceAction(const TBWEMULATEDEVICEACTION* pAction)
{
    LOG_FUNCNAME();
    IOLockLock(_deviceLock);
    TBLogDebug("[TBW] TrackballWorks2Service::emulateDeviceAction");
    IOReturn ir = kIOReturnInvalid;

    if (pAction->uAction == DA_BUTTONCLICK)
    {
        if (pAction->u.ButtonClick.mask != 0)
        {
            TBLogDebug("\t[TBW] TrackballWorks2Service::emulateDeviceAction - emulating button click, mask: 0x%04x\n", pAction->u.ButtonClick.mask);
            // button down
            TBProcessButtonEvent(&_tbwConfig, pAction->u.ButtonClick.mask);
            // button up
            TBProcessButtonEvent(&_tbwConfig, 0);

            ir = kIOReturnSuccess;
        }
        else
        {
            TBLogError("\t[TBW] TrackballWorks2Service::emulateDeviceAction - bad argument - button mask == 0\n");
            ir = kIOReturnBadArgument;
        }

    }
    IOLockUnlock(_deviceLock);

    return ir;
}

IOReturn TrackballWorks2Service::sGetPointerParams(
    OSObject* target,
    void* reference,
    IOExternalMethodArguments* arguments)
{
    LOG_FUNCNAME();
    return reinterpret_cast<TrackballWorks2Service*>(target)->getPointerParams(
        reinterpret_cast<TBWPOINTERPARAMS*>(arguments->structureOutput));
}

IOReturn TrackballWorks2Service::getPointerParams(TBWPOINTERPARAMS* params)
{
    TBLogDebug("[TBW] TrackballWorks2Service::getPointerParams");
    params->flags = POINTER_PARAM_SPEED|POINTER_PARAM_ACCELERATION;
    params->speed = TBGetPointerSpeed(&_tbwConfig);
    params->acceleration = 0;

    return kIOReturnSuccess;
}

// kKWDriverSetPointerParams handler
IOReturn TrackballWorks2Service::sSetPointerParams(
    OSObject* target,
    void* reference,
    IOExternalMethodArguments* arguments)
{
    LOG_FUNCNAME();
    TBLogDebug("[TBW] TrackballWorks2Service::sSetPointerParams");
    return reinterpret_cast<TrackballWorks2Service*>(target)->setPointerParams(
        reinterpret_cast<const TBWPOINTERPARAMS*>(arguments->structureInput));
}

IOReturn TrackballWorks2Service::setPointerParams(const TBWPOINTERPARAMS* p)
{
    TBLogDebug("[TBW] TrackballWorks2Service::setPointerParams - flags: 0x%04x, speed: %d.%d, accel: %llu\n",
        (uint32_t)p->flags,
        (int)p->speed,
        (int)((p->speed*100)-((int)p->speed*100)),
        p->acceleration);

    if (p->flags & POINTER_PARAM_SPEED)
        TBSetPointerSpeed(&_tbwConfig, p->speed);
    if (p->flags & POINTER_PARAM_ACCELERATION)
        setDevicePointerAccelerationRate(p->acceleration);

    return kIOReturnSuccess;
}


IOReturn TrackballWorks2Service::sPostKeystroke(OSObject* target,
                                  void* reference,
                                  IOExternalMethodArguments* arguments)
{
    LOG_FUNCNAME();
    TBLogDebug("[TBW] TrackballWorks2Service::sPostKeystroke");
    return reinterpret_cast<TrackballWorks2Service*>(target)->postKeystroke(
        reinterpret_cast<const TBWPOSTKEYSTROKE*>(arguments->structureInput));
}

IOReturn TrackballWorks2Service::postKeystroke(const TBWPOSTKEYSTROKE* p)
{
    /*
    const unsigned VK_COMMAND = 0x37;
    const unsigned VK_FN = 0x3F;
    const unsigned int VK_W = 0x0D;
    const unsigned int VK_F11 = 0x67;
    _keyboard->postEvent(VK_W, true);
    _keyboard->postEvent(VK_W, false);
    _keyboard->postEvent(VK_COMMAND, false);
    */

    LOG_FUNCNAME();
    TBLogDebug("\t[TBW] TrackballWorks2Service::postKeystroke - Total # of keystrokes - %llu\n", p->count);

    size_t numKeys = min((size_t)p->count, sizeof(p->keystrokes)/sizeof(p->keystrokes[0]));
    for (size_t i=0; i<numKeys; i++) {
        auto& ks = p->keystrokes[i];
        TBLogDebug("\t[TBW] #: %lu keyCode: 0x%04x, dir: %s\n",
            i,
            (unsigned)ks.keyCode,
            (ks.down ? "DOWN" : "UP"));

        _keyboard->postEvent((unsigned)ks.keyCode, ks.down ? true : false);
    }

    return kIOReturnSuccess;
}

IOReturn TrackballWorks2Service::sPostPointerClick(OSObject* target,
                                  void* reference,
                                  IOExternalMethodArguments* arguments)
{
    LOG_FUNCNAME();
    return reinterpret_cast<TrackballWorks2Service*>(target)->postPointerClick(
        reinterpret_cast<const TBWPOSTPOINTERCLICK*>(arguments->structureInput));
}

IOReturn TrackballWorks2Service::postPointerClick(const TBWPOSTPOINTERCLICK* p)
{
    TBLogDebug("[TBW] TrackballWorks2Service::postPointerClick - buttonState: 0x%x",
               p->button);
    _pointer->dispatchButtonEvent(p->button);

    return kIOReturnSuccess;
}

IOReturn TrackballWorks2Service::setSlowPointer(const TBWSLOWPOINTERPARAMS* args)
{
    TBLogDebug("[TBW] TrackballWorks2Service::setSlowPointer - status: %s, modifier: %llu",
               (args->enable ? "ENABLE": "DISABLE"), args->modifierKey);

    TBSetSlowPointer(&_tbwConfig, args->enable ? TRUE : FALSE);
    return kIOReturnSuccess;
}

IOReturn TrackballWorks2Service::getSlowPointer(TBWSLOWPOINTERPARAMS* args)
{
    args->enable = TBGetSlowPointer(&_tbwConfig);
    TBLogDebug("[TBW] TrackballWorks2Service::getSlowPointer - status: %s",
               (args->enable ? "ENABLE": "DISABLE"));
    return kIOReturnSuccess;
}

IOReturn TrackballWorks2Service::setTBWSINGLEAXISMOVEMENTPARMS(const TBWSINGLEAXISMOVEMENTPARMS* args)
{
    TBLogDebug("[TBW] TrackballWorks2Service::setTBWSINGLEAXISMOVEMENTPARMS - enable: %llu, modifier: %llu",
               args->enable, args->modifierKey);

    TBSetLockedPointerAxis(&_tbwConfig, args->enable ? TRUE : FALSE);
    return kIOReturnSuccess;
}

IOReturn TrackballWorks2Service::getTBWSINGLEAXISMOVEMENTPARMS(TBWSINGLEAXISMOVEMENTPARMS* args)
{
    args->enable = TBGetLockedPointerAxis(&_tbwConfig);
    TBLogDebug("[TBW] TrackballWorks2Service::getTBWSINGLEAXISMOVEMENTPARMS - status: %s",
               (args->enable ? "ENABLED" : "DISABLED"));
    return kIOReturnSuccess;
}

IOReturn TrackballWorks2Service::getNextEventAsync(IOExternalMethodArguments *arguments)
{
    TBLogDebug("[TBW] TrackballWorks2Service::getNextEventAsync - buf: %p, size: %d\n",
        (void*)arguments->scalarInput[1],
        (uint32_t)arguments->scalarInput[0]);

    if (_notifyEventDescriptor) {
        TBLogError("[TBW] getNextEventAsync - notifyEventDescriptor != NULL\n");
        return kIOReturnBusy;
    }

    IOLockLock(_deviceLock);

    // Take a copy of the asyncReference buffer
    bcopy(arguments->asyncReference, _notifyEventAsyncRef, sizeof(OSAsyncReference64));

    if (_userClient) {
        _notifyEventDescriptor = IOMemoryDescriptor::withAddressRange(
            (mach_vm_address_t) arguments->scalarInput[1],
            arguments->scalarInput[0],
            kIODirectionInOut,
            _userClient->task());

        if (!_notifyEventDescriptor) {
            TBLogError("[TBW] TrackballWorks2Service::getNextEventAsync - error creating memory descriptor for user buffer\n");
            IOLockUnlock(_deviceLock);
            return kIOReturnBadArgument;
        }

        _notifyEventDescriptor->map();
    }

    // dequeue next event, if one is present send it to user mode program
    dequeueNextTbwDriverEvent();

    IOLockUnlock(_deviceLock);

    TBLogDebug("[TBW] TrackballWorks2Service::getNextEventAsync - done\n");
    return kIOReturnSuccess;
}

IOReturn TrackballWorks2Service::setButtonsConfig(const TBWBUTTONSCONFIG *pButtonsConfig)
{
    LOG_FUNCNAME();
    TBLogDebug("[TBW] TrackballWorks2Service::setButtonsConfig - start\n");
    IOLockLock(_deviceLock);
    _buttonsConfig = *pButtonsConfig;
    TBLIBBUTTONSCONFIG2 newConfig = {0};
    newConfig.cbSize = sizeof(TBLIBBUTTONSCONFIG2);
    newConfig.chordMask = pButtonsConfig->chordMask;
    newConfig.nButtons = pButtonsConfig->nButtons;

    memcpy(newConfig.chordMsks, pButtonsConfig->chordMsks, MAX_CHORD_BUTTONS_NO * sizeof(uint32_t)); //vv0427

    for (size_t i=0; i<pButtonsConfig->nButtons; i++) {
        newConfig.buttons[i].buttonMask = pButtonsConfig->buttons[i].buttonMask;
        if (BUTTON_ACTION_CLICK == pButtonsConfig->buttons[i].action) {
            newConfig.buttons[i].action = TBLIB_BUTTON_ACTION_CLICK;
            newConfig.buttons[i].u.ClickActionParams.buttonMask = pButtonsConfig->buttons[i].u.ClickActionParams.buttonMask;
            newConfig.buttons[i].u.ClickActionParams._modifiers = pButtonsConfig->buttons[i].u.ClickActionParams._modifiers;
        } else if (BUTTON_ACTION_SCROLL == pButtonsConfig->buttons[i].action) {
            newConfig.buttons[i].action = TBLIB_BUTTON_ACTION_SCROLL;
            newConfig.buttons[i].u.ScrollActionParams.direction =
                pButtonsConfig->buttons[i].u.ScrollActionParams.direction;
            newConfig.buttons[i].u.ScrollActionParams.lines =
                pButtonsConfig->buttons[i].u.ScrollActionParams.lines;
        } else if (BUTTON_ACTION_DRAG == pButtonsConfig->buttons[i].action) {
            newConfig.buttons[i].action = TBLIB_BUTTON_ACTION_DRAG;
            newConfig.buttons[i].u.ClickActionParams.buttonMask = pButtonsConfig->buttons[i].u.ClickActionParams.buttonMask;
            newConfig.buttons[i].u.ClickActionParams._modifiers = pButtonsConfig->buttons[i].u.ClickActionParams._modifiers;
        }
    }
    TBSetButtonsConfig(&_tbwConfig, &newConfig);

    dumpButtonsConfig(pButtonsConfig);

    IOLockUnlock(_deviceLock);

    return kIOReturnSuccess;
}

bool TrackballWorks2Service::setDevicePointerAccelerationRate(uint64_t rate)
{
    bool ret = false;
    OSDictionary* dict = OSDictionary::withCapacity(1);
    OSNumber* value = OSNumber::withNumber(rate, 64);
    TBLogDebug("[TBW] TrackballWorks2Service::setDevicePointerAccelerationRate - start\n");
    if (dict && value)
    {
        TBLogDebug("[TBW] TrackballWorks2Service - setting device accel rate to: %llu\n", rate);
        dict->setObject("HIDMouseAcceleration", value);
        _pointer->setParamProperties(dict);
        ret = true;
    }
    OSSafeReleaseNULL(value);
    OSSafeReleaseNULL(dict);

    return ret;
}

bool TrackballWorks2Service::parseHIDCapabilities()
{
    OSStatus status = kIOReturnError;
    TBLogDebug("[TBW] TrackballWorks2Service::parseHIDCapabilities - start\n");
    OSObject* obj = copyProperty("ReportDescriptor",
                                 gIOServicePlane,
                                 kIORegistryIterateRecursively|kIORegistryIterateParents);
    OSData* descriptor = OSDynamicCast(OSData, obj);
    if (!descriptor) {
        TBLogDebug("[TBW] TrackballWorks2Service - could not retrieve report descriptor\n");
        OSSafeReleaseNULL(obj);
        return false;
    }

    HIDPreparsedDataRef  parseData;
    status = HIDOpenReportDescriptor((void*)descriptor->getBytesNoCopy(),
                                     descriptor->getLength(),
                                     &parseData,      /* pre-parse data */
                                     0);             /* flags */

    if (status != kHIDSuccess)
    {
        TBLogError("[TBW] TrackballWorks2Service - error opening the report descriptor\n");
        OSSafeReleaseNULL(obj);
        return false;
    }

    HIDCapabilities	caps;
    status = HIDGetCapabilities( parseData, &caps );

    HIDCloseReportDescriptor(parseData);
    OSSafeReleaseNULL(obj);

    if (status != kHIDSuccess)
    {
        TBLogError("[TBW] TrackballWorks2Service - error getting caps from report descriptor\n");
        return false;
    }

    TBLogDebug("[TBW] TrackballWorks2Service - Report bytes: input:%ld output:%ld feature:%ld",
                (long)caps.inputReportByteLength,
                (long)caps.outputReportByteLength,
                (long)caps.featureReportByteLength);
    TBLogDebug("[TBW] TrackballWorks2Service - Collections : %ld", (long)caps.numberCollectionNodes);
    TBLogDebug("[TBW] TrackballWorks2Service - Buttons     : input:%ld output:%ld feature:%ld",
                (long)caps.numberInputButtonCaps,
                (long)caps.numberOutputButtonCaps,
                (long)caps.numberFeatureButtonCaps);
    TBLogDebug("[TBW] TrackballWorks2Service - Values      : input:%ld output:%ld feature:%ld",
                (long)caps.numberInputValueCaps,
                (long)caps.numberOutputValueCaps,
                (long)caps.numberFeatureValueCaps);

    return true;
}

// internal method -- mutex is expected to have been acquired!
void TrackballWorks2Service::enqueueTbwDriverEvent(TBWDRIVEREVENT* pEvent)
{
    TBLogDebug("[TBW] TrackballWorks2Service::enqueueTbwDriverEvent - start\n");
    if (_userClient == nullptr) {
        TBLogError("[TBW] TrackballWorks2Service::enqueueTbwDriverEvent - userClient == NULL\n");
        return;
    }

    _notifyEventQueue->enqueue((void*)pEvent);

    // try and dequeue next event from the top of the queue
    dequeueNextTbwDriverEvent();
}

// internal method -- mutex is expected to have been acquired!
void TrackballWorks2Service::dequeueNextTbwDriverEvent()
{
    TBLogDebug("[TBW] TrackballWorks2Service::dequeueNextTbwDriverEvent - start\n");
    if (_userClient == nullptr
        || _notifyEventDescriptor == nullptr) {
        TBLogError("[TBW] dequeueNextTbwDriverEvent - userClient == NULL || _notifyEventDescriptor == NULL\n");
        return;
    }

    TBWDRIVEREVENT ev = {0};
    if (_notifyEventQueue->dequeue((void*)&ev)) {
        // event was unqueued, post it to the userClient buffer
        if (_notifyEventDescriptor->prepare() != kIOReturnSuccess) {
            TBLogError("[TBW] dequeueNextTbwDriverEvent - failed to prepare memory descriptor\n");
            // Since prepare() failed, we need to release explicitly!
            _notifyEventDescriptor->release();
            _notifyEventDescriptor = nullptr;
        } else {
            // copy event into user mode buffer
            IOByteCount cb = _notifyEventDescriptor->writeBytes(0, &ev, sizeof(TBWDRIVEREVENT));

            // complete() does an implicit Object::release()!
            _notifyEventDescriptor->complete();
            _notifyEventDescriptor = nullptr;

            TBLogDebug("[TBW] dequeueNextTbwDriverEvent - copied TBWDRIVEREVENT to user buffer, cb: %llu\n", cb);
        }

        TBLogDebug("[TBW] dequeueNextTbwDriverEvent - notifying async result\n");
        _userClient->notifyAsyncResult(_notifyEventAsyncRef, kIOReturnSuccess);
    } else {
        TBLogDebug("[TBW] dequeueNextTbwDriverEvent - failed dequeue event from circular queue\n");
    }
}

void TrackballWorks2Service::postPointerEvent(int dx, int dy, UInt32 buttonState, bool btnEvent, bool ptrEvent)
{
    TBLogDebug("[TBW] TrackballWorks2Service::postPointerEvent\n");
    if (!_pointer)
    {
        TBLogDebug("[TBW] TrackballWorks2Service::postPointerEvent - !_pointer\n");
        return;
    }

    IOLockLock(_deviceLock);
    if(ptrEvent) // prevent not-necessary events
        TBProcessPointerEvent(&_tbwConfig, dx, dy);
    if(btnEvent) // prevent not-necessary events
        TBProcessButtonEvent(&_tbwConfig, buttonState);
    IOLockUnlock(_deviceLock);
}

/**
 * wheel == 1 => vertical scroll
 * wheel == 2 => horizontal scroll
 */
void TrackballWorks2Service::postWheelEvent(int wheel, int units)
{
    TBLogDebug("[TBW] TrackballWorks2Service::postWheelEvent\n");
    if (!_pointer)
        return;

    // validate wheel argument
    if (wheel != TBLIB_SCROLL_WHEEL_VERT && wheel != TBLIB_SCROLL_WHEEL_HORZ)
        return;

    IOLockLock(_deviceLock);
    if (wheel == TBLIB_SCROLL_WHEEL_HORZ) {
        TBProcessACPanEvent(&_tbwConfig, units);
    } else {
        TBProcessWheelEvent(
            &_tbwConfig,
            TBLIB_SCROLL_WHEEL_VERT,
            units);
    }
    IOLockUnlock(_deviceLock);
}

void TrackballWorks2Service::setDefaultButtonsConfig()
{
    TBLogDebug("[TBW] TrackballWorks2Service::setDefaultButtonsConfig\n");
    memset(&_buttonsConfig, 0, sizeof(TBWBUTTONSCONFIG));
    _buttonsConfig.cbSize = sizeof(TBWBUTTONSCONFIG);
    _buttonsConfig.chordMask = 0x0;
    const size_t MAX_BUTTONS = 5;
    for (size_t i=0; i<MAX_BUTTONS; i++) {
        TBWBUTTONACTION& btnAction = _buttonsConfig.buttons[i];
        btnAction.action = BUTTON_ACTION_CLICK;
        btnAction.buttonMask =
        btnAction.u.ClickActionParams.buttonMask = (uint32_t)0x1 << i;
    }
    // ACPan Left
    {
        TBWBUTTONACTION& btnAction = _buttonsConfig.buttons[MAX_BUTTONS];
        btnAction.action = BUTTON_ACTION_SCROLL;
        btnAction.u.ScrollActionParams.direction = SCROLL_DIRECTION_LEFT;
        btnAction.u.ScrollActionParams.lines = 1;
    }

    // ACPan Right
    {
        TBWBUTTONACTION& btnAction = _buttonsConfig.buttons[MAX_BUTTONS+1];
        btnAction.action = BUTTON_ACTION_SCROLL;
        btnAction.u.ScrollActionParams.direction = SCROLL_DIRECTION_RIGHT;
        btnAction.u.ScrollActionParams.lines = 1;
    }
    _buttonsConfig.nButtons = MAX_BUTTONS+1;
}

void TrackballWorks2Service::saveCurButtonsConfig()
{
    LOG_FUNCNAME();
    TBLogDebug("[TBW] TrackballWorks2Service::saveCurButtonsConfig\n");
    setProperty("ButtonMasksCount", _buttonsConfig.nButtons, 32);
    setProperty("ButtonMasks", _buttonsConfig.buttons, sizeof(TBWBUTTONACTION)*_buttonsConfig.nButtons);
}

void TrackballWorks2Service::restoreLastButtonsConfig()
{
    LOG_FUNCNAME();
    TBLogDebug("[TBW] TrackballWorks2Service::restoreLastButtonsConfig\n");
    OSNumber* number = OSDynamicCast(OSNumber, getProperty("ButtonMasksCount"));
    if (number) {
        uint32_t nButtons = number->unsigned32BitValue();
        TBLogDebug("\t# of masks: %d\n", nButtons);
        if (nButtons > 0) {
            OSData* masksData = OSDynamicCast(OSData, getProperty("ButtonMasks"));
            if (masksData) {
                TBLogDebug("\tmasks, length: %d bytes\n", masksData->getLength());
                ::memcpy(_buttonsConfig.buttons, masksData->getBytesNoCopy(), masksData->getLength());
                _buttonsConfig.nButtons = nButtons;
            }
        }
    }
}

// callbacks for TB library
void TrackballWorks2Service::tbPostButtonEvent(uint32_t buttonState)
{
    LOG_FUNCNAME();
    TBLogDebug("[TBW] TrackballWorks2Service::tbPostButtonEvent\n");
    
    _pointer->dispatchButtonEvent(buttonState);
    _tbwConfig.curButtonState = buttonState;
}

void TrackballWorks2Service::tbPostCusrorEvent(int dx, int dy)
{
    LOG_FUNCNAME();
    TBLogDebug("[TBW] TrackballWorks2Service::tbPostCusrorEvent\n");
    _pointer->dispatchCursorEvent(dx, dy);
}

void TrackballWorks2Service::tbPostWheelEvent(int wheel, int offset)
{
    LOG_FUNCNAME();
    TBLogDebug("[TBW] TrackballWorks2Service::tbPostWheelEvent\n");
    if (wheel == TBLIB_SCROLL_WHEEL_VERT)
        _pointer->dispatchScrollEvent(offset, 0, 0);
    else if (wheel == TBLIB_SCROLL_WHEEL_HORZ)
        _pointer->dispatchScrollEvent(0, offset, 0);
}

void TrackballWorks2Service::tbEnqueueButtonEvent(uint32_t uDownState, uint32_t uUpState)
{
    LOG_FUNCNAME();
    TBLogDebug("[TBW] TrackballWorks2Service::tbEnqueueButtonEvent\n");
    TBWDRIVEREVENT de = {0};
    de.cb = sizeof(de);
    de.flags = TBW_BUTTON_EVENT;
    de.buttonDownMask = uDownState;
    de.buttonUpMask = uUpState;
    enqueueTbwDriverEvent(&de);
}

uint32_t TrackballWorks2Service::tbStartTimer(uint32_t timerId, uint32_t timeOutMS)
{
    LOG_FUNCNAME();
    TBLogDebug("[TBW] TrackballWorks2Service::tbStartTimer\n");
    switch (timerId) {
        case TIMER_ID_TILT_BUTTON: // *&*&*&V1_20210119 for Wired Vertical Trackball
            _tbwConfig.bTiltButtonTimerStarted = TRUE;
            _timerTiltButton->setTimeoutMS(timeOutMS);
            break;
        case TIMER_ID_DEBOUNCE: // *&*&*&V1_20200914
            _tbwConfig.bChordDebounceTimerStarted = TRUE;
            _timerDebounce->setTimeoutMS(timeOutMS);
            break;

        //case TIMER_ID_CHORD:
        //	_timerDetectChords->setTimeoutMS(timeOutMS);
        //    break;

        case TIMER_ID_INERTIAL_SCROLL:
            _timerInertialScroll->setTimeoutMS(timeOutMS);
            break;
#ifdef SCROLL_WITH_TRACKBALL
        case TIMER_ID_AUTO_SCROLL:
            _tbwConfig.bAutoScrollTimerStarted = TRUE;
            _timerAutoScroll->setTimeoutMS(timeOutMS);
        break;
#endif
    }
    return timerId;
}

void TrackballWorks2Service::tbStopTimer(uint32_t timerId)
{
    LOG_FUNCNAME();
    TBLogDebug("[TBW] TrackballWorks2Service::tbStopTimer. timerId=%d\n", timerId);
    switch (timerId) {
        case TIMER_ID_TILT_BUTTON:
            _tbwConfig.bTiltButtonTimerStarted = FALSE;
            _timerTiltButton->cancelTimeout();
        break;

        case TIMER_ID_INERTIAL_SCROLL:
        break;
#ifdef SCROLL_WITH_TRACKBALL
        case TIMER_ID_AUTO_SCROLL: // vannes20200519
            _tbwConfig.bAutoScrollTimerStarted = FALSE;
            _timerAutoScroll->cancelTimeout();
        break;
#endif
        case TIMER_ID_DEBOUNCE: // *&*&*&Va_20200914
            _tbwConfig.bChordDebounceTimerStarted = FALSE;
            _timerDebounce->cancelTimeout();
        break;
    }
}
// end TB library callbacks

 // *&*&*&V1_20200918 return helper connected status...
uint32_t TrackballWorks2Service::getHelperConnectedStatus()
{
    //uint32_t    fHelperConnected;
    return _tbwConfig.fHelperConnected;
}

