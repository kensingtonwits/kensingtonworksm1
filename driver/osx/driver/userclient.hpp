//
//  ServiceUserClient.hpp
//  trackball2
//
//  Created by Hariharan Mahadevan on 2019/1/25.
//  Copyright © 2019 Kensington. All rights reserved.
//

#pragma once

#include <IOKit/IOLib.h>
#include <IOKit/IOUserClient.h>
#include <IOKit/hidsystem/IOHIDevice.h>

#include "KWDriverInterface.h"

class TrackballWorks2Service;

class TrackballWorks2UserClient : public IOUserClient {
    
    OSDeclareDefaultStructors(TrackballWorks2UserClient)
    
    typedef IOUserClient super;
    
    TrackballWorks2Service* _service;
    task_t _task;
    bool _taskIsAdmin;
    
    static const IOExternalMethodDispatch sMethods[kKWDriverSentinel];
    
public:
    static TrackballWorks2UserClient* withProvider(IOService* provider);

    virtual bool initWithTask(task_t owningTask,
                              void* securityToken,
                              UInt32 type,
                              OSDictionary* properties) override;
    virtual bool start(IOService* provider) override;
    virtual void stop(IOService* provider) override;
    virtual IOReturn clientClose() override;
    virtual IOReturn externalMethod(uint32_t selector,
                                    IOExternalMethodArguments* arguments,
                                    IOExternalMethodDispatch* dispatch = 0,
                                    OSObject* target = 0,
                                    void* reference = 0) override;

    // Returns the task id of the driver helper agent
    task_t task()
    { return _task; }

    static IOReturn sRegisterHelper(
        OSObject* target,
        void* reference,
        IOExternalMethodArguments* arguments);
    IOReturn registerHelper();
    
    static IOReturn sDeregisterHelper(
        OSObject* target,
        void* reference,
        IOExternalMethodArguments* arguments);
    IOReturn deregisterHelper();
    
    void notifyAsyncResult(OSAsyncReference64, IOReturn);
};
