//
//  circularqueue.h
//  driver
//
//  Created by Hariharan Mahadevan on 2019/5/9.
//

#ifndef circularqueue_h
#define circularqueue_h

#ifdef enqueue
#undef enqueue
#endif

#ifdef dequeue
#undef dequeue
#endif

#include <IOKit/IOLib.h>

typedef unsigned char byte;

class CircularQueue {


    byte* _buf;
    byte* _head;
    byte* _tail;
    size_t _cbElem;
    size_t _nElem;
    
    bool init(size_t cbElem, size_t nElem)
    {
        _buf = (byte*)IOMalloc(cbElem*nElem);
        if (_buf) {
            _head = _tail = _buf;
            _cbElem = cbElem;
            _nElem = nElem;
            return true;
        }
        return false;
    }

    void deinit()
    {
        if (_buf)
            IOFree(_buf, _cbElem*_nElem);
        _buf = nullptr;
    }
    
    byte* start()
    {
        return _buf;
    }

    // return the end of the buffer, that is the outer boundary of the last
    // element.
    byte* end()
    {
        return _buf + _cbElem*_nElem;
    }

public:
    static CircularQueue* withElementSize(size_t cbElem, size_t nElem)
    {
        CircularQueue* queue = new CircularQueue();
        if (queue) {
            if (queue->init(cbElem, nElem))
                return queue;
            delete queue;
        }
        return nullptr;
    }
    
    static void destroy(CircularQueue* queue)
    {
        queue->deinit();
        delete queue;
    }

    // # of elements the queue can hold
    size_t capacity()
    {
        return _nElem;
    }

    // number of elements in the queue
    size_t numElements()
    {
        if (_tail > _head) {
            return (_tail - _head) / _cbElem;
        } else if (_head > _tail) {
            return ((end()-_head)/_cbElem) + ((_tail-start())/_cbElem);
        }
        return 0;
    }

    // clear the queue
    void clear()
    {
        _head = _tail = _buf;
    }
    
    // enqueue an element with the circular queue
    void enqueue(void* pElem)
    {
        memcpy(_tail, (byte*)pElem, _cbElem);
        _tail += _cbElem;
        if (_tail >= end())
            _tail = start();
    }

    // dequeue an element from the queue
    bool dequeue(void* pElem)
    {
        if (_head == _tail)
            return false;
        
        ::memcpy((byte*)pElem, _head, _cbElem);
        _head += _cbElem;
        if (_head >= end())
            _head = start();
        
        return true;
    }
};

#endif /* circularqueue_h */
