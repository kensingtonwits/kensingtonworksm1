//
//  tblog.h
//  trackball2
//
//  Created by Hariharan Mahadevan on 1/19/19.
//  Copyright © 2019 Kensington. All rights reserved.
//

#pragma once

#include <os/log.h>

static inline os_log_t _TBLog(void)
{
    static os_log_t log = NULL;
    
    if (!log) {
        log = os_log_create("com.kensington.trackball2", "default");
    }
    
    return log;
}

#define TBLog(fmt, ...)        os_log(_TBLog(), fmt, ##__VA_ARGS__)
#define TBLogError(fmt, ...)   os_log_error(_TBLog(), fmt, ##__VA_ARGS__)

/* probably won't use these */
#define TBLogInfo(fmt, ...)    os_log_info(_TBLog(), fmt, ##__VA_ARGS__)
#define TBLogFault(fmt, ...)   os_log_fault(_TBLog(), fmt, ##__VA_ARGS__)

// Suppress Debug messages in release builds
#ifdef DEBUG
#define TBLogDebug(fmt, ...)   os_log_debug(_TBLog(), fmt, ##__VA_ARGS__)
#else
#define TBLogDebug(fmt, ...)   ((void)0)
#endif

#define LOG_FUNCNAME()          TBLogDebug("%s\n", __FUNCTION__)
