/*
 Kensington Trackball driver for USB devices.
 
 Copyright (c) 2019 Kensington Computer Products Group, a division of ACCO Brands
 */

#pragma once

#include <IOKit/usb/IOUSBHostHIDDevice.h>
#include <IOKit/hidevent/IOHIDEventDriver.h>
#include "service.hpp"

#include "util.h"
#include "KWDriverInterface.h"

class com_kensington_TrackballWorks2USB : public IOUSBHostHIDDevice
{
    OSDeclareDefaultStructors(com_kensington_TrackballWorks2USB)
    typedef IOUSBHostHIDDevice super;
    
    int _interfaceNumber;       // Interface number that this object is instantiated for
    int _numInputReportElements; // # of HID input report elements
    FirmwareType _ft;
    
    TrackballWorks2Service* _service;

private:
    bool handleLegacyFirmwareReport(UInt8* report, IOByteCount len);
    bool handleExpertMouseFirwareReport(UInt8* report, IOByteCount len);
    
public:
    virtual void detach(IOService* provider) override;
    
    virtual bool handleStart(IOService* provider) override;
    virtual void handleStop(IOService* provider) override;
    
    virtual IOReturn handleReport(IOMemoryDescriptor* report,
                                  IOHIDReportType reportType = kIOHIDReportTypeInput,
                                  IOOptionBits options = 0) override;
};
