sudo rm -R Build/Release/trackballworks2.kext
xcodebuild -sdk macosx10.12 clean build CODE_SIGN_IDENTITY="" CODE_SIGNING_REQUIRED=NO -alltargets -configuration Release build
sudo codesign -d -v -s "Developer ID Application: Kensington Computer Products Group (293UQF7R4S)" Build/Release/trackballworks2.kext
sudo kextunload -b com.kensington.trackballworks2
sudo rm -R /Library/Extensions/trackballworks2.kext
sudo cp -R Build/Release/trackballworks2.kext /Library/Extensions/
sudo kextload /Library/Extensions/trackballworks2.kext
sudo kextcache -i /
