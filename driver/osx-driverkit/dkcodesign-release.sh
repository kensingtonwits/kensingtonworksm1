#!/bin/bash

# Replace with your identity
readonly CODE_SIGN_IDENTITY=30BE8DC925C6C8744378B645995F308911C90E35

set -e # forbid command failure

#
# Sign Karabiner-DriverKit-VirtualHIDDevice.dext
#

# Embed provisioning profile
cp \
    tbw_DK_driver_profile-2.provisionprofile \
    build/Release/KensingtonWorks.app/Contents/Library/SystemExtensions/com.kensington.tbwDKDriver.dext/embedded.provisionprofile

# Sign
codesign \
    --sign $CODE_SIGN_IDENTITY \
    --entitlements tbwDKDriver/tbwDKDriver.entitlements \
    --options runtime \
    --verbose \
    --force \
    build/Release/KensingtonWorks.app/Contents/Library/SystemExtensions/com.kensington.tbwDKDriver.dext

#
# Sign Karabiner-DriverKit-ExtensionManager.app
#

# Embed provisioning profile
cp \
    tbw_DK_driver_manager.provisionprofile \
    build/Release/KensingtonWorks.app/Contents/embedded.provisionprofile

# Sign
codesign \
    --sign $CODE_SIGN_IDENTITY \
    --entitlements tbwDKManager/tbwDKManager.entitlements \
    --options runtime \
    --verbose \
    --force \
    build/Release/KensingtonWorks.app
