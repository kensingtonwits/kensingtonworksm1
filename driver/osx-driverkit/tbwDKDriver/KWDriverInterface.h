//
//  KWDriverUserClient.h
//  kwdriver
//
//  Created by Hariharan Mahadevan on 2019/1/31.
//  Copyright © 2019 Kensington. All rights reserved.
//

#pragma once

// *&*&*&_V1_ADD_20200306
#define SCROLL_WITH_TRACKBALL
// *&*&*&_V2_ADD_20200306

typedef struct _TBWDEVICEINFO {
    uint32_t    cbSize;
    uint16_t    usVenderId;            // USB vender ID
    uint16_t    usProductId;            // USB product ID
    uint16_t    usVersion;              // Device version
    char        szName[127];            // null terminated string
    char        szSerialNumber[127];    // null terminated string
} TBWDEVICEINFO;

typedef struct _TBWSLOWPOINTERPARAMS {
    uint64_t enable;
    uint64_t modifierKey;
} TBWSLOWPOINTERPARAMS;

typedef struct _TBWSINGLEAXISMOVEMENTPARMS {
    uint64_t enable;
    uint64_t modifierKey;
} TBWSINGLEAXISMOVEMENTPARMS;

enum ScrollWheel {
    WHEEL_VERT = 1,
    WHEEL_HORZ = 2
};

typedef struct _TBWSCROLLWHEELPARAMS {
    int wheel;
    uint64_t invert;
    double speed;
    uint64_t inertial;
} TBWSCROLLWHEELPARAMS, *PTBWSCROLLWHEELPARAMS;

typedef struct _TBWPOINTERPARAMS {
    int64_t flags;
    double speed;
    int64_t acceleration;
} TBWPOINTERPARAMS;

// tells which field in TBWPOINTERPARAMS is relevant
#define POINTER_PARAM_SPEED         0x00000001
#define POINTER_PARAM_ACCELERATION  0x00000002
#define TBWPOINTERPARAMS_ALL          0x00000003

// *&*&*&V1_ADD, 20200310
// whenever new trackball function is added, below definition should be considered also.
// Scroll action direction
#define SCROLL_DIRECTION_UP     1
#define SCROLL_DIRECTION_DOWN   2
#define SCROLL_DIRECTION_LEFT   3
#define SCROLL_DIRECTION_RIGHT  4
#define SCROLL_DIRECTION_TRACK  5 // *&*&*&V1_ADD, 20200309 for scroll with trackball function
#define SCROLL_DIRECTION_AUTO   6 // vannes20200518

// Button actions
#define BUTTON_ACTION_CLICK     0
#define BUTTON_ACTION_SCROLL    1
#define BUTTON_ACTION_DRAG      2
#define BUTTON_ACTION_NOACTION  98
#define BUTTON_ACTION_DISABLE   99

typedef struct _TbwButtonAction {
    uint32_t buttonMask;
    uint8_t action;    // use button action macros above
    union {
        struct {
            uint32_t buttonMask;
            uint32_t _modifiers;
        } ClickActionParams;
        struct {
            uint8_t direction; // use scroll direction macros above
            uint8_t lines;
        } ScrollActionParams;
    } u;
} TBWBUTTONACTION;

typedef struct _TbwButtonsConfig {
    uint32_t      cbSize;
    uint32_t      chordMask;
    uint32_t      chordMsks[16];
    uint32_t      nButtons;
    TBWBUTTONACTION buttons[32];
} TBWBUTTONSCONFIG;

// Asynchronous notifications sent by the driver to user mode app
typedef struct _TBWDRIVEREVENT {
    uint32_t cb;
    uint32_t flags;
    uint32_t buttonDownMask;
    uint32_t buttonUpMask;
    uint64_t time;
} TBWDRIVEREVENT;

const uint32_t TBW_NULL_EVENT = 0x00000001;
const uint32_t TBW_BUTTON_EVENT = 0x00000002;

typedef struct _TBWEMULATEDEVICEACTION {
    uint32_t  cbSize;         // structure size (provision for future enhancement)
    uint32_t  uAction;        // See action types below
    union {
        struct {
            uint32_t mask;      // mask for button click
            uint32_t dir;       // 1 - down, 2 - up, 3 - down & up
        } ButtonClick;
    } u;
} TBWEMULATEDEVICEACTION;

// action codes
#define DA_BUTTONCLICK  1

typedef struct _TBWKEYSTROKE {
    uint64_t keyCode;
    uint64_t down;
} TBWKEYSTROKE;

typedef struct _POSTKEYSTROKE {
    uint64_t count; // number of keystrokes
    TBWKEYSTROKE keystrokes[128];
} TBWPOSTKEYSTROKE;

// These map to tbwhelper.Modifiers.h constants, which in turn
// map to legacy TrackballWorks settings contants.
#define MOD_COMMAND     0x0100
#define MOD_SHIFT       0x0200
#define MOD_OPTION      0x0800
#define MOD_CTRL        0x1000

typedef struct _POSTPOINTERCLICK {
    uint32_t button;    // bitmask where bit 0: button 1, bit 1: button 2
                        // set the relevant bit for button down and clear
                        // it for button up. Driver keeps a button state flag
                        // that'll help it identify the down/up movement
                        // depending on the bits set/cleared.
} TBWPOSTPOINTERCLICK;

enum KWDriverRequestCode {

    // Returns information about this trackball device
    //
    // Call with IOConnectCallStructMethod
    //
    // Args:
    //  TBWDEVICEINFO*, which will be filled with information
    //  anbout the Kensington trackball device.
    kKWDriverGetDeviceInfo,

    // Registers app as the helper app. This has to be done between
    // user session chanages.
    //
    // Call with IOConnectCallScalarMethod
    //
    // Args:
    //  scalarInput[0] = process id of the helper app
    kKWDriverRegisterHelper,

    // Deregister ourselves as the helper app. Only requests from
    // apps that previously registered themselves as the helper app
    // will be able to make this request.
    //
    // Call with IOConnectCallScalarMethod
    //
    // Args:
    //  None
    kKWDriverDeregisterHelper,
    
    // Get slow pointer enabled/disabled status
    //
    // Args:
    //  structureOutput - TBWSLOWPOINTERPARAMS*
    kKWDriverGetSlowPointer,
    
    // Enable/Disable slow pointer
    //
    // Call with IOConnectCallStructMethod
    //
    // Args:
    //  structureInput - TBWSLOWPOINTERPARAMS*
    kKWDriverSetSlowPointer,

    // Get single axis movement enabled status
    //
    // Call with IOConnectCallStructMethod
    //
    // Args:
    //  structOutput - TBWSINGLEAXISMOVEMENTPARMSArgs*
    kKWDriverGetTBWSINGLEAXISMOVEMENTPARMS,

    // Enable/Disable single axis movement
    //
    // Call with IOConnectCallStructMethod
    //
    // Args:
    //  structInput - TBWSINGLEAXISMOVEMENTPARMSArgs*
    kKWDriverSetTBWSINGLEAXISMOVEMENTPARMS,

    // Get current scroll wheel params
    //
    // Call with IOConnectCallStructMethod
    //
    // Args:
    //  structOutput - TBWSCROLLWHEELPARAMS*
    kKWDriverGetScrollWheelParams,

    // Set scroll wheel params
    //
    // Call with IOConnectCallStructMethod
    //
    // Args:
    //  structInput - TBWSCROLLWHEELPARAMS*
    kKWDriverSetScrollWheelParams,

    // Get next driver event from the event queue
    //
    // Call with IOConnectCallStructMethod
    //
    // Args:
    //  structOutput - TBWDRIVEREVENT*
    kKWDriverGetNextEventAsync,

    // Set buttons configuration
    //
    // Call with IOConnectCallStructMethod
    //
    // Args:
    //  TBWBUTTONSCONFIG*
    kKWDriverSetButtonsConfig,

    // Emulate device action
    //
    // Call with IOConnectCallStructMethod
    //
    // Args:
    //  structInput - TBWEMULATEDEVICEACTION*
    kKWDriverEmulateDeviceAction,

    // Set pointer parameters
    //
    // Call with IOConnectCallStructMethod
    //
    // Args:
    //  structOutput - TBWPOINTERPARAMS*
    kKWDriverGetPointerParams,

    // Set pointer parameters
    //
    // Call with IOConnectCallStructMethod
    //
    // Args:
    //  structInput - TBWPOINTERPARAMS*
    kKWDriverSetPointerParams,
    
    // Post a keystroke into the system using the virtual keyboard driver
    //
    // Call with IOConnectCallStructMethod
    //
    // Args
    //  structInput - TBWKEYSTROKE*
    kKWDriverPostKeystroke,
    
    // Post a pointer click into the system using the virual mouse driver
    //
    // Call with IOConnectCallStructMethod
    //
    // Args
    //  structInput - TBWPOSTPOINTERCLICK*
    kKWDriverPostPointerClick,
    
    kKWDriverSentinel
};
