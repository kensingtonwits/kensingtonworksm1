#pragma once

#include "buttons.hpp"
#include <cstdint>

namespace hid_report {

class __attribute__((packed)) pointing_input final {
public:
  pointing_input(void) : buttons{}, x(0), y(0), vertical_wheel(0), horizontal_wheel(0) {}
  bool operator==(const pointing_input& other) const { return (memcmp(this, &other, sizeof(*this)) == 0); }
  bool operator!=(const pointing_input& other) const { return !(*this == other); }

  buttons buttons;
  int8_t x;
  int8_t y;
  int8_t vertical_wheel;
  int8_t horizontal_wheel;
};

} // namespace hid_report
