//
//  ServiceHIDKeyboard.hpp
//  trackball2
//
//  Created by Hariharan Mahadevan on 2019/1/25.
//  Copyright © 2019 Kensington. All rights reserved.
//

#pragma once

#include <IOKit/hidsystem/IOHIKeyboard.h>

class TrackballWorks2Keyboard : public IOHIKeyboard {
    
    OSDeclareDefaultStructors(TrackballWorks2Keyboard)

    typedef IOHIKeyboard super;
    
public:
    static TrackballWorks2Keyboard* withProvider(IOService* provider);

    virtual bool start(IOService* provider) override;
    virtual void stop(IOService* provider) override;
    
    // IOHIDevice.h
    virtual UInt32 deviceType() override;
    virtual UInt32 interfaceID() override;

    // IOHIKeyboard.h
    virtual const unsigned char * defaultKeymapOfLength(UInt32 * length) override;
    virtual void setAlphaLockFeedback(bool val) override;
    virtual void setNumLockFeedback(bool val) override;
    virtual UInt32 maxKeyCodes() override;

    virtual void postEvent(unsigned int keyCode, bool goingDown);
};
