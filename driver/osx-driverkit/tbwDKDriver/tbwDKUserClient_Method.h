//
//  tbwDKUserClient_Method.h
//  tbwDKManager
//
//  Created by ginolin on 2020/8/8.
//  Copyright © 2020 Kensington. All rights reserved.
//

#ifndef tbwDKUserClient_Method_h
#define tbwDKUserClient_Method_h
namespace tbwDK {

enum class user_client_method {
  //
  // keyboard
  //

  tbwDK_trackball_start = 999,
  tbwDK_trackball_terminate,
  tbwDK_trackball_post_report,
  tbwDK_trackball_reset,

};

} // namespace tbwDK

#endif /* tbwDKUserClient_Method_h */
