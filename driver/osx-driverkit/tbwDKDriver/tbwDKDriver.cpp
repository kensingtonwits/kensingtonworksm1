//
//  tbwDKDriver.cpp
//  tbwDKDriver
//
//  Created by ginolin on 2020/7/23.
//  Copyright © 2020 Kensington. All rights reserved.
//

#include <os/log.h>

//#include <DriverKit/IOUserServer.h>
//#include <DriverKit/IOLib.h>

#include "tbwDKDriver.h"
#include "version.hpp"
#include <stdio.h>
#include <stdlib.h>
#include <DriverKit/DriverKit.h>
#include <DriverKit/OSCollections.h>
#include <HIDDriverKit/IOHIDDeviceKeys.h>
#include <HIDDriverKit/IOHIDElement.h>
#include <HIDDriverKit/IOUserHIDEventDriver.h>
#include <HIDDriverKit/IOHIDUsageTables.h>

#include "tbwDKUserClient.h"
#include "hid_report/keyboard_input.hpp"
#include "hid_report/pointing_input.hpp"
#include "IOBufferMemoryDescriptorUtility.hpp"

#include "../../shared/trackball.h"
#include "circularqueue.h"
#include "tblog.h"
#undef  super
#define super IOUserHIDEventDriver

#define LOG_PREFIX "twbDKDriver " TBWDKDRIVER_VERSION


// Bits associated with tbwDKDriver-debug boot-arg
enum {
    kDebugDisableDriver = 1 << 0
};

struct tbwDKDriver_IVars
{
    //IOHIDInterface  *interface;
    OSDictionaryPtr properties;

    TBWDEVICEINFO devInfo;
    BOOLEAN       bhasACPanButtons;
    tbwDKUserClient    *client;

    
    // IOHIDEventDriver.h
    struct {
        OSArray *relative;
        uint8_t *report;
        uint32_t reportLength;
    } pointer;
    
    struct {
        OSArray *elements;
    } scroll;
    
    struct {
        OSArray* elements;
        uint8_t *report;
        uint32_t reportLength;
    } keyboard;
    
    uint32_t last_buttonstate;
    uint64_t lastest_timestamp;
    //uint64_t queued_btndntimestamp;

};

#define _interface      (ivars->interface)
#define _properties     (ivars->properties)
#define _devInfo        (ivars->devInfo)
#define _pointer        (ivars->pointer)
#define _scroll         (ivars->scroll)
//#define _keyboard       (ivars->keyboard)
#define _bhasACPanButtons  (ivars->bhasACPanButtons)
#define _client         (ivars->client)


bool tbwDKDriver::init()
{
    os_log(OS_LOG_DEFAULT, " q:::::::::::P");
    TBLogDebug(" init()");
    
    bool result = false;
    
    result = super::init();
    //require_action(result, exit, IOLog("Init:%x", result));
    if(!result)
    {
        TBLogDebug("Init:%x", result);
        return result;
    }
    
    ivars = IONewZero(tbwDKDriver_IVars, 1);
    
    if (ivars == nullptr)
        return false;
    
exit:
    return result;
}

void tbwDKDriver::free()
{
    TBLogDebug(" free()");
    if (ivars) {
        OSSafeReleaseNULL(_properties);
        OSSafeReleaseNULL(_pointer.relative);
        //OSSafeReleaseNULL(_keyboard.elements);
        OSSafeReleaseNULL(_scroll.elements);
    }
    
    IOSafeDeleteNULL(ivars, tbwDKDriver_IVars, 1);

    super::free();
    
    os_log(OS_LOG_DEFAULT, " q: ================ :p");
}

kern_return_t
IMPL(tbwDKDriver, Start)
{
    kern_return_t ret = kIOReturnUnsupported;
    //uint32_t debugMask = 0;
    
    TBLogDebug("Start()");
    
    // 48347141: need a way to iterate service plane
    //_pointer.appleVendorSupported = true;
    //getProperty(kIOHIDAppleVendorSupported, gIOServicePlane);
    
    provider->CopyProperties(&_properties);
    if (!_properties) {
        return kIOReturnError;
    }
#if 0
    if (!deviceSupported()) {
        return kIOReturnUnsupported;
    }
#endif
    getinfoDescription();
    ret = Start(provider, SUPERDISPATCH);
    //require_noerr_action(ret, exit, HIDServiceLogError("Start: 0x%x", ret));
    
    ret = RegisterService();
    

    
exit:
    if (ret != kIOReturnSuccess) {
        //HIDServiceLogFault("Start failed: 0x%x", ret);
        Stop(provider);
    }
    
    TBLogDebug(" Started");
    
    return ret;
}

kern_return_t
IMPL(tbwDKDriver, Stop)
{
    kern_return_t   ret;
    
    TBLogDebug(" Stop()");
    
    ret = Stop(provider, SUPERDISPATCH);
    //HIDServiceLog("Stop: 0x%x", ret);
    
    return ret;
}

kern_return_t IMPL(tbwDKDriver, NewUserClient) {
  TBLogDebug(" NewUserClient type:%{public}d", type);


  IOService* client;

  auto kr = Create(this, "UserClientProperties", &client);
  if (kr != kIOReturnSuccess) {
    TBLogDebug(" Create failed: 0x%x", kr);
    return kr;
  }
    
    *userClient = OSDynamicCast(IOUserClient, client);
    if (!*userClient) {
      TBLogDebug(" OSDynamicCast failed");
      client->release();
      return kIOReturnError;
    }
    
    _client = (tbwDKUserClient *)*userClient;
    
  TBLogDebug(" UserClient is created");

  return kIOReturnSuccess;
}

void tbwDKDriver::getinfoDescription()
{
    OSNumber *up, *u, *vid, *pid, *ver;
    OSString *product = NULL, *sn = NULL;
        
    product = OSDynamicCast(OSString, OSDictionaryGetValue(_properties, kIOHIDProductKey));
    up = OSDynamicCast(OSNumber, OSDictionaryGetValue(_properties, kIOHIDPrimaryUsagePageKey));
    u = OSDynamicCast(OSNumber, OSDictionaryGetValue(_properties, kIOHIDPrimaryUsageKey));
    vid = OSDynamicCast(OSNumber, OSDictionaryGetValue(_properties, kIOHIDVendorIDKey));
    pid = OSDynamicCast(OSNumber, OSDictionaryGetValue(_properties, kIOHIDProductIDKey));
    ver = OSDynamicCast(OSNumber, OSDictionaryGetValue(_properties, kIOHIDVersionNumberKey));
    sn = OSDynamicCast(OSString, OSDictionaryGetValue(_properties, kIOHIDSerialNumberKey));
    
    if(product)
        memcpy(_devInfo.szName, product->getCStringNoCopy(), product->getLength());
    else
        memcpy(_devInfo.szName, " ", sizeof(" "));

    _devInfo.usVenderId = vid ? vid->unsigned32BitValue() : 0;
    _devInfo.usProductId = pid ? pid->unsigned32BitValue() : 0;
    _devInfo.usVersion = ver ? ver->unsigned16BitValue() : 0;
    if(sn)
        memcpy(_devInfo.szSerialNumber, sn->getCStringNoCopy(), sn->getLength());
    else
        memcpy(_devInfo.szSerialNumber, " ", sizeof(" "));
    
    _bhasACPanButtons = false;
    if(_devInfo.usVenderId == 1149)
    {
        switch(_devInfo.usProductId)
        {
            case 32872: // Ergo Vertical Wireless Trackball
            case 32873: // Ergo Vertical Wireless Trackball (BT)
            case 32925: // wired vertical trackball
                // HW has AC pan buttons
                _bhasACPanButtons = true;
                break;
            default:
                // HW doesn't have AC Pan buttons
                break;
            
        }
    }
    
    os_log(OS_LOG_DEFAULT, "%{public}s usagePage: %d usage: %d vid: %d pid: %d",
          product ? product->getCStringNoCopy() : "",
          up ? up->unsigned32BitValue() : 0,
          u ? u->unsigned32BitValue() : 0,
          vid ? vid->unsigned32BitValue() : 0,
          pid ? pid->unsigned32BitValue() : 0);
exit:
    return;
}

kern_return_t tbwDKDriver::getDeviceInfo(TBWDEVICEINFO* pDevInfo)
{
    memcpy(pDevInfo, &_devInfo, sizeof(TBWDEVICEINFO));
    
    return kIOReturnSuccess;

}


bool tbwDKDriver::parseElements(OSArray *elements)
{
    
        //os_log("tbwDKDriver-parseElements()");
        bool result = false;
        
        result = super::parseElements(elements);

        return result;
}

bool tbwDKDriver::parsePointerElement(IOHIDElement *element)
{
    
        //os_log("tbwDKDriver-parsePointerElement()");
        bool result = false;
        uint32_t usagePage = element->getUsagePage();
        uint32_t usage = element->getUsage();
        //bool absolute = false;

        switch (usagePage) {
            case kHIDPage_GenericDesktop:
                switch (usage) {
                    case kHIDUsage_GD_X:
                    case kHIDUsage_GD_Y:
                        //IOLog("kHIDUsage_GD_X-Y");
                        result = true;
                        break;
                }
                break;
            case kHIDPage_Button:
                //IOLog("kHIDPage_Button");
                result = true;
            default:
                break;
        }
        
        //require_quiet(result, exit);
        if(!result) return result;
            
        if (!_pointer.relative) {
            _pointer.relative = OSArray::withCapacity(4);
            //require(_pointer.relative, exit);
            if(!_pointer.relative)
            {   TBLogDebug("OSArray::withCapacity(4) return null");
                return false;
            }
        }
        _pointer.relative->setObject(element);
        //TBLogDebug("_pointer usage: %d", usage);
        
        
    exit:
        return result;
}


bool tbwDKDriver::parseScrollElement(IOHIDElement *element)
{
    bool result = false;
    uint32_t usagePage = element->getUsagePage();
    uint32_t usage = element->getUsage();
    
    switch (usagePage) {
        case kHIDPage_GenericDesktop:
            switch (usage) {
                case kHIDUsage_GD_Dial:
                case kHIDUsage_GD_Wheel:
                case kHIDUsage_GD_Z:
                    //if ((element->getFlags() & (kIOHIDElementFlagsNoPreferredMask|kIOHIDElementFlagsRelativeMask)) == 0) {
                    //    calibrateCenteredPreferredStateElement(element, _preferredAxisRemovalPercentage);
                    //}
                    //IOLog("kHIDUsage_GD_Wheel-Dial-Z");
                    result = true;
                    break;
            }
            break;
        case kHIDPage_Consumer:
            switch (usage) {
                case kHIDUsage_Csmr_ACPan:
                    TBLogDebug("[TBW] kHIDUsage_Csmr_ACPan");
                    //result = true;
#if 1
                    if(_bhasACPanButtons)
                    {
                        result = true;
                    }
#endif
                    break;
            }
            break;
    }
    
    //require_quiet(result, exit);
    if(!result) return result;
    
    if (!_scroll.elements) {
        _scroll.elements = OSArray::withCapacity(4);
        //require(_scroll.elements, exit);
        if(!_scroll.elements) return false;
    }
    
    _scroll.elements->setObject(element);
    
exit:
    return result;
}


#define SLIMBLADE_PRODUCT_ID    0x2041
#define SLIMBLADE_REPORT_LENGTH 5
#define WIRED_VERTICAL_TRACKBALL_PRODUCT_ID    0x809D
//#define SLIMBLADE_REPORT_LENGTH 5

void tbwDKDriver::handleReport(uint64_t timestamp,
                                           uint8_t *report,
                                           uint32_t reportLength,
                                           IOHIDReportType type,
                                           uint32_t reportID)
{
    //handleVendorReport(timestamp, reportID);
    //TBLogDebug("[TBW] tbwDKDriver-handleReport() reportLength: %d - type : %d -  reportID: %d", reportLength, type, reportID);
    
    //super::handleReport(timestamp, report, reportLength, type, reportID);
#if 0
    if (SLIMBLADE_PRODUCT_ID == _devInfo.usProductId && SLIMBLADE_REPORT_LENGTH == reportLength) {
        // old SlimBlad button 4 & 5 support
        _pointer.reportLength = reportLength;
        _pointer.report = report;

    }
#endif
    _pointer.reportLength = reportLength;
    _pointer.report = report;
    
    handleRelativePointerReport(timestamp, reportID);
    handleScrollReport(timestamp, reportID);
    //handleKeyboardReport(timestamp, reportID);
    //super::handleReport(timestamp, report, reportLength, type, reportID);
}


static inline void setButtonState(uint32_t * state, uint32_t bit, uint32_t value)
{
    uint32_t buttonMask = (1 << bit);

    if ( value != 0 )
        (*state) |= buttonMask;
    else
        (*state) &= ~buttonMask;
}


void tbwDKDriver::handleRelativePointerReport(uint64_t timestamp,
                                                   uint32_t reportID)
{
    //TBLogDebug("tbwDKDriver-handleRelativePointerReport()");
    bool handled = false;
    bool btn_event = false;
    IOFixed x = 0;
    IOFixed y = 0;
    uint32_t buttonState = 0;
            
    //require_quiet(_pointer.relative, exit);
    if(!_pointer.relative)
    {
        //IOLog("Null _pointer.relative!");
        return;
    }
              
    for (unsigned int i = 0; i < _pointer.relative->getCount(); i++) {
        IOHIDElement *element;
        uint64_t elementTimeStamp;
        uint32_t usagePage, usage;
        bool elementIsCurrent;
        
        element = (IOHIDElement *)_pointer.relative->getObject(i);
        
        elementTimeStamp = element->getTimeStamp();
        elementIsCurrent = (element->getReportID()==reportID) && (timestamp == elementTimeStamp);
        
        handled |= elementIsCurrent;
        
        usagePage = element->getUsagePage();
        usage = element->getUsage();
        
        //TBLogDebug(" %d >>  usagepage: %d, usage: %d", i, usagePage, usage);
        switch (usagePage) {
            case kHIDPage_GenericDesktop:
                switch (usage) {
                    case kHIDUsage_GD_X:
                        //x = elementIsCurrent ? element->getValue(0) << 16 : 0; //kIOHIDValueOptionsFlagPrevious
                        x = elementIsCurrent ? element->getValue(0) : 0; //kIOHIDValueOptionsFlagPrevious
                        //IOLog("handleRelativePointerReport()-kHIDUsage_GD_X %d", x);
                        break;
                    case kHIDUsage_GD_Y:
                        //y = elementIsCurrent ? element->getValue(0) << 16 : 0; //kIOHIDValueOptionsFlagPrevious
                        y = elementIsCurrent ? element->getValue(0) : 0; //kIOHIDValueOptionsFlagPrevious
                        //IOLog("handleRelativePointerReport()-kHIDUsage_GD_Y %d", y);
                        break;
                }
                break;
            case kHIDPage_Button:
                bool    bprocessed = false;
                btn_event = true;
                //TBLogDebug("kHIDPage_Button 0x%04x", _pointer.report[0]);
                if ((SLIMBLADE_PRODUCT_ID == _devInfo.usProductId) &&
                        (0x047D == _devInfo.usVenderId) &&
                        (SLIMBLADE_REPORT_LENGTH == _pointer.reportLength))
                {
                    uint8_t value = _pointer.report[4] & 0x03;
                    handled = true;
                    if((usage == 1 && (value & 0x01)) || (usage == 2 && (value & 0x02)))
                    {
                        //TBLogDebug("qusage: %d, value: %d", usage, value);
                        setButtonState(&buttonState, (usage + 1), 1);
                        bprocessed = true;
                    
                    }
                }
                if ((WIRED_VERTICAL_TRACKBALL_PRODUCT_ID == _devInfo.usProductId) && (0x047D == _devInfo.usVenderId) )
                {
                    handled = true;
                }
                if(!bprocessed)
                {
                    //TBLogDebug("handleRelativePointerReport()-kHIDPage_Button %d - %d - %d", buttonState, (usage - 1), element->getValue(0));
                    setButtonState(&buttonState, (usage - 1), element->getValue(0)); //kIOHIDValueOptionsFlagRelativeSimple));

                }
                break;
        }
    } //for-loop
            
    //require_quiet(handled, exit);
    if(!handled) return;

    if (btn_event && (WIRED_VERTICAL_TRACKBALL_PRODUCT_ID == _devInfo.usProductId) && (0x047D == _devInfo.usVenderId) )
    {
        //TBLogDebug("_pointer.report: 0x%04x 0x%04x 0x%04x 0x%04x", _pointer.report[0], _pointer.report[1], _pointer.report[2], _pointer.report[3]);
        if(_pointer.report[0] & 0x20)
        {
            setButtonState(&buttonState, 5, 1);
            TBLogDebug("wired VTB buttonState: 0x%04x", buttonState);
        }
        if(_pointer.report[0] & 0x40)
        {
            setButtonState(&buttonState, 6, 1);
            TBLogDebug("wired VTB buttonState: 0x%04x", buttonState);
        }
    }
    
    if(_client)
    {
        //TBLogDebug("_client->postPointerEvent, last=0x%04x, current=0x%04x", ivars->last_buttonstate, buttonState);
        ivars->lastest_timestamp = timestamp;

        if(ivars->last_buttonstate != buttonState)
        {
            ivars->last_buttonstate = buttonState;
            btn_event = true;
        }
        else
        {
            btn_event = false;
        }

        _client->postPointerEvent(x, y, buttonState, btn_event);
        
    }else
    {
        dispatchRelativePointerEvent(timestamp, x << 16, y << 16, buttonState, 0, true);
    }
    //
    
    //HIDServiceLog("DispatchRelativePointerEvent x: %d y: %d button: 0x%x", x, y, buttonState);
    //os_log(OS_LOG_DEFAULT,"DispatchRelativePointerEvent- x: %d y: %d button: 0x%x - %llu", x >> 16, y >> 16, buttonState, timestamp);

exit:
    return;
}

void tbwDKDriver::handleScrollReport(uint64_t timestamp,
                                                   uint32_t reportID)
{
    //super::handleScrollReport(timestamp, reportID);
    
    TBLogDebug("[TBW] tbwDKDriver-handleScrollReport()");
    
        IOFixed scrollVert = 0;
        IOFixed scrollHoriz = 0;
        
        if(!_scroll.elements) return;
        
        for (unsigned int i = 0; i < _scroll.elements->getCount(); i++) {
            IOHIDElement *element;
            uint64_t elementTimeStamp;
            uint32_t usagePage, usage;
            
            element = (IOHIDElement *)_scroll.elements->getObject(i);
            
            if (element->getReportID() != reportID) {
                continue;
            }
            
            elementTimeStamp = element->getTimeStamp();
            if (timestamp != elementTimeStamp) {
                continue;
            }
            
            usagePage = element->getUsagePage();
            usage = element->getUsage();
            TBLogDebug("[TBW] usage - page = 0x%x - 0x%x", usage, usagePage);
            switch (usagePage) {
                case kHIDPage_GenericDesktop:
                    switch (usage) {
                        case kHIDUsage_GD_Wheel:
                        case kHIDUsage_GD_Dial:
                            if (element->getFlags() & kIOHIDElementFlagsWrapMask) {
                                scrollVert = element->getValue(kIOHIDValueOptionsFlagRelativeSimple);
                            } else {
                                scrollVert = element->getValue(0); //kIOHIDValueOptionsFlagPrevious
                            }
                            if(_client)
                                _client->postWheelEvent(1, scrollVert);
                            break;
                        case kHIDUsage_GD_Z:
                            if (element->getFlags() & kIOHIDElementFlagsWrapMask) {
                                scrollHoriz = element->getValue(kIOHIDValueOptionsFlagRelativeSimple);
                            } else {
                                scrollHoriz = element->getValue(0); //kIOHIDValueOptionsFlagPrevious
                            }
                            if(_client)
                                _client->postWheelEvent(2, scrollHoriz);
                            break;
                        default:
                            break;
                    }
                    break;
                case kHIDPage_Consumer:
                    switch (usage) {
                        case kHIDUsage_Csmr_ACPan:
#if 0
                            if(!_bhasACPanButtons)
                            {
                                TBLogDebug("fake ACPan event!");
                                return;
                            }
#endif
                            scrollHoriz = (-element->getValue(0)); //kIOHIDValueOptionsFlagPrevious
                            //scrollHoriz = (element->getValue(0));
                            if(_client)
                                _client->postWheelEvent(2, scrollHoriz);
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }
        }

    
        if(!(scrollVert || scrollHoriz)) return;
        
        if(!_client)
            dispatchRelativeScrollWheelEvent(timestamp, scrollVert << 16, scrollHoriz << 16, 0, 0, true);
        //IOLog("DispatchScrollWheelEventWithFixed vert: %d horiz: %d", scrollVert, scrollHoriz);
    exit:
        return;
}

void IMPL(tbwDKDriver, postButtonEvent)
{
   TBLogDebug("PostButtonEvent - buttonState : %d============================", buttonState);
    uint64_t timestamp;
	if(ivars->lastest_timestamp > 0)
        timestamp = ivars->lastest_timestamp;
	else
		timestamp = mach_absolute_time();
    dispatchRelativePointerEvent(timestamp,
                                 0, //ivars->_xTmp << 16,
                                 0, //ivars->_yTmp << 16,
                                 buttonState, //ivars->_buttonStateTmp,
                                 0,
                                 true); // vannes
    ivars->lastest_timestamp = 0;

#if 0
    TBLogDebug("postButtonEvent : buttonState %d  - timestamp (%lld, %lld) =============", buttonState, ivars->lastest_timestamp, ivars->queued_btndntimestamp);

    // process only for real button down, up with stored timestamp
    // virtual time stamp is invalid
    uint64_t timestamp = 0;
    
    if(ivars->queued_btndntimestamp > 0)
    {
        TBLogDebug("postButtonEvent : queued_btndntimestamp");
        timestamp = ivars->queued_btndntimestamp;
        ivars->queued_btndntimestamp = ivars->lastest_timestamp;
        ivars->lastest_timestamp = 0;
    }else
    {
        TBLogDebug("postButtonEvent : lastest_timestamp");
        timestamp = ivars->lastest_timestamp;
        ivars->lastest_timestamp = 0;
    }
    
    if(timestamp == 0)
    {
        TBLogDebug("postButtonEvent : timestamp == 0");
        timestamp = mach_absolute_time(); //wrong
    }
      
    dispatchRelativePointerEvent(timestamp,
                                 0,
                                 0,
                                 buttonState,
                                 0,
                                 true);
#endif

}

#if 0

//enum class modifier : uint8_t {
//  left_control = 0x1 << 0,
//  left_shift = 0x1 << 1,
//  left_option = 0x1 << 2,
//  left_command = 0x1 << 3,
//  right_control = 0x1 << 4,
//  right_shift = 0x1 << 5,
//  right_option = 0x1 << 6,
//  right_command = 0x1 << 7,
//};

kern_return_t IMPL(tbwDKDriver, postKBReport) {
  //if (!report) {
  //  return kIOReturnBadArgument;
  //}
 
    
    
    
    //if(_keyboard.elements)
    //{
    //    IOHIDElement *element = OSDynamicCast(IOHIDElement, _keyboard.elements->getObject(1));
    //    if(element)
    //    {
    //        element->setUsage(0x52);
    //        element->setUsagepage(HID_USAGE_PAGE_GENERIC);  //0x07
    //        element->setTimeStamp(mach_absolute_time());
    //        element->setReportID(0x01);
    //        element->setValue(kIOHIDValueOptionsFlagPrevious, 0);
    //        element->setValue(0, 0x52);
    //
    //    }
    //}
    
#if 0
    IOHIDEvent * event = NULL;
    event = super::IOUserHIDEventService::IOHIDEvent::keyboardEvent(0, 0, 0, 0, 0);
    
    
    event = this->IOUserHIDEventService::IOHIDEvent::withType(kIOHIDEventTypeKeyboard, 0);
    
    ->withType(kIOHIDEventTypeKeyboard, 0);
    //IOHIDEvent::withType(kIOHIDEventTypeKeyboard,
    //                             mach_absolute_time(),
    //                             0,
    //                             eventOptions);
    if(!event) return kIOReturnError;
    
    
#endif
    
    
#if 0
    
    //kHIDUsage_KeyboardUpArrow  //0x52
os_log(OS_LOG_DEFAULT,  " tbwDKDriver postKBReport");
    uint8_t kbreport[8] = {0x01, 0x01, 0x52, 0x00, 0x00, 0x00, 0x00, 0x00};
    uint8_t zrreport[8] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    // byte 0 : 0x01
    // byte 1 : modifiers
    // byte 2 ~ l : keys
    uint64_t t = mach_absolute_time();
    
    _xproductID = 0xffff;
    handleReport(t, kbreport, 8, kIOHIDReportTypeInput, 0);
    handleReport(t+1000, zrreport, 8, kIOHIDReportTypeInput, 0);
    _xproductID = 0;
#endif
   
    return kIOReturnSuccess;
}
#endif

#if 0
kern_return_t tbwDKDriver::qpostReport(IOUserClientMethodArguments *arguments) {
  //if (!arguments) {
  //  return kIOReturnBadArgument;
  //}

    //IOLog("iolog tbwDKDriver post report ");
    
    os_log(OS_LOG_DEFAULT,  " tbwDKDriver post report");
    
    hid_report::keyboard_input* in;
    in = (hid_report::keyboard_input*) (arguments->structureInput);
    os_log(OS_LOG_DEFAULT,  " x11");
    uint8_t *keys;
    keys = (uint8_t*)in->keys.get_raw_value();
    uint8_t mod;
    os_log(OS_LOG_DEFAULT,  " x22");
    for(int i =0;i<32;i++)
    {
        //os_log(OS_LOG_DEFAULT, " keys[%d] ", i);//"= %x", i, keys[i]);
        //IOLog("iolog keys[%d] = %x", i, keys[i]);
    }
    os_log(OS_LOG_DEFAULT,  " x33");
    //mod = in->modifiers.get_raw_value();
    //IOLog("post report: key = %x%x, modifiers = %x ", keys[0], keys[1], mod);
    
    //os_log(OS_LOG_DEFAULT,  "keys = %x%x, modifiers = %x ", mod, mod, mod);
    uint32_t usagePage = 0x07;  //HID_USAGE_PAGE_GENERIC;//0x07;
    uint32_t usage = 0x52; //HID_USAGE_GENERIC_KEYBOARD;
    IOHIDElement *element;
      //uint64_t timestamp;

      
      element = (IOHIDElement *)_pointer.relative->getObject(0);
      
      //timestamp = element->getTimeStamp();
    uint64_t t = mach_absolute_time();
    dispatchKeyboardEvent(t, usagePage, 0xe0, 1, 0, true);
    dispatchKeyboardEvent(t+100, usagePage, 0x52, 1, 0, true);
    dispatchKeyboardEvent(t+200, usagePage, 0x52, 0, 0, true);
    dispatchKeyboardEvent(t+300, usagePage, 0xe0, 0, 0, true);
    os_log(OS_LOG_DEFAULT,  " x44");
    
    handleReport(<#uint64_t timestamp#>, uint8_t *report, <#uint32_t reportLength#>, <#IOHIDReportType type#>, <#uint32_t reportID#>)
    
    
    os_log(OS_LOG_DEFAULT,  "time = %llu - ",t);
    //handleKeyboardReport(t, 0);
    return kIOReturnSuccess;
}
#endif

#if 0
void tbwDKDriver::handleKeyboardReport(uint64_t timestamp,
                                                uint32_t reportID)
{
    os_log(OS_LOG_DEFAULT,  "handleKeyboardReport");
    
    if(_xproductID == 0xffff)
    {
        uint64_t t = mach_absolute_time();
        os_log(OS_LOG_DEFAULT,  "_xproductID == 0xffff");
        dispatchKeyboardEvent(t+200, 0x07, 0xe0, 1, 0, true);
        dispatchKeyboardEvent(t+300, 0x07, 0x52, 1, 0, true);
        dispatchKeyboardEvent(t+400, 0x07, 0x52, 0, 0, true);
        dispatchKeyboardEvent(t+500, 0x07, 0xe0, 0, 0, true);
        return;
    }
    
    //require_quiet(_keyboard.elements, exit);
    if(!_keyboard.elements)
        return;
    
    
    for (unsigned int i = 0; i < _keyboard.elements->getCount(); i++) {
        IOHIDElement *element = NULL;
        uint64_t elementTimeStamp;
        uint32_t usagePage, usage, value, preValue;
        
        element = (IOHIDElement *)_keyboard.elements->getObject(i);
        
        os_log(OS_LOG_DEFAULT,  "reportID == %d", element->getReportID());
                
        if (element->getReportID() != reportID) {
            continue;
        }
        
        os_log(OS_LOG_DEFAULT,  "reportID == %d", element->getReportID());
        
        elementTimeStamp = element->getTimeStamp();
        if (timestamp != elementTimeStamp) {
            continue;
        }
        
        preValue = element->getValue(kIOHIDValueOptionsFlagPrevious) != 0;
        value = element->getValue(0) != 0;
        
        if (value == preValue) {
            continue;
        }
        
        usagePage = element->getUsagePage();
        usage = element->getUsage();
        
        //dispatchKeyboardEvent(timestamp, usagePage, usage, value, 0, true);
        
        uint64_t t = mach_absolute_time();
        //dispatchKeyboardEvent(timestamp+200, 0x07, 0xe0, 1, 0, true);
        //dispatchKeyboardEvent(timestamp+300, 0x07, 0x52, 1, 0, true);
        //dispatchKeyboardEvent(timestamp+400, 0x07, 0x52, 0, 0, true);
        //dispatchKeyboardEvent(timestamp+500, 0x07, 0xe0, 0, 0, true);
        dispatchKeyboardEvent(t+200, 0x07, 0xe0, 1, 0, true);
        dispatchKeyboardEvent(t+300, 0x07, 0x52, 1, 0, true);
        dispatchKeyboardEvent(t+400, 0x07, 0x52, 0, 0, true);
        dispatchKeyboardEvent(t+500, 0x07, 0xe0, 0, 0, true);
        os_log(OS_LOG_DEFAULT, "timestamp = %llu, t = %llu", timestamp, t);
        os_log(OS_LOG_DEFAULT, "Handle Key Report - usage: 0x%x, page: 0x%x, val: 0x%x", usage, usagePage, value);
        
        //IOLog("Handle Key Report - usage: 0x%x, page: 0x%x, val: 0x%x", usage, usagePage, value);
    }

    //super::handleKeyboardReport(timestamp, reportID);
    
exit:
    return;
}



bool tbwDKDriver::parseKeyboardElement(IOHIDElement *element)
{
    bool result = false;
    uint32_t usagePage = element->getUsagePage();
    uint32_t usage = element->getUsage();
    os_log(OS_LOG_DEFAULT,  "parseKeyboardElement : %d %d", usagePage, usage);
    
    switch (usagePage) {
        case kHIDPage_GenericDesktop:
            switch (usage) {
                case kHIDUsage_GD_Start:
                case kHIDUsage_GD_Select:
                case kHIDUsage_GD_SystemPowerDown:
                case kHIDUsage_GD_SystemSleep:
                case kHIDUsage_GD_SystemWakeUp:
                case kHIDUsage_GD_SystemContextMenu:
                case kHIDUsage_GD_SystemMainMenu:
                case kHIDUsage_GD_SystemAppMenu:
                case kHIDUsage_GD_SystemMenuHelp:
                case kHIDUsage_GD_SystemMenuExit:
                case kHIDUsage_GD_SystemMenuSelect:
                case kHIDUsage_GD_SystemMenuRight:
                case kHIDUsage_GD_SystemMenuLeft:
                case kHIDUsage_GD_SystemMenuUp:
                case kHIDUsage_GD_SystemMenuDown:
                case kHIDUsage_GD_DPadUp:
                case kHIDUsage_GD_DPadDown:
                case kHIDUsage_GD_DPadRight:
                case kHIDUsage_GD_DPadLeft:
                    result = true;
                    break;
            }
            break;
        case kHIDPage_KeyboardOrKeypad:
            if (usage < kHIDUsage_KeyboardA ||
                usage > kHIDUsage_KeyboardRightGUI) {
                break;
            }
        case kHIDPage_Consumer:
            // kHIDUsage_Csmr_ACPan is handled in parseScrollElement
            if (usage != kHIDUsage_Csmr_ACPan) {
                result = true;
            }
            break;
        case kHIDPage_Telephony:
        case kHIDPage_CameraControl:
            result = true;
            break;
        default:
            break;
    }
    
    //require_quiet(result, exit);
    if(!result)
        return result;
    
    
    if (!_keyboard.elements) {
        _keyboard.elements = OSArray::withCapacity(4);
        //require(_keyboard.elements, exit);
        if(!_keyboard.elements)
            return false;
    }
    
    _keyboard.elements->setObject(element);
    
exit:
    return result;
}
#endif

