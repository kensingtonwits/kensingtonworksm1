// WindowsProject2.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "kwdwnldr.h"

#include <iostream>
#include <fstream>
#include <cstdarg>
#include <utility>
#include <thread>
#include <chrono>
#include <Rpc.h>
#include <ctime>
#pragma comment(lib, "rpcrt4.lib")

#include <WtsApi32.h>
#pragma comment(lib, "wtsapi32.lib")

#include <Msi.h>
#pragma comment(lib, "msi.lib")

#include "conslsvc.h"
#include "TrayIcon.h"

#define LOG_VERBOSE	0
#define LOG_DEBUG	1
#define LOG_INFO	2
#define LOG_WARN	3
#define LOG_ERROR	4

wchar_t g_logFile[1024] = { 0 };
#ifdef _DEBUG
int g_logLevel = LOG_VERBOSE;
#else
int g_logLevel = LOG_INFO;
#endif

CTrayIcon* g_trayIcon = nullptr;

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name
HWND g_hWnd = NULL;

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

const wchar_t* KENSINGTONWORKS_WEB = L"https://www.kensington.com/software/kensingtonworks/";	// L"https://www.kensington.com";
const wchar_t* APP_REGISTRY_KEY = L"SOFTWARE\\Kensington\\KensingtonWorks2";
const wchar_t* REGISTRY_VALUE_NAME = L"DownloadNotified";

//&*&*&*G1_ADD
WCHAR szMessage[MAX_LOADSTRING];                  // The title bar text
const std::wstring locales[] = { L"en", L"de", L"fr", L"nl", L"es", L"it", L"pt", L"da", L"nb", L"sv", 
								L"fi", L"et", L"lv", L"lt", L"pl", L"cs", L"sk", L"hu", L"ro", L"ru", 
								L"uk", L"kk", L"tr", L"el", L"ar", L"zh-CN", L"ja", L"zh-TW", L"ko" };
const unsigned int langIndex[] = { 
	IDS_BALLOON_MESSAGE_en, 
	IDS_BALLOON_MESSAGE_de, 
	IDS_BALLOON_MESSAGE_fr,
    IDS_BALLOON_MESSAGE_nl,          
    IDS_BALLOON_MESSAGE_es,          
    IDS_BALLOON_MESSAGE_it,          
    IDS_BALLOON_MESSAGE_pt,          
    IDS_BALLOON_MESSAGE_da,          
    IDS_BALLOON_MESSAGE_nb,          
    IDS_BALLOON_MESSAGE_sv,          
    IDS_BALLOON_MESSAGE_fi,          
    IDS_BALLOON_MESSAGE_et,          
    IDS_BALLOON_MESSAGE_lv,          
    IDS_BALLOON_MESSAGE_lt,          
    IDS_BALLOON_MESSAGE_pl,          
    IDS_BALLOON_MESSAGE_cs,          
    IDS_BALLOON_MESSAGE_sk,          
    IDS_BALLOON_MESSAGE_hu,          
    IDS_BALLOON_MESSAGE_ro,          
    IDS_BALLOON_MESSAGE_ru,          
    IDS_BALLOON_MESSAGE_uk,          
    IDS_BALLOON_MESSAGE_kk,          
    IDS_BALLOON_MESSAGE_tr,          
    IDS_BALLOON_MESSAGE_el,          
    IDS_BALLOON_MESSAGE_ar,          
    IDS_BALLOON_MESSAGE_zh_CN,       
    IDS_BALLOON_MESSAGE_ja,          
    IDS_BALLOON_MESSAGE_zh_TW,       
	IDS_BALLOON_MESSAGE_ko			 };
//&*&*&*G2_ADD

const WCHAR* getLogFileFullName()
{
	static WCHAR s_szLogFileFullName[MAX_PATH + 1] = { 0 };
	if (::wcslen(s_szLogFileFullName) == 0)
	{
		size_t nChars = ::GetTempPath(
			sizeof(s_szLogFileFullName) / sizeof(s_szLogFileFullName[0]),
			s_szLogFileFullName);
		if (nChars > 0)
		{
			if (s_szLogFileFullName[nChars - 1] != L'\\')
			{
				::wcscat_s(s_szLogFileFullName, L"\\");
			}
			::wcscat_s(s_szLogFileFullName, L"tbwsvc.log");
			OutputDebugStringW(s_szLogFileFullName);
		}

	}

	return s_szLogFileFullName;
}

void log(const char* msg)
{
	try
	{
		CHAR szTo[MAX_PATH + 1] = { 0 };
		if (::WideCharToMultiByte(CP_ACP, 0, getLogFileFullName(), -1, szTo, sizeof(szTo), NULL, NULL) > 0)
		{
			std::ofstream of(szTo, std::ofstream::app);
			of << msg;
		}
	}
	catch (const std::exception&)
	{

	}
}

void logW(const wchar_t* msg)
{
	try
	{
		std::wofstream of(getLogFileFullName(), std::ofstream::app);
		of << msg;
	}
	catch (const std::exception&)
	{
	}
}

void logMsgA(int level, const char* format, ...)
{
	if (level < g_logLevel)
		return;

	char buffer[2048] = { 0 };
	va_list args;
	va_start(args, format);
	vsnprintf_s(buffer, sizeof(buffer) / sizeof(buffer[0]), format, args);
	OutputDebugStringA(buffer);
	log(buffer);
}

void logMsgW(int level, const wchar_t* format, ...)
{
	if (level < g_logLevel)
		return;

	wchar_t buffer[2048] = { 0 };
	va_list args;
	va_start(args, format);
	vswprintf_s(buffer, sizeof(buffer) / sizeof(buffer[0]), format, args);
	OutputDebugStringW(buffer);
	logW(buffer);
}


#ifdef _DEBUG
const HKEY HKEY_APP_ROOT = HKEY_CURRENT_USER;
#else
const HKEY HKEY_APP_ROOT = HKEY_LOCAL_MACHINE;
#endif

/*
 * Returns a boolean indicating if KensingtonWorks App is installed.
 *
 * Does this by checking the 
 *	HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Run
 * for value 'KensingtonWorks'.
 */
bool isKensingtonWorksInstalled()
{
	/*
	DWORD dwType = REG_SZ, dwValue = 0, dwRet = 0;;
	TCHAR szValue[512] = { 0 };
	DWORD cbData = sizeof(szValue);
	HKEY hKey = NULL;

#if _WIN64
	const wchar_t* AUTO_RUN_REG_KEY = L"SOFTWARE\\WOW6432Node\\Microsoft\\Windows\\CurrentVersion\\Run";
#else
	const wchar_t* AUTO_RUN_REG_KEY = L"SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run";
#endif

	if (::RegOpenKey(HKEY_LOCAL_MACHINE, AUTO_RUN_REG_KEY, &hKey) == ERROR_SUCCESS) {
		dwRet = ::RegQueryValueEx(hKey, L"KensingtonWorks2", NULL, &dwType, (LPBYTE)szValue, &cbData);
		RegCloseKey(hKey);
		if (dwRet == ERROR_SUCCESS) {
			return ::wcslen(szValue) > 0;
		}
	}
	return false;
	*/
	// Use MSI API to detect if KensingtonWorks2 is installed.
	WCHAR szProductCode[40] = { 0 };
	// Note that this is the link between tbwsvc and the installer, and therefore
	// if MSI installer's upgrade code is changed, it has to be updated here too.
	const LPCWSTR KGTW_MSI_UPGRADE_CODE = L"{3b5012c7-25b4-4337-84a2-0fc109607e95}";
	UINT uRet = MsiEnumRelatedProducts(
		KGTW_MSI_UPGRADE_CODE,
		0,
		0,
		szProductCode);
	return uRet == ERROR_SUCCESS;
}

/*
 * Returns a boolean indicating if we have notified the user about KGTW package.
 */
time_t lastNotificationTime()
{
	time_t last = 0;
	DWORD cbData = sizeof(time_t);
	HKEY hKey = NULL;
	if (::RegOpenKey(HKEY_APP_ROOT, APP_REGISTRY_KEY, &hKey) == ERROR_SUCCESS) {
		DWORD dwRet = ::RegQueryValueEx(hKey, REGISTRY_VALUE_NAME, NULL, NULL, (LPBYTE)&last, &cbData);
		RegCloseKey(hKey);
		if (dwRet == ERROR_SUCCESS) {
			return last;
		}
	}
	return 0;
}

/*
 * If the application key exists, opens it with read/write access. If it does not
 * creates it.
 *
 * Returns the HKEY, if successful. NULL otherwise.
 */
HKEY createKeyIfDoesNotExist()
{
	HKEY hKey = NULL;
	if (::RegOpenKeyEx(HKEY_APP_ROOT, APP_REGISTRY_KEY, 0, KEY_READ|KEY_WRITE, &hKey) != ERROR_SUCCESS) {
		::RegCreateKey(HKEY_APP_ROOT, APP_REGISTRY_KEY, &hKey);
	}
	return hKey;
}

/*
 * Sets the DownloadNotified value to current time such that we can throttle
 * such messages to the user.
 */
void setNotificationTime()
{
	HKEY hKey = createKeyIfDoesNotExist();
	if (hKey) {
		time_t now = 0; // 64-bit int
		time(&now);
		DWORD dwRet = ::RegSetValueEx(hKey, REGISTRY_VALUE_NAME, 0, REG_QWORD, (LPBYTE)&now, sizeof(time_t));
		if (dwRet != ERROR_SUCCESS) {
			logMsgA(0, "Error setting registry value: %d\n", dwRet);
		}
		RegCloseKey(hKey);
	}
}

/*
 * Returns a boolean indicating if user is logged in.
 */
bool isUserLoggedIn()
{
	DWORD sessionId = ::WTSGetActiveConsoleSessionId();
	return sessionId != 0xffffffff;
}

/*
 * Starts a process in the logged in user's context, if one is logged in.
 */
bool StartInteractiveProcess(LPTSTR cmd, LPCTSTR cmdDir)
{
	STARTUPINFO si;
	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	si.lpDesktop = (LPWSTR)TEXT("winsta0\\default");  // Use the default desktop for GUIs
	PROCESS_INFORMATION pi;
	ZeroMemory(&pi, sizeof(pi));
	HANDLE token;
	DWORD sessionId = ::WTSGetActiveConsoleSessionId();
	if (sessionId == 0xffffffff)
	{
		// Noone is logged-in
		logMsgA(LOG_ERROR, "No one is logged in\n");
		return false;
	}

	// This only works if the current user is the system-account (we are probably a Windows-Service)
	HANDLE dummy;
	if (::WTSQueryUserToken(sessionId, &dummy)) {
		if (!::DuplicateTokenEx(dummy, TOKEN_ALL_ACCESS, NULL, SecurityDelegation, TokenPrimary, &token)) {
			::CloseHandle(dummy);
			logMsgA(LOG_ERROR, "Failed to duplicate token, %d\n", ::GetLastError());
			return false;
		}
		::CloseHandle(dummy);
		// Create process for user with desktop
		if (!::CreateProcessAsUser(
			token, 
			NULL, 
			cmd, 
			NULL, 
			NULL, 
			FALSE, 
			CREATE_BREAKAWAY_FROM_JOB,
			NULL, 
			cmdDir, 
			&si, 
			&pi)) {  // The "new console" is necessary. Otherwise the process can hang our main process
			::CloseHandle(token);
			logMsgA(LOG_ERROR, "Failed to create process(1) as user, %d\n", ::GetLastError());
			return false;
		}
		::CloseHandle(token);
	}
	::CloseHandle(pi.hProcess);
	::CloseHandle(pi.hThread);
	return true;
}


/*
 * Launch the product page.
 *
 * Essentially forks explorer.exe with the URL. Explorer, being a single instance
 * process, would find the active explorer and then would signal it to open the
 * the URL and quit.
 */
void launchProductPage()
{
	STARTUPINFO si;
	ZeroMemory(&si, sizeof(si));
	si.cb = sizeof(si);
	si.lpDesktop = (LPWSTR)TEXT("winsta0\\default");  // Use the default desktop for GUIs
	PROCESS_INFORMATION pi;
	ZeroMemory(&pi, sizeof(pi));
	WCHAR szCmd[1024] = { 0 };
	::wcscpy_s(szCmd, L"c:\\windows\\explorer.exe ");
	::wcscat_s(szCmd, KENSINGTONWORKS_WEB);
	if (::CreateProcessW(NULL,
		szCmd,
		NULL,
		NULL,
		FALSE,
		DETACHED_PROCESS,
		NULL,
		L"c:\\windows\\system32",
		&si,
		&pi))
	{
		logMsgA(LOG_INFO, "KensingtonWorks product webpage started\n");
		::CloseHandle(pi.hThread);
		::CloseHandle(pi.hProcess);
	}
	else
	{
		logMsgA(LOG_ERROR, "Error creating launching product webpage, rc: %d\n", ::GetLastError());
	}
}

void forkToGotoWeb()
{
	logMsgW(LOG_DEBUG, L"kwdwnldr - forking with -gotoweb argument\n");
	WCHAR szThisFile[MAX_PATH] = { 0 }, szDirectory[MAX_PATH] = { 0 }, szCommand[MAX_PATH] = { 0 };
	::GetModuleFileNameW(NULL, szThisFile, sizeof(szThisFile) / sizeof(szThisFile[0]));
	::wcscpy_s(szDirectory, MAX_PATH, szThisFile);
	WCHAR* pc = ::wcsrchr(szDirectory, L'\\');
	if (pc) {
		*pc = L'\0';
	}

	::swprintf_s(szCommand, L"\"%s\" -gotoweb", szThisFile);
	if (!StartInteractiveProcess(szCommand, szDirectory))
	{
		logMsgA(LOG_ERROR, "Error forking to display product page\n");
	}
}

void forkToShowPopup()
{
	logMsgW(LOG_DEBUG, L"kwdwnldr - forking with -gotoweb argument\n");
	WCHAR szThisFile[MAX_PATH] = { 0 }, szDirectory[MAX_PATH] = { 0 }, szCommand[MAX_PATH] = { 0 };
	::GetModuleFileNameW(NULL, szThisFile, sizeof(szThisFile) / sizeof(szThisFile[0]));
	::wcscpy_s(szDirectory, MAX_PATH, szThisFile);
	WCHAR* pc = ::wcsrchr(szDirectory, L'\\');
	if (pc) {
		*pc = L'\0';
	}

	::swprintf_s(szCommand, L"\"%s\" -showpopup", szThisFile);
	if (!StartInteractiveProcess(szCommand, szDirectory))
	{
		logMsgA(LOG_ERROR, "Error forking to display product page\n");
	}
}

void notifyThread()
{
#ifdef _DEBUG
	const unsigned MAX_WAIT_TIME = 20;
#else
	const unsigned MAX_WAIT_TIME = 60;
#endif

	time_t lastNotify = lastNotificationTime();
	if (lastNotify != 0)
	{
		logMsgA(LOG_INFO, "Notify thread - previously notified, nothing to do.\n");
		return;
	}

	auto start = std::chrono::system_clock::now();
	while (1)
	{
		auto elapsed = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now() - start);
		if (elapsed.count() >= MAX_WAIT_TIME)
			break;

		logMsgA(LOG_DEBUG, "Notify thread - sleeping 1 second\n");
		std::this_thread::sleep_for(std::chrono::milliseconds(1000)); // sleep for 1 second

		logMsgA(LOG_DEBUG, "Notify thread - checking for KensingtonWorks...\n");
		if (isKensingtonWorksInstalled()) {
			logMsgA(LOG_INFO, "Notify thread - KensingtonWorks detected, nothing to do.\n");
			return;
		}
	}

	// we timedout, display the popup
	logMsgA(LOG_INFO, "Notify thread - forking self to show popup.\n");
	setNotificationTime();
	forkToShowPopup();
}

class MyService : public ConsoleService {	// uses FileLogger
	typedef ConsoleService baseClass;
public:
	MyService()
		: baseClass(_T("myservice"))
	{}

	// override Run() so that we can register for device notifications
	DWORD run()
	{
		logMsgA(LOG_INFO, "Starting Kensington TrackballWorks service...\n");

		// specify flags for additional control commands we accept
		//status_.dwControlsAccepted = SERVICE_ACCEPT_SHUTDOWN | SERVICE_ACCEPT_STOP;

		std::thread t(notifyThread);
		
		// call base class method to block on the quit event
		DWORD dwRet = baseClass::run();

		// service quitting...
		logMsgA(LOG_INFO, "Stopping Kensington TrackballWorks service...\n");

		t.join();

		return dwRet;
	}

	virtual void onShutdown()
	{
		// do any shutdown wind-down tasks here
	}

	virtual DWORD onSessionChange(DWORD dwEventType, PWTSSESSION_NOTIFICATION pSession)
	{
		logMsgW(LOG_DEBUG, L"onSessionChange - eventCode: %d, SessionId: %d\n", (int)dwEventType, (int)pSession->dwSessionId);
		return NO_ERROR;
	}

	virtual void onInterrogate() throw()
	{
#ifdef _DEBUG
		forkToGotoWeb();
		//forkToShowPopup();
#endif
	}

private:
};

MyService _service;

int runAsApp(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPWSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_WINDOWSPROJECT2, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_WINDOWSPROJECT2));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}

void TrayIconMessageFunc(CTrayIcon* pTrayIcon, UINT uMsg)
{
	switch (uMsg)
	{
	case NIN_BALLOONUSERCLICK:
		launchProductPage();
		if (g_hWnd) {
			PostMessage(g_hWnd, WM_COMMAND, IDM_EXIT, 0);
		}
		break;

#if 0
	// Enabling this terminates the program when balloon is hidden either
	// by the user or by the system default hide timeout occurs.
	case NIN_BALLOONHIDE:
	case NIN_BALLOONTIMEOUT:
		if (g_hWnd) {
			PostMessage(g_hWnd, WM_COMMAND, IDM_EXIT, 0);
		}
		break;
#endif

	default:
		break;
	}
}


//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_WINDOWSPROJECT2));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_WINDOWSPROJECT2);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_WINDOWSPROJECT2));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   g_hWnd = hWnd;
   ShowWindow(hWnd, SW_HIDE);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE: Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
	case WM_CREATE:
		{
			LRESULT lRet = DefWindowProc(hWnd, message, wParam, lParam);
			g_trayIcon = new CTrayIcon(L"KensingtonWorks",
				false,
				LoadIcon(NULL, MAKEINTRESOURCE(IDI_WINDOWSPROJECT2)));
			g_trayIcon->SetListener(TrayIconMessageFunc);
			g_trayIcon->SetIcon(LoadIcon(hInst, MAKEINTRESOURCE(IDI_WINDOWSPROJECT2)));
			g_trayIcon->SetVisible(true);


			TCHAR szTitle[512] = { 0 }, szMessage[1024] = { 0 };
			LoadStringW(hInst, IDS_BALLOON_TITLE, szTitle, MAX_LOADSTRING);

			//&*&*&*G1_MOD, localization
			//LoadStringW(hInst, IDS_BALLOON_MESSAGE, szMessage, MAX_LOADSTRING);
			ULONG numberOfLanguages = 0;
			DWORD bufferLength = 0;
			std::wstring szLang;
			UINT	msgId = IDS_BALLOON_MESSAGE_en;

			GetUserPreferredUILanguages(MUI_LANGUAGE_NAME, &numberOfLanguages, nullptr, &bufferLength);  //MUI_LANGUAGE_ID
			if (bufferLength > 0)
			{
				TCHAR szll[32];
				int index;
				size_t f;

				szLang.resize(bufferLength);
				auto result2 = GetUserPreferredUILanguages(MUI_LANGUAGE_NAME, &numberOfLanguages, (PZZWSTR)szLang.data(), &bufferLength);
				std::wstring b = szLang.data();
				size_t lb = b.length();

				for (index = 0; index < _countof(locales); index++)
				{
					std::wstring a = locales[index].data();
					f = b.find(a);
					size_t la = a.length();
					if (f <= (lb - la))  // != std::string::npos)
					{
						msgId = langIndex[index];
						break;
					}
				}
			}
			//TCHAR szMessage[1024] = { 0 };
			if (LoadStringW(hInst, msgId, szMessage, MAX_LOADSTRING) == 0)
			{
				LoadStringW(hInst, IDS_BALLOON_MESSAGE, szMessage, MAX_LOADSTRING);
			}

			//MessageBoxW(NULL, szMessage, L"HelloMUI !", MB_OK | MB_ICONERROR);
			//&*&*&*G2_MOD, localization

			g_trayIcon->ShowBalloonTooltip(
				szTitle,
				szMessage,
				CTrayIcon::eTI_Info
			);

			// auto terminate program in 1 minute
			SetTimer(hWnd, 100, 1 * 60 * 1000, NULL);
			return lRet;
		}
		break;

	case WM_TIMER:
		{
			KillTimer(hWnd, 100);
			PostMessage(hWnd, WM_COMMAND, IDM_EXIT, 0);
		}
		break;

    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            // Parse the menu selections:
            switch (wmId)
            {
#if 0
			case IDM_ABOUT:
                DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
                break;

			case ID_HELP_OPENURL:
				{
					logMsgA(LOG_INFO, "KW Downloader service - opening KGTW products page 2\n");
					HINSTANCE hInst = ShellExecuteW(NULL, L"open", KENSINGTONWORKS_WEB, NULL, NULL, SW_SHOWDEFAULT);
					if (hInst > (HINSTANCE)32) {
						return 0;
					}
					else
					{
						logMsgA(LOG_ERROR, "Error opening kensington web, rc: %d", (int)hInst);
					}
				}
				break;

			case ID_HELP_MESSAGEBOX:
				{
					STARTUPINFO si;
					ZeroMemory(&si, sizeof(si));
					si.cb = sizeof(si);
					si.lpDesktop = (LPWSTR)TEXT("winsta0\\default");  // Use the default desktop for GUIs
					PROCESS_INFORMATION pi;
					ZeroMemory(&pi, sizeof(pi));
					WCHAR szCmd[1024] = { 0 };
					::wcscpy_s(szCmd, L"c:\\windows\\explorer.exe ");
					::wcscat_s(szCmd, KENSINGTONWORKS_WEB);
					if (!::CreateProcessW(NULL,
						szCmd,
						NULL,
						NULL,
						FALSE,
						DETACHED_PROCESS,
						NULL,
						L"c:\\windows\\system32",
						&si,
						&pi))
					{
						logMsgA(LOG_ERROR, "Error creating explorer.exe, rc: %d\n", ::GetLastError());
					}
					else
					{
						::CloseHandle(pi.hProcess);
					}
					//MessageBox(hWnd, L"Hello World", L"Sample Box", MB_OK);
				}
				break;
#endif
            case IDM_EXIT:
                DestroyWindow(hWnd);
                break;

			default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: Add any drawing code that uses hdc here...
            EndPaint(hWnd, &ps);
        }
        break;
    case WM_DESTROY:
		if (g_trayIcon)
		{
			g_trayIcon->SetVisible(false);
			delete g_trayIcon; g_trayIcon = nullptr;
		}
		PostQuitMessage(0);
        break;

	default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPWSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	::wcscat_s(g_logFile, L"c:\\windows\\logs\\tbwsvc.log");

#ifdef _DEBUG	// to test get/set registry values & lastNotification time
	notifyThread();
#endif

	LPWSTR cmdLine = ::GetCommandLineW();
	logMsgW(LOG_DEBUG, L"KW Downloader service - command line: %s\n", cmdLine);

	// Look for -gotoweb (or /gotoweb) argument.
	int nArgs = 0;
	LPWSTR* alpszArgs = ::CommandLineToArgvW(cmdLine, &nArgs);
	for (int i = 0; i < nArgs; i++) {
		LPWSTR lpszArg = alpszArgs[i];
		if (lpszArg[0] == L'/' || lpszArg[0] == L'-') {
			if (::_wcsicmp(&lpszArg[1], L"gotoweb") == 0)
			{
				launchProductPage();
				return 0;
			}
			else if (::_wcsicmp(&lpszArg[1], L"showpopup") == 0)
			{
				runAsApp(hInstance, hPrevInstance, lpCmdLine, SW_SHOWNORMAL);
				return 0;
			}
		}
	}

	_service.start();

	return 0;
}
