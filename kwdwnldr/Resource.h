//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by kwdwnldr.rc
//
#define IDC_MYICON                      2
#define IDD_WINDOWSPROJECT2_DIALOG      102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDS_BALLOON_TITLE               104
#define IDM_EXIT                        105
#define IDI_WINDOWSPROJECT2             107
#define IDI_SMALL                       108
#define IDC_WINDOWSPROJECT2             109
#define IDR_MAINFRAME                   128
#define IDI_ICON1                       129
#define ID_HELP_MESSAGEBOX              32771
#define ID_HELP_OP                      32772
#define ID_HELP_OPENURL                 32773
#define IDS_BALLOON_MESSAGE             105
#define IDS_BALLOON_MESSAGE_en          201
#define IDS_BALLOON_MESSAGE_de          202
#define IDS_BALLOON_MESSAGE_fr          203
#define IDS_BALLOON_MESSAGE_nl          204
#define IDS_BALLOON_MESSAGE_es          205
#define IDS_BALLOON_MESSAGE_it          206
#define IDS_BALLOON_MESSAGE_pt          207
#define IDS_BALLOON_MESSAGE_da          208
#define IDS_BALLOON_MESSAGE_nb          209
#define IDS_BALLOON_MESSAGE_sv          210
#define IDS_BALLOON_MESSAGE_fi          211
#define IDS_BALLOON_MESSAGE_et          212
#define IDS_BALLOON_MESSAGE_lv          213
#define IDS_BALLOON_MESSAGE_lt          214
#define IDS_BALLOON_MESSAGE_pl          215
#define IDS_BALLOON_MESSAGE_cs          216
#define IDS_BALLOON_MESSAGE_sk          217
#define IDS_BALLOON_MESSAGE_hu          218
#define IDS_BALLOON_MESSAGE_ro          219
#define IDS_BALLOON_MESSAGE_ru          220
#define IDS_BALLOON_MESSAGE_uk          221
#define IDS_BALLOON_MESSAGE_kk          222
#define IDS_BALLOON_MESSAGE_tr          223
#define IDS_BALLOON_MESSAGE_el          224
#define IDS_BALLOON_MESSAGE_ar          225
#define IDS_BALLOON_MESSAGE_zh_CN       226
#define IDS_BALLOON_MESSAGE_ja          227
#define IDS_BALLOON_MESSAGE_zh_TW       228
#define	IDS_BALLOON_MESSAGE_ko			229


#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        130
#define _APS_NEXT_COMMAND_VALUE         32774
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
