const { series, src, dest, task } = require('gulp');
const del = require('del');
const os = require('os');
const fs = require('fs');
const vfs = require('vinyl-fs');
const util = require('util');
const { exec, execSync } = require('child_process');
const prExec = util.promisify(require('child_process').exec);
const minimist = require('minimist');
const rename = require('gulp-rename');
const xmlpoke = require('xmlpoke');
const packager = require('electron-packager');
const zip = require('gulp-zip');
const readlineSync = require('readline-sync');
const minimatch = require('minimatch')
const dformat = require('date-format')
const axios = require('axios');
//const path = require('path'); //*&*&*&V1_20201104 to solve create path recursively

const g_pkgBuildFolder = './setup/osx/build/pkg'
// Once the user is asked for PFX file path and its password, the info is
// is  stored here. Subsequent calls to retrieve the same will  only prompt
// the user if found to be empty.
let g_signPfxFile = './kcpg.cer'; //&*&*&*G1_MOD,  '';
let g_signPassword = '';
let g_signTimeStampServer = 'http://timestamp.digicert.com'; //&*&*&*G1_MOD, 'http://timestamp.comodoca.com/rfc3161';

// If your Windows certificate is cross-signed, specifiy its root certificate
// filename here. Only for Windows driver.
// With driver being moved to ./bin folder, contents of which are not touched
// by the build script, this is somewhat redundant.
//&*&*&*G1_MOD
//let g_signRootCertificate = 'addtrustexternalcaroot_kmod.crt'
let g_signRootCertificate = 'DigiCert High Assurance EV Root CA.crt'
//&*&*&*G2_MOD
// This is the Windows binary signing certificate Common Name.
// Replace this with Kensington's certificate Common Name.
let g_signCN = 'Kensington Computer Products Group, a division of ACCO Brands'       // 'Smallpearl LLC'

// This is the signing identiTy, issued by Apple, installed in KeyChain Access.
// Replace this with Kensington's Apple issued signing identity.
const MAC_PRODUCT_SIGN_IDENTITY = 'Developer ID Installer: Kensington Computer Products Group (293UQF7R4S)';
const MAC_APP_SIGN_IDENTITY = 'Developer ID Application: Kensington Computer Products Group (293UQF7R4S)';
const MAC_DEVELOPER_ACCOUNT_ID = 'vannes.yang@kensington.com';
const MAC_DEVELOPER_ALTOOL_PASSWORD = process.env.ALTOOL_PASSWORD;
const NOTARIZATION_CHECK_WAIT_INTERVAL = 10; // In seconds
const CODE_SIGN_IDENTITY='30BE8DC925C6C8744378B645995F308911C90E35'

// Make this a variable so that we can easily change it later.
const MAC_GUI_BUILD_OUTPUT_NAME = 'KensingtonWorks';
/**
 * This is the target app name that will be created under /Applications.
 * So changing this to 'KensingtonWorks ' will create
 * /Applications/KensingtonWorks .app, giving people the impression that
 * they are using KensingtonWorks, where in effect they will be using
 * KensingtonWorks .app.
 *
 * Since changing this variable affects the target location where the
 * app is installed, the following files also need to be updated
 * accordingly:
 *
 *  - Launcher Agent script - for auto starting Agent
 *      ./tbwhelper/osx/KensingtonWorksAgent/com.kensington.trackballworks.plist
 *  - Preference Pane - for launching GUI from status icon menu
 *      ./prefpane/TrackballWorks/TrackballWorks.m
 *  - Uninstaller - For removing all the packages correctly
 *      ./uninstaller/uninstaller/AppDelegate.m
 *  - Installer package script - For marking the app to be upgraded
 *      ./setup/osx/src/trackballworks2.plist
 *  - Post install script - For registering the agent with Accessibility
 *      ./setup/osx/src/Scripts/postinstall
 */
const MAC_GUI_TARGET_NAME = 'KensingtonWorks ';

// Update server path
//&*&*&*G1_MOD
//const UPDATE_SERVER_PATH = 'https://www.smallpearl.com/media/packages/KensingtonWorks/'
const UPDATE_SERVER_PATH = 'https://accoblobstorageus.blob.core.windows.net/software/version/KensingtonWorks2/'
//&*&*&*G2_MOD
// list of the languages, and their LCID code that we'll be generating
// transforms of. en-US is considered the base language and rest of the locales'
// transforms will be generated from this.
const LOCALIZATION_LANGS = [
    { tag: 'ar-SA', code: 1025 },
    { tag: 'cs-CZ', code: 1029 },
    { tag: 'da-DK', code: 1030 },
    { tag: 'de-DE', code: 1031 },
    { tag: 'el-GR', code: 1032 },
    { tag: 'es-ES', code: 1034 },
    { tag: 'et-EE', code: 1061 },
    { tag: 'fi-FI', code: 1035 },
    { tag: 'fr-FR', code: 1036 },
    { tag: 'hu-HU', code: 1038 },
    { tag: 'it-IT', code: 1040 },
    { tag: 'ja-JP', code: 1041 },
    { tag: 'ko-KR', code: 1042 },
    { tag: 'lt-LT', code: 1063 },
    { tag: 'lv-LV', code: 1062 },
    { tag: 'nb-NO', code: 1044 },
    { tag: 'nl-NL', code: 1043 },
    { tag: 'pl-PL', code: 1045 },
    { tag: 'pt-PT', code: 2070 },
    { tag: 'ro-RO', code: 1048 },
    { tag: 'ru-RU', code: 1049 },
    { tag: 'sk-SK', code: 1051 },
    { tag: 'sv-SE', code: 1053 },
    { tag: 'tr-TR', code: 1055 },
    { tag: 'uk-UA', code: 1058 },
    { tag: 'zh-CN', code: 2052 },
    { tag: 'zh-TW', code: 1028 }
];

// function suspend execution for 'n' milliseconds
function msleep(n) {
    Atomics.wait(new Int32Array(new SharedArrayBuffer(4)), 0, 0, n);
}

// function suspend execution for 'n' seconds
function sleep(n) {
    msleep(n * 1000);
}

// function create path recursely
// function mkdirRecurse(inputPath) {
//     if (fs.existsSync(inputPath)) {
//       return;
//     }
//     const basePath = path.dirname(inputPath);
//     if (fs.existsSync(basePath)) {
//       fs.mkdirSync(inputPath);
//     }
//     mkdirRecurse(basePath);
// }

function getPfxNameAndPassword() {

    // PFX based signing is required only for Windows
    if (process.platform != 'win32') {
        return;
    }

    if (g_signPfxFile || g_signPassword)
        return;

    if (options.cert) {
        g_signPfxFile = options.cert;
    } else {
        g_signPfxFile = readlineSync.question('Full path to the PFX file: ');
    }

    if (options.certpwd) {
        g_signPassword = options.certpwd;
    } else {
        g_signPassword = readlineSync.question('PFX file password: ', {
            hideEchoBack: true
        });
    }
}

var knownOptions = {
    string: [
        'arch', 'configuration', 'releaseType', 'cert', 'certpwd',
        'guiEnv', 'notarizeUuid'
    ],
    boolean: [
        'sign', 'noIncrVersion', 'noSetup', 'doNotNotarize'
        ],
    default: {
        arch: process.platform == 'win32' ? 'x86' : 'x64',
        configuration: 'Release',
        releaseType: 'patch',
        sign: false,
        noIncrVersion: false,
        cert: '',
        certpwd: '',
        guiEnv: 'production',
        notarizeUuid: '',
        doNotNotarize: false
    }
};
var options = minimist(process.argv.slice(2), knownOptions);
console.log('Build options: ', options);

/**
 * Runs a process in the specified folder and calls the completion callback.
 *
 * @param {*} folder
 * @param {*} cmdLine
 * @param {*} cb
 */
function execTaskInFolder(folder, cmdLine, cb) {
    var cwd = process.cwd();
    try {
        process.chdir(folder);
        exec(cmdLine,
            { maxBuffer: 1000*1024 },
            (err, stdout, stderr) => {
            console.log(stdout);
            console.log(stderr);
            cb(err);
        });
    } finally {
        process.chdir(cwd);
    }
}

/**
 * Clean up the distribution folder
 */
function cleanDist(cb) {
    del.sync('dist/');
    cb();
}

/**
 * Clean output folders of all components
 *
 * @param {*} cb
 */
function cleanAll(cb) {

    let paths = [
        './gui/dist',
        './build'
    ];
    if (process.platform === 'win32') {
        paths = paths.concat([
            './driver/windows/x64',
            './driver/windows/x86',
            './kwdwnldr/x64',
            './kwdwnldr/x86',
            './tbwhelper/windows/x86',
            './remlegacy/windows/x86',
            './setup/windows/obj',
            './setup/windows/bin',
        ]);
    } else {
        paths = paths.concat([
            './driver/osx/build',
            './driver/osx/DerivedData',
            //'./driver/osx-driverkit/build', //V_20210106 暫停清除DriverKit
            './driver/osx-driverkit/DerivedData',
            './prefpane/build',
            './prefpane/DerivedData',
            './tbwhelper/osx/build',
            './tbwhelper/osx/DerivedData',
            './uninstaller/build',
            './uninstaller/DerivedData',
            './setup/osx/pkgKext/content/Library/Extensions/trackballworks2.kext'
        ]);
    }
    del.sync(paths, {force: true});
    cb();
}
exports.cleanAll = cleanAll;

/*
 * Get the next version for the specified module for the current platform
 * from version.json.
 *
 * Parameters:
 *  module:
 *      {"product"|"driver"}
 *  releaseType:
 *      {'major'|'minor'|'patch'}
 *
 * Returns:
 *  An array of three version numbers:
 *      [MAJOR, MINOR, PATCH, BETA]
 *
 *  MAJOR, MINOR & PATCH are numbers. BETA is a boolean.
 *
 * Requires:
 *  File version.json in current folder, with the structure:-
 *      {
 *          "osx": {
 *              "<module>": "n.n.n",
 *          },
 *           "windows": {
 *              "<module>": "n.n.n",
 *          }
 *      }
 *  where <module> is the module next version for which is requested
 *  and 'n.n.n' is its semantic version number.
 */
function getVersion(module, releaseType) {
    let component = 2;  // default to patch
    if (releaseType == 'major') {
        component = 0;
    } else if (releaseType == 'minor') {
        component = 1;
    } else if (releaseType == 'patch') {
        component = 2;
    } else {
        throw Error("Invalid releaseType. Supported values: 'major', 'minor' or 'patch'.");
    }

    let versionFile = '';
    if (module == 'tau') {
        versionFile = 'version_tau.json';
    } else {
        if (os.platform() == 'darwin') {
            versionFile = 'version_osx.json';
        } else {
            versionFile = 'version_win.json';
        }
    }
    let rawVersion = fs.readFileSync(versionFile);
    let platformVersion = JSON.parse(rawVersion.toString());
    let moduleVersion = platformVersion[module];

    let versionComps = moduleVersion.split(".");
    for (let index = 0; index < versionComps.length; index++) {
        versionComps[index] = parseInt(versionComps[index]);
    }
    const retVal = [...versionComps];
    retVal.push(platformVersion['beta'] ? true : false);
    return retVal;
}

/*
 * Increment the version number for the specified module.
 *
 * Parameters:
 *  module:
 *      {"product"|"driver"}
 *  releaseType:
 *      {'major'|'minor'|'patch'}
 *
 * Returns:
 *  None
 *
 * Notes:
 *  For 'major' and 'minor' releaseType, version numbers in the lower
 *  version quadrant are set to 0.
 */
function incrVersion(module, releaseType) {
    let component = 2;  // default to patch
    if (releaseType == 'major') {
        component = 0;
    } else if (releaseType == 'minor') {
        component = 1;
    } else if (releaseType == 'patch') {
        component = 2;
    } else {
        throw Error("Invalid releaseType. Supported values: 'major', 'minor' or 'patch'.");
    }

    let versionFile = '';
    if (module == 'tau') {
        versionFile = 'version_tau.json';
    } else {
        if (os.platform() == 'darwin') {
            versionFile = 'version_osx.json';
        } else {
            versionFile = 'version_win.json';
        }
    }

    let rawVersion = fs.readFileSync(versionFile);
    let platformVersion = JSON.parse(rawVersion.toString());
    let moduleVersion = platformVersion[module];

    let versionComps = moduleVersion.split(".");
    for (let index = 0; index < versionComps.length; index++) {
        versionComps[index] = parseInt(versionComps[index]);
    }

    versionComps[component]++
    if (component == 0) {
        versionComps[1] = 0;
        versionComps[2] = 0;
    } else if (component == 1) {
        versionComps[2] = 0;
    }
    platformVersion[module] = `${versionComps[0]}.${versionComps[1]}.${versionComps[2]}`
    let jsonVersion = JSON.stringify(platformVersion, null, 2);
    fs.writeFileSync(versionFile, jsonVersion);
}

/*
 * Writes the header file version.h in path with the following entries:-
 *
 *  #define _FILEVERSION            n.n.n
 *  #define _FILEVERSION_STR        "n, n, n\0"
 *  #define _PRODUCTVERSION         n.n.n
 *  #define _PRODUCTVERSION_STR     "n, n, n\0"
 *
 * Parmeters:
 *  path - full path of the file to create.
 *  fileVersion - array of three numbers
 *  productVersion - array of three numbers
 *
 */
function writeVersionHeader(path, fv, pv) {
    if (fv.length < 3 || pv.length < 3) {
        throw Error("Provide version as an array of three elements");
    }

    let vFileTemplate = `
    #define VERSION_MAJOR           ${fv[0]}
    #define VERSION_MINOR           ${fv[1]}
    #define VERSION_PATCH           ${fv[2]}
    #define _FILEVERSION            ${fv[0]},${fv[1]},${fv[2]},0
    #define _FILEVERSION_STR        "${fv[0]}.${fv[1]}.${fv[2]}.0"
    #define _PRODUCTVERSION         ${pv[0]},${pv[1]},${pv[2]},0
    #define _PRODUCTVERSION_STR     "${pv[0]}.${pv[1]}.${pv[2]}.0"
    `;

    fs.writeFileSync(path, vFileTemplate, 'utf8');
}

// change setting  of target plist  file
function updatePlistBundleVersion(plistFilePath, version) {
    if (version.length < 3) {
        throw Error("Provide version as an array of three elements");
    }
    var plist = require('plist');
    var infoObj = plist.parse(fs.readFileSync(plistFilePath, 'utf8'));
    // @ts-ignore
    infoObj.CFBundleShortVersionString =
    // @ts-ignore
    infoObj.CFBundleVersion = `${version[0]}.${version[1]}.${version[2]}`;
    fs.writeFileSync(plistFilePath, plist.build(infoObj));
}

/**
 * Bump driver build version.
 *
 * @param {function} cb Gulp task completion callback function.
 */
function bumpDriverVersion(cb) {
    incrVersion('driver', options.releaseType);
    cb();
}

/**
 * Task to build the driver, which invokes the appropriate platform
 * specific function.
 */
function buildCoreDriver(cb) {
    let fv = getVersion("driver", options.releaseType);
    let pv = getVersion("driver", options.releaseType);
    if (os.platform() == 'darwin') {
        /**
         * Building driver on MacOSX requires 10.12 SDK to be installed under the current
         * Xcode MacOS SDKs folder. For Xcode 10.1 this is:
         * /Applications/Xcode10.1.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/
         *
         * By default Xcode 10.1 installs 10.14 SDK here. To get 10.12 SDK, we have to manually
         * copy it over from an older Xcode such as Xcode 8.2.1. That is install Xcode
         * 8.2.1 under /Applications by renaming it to Xcode8.2.1.app and then either copy
         * over the SDK folder to the Xcode10.1.app platform SDK folder shown above or
         * make a symbolic link.
         *
         * If all went well, if you issue xcodebuild -showsdks, you should get an output
         * like below (listing pruned to remove irrelevant iOS & watchOS SDKs):
         * $ xcodebuild -showsdks

                macOS SDKs:
                    macOS 10.12                   	-sdk macosx10.12
                    macOS 10.14                   	-sdk macosx10.14

         */
        console.log('Building driver for OSX. Version: ', `${fv[0]}.${fv[1]}.${fv[2]}`);
        updatePlistBundleVersion('./driver/osx/driver/Info.plist', fv);
        execSync(
            'xcodebuild -sdk macosx10.12 -alltargets -configuration Release build',
            {cwd: './driver/osx'});
    } else if (os.platform() == 'win32') {
        writeVersionHeader("driver/windows/version.h", fv, pv);
        process.env.STAMPINF_VERSION = `${fv[0]}.${fv[1]}.${fv[2]}`;
        ['x64', 'x86'].forEach(arch => {
            console.log('Building Windows driver, version:', process.env.STAMPINF_VERSION, 'for', arch);
            execSync(
                `msbuild tbwkern.vcxproj /t:Rebuild /p:Configuration=${options.configuration} /p:Platform=${arch} /p:STAMPINF_VERSION=${process.env.STAMPINF_VERSION}`,
                { cwd: './driver/windows' }
            );
        });
    }
    cb();
}

/**
 * Builds the download service
 * @param {*} cb
 */
function buildDownloadService(cb) {
    let fv = getVersion("driver", options.releaseType);
    let pv = getVersion("driver", options.releaseType);
    if (os.platform() == 'win32') {
        writeVersionHeader("kwdwnldr/version.h", fv, pv);
        const VERSION = `${fv[0]}.${fv[1]}.${fv[2]}`;
        ['x64', 'x86'].forEach(arch => {
            console.log('Building Download service, version:', VERSION, 'for', arch);
            execSync(
                `msbuild kwdwnldr.vcxproj /t:Rebuild /p:Configuration=${options.configuration} /p:Platform=${arch}`,
                { cwd: './kwdwnldr' }
            );
        });
    }
    cb();
}

/**
 * Builds the helper.
 *
 * @param {*} cb function Gulp completion callback
 */
function buildHelper(cb) {
    let pv = getVersion("product", options.releaseType);
    if (os.platform() == 'darwin') {
        console.log('Building helper for OSX. Version: ', `${pv[0]}.${pv[1]}.${pv[2]}`);
        writeVersionHeader("tbwhelper/osx/KensingtonWorksAgent/version.h", pv, pv);
        updatePlistBundleVersion('./tbwhelper/osx/KensingtonWorksAgent/Info.plist', pv);
        execTaskInFolder(
            './tbwhelper/osx',
            'xcodebuild -alltargets -configuration Release build',
            cb);
    } else if (os.platform() == 'win32') {
        writeVersionHeader("tbwhelper/windows/version.h", pv, pv);
        console.log('Building Windows helper, version: ', `${pv[0]}.${pv[1]}.${pv[2]}`);
        return execTaskInFolder(
            './tbwhelper/windows',
            `msbuild tbwhelper.vcxproj /t:Rebuild /p:Configuration=${options.configuration} /p:Platform=${options.arch}`,
            cb
        );
    }
}



/**
 * Signs the helper.
 *
 * @param {*} cb function Gulp completion callback
 */
function signtbwDKManager(cb) {
    codeSingtbwDKManager();
    cb();
}

/**
 * Signs the helper.
 *
 * @param {*} cb function Gulp completion callback
 */
function signtbwDKManager11(cb) {
    codeSingtbwDKManager11();
    cb();
}

/**
 * Builds the tbwDKManager.
 *
 * @param {*} cb function Gulp completion callback
 */
function buildtbwDKManager(cb) {
    let pv = getVersion("product", options.releaseType); // *&*&*&Va_20200922
    let fv = getVersion("driverkit", options.releaseType);
    if (os.platform() === 'darwin') {
        console.log('Building the tbwDKManager(osx-driverkit)...');
        // Give it versions...
        updatePlistBundleVersion('./driver/osx-driverkit/tbwDKManager/Info.plist', pv);
        updatePlistBundleVersion('./driver/osx-driverkit/tbwDKDriver/Info.plist', fv);
        execTaskInFolder(
            './driver/osx-driverkit',
            'xcodebuild -configuration Release build',
            cb);
    }
}

/**
 * Builds the tbwDKManager for Big Sur.
 *
 * @param {*} cb function Gulp completion callback
 */
function buildtbwDKManager11(cb) {
    let pv = getVersion("product", options.releaseType); // *&*&*&Va_20200922
    let fv = getVersion("driverkit", options.releaseType);
    if (os.platform() === 'darwin') {
        console.log('Building the tbwDKManager(osx-driverkit) for Big Sur...');
        // Give it versions...
        updatePlistBundleVersion('./driver/osx-driverkit-11/tbwDKManager/Info.plist', pv);
        updatePlistBundleVersion('./driver/osx-driverkit-11/tbwDKDriver/Info.plist', fv);
        execTaskInFolder(
            './driver/osx-driverkit-11',
            '/Applications/Xcode12.3.app/Contents/Developer/usr/bin/xcodebuild -configuration Release build',
            cb);
    }
}

/**
 * Signs the Helper and tbwDKManager.
 *
 * @param {*} cb function Gulp completion callback
 */
function signHelper(cb) {
    // let pv = getVersion("product", options.releaseType);
    if (os.platform() === 'darwin') {
        codeSingHelper();
        //codeSingtbwDKManager(); // V_20210106 暫時停止DriverKit自動sign
    }
    cb();
}

/**
 * Build OSX uninstaller
 * @param {*} cb Gulp task completion callback
 */
function buildOSXUninstaller(cb) {
    if (os.platform() === 'darwin') {
        let pv = getVersion("product", options.releaseType);
        console.log('Building uninstaller for OSX. Version: ', `${pv[0]}.${pv[1]}.${pv[2]}`);
        updatePlistBundleVersion('./uninstaller/uninstaller/Info.plist', pv);
        execTaskInFolder(
            './uninstaller',
            'xcodebuild -alltargets -configuration Release build',
            cb);
    } else if (os.platform() === 'win32') {
        cb();
    }
}

 /**
 * electron-packager ignore callback function, that ignore ZURB foundation's
 * non-dist folders from being included in the final package.
 */
function reduceDependentPackageFootprint(/** @type {string} */filePath)
{
    let ignoreGlobs = [
        "**/*.js.map",
        "**/*.d.ts",
        "**/.vscode/*",
        "**/node_modules/*/{CHANGELOG.md,README.md,README,readme.md,readme}",
        "**/node_modules/*/{test,__tests__,tests,powered-test,example,examples}",
        "**/node_modules/*.d.ts",
        "**/node_modules/.bin",
        "**/*.{iml,o,hprof,orig,pyc,pyo,rbc,swp,csproj,sln,xproj}",
        ".editorconfig",
        "**/._*",
        "**/{.DS_Store,.git,.hg,.svn,CVS,RCS,SCCS,.gitignore,.gitattributes}",
        "**/{__pycache__,thumbs.db,.flowconfig,.idea,.vs,.nyc_output}",
        "**/{appveyor.yml,.travis.yml,circle.yml}",
        "**/{npm-debug.log,yarn.lock,.yarn-integrity,.yarn-metadata.json}",
        "**/node_modules/foundation-sites/_vendor/*",
        "**/node_modules/foundation-sites/assets/*",
        "**/node_modules/foundation-sites/customer/*",
        "**/node_modules/foundation-sites/docs/*",
        "**/node_modules/foundation-sites/gulp/*",
        "**/node_modules/foundation-sites/js/*",
        "**/node_modules/foundation-sites/script/*",
        "**/node_modules/foundation-sites/scss/*",
        "**/node_modules/foundation-sites/test/*",
        "/dist/*",
        "/src/*",
        "/build/*",
        "**/*.ps1" // *&*&*&Va_20201109 
    ];
    for (let index = 0; index < ignoreGlobs.length; index++) {
        const element = ignoreGlobs[index];
        if (minimatch(filePath, element)) {
            return true;
        }
    }
    return false;
}

/**
 * Build the GUI using electron-packager API
 * @param {Function} cb Gulp task completion callback.
 */
function buildOldGui(cb) {
    let options = {
        dir: ".",
        name: "TrackballWorks",
        asar: true,
        prune: true,
        overwrite: true,
        out: `./build/${os.platform()}`,
        ignore: reduceDependentPackageFootprint
    };

    if (os.platform() === 'darwin') {
        options["app-bundle-id"] = "com.kensington.kensingtonworks2.app";
        options["icon"] = "./TrackballWorks.icns";
        options["arch"] = "x64";
    } else {
        options["icon"] = "TrackballWorks.ico";
        options["version-string.CompanyName"] = "CE";
        options["version-string.FileDescription"] = "TrackballWorks Configuration Utility";
    }

    var cwd = process.cwd();
    process.chdir("gui");
    packager(options)
        .then(appPaths => {
            console.log("GUI built using electron-packager API!");
            cb();
        }).finally(() => {
            process.chdir(cwd);
        });
}

/**
 * Builds the remlegacy.
 *
 * @param {*} cb gulp task callbak
 */
function buildRemLegacy(cb) {
    if (os.platform() == 'darwin') {
        console.log('Building remlegacy for OSX..');
        //throw Error("TODO");
        cb();
    } else if (os.platform() == 'win32') {
        console.log('Building Windows remlegacy...');
        return execTaskInFolder(
            './remlegacy/windows',
            `msbuild remlegacy.vcxproj /t:Rebuild /p:Configuration=${options.configuration} /p:Platform=${options.arch}`,
            cb
        );
    }
}

/**
 * Build the preferences pane app that launches the TrackballWorks app.
 *
 * @param {*} cb Gulp task completion callback
 */
function buildPrefPane(cb) {
    if (os.platform() == 'darwin') {
        execTaskInFolder(
            './prefpane',
            'xcodebuild -alltargets -configuration Release build',
            cb);
    } else if (os.platform() == 'win32') {
        cb();
    }
}

/**
 * Copies the setup scripts & all build outputs from individual components into
 * the relevant folder under ./dist/osx/build so that the OSX package build
 * can be run on that folder.
 *
 * Returns a promise which when resolved, all files have been copied to their
 * respective destinations.
 */
function setupOSXCopyFiles() {
    let targetPathBase = './setup/osx';
    let buildPath = targetPathBase + '/build';
    let contentsPath = buildPath + '/content';
    let setupSrcPath = './setup';

    // clean up previous build outputs
    // console.log('Cleaning up ./dist/osx folder..');
    del.sync(`${buildPath}/**`);
    fs.mkdirSync(buildPath);
    console.log('Copying component build outputs from their ./build folders..');
    return Promise.all([
        new Promise((resolve, reject) => {
            // copy files from the package/osx to ./dist/osx
            src([setupSrcPath+'/osx/src/**/*', '!.DS_Store'])
            .on('error', reject)
            .pipe(dest(buildPath))
            .on('end', resolve)
        }),
        new Promise((resolve, reject) => {
            console.log('Copying GUI..');
            vfs.src(
                [`./gui/dist/x64/${MAC_GUI_BUILD_OUTPUT_NAME}-darwin-x64/${MAC_GUI_BUILD_OUTPUT_NAME}.app/**`],
                {resolveSymlinks: false})
            .on('error', reject)
            .pipe(vfs.dest(contentsPath+`/Applications/${MAC_GUI_TARGET_NAME}.app`)) // Here the App is renamed
            .on('end', resolve);
        }),
        new Promise((resolve, reject) => { // *&*&*&Va_20200908 copy helper to '/Library/Application Support/'
            console.log('Copying helper...');
            src(['./tbwhelper/osx/build/Release/KensingtonWorksHelper.app/**'])
            .on('error', reject)
            .pipe(dest(contentsPath+'/Library/Application Support/Kensington/KensingtonWorks2/KensingtonWorksHelper.app'))
            .on('end', resolve);
        }),
        // new Promise((resolve, reject) => { // *&*&*&V1_MOD_20200220, not need anymore
        //     // copy daemon
        //     console.log('Copying launcher agent..');
        //     src(['./tbwhelper/osx/build/Release/KensingtonWorksHelper.app/**/*'])
        //     .on('error', reject)
        //     .pipe(dest(contentsPath+`/Applications/${MAC_GUI_TARGET_NAME}.app/Contents/Helper/KensingtonWorksHelper.app`))
        //     .on('end', resolve);
        // }),
        new Promise((resolve, reject) => {
            // copy daemon launch agent plist
            console.log('Copying launchctl script..');
            src(['./tbwhelper/osx/com.kensington.trackballworks.plist'])
            .on('error', reject)
            .pipe(dest(contentsPath+'/Library/LaunchAgents'))
            .on('end', resolve);
        }),
        //new Promise((resolve, reject) => { // *&*&*&Va_20200909 to support dual driver, we can't install kext directly, package to another .pkg instead
        //    // copy driver
        //    console.log('Copying driver..');
        //    src(['./bin/osx/driver/trackballworks2.kext/**'])
        //    .on('error', reject)
        //    .pipe(dest(contentsPath+'/Library/Extensions/trackballworks2.kext'))
        //    .on('end', resolve);
        //}),
        new Promise((resolve, reject) => { // *&*&*&V_20200908 for driverkit
            // copy driver-kit
            console.log('Copying driverkit..');
            src(['./bin/osx/driverkit/KensingtonWorks.app/**']) // vv20210108
            .on('error', reject)
            .pipe(dest(contentsPath+'/Applications/.KensingtonWorks.app')) // *&*&*&V1_20201120 make it hidden and changed name because user will see it when installing 
            .on('end', resolve);
        }),
        new Promise((resolve, reject) => { // *&*&*&V1_20210108 for driverkit11
            // copy driver-kit for macOS11
            console.log('Copying driverkit for macOS11..');            
            src(['./bin/osx/driverkit11/KensingtonWorks11.app/**']) // vv20210108
            .on('error', reject)
            .pipe(dest(contentsPath+'/Applications/.KensingtonWorks-for-BigSur-above.app'))
            .on('end', resolve);
        }),
        new Promise((resolve, reject) => {
            // copy preferences pane app
            // console.log('Copying preference pane..');
            src(['./prefpane/build/Release/KensingtonWorks.prefPane/**'])
            .on('error', reject)
            .pipe(dest(contentsPath+'/Library/PreferencePanes/KensingtonWorks.prefPane'))
            .on('end', resolve);
        }),
        new Promise((resolve, reject) => {
            // console.log('Copying Uninstaller..');
            src(['./uninstaller/build/Release/KensingtonWorks Uninstaller.app/**/*'])
            .on('error', reject)
            .pipe(dest(contentsPath+'/Applications/Utilities/KensingtonWorks Uninstaller.app'))
            .on('end', resolve);
        })
    ]);
}

/**
 * Builds the intermediate component pkg file that is to be supplied to
 * productbuild to generate the final distributable .pkg file.
 */
async function setupOSXBuildPackage() {
    console.log('Building OSX intermediate package...');
    let origDir = process.cwd();
    let set_up_path = './setup/osx/build'
    try {
        process.chdir(set_up_path);

        //權限會影響安裝
        let  chmod_cmd = 'chmod 755 ./Scripts/*';
        console.log(`[CMD_9876] ${chmod_cmd}`)
        const { chmod_out, chmod_err } = await prExec(
            chmod_cmd
        );
        console.log(chmod_out);
        console.log(chmod_err);

        const { stdout, stderr } = await prExec(
            'pkgbuild --root ./content --component-plist trackballworks2.plist --identifier com.kensington.trackballworks2.installer --scripts ./Scripts kensingtonworks.pkg'
        );
        console.log(stdout);
        console.log(stderr);

    } finally {
        process.chdir(origDir);
        console.log('OSX intermediate package built!');
    }

    // 移動到放pkg的folder
    //let mkdir =  `mkdir ${g_pkgBuildFolder}`; // *&*&*&_Va_20200924
    //await prExec(mkdir);

    //let cp_pkg  = `mv ${set_up_path}/kensingtonworks.pkg ${g_pkgBuildFolder}`;
    //console.log(`[CMD_9090] ${cp_pkg}`);
    //const {cp_out, cp_error} = await prExec(cp_pkg);
    //console.log(cp_out);
    //console.log(cp_error);
}

/**
 * Builds the final TrackballWorks installer pkg file -
 * TrackballWorksInstaller.pkg.
 * Output placed in ./dist folder.
 */
async function setupOSXBuildDistributable() {
    let pv = getVersion("product", options.releaseType);
    console.log('Building OSX distributable. Version: ', `${pv[0]}.${pv[1]}.${pv[2]}`);
    // let origDir = process.cwd();
    try {
        let target_folder = './setup/osx/build';
        // process.chdir();
        // see: setup/osx/src/prodBuild.sh

        let product_build_pkg = `productbuild --distribution ${target_folder}/distribution1.xml \
                                --package-path   ${target_folder}    \
	                            --resources ${target_folder}/Resources  \
                                ${target_folder}/KensingtonWorks_${pv[0]}.${pv[1]}.${pv[2]}.pkg`
        
        console.log(`[CMD_9091]  ${product_build_pkg}`)
        const { stdout, stderr } = await prExec(product_build_pkg)
        console.log(stdout);
        console.log(stderr);
    } finally {
        // process.chdir(origDir);
        console.log('OSX distributable built!');
    }
}

async function writeVersionToOSXDistributionXml() {
    let pv = getVersion("product", options.releaseType);
    xmlpoke('./dist/osx/build/Distribution.xml', function(xml) {
        xml.set('installer-gui-script/title', `KensingtonWorks ${pv[0]}.${pv[1]}.${pv[2]}`);
        xml.setOrAdd('installer-gui-script/pkg-ref[@id="com.kensington.trackballworks2.installer"]/@version', `${pv[0]}.${pv[1]}.${pv[2]}` );
    });
}
// *&*&*&V1_20201104 test build kext package only
exports.buildKext = buildPkgExt_test;
function buildPkgExt_test(cb){
    // generate folder first
    if (!fs.existsSync('./setup/osx/pkgKext/content')) {fs.mkdirSync('./setup/osx/pkgKext/content'); }
    if (!fs.existsSync('./setup/osx/pkgKext/content/Library')) { fs.mkdirSync('./setup/osx/pkgKext/content/Library'); }
    if (!fs.existsSync('./setup/osx/pkgKext/content/Library/Extensions')) { fs.mkdirSync('./setup/osx/pkgKext/content/Library/Extensions'); }

    let rm_kext = `rm -rf ./setup/osx/pkgKext/content/Library/Extensions/trackballworks2.kext`
    console.log(`[CMD_7800] ${rm_kext}`)
    execSync(rm_kext, {stdio: 'inherit'});
    let copy_lib  = `cp -R ./bin/osx/driver/trackballworks2.kext ./setup/osx/pkgKext/content/Library/Extensions/trackballworks2.kext`
    console.log(`[CMD_7801] ${copy_lib}`)
    execSync(copy_lib, {stdio: 'inherit'});
    // project  root
    let cwd = process.cwd();
    let path = './setup/osx/pkgKext';
    console.log(`[CMD_12099] call tbw2Build.sh at ${path}; current folder at ${cwd}`)

    let chmodcmd = `chmod 755 ./tbw2Build.sh` // *&*&*&_V1_20200924
    execSync(chmodcmd, {cwd: path});
    execSync('./tbw2Build.sh', {cwd: path})

    cb();
}

function buildOSXSetup(cb) {
    //buildPkgExt().catch( err =>{ cb(err)});
    setupOSXCopyFiles()
        .then(() => {return buildPkgExt();})
        .then(() => {return changePermission();})
        .then(() => {return writeVersionToOSXDistributionXml();})
        .then(() => {return setupOSXBuildPackage();})
        .then(() => {return setupOSXBuildDistributable();
    }).then(() => {
        console.log('Done!');
        cb();
    }).catch(err => {
        cb(err);
    });

    async function changePermission() {
        // plist require 644 permission mode, when launching command `launchctl load`
        let targetPathBase = './setup/osx';
        let buildPath = targetPathBase + '/build';
        let contentsPath = buildPath + '/content';
        let target_cmd = `chmod 644 ${contentsPath}/Library/LaunchAgents/com.kensington.trackballworks.plist`
        console.log(`[CMD_780] ${target_cmd}`)
        execSync(target_cmd, {stdio: 'inherit'});
    }

    // trackballworks2 會被打包成pkg file
    async function buildPkgExt(){
        // generate folder first tbd
        // generate folder first
        if (!fs.existsSync('./setup/osx/pkgKext/content')) {fs.mkdirSync('./setup/osx/pkgKext/content'); }
        if (!fs.existsSync('./setup/osx/pkgKext/content/Library')) { fs.mkdirSync('./setup/osx/pkgKext/content/Library'); }
        if (!fs.existsSync('./setup/osx/pkgKext/content/Library/Extensions')) { fs.mkdirSync('./setup/osx/pkgKext/content/Library/Extensions'); }

        let rm_kext = `rm -rf ./setup/osx/pkgKext/content/Library/Extensions/trackballworks2.kext`
        console.log(`[CMD_7800] ${rm_kext}`)
        execSync(rm_kext, {stdio: 'inherit'});
        let copy_lib  = `cp -R ./bin/osx/driver/trackballworks2.kext ./setup/osx/pkgKext/content/Library/Extensions/trackballworks2.kext`
        console.log(`[CMD_7801] ${copy_lib}`)
        execSync(copy_lib, {stdio: 'inherit'});
        // project  root
        let cwd = process.cwd();
        let path = './setup/osx/pkgKext';
        console.log(`[CMD_12099] call tbw2Build.sh at ${path}; current folder at ${cwd}`)

        let chmodcmd = `chmod 755 ./tbw2Build.sh` // *&*&*&_VA_20200924
        execSync(chmodcmd, {cwd: path});
        execSync('./tbw2Build.sh', {cwd: path})
    }
}

/**
 * Sign a file using the PFX file & password values stored in global variable
 * g_sign*. These variables should've been initialized appropriately by a call
 * to getPfxNameAndPassword().
 *
 * @param {*} file string the file to sign
 */
function signFile(file, rootCertificateFile, appendSignature) {
    if (os.platform() == "win32") {
        try {
            let cmdline = 'signtool.exe sign /v '; // requires trailing blank
            if (rootCertificateFile) {
                cmdline +=  `/ac ${rootCertificateFile} `; // requires trailing blank
            }
            // if (appendSignature) {
            //     cmdline += '/as '; // requires trailing blank
            // }
            //&*&*&*G1_MOD
            //cmdline +=  `/f ${g_signPfxFile} /p ${g_signPassword} /n "${g_signCN}" /tr ${g_signTimeStampServer} /fd sha256 /td sha256 ${file}`;
            cmdline += `/f ${g_signPfxFile}  /tr ${g_signTimeStampServer} /td sha256 /fd sha256 ${file}`;
            //&*&*&*G2_MOD
            //cmdline +=  `/f ${g_signPfxFile} /p ${g_signPassword} /n "${g_signCN}" /tr ${g_signTimeStampServer} ${file}`;
            console.log("Sign command line:", cmdline);
            execSync(cmdline, {
                stdio: 'inherit'
            });
        } finally {
        }
    } else {
        let cmdline = `codesign -s "${MAC_APP_SIGN_IDENTITY}" --options=runtime --force  ${file}`;
        console.log('Sign command line:'+ cmdline);
        execSync(cmdline, {stdio: 'inherit'});
    }
}

/**
 * Sign the driver files for the Windows platform.
 *
 * @param {function} cb Gulp task completion callback.
 */
function signDriversWindows(cb) {
    if (!options.sign) {
        console.log('Not signing binaries as option --sign not specified.')
        cb();
        return;
    }
    if (os.platform() !== "win32") {
        cb();
    }

    let files = [];
    //&*&*&*G1_DEL, getPfxNameAndPassword();
    // windows files
    files = [];
    ['x64', 'x86'].forEach(arch => {
        [
            [`./driver/windows/${arch}/Release/tbwkern/tbwkern.sys`, g_signRootCertificate],
            [`./driver/windows/${arch}/Release/tbwkern/tbwkern.cat`, g_signRootCertificate],
            [`./driver/windows/${arch}/Release/tbwkern/tbwsvc.exe`, g_signRootCertificate]
        ].forEach((element) => {
            files.push(element);
        });
    });
    files.forEach((val, index, ar) => {
        signFile(val[0], val[1], true);
    })
    cb();
}

/** // vannes20200220
 * Copy the daemon and related files first before we sign binaries.
 * @param {function} cb Gulp task completion callback
 */
function genTheWholeMainApp(cb) {
    //  gulp pipe cause code sign signature erased.
    let target_dir =  `./gui/dist/x64/${MAC_GUI_BUILD_OUTPUT_NAME}-darwin-x64/${MAC_GUI_BUILD_OUTPUT_NAME}.app/Contents/Helper`
    let mkdir =  `mkdir  ${target_dir}`;
    execSync(mkdir, {stdio: 'inherit'});
    cb()
}

/**
 * Sign platform specific binaries, before building the package.
 * @param {function} cb Gulp task completion callback
 */
function signBinaries(cb) {

    // 選擇性的ＳＩＧＮ
    if (!options.sign) {
        console.log('Not signing binaries as option --sign not specified.')^
        cb();
        return;
    }

    //&*&*&*G1_DEL
    //getPfxNameAndPassword();
    //&*&*&*G2_DEL
    let files = [];
    if (os.platform() == "win32") {
        // windows files
        let guiPlatformExt = options.arch == 'x64' ? 'x64' : 'ia32';
        files = [];
        // Windows drivers are signed externally and post signing are stored
        // in ./bin/windows/driver. Setup grabs files from this folder.
        [
            [`./tbwhelper/windows/${options.arch}/Release/tbwhelper.exe`, null],
            [`./remlegacy/windows/${options.arch}/Release/remlegacy.exe`, null],
            [`./gui/dist/${options.arch}/KensingtonWorks2-win32-${guiPlatformExt}/KensingtonWorks2.exe`, null]
        ].forEach((element) => {
            files.push(element);
        })
    } else {
        // OSX. In OSX only the electron binaries need to be signed explicitly.
        // Helper is automatically signed by XCode as it's configured so.
        let file = `./gui/dist/x64/${MAC_GUI_BUILD_OUTPUT_NAME}-darwin-x64/${MAC_GUI_BUILD_OUTPUT_NAME}.app`;
        let cmdline = `electron-osx-sign ${file} --identity "${MAC_APP_SIGN_IDENTITY}" --platform=darwin --hardened-runtime --entitlements=./gui/KensingtonWorks.entitlements --entitlements-inherit=./gui/KensingtonWorks.entitlements --no-pre-auto-entitlements`;
        console.log("Sign command line:", cmdline);
        execSync(cmdline, {
            stdio: 'inherit'
        });
        // electron-osx-sign  會把electron底下相依的ＡＰＰ和ＦＲＡＭＥＷＯＲＫ用自己的方式ＳＩＧＮ過一次
        // 但是我們不得KensingtonWorksHelper.app有自己獨特的ＳＩＧＮ的方法所以要, 取代之
        //replaceKensingtonWorksHelper() // *&*&*&Va_20200917 這樣放會造成app sign 失敗
        // 對KensingtonWorks.app的根目錄做codeSign
        //codeSignTimeStamp4AppAndDextIfMac() // 不做了！在SIP ON時會crash!
    }
    files.forEach((val, index, ar) => {
        // SignFile Windows Only
        signFile(val[0], val[1], true); // Only executing when Windows build
    })

    cb();

    /**
     *   對app project 做 code sign timestamp
     */
    function codeSignTimeStamp4AppAndDextIfMac() {

        if (os.platform() === "darwin") {
            /**
             *
             * @type {string}
             */
            // let cp_p =  `cp tbwhelper/osx/tbw_DK_driver_profile-2.provisionprofile   gui/dist/x64/KensingtonWorks-darwin-x64/KensingtonWorks.app/Contents/Helper/KensingtonWorksHelper.app/Contents/Library/SystemExtensions/com.kensington.tbwDKDriver.dext/embedded.provisionprofile`;
            // execSync(cp_p, {stdio: 'inherit'});
            // codeSign(`gui/dist/x64/KensingtonWorks-darwin-x64/KensingtonWorks.app/Contents/Helper/KensingtonWorksHelper.app/Contents/Library/SystemExtensions/com.kensington.tbwDKDriver.dext`,
            //     `tbwhelper/osx/tbwDKDriver/tbwDKDriver.entitlements`);
            //
            // let cp_pp  = `cp tbwhelper/osx/KensingtonWorksHelper_using_DriverKit.provisionprofile  gui/dist/x64/KensingtonWorks-darwin-x64/KensingtonWorks.app/Contents/Helper/KensingtonWorksHelper.app/Contents/embedded.provisionprofile`;
            // execSync(cp_pp, {stdio: 'inherit'});
            // codeSign(`gui/dist/x64/KensingtonWorks-darwin-x64/KensingtonWorks.app/Contents/Helper/KensingtonWorksHelper.app`,
            //         `tbwhelper/osx/KensingtonWorksAgent/KensingtonWorksAgent.entitlements`)

            /*
             * sign打包過的ＡＰＰ
             */
            let noEntity = [
                // `gui/dist/x64/KensingtonWorks-darwin-x64/KensingtonWorks.app/Contents/Helper/KensingtonWorksHelper.app/Contents/Library/SystemExtensions/com.kensington.tbwDKDriver.dext`,
                // `gui/dist/x64/KensingtonWorks-darwin-x64/KensingtonWorks.app/Contents/Helper/KensingtonWorksHelper.app`,
                // "gui/dist/x64/KensingtonWorks-darwin-x64/KensingtonWorks.app/Contents/Frameworks/KensingtonWorks\\ Helper.app",
                // "gui/dist/x64/KensingtonWorks-darwin-x64/KensingtonWorks.app/Contents/Frameworks/Electron\\ Framework.framework",
                // "gui/dist/x64/KensingtonWorks-darwin-x64/KensingtonWorks.app/Contents/Frameworks/Squirrel.framework",
                // "gui/dist/x64/KensingtonWorks-darwin-x64/KensingtonWorks.app/Contents/Frameworks/ReactiveCocoa.framework",
                // "gui/dist/x64/KensingtonWorks-darwin-x64/KensingtonWorks.app/Contents/Frameworks/Mantle.framework",
                `gui/dist/x64/KensingtonWorks-darwin-x64/KensingtonWorks.app`,
            ]
            noEntity.forEach((value => {
                codeSign(value)
            }))
        }
        /**
         *
         * @param path string, root  of folder signed
         * @param entityFile string .entitlements file as  argument for codesign
         */
        function codeSign(path, entityFile=null) {
            let cmdline = `codesign -s ${CODE_SIGN_IDENTITY} `;

            if (entityFile !== null ) {
                cmdline  = cmdline + ` --entitlements ${entityFile} `
            }
            //--timestamp
            //cmdline = cmdline + `  --options=runtime --verbose --force  ${path}`;
            cmdline = cmdline + ` --options=runtime --force  ${path}`; // *&*&*&Va_20200916 test
            console.log('[codeSign] :'+ cmdline);
            execSync(cmdline, {stdio: 'inherit'});
        }
    }
}

function codeSingHelper() {
    let currentCwd = process.cwd();
    process.chdir("tbwhelper/osx");
    try {
        let  chmod = 'chmod 755 ./dkcodesign-rel.sh' // *&*&*&_V1_20200908
        execSync(chmod);
        let cmd = './dkcodesign-rel.sh'
        execSync(cmd);
        console.log('[codesign] tbwhelper had been codesign')
    } finally {
        process.chdir(currentCwd);
    }
}
// *&*&*&V1_20200908
function codeSingtbwDKManager() {
    let currentCwd = process.cwd();
    process.chdir("driver/osx-driverkit");
    try {
        let  chmod = 'chmod 755 ./dkcodesign-release.sh'
        execSync(chmod);
        let cmd = './dkcodesign-release.sh'
        execSync(cmd);
        console.log('[codesign] KensingtonWorks.app(DriverKit) had been codesign')
    } finally {
        process.chdir(currentCwd);
    }
}

// *&*&*&V1_20210120 DriverKit for Big Sur
function codeSingtbwDKManager11() {
    let currentCwd = process.cwd();
    process.chdir("driver/osx-driverkit-11");
    try {
        let  chmod = 'chmod 755 ./dkcodesign-release.sh'
        execSync(chmod);
        let cmd = './dkcodesign-release.sh'
        execSync(cmd);
        console.log('[codesign] KensingtonWorks.app(DriverKit for Big Sur) had been codesign')
    } finally {
        process.chdir(currentCwd);
    }
}

/*
        let cmdline = `electron-osx-sign`` ${file} --identity "${MAC_APP_SIGN_IDENTITY}"`;
        console.log("Sign command line:", cmdline);
        execSync(cmdline, {
            stdio: 'inherit'
        });
*/
/**
 * Sign the specified final distributable package.
 *
 * @param {function} cb Gulp callback handle
 */
function signPackage(cb) {
    if (options.noSetup) {
        console.log('Skipping signing package as setup has been disabled.')
        cb();
        return;
    }

    if (options.sign) {
        if (os.platform() == "win32") {
            //&*&*&*G1_DEL
            //getPfxNameAndPassword();
            //&*&*&*G2_DEL
            let pv = getVersion("product", options.releaseType);
            process.env.PRODUCT_VERSION = `${pv[0]}.${pv[1]}.${pv[2]}.0`;
            signFile(
                `./dist/windows/KensingtonWorks_${pv[0]}.${pv[1]}.${pv[2]}.msi`,
                null,
                false
                );
        } else {
            // TODO:
            let pv = getVersion("product", options.releaseType);
            execSync(`productsign --sign "${MAC_PRODUCT_SIGN_IDENTITY}" --timestamp ./dist/osx/KensingtonWorks_${pv[0]}.${pv[1]}.${pv[2]}_unsigned.pkg ./dist/osx/KensingtonWorks_${pv[0]}.${pv[1]}.${pv[2]}.pkg`);
        }
    }
    cb();
}

/**
 * Generate gui.wxs for the given bit platform
 * @param {*} platform {x64|x86}
 */
function genGuiWixFileListForPlatform(platform) {
    let wixPath = process.env.WIX;
    let cwd = process.cwd();
    process.chdir("setup/windows");
    console.log(`Running WiX heat.exe to generate GUI package file list for ${platform}..`);
    try {
        let platformExt = platform == 'x64' ? 'x64' : 'ia32';
        let cmd = `\"${wixPath}bin/heat.exe\"  dir \"../../gui/dist/${platform}/KensingtonWorks2-win32-${platformExt}" -gg -scom -sfrag -sreg -srd -cg GUI -dr INSTALLFOLDER -var wix.guiBuildFolder -o "gui.wxs`;
        execSync(cmd);
        console.log('GUI package file list generated.')
    } finally {
        process.chdir(cwd);
    }
}

/**
 * Uses WiX heat.exe to generate GUI file list -- only on Windows
 */
function genGuiWiXFileList(cb) {
    if (os.platform() == 'win32') {
        // Shipping 32-bit binaries only in Windows
        genGuiWixFileListForPlatform('x86');
    }
    if (cb) { cb(); }
}
exports.genGuiWiXFileList = genGuiWiXFileList;

/**
 * Experimental unified setup for Windows
 * @param {*} cb
 */
function buildSetup(cb) {
    if (options.noSetup) {
        console.log('Not building package as setup has been disabled.')
        cb();
        return;
    }

    let pv = getVersion("product", options.releaseType);
    process.env.PRODUCT_VERSION = `${pv[0]}.${pv[1]}.${pv[2]}.0`;
    if (os.platform() === 'darwin') {
        console.log('Building OS X package. Version:', process.env.PRODUCT_VERSION);
        buildOSXSetup(cb);
    } else if (os.platform() === 'win32') {
        genGuiWixFileListForPlatform('x86');
        console.log('Building Windows MSI package. Version: ', process.env.PRODUCT_VERSION);
        return execTaskInFolder(
            './setup/windows',
            `msbuild unified.wixproj /p:Configuration=${options.configuration} /p:Platform=x86 /p:PRODUCT_VERSION=${pv[0]}.${pv[1]}.${pv[2]}.0`,
            cb
        );
    }
}

/**
 * Generates MSI language transforms for all supported locales, listed in
 * LOCALIZATION_LANGS array.
 *
 * @param {*} function Gulp task completion callback
 */
function generateLangTransforms(cb) {
    if (process.platform != 'win32') {
        cb();
        return;
    }

    LOCALIZATION_LANGS.forEach((lang, index, langs) => {
        console.log(`Generating MSI transform for ${lang.tag}...`);
        let cmd = `torch -t language ./bin/x86/Release/en-US/kwsetup.msi ./bin/x86/Release/${lang.tag}/kwsetup.msi -out ./bin/x86/Release/${lang.tag}/kwsetup.mst`;
        execSync(
            cmd,
            { cwd: 'setup/windows' }
        );
    })
    cb();
}
exports.generateLangTransforms = generateLangTransforms;

/**
 * Integrates language transforms stored in individual locale subfolder into
 * the en-US MSI.
 *
 * @param {*} function Gulp task completion callback
 */
function integrateLangTransforms(cb) {
    if (process.platform != 'win32') {
        cb();
        return;
    }

    // Copy en-US setup MSI to its parent folder
    // We work on this copy instead of the original build output so that if
    // something goes wrong, we can attempt the process again.
    fs.copyFileSync(
        './setup/windows/bin/x86/Release/en-US/kwsetup.msi',
        './setup/windows/bin/x86/Release/en-US/kwsetup-org.msi'
        );

    // append language transforms to en-US
    let summaryInfoLcdIdList = '1033';
    LOCALIZATION_LANGS.forEach((lang, index, langs) => {
        console.log(`Appending language transform for ${lang.tag}...`);
        let cmd = `wisubstg.vbs ./bin/x86/Release/en-US/kwsetup.msi ./bin/x86/Release/${lang.tag}/kwsetup.mst ${lang.code}`;
        execSync(
            cmd,
            { cwd: 'setup/windows' }
        );
        summaryInfoLcdIdList += `,${lang.code}`;
    })

    // Update the summary information of the en-US MSI with all support language locale codes.
    execSync(
        `wilangid.vbs ./bin/x86/Release/en-US/kwsetup.msi Package ${summaryInfoLcdIdList}`,
        { cwd: 'setup/windows' }
    );
    cb();
}
exports.integrateLangTransforms = integrateLangTransforms;

/*
 * Copy the setup file from setup/{osx|windows}/... to dist/{osx|windows}
 */
function copySetupToDist(cb) {
    if (options.noSetup) {
        console.log('Not copying package to dist/ as setup has been disabled.')
        cb();
        return;
    }

    if (os.platform() === "darwin") {
        if (!fs.existsSync('./dist')) {fs.mkdirSync('./dist'); }
        if (!fs.existsSync('./dist/osx')) { fs.mkdirSync('./dist/osx'); }
        console.log("Copying setup package to dist/osx...");
        let pv = getVersion("product", options.releaseType);
        return src(`./setup/osx/build/KensingtonWorks_${pv[0]}.${pv[1]}.${pv[2]}.pkg`)
            .pipe(rename((path) => {
                path.basename += '_unsigned';
            }))
            .pipe(dest(`./dist/osx/`));
    } else if (os.platform() == "win32") {
        if (!fs.existsSync('./dist')) {fs.mkdirSync('./dist'); }
        if (!fs.existsSync('./dist/windows')) { fs.mkdirSync('./dist/windows'); }
        console.log("Copying setup package to dist/windows...");
        let pv = getVersion("product", options.releaseType);
        return src(`./setup/windows/bin/x86/Release/en-US/kwsetup.msi`)
            .pipe(rename((path) => {
                path.basename = `KensingtonWorks_${pv[0]}.${pv[1]}.${pv[2]}`;
            }))
            .pipe(dest(`./dist/windows/`));
    }
}

/**
 * Bump product version for the releaseType specified in command line option.
 *
 * @param {function} cb Gulp task completion callback.
 */
function bumpVersion(cb) {
    // Increment versions of relevant components and product
    if (options.noIncrVersion) {
        console.log('Not incrementing version as noIncrVersion specified.')
        cb();
        return;
    }
    incrVersion('product', options.releaseType);
    cb();
}

// For dev testing only
function buildOSXCopyFiles(cb) {
    del.sync('dist/');
    setupOSXCopyFiles();
    cb();
}

// Build TAU gui binary
function buildTAUGUI(cb) {
    del.sync('./dist/taugui*.zip');
    return execTaskInFolder(
        '.',
        'powershell.exe ./tests/buildgui.ps1',
        cb
    );
}

// Zip up the final tau GUI package and rename it to embed incremented
// version number and copy to ./dist folder.
function zipTAUGUI(cb) {
    let pv = getVersion("tau", 'minor');
    return src('./tests/dist/taugui/**/*')
        .pipe(zip(`taugui-${pv[0]}.${pv[1]}.${pv[2]}.zip`))
        .pipe(dest('./dist'));
}

function copyTAUGUIBinaryToDist(cb) {
    if (os.platform() == 'win32') {
        del.sync('./dist/taugui*.exe');
        let pv = getVersion("tau", 'minor');
        return src(['tests/dist/taugui.exe'])
            .pipe(zip(`taugui-${pv[0]}.${pv[1]}.${pv[2]}.exe`))
            .pipe(dest('./dist'));
    } else {
        console.log('TAU GUI not supported in Mac');
        cb();
    }
}

/**
 * Builds tbwtests.exe which is the standalone executable for running
 * all the tests.
 *
 * @param {*} cb
 */
function buildTAUCommandline(cb) {
    if (os.platform() == 'win32') {
        del.sync('./tests/build')
        del.sync('./tests/dist')
        return execTaskInFolder(
            './tests',
            'powershell.exe ./buildtau.ps1',
            cb
        );
    } else {
        console.log('TAU command line binary not supported in Mac');
        cb();
    }
}

/**
 * Clean TAU output from dist folder.
 *
 * @param {*} cb
 */
function cleanTAU(cb) {
    del.sync('./dist/tbwtests*')
    del.sync('./dist/taugui*')
    cb();
}
/**
 * Copies the TAU standalone binary, tbwtests.exe to ./dist folder
 * @param {*} cb
 */
function copyTAUBinaryToDist(cb) {
    if (os.platform() === 'win32') {
        del.sync('./dist/tbwtests*.exe');
        let pv = getVersion("tau", 'minor');
        return src(['tests/dist/tbwtests.exe'])
        .pipe(rename((path) => {
            path.basename += `_${pv[0]}.${pv[1]}.${pv[2]}`;
        }))
        .pipe(zip(`tbwtests-bin-${pv[0]}.${pv[1]}.${pv[2]}.zip`))
        .pipe(dest('./dist'));
    } else {
        console.log('TAU binary not suppoted in Mac');
        cb();
    }
}

// Zip Python test scripts into ./dist/tbwtests.zip
function zipTestScripts(cb) {
    del.sync('./dist/tbwtests*.zip');
    let pv = getVersion("tau", 'minor');
    return src(['tests/*', '!tests/build', '!tests/dist', '!tests/__pycache__', '!tests/test_results.txt'])
        .pipe(zip(`tbwtests-src-${pv[0]}.${pv[1]}.${pv[2]}.zip`))
        .pipe(dest('./dist'));
}

if (process.platform === 'win32') {
    exports.buildDriver = series(bumpDriverVersion, buildDownloadService, buildCoreDriver, signDriversWindows);
} else {
    exports.buildDriver = series(bumpDriverVersion, buildCoreDriver);
}
exports.buildHelper = buildHelper;
exports.buildtbwDKManager = buildtbwDKManager; // *&*&*&_V1_20200908
exports.buildtbwDKManager11 = buildtbwDKManager11; // *&*&*&_V1_20200908
// for test, all sign functions are put at signBinaries() function
exports.signtbwDKManager = signtbwDKManager;
exports.signtbwDKManager11 = signtbwDKManager11;
exports.buildOSXUninstaller = buildOSXUninstaller;
exports.buildPrefPane = buildPrefPane;
exports.buildSetup = buildSetup;
exports.buildCopyOSXFiles = buildOSXCopyFiles;
exports.writeVersion = writeVersionToOSXDistributionXml;

// build GUI frontend to TAU as a executable
exports.buildTAU = series(
    (cb) => {
        incrVersion('tau', 'minor');
        cb();
    },
    cleanTAU,
    zipTestScripts,
    buildTAUCommandline,
    copyTAUBinaryToDist
);

function writeGuiVersion(cb) {
    // write version header
    let pv = getVersion("product", options.releaseType);
    let vFileTemplate = `
    export const PRODUCT_VERSION = {
        major: ${pv[0]},
        minor: ${pv[1]},
        patch: ${pv[2]},
        beta: ${pv[3]}
    };
    `;
    fs.writeFileSync('./gui/src/version.ts', vFileTemplate, 'utf8');
    if (cb) { cb(); }
}

function generateUpdateMetaFile(cb) {
    let pv = getVersion("product", options.releaseType);
    let platform = 'osx';
    let distPath = ''
    let distFile = '';
    if (process.platform === 'win32') {
        platform = 'win';
        distPath = './dist/windows/';
        distFile = `KensingtonWorks_${pv[0]}.${pv[1]}.${pv[2]}.msi`;
    } else {
        distPath = './dist/osx/';
        distFile = `KensingtonWorks_${pv[0]}.${pv[1]}.${pv[2]}.pkg`;
    }
    // read distributable size
    let stats = fs.statSync(distPath+distFile);
    let fileSizeInBytes = stats["size"]
    let downloadBaseUrl = `${UPDATE_SERVER_PATH}`;
    let vFileTemplate = `
    {
        "code": "KGTW",
        "name": "KensingtonWorks",
        "version_major": ${pv[0]},
        "version_minor": ${pv[1]},
        "version_build": ${pv[2]},
        "os": "${process.platform}",
        "published_date": "${dformat.asString(dformat.ISO8601_WITH_TZ_OFFSET_FORMAT)}",
        "size": ${fileSizeInBytes},
        "download_url": "${downloadBaseUrl}${distFile}",
        "active": true
    }
    `;
    fs.writeFileSync(`./dist/kgtw_${platform}.json`, vFileTemplate, 'utf8');
    if (cb) { cb(); }
}

/**
 * Copies env.production as the deployment environment.
 */
function copyguiEnvironment(cb) {
    return src([`./gui/config/env_${options.guiEnv}.json`])
    .pipe(rename((path) => {
        path.basename = 'env';
    }))
    .pipe(dest('./gui/app', {overwrite: true}));
}

/**
 * Builds new TypeScript GUI
 * @param {*} cb function Gulp completion callback
 */
function buildGuiRaw(cb) {
    let pv = getVersion("product", options.releaseType);
    let guiOptions = {
        dir: ".",
        name: "KensingtonWorks2",
        asar: true,
        //asar: false,
        prune: true,
        overwrite: true,
        ignore: reduceDependentPackageFootprint,
        appVersion: `${pv[0]}.${pv[1]}.${pv[2]}`,
        win32metadata: {
            CompanyName: 'Kensington, a division of ACCO BRANDS',
            ProductName: 'KensingtonWorks',
            FileDescription: "KensingtonWorks Configuration Utility"
        }
    };
    let target_dir = `./gui/Release`;
    if (fs.existsSync('./gui/build/Release/commandServer.node')) {
        if(!fs.existsSync('./gui/Release'))
        {
            fs.mkdirSync(target_dir);
        }
        fs.copyFileSync(`./gui/build/Release/commandServer.node`, `./gui/Release/commandServer.node`); // Just overwrite it 
    }
    else{
        cb('commandServer.node is not existing. Please check node-gyp project.'); // stop build
    }

    if (os.platform() === 'darwin') {
        guiOptions["appBundleId"] = "com.kensington.kensingtonworks2.app";
        guiOptions["icon"] = "./icon/TrackballWorks.icns";
        // Mac is always 64-bit
        guiOptions["out"] = './dist/x64';
        guiOptions["arch"] = "x64";
        guiOptions["name"] = "KensingtonWorks";
        guiOptions["executableName"] = 'KensingtonWorks2';
        guiOptions['extendInfo'] = {
            ElectronTeamID: '38YBH9NQ69',
            LSMultipleInstancesProhibited: true
        };
    } else {
        guiOptions["icon"] = "./icon/TrackballWorks.ico";
        guiOptions["version-string.CompanyName"] = "Kensington, a division of ACCO BRANDS";
        guiOptions["version-string.FileDescription"] = "KensingtonWorks Configuration Utility";
        // We're shipping 32-bit binaries (except the driver of course)
        // on Windows.
        guiOptions["out"] = './dist/x86';        
        guiOptions["arch"] = 'ia32';
        /*
        guiOptions["out"] = './dist/x64'; //*&*&*&V1_20201109 test 64bit for addon
        guiOptions["arch"] = 'x64';*/
        //guiOptions["extra-resource"] = './commandServer.node'; // doesnt work
    }
    console.log('GUI build options:', guiOptions);
    var cwd = process.cwd();
    writeGuiVersion();
    execSync("npm run build", {cwd: 'gui'});
    process.chdir("gui");
    packager(guiOptions)
    .then(appPaths => {
        console.log("GUI built, target in:", appPaths);
        process.chdir(cwd);
        cb();
    }).finally(() => {
    });
}

/**
 * Compose buildGui task as copying the requested environment & and then
 * spawning the actual GUI build task.
 */
var buildGui = series(
    copyguiEnvironment,
    buildGuiRaw
);

function setTargetToX64(cb) {
    options.arch = 'x64'
    cb();
}

function setTargetToX86(cb) {
    options.arch = 'x86'
    cb();
}

/**
 * Copy the platform drivers to the 'bin/windows/driver/{arch}' folder.
 *
 * @param {string} platform OS platform, 'win10' or 'win7'
 * @param {string} arch Architecture, 'x64' or 'x86'
 */
function copyWindowsPlatformDriversToBin(platform, arch) {
    return src(`./driver/windows/${arch}/Release/tbwkern/*`)
        .pipe(dest(`./bin/windows/driver/${platform}/${arch}/`));
}

function copyWindowsDriversToBin(cb) {
    if (os.platform() !== "win32") {
        cb();
        return;
    }

    console.log(`Copying drivers to bin/windows..`);
    copyWindowsPlatformDriversToBin('win10', 'x64')
    copyWindowsPlatformDriversToBin('win10', 'x86')
    copyWindowsPlatformDriversToBin('win7', 'x64')
    copyWindowsPlatformDriversToBin('win7', 'x86')

    cb();
}

exports.copyWindowsDriversToBin = copyWindowsDriversToBin;

exports.setupOSXCopyFiles = ((cb) => {
    setupOSXCopyFiles();
    cb();
});

/**
 * Generates a zip file containing all the relevant binaries
 * that can be uploaded to Apple server for notarization.
 * Zip file is generated as ./dist/osx/notarize_X.Y.Z.zip,
 * where X, Y & Z are product major, minor & patch version
 * numbers.
*/
function zipForNotarization(cb) {
    if (process.platform != 'darwin') {
        console.error('This task only applies to OSX!');
        cb();
    }
    let pv = getVersion("product", options.releaseType);
    let folderName = `notarize_${pv[0]}.${pv[1]}.${pv[2]}`;
    let targetPath = `/tmp/${folderName}`;
    del.sync(targetPath, {force: true});
    console.log(`Copying binaries to notarize to ${targetPath}...`);
    vfs.src([ // *&*&*&V1_20200917 gui和helper都不做了（如果helper分開放還是可以做)
            './uninstaller/build/Release/**',
            './bin/osx/driver/**',
            //'./driver/osx-driverkit/build/Release/**', // *&*&*&V1_20200916
            './bin/osx/driverkit/**', // *&*&*&V_20210108
            './bin/osx/driverkit11/**', // *&*&*&V_20210108
            '!.DS_Store',
            '!./**/*.dSYM',
            '!./**/*.dSYM/**/*',
            //'!./driver/osx-driverkit/build/Release/KensingtonWorks.swiftmodule', // *&*&*&Va_20200916
            //'!./driver/osx-driverkit/build/Release/KensingtonWorks.swiftmodule/**',
            //'!./driver/osx-driverkit/build/Release/KensingtonWorks.swiftmodule/**/*',
        ], {resolveSymlinks: false})
        .pipe(vfs.dest(targetPath))
        .on('end', () => {
            // no zip the targetFolder contents
            let zipFilename = `./dist/osx/${folderName}.zip`;
            console.log(`Zipping up binaries from ${targetPath} as ${zipFilename}..`);
            del.sync(zipFilename);
            execSync(`ditto -c -k ${targetPath} ${zipFilename}`);
            del.sync(targetPath, {force: true});
            cb();
        });
}

/**
 * Uploads the notarization zip file to Apple notarization server for
 * notarization.
 *
 * Function expects a valid notarization zip file to be present in
 * dist/osx folder, generated by the function above, zipForNotarization.
 *
 * @param {*} fileToNotarize string filepath to notarize
 *
 * @returns:
 *      Notarization request UUID on success
 *      null on failure
 *
 * Requires:
 *      - Xcode 10.x to be installed
 *      - Environment variable ALTOOL_PASSWORD to be set to altool's
 *        app specific password in Apple Dev portal.
 */
async function submitForNotarizationImpl(fileToNotarize) {
    if (process.platform != 'darwin') {
        console.error('This task only applies to OSX!');
        return null;
    }
    if (!MAC_DEVELOPER_ALTOOL_PASSWORD) {
        console.error('Set env variable "ALTOOL_PASSWORD" to its app password as set in Apple dev portal!');
        return null;
    }
    console.log(`Uploading ${fileToNotarize} for notarization...`);
    let cmd = `xcrun altool --notarize-app --primary-bundle-id "com.kensington.kensingtonworks2" --username "${MAC_DEVELOPER_ACCOUNT_ID}" --password "${MAC_DEVELOPER_ALTOOL_PASSWORD}" --file ${fileToNotarize}`;
    console.log("[notarization] " + cmd);
    const { stdout, stderr } = await prExec(cmd);
    //let output = stderr.toString(); // *&*&*&V1_MOD, for SDK10.15 
    let output = stdout.toString();
    //console.log('VVVVVVV the output string is8:', output);
    //console.log('VVVVVVV the stdout string is:', stdout);
    let parts = output.split('=');
    if (parts.length >= 2) {
        let uuid = parts[1].trim();
        // Apple's notarization server runs on a queing system (unsurprisingly)
        // Give it a few seconds to trigger, before returning the UUID.
        // Ideally this timer interval shouldn't be hardcodew, but it ought to
        // work most of the time. (Unless there's a serious problem with
        // network or Apple's servers or both)
        console.log(`Submitted files, waiting ${NOTARIZATION_CHECK_WAIT_INTERVAL} secs for Apple notarization task to trigger...`);
        sleep(NOTARIZATION_CHECK_WAIT_INTERVAL);
        return uuid;
    }
    return null;
}

function submitForNotarization(cb) {
    if (process.platform !== 'darwin') {
        console.error('This task only applies to OSX!');
        return null;
    }
    let pv = getVersion("product", options.releaseType);
    let zipFilename = `./dist/osx/notarize_${pv[0]}.${pv[1]}.${pv[2]}.zip`;
    submitForNotarizationImpl(zipFilename)
    .then(uuid => {
        options.notarizeUuid = uuid;
        console.log('Notarization request success, Request UUID:', uuid);
    }).catch(err => {
        cb(err);
    }).finally(() => {
        cb();
    });
};

/**
 *
 * @param {*} url string Notarization request log URL
 */
async function getNotarizationResults(url) {
    // @ts-ignore
    return axios.get(url)
    .then(response => {
        let result = response.data;
        // console.log('getNotarizationResults: The result is:', result);
        return result;
    })
}

/**
 * Function waits for notarization to be completed by repeatedly querying
 * the notarization server for status of the UUID.
 *
 * @param {*} uuid string UUID of the notarization request that will be polled
 * for status.
 *
 * @returns string The notarization log URL upon success
 *          null upon failure.
 */
async function waitForNotarizationImpl(uuid) {
    if (!MAC_DEVELOPER_ALTOOL_PASSWORD) {
        throw Error('Set env variable "ALTOOL_PASSWORD" to its app password as set in Apple dev portal!');
    }
    let cmd = `xcrun altool --notarization-info ${uuid} -u "${MAC_DEVELOPER_ACCOUNT_ID}" -p "${MAC_DEVELOPER_ALTOOL_PASSWORD}"`;
    let success = false;
    while (true) {
        console.log('Checking notarization status..');
        const { stdout, stderr } = await prExec(cmd);
        //console.log('vvvvvv the stdout is:', stdout);
        //console.log('vvvvvv the stderr is:', stderr);
        //let output = stderr.toString(); // *&*&*&V1_MOD, for SDK10.15, 20200320
        let output = stdout.toString();
        //console.log('vvvvvv the Output is:', output);
        let lines = output.split('\n');
        for (let index = 0; index < lines.length; index++) {
            let line = lines[index];
            console.log("[notarization] " + line)
            line = line.trim();
            if (line.indexOf('Status:') > -1) {
                let status = line.substr('Status:'.length).trim();
                if (status === 'success') {
                    console.log('Notarization completed!')
                    success = true;
                    break;
                } else if (status !== 'in progress') {
                    console.error('Error notarizing..');
                    return null;
                }
            }
        }
        if (success) {
            break;
        }
        console.log('In progress, trying again in 30 seconds..')
        sleep(30);
    }

    if (success) {
        console.log('Retrieving log file URL..');
        let logFileURL = null;
        while (logFileURL == null) {
            const { stdout, stderr } = await prExec(cmd);
            // let output = stderr.toString(); // *&*&*&V1_MOD for SDK10.15, 20200320
            let output = stdout.toString();
            console.log('The stderr is:', output);
            let lines = output.split('\n');
            for (let index = 0; index < lines.length; index++) {
                let line = lines[index].trim();
                let lineParts = line.split(':');
                if (lineParts.length >= 2) {
                    let field = lineParts[0].trim();
                    let value = lineParts[1].trim();
                    //console.log('\tfield:', field, ', value:', value);
                    if (field.indexOf('LogFileURL') > -1) {
                        lineParts.splice(0, 2);
                        logFileURL = 'https:' + lineParts.join('');
                        // console.log('The url is:', logFileURL);
                        break;
                    }
                }
            }
            if(!logFileURL) {
                console.log('Log file URL not ready, trying again in 10 secs..');
                sleep(10);
            }
        }
        return getNotarizationResults(logFileURL);
    }

    return null; // return null to indicate failure
}

/**
 * A task that polls the Apple server for notarization result and prints
 * its status along with any issues.
 */
function waitForNotarization(cb) {
    if (process.platform != 'darwin') {
        console.error('This task only applies to OSX!');
        return null;
    }

    //fenrir
    if (options.notarizeUuid ===null || options.notarizeUuid.length === 0) {

        throw Error('Specify notarization request UUID as --notarizeUuid parameter value');
    }
    let uuid = options.notarizeUuid;
    waitForNotarizationImpl(uuid)
    .then((result) => {
        //fenrir
        if (result !== null) {
            console.log('Notarization result:' + result.status);
            if (result.issues) {
                console.error('Notarization has the following issues:', result.issues, ', please correct them are resubmit the package.')
            } else {
                console.log('Congratulations, no issues reported in notarization! You may staple the apps & build the setup package.')
            }
            cb();
        }  else  {
            throw "can not get result of Notarization status, because result  is null";
        }
    }).catch(err => {
        console.error('Error notarizing:', err);
        cb(err);
    });
}

/**
 *
 * @param {*} app string
 */
function stapleFn(app) {
    return execSync(`xcrun stapler staple "${app}"`);
}

/**
 * Post successful notarization, staples the respective binary with its
 * notarization info, preparing it to be copied to the setup's content
 * folder before the setup build script is invoked.
 *
 * Expects a previous notarization to have been successful on all the
 * binaries listed below.
 */
function stapleApps(cb) {
    if (process.platform != 'darwin') {
        console.error('This task only applies to OSX!');
        return null;
    }
    let apps = [ // *&*&*&Va_20200917 gui和helper都不做了
        //`./gui/dist/x64/${MAC_GUI_BUILD_OUTPUT_NAME}-darwin-x64/${MAC_GUI_BUILD_OUTPUT_NAME}.app`,
        //'./tbwhelper/osx/build/Release/KensingtonWorksHelper.app',
        './uninstaller/build/Release/KensingtonWorks Uninstaller.app',
        './bin/osx/driver/trackballworks2.kext',
        './bin/osx/driverkit/KensingtonWorks.app', // *&*&*&V_20210108
        './bin/osx/driverkit11/KensingtonWorks11.app', // *&*&*&V_20210108
    ];
    for (let index = 0; index < apps.length; index++) {
        const app = apps[index];
        stapleFn(app);
    }
    cb();
}

/**
 * Composite function to handle the entire notarization workflow.
 *
 * @param {*} cb callback
 */
exports.notarizeApps = options.doNotNotarize ? (cb) => { cb(); } : series(
    zipForNotarization,
    submitForNotarization,
    waitForNotarization,
    stapleApps
);

function justNotarizePackage(cb) {
    if (process.platform != 'darwin') {
        console.error('This task only applies to OSX!');
        return null;
    }
    console.log('Notarizing package...');
    let pv = getVersion("product", options.releaseType);
    let package = `./dist/osx/KensingtonWorks_${pv[0]}.${pv[1]}.${pv[2]}.pkg`;
    submitForNotarizationImpl(package)
    .then(requestUuid => {
        return waitForNotarizationImpl(requestUuid);
    }).then(result => {
        console.log('The resule of justNotarizePackage is:', result);
        //if (result.issues.length == 0) {            
        //if (result.issues == null) {
        if(result.status == 'Accepted'){
            console.log('Notarization succeeded, without any issues!');
        } else {
            let expectedIssue = false;
            let issues = result.issues;
            if (issues.length == 1) {
                let component = issues[0];
                // This is an expected error as we rename 'KensingtonWorks.app' to 'KensingtonWorks .app'
                if (component.path.indexOf('KensingtonWorks .app/Contents/MacOS/KensingtonWorks') > -1) {
                    expectedIssue = true;
                }
                if (!expectedIssue) {
                    cb(new Error(`Error in package notarization, issues: ${issues}`));
                }
            }
        }
        cb();
    }).catch(err => {
        console.error("Notarizing package failed!, error:", err);
        cb(err);
    });
}

/**
 * Staples the final installable package in dist/osx. Package name is derived
 * from product version information in version_osx.json.
 */
function staplePackage(cb) {
    if (process.platform != 'darwin') {
        console.error('This task only applies to OSX!');
        return null;
    }
    console.log('Stapling package...');
    let pv = getVersion("product", options.releaseType);
    let apps = [
        `./dist/osx/KensingtonWorks_${pv[0]}.${pv[1]}.${pv[2]}.pkg`,
    ];
    for (let index = 0; index < apps.length; index++) {
        const app = apps[index];
        stapleFn(app);
    }
    cb();
}

/**
 * Composite function to notarize the current package in dist/osx & staple it.
 * @param {*} cb function Gulp callback
 */
exports.notarizePackage = options.doNotNotarize ? (cb) => { cb(); } : series(
    justNotarizePackage,
    staplePackage
);

exports.buildGui = buildGui;

// sign binaries
exports.genTheWholeMainApp = genTheWholeMainApp; // *&*&*&V1_ADD_20200220
exports.signDriversWindows = signDriversWindows;
exports.signBinaries = signBinaries;
exports.copySetupToDist = copySetupToDist;
// sign the final setup package in ./dist/{platform}/
exports.signPackage = signPackage;
exports.genGuiWixFileList = genGuiWiXFileList;
exports.cleanDist = cleanDist
exports.bumpVersion = bumpVersion;
exports.buildDownloadService = buildDownloadService;
exports.generateUpdateMetaFile = generateUpdateMetaFile;

// Notarization primitives
exports.zipForNotarization = zipForNotarization;
exports.submitForNotarization = submitForNotarization;
exports.waitForNotarization = waitForNotarization;
exports.stapleApps = stapleApps;
exports.justNotarizePackage = justNotarizePackage;
exports.staplePackage = staplePackage;

/**
 * Builds the unified setup after building x86 & x64 drivers
 */
if (os.platform() == "win32") {
    // @ts-ignore 2323
    exports.buildAll = series(
        cleanAll,
        bumpVersion,
        buildHelper,
        buildGui,
        buildRemLegacy,
        signBinaries,
        buildSetup,
        generateLangTransforms,
        integrateLangTransforms,
        copySetupToDist,
        signPackage,
        generateUpdateMetaFile
    );
} else {
    // @ts-ignore 2323
    exports.buildAll = series(
        cleanAll,
        bumpVersion,
        buildHelper,
        //buildtbwDKManager, // v_20210106: 暫時停止DriverKit自動編譯
        signHelper, // also sign tbwDKManager
        buildOSXUninstaller,
        buildPrefPane,
        buildGui,
        //genTheWholeMainApp, // *&*&*&V1_ADD_20200220 to generate the whole app first(*&*&*&Va_20200914 create Helper directory only)
        signBinaries,
        this.notarizeApps,
        buildSetup,
        copySetupToDist,
        signPackage,
        this.notarizePackage,
        generateUpdateMetaFile
    );
}
