#!/bin/bash

source ./common_funcs.sh
# parse HELPER_ADDR & DEVICE_ID from environment variables
parse_common_args


BUTTON=$1
RESOURCE=$2 

if [ "$BUTTON" == "" ] || [ "$RESOURCE" == "" ]; then
        echo "Configure Trackball button to open an app/folder/website."
	echo "Usage:"
	echo "   $ cfgopen <button-mask> <app path>"
	echo
	echo "       button-mask - Button mask to configure, 1~15"
	echo "       path - Full path to the app to open"
	echo
	common_args_usage
	exit 1
fi

JSON="{\"command\":\"launching_openApplication\", \"params\": {\"path\":\"$RESOURCE\"}}"
dump_common_args
echo "Button:" $BUTTON
echo "App:" $RESOURCE

curl --header "Content-Type: application/json" --request POST --data "$JSON" http://$HELPER_ADDR:9090/config/*/$DEVICE_ID/buttons/$BUTTON -i

