cd tests
workon tau
if (!$?) {
    throw "Building TAU python executable requires 'tau' virtualenv to be setup"
}
pyinstaller.exe -F .\taugui.spec -y
if (!$?) {
    throw "Error building the TAU executable. Is PyInstaller installed?"
}