#!/bin/bash

source ./common_funcs.sh
# parse HELPER_ADDR & DEVICE_ID from environment variables
parse_common_args

BUTTON=$1
KEYCODE=$2
MODIFIER="0"

if [ "$BUTTON" == "" ] || [ "$KEYCODE" == "" ]; then
	echo "Configure Trackball button to generate a keystroke."
	echo "Usage:"
	echo "   $ cfgkeystroke <button-mask-number> <keyCode> [<modifier>]"
	echo
	echo "       button-mask - 1~15. For chorded buttons, specify the combined mask."
	echo "			           For example, 3 is the mask for bottom two buttons chord"
	echo "			           and 15 is the mask for top two buttons chord."
	echo "       keyCode     - The key scancode to poke"
	echo "       modifiers   - Combination of {shift|ctrl|alt|cmd|win}, delimited"
	echo "       	           by comma."
	echo
	common_args_usage
	exit 1
fi

if [ -z "$3" ]
then
	MODIFIER=""
else
	MODIFIER=$3
fi

dump_common_args
echo "button:" $BUTTON
echo "keyCode:" $KEYCODE
echo "Modifier:" $MODIFIER

curl --header "Content-Type: application/json" --request POST --data "{\"command\":\"keystroke\", \"modifiers\":\"$MODIFIER\", \"keyCode\":$KEYCODE}" http://$HELPER_ADDR:9090/config/*/$DEVICE_ID/buttons/$BUTTON -i

