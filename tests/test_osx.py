import sys
import requests
import json
import subprocess
from AppKit import NSWorkspace
from Quartz import (
    CGWindowListCopyWindowInfo,
    kCGWindowListOptionOnScreenOnly,
    kCGNullWindowID
)

from pynput import keyboard, mouse
import core

# so that common tests get pull in
from test_common import *

def getActiveWindowDetails():
    '''Returns a 2-tuple consisting of (process name, windowTitle)'''
    #curr_app = NSWorkspace.sharedWorkspace().frontmostApplication()
    curr_pid = NSWorkspace.sharedWorkspace().activeApplication()['NSApplicationProcessIdentifier']
    #curr_app_name = curr_app.localizedName()
    options = kCGWindowListOptionOnScreenOnly
    windowList = CGWindowListCopyWindowInfo(options, kCGNullWindowID)
    for window in windowList:
        pid = window['kCGWindowOwnerPID']
        ownerName = window['kCGWindowOwnerName']
        windowTitle = window.get('kCGWindowName', u'Unknown')
        if curr_pid == pid:
            #print("%s - %s" % (ownerName, windowTitle))
            return ownerName, windowTitle

    return ('','')


def windowWithTitleExists(title):
    '''Returns a 2-tuple consisting (boolean, window) if a window
    with the given title exists'''
    options = kCGWindowListOptionOnScreenOnly
    windowList = CGWindowListCopyWindowInfo(options, kCGNullWindowID)
    for window in windowList:
        windowTitle = window.get('kCGWindowName', u'Unknown')
        #print("windowTitle: %s " % windowTitle)
        if windowTitle == title:
            return (True, window)

    return (False, None)
    

class TestEditingOSX(TestKeystrokeBase):

    def test_editing_duplicate(self):
        '''Test editing -> duplicate (Cmd+d)'''
        self._test_keystroke_command(Button.Button3, 'editing_duplicate', 'd', modifiers='cmd')

    def test_editing_increaseSize(self):
        '''Test editing -> increase size (Cmd+plus)'''
        self._test_keystroke_command(Button.Button3, 'editing_increaseSize', '+', modifiers='cmd')

    def test_editing_decreaseSize(self):
        '''Test editing -> decrease size (Cmd+minus)'''
        self._test_keystroke_command(Button.Button3, 'editing_decreaseSize', '-', modifiers='cmd')


class TestApplicationOSX(TestKeystrokeBase):

    pass
    """
    def test_minimizeWindow(self):
        '''Test Application -> Minimize Window (Cmd+m) '''
        subprocess.call(["open", "/Applications/TextEdit.app"])
        timeout = 3.0
        start = time.time()
        while (time.time() - start) < timeout:
            time.sleep(0.1)
            ownerApp, windowTitle, window = getActiveWindowDetails()
            if ownerApp == "TextEdit":
                break

        if time.time() >= (start + timeout):
            print("Could not launch TextEdit to test app minimization, skipping test..")
            return

        self.assertTrue(True)
        #self._test_keystroke_command(Button.Button3, 'application_minimizeWindow', 'm', modifiers='cmd')

    """

"""
class TestSystemOSX(TestKeystrokeBase):
    '''These tests always fail when run as part of the entire test suite.
    However, when run individually by using the command
            $ python tbwtests.py TestSystemOSX
    they will succeed. Not sure why.'''
    def test_spotlight(self):
        self.configure_button_action(Button.Button3, command='system_spotlight')
        emulate_button_click(Button.Button3)
        time.sleep(2.0)
        exists, _ = windowWithTitleExists('Spotlight')
        if exists:
            controller = keyboard.Controller()
            controller.press(keyboard.Key.esc)
            controller.release(keyboard.Key.esc)
        self.assertTrue(exists)

    def test_showSpotlightWindow(self):
        self.configure_button_action(Button.Button3, command='system_showSpotlightWindow')
        emulate_button_click(Button.Button3)
        time.sleep(2.0)
        exists, _ = windowWithTitleExists('Searching “This Mac”')
        if exists:
            controller = keyboard.Controller()
            controller.press(keyboard.Key.esc)
            controller.release(keyboard.Key.esc)
        self.assertTrue(exists)

    def test_showForceQuitDialog(self):
        '''Test System -> Show Force Quit Dialog'''
        self.configure_button_action(Button.Button3, command='system_showForceQuitDialog')
        emulate_button_click(Button.Button3)
        time.sleep(2.0)
        exists, _ = windowWithTitleExists('Force Quit Applications')
        if exists:
            controller = keyboard.Controller()
            controller.press(keyboard.Key.esc)
            controller.release(keyboard.Key.esc)
        self.assertTrue(exists)
"""


if __name__ == '__main__':
    if sys.platform != 'darwin':
        print("These tests are meant to be run from OSX!")
        exit(1)

    #wait_for_keyboard_input(core.Button.Button3, 10, timeout=10, emulate=False)
    core.tbw_tests_main()
