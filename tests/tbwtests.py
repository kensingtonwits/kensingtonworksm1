# test launcher
import sys

from core import tbw_tests_main


if sys.platform == "win32":
    from test_win import *
elif sys.platform == "darwin":
    from test_osx import *

if __name__ == '__main__':
    tbw_tests_main()
