"""
Interface for TAU pointer action emulator firmware.
"""

import enum
import time
import serial
import argparse
import os, sys
import shutil
import subprocess

if sys.platform == 'darwin':
    import plistlib # OS X
else:
    import xml  # windows

import emulator_types as etypes


def button_to_index(button):
    """Converts button enum to its equivalent 0-based index value.
    Or -1 if specified button is not found in our list."""

    assert isinstance(button, etypes.Button)

    buttons = [
        etypes.Button.Button1, etypes.Button.Button2, etypes.Button.Button3, etypes.Button.Button4,
        etypes.Button.Button5, etypes.Button.Button6, etypes.Button.Button7, etypes.Button.Button8,
        ]
    if button in buttons:
        return buttons.index(button)
    return -1


def index_to_button(index):
    """Converts 0-based button index supplied to its equivalent
    Button enum value."""

    buttons = [
        etypes.Button.Button1, etypes.Button.Button2, etypes.Button.Button3, etypes.Button.Button4,
        etypes.Button.Button5, etypes.Button.Button6, etypes.Button.Button7, etypes.Button.Button8,
        ]
    assert index < len(buttons)
    return buttons[index]


# ID of the Kensington trackball device that we're using for testing
g_device_id = 32792

def settings_filepath():
    """Returns the path to TrackballWorks settings file"""

    if sys.platform == 'darwin':
        return os.path.expanduser('~/Library/Preferences/com.kensington.trackballworks.settings.plist')
    else:
        return ''   # TODO


def get_trackballworks_pid():
    """Returns pid of TrackballWorks helper process that is
    started as part of System Preferences

    Returns -1 if process is not found
    """

    if sys.platform != 'darwin':
        return -1

    child = subprocess.Popen(['pgrep TrackballWorksHelper'], stdout=subprocess.PIPE, shell=True)
    res = child.communicate()[0]    # A binary string, which contains pid of existing TrackballWorksHelper

    if len(res):
        return int(res)

    return -1


def get_system_preferences_pid():
    """Returns pid of existing System Prefernces process.

    Or -1 if process is not found
    """

    if sys.platform != 'darwin':
        return -1

    child = subprocess.Popen(["pgrep 'System Preferences'"], stdout=subprocess.PIPE, shell=True)
    res = child.communicate()[0]    # A binary string, which contains pid of existing System Preferences

    if len(res):
        return int(res)

    return -1


def kill_pid(pid, timeout=5):
    """Terminates a process with the given pid.

    Function waits till the process is terminated or timeout expires.

    Returns boolean True if process really terminated. False if timeout
    expires before process termination could be confirmed.
    """
    if sys.platform != 'darwin':
        return True

    kill = subprocess.Popen(["kill", str(pid)])
    return kill.wait(timeout) == 0


def kill_trackballworks():
    """Closes the running instance of TrackballWorksHelper
    Only applicable to Mac. Always succeeds on Windows.

    Returns boolean success indicator.
    """
    if sys.platform != 'darwin':
        return True

    tbw_pid = get_trackballworks_pid()
    if tbw_pid != -1 and not kill_pid(tbw_pid):
        return False

    pref_pid = get_system_preferences_pid()
    if pref_pid != -1 and not kill_pid(pref_pid):
        return False

    return True


def start_trackballworks():
    """Starts the Trackball Works system preferences pane.
    Only applicable in Mac. On Windows just returns success.

    Returns boolean success indicator.
    """
    if sys.platform != 'darwin':
        return True

    child = subprocess.Popen(
        ['open', '-b', 'com.apple.systempreferences', '/Library/PreferencePanes/TrackballWorks.prefPane'])
    # time.sleep(0.3) # give the process 300 milliseconds to start
    if child.wait() != 0:
        return False

    start = time.monotonic()
    START_TIMEOUT = 3 # 2 seconds for the process to start
    while get_trackballworks_pid() == -1 \
        and (time.monotonic()-start) < START_TIMEOUT:
        time.sleep(0.1)

    return get_trackballworks_pid() != -1


def _configure_mouse_and_keyboard_action(button, action, modifiers=etypes.Modifier.NONE, key=0):
    cmds = {
        etypes.ButtonAction.NO_ACTION: 0,
        etypes.ButtonAction.MOUSE_AND_KEYBOARD_KEYBOARD_SHORTCUT: 10,
        etypes.ButtonAction.MOUSE_AND_KEYBOARD_CLICK: 1,
        etypes.ButtonAction.MOUSE_AND_KEYBOARD_RIGHT_CLICK: 2,
        etypes.ButtonAction.MOUSE_AND_KEYBOARD_MIDDLE_CLICK: 3,
        etypes.ButtonAction.MOUSE_AND_KEYBOARD_DRAG: 14,
        etypes.ButtonAction.MOUSE_AND_KEYBOARD_RIGHT_DRAG: 15,
        etypes.ButtonAction.MOUSE_AND_KEYBOARD_MIDDLE_DRAG: 16,
        etypes.ButtonAction.MOUSE_AND_KEYBOARD_DOUBLE_CLICK: 12,
        etypes.ButtonAction.MOUSE_AND_KEYBOARD_DOUBLE_RIGHT_CLICK: 17,
        etypes.ButtonAction.MOUSE_AND_KEYBOARD_DOUBLE_MIDDLE_CLICK: 18,
        etypes.ButtonAction.MOUSE_AND_KEYBOARD_TRIPLE_CLICK: 13,
    }

    success = False
    org = settings_filepath()
    bak = settings_filepath() + '.bak'

    try:
        # make a copy of the current settings file
        shutil.copyfile(org, bak)
        with open(bak, 'rb') as fp:
            config = plistlib.load(fp)
            device = [d for d in config['devices'] if d['target device']['device product id'] == g_device_id][0]
            index = "%02d" % button_to_index(button)
            button_settings = device['buttons'][index]
            button_settings['cmd'] = cmds[action]
            if modifiers != etypes.Modifier.NONE:
                button_settings['mod'] = modifiers.value
            elif 'mod' in button_settings:
                del button_settings['mod']

            if action == etypes.ButtonAction.MOUSE_AND_KEYBOARD_KEYBOARD_SHORTCUT:
                button_settings['key'] = key
            elif 'key' in button_settings:
                del button_settings['key']

            with open(org, 'wb') as fpNew:
                plistlib.dump(config, fpNew)
                os.remove(bak)
                success = True
    except BaseException as e:
        print("Exception while updating trackball plist file:", e)
    finally:
        if not success:
            shutil.copyfile(bak, org)


## Configurator ##
def _configure_device_osx(button, action, modifiers=etypes.Modifier.NONE, **kwargs):
    """OS X specific configurator"""
    assert isinstance(button, etypes.Button)
    assert isinstance(action, etypes.ButtonAction)
    assert isinstance(modifiers, etypes.Modifier)
    if action.value < etypes.ButtonAction.MOUSE_AND_KEYBOARD_SENTINEL.value:
        _configure_mouse_and_keyboard_action(
            button,
            action,
            modifiers,
            kwargs['key'] if 'key' in kwargs else 0)
    elif action.value < etypes.ButtonAction.SNIPPETS_SENTINEL.value:
        raise NotImplementedError()
    else:
        raise NotImplementedError()


def _configure_device_windows(button, action, modifiers=etypes.Modifier.NONE, **kwargs):
    """Windows specific configurator"""
    assert isinstance(button, etypes.Button)
    assert isinstance(action, etypes.ButtonAction)
    assert isinstance(modifiers, etypes.Modifier)
    # TODO
    pass


def configure_device(button, action, modifiers=etypes.Modifier.NONE, **kwargs):
    """Configure the device's button with the action specified by the
    command argument.

    All it does is to delegate the call to the platform specific
    implementation.
    """
    assert isinstance(button, etypes.Button)
    assert isinstance(action, etypes.ButtonAction)
    assert isinstance(modifiers, etypes.Modifier)

    if sys.platform == 'darwin':
        _configure_device_osx(button, action, modifiers, **kwargs)
    else:
        _configure_device_windows(button, action, modifiers, **kwargs)


class EmulatorException(BaseException):
    def __repr__(self):
        return "EmulatorException"


class InitException(EmulatorException):
    def __repr__(self):
        return "InitException"


class ReadFailure(EmulatorException):
    def __repr__(self):
        return "ReadFailure"


class ReadTimeout(EmulatorException):
    def __repr__(self):
        return "ReadTimeout"


class WriteFailure(EmulatorException):
    def __repr__(self):
        return "WriteFailure"


class CommandAckNotReceived(EmulatorException):
    def __repr__(self):
        return "CommandAckNotReceived"


class CommandFailure(EmulatorException):
    def __repr__(self):
        return "CommandFailure"


class Emulator(object):
    """
    An abstraction of the TAU Emulator pointer device. Class is designed to be instantiated
    globally and then 'open'ed and 'close'd as desired.
    """

    WRITE_COMMAND_INTER_BYTE_DELAY = 0.005  # Delay between writing each byte of the 12 byte command.
                                            # 0.01 second or 10 milliseconds
    COMMAND_ACK_READ_TIMEOUT = 5            # Timeout before command ACK read is aborted.
                                            # 5 seconds
    INTER_COMMAND_DELAY = 0.04              # Delay between writing two successive commands.
                                            # 40 milliseconds

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.ser = None
        self.port = None
        if 'port' in kwargs:
            self.port = kwargs['port']

    def __del__(self):
        self.close()

    def open(self, port=None, max_wait_seconds=30):
        """
        Initializes the input device's click emulation firmware

        Params:
            port - the COM port to use. Specify com1, com2, com3, ...
            max_wait_seconds - maximum number of seconds to wait for the ready
                signature. Defaults to 30.

        Returns:
            bool - success indicator

        Throws:
            pyserial exceptions
        """
        if not port:
            port = self.port

        ser = serial.Serial(port, 115200, timeout=0)

        READY_SIGNATURE = bytearray(b'\xAA\x55\xFE\x00')
        buf = bytearray()
        start = time.monotonic()
        prevdot = start
        while True:

            if (time.monotonic()-prevdot) >= 1:
                print('.')
                prevdot = time.monotonic()

            ready_signal = ser.read()
            if len(ready_signal) > 0:
                buf.extend(ready_signal)
            if len(buf) >= len(READY_SIGNATURE):
                # look for READY_SIGNATURE in buf
                if buf.find(READY_SIGNATURE) >= 0:
                    print('Device init sequence received!')
                    self.ser = ser
                    return True

            # READY_SIGNATURE not yet received.
            # Yield our thread to OS and continue to read from the port
            if (time.monotonic() - start) > max_wait_seconds:
                break

        ser.close()
        return False

    def close(self):
        if self.ser:
            self.ser.close()
            self.ser = None

    def emulate_click(self, button):
        """
        Emulates a single click of the given etypes.Button. Single click constitutes of
        the Depress and Release actions of the etypes.Button.

        :param button: The button click to emulate. An instance of Button enum above
        :returns: None
        :raises: EmulatorException
        """
        assert self.ser != None
        self._emulate_button_action(button, False)
        time.sleep(self.INTER_COMMAND_DELAY)
        self._emulate_button_action(button, True)

    def emulate_double_click(self, button):
        assert self.ser != None
        pass

    def _emulate_button_action(self, button, up=False):
        """
        Parameters:
            button - Button to press/release. An instance of Button defined
                    above
            release - Whether to emulate button release. By default set to
                    False, calling this method causes the button press emulation.
                    Calling again with release=True, causes the last pressed
                    set of buttons to be released.

        Returns:
            None

        Throws:
            CommandAckNotReceived if the required ack is not read back.
        """
        assert isinstance(button, etypes.Button)
        assert isinstance(up, bool)

        # ###########################################################
        # Command is a sequence of 12 bytes, comprised of
        #
        #   bytes 0-1 - header: 0x55AA
        #   byte 2 - reserved:  0x00
        #   byte 3 - command:   0x01 - Mouse button event
        #                       0x02 - Keyboard event
        #   bytes 4-11 - data:  data bytes that depends on the preceding
        #                       command byte.
        # ###########################################################
        command = bytearray([0x55, 0xAA, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00])
        ACK_SUCCESS = bytes([0xAA, 0x55, 0x01, 0x00])
        # ACK_FAILURE = bytes([0xAA, 0x55, 0xFF, 0x00])

        # print("Emulating action on button", button)
        if not up:
            command[4] = button & 0xff

        self._write(command)

        # read back the necessary ACK
        result = self._read(4)

        if result != ACK_SUCCESS:
            raise CommandFailure()

    def _write(self, buf):
        """
        Writes bytes in buf, which is a bytearray, byte-by-byte.
        Writing the 12 byte command has to be done one byte at a time
        as the UART buffer does not seem to be able to keep up the fast
        speeds of modern CPUs. Therefore we write it byte at a time
        with a small delay (~10 milliseconds) between each write.

        :param buf: A bytearray buffer to write to device.

        :returns: None

        :raises: WriteFailure
        """
        try:
            for b in buf:
                self.ser.write(bytearray([b]))
                time.sleep(self.WRITE_COMMAND_INTER_BYTE_DELAY)
        except Exception:
            raise WriteFailure()

    def _read(self, count, timeout=-1):
        """
        Read count number of bytes from the device.

        :param count: Number of bytes to read
        :param timeout: Number of seconds to attempt reading, before timing out.
                Defaults to 5 seconds.
        :returns bytearray: The read value as a bytearray
        :raises: ReadTimeout or ReadFailure
        """
        if timeout < 0:
            timeout = self.COMMAND_ACK_READ_TIMEOUT

        buf = bytearray()
        start = time.monotonic()
        try:
            while True:
                data = self.ser.read()
                if len(data) > 0:
                    buf.extend(data)

                if len(buf) >= count:
                    return buf

                time.sleep(0)
                if (time.monotonic()-start) > timeout:
                    raise ReadTimeout()

        except Exception:
            raise ReadFailure()


def auto_detect_serial_port():
    """Tries to auto detect the serial port by using
    pyserial's list_ports module. From the available ports
    selects the first port with 'usbserial' or 'comX' prefix.

    Returns None if no serial ports with the requisite properties
    could be enumerated.
    """
    from serial.tools import list_ports
    # look for ports with 'usbserial' prefix (only on Mac)
    if sys.platform == 'darwin':
        ports = [p.device for p in list_ports.grep('usbserial')]
        if len(ports):
            return ports[0]

    # if not found, look for ports with 'com' prefix (on Windows)
    ports = [p.device for p in list_ports.grep('com')]
    if len(ports):
        return ports[0]

    return None


"""
File can be invoked from command line with the right arguments
to emulate button click/doubleclick actions.
"""
if __name__ == "__main__":
    #
    parser = argparse.ArgumentParser(description='Emulate trackball button clicks')
    parser.add_argument('--port', default='', help='Serial port to use to program the trackball. Defaults to first serial port detected')
    parser.add_argument('--button', type=int, default='3', help='Button to emulate the action on. {1...8}. Defaults to 3')
    parser.add_argument('--action', default='click', help='Action to emulate. {click|doubleclick}. Defaults to \'click\'')
    options, extra = parser.parse_known_args()
    port = options.port or auto_detect_serial_port()

    # print("Port:", port, "Button:", options.button, "action:", options.action)
    if not port:
        print("No serial port detected")
        parser.print_help()
        exit(1)

    # validate button number. Should be between 1 & 8
    if options.button not in list(range(1, 9)):
        print("Invalid button specified.")
        parser.print_help()
        exit(1)

    if options.action not in ['click', 'doubleclick']:
        print("Invalid action specified.")
        parser.print_help()
        exit(1)

    print("Waiting for device:", port)

    emu = Emulator()
    if not emu.open(port):
        print("Error initializing the emulator device")
        exit(1)

    try:
        print("Emulating {action} on button {button}..".format(action=options.action, button=options.button))
        emu.emulate_click(index_to_button(options.button-1))
        print("Action successfully emulated")
    except Exception as err:
        print("Exception while emulating button click: ", err)
