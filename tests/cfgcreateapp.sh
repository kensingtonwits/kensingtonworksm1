#!/bin/bash

source ./common_funcs.sh
# parse HELPER_ADDR & DEVICE_ID from environment variables
parse_common_args


NAME=$1
IDENTIFIER=$2

if [ "$NAME" == "" ] || [ "$IDENTIFIER" == "" ]; then
        echo "Create a new application configuration."
	echo "Usage:"
	echo "   $ cfgopen <name> <identifier>"
	echo
	echo "       name - A friendly name for the app (can be used in the GUI)"
	echo "       identifier - A unique identifier to the program. Usually this is the program"
	echo "                    binary/executable, with additional info such bundle-id in Mac."
	echo
	common_args_usage
	exit 1
fi

JSON="{\"name\":\"$NAME\", \"identifier\":\"$IDENTIFIER\"}"
dump_common_args
echo "Name:" $NAME
echo "Identifier:" $IDENTIFIER

curl --header "Content-Type: application/json" --request POST --data "$JSON" http://$HELPER_ADDR:9090/apps -i

