#!/bin/bash

source ./common_funcs.sh
# parse HELPER_ADDR & DEVICE_ID from environment variables
parse_common_args


BUTTON=$1
RESOURCE=$2 

if [ "$BUTTON" == "" ] || [ "$RESOURCE" == "" ]; then
        echo "Configure Trackball button to open an app/folder/website."
	echo "Usage:"
	echo "   $ cfgopen <button-mask> <url>"
	echo
	echo "       button-mask - Button mask to configure, 1~15"
	echo "       url - URL to open"
	echo
	common_args_usage
	exit 1
fi

JSON="{\"command\":\"launching_openURL\", \"params\": {\"path\":\"$RESOURCE\"}}"
dump_common_args
echo "Button:" $BUTTON
echo "App:" $RESOURCE

curl --header "Content-Type: application/json" --request POST --data "$JSON" http://$HELPER_ADDR:9090/config/*/$DEVICE_ID/buttons/$BUTTON -i

