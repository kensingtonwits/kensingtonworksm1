#!/bin/bash 
source ./common_funcs.sh
# parse HELPER_ADDR & DEVICE_ID from environment variables
parse_common_args

BUTTON=$1
MACRO=$2

if [ "$BUTTON" == "" ] || [ "$MACRO" == "" ]; then
    echo "Configure Trackball button with a single command macro without any args."
	echo "Usage:"
	echo "   $ cfgbutton <button-mask> <macro>"
	echo
	echo "       button-mask - 1~15. For chorded buttons, specify the combined mask."
	echo "			   For example, 3 is the mask for bottom two buttons chord"
	echo "			   and 15 is the mask for top two buttons chord."
	echo "       macro - Can be any macro command that does not rely on any dependeing"
    echo"              arguments. For instance, you can specify any of the following"
    echo "             for emulating the desired mouse clicks."
    echo "              {mouse_leftClick|mouse_rightClick|mouse_middleClick"
    echo "              |mouse_button4Click|mouse_button5Click"
    echo "              |mouse_leftDoubleClick|mouse_rightDoubleClick|"
    echo "              |mouse_middleDoubleClick|mouse_leftTripleClick}"
	echo
	common_args_usage
	exit 1
fi

dump_common_args
echo "Button: $BUTTON"
echo "Command:" $MACRO
JSON="{\"command\":\"$MACRO\"}"

curl --header "Content-Type: application/json" --request POST --data "$JSON" "http://$HELPER_ADDR:9090/config/buttons?device=$DEVICE_ID&app=*&button=$BUTTON" -i

