#!/bin/bash

source ./common_funcs.sh
# parse HELPER_ADDR & DEVICE_ID from environment variables
parse_common_args


dump_common_args

curl --header "Content-Type: application/json" "http://$HELPER_ADDR:9090/config/snippets?device=$DEVICE_ID&app=*" -i
# curl --header "Content-Type: application/json" --request POST --data "$JSON" "http://$HELPER_ADDR:9090/config/buttons?device=$DEVICE_ID&app=*&button=$BUTTON" -i

