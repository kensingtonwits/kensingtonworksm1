#!/bin/bash

source ./common_funcs.sh
# parse HELPER_ADDR & DEVICE_ID from environment variables
parse_common_args

IDENTIFIER=$1

if [ "$IDENTIFIER" == "" ]; then
        echo "Delete an existing application configuration."
	echo "Usage:"
	echo "   $ cfgopen <identifier>"
	echo
	echo "       identifier - The unique identifier to the program."
	echo
	common_args_usage
	exit 1
fi

JSON="{\"identifier\":\"$IDENTIFIER\"}"
dump_common_args
echo "Identifier:" $IDENTIFIER

curl --header "Content-Type: application/json" --request DELETE --data "$JSON" http://$HELPER_ADDR:9090/apps -i

