#!/bin/bash

function error_exit
{
    echo "$1" 1>&2
    exit 1
}

# check if we're being run as root
function check_root
{
    if [ "$EUID" -ne 0 ]; then
        echo "Please run as root using sudo"
        exit
    fi
}

check_root

# Kill any GUI processes
killall KensingtonWorks2 2> /dev/null

# Stop Helper launch-agent.
CONSOLE_USER=$(stat -f%Su /dev/console)
echo $CONSOLE_USER
su -l $CONSOLE_USER -c '/bin/launchctl unload /Library/LaunchAgents/com.kensington.trackballworks.plist' 2> /dev/null

rm -rf /Applications/KensingtonWorks\ .app 2> /dev/null
rm -rf /Applications/KensingtonWorks2.app 2> /dev/null
rm -rf /Applications/Utilities/KensingtonWorks\ Uninstaller.app 2> /dev/null
rm -rf /Library/PreferencePanes/KensingtonWorks.prefPane 2> /dev/null
rm -rf /Library/Extensions/trackballworks2.kext 2> /dev/null
rm -f /Library/LaunchAgents/com.kensington.trackballworks.plist 2> /dev/null

# restart the system
shutdown -r now
