'''
Tests that are common between Windows & OSX
'''
import sys

import requests
import json

from core import *

class TestPointerSettings(TrackballTest):
    '''Tests that pointer settings can be set & read back'''

    def test_pointer_settings_are_set(self):
        """Tests that pointer settings are persisted."""
        myurl = self.get_helper_address() + "/config/pointer?app=*&device={0}".format(
            self.get_device_id()
        )
        response = requests.get(myurl)
        self.assertEqual(response.status_code, 200)
        response = requests.post(myurl, data=json.dumps({
            'acceleration': False,
            'accelerationRate': 2,
            'lockedAxisModifiers': "ctrl,shift",
            'slowModifiers': "alt,ctrl",
            'speed': 100
        }))
        self.assertEqual(response.status_code, 200)
        response = requests.get(myurl)
        self.assertEqual(response.status_code, 200)
        new_settings = response.json()
        self.assertEqual(new_settings['acceleration'], False)
        self.assertEqual(new_settings['accelerationRate'], 2)
        self.assertEqual(new_settings['lockedAxisModifiers'], "ctrl,shift")
        self.assertEqual(new_settings['slowModifiers'], "alt,ctrl")
        self.assertEqual(new_settings['speed'], 100)


class TestScrollSettings(TrackballTest):
    '''Tests that scroll settings can be set & read back'''

    def test_scroll_settings_are_set(self):
        """Tests that scroll settings are persisted."""
        myurl = self.get_helper_address() + "/config/scroll?app=*&device={0}".format(
            self.get_device_id()
        )
        response = requests.get(myurl)
        cur_settings = response.json()
        self.assertEqual(response.status_code, 200)
        response = requests.post(myurl, data=json.dumps({
            'inertial': not cur_settings["inertial"],
            'invert': not cur_settings["invert"],
            'speed': cur_settings["speed"] + 1
        }))
        self.assertEqual(response.status_code, 200)
        response = requests.get(myurl)
        self.assertEqual(response.status_code, 200)
        new_settings = response.json()
        self.assertEqual(new_settings['inertial'], not cur_settings["inertial"])
        self.assertEqual(new_settings['invert'], not cur_settings["invert"])
        self.assertEqual(new_settings['speed'], cur_settings["speed"] + 1)


class TestAppAPI(TrackballTest):
    '''Tests app CRUD API'''

    def _get_custom_app(self):
        if sys.platform == 'win32':
            app_id = 'c:\\windows\\system32\\notepad.exe'
            app_name = 'Notepad'
        else:
            app_id = '/Applications/TextEdit.app'
            app_name = 'TextEdit'
        return (app_id, app_name)

    def test_get_apps(self):
        myurl = self.get_helper_address() + "/config/apps?device={0}".format(
            self.get_device_id()
        )
        response = requests.get(myurl)
        apps = response.json()
        self.assertTrue( {'*':'*'} in apps['apps'])

    def test_create_app(self):
        '''Tests create new app API'''
        myurl = self.get_helper_address() + "/config/apps?device={0}".format(
            self.get_device_id()
        )
        custom_app = self._get_custom_app()
        response = requests.post(myurl, data=json.dumps({
            'name': custom_app[1],
            'identifier': custom_app[0]
        }))
        response = requests.get(myurl)
        self.assertEqual(response.status_code, 200)
        apps = response.json()
        # creation should return the entire app list
        self.assertTrue( {'*':'*'} in apps['apps'])
        self.assertTrue({custom_app[0]: custom_app[1]} in apps['apps'])

    def test_delete_app(self):
        '''Tests delete an existing app API'''
        myurl = self.get_helper_address() + "/config/apps?device={0}".format(
            self.get_device_id()
        )
        custom_app = self._get_custom_app()
        response = requests.post(myurl, data=json.dumps({
            'name': custom_app[1],
            'identifier': custom_app[0]
        }))
        response = requests.get(myurl)
        self.assertEqual(response.status_code, 200)
        apps = response.json()
        self.assertTrue( {'*':'*'} in apps['apps'])
        self.assertTrue({custom_app[0]: custom_app[1]} in apps['apps'])

        response = requests.delete(myurl, data=json.dumps({
            'identifier': custom_app[0]
        }))
        apps = response.json()
        self.assertTrue( {'*':'*'} in apps['apps'])
        self.assertFalse({custom_app[0]: custom_app[1]} in apps['apps'])

    def test_create_existing_app_is_disallowed(self):
        myurl = self.get_helper_address() + "/config/apps?device={0}".format(
            self.get_device_id()
        )
        custom_app = self._get_custom_app()
        response = requests.post(myurl, data=json.dumps({
            'name': custom_app[1],
            'identifier': custom_app[0]
        }))
        response = requests.get(myurl)
        self.assertEqual(response.status_code, 200)
        apps = response.json()
        self.assertTrue( {'*':'*'} in apps['apps'])
        self.assertTrue({custom_app[0]: custom_app[1]} in apps['apps'])

        response = requests.post(myurl, data=json.dumps({
            'name': custom_app[1],
            'identifier': custom_app[0]
        }))
        apps = response.json()
        self.assertEqual(response.status_code, 400)

    def test_delete_global_app_is_disallowed(self):
        myurl = self.get_helper_address() + "/config/apps?device={0}".format(
            self.get_device_id()
        )
        response = requests.delete(myurl, data=json.dumps({
            'identifier': '*'
        }))
        self.assertEqual(response.status_code, 400)


class TestKeystroke(TestKeystrokeBase):
    '''Tests various keystrokes with & without various combinations of
    modifier keys.'''

    def test_keystroke_without_modifer(self):
        """Tests that keystroke (without any modifier key) is correctly
        inserted into the system input queue."""
        self._test_keystroke_raw(
            Button.Button3,
            get_tbw_key_code('e' if sys.platform == "darwin" else 'E'),
            keyboard.KeyCode.from_char('e'),
            modifiers='')

    def test_keystroke_with_modifer_ctrl(self):
        """Tests that keystroke with modifier key is correctly inserted into
        the system input queue."""
        self._test_keystroke_raw(
            Button.Button3,
            get_tbw_key_code('e' if sys.platform == "darwin" else 'E'),
            keyboard.KeyCode.from_char('e'),
            modifiers='ctrl')

    def test_keystroke_with_modifer_alt(self):
        """Tests that keystroke with modifier key is correctly inserted into
        the system input queue."""
        self._test_keystroke_raw(
            Button.Button3,
            get_tbw_key_code('e' if sys.platform == "darwin" else 'E'),
            keyboard.KeyCode.from_char('E' if sys.platform == "darwin" else 'e'),
            modifiers='alt')

    def test_keystroke_with_modifer_shift(self):
        """Tests that keystroke with modifier key is correctly inserted into
        the system input queue."""
        self._test_keystroke_raw(
            Button.Button3,
            get_tbw_key_code('e' if sys.platform == "darwin" else 'E'),
            keyboard.KeyCode.from_char('E' if sys.platform == "darwin" else 'e'),
            modifiers='shift')


class TestButtons(TrackballTest):

    def test_get_all(self):
        '''Tests the API to get/set all buttons config by specifying button=0 in the URL'''
        myurl = self.get_helper_address() + "/config/buttons?app=*&device={0}&button=0".format(
            self.get_device_id()
        )
        response = requests.get(myurl)
        self.assertEqual(response.status_code, 200)
        button_settings = response.json()
        response = requests.post(myurl, json=button_settings)
        self.assertEqual(response.status_code, 200)

    def test_button3_left_click(self):
        """Tests that Trackball button 3 emulates mouse left button click"""
        # configure button 3 for left click
        self.configure_button_action(Button.Button3, command='mouse_leftClick')
        self.assertTrue(wait_for_mouse_button_click(Button.Button3, mouse.Button.left))

    def test_button3_left_click_with_ctrl_alt(self):
        """Tests that Trackball button 3 emulates mouse left button click with ALT & CTRL"""
        params = {
            "modifiers": "alt,ctrl"
        }
        self.configure_button_action(Button.Button3, command='mouse_leftClick', params=params)
        self.assertTrue(wait_for_mouse_button_click_with_modifiers(
            Button.Button3, mouse.Button.left, params['modifiers']
        ))

    def test_button3_left_click_with_shift_alt(self):
        """Tests that Trackball button 3 emulates mouse left button click with SHIFT & ALT"""
        params = {
            "modifiers": "shift,alt"
        }
        self.configure_button_action(Button.Button3, command='mouse_leftClick', params=params)
        self.assertTrue(wait_for_mouse_button_click_with_modifiers(
            Button.Button3, mouse.Button.left, params['modifiers']
        ))

    def test_button3_left_click_with_win_alt(self):
        """Tests that Trackball button 3 emulates mouse left button click with WIN & ALT"""
        if sys.platform == "win32":
            params = {
                "modifiers": "win,ctrl"
            }
        else:
            params = {
                "modifiers": "cmd,ctrl"
            }

        self.configure_button_action(Button.Button3, command='mouse_leftClick', params=params)
        self.assertTrue(wait_for_mouse_button_click_with_modifiers(
            Button.Button3, mouse.Button.left, params['modifiers']
        ))

    def test_button3_left_click_with_alt(self):
        """Tests that Trackball button 3 emulates mouse left button click with ALT"""
        params = {
            "modifiers": "alt"
        }
        self.configure_button_action(Button.Button3, command='mouse_leftClick', params=params)
        self.assertTrue(wait_for_mouse_button_click_with_modifiers(
            Button.Button3, mouse.Button.left, params['modifiers']
        ))

    def test_button3_right_click(self):
        """Tests that Trackball button 3 emulates mouse right button click"""
        # configure button 3 for right click
        self.configure_button_action(Button.Button3, command='mouse_rightClick')
        self.assertTrue(wait_for_mouse_button_click(Button.Button3, mouse.Button.right))

    def test_button3_right_click_with_modifiers(self):
        """Tests that Trackball button 3 emulates mouse right button click"""
        # configure button 3 for right click
        params = {
            "modifiers": "shift,alt"
        }
        self.configure_button_action(Button.Button3, command='mouse_rightClick', params=params)
        self.assertTrue(wait_for_mouse_button_click_with_modifiers(
            Button.Button3, mouse.Button.right, params['modifiers'])
        )

    def test_button3_middle_click(self):
        """Tests that Trackball button 3 emulates mouse left button click"""
        # configure button 3 for left click
        self.configure_button_action(Button.Button3, command='mouse_middleClick')
        self.assertTrue(wait_for_mouse_button_click(Button.Button3, mouse.Button.middle))

    def test_swap_left_right_buttons(self):
        # Assign button 1 to right-click
        self.configure_button_action(Button.Button1, command='mouse_rightClick')
        # Assign button 2 to left-click
        self.configure_button_action(Button.Button2, command='mouse_leftClick')
        self.assertTrue(wait_for_mouse_button_click(Button.Button1, mouse.Button.right))
        self.assertTrue(wait_for_mouse_button_click(Button.Button2, mouse.Button.left))

    def test_button3_double_click(self):
        """Tests that Trackball button 3 emulates mouse left button double click"""
        # configure button 3 for left button double click
        self.configure_button_action(Button.Button3, command='mouse_leftDoubleClick')
        self.assertTrue(wait_for_mouse_button_click(Button.Button3, mouse.Button.left, count=2))

    def test_button3_double_click_with_ctrl_alt(self):
        """Tests that Trackball button 3 emulates mouse left button double click with CTRL & ALT"""
        params = {
            "modifiers": "ctrl,alt"
        }
        self.configure_button_action(Button.Button3, command='mouse_leftDoubleClick', params=params)
        self.assertTrue(wait_for_mouse_button_click_with_modifiers(
            Button.Button3, mouse.Button.left, params['modifiers'], count=2
        ))

    def test_button4_double_click(self):
        """Tests that Trackball button 3 emulates mouse left button double click"""
        # configure button 4 for left button double click
        self.configure_button_action(Button.Button4, command='mouse_leftDoubleClick')
        self.assertTrue(wait_for_mouse_button_click(Button.Button4, mouse.Button.left, count=2))

    def test_button1_double_click(self):
        """Tests that Trackball button 3 emulates mouse left button double click"""
        # configure button 4 for left button double click
        self.configure_button_action(Button.Button1, command='mouse_leftDoubleClick')
        self.assertTrue(wait_for_mouse_button_click(Button.Button1, mouse.Button.left, count=2))

    def test_button3_right_double_click(self):
        """Tests that Trackball button 3 emulates mouse right button double click"""
        # configure button 3 for left button double click
        self.configure_button_action(Button.Button3, command='mouse_rightDoubleClick')
        self.assertTrue(wait_for_mouse_button_click(Button.Button3, mouse.Button.right, count=2))

    def test_button3_right_double_click_with_shift_alt(self):
        """Tests that Trackball button 3 emulates mouse right button double click with SHIFT & ALT"""
        params = {
            "modifiers": "shift,alt"
        }
        self.configure_button_action(Button.Button3, command='mouse_rightDoubleClick', params=params)
        self.assertTrue(wait_for_mouse_button_click_with_modifiers(
            Button.Button3, mouse.Button.right, params['modifiers'], count=2
        ))

    def test_button3_middle_double_click(self):
        """Tests that Trackball button 3 emulates mouse middle button double click"""
        # For some reason Mac doesn't forward middle button double clicks
        # So disabling the test on Mac
        # configure button 3 for left button double click
        self.configure_button_action(Button.Button3, command='mouse_middleDoubleClick')
        self.assertTrue(wait_for_mouse_button_click(Button.Button3, mouse.Button.middle, count=2))

    def test_button3_middle_double_click_with_alt_shift(self):
        """Tests that Trackball button 3 emulates mouse middle button double click with SHIFT & ALT"""
        # For some reason Mac doesn't forward middle button double clicks
        # So disabling the test on Mac
        params = {
            "modifiers": "shift,alt"
        }
        # configure button 3 for left button double click
        self.configure_button_action(Button.Button3, command='mouse_middleDoubleClick', params=params)
        self.assertTrue(wait_for_mouse_button_click_with_modifiers(
            Button.Button3, mouse.Button.middle, params['modifiers'], count=2))

    def test_triple_click(self):
        """Tests that Trackball button 3 emulates mouse left button triple click"""
        # configure button 3 for left button triple click
        self.configure_button_action(Button.Button3, command='mouse_leftTripleClick')
        self.assertTrue(wait_for_mouse_button_click(Button.Button3, mouse.Button.left, count=3))

    def test_button3_triple_click_with_shift_alt(self):
        """Tests that Trackball button 3 emulates mouse left triple click with SHIFT & ALT"""
        params = {
            "modifiers": "shift,alt"
        }
        self.configure_button_action(Button.Button3, command='mouse_leftTripleClick', params=params)
        self.assertTrue(wait_for_mouse_button_click_with_modifiers(
            Button.Button3, mouse.Button.left, params['modifiers'], count=3
        ))


class TestEditing(TestKeystrokeBase):

    DEF_MODIFIER = 'ctrl' if sys.platform == 'win32' else 'cmd'

    def test_editing_undo(self):
        '''Test editing undo action'''
        keyCode = keyboard.KeyCode.from_char('z')
        self._test_keystroke_command(Button.Button3, 'editing_undo', keyCode, modifiers=self.DEF_MODIFIER)

    def test_editing_redo(self):
        '''Test editing redo action'''
        keyCode = keyboard.KeyCode.from_char('y') if sys.platform == 'win32' else keyboard.KeyCode.from_char('z')
        modifiers='ctrl' if sys.platform=='win32' else 'cmd,shift'
        self._test_keystroke_command(Button.Button3, 'editing_redo', keyCode, modifiers=modifiers)

    def test_editing_copy(self):
        '''Tests system clipboard copy action'''
        keycode = keyboard.KeyCode.from_char('c')
        self._test_keystroke_command(Button.Button3, 'editing_copy', keycode, modifiers=self.DEF_MODIFIER)

    def test_editing_cut(self):
        '''Tests system clipboard cut action'''
        keycode = keyboard.KeyCode.from_char('x')
        self._test_keystroke_command(Button.Button3, 'editing_cut', keycode, modifiers=self.DEF_MODIFIER)

    def test_editing_paste(self):
        '''Tests system clipboard paste action'''
        keycode = keyboard.KeyCode.from_char('v')
        self._test_keystroke_command(Button.Button3, 'editing_paste', keycode, modifiers=self.DEF_MODIFIER)

    def test_editing_select_all(self):
        '''Tests Editing -> Select All'''
        keycode = keyboard.KeyCode.from_char('a')
        self._test_keystroke_command(Button.Button3, 'editing_selectAll', keycode, modifiers=self.DEF_MODIFIER)

    def test_editing_find(self):
        '''Tests Editing -> Find'''
        keycode = keyboard.KeyCode.from_char('f')
        self._test_keystroke_command(Button.Button2, 'editing_find', keycode, modifiers=self.DEF_MODIFIER)


class TestSaveRestoreSettings(TrackballTest):

    def _load_settings_file(self, fileName):
        '''Loads the given json file and returns it as a python object'''
        with open(fileName, 'r') as sf:
            return json.load(sf)
        return None

    def test_import_invalid_settings_fails(self):
        '''Tests that when a JSON with invalid content is imported, import
        fails with response code=400 & tbwhelper continues to run.'''
        settings = self._load_settings_file('TbwSettings_invalid.json')
        self.assertIsNotNone(settings)
        myurl = self.get_helper_address() + "/config"
        response = requests.post(myurl, data=json.dumps(settings))
        self.assertEqual(response.status_code, 400)

    def test_import_empty_device_settings(self):
        '''Tests that when a setting with empty device list is imported,
        default settings for connected devices are automatically created.'''
        settings = self._load_settings_file(
            'TbwSettings_empty_win.json' if sys.platform == 'win32' else 'TbwSettings_empty_osx.json')
        self.assertIsNotNone(settings)
        myurl = self.get_helper_address() + "/config"
        response = requests.post(myurl, data=json.dumps(settings))
        self.assertEqual(response.status_code, 200)


if __name__ == '__main__':
    tbw_tests_main()
