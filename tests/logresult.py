import datetime
import unittest

class LogResults(object):

    '''A test results class that logs test status to test.log'''
    def __init__(self, *args, **kwargs):
        self.my_total = 0
        self.my_failed = 0
        self.my_errored = 0
        self.my_skipped = 0
        self.my_start_time = 0
        self.my_errorInfo = None

    def _log_message(self, msg):
        with open("test.log", "a") as log:
            log.write(msg)

    def _getTestDescription(self, test):
        doc_first_line = test.shortDescription() or ''
        return '\n'.join((str(test), doc_first_line))

    def logStartTestRun(self):
        self.my_total = 0
        self.my_failed = 0
        self.my_errored = 0
        self.my_skipped = 0
        self.my_start_time = datetime.datetime.now()
        filename = "test_{0}.log".format(
            self.my_start_time.strftime("%Y-%m-%d_%H-%M-%S"))
        with open(filename, "w") as log:
            log.write("Starting TBW tests @ {0}\n".format(datetime.datetime.now()))

    def logStopTestRun(self):
        with open("test.log", "a") as log:
            duration = datetime.datetime.now()-self.my_start_time
            log.write("Finished in {0} secs\n".format(duration.seconds))
            log.write("TEST STATISTICS:\n")
            log.write("\tRun: {0}, Success: {1}, Fail: {2}, Error: {3}, Skipped: {4}\n".format(
                self.my_total,
                self.my_total-(self.my_failed+self.my_errored+self.my_skipped),
                self.my_failed,
                self.my_errored,
                self.my_skipped))

    def logStartTest(self, test):
        self.my_total += 1
        self._log_message(self._getTestDescription(test) + '...')

    def logSuccess(self, test):
        self._log_message("ok\n")

    def logFailure(self, test, err):
        self._log_message("FAIL\n")
        self.my_failed += 1

    def logError(self, test, err):
        self._log_message("ERROR\n")
        self.my_errored += 1

    def logSkip(self, test, reason):
        self._log_message("skipped {0!r}\n".format(reason))
        self.my_skipped += 1
