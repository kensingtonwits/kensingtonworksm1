"""
Enumerated types shared between Mac & Windows code. Some enum values are
applicable only to a specific platform. These are appropriately marked with
the platform prefix (MAC or WIN)
"""
import enum

class Button(enum.IntEnum):
    """
    Definitions of the various buttons on the trackball. We're going to
    use location instead of 1, 2,..4 so that it's clear which is being
    programmed in the unit test.
    """
    Button1 = 1
    Button2 = 2
    Button3 = 4
    Button4 = 8
    Button5 = 16
    Button6 = 32
    Button7 = 64
    Button8 = 128
    ButtonChording12 = 3
    ButtonChording34 = 12


class Modifier(enum.IntFlag):
    """
    Symbolic names for modifier keys. These values map directly to
    modifier=<value> in TbwSettings.xml' .
    """
    NONE = 0
    WIN_SHIFT = 1
    WIN_CTRL = 4
    WIN_ALT = 16
    WIN_WINDOWS = 64

    MAC_COMMAND = 256
    MAC_SHIFT = 512
    MAC_OPTION = 2048
    MAC_CTRL = 4096


class ButtonAction(enum.Enum):
    NO_ACTION = enum.auto()
    MOUSE_AND_KEYBOARD_KEYBOARD_SHORTCUT = enum.auto()
    MOUSE_AND_KEYBOARD_CLICK = enum.auto()
    MOUSE_AND_KEYBOARD_RIGHT_CLICK = enum.auto()
    MOUSE_AND_KEYBOARD_MIDDLE_CLICK = enum.auto()
    MOUSE_AND_KEYBOARD_DOUBLE_CLICK = enum.auto()
    MOUSE_AND_KEYBOARD_DOUBLE_RIGHT_CLICK = enum.auto()
    MOUSE_AND_KEYBOARD_DOUBLE_MIDDLE_CLICK = enum.auto()
    MOUSE_AND_KEYBOARD_TRIPLE_CLICK = enum.auto()
    MOUSE_AND_KEYBOARD_DRAG = enum.auto()
    MOUSE_AND_KEYBOARD_RIGHT_DRAG = enum.auto()
    MOUSE_AND_KEYBOARD_MIDDLE_DRAG = enum.auto()
    MOUSE_AND_KEYBOARD_SENTINEL = enum.auto()

    SNIPPETS_SNIPPETS_MENU = enum.auto()
    SNIPPETS_SNIPPET_1 = enum.auto()
    SNIPPETS_SNIPPET_2 = enum.auto()
    SNIPPETS_SNIPPET_3 = enum.auto()
    SNIPPETS_SNIPPET_4 = enum.auto()
    SNIPPETS_SNIPPET_5 = enum.auto()
    SNIPPETS_SNIPPET_6 = enum.auto()
    SNIPPETS_SNIPPET_7 = enum.auto()
    SNIPPETS_SNIPPET_8 = enum.auto()
    SNIPPETS_SNIPPET_9 = enum.auto()
    SNIPPETS_SNIPPET_10 = enum.auto()
    SNIPPETS_SENTINEL = enum.auto()


"""
629 2563959461
N95WVM

Taken from /System/Library/Frameworks/Carbon.framework/Versions/A/Frameworks/HIToolbox.framework/Versions/A/Headers/Events.h

These constants are the virtual keycodes defined originally in
Inside Mac Volume V, pg. V-191. They identify physical keys on a
keyboard. Those constants with "ANSI" in the name are labeled
according to the key position on an ANSI-standard US keyboard.
For example, kVK_ANSI_A indicates the virtual keycode for the key
with the letter 'A' in the US keyboard layout. Other keyboard
layouts may have the 'A' key label on a different physical key;
in this case, pressing 'A' will generate a different virtual
keycode.

TODO: Need to define this for Windows as well
"""
class KeyCode(enum.IntEnum):
    VK_ANSI_A = 0x00
    VK_ANSI_S = 0x01
    VK_ANSI_D = 0x02
    VK_ANSI_F = 0x03
    VK_ANSI_H = 0x04
    VK_ANSI_G = 0x05
    VK_ANSI_Z = 0x06
    VK_ANSI_X = 0x07
    VK_ANSI_C = 0x08
    VK_ANSI_V = 0x09
    VK_ANSI_B = 0x0B
    VK_ANSI_Q = 0x0C
    VK_ANSI_W = 0x0D
    VK_ANSI_E = 0x0E
    VK_ANSI_R = 0x0F
    VK_ANSI_Y = 0x10
    VK_ANSI_T = 0x11
    VK_ANSI_1 = 0x12
    VK_ANSI_2 = 0x13
    VK_ANSI_3 = 0x14
    VK_ANSI_4 = 0x15
    VK_ANSI_6 = 0x16
    VK_ANSI_5 = 0x17
    VK_ANSI_EQUAL = 0X18
    VK_ANSI_9 = 0X19
    VK_ANSI_7 = 0X1A
    VK_ANSI_MINUS = 0X1B
    VK_ANSI_8 = 0X1C
    VK_ANSI_0 = 0X1D
    VK_ANSI_RIGHTBRACKET = 0X1E
    VK_ANSI_O = 0X1F
    VK_ANSI_U = 0X20
    VK_ANSI_LEFTBRACKET = 0X21
    VK_ANSI_I = 0X22
    VK_ANSI_P = 0X23
    VK_ANSI_L = 0X25
    VK_ANSI_J = 0X26
    VK_ANSI_QUOTE = 0X27
    VK_ANSI_K = 0X28
    VK_ANSI_SEMICOLON = 0X29
    VK_ANSI_BACKSLASH = 0X2A
    VK_ANSI_COMMA = 0X2B
    VK_ANSI_SLASH = 0X2C
    VK_ANSI_N = 0X2D
    VK_ANSI_M = 0X2E
    VK_ANSI_PERIOD = 0X2F
    VK_ANSI_GRAVE = 0X32
    VK_ANSI_KEYPADDECIMAL = 0X41
    VK_ANSI_KEYPADMULTIPLY = 0X43
    VK_ANSI_KEYPADPLUS = 0X45
    VK_ANSI_KEYPADCLEAR = 0X47
    VK_ANSI_KEYPADDIVIDE = 0X4B
    VK_ANSI_KEYPADENTER = 0X4C
    VK_ANSI_KEYPADMINUS = 0X4E
    VK_ANSI_KEYPADEQUALS = 0X51
    VK_ANSI_KEYPAD0 = 0X52
    VK_ANSI_KEYPAD1 = 0X53
    VK_ANSI_KEYPAD2 = 0X54
    VK_ANSI_KEYPAD3 = 0X55
    VK_ANSI_KEYPAD4 = 0X56
    VK_ANSI_KEYPAD5 = 0X57
    VK_ANSI_KEYPAD6 = 0X58
    VK_ANSI_KEYPAD7 = 0X59
    VK_ANSI_KEYPAD8 = 0X5B
    VK_ANSI_KEYPAD9 = 0X5C

    # keycodes for keys that are independent of keyboard layout
    VK_RETURN = 0X24,
    VK_TAB = 0X30,
    VK_SPACE = 0X31,
    VK_DELETE = 0X33,
    VK_ESCAPE = 0X35,
    VK_COMMAND = 0X37,
    VK_SHIFT = 0X38,
    VK_CAPSLOCK = 0X39,
    VK_OPTION = 0X3A,
    VK_CONTROL = 0X3B,
    VK_RIGHTSHIFT = 0X3C,
    VK_RIGHTOPTION = 0X3D,
    VK_RIGHTCONTROL = 0X3E,
    VK_FUNCTION = 0X3F,
    VK_F17 = 0X40,
    VK_VOLUMEUP = 0X48,
    VK_VOLUMEDOWN = 0X49,
    VK_MUTE = 0X4A,
    VK_F18 = 0X4F,
    VK_F19 = 0X50,
    VK_F20 = 0X5A,
    VK_F5 = 0X60,
    VK_F6 = 0X61,
    VK_F7 = 0X62,
    VK_F3 = 0X63,
    VK_F8 = 0X64,
    VK_F9 = 0X65,
    VK_F11 = 0X67,
    VK_F13 = 0X69,
    VK_F16 = 0X6A,
    VK_F14 = 0X6B,
    VK_F10 = 0X6D,
    VK_F12 = 0X6F,
    VK_F15 = 0X71,
    VK_HELP = 0X72,
    VK_HOME = 0X73,
    VK_PAGEUP = 0X74,
    VK_FORWARDDELETE = 0X75,
    VK_F4 = 0X76,
    VK_END = 0X77,
    VK_F2 = 0X78,
    VK_PAGEDOWN = 0X79,
    VK_F1 = 0X7A,
    VK_LEFTARROW = 0X7B,
    VK_RIGHTARROW = 0X7C,
    VK_DOWNARROW = 0X7D,
    VK_UPARROW = 0X7E
