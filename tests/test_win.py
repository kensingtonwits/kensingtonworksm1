# coding=utf-8
import sys
import requests
import json

from core import *
from test_common import *

#   TrackballTest, self.get_helper_address(), self.get_device_id(), \
#    wait_for_keyboard_input, wait_for_mouse_button_click, \
#    wait_for_mouse_button_click_with_modifiers, mouse, keyboard, Button
import win32gui, win32con, win32process


def string_to_keycodes(s, xlate_uppercase_to_shift=False):
    """Converts a string into an array of pynput.keyboard.KeyCode elements.
    If xlate_uppercase_to_shift is set to True, string is parsed for uppercase
    characters and for each of them a corresponding Shift DOWN/UP keystrokes
    are not added to the returned array, before inserting the keycode for the
    corresponding character's lowercase value.
    """
    keycodes = []
    shift_down = False
    for c in s:
        if xlate_uppercase_to_shift:
            if c.isupper() and not shift_down:
                keycodes.append((keyboard.Key.shift_l, True))
                shift_down = True
            elif c.islower() and shift_down:
                keycodes.append((keyboard.Key.shift_l, False))
                shift_down = False

        if c == ' ':
            keycode = keyboard.Key.space if xlate_uppercase_to_shift else keyboard.KeyCode.from_char(' ')
        else:
            keycode = keyboard.KeyCode.from_char(
                c.lower() if xlate_uppercase_to_shift else c)
        keycodes.append((keycode, True))
        keycodes.append((keycode, False))

    return keycodes


class TestBrowser(TestKeystrokeBase):

    def test_browser_navigate_backwards(self):
        self._test_keystroke_command(Button.Button3, 'browser_back', keyboard.KeyCode.from_vk(VK.BROWSER_BACK), modifiers='')

    def test_browser_navigate_forward(self):
        self._test_keystroke_command(Button.Button3, 'browser_forward', keyboard.KeyCode.from_vk(VK.BROWSER_FORWARD), modifiers='')

    def test_browser_refresh_page(self):
        self._test_keystroke_command(Button.Button3, 'browser_refresh', keyboard.KeyCode.from_vk(VK.BROWSER_REFRESH), modifiers='')

    def test_browser_stop_loading(self):
        self._test_keystroke_command(Button.Button3, 'browser_stop', keyboard.KeyCode.from_vk(VK.BROWSER_STOP), modifiers='')


class TestNavigation(TestKeystrokeBase):

    # Scroll Up/Down are now handled in the driver via scroll
    # mouse events (Ergo Vertical TB support). So comment out.
    # def test_navigation_scroll_up(self):
    #     self._test_keystroke_command(Button.Button3, 'navigation_scrollUp', keyboard.Key.up, modifiers='')

    # def test_navigation_scroll_down(self):
    #     self._test_keystroke_command(Button.Button3, 'navigation_scrollDown', keyboard.Key.down, modifiers='')

    def test_navigation_page_up(self):
        self._test_keystroke_command(Button.Button3, 'navigation_pageUp', keyboard.Key.page_up, modifiers='')

    def test_navigation_page_down(self):
        self._test_keystroke_command(Button.Button3, 'navigation_pageDown', keyboard.Key.page_down, modifiers='')


class TestMedia(TestKeystrokeBase):
    '''Tests media navigation & volume control shortcuts'''
    def test_media_play_pause(self):
        self._test_keystroke_command(Button.Button3, 'media_playPause', keyboard.KeyCode.from_vk(VK.MEDIA_PLAY_PAUSE), modifiers='')

    def test_media_stop(self):
        self._test_keystroke_command(Button.Button3, 'media_stop', keyboard.KeyCode.from_vk(VK.MEDIA_STOP), modifiers='')

    def test_media_next_track(self):
        self._test_keystroke_command(Button.Button3, 'media_nextTrack', keyboard.KeyCode.from_vk(VK.MEDIA_NEXT_TRACK), modifiers='')

    def test_media_previous_track(self):
        self._test_keystroke_command(Button.Button3, 'media_prevTrack', keyboard.KeyCode.from_vk(VK.MEDIA_PREV_TRACK), modifiers='')

    def test_media_volume_up(self):
        self._test_keystroke_command(Button.Button3, 'media_volumeUp', keyboard.KeyCode.from_vk(VK.VOLUME_UP), modifiers='')

    def test_media_volume_down(self):
        self._test_keystroke_command(Button.Button3, 'media_volumeDown', keyboard.KeyCode.from_vk(VK.VOLUME_DOWN), modifiers='')

    def test_media_mute(self):
        self._test_keystroke_command(Button.Button3, 'media_mute', keyboard.KeyCode.from_vk(VK.VOLUME_MUTE), modifiers='')


class TestVirtualDesktop(TestKeystrokeBase):
    '''Tests virtual desktop shortcuts'''

    def test_virtual_desktop_create_new(self):
        self._test_keystroke_command(Button.Button3, 'windowSnapping_newVirtualDesktop', keyboard.KeyCode.from_char('d'), modifiers='ctrl,win')

    def test_virtual_desktop_close_current(self):
        self._test_keystroke_command(Button.Button3, 'windowSnapping_closeVirtualDesktop', keyboard.Key.f4, modifiers='ctrl,win')

    def test_virtual_desktop_goto_left(self):
        self._test_keystroke_command(Button.Button3, 'windowSnapping_switchVirtualDesktopLeft', keyboard.Key.left, modifiers='ctrl,win')

    def test_virtual_desktop_goto_right(self):
        self._test_keystroke_command(Button.Button3, 'windowSnapping_switchVirtualDesktopRight', keyboard.Key.right, modifiers='ctrl,win')


class TestSystem(TestKeystrokeBase):
    '''
    Capture screen to Clipboard	command: "keystroke", keyCode: <vk_snapshot>
    Task Manager	command: "keystroke", keyCode: <vk_esc>, modifiers: "ctrl,shift"
    Lock Screen	command: "keystroke", keyCode: <vk_l>, modifiers: "win"
    Open Explorer Window	command: "keystroke", keyCode: <vk_e>, modifiers: "win"
    Show Run Dialog	command: "keystroke", keyCode: <vk_r>, modifiers: "win"
    Show System Search	command: "keystroke", keyCode: <vk_s>, modifiers: "win"
    Task view	command: "keystroke", keyCode: <vk_tab>, modifiers: "win"
    '''
    def test_system_capture_screen_to_clipboard(self):
        self._test_keystroke_command(Button.Button3, 'system_captureScreenToClipboard', keyboard.Key.print_screen, modifiers='')

    def test_system_open_task_manager(self):
        if sys.platform != "win32":
            return

        self.configure_button_action(Button.Button3, command='system_taskManager')
        expected_keystrokes = [
            (MODIFIERS_TO_KEYS['shift'], True),
            (MODIFIERS_TO_KEYS['ctrl'], True),
            (keyboard.Key.esc, True),
            (keyboard.Key.esc, False)
        ]
        wait_for_keyboard_input(Button.Button3, len(expected_keystrokes), timeout=5.0, suppress=True)
        received = g_keyboard_events[:len(expected_keystrokes)]
        self.assertEqual(received, expected_keystrokes)

    # def test_system_lock_screen(self):
    #     self.configure_button_action(Button.Button3, command='system_lockScreen')
    #     emulate_button_click(Button.Button3)

    #     # desktop state verification
    #     user32 = ctypes.windll.User32
    #     OpenDesktop = user32.OpenDesktopA
    #     SwitchDesktop = user32.SwitchDesktop
    #     DESKTOP_SWITCHDESKTOP = 0x0100

    #     TIMEOUT = 5.0  # try for 10 seconds
    #     start = time.time()
    #     locked = False
    #     while (time.time() - start) < TIMEOUT:
    #         hDesktop = OpenDesktop ("default", 0, False, DESKTOP_SWITCHDESKTOP)
    #         result = SwitchDesktop (hDesktop)
    #         if not result:
    #             print(time.asctime(), "locked")
    #             locked = True
    #             break
    #         time.sleep(1.0)

    #     self.assertTrue(locked)


    def test_system_open_explorer_window(self):
        self._test_keystroke_command(Button.Button3, 'system_openExplorerWindow', keyboard.KeyCode.from_char('e'), modifiers='win')

    def test_system_show_run_dialog(self):
        self._test_keystroke_command(Button.Button3, 'system_showRunDialog', keyboard.KeyCode.from_char('r'), modifiers='win')

    def test_system_show_system_search(self):
        self._test_keystroke_command(Button.Button3, 'system_showSystemSearch', keyboard.KeyCode.from_char('s'), modifiers='win')

    def test_system_show_task_view(self):
        self._test_keystroke_command(Button.Button3, 'system_taskView', keyboard.Key.tab, modifiers='win')


class TestSnippet(TrackballTest):
    def test_simple_snippet(self):
        """Tests simple English string snippet"""
        SNIPPET = "hello"
        params = {
            "content": SNIPPET
        }
        self.configure_button_action(Button.Button3, command='snippet', params=params)
        expected_keystrokes = string_to_keycodes(SNIPPET)
        wait_for_keyboard_input(Button.Button3, len(expected_keystrokes), timeout=5.0)
        received = g_keyboard_events[:len(expected_keystrokes)]
        self.assertEqual(received, expected_keystrokes)

    def test_complex_snippet(self):
        """Tests snippet consisting of a mix of uppercase & lowercase characters"""
        SNIPPET = "Hello World"
        params = {
            'content': SNIPPET
        }
        self.configure_button_action(Button.Button3, command='snippet', params=params)
        expected_keystrokes = string_to_keycodes(SNIPPET)
        wait_for_keyboard_input(Button.Button3, len(expected_keystrokes), timeout=5.0)
        received = g_keyboard_events[:len(expected_keystrokes)]
        self.assertEqual(received, expected_keystrokes)

    def test_unicode_snippet(self):
        """Tests snippet consisting of unicode characters"""
        SNIPPET = "你好! お元気ですか! എന്തൊക്കെയുണ്ട്! как твои дела"
        params = {
            'content': SNIPPET
        }
        self.configure_button_action(Button.Button3, command='snippet', params=params)
        expected_keystrokes = string_to_keycodes(SNIPPET)
        wait_for_keyboard_input(Button.Button3, len(expected_keystrokes), timeout=5.0)
        received = g_keyboard_events[:len(expected_keystrokes)]
        self.assertEqual(received, expected_keystrokes)


class TestOpenItem(TrackballTest):

    def _get_running_apps(self, procname):
        """Returns a list of psutil.proc objects of all active processes."""
        running_apps = []
        for proc in psutil.process_iter():
            if proc.name() == procname:
                running_apps.append(proc)
        return running_apps

    def _get_active_windows(self, title):
        """Returns a list of window handles of all active windows"""
        hwnds = []
        if sys.platform == 'win32':
            def enum_windows_callback(hwnd, hwnds):
                wintitle = win32gui.GetWindowText(hwnd)
                if title == wintitle:
                    hwnds.append(hwnd)

            win32gui.EnumWindows(enum_windows_callback, hwnds)
        elif sys.platform == 'darwin':
            # TODO: Implement for OSX
            pass

        return hwnds

    def _get_active_window_titles(self):
        """Returns a list of strings of all active windows' titles."""
        titles = []
        if sys.platform == 'win32':

            def enum_windows_callback(hwnd, hwnds):
                wintitle = win32gui.GetWindowText(hwnd)
                if len(wintitle) > 0:
                    titles.append(wintitle)

            win32gui.EnumWindows(enum_windows_callback, titles)
        elif sys.platform == 'darwin':
            # TODO: Implement for OSX
            pass

        return titles

    def _get_process_ids(self, title):
        '''Returns list of process ids of processes with title string in their
        Window Title.'''
        processes = set()
        if sys.platform == 'win32':

            def enum_windows_callback(hwnd, hwnds):
                wintitle = win32gui.GetWindowText(hwnd)
                if len(wintitle) > 0 and title in wintitle:
                    _, pid = win32process.GetWindowThreadProcessId(hwnd)
                    processes.add(pid)

            win32gui.EnumWindows(enum_windows_callback, processes)
        elif sys.platform == 'darwin':
            # TODO: Implement for OSX
            pass

        return processes

    def test_open_program(self):
        """Test that open command to launch a program, by its executable path,
        works."""
        PROCESS_NAME = "notepad.exe"
        if sys.platform == 'darwin':
            PROCESS_NAME = "textEdit"

        # Cache any current running instances of the process
        apps_snapshot1 = set(self._get_running_apps(PROCESS_NAME))

        self.configure_button_action(
            Button.Button3,
            command='launching_openApplication',
            params={'path': PROCESS_NAME})

        emulate_button_click(Button.Button3)

        # Wait a little while for the program to launch
        time.sleep(0.5) # 500 milliseconds ought to be emough
        apps_snapshot2 = set(self._get_running_apps(PROCESS_NAME))
        diff = apps_snapshot2.difference(apps_snapshot1)

        # look for PROCESS_NAME in the difference set
        found = False
        for proc in diff:
            if proc.name() == PROCESS_NAME:
                found = True
                # since we found the process, we may kill it as well
                proc.kill()
                break
        self.assertTrue(found)

    def test_open_folder(self):
        """Test that open command to open a local folder, by its path,
        works."""
        FOLDER = "C:\\Program Files"
        if sys.platform == 'darwin':
            # TODO: fix folder path to one that works on Mac
            FOLDER = "~"    # home folder?

        p = Path(FOLDER)
        active = self._get_active_windows(p.name)
        for hwnd in active:
            if sys.platform == 'win32':
                win32gui.CloseWindow(hwnd)
            elif sys.platform == 'darwin':
                # TODO - implement for Mac
                pass
        active = []

        self.configure_button_action(
            Button.Button3,
            command='launching_openFolder',
            params={'path':FOLDER})
        emulate_button_click(Button.Button3)

        TIMEOUT = 5.0   # wait for 5 seconds max for the folder to launch
        start = time.perf_counter()
        while (time.perf_counter()-start) < TIMEOUT:
            # verify that the folder was indeed opened
            active = self._get_active_windows(p.name)
            if len(active) > 0:
                # time.sleep(0.5) # need to wait this arbitrary interval
                #                 # for Explorer window to complete its init.
                # if sys.platform == 'win32':
                #     win32gui.CloseWindow(active[0])
                # elif sys.platform == 'darwin':
                #     # TODO - implement for Mac
                #     pass
                break
            time.sleep(0.2)

        self.assertTrue((time.perf_counter()-start) < TIMEOUT)

    def test_open_url(self):
        """Test that open command to open a URL in default browser works."""

        URL = "http://www.google.com"
        self.configure_button_action(
            Button.Button3,
            command='launching_openURL',
            params={'path':URL})
        emulate_button_click(Button.Button3)

        # We wait 10 seconds for the browser to be launched and the window
        # to be opened. If testing on a VM, event 10 seconds may not be enough
        # if the host system is hunder heavy load. So this test may fail even
        # though the feature works.
        TIMEOUT = 10.0
        start = time.perf_counter()
        found = False
        while (time.perf_counter()-start) < TIMEOUT and not found:
            titles = self._get_active_window_titles()
            for title in titles:
                if 'Google' in title:
                    found = True
                    break

            time.sleep(0.1)

        self.assertTrue(found)

        browser_pids = self._get_process_ids("Google")
        if browser_pids:
            for pid in browser_pids:
                kill_process_by_pid(pid)

if __name__ == '__main__':
    if sys.platform != 'win32':
        print("These tests are meant to be run on Windows!")
        exit(1)
    tbw_tests_main()
