# To be sourced from the other modules

# Exits the script with a message and exit code 1
function error_exit
{
    echo "$1" 1>&2
    exit 1
}

# check if we're being run as root
function check_root
{
    if [ "$EUID" -ne 0 ]; then
        echo "Please run as root"
        exit
    fi
}

function parse_common_args
{
    if [ "$HELPER_ADDR" == "" ]; then
        HELPER_ADDR=127.0.0.1
    fi

    if [ "$DEVICE_ID" == "" ]; then
        DEVICE_ID=32792
    fi
}

function dump_common_args
{
    echo "Helper address: $HELPER_ADDR"
    echo "Device ID: $DEVICE_ID"
}

function common_args_usage
{
    echo "Set DEVICE_ID environment variable to the USB product ID of the "
    echo "Trackball device that you want to configure. If not set, this"
    echo "defaults to 32792, which is the ID for Expert Wireless TB."
}
