#!/bin/bash

source ./common_funcs.sh
# parse HELPER_ADDR & DEVICE_ID from environment variables
parse_common_args

SPEED=$1
INERTIAL=$2
INVERT=$3

if [ "$SPEED" == "" ] || [ "$INERTIAL" == "" ] || [ "$INVERT" == "" ]; then
	echo "Configure Trackball scroll wheel properties."
	echo "Usage:"
	echo "   $ cfgscroll <speed> <inertial> <invert>"
	echo
	echo "       speed - 1~100"
	echo "       inertial - {true|false}"
	echo "       invert - {true|false}"
	echo
	common_args_usage
	exit 1
fi

dump_common_args
echo "Speed: $SPEED"
echo "Invert: $INVERT"
echo "Inertial: $INERTIAL"

curl --header "Content-Type: application/json" --request POST --data "{\"speed\":$SPEED, \"invert\":$INVERT, \"inertial\":$INERTIAL}" "http://$HELPER_ADDR:9090/config/scroll?app=*&device=$DEVICE_ID" -i

