#!/bin/bash

source ./common_funcs.sh
# parse HELPER_ADDR & DEVICE_ID from environment variables
parse_common_args


dump_common_args

curl --header "Content-Type: application/json" "http://$HELPER_ADDR:9090/favapps?count=20&device=$DEVICE_ID" -i

