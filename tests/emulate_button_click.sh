#!/bin/bash

source ./common_funcs.sh
# parse HELPER_ADDR & DEVICE_ID from environment variables
parse_common_args


BUTTON=$1
NAME=$2
RESOURCE=$3 

if [ "$BUTTON" == "" ]; then
    echo "Emulate a Trackball button click."
	echo "Usage:"
	echo "   $ emulate_button_click <button-mask>"
	echo
	echo "       button-mask - Button mask to emulate, 1~15"
	echo
	common_args_usage
	exit 1
fi

dump_common_args
echo "Button:" $BUTTON

sleep 3
curl --header "Content-Type: application/json" --request POST --data "$JSON" http://$HELPER_ADDR:9090/devices/$DEVICE_ID/emulatebuttonclick/$BUTTON -i

