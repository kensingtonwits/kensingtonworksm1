workon tau
if (!$?) {
    throw "Building TAU python executable requires 'tau' virtualenv to be setup"
}
pyinstaller.exe -F -y .\tbwtests.py
if (!$?) {
    throw "Error building the TAU executable. Is PyInstaller installed?"
}