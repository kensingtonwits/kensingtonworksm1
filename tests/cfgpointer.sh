#!/bin/bash

source ./common_funcs.sh
# parse HELPER_ADDR & DEVICE_ID from environment variables
parse_common_args


SPEED=$1
ACCELERATION=$2
SLOWMODIFIERS=$3
SNAPMODIFIERS=$4

if [ "$SPEED" == "" ] || [ "$ACCELERATION" == "" ]; then
        echo "Configure Trackball pointer properties."
	echo "Usage:"
	echo "   $ cfgpointer <speed> <acceleration>"
	echo
	echo "       speed - 1~100. 10 sets speed to 1:1."
	echo "       acceleration - 0~100. 0 disables acceleration."
	echo "       slowmodifiers - Combination of {shift|ctrl|alt|cmd|win}, delimited"
	echo "       	by comma."
	echo "       snapmodifiers - Combination of {shift|ctrl|alt|cmd|win}, delimited"
	echo "       	by comma."
	echo
	common_args_usage
	exit 1
fi

dump_common_args
echo "Speed: $SPEED"
echo "Acceleration: $ACCELERATION"
echo "Slow modifiers: $SLOWMODIFIERS"
echo "Snap modifiers: $SNAPMODIFIERS"

curl --header "Content-Type: application/json" --request POST --data "{\"accelerationRate\":$ACCELERATION, \"speed\": $SPEED, \"slowModifiers\":\"$SLOWMODIFIERS\", \"lockedAxisModifiers\":\"$SNAPMODIFIERS\"}" "http://$HELPER_ADDR:9090/config/pointer?app=*&device=$DEVICE_ID" -i 
