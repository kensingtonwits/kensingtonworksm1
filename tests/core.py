# coding=utf-8

import unittest
import serial
import requests
import json
import ctypes, os, sys, tempfile
import enum
import time
from pathlib import Path
from enum import IntEnum, IntFlag
from xml.etree import ElementTree as ET
import argparse
import psutil
from pynput import mouse, keyboard
import struct
import threading
import pynput._util.win32_vks as VK
import shutil
import datetime
import collections
import keyboard as syskeyboard

import logresult
from emulator_types import Button
from emulator import (
    Emulator, configure_device, auto_detect_serial_port,
    )

# Global Emulator instance, that all unit tests can use
g_emu = Emulator()

if sys.platform == 'win32':
    import win32gui, win32con, win32process

if sys.platform == "darwin":
    import Quartz


class ButtonClick(enum.IntEnum):
    DOWN = 1
    UP = 2
    DOWN_AND_UP = 3

def tbwkern_emulate_button_click(deviceFilename, button_mask):
    '''
    Helepr function that emulates a button click by sending an emulate action
    ioctl to tbwkern device.

    Parameters:
        deviceFilename - str, the kernel device filename to open
        button_mask - the button mask to emulate. Follows button masks as
            reported by trackball devices like below:

            0x01 - button 1
            0x02 - button 2
            0x04 - button 3
            0x08 - button 4
            0x03 - buttons 1 & 2 (chording)
            0x0C - buttons 3 & 4 (chording)

    Raises:
        Exception if device open failed or if Ioctl failed.
    '''
    import win32con # pylint: disable = E0401
    from win32 import win32gui
    from win32 import win32file
    #from win32.lib import win32con, winioctlcon, shell

    # EMULATEDEVICEACTION Ioctl takes the following structure as its argument.
    '''
        UINT32  cbSize;         // structure size (provision for future enhancement)
        UINT32  uAction;        // See action types below
        BYTE    aParam[256];    // generic 256 byte array of parameters
    '''
    IOCTL_EMULATEDEVICEACTION_FMT = "L L 256B"
    IOCTL_EMULATEDEVICEACTION = winioctlcon.CTL_CODE(winioctlcon.FILE_DEVICE_UNKNOWN, 0x800+15, winioctlcon.METHOD_BUFFERED, winioctlcon.FILE_ANY_ACCESS)

    #deviceFilename = "\\\\?\\{af111662-3d80-43ac-b39b-d7b15acb0b54}#knstbw#8&2b20731d&0&01#{1d0785a7-8cf1-46d9-9643-6a3a99eb21a0}"
    hDevice = win32file.CreateFile(
        deviceFilename,
        win32con.GENERIC_READ|win32con.GENERIC_WRITE,
        win32con.FILE_SHARE_READ|win32con.FILE_SHARE_WRITE,
        None,
        win32con.OPEN_EXISTING,
        win32con.FILE_ATTRIBUTE_NORMAL,
        None) # hTemplate

    if not hDevice:
        raise Exception("Device open failed")

    buf = bytearray(256)
    buf[0] = button_mask # emulate DOWN & UP actions
    buf[1] = 3 # emulate DOWN & UP actions
    input_buf = struct.pack(
        IOCTL_EMULATEDEVICEACTION_FMT,
        264,    # 4 + 4 + 256 = 264
        1,
        *list(buf))

    # issue IOCTL to read device info
    win32file.DeviceIoControl(
        hDevice,
        IOCTL_EMULATEDEVICEACTION,
        input_buf,
        0,
        None)
    hDevice.Close()

# Modifier string representations to keyboard.Key mapping
MODIFIERS_TO_KEYS = {
    'ctrl': keyboard.Key.ctrl_l,
    'shift': keyboard.Key.shift_l,
    'alt': keyboard.Key.alt_l,
    'win': keyboard.Key.cmd,
    'cmd': keyboard.Key.cmd,
    'meta': keyboard.Key.cmd
}

# TbwHelper HTTP server address
TBWHELPER_ADDRESS = "http://localhost:9090"
# TbwHelper test instrumentation device id. Defaults to Expert Wireless Mouse
# Trackball as that's what I've been using.
DEVICE_ID = 0

# Enum for Hardware or software emulation for button presses
class EmulationMode(enum.IntEnum):
    SOFTWARE = 1
    HARDWARE = 2

# global emulation mode, set to SOFTWARE
EMULATION_MODE = EmulationMode.SOFTWARE

ERROR = 1
WARNING = 2
INFO = 2
VERBOSE = 4

verbosity = WARNING

def verbose(msg, *args):
    if verbosity >= VERBOSE:
        print(msg, *args)


def kill_process_by_pid(pid):
    '''Kill process by PID. pid is a number'''
    if sys.platform == "win32":
        os.system("taskkill /PID {0} /F".format(pid))
    else:
        os.system("kill -9 {0}".format(pid))


def get_active_windows(title):
    """Returns a list of window handles of all active windows, with
    'title' in its titlebar."""
    hwnds = []
    if sys.platform == 'win32':
        def enum_windows_callback(hwnd, hwnds):
            wintitle = win32gui.GetWindowText(hwnd)
            if title == wintitle:
                hwnds.append(hwnd)

        win32gui.EnumWindows(enum_windows_callback, hwnds)
    elif sys.platform == 'darwin':
        # TODO: Implement for OSX
        pass

    return hwnds

def close_window_by_title(title):
    '''Closes a window which has the text 'title' in its titlebar.'''
    if sys.platform == "win32":
        active = get_active_windows(title)
        for hwnd in active:
            win32gui.PostMessage(hwnd, win32con.WM_CLOSE, 0, 0)
    else:
        pass


def wait_for_process_to_start(process_binary, timeout=5.0):
    '''Waits the given timeout period for a process started with the given
    binary filename. If process is detected within the timeout period,
    returns True. Returns false otherwise.'''
    start = time.time()
    while (time.time() - start) < timeout:
        time.sleep(0.1)
        if getProcessId(process_binary) != -1:
            return True
    return False

def emulate_button_click(button):
    '''Emulates a click of the button specified by the argument.
    button has to be of type Button. Emulation can happen via SOFTWARE
    or HARDWARE. Software would use the testing API provided by tbwhelper
    whereas hardware based emulation will be done through the custom firmware.
    '''
    global DEVICE_ID, EMULATION_MODE

    if EMULATION_MODE == EmulationMode.SOFTWARE:
        myurl = TBWHELPER_ADDRESS + "/devices/{0}/emulatebuttonclick/{1}".format(DEVICE_ID, button)
        response = requests.post(myurl, json={"delay": 10})
        # assertEqual(response.status_code, 200)
        assert response.status_code == 200
    else:
        # H/w emulation. Assuming that h/w is connected and properly inited.
        g_emu.emulate_click(button)


# Global array storing move events detected by the hook procedure. After a
# unit test is run, this array can be checked if the expected set of pointer
# clicks were received.
g_mouse_events = []
g_mouse_events_lock = threading.Lock()

# Mouse listener click handler
def on_click(x, y, button, pressed):
    """Click handler returns False if the pressed button matches
    g_mouse_listener_exit_button and pressed = False.
    """
    global g_mouse_events

    try:
        g_mouse_events_lock.acquire()

        verbose('mouse.listener.on_click: ', button, pressed)
        g_mouse_events.append((button, pressed))
    finally:
        g_mouse_events_lock.release()


def number_of_clicks_received(mouse_button):
    '''
    Returns the number of mouse_button events stored in the global
    g_mouse_events array.

    Params:
        mouse_button - mouse.Button instance

    Returns:
        Number of mouse_button events in the array
    '''
    assert isinstance(mouse_button, mouse.Button)
    events_received = 0
    try:
        g_mouse_events_lock.acquire()
        for event in g_mouse_events:
            # event is a tuple of (mouse.Button, pressed-boolean)
            # So we check for the click-released boolean to mean that
            # a full click as received.
            if event[0] == mouse_button and not event[1]:
                events_received += 1

    finally:
        g_mouse_events_lock.release()

    return events_received


def compare_modifier_keystroke_lists(expected, received):
    '''Compares two lists of keystroke up,down events and returns true
    if the expected keystrokes were found in the 'received' list.
    Each element of the keystroke list is expected to be a 2-tuple of the
    form (keycode, down-boolean).
    '''
    def get_keystrokes_dir(keystrokes, dir):
        '''Returns the keystrokes in the specified direction. Stops scannign the
        list when a key with direction not matching the specified 'dir' is
        encountered.'''
        dir_keys = []
        index = 0

        # find the first keystroke in matching direction
        for keys in keystrokes:
            if keys[1] == dir:
                keystrokes = keystrokes[index:]
                break
            index += 1

        # accumulate all keystrokes until a keystroke in a with different
        # direction is encountered.
        for keys in keystrokes:
            if keys[1] != dir:
                break
            dir_keys.append(keys)

        return dir_keys

    expected_down = get_keystrokes_dir(expected, True)
    received_down = get_keystrokes_dir(received, True)
    expected_up = get_keystrokes_dir(expected, False)
    received_up = get_keystrokes_dir(received, False)

    def get_key_code(elem): # sort function
        return elem[0].value.vk

    # sort lists for quicker comparison
    expected_down.sort(key=get_key_code)
    received_down.sort(key=get_key_code)
    expected_up.sort(key=get_key_code)
    received_up.sort(key=get_key_code)

    return expected_down == received_down and expected_up == received_up


def wait_for_mouse_button_click(trackball_button, mouse_button, count=1, timeout=3.0, emulate=True, suppress=True):
    """Function sets up a global mouse listener hook which waits for
    the specified button to be pressed and released (clicked) before
    releasing the hook. timeout specifies the maximum time to wait
    for the click to arrive before the global hook is released.

    Returns boolean indicating if the listener quit gracefully, owing
    to the expected click arriving within the specified timeout period.
    """
    assert isinstance(trackball_button, Button)
    assert isinstance(mouse_button, mouse.Button)

    global g_mouse_events
    g_mouse_events.clear()

    # suppress = False
    # if sys.platform == "win32":
    #     suppress = True
    listener = mouse.Listener(on_click=on_click, suppress=suppress)
    verbose("Starting mouse listener..")
    listener.start()
    start = time.time()
    while (time.time() - start) < timeout:
        time.sleep(0.1)
        if listener.running:
            break

    if emulate:
        verbose("Emulating button press..")
        emulate_button_click(trackball_button)

    start = time.time()
    while (time.time() - start) < timeout:
        time.sleep(0.1)
        if number_of_clicks_received(mouse_button) == count:
            break

    verbose("Stopping listener..")
    listener.stop()
    verbose("Listener stopped")
    listener.join()
    verbose("Listener worker terminated")

    # Return value is a boolean indicating if th expected # of clicks
    # of the specified button were received.
    retval = number_of_clicks_received(mouse_button) == count
    g_mouse_events.clear()

    return retval


def wait_for_mouse_button_click_with_modifiers(trackball_button, mouse_button, modifiers, count=1, timeout=3.0, emulate=True):

    global g_keyboard_events
    g_keyboard_events.clear()

    listener = keyboard.Listener(on_press=on_press, on_release=on_release, suppress=True)
    verbose("Starting keyboard listener..")
    listener.start()
    start = time.time()
    while (time.time() - start) < timeout:
        time.sleep(0.1)
        if listener.running:
            break

    # this delay ensures that the keyboard listener is setup and ready to
    # receive input events.
    #time.sleep(1.0)

    clickReceived = wait_for_mouse_button_click(trackball_button, mouse_button, count, timeout, emulate)

    # now wait timeout period for the keyboard events to arrive
    num_keystrokes_to_wait_for = len(modifiers.split(','))*2
    start = time.time()
    while (time.time() - start) < timeout:
        time.sleep(0.1)
        if len(g_keyboard_events) == num_keystrokes_to_wait_for:
            break

    listener.stop()
    listener.join()

    if not clickReceived:
        g_keyboard_events.clear()
        return False

    # build expected keystrokes array
    expected_keystrokes = []
    mods = modifiers.split(",")
    for mod in mods:
        expected_keystrokes.append((MODIFIERS_TO_KEYS[mod], True))
    for mod in mods:
        expected_keystrokes.append((MODIFIERS_TO_KEYS[mod], False))

    # check if expected keystrokes for the modifier keys were received
    received = g_keyboard_events[:len(expected_keystrokes)]
    #return received == expected_keystrokes
    return compare_modifier_keystroke_lists(expected_keystrokes, received)


# Global list where keyboard inputs are stored by the hook procedure
# For tests that result in keystrokes, this array can be checked if the
# expected set of keystrokes were received or not.
g_keyboard_events = []

def on_press(key):
    """Key press hook handler"""
    g_keyboard_events.append((key, True))
    if verbosity >= VERBOSE:
        try:
            verbose('alphanumeric key {0} pressed'.format(
                key.char))
        except AttributeError:
            verbose('special key {0} pressed'.format(
                key))


def on_release(key):
    """Key release hook handler"""
    g_keyboard_events.append((key, False))
    if verbosity >= VERBOSE:
        try:
            verbose('alphanumeric key {0} released'.format(
                key.char))
        except AttributeError:
            verbose('special key {0} released'.format(
                key))

    if key == keyboard.Key.esc:
        # Stop listener
        return False


def wait_for_keyboard_input(trackball_button, num_keystrokes_to_wait_for, timeout=3.0, emulate=True, suppress=True):

    """Function sets up a global keyboard hook which collects all the
    detected keystrokes into the global g_keyboard_events array.
    """

    global g_keyboard_events
    g_keyboard_events.clear()

    listener = keyboard.Listener(on_press=on_press, on_release=on_release, suppress=suppress)
    verbose("Starting keyboard listener..")
    listener.start()
    start = time.time()
    while (time.time() - start) < timeout:
        time.sleep(0.1)
        if listener.running:
            break

    if emulate:
        #verbose("Emulating trackball button press..")
        # Ask emulator to emulate button 3 click
        emulate_button_click(trackball_button)

    start = time.time()
    while (time.time() - start) < timeout:
        time.sleep(0.1)
        if len(g_keyboard_events) == num_keystrokes_to_wait_for:
            break

    verbose("Stopping listener..")
    listener.stop()
    verbose("Waiting for listener worker to terminate..")
    listener.join(timeout)
    verbose("Listener worker terminated")
    verbose("Keyboard events: ", g_keyboard_events)


def getProcessId(processBinFile):
    '''Returns the id of the process whose executable is processBinary.
    Returns -1 if not found.
    '''
    for proc in psutil.process_iter():
        try:
            # Check if process name contains the given name string.
            if processBinFile.lower() in proc.name().lower():
                return proc.ppid()
        except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
            pass
    return -1


def checkIfProcessRunning(processName):
    '''
    Check if there is any running process that contains the given name processName.
    '''
    #Iterate over the all the running process
    for proc in psutil.process_iter():
        try:
            # Check if process name contains the given name string.
            if processName.lower() in proc.name().lower():
                return True
        except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
            pass
    return False


def is_tbwhelper_running():
    '''Returns a boolean indicating if tbwhelper is running.'''
    return checkIfProcessRunning('KensingtonWorksHelper' if sys.platform == 'darwin' else 'tbwhelper')


class TrackballTest(unittest.TestCase):
    ''' Base class for all TrackballWorks unit tests '''

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._old_settings = None

    def _get_settings_filepath(self):
        folder = ''
        if sys.platform == "win32":
            folder = os.path.join(os.path.expanduser('~'), 'AppData', 'Roaming', 'Kensington', 'TrackballWorks')
        else:
            folder = os.path.join(os.path.expanduser('~'), 'Library', 'Preferences')
        return os.path.join(folder, 'TbwSettings.json')

    def _backup_tbwsettings(self):
        # TODO: migrate to using HTTP API
        # tbwsettings = self._get_settings_filepath()
        # backup = tbwsettings + ".bak"
        # try:
        #     os.remove(backup)
        # except FileNotFoundError:
        #     pass
        # shutil.copy(tbwsettings, backup)
        myurl = TBWHELPER_ADDRESS + "/config"
        response = requests.get(myurl)
        self.assertEqual(response.status_code, 200)
        if response.status_code == 200:
            # save current settings
            self._old_settings = response.json()

    def _restore_tbwsettings(self):
        # TODO: migrate to using HTTP API
        # tbwsettings = self._get_settings_filepath()
        # backup = tbwsettings + ".bak"
        # shutil.copy(backup, tbwsettings)
        # try:
        #     os.remove(backup)
        # except FileNotFoundError:
        #     pass
        if (self._old_settings):
            # restore previous settings
            myurl = TBWHELPER_ADDRESS + "/config"
            response = requests.post(myurl, json=self._old_settings)
            self.assertEqual(response.status_code, 200)
            self._old_settings = None


    def setUp(self):
        self._backup_tbwsettings()
        return super().setUp()

    def tearDown(self):
        self._restore_tbwsettings()
        return super().tearDown()

    def configure_button_action(self, tbButton, **kwargs):
        '''Configure the device button'''
        myurl = TBWHELPER_ADDRESS + "/config/buttons?app=*&device={0}&button={1}".format(
            DEVICE_ID, tbButton
        )
        response = requests.post(myurl, json=kwargs)
        self.assertEqual(response.status_code, 200)
        time.sleep(0.2) # delay is for the new settings to take effect

    def get_helper_address(self):
        '''Returns TBW helper process HTTP server address'''
        global TBWHELPER_ADDRESS
        return TBWHELPER_ADDRESS

    def get_device_id(self):
        '''Returns configured device id'''
        global DEVICE_ID
        return DEVICE_ID


def is_admin():
    '''Returns a boolean true indicating if current user has
    administrator privileges'''
    if sys.platform == 'darwin':
        return os.geteuid() == 0
    # Windows
    return shell.IsUserAnAdmin()


def get_attached_devices():
    '''Returns list of attached Kensington Trackball devices as a list of
    2-tuples each of which is (<device id>, <Device Name>)'''
    myurl = TBWHELPER_ADDRESS + "/devices"
    devices = []
    try:
        response = requests.get(myurl).json()
        all_devices = response['devices']
        for device in all_devices:
            if device['present']:
                devices.append(device)

    except:
        pass
    return devices


def check_if_device_is_present(deviceid):
    '''Returns a boolean indicating if a TBW device is present'''
    myurl = TBWHELPER_ADDRESS + "/devices"
    response = requests.get(myurl).json()
    if len(response['devices']) > 0:
        for device in response['devices']:
            if (device['id'] == deviceid):
                return True

    return False


# No longer used, keeping it for posterity
def get_xfer_options():
    '''Returns a 2-tuple options transferred from taugui.py.
        (DEVICE_ID, <EMULATION>)
         - DEVICE_ID - number, device id
         - EMULATION: str, {"SOFTWARE"|"HARDWARE"}
    '''
    xfer_device_id = 0
    emulation = "SOFTWARE"
    xfer_device_id_filename = os.path.join(
        tempfile.gettempdir(),
        '.tbw_tests_deviceid')
    if os.path.exists(xfer_device_id_filename):
        try:
            with open(xfer_device_id_filename) as f:
                options = f.readlines()
                for option in options:
                    k, v = option.split("=")
                    v.replace('\n', '')
                    if k == "DEVICE_ID":
                        xfer_device_id = int(v)
                    elif k == "EMULATION":
                        emulation = v
        except:
            pass

    return xfer_device_id, emulation


def set_test_options(deviceid, emulation_mode):
    '''
    Sets the global options to run the tests in this file.
        deviceid - number, device id
        emulation_mode - str, {"SOFTWARE"|"HARDWARE"}
    '''
    global DEVICE_ID, EMULATION_MODE

    DEVICE_ID = deviceid
    if emulation_mode == "HARDWARE":
        EMULATION_MODE = EmulationMode.HARDWARE


def auto_hex(v):
    return int(v, 16)


class LoggingTestResults(unittest.TextTestResult, logresult.LogResults):
    '''A test results class that logs test status to test.log'''

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def startTestRun(self):
        self.logStartTestRun()
        return super().startTestRun()

    def stopTestRun(self):
        self.logStopTestRun()
        return super().stopTestRun()

    def startTest(self, test):
        self.logStartTest(test)
        return super().startTest(test)

    def addSuccess(self, test):
        self.logSuccess(test)
        return super().addSuccess(test)

    def addFailure(self, test, err):
        self.logFailure(test, err)
        return super().addFailure(test, err)

    def addError(self, test, err):
        self.logError(test, err)
        return super().addError(test, err)

    def addSkip(self, test, reason):
        self.logSkip(test, reason)
        return super().addSkip(test, reason)



class TestKeystrokeBase(TrackballTest):
    '''Base class to test testing scenarios that poke predefined system
    keystrokes OS/app specific actions'''

    def _build_expected_keystrokes(self, expectedChar, modifiers=""):
        '''
        Given an expected character and an optional combination of modifier keys,
        returns an array of pynput.keyboard.KeyCode objects that makes up the
        keystroke combination.

        So if modifiers are specified, the returned array would list the KEYDOWN
        events for the modifers first, followed by the KEYDOWN & KEYUP for the
        expectedChar argument and KEYUP for the specified modifiers.
        '''
        expected_keystrokes = []

        # build expected keystrokes array
        if modifiers:
            mods = modifiers.split(",")
            for mod in mods:
                expected_keystrokes.append((MODIFIERS_TO_KEYS[mod], True))

        if sys.platform == "win32":
            expected_keystrokes.append((expectedChar, True))
            expected_keystrokes.append((expectedChar, False))
        else:
            # In Darwin, the keyboard hook returns the scan code that was
            # poked into the input queue. So we need to conver the expected
            # character into its equivalent scan code and their build
            # pynput.keyboard.KeyCode corresponding to that
            if type(expectedChar) == keyboard.KeyCode:
                scanCode = syskeyboard.key_to_scan_codes(expectedChar.char)[0]
            elif type(expectedChar) == str:
                scanCode = syskeyboard.key_to_scan_codes(expectedChar)[0]
            else:
                raise Exception('Invalid scancode argument')
            #scanCode = syskeyboard.key_to_scan_codes(expectedChar.char)[0]
            keyCode = keyboard.KeyCode.from_vk(scanCode)
            expected_keystrokes.append((keyCode, True))
            expected_keystrokes.append((keyCode, False))

        if modifiers:
            mods = modifiers.split(",")
            for mod in mods:
                expected_keystrokes.append((MODIFIERS_TO_KEYS[mod], False))
        return expected_keystrokes

    def _test_keystroke_raw(self, button, keyCode, expectedChar, modifiers=""):
        '''
        Method programs the given button to generate the specified keystroke
        combination and then emulates the button click and verifies that the
        programmed keystroke was received in the system input queue.
        '''
        params = {
            "keyCode": keyCode,
            "modifiers": modifiers
        }
        self.configure_button_action(button, command='keyboard_shortcut', params=params)
        # expected_keystrokes = []

        # # build expected keystrokes array
        # if modifiers:
        #     mods = modifiers.split(",")
        #     for mod in mods:
        #         expected_keystrokes.append((MODIFIERS_TO_KEYS[mod], True))

        # expected_keystrokes.append((expectedChar, True))
        # expected_keystrokes.append((expectedChar, False))

        # if modifiers:
        #     mods = modifiers.split(",")
        #     for mod in mods:
        #         expected_keystrokes.append((MODIFIERS_TO_KEYS[mod], False))
        expected_keystrokes = self._build_expected_keystrokes(expectedChar, modifiers)
        wait_for_keyboard_input(button, len(expected_keystrokes), timeout=3.0, suppress=True)
        received = g_keyboard_events[:len(expected_keystrokes)]
        self.assertEqual(received, expected_keystrokes)

    def _test_keystroke_command(self, button, command, expectedChar, modifiers="", suppress=True):
        '''
        Test
        '''
        self.configure_button_action(button, command=command)
        expected_keystrokes = self._build_expected_keystrokes(expectedChar, modifiers)
        wait_for_keyboard_input(button, len(expected_keystrokes), timeout=5.0, suppress=suppress)
        received = g_keyboard_events[:len(expected_keystrokes)]
        #import ipdb; ipdb.set_trace()
        self.assertEqual(received, expected_keystrokes)


def get_tbw_key_code(key):
    '''Returns the keyboard scan code for the given key'''
    if sys.platform == "darwin":
        return syskeyboard.key_to_scan_codes(key)[0]
    return ord(key)


def prompt_for_deviceid(devices):
    '''
    Prompts user to choose a device id from the list of connected device ids
        devices - is the return value from get_attached_devices().

    Returns None if user selected Quit tests or the real device id that
    they want to run the tests on.
    '''
    print('\nThe following devices are connected:')
    index = 1
    for device in devices:
        print('\t{0}. {1}, id: {2}'.format(index, device['name'] if device['name'] else "Uknown device, possibly BLE", device['realId']))
        index += 1

    # get user input -- device index or {Q|q}
    response = '0'
    while response not in range(1, len(devices)+1):
        response = input('Enter [1..n] to run tests on that device or Q to abort\n')
        if response == 'Q' or response == 'q':
            return None
        response = int(response) if response.isdecimal() else 0

    return devices[int(response)-1]['id']


def get_mapped_device_id(devices, deviceid):
    '''
    Given a physical trackball device id, returns the corresponding
    id that can be used with the TBWHelper APIs.

        devices - is the return value from get_attached_devices().
        deviceid - id to map to the value expected by TBWHelper API

    Returns the mapped device id. Or 0 if supplied deviceid was not
    found in devices.
    '''
    for device in devices:
        ids = device['realId'].split(',')
        for id in ids:
            if int(deviceid) == device['id'] or int(deviceid) == int(id):
                return device['id']
    return 0


def tbw_tests_main(initialTestName=""):

    # Check program is being run as administrator
    #if sys.platform == 'darwin' and not is_admin():
    #    print("Running the tests on Mac requires root privileges. Please start with 'sudo'")
    #    sys.exit(1)

    if not is_tbwhelper_running():
        print("Helper process not detected!")
        sys.exit(1)

    devices = get_attached_devices()
    if len(devices) == 0:
        print("Could not detect a Trackball device!")
        sys.exit(1)

    # Test device defaults to the first device if only one is connected.
    #DEFAULT_DEVICE_ID = devices[0]['id'] if len(devices) == 1 else 0
    DEFAULT_DEVICE_ID = 0

    # This mechanism is no longer used!! Keeping it for record, in case we
    # need to activate this interface.
    #
    # Look for <tempfolder>/.tbw_tests_deviceid. If it's present use its
    # contents as the target device to test. This file will be written by
    # the test launcher GUI to share the user's target device selection.
    #
    # If it's not present use  Expert Wireless TB as the default target device.
    # xfer_device_id, emulation = get_xfer_options()
    # print("DEVICE_ID: {0}, EMULATION: {1}".format(xfer_device_id, emulation))

    # if xfer_device_id:
    #     DEFAULT_DEVICE_ID = xfer_device_id

    # Parse the commandline to read the desired COM port
    parser = argparse.ArgumentParser(description='Utility to exercise & test trackball functions')
    parser.add_argument('--comport', default='', help='COM port to use to program the trackball with test instrumentation firmware. Specify com1, com2, etc.')
    parser.add_argument('--apiport', default=9090, help='TCP/IP port where the tbwhelper HTTP REST API servier is available. Defaults to 9090')
    parser.add_argument('--deviceid', type=auto_hex, default=DEFAULT_DEVICE_ID, help='Product Id, in HEX, of the trackball device connected to the computer. Defaults to 8018(Expert Wireless Mouse)')
    parser.add_argument('--hwassisted', action='store_true', help='Enable hardware assisted testing. Button click emulations will be done through custom firmware attached to COM port')
    options, extra = parser.parse_known_args()

    user_deviceid = options.deviceid

    if options.deviceid == 0:
        choice = prompt_for_deviceid(devices)
        if not choice:
            sys.exit(1)
        options.deviceid = choice

    options.deviceid = get_mapped_device_id(devices, options.deviceid)

    device_exists = False
    for device in devices:
        if options.deviceid == device['id']:
            device_exists = True
            break

    if not device_exists:
        print("Trackball device with id {0}({1}) not detected!".format(
            hex(user_deviceid),
            user_deviceid))
        sys.exit(1)

    if '--verbose' in sys.argv:
        global verbosity
        verbosity = VERBOSE

    # set global test options
    set_test_options(
        int(options.deviceid) if options.deviceid else DEFAULT_DEVICE_ID,
        "HARDWARE" if options.hwassisted else "SOFTWARE"
    )

    if options.hwassisted:
        port = auto_detect_serial_port() if not options.comport else options.comport
        if not port:
            print("Serial port not specified or one couldn't be detected")
            parser.print_help()
            sys.exit(1)


        print("Using {0} to interface with the trackball".format(port))

        print("Waiting for device.")

        if not g_emu.open(port):
            print("Error opening the emulator device")
            sys.exit(1)

    # Go on and run the tests
    if not extra:
        extra = []
    extra.insert(0, sys.argv[0])

    runner = unittest.TextTestRunner(verbosity=2, resultclass=LoggingTestResults)
    unittest.main(argv=extra, testRunner=runner)
