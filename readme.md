# Contents
- [Introduction](#introduction)
- [Architecture](#architecture)
- [Projects and Folders](#projects-and-folders)
- [Toolset](#toolset)
- [Thirdparty Libraries Used](#thirdparty-libraries-used)
- [Migrating from Smallpearl to Kensington](#migrating-from-smallpearl-to-kensington)
- [Development Environment](#development-environment)
- [Build Environment](#build-environment)
- [Build Script](#build-script)
- [Version Numbering](#version-numbering)
- [Adding support for new devices](#adding-support-for-new-devices)
- [Localization](#localization)

# Introduction
This repository contains the source code for KensingtonWorks2 for
Windows and OS X platforms.

# Architecture
On both the supported platforms, KensingtonWorks2 suite primarily consists of
three major components:
* Low level device driver running the OS kernel.
* Helper application, which is akin to a daemon, but running the logged in user's context.
This process exposes an HTTP REST API to access & configure the trackball device controlled
by the kernel mode driver.
* A high level GUI app, written in HTML & Javascript, that interfaces with the helper app
through its HTTP REST API.

This can be represented by the following block diagram:-

![Architecture](architecture.jpg "Architecture")

Though the diagram only shows three components, there are a few other projects
in the repository that perform a supporting role. An example for this
is `remlegacy`, which is a Windows specific program that is used
by the MSI installer to automate the uninstallation of TrackballWorks 1.x
if one is found during KensingtonWorks2 installation.

Another example for such a supporting component, but for Mac OSX, is the
`prefpane` project, which adds itself to the OS System Preferences app, and when
activated simply launches the GUI app installed under OS X standard `Applications`
folder.

# Projects and Folders
Each componenent of the suite is placed in its own subfolder under the project
folder and for such each component, further specialization is made according to
the platform  the component targets. Therefore, for the low level kernel mode
driver, source code is stored under 'driver' and under 'driver' two subfolders,
'osx' & 'windows' are used to further separate the code.

Note that this structure is applied only to components that are shared across
the platforms. For platform specific components, they are placed in its own
folder under the project root folder. An example for this kind of component is
`prefpane` project, which only applies to OS X.

## Folders
This table lists the various folders under the code repository root and their
description, applicable platform & development framework/language.

| Folder        | Description                    | Platform | Language
|---------------|--------------------------------|----------|---------
| driver        | Kernel mode drivers            | Win,Mac  | C/C++
| gui           | KensingtonWorks Application    | Win,Mac  | TypeScript
| kwdwnldr      | User mode service              | Win      | C/C++
| prefpane      | OSX Preference Pane            | Mac      | Objective-C
| redist        | Binary Redistributables        | Win,Mac  |
| remlegacy     | Legacy TBW Uninstaller         | Win      | C
| setup         | Installer scripts              | Win,Mac  | WiX,XML
| shared        | Shared code                    | Win      |
| tbwhelper     | Helper module (User agent)     | Win,Mac  | C/C++
| tests         | Automated tests & test scripts | Win,Mac  | Python
| tools         |                                | Win      |
| uninstaller   | KensingtonWorks Uninstaller    | Mac      | Objective-C

# Toolset
Following are the tools that are expected to be installed globally for building
the software.

## Common
- [Node.js](https://nodejs.org/) & NPM, v12.0.0 (Use platform specific installer package. For Windows please download x86 installer.)
- [TypeScript](http://www.typescriptlang.org/), v3.5.2 (Use `npm install typescript@3.5.2 -g`)
- [Gulp](https://gulpjs.com/), CLI v2.0.1 (Use `npm install gulp-cli@2.0.1 -g`)
- [node-gyp](https://github.com/nodejs/node-gyp), v7.1.2 (use `npm install -g node-gyp@7.1.2`)

## Mac
- Xcode 12.3, for masOS Big Sur(SDK 11.1) required for building DriverKit drivers compatible with Apple MacBook M1 and Intel X86 base. 
- Xcode 11.6, for MacOS SDK 10.15 required for building DriverKit dirver compatible with Catalina.
- Xcode 8.2.1, for MacOS SDK 10.12 required for building drivers compatible with Sierra.
Refer to the link [here](https://roadfiresoftware.com/2017/09/how-to-install-multiple-versions-of-xcode-at-the-same-time/)
on how to install and keep two versions on Xcode on the same machine. There are numerous other articles on the web
that provide instructions on doing this. The process is the same irrespective of the Xcode version.

## Windows
- Visual Studio 2017, any edition
- WiX, v3.11.1
- Windows 10 Platform SDK, latest version
- Windows Driver Kit for Windows 10

# Thirdparty Libraries Used
These are the third party libraries used to build the software. These have been carefully
selected to ensure that all such libraries have a 'permissive' license scheme that would not
infringe upon anyone's copyright or intellectual property claims and result in any legal issues.

- [Boost](https://www.boost.org/), v1.68.0, [Boost Software License](https://www.boost.org/users/license.html)
- [JSON for Modern C++](https://github.com/nlohmann/json), [MIT License](https://github.com/nlohmann/json/blob/develop/LICENSE.MIT)
- [RapidXML](http://rapidxml.sourceforge.net/), v1.13, [Boost Software License](http://rapidxml.sourceforge.net/license.txt)
- [Plist C++ reader & writer](https://github.com/animetrics/PlistCpp), [MIT license](https://opensource.org/licenses/MIT).

## Setting up of libraries
### Boost
Boost library is distributed in source code form and needs to be built on each platform
so that it can be linked with the project. Follow platform specific instructions
[here](https://www.boost.org/doc/libs/1_71_0/more/getting_started/windows.html) and
[here](https://www.boost.org/doc/libs/1_71_0/more/getting_started/unix-variants.html)
to get it setup.

Though Boost libraries are mostly backward compatible, it's best to stick to the same version as what is used for this project.

**NOTE** Only the Helper project uses Boost library.

#### Static Libraries Required
Boost libraries are linked in their static form. Therefore remember to build the static libraries.

Also, under Mac OSX, for some reason if the dynamic libraries are present, the linker always seems to link to
them even when static link boost macro is defined. Besides specifying the individual libraries to the
linker, an easy fix is to delete all the boost dynamic libraries from `/usr/local/lib`
(`# rm /usr/local/lib/libboost_*.dylib`). This causes the linker to pick up the static libraries.

#### Library Path
In Windows, Helper project expects Boost library to be in `C:\boost_1_67_0`. If you use a different folder, please update Helper project settings with the new path. In OSX, building boost places the headers and libraries under `/usr/local/{lib\|include}`. These folders are already added to the Helper project's compiler include and linker library paths. So it'll be able to pick up the relevant libraries.

### Others
All the other third party libraries are used as header files and as such do not
require any extra setup. These libraries are included in the source code repository.

# Migrating from Smallpearl to Kensington
Since almost all the binaries are signed with Smallpearl certificate and since some of their
embedded resources such as update server point to Smallpearl resources, these need to be
updated to Kensington's resources before final public distribution. This section lists the
items that need to be modified to do this migration.

Please note that this list should not be considered complete and as such if any information
is found missing, please update this section accordingly when such issues are encountered
and eventually resolved.

## Code Signing
### Windows
All code signing is managed by the build script. Certificate is provided as `.pfx` file to
`signtool.exe` (part of SDK). It is expected that the certificate file and its password
will be kept external to the code repository and therefore these are provided as command line
options to build script.

Refer to `buildAll` build script command.

### Mac
Binaries generated using Xcode are all signed by Xcode during release build. GUI, being a non-Xcode app and
the finall installation package, is signed manually using `codesign` utility. This process is
automated by the the build script.

For all the above cases, the signing certificate is expected to be installed in the build machine's KeyChain Access.

For Xcode projects, modify the project settings to point to the correct certificate name in KeyChain Access.
These projects are:

- driver
- tbwhelper (KensingtonWorksHelper)
- uninstaller
- prefpane


These projects are currently configured to use Smallpearl signing certificate. You can change this from Xcode here:
![Xcode Signing Options](xcode-signing-profile.png "Signing Profile")

Once the certificate is installed in KeyChain, update the following variables in build script
source file `gulpfile.js`.

| Variable          | Purpose
|-------------------|--------
| MAC_PRODUCT_SIGN_IDENTITY | Developer ID Installer in KeyChain Access
| MAC_APP_SIGN_IDENTITY | Developer ID Application in KeyChain Access
| MAC_DEVELOPER_ACCOUNT_ID | Developer Account ID (typically official email used to login to Developer portal)
| MAC_DEVELOPER_ALTOOL_PASSWORD | Notarization request tool password, read from environment variable `ALTOOL_PASSWORD`. Refer to [Customizing the Notarization Workflow](https://developer.apple.com/documentation/xcode/notarizing_your_app_before_distribution/customizing_the_notarization_workflow?language=objc) for more details on `altool`.

## Update Server
Currently `smallpearl.com` is used as the update server. To change this, update the following:
- `UPDATE_SERVER_PATH` in `gulpfile.js`. This is used to generate the download URL that gets
embedded in the update meta file. It currently points to `https://smallpearl.com`.
- `GET_LATEST_PACKAGE_INFO_URL` in `gui/src/update.ts`. This variable is used to download
the update meta file that is used by the GUI to detect new versions., It currently points to `https://smallpearl.com`.

Also please refer to the following Azure Devops work items for related information:
- [#123](https://dev.azure.com/kensington-sw/new%20KensingtonWorks/_workitems/edit/123) For details on the upload meta file & its contents
- [#109](https://dev.azure.com/kensington-sw/new%20KensingtonWorks/_workitems/edit/109) For details on testing the update mechanism before making it *live*.

## Download Service Link
Windows driver package includes a download service that will display a message
to the user if the drivers are installed separately without KensingtonWorks. This
service embeds ah HTTP link to the product page that can be updated by editing
the variable `KENSINGTONWORKS_WEB` in the file `.\kwdwnldr\kwdwnldr.cpp`. Currently
this is set to `https://www.kensington.com`.

# Development Environment

## GUI
Since GUI is written using Electron cross-platform framework, its a single source project. However, within the source platform differences are handled, but  during runtime by detecting the running platform. To edit/debug this project, switch to `./gui` and start editing the source files in `./gui/src` folder. Visual Sudio Code is a nice IDE for this as it supports TypeScript/JavaScript out of the box.

## Windows
Load the solution, `tbw.sln` from Visual Studio. Edit the projects like any other generated by Visual Studio project wizard.

## Mac
Load the respective project in Xcode and continue working on it as usual.

**NOTE** Remember to change the developer identifier in all Xcode projects to Kensington specific identifier.

# Build Environment

## Common
- Install node.js globally. `node` & `npm` should be available from command line. Try to install node version that matches the version listed early in the document (v10.16). Once installed, open PowerShell or Terminal and type `node --version`. Verify the version. Do the same with `npm --version`.
- Install node package dependencies in project root folder. Change directory to project root and run `npm install`. This will install all the node packages listed in `package.json` in project root folder. You're encouraged to take a look inside this file to see the dependent packages. You'll note that all the dependent packages are listed under `devDependencies` in project root.

## GUI
- For Windows, open PowerShell prompt comlet to set environment. For macOS, open terminal to set environment.
- Change directory to `./gui`
- For Windows, type `initBuild.ps1`. For macOS, type `sh initBuild.sh`. The command initBuild.ps1/initBuild.sh script install all dependent node packages and all the node packages that GUI depends on. GUI contains node packages under both `dependencies` & `devDependencies`. Those that will be bundled into the production app are listed under the former whereas the latter lists those that are used purely for development.
  From version v2.2.4 the GUI adds a node-gyp addon module as communication bridge with helper, however the gode-gyp's NODE_MODULE_VERSION is different with electron's which this project using. To solve this problem, electron-rebuild is added into package.json to generate a library folder used by the node-gyp addon module.

## Driver, Helper & Other Components

### Windows
In Windows, default system PATH does not include the paths to Visual Studio 2017. So to make it
easy to setup the environment, a PowerShell script is included in the source that will set this
up for you. This assumes that Visual Studio 2017 (Community Edition) is installed in its
default installation path.

To enable the environment:
- Open Powershell
- Change directory to repository root
- Issue command (assuming project root is at `c:\Users\hari\kensingtonworks2`): `c:\Users\hari\kensingtonworks2> .\vs2017.ps1`.

You are now good to start running the build commands documented in the next section.

### Mac
#### Install both Xcode 8.2.1 & Xcode 11.6
Driver uses v10.12 SDK which is bundled with Xcode 8.2.1. However, user mode components require Xcode 11.6 (or newer). This is because the tools required for notarizing apps are only available from Xcode 11.6 onwards. Therefore you need to install side-by-side.

The following sections assume that you have both Xcode versions installed and have been renamed as:

- Xcode 8.2.1 is renamed to `/Applications/Xcode8.2.1.app`
- Xcode 11.6 is renamed to `/Applications/Xcode11.6.app`

#### Select Xcode Version
In Mac, Xcode installation makes Xcode & associated tools available for use
from default Terminal. However, if you have multiple versions of Xcode installed,
you would have to select the default Xcode version to use by using `xcode-select`.

Use `xcode-select -h` to list current selection as well as other available options.

#### Add SDK 10.12 support in Xcode 11.6 (tbd: DriverKit description)
Xcode 11.6 is bundled with SDK v10.15. However to support earlier versions of OSX, driver needs to be compiled with SDK v10.12 (OSX Sierra). Therefore, SDK v10.12 support needs to be added to Xcode 11.6.

The following steps outline how to add SDK 10.12 support to Xcode 11.6. This example assumes the following:

- Xcode 8.2.1 is installed and renamed to `/Applications/Xcode8.2.1.app`
- Xcode 11.6 is installed and renamed to `/Applications/Xcode11.6.app`

```
hari$ sudo su -
root# cd /Applications/Xcode11.6.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs
root# ln -s /Applications/Xcode8.2.1.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX.sdk MacOSX10.12.sdk
root# exit
```

If all the above commands were completed successfully, you can issue following command to verify that Xcode 11.6 now supports two SDKs:

```
idukki:SDKs hari$ xcodebuild -showsdks
iOS SDKs:
	iOS 13.6                      	-sdk iphoneos13.6

iOS Simulator SDKs:
	Simulator - iOS 13.6          	-sdk iphonesimulator13.6

macOS SDKs:
    DriverKit 19.0                	-sdk driverkit.macosx19.0
	macOS 10.12                   	-sdk macosx10.12
	macOS 10.15                   	-sdk macosx10.15

tvOS SDKs:
	tvOS 13.4                     	-sdk appletvos13.4

tvOS Simulator SDKs:
	Simulator - tvOS 13.4         	-sdk appletvsimulator13.4

watchOS SDKs:
	watchOS 6.2                   	-sdk watchos6.2

watchOS Simulator SDKs:
	Simulator - watchOS 6.2       	-sdk watchsimulator6.2
```

Notice how two SDKs are listed for `macOS SDKs:`. Once setup, you can use the environment variable `SDKROOT` to control which SDK the `clang` compiler would choose when building a project from commandline. This is separately documented in [Set Default SDK Version](#set-default-sdk-version) below.

Note that this variable can also be set from Xcode project settings, to override what's set in the environment variable. In fact, this is the mechanism used to force Xcode to use 10.12 SDK while building the driver. For rest
of the components SDK v10.15 is used as default.

While the mechanism listed here is specific to Xcode 11.6, it ought to work for newer versions of Xcode as well.

#### Set Default SDK Version
The clang compiler that comes with Xcode requires an environment variable to
inform it the default MacOS SDK that you wish to link the programs with. This
can be set from `.bash_profile`, such that it's always set for all Terminal
windows.

Here's how this can be set:

```
export SDKROOT=macosx10.15
```

SDK v10.15 is the SDK version that comes with Xcode11.6.

#### Install Xcode 12.3
To support Big Sur which is running on both Apple Silicon and Intel X86 platform, Xcode 12.3 is required to build a 
DriverKit driver compatible with Big Sur. Xcode 12.3 fully supports Apple Silicon and Intel X86 by `Universal App` 
style. It can't back forward support so we should build the driver separately.
Please move the downloaded Xcode.app to /Applications folder then rename it as `Xcode12.3.app` so that gulpfile can 
find this path to build.

#### Set Notarization Tool Password
`buildAll` command (see below) integrates notarization task as well. However this
requires a application specific password to be created for the Xcode notarization tool, `altool`.
This process is well documented in the document [Using app-specific passwords](https://support.apple.com/en-us/HT204397) and
can be done from Apple's developer portal.

Once this password has been created, this too
has to be configured as an environment variable so that the build script can pick it up. (Using
environment variable is the recommended approach than hardcoding the password in the source file
as then everyone who has access to the source will be able to notarize any app
using company developer account)

This can be set from `.bash_profile` as:

```
export ALTOOL_PASSWORD=<your-altool-password>
```

You may search for `ALTOOL_PASSWORD` in `gulpfile.js` to see how this environment variable
is used to compose the command line required to notarize apps.

# Build Script
Building the various components, finally resulting in an distributable
installable package is done through build automation script. JavaScript based
task runner [Gulp](https://gulpjs.com/) is used for this purposes.

A JavaScript based tool was preferred over more traditional tools such as `make`,
as node.js & JavaScript are integral to the project as GUI is entirely written
in JavaScript and therefore since there is a dependency for node.js for the project,
JavaScript based toolset made more sense to use.

Besides, JavaScript based scripts are easier to maintain than the more cryptic `makefile`.

Gulp commands are invoked using the pattern (from project root directory, where `gulpfile.js` is present):
```
gulp <command> [<options>]
```
Before proceeding with the commands listed below, it would be a good idea to read the
Gulp Quick Start guide [here](https://gulpjs.com/docs/en/getting-started/quick-start) and
try out the tutorials mentioned there.

## Build Commands
There are two sets of build commands.

One set of Helper commands that build the individual components which can be used during development phase. And another set
of Core commands that are implemented using the Helper commands to generate the final installer and related packages.

We'll look at the Core commands first as this is what you're most likely to be working with.

## Core Commands
These are the primary commands that yield the output that you can distribute. This product package is divided to primary streams -- driver & app, there are only two Core commands. One to build the driver and another to build the app (essentially user mode components)

 Command | Description | Options
---------|-------------|--------
`buildDriver` | Builds all kernel mode components, optionally signing them. In OSX signing is configured from Xcode and therefore controlled by that. In Windows,this builds both the x86 & x64 drivers. `--sign` option controls if the built drivers are to be signed or not. If `--sign` is not specified, the drivers are not signed. Note that if `--sign` is specified, you need to specify the PFX file & PFX password as options `cert` & `certpwd`, respectively. | `sign`
`buildAll` | Builds all user mode components & builds the final installer too. In OSX, this would take quite some time as it has to go through the notarization process. Only user mode components are built, as drivers packaged in the installer are taken from `./bin/{osx\|windows}/driver` folder and therefore final setup package built is not dependent on driver source changes. | `sign`, `noSetup`


## Helper Commands
These are essentially gulp tasks which are used to compose the Core commands above. These are similar to `subroutines` in conventional programming terms. Typically, once your environment is setup and functioning well, you would hardly have to use these commands. However, if you are experimenting with changes to code to a specific component, these commands will come handy.

 Command | Description | Options
---------|-------------|--------
`buildCoreDriver` | Build the kernel mode driver. Under Windows this builds both the x86 & x64 drivers. Under OSX, only x64 drivers are built.|
`buildDownloadService` | Builds the download service. Under Windows this builds both the x86 & x64 drivers. Under OSX, only x64 drivers are built.|
`buildHelper` | Build the helper middleware. (Under Windows x86 binaries are built as all user mode components are 32-bit)|
`buildGui` | Build GUI application. |
`buildRemLegacy` | Build the remove legacy component. |
`generateUpdateMetaFile` | Generates the update meta file for the latest package that can be uploaded to update server. |
`notarizeApps` | Notarize the various apps that are bundled in the Mac installer |
`stapleApps` | Staple the apps that were successfully notarized by the `notarizeApps` command |
`notarizePackage` | Notarize the finall installer that contains the notarized apps. |
`staplePackage` | Staple the package that was successfully notarized by the `notarizePackage` command |
`zipForNotarization` | Creates a ZIP file named `notarize_<m>_<n>_<p>.zip` in `dist\osx` folder. `m`, `n` & `p` stand for Major, Minor and patch number of the product. This ZIP file can be uploaded to Apple's notarization server for notarization.|
`uploadForNotarization` | Uploads the ZIP file generated by `zipForNotarization` to Apple's notarization server. Once it has been successfully uploaded, it will print the upload request UUID that can be queried for upload status.|
`waitForNotarization` | Polls the notarization server for upload status. This is a timer routine that keeps querying the notarization server until the notarization is complete. Once detected as complete, it prints the status along with any issues found. IMPORTANT: This method relies on the status messages returned by the notarization server, which are text messages. Only two status messages are detected now -- `in progress` & `success`. There could be other status messages that reflect other conditions. If those are encountered, update the script to handle them accordingly. Also if Apple updates their server to change these strings, it would break the script. Please update the script accordingly if that happens.|
`generateLangTransforms` | Generate MSI transforms for all the languages defined in `LOCALIZATION_LANGS` array declared at the top of `gulpfile.js`. |
`integrateLangTransforms` | Integrate the language transform (`*.mst`) files for all the languages defined in `LOCALIZATION_LANGS` array declared at the top of `gulpfile.js`.|
`cleanAll` | Removes the build output folder of all components relevant to the platform where the command is invoked. `buildAll` calls this command to first before proceeding with the rest of the build process. |

## Command Options
Build command options are specified using standard `gulp` command parameters specification. String arguments are
specified as `gulp --option=value` and boolean arguments as `gulp --option`.

Option | Type | Values | Description
-------|------|--------|------------
releaseType | string | {major\|minor\|patch} | Release type, which controls how version is incremented.
cert | string | <path> | Path to Windows `.pfx` file. Eg.: `--cert=e:\smallpearl-prod.pfx`
certpwd | string | <password> | Password for the `.pfx` file. Eg.: `--certpwd=<your-certificate-password>`
guiEnv | string | {development\|production} | GUI target build environment.
notarizeUuid | string | | The notarization request UUID to query status of.
sign | boolean | | Sign the binaries & setup built. In Windows, this option also requires `cert` & `certpwd` options to be specified. In OSX, all Xcode generated binaries are signed by the developer profile in Xcode project configuration & therefore this option only signs the Electron binaries.
incrVersion | boolean | | Increment version before starting the build.
noSetup | boolean | | Skip building the installer.

## Examples
### Windows
The two examples below show how the driver & the final package along with various user mode components are built.

- `gulp buildDriver --sign --cert=e:\smallpearl-prod.pfx --certpwd=<vert-password>`
- `gulp buildAll --sign --cert=e:\smallpearl-prod.pfx --certpwd=<cert-password>`

Note that the signing certificate (saved as a PFX file) is stored in a USB thumbdrive and its path is supplied to the build script through a build option. Since the certificate requires a password, it too is supplied as a build option.

### Mac
The two examples below show how the driver & the final package along with various user mode components are built.

- `gulp buildDriver`
- `gulp buildAll --sign`

Unlike in Windows, under Mac, signing is managed by Xcode and Keychain Access.

All Xcode projects in the system select the signing profile installed in Keychain Access tool. Therefore when Xcode projects are built, the final build output is signed. However, this does not apply to GUI as it's not built using Xcode. Therefore `buildAll` requires the `--sign` option to inform it that the GUI binaries need to be signed.

# Version Numbering
Product versioning is automated by the build script. Each time `buildAll` is invoked,
the version number are incremented. These version numbers are stored in the following
two files:
- version_win.json
- version_osx.json

Version numbers are split into three quadrants -- major, minor & patch. Build script commands
accept a command line argument for `releaseType` that controls which quadrant is incremented.

Currently, two independent version number streams are used by the code. One for driver and
another for the app. Independent version numbers are used as the app can typically be expected
to be updated more frequently than the drivers. Moreover, the driver package that is bundled
in the product installer is taken from `bin/{windows|osx}/driver` folder and not from its
build output folder under `driver/{windows|osx}`.

The two streams are appropriately named `driver` & `product`. Here's how the JSON file
looks:
```
{
  "product": "2.1.0",
  "driver": "2.1.1",
  "tau": "1.26.0",
  "beta": true
}
```

`tau` is the version numbering sequence for Test Automation Utility. This can be safely ignored
for this discussion.

`beta` flag controls whether the `β` symbol is added to the product version string in the GUI
(at the botton as well as in the About popup). For final release set this to `false`.

Version numbers are arbitrary numbers to start with. These can be manually updated to the requisite
version and then issue the `buildAll` command. `buildAll` auto increments the version number and updates
`version_{win|osx}.json` so that subsequent build will use a different version.

**NOTE**
A good practice is to commit & tag after each successful build such that the source code
for a specific build can be extracted at any point in time and used for troubleshooting.

Also, while working on a specific change required for a bugfix or a new feature, make sure
you work on a *development* branch of the repository. Once changes are tested and verified, they
can be merged into the `master` branch from where the new version for public release can be built.

# Adding support for new devices
Adding support for new devices involves updating the following components.

## Driver
1. Update driver inf/plist for the vendor/device id. For Windows update `driver/windows/tbwkern.inx`. For Mac, update `driver/osx/driver/Info.plist`.
For Mac, an easy approach is to copy the `<dict>` node for an existing device and update the vendor/device ids.
2. Build the driver & sign it.
3. Copy the signed drivers to `bin/<platform>/driver`. Note that for Windows there are four sets of drivers -- `x86` & `x64` for Win7 and `x86` & `x64` for Win10.

## Helper (Supporting single configuration for Bluetooth & 2.4G)
Since helper is device agnostic, in usual circumstances it doesn't need to be updated.

However, if the device supports connections via multiple interfaces (USB & Bluetooth), then to maintain a single setting across both the connection interfaces, the device id for one of the connection interface has to be mapped to the device id of the other interface. To do this:
1. Update `tbwhelper/shared/DeviceIdRemap.cpp` with the relevant entry for the device id. Currently, USB device id is taken as the reference ID for devices with dual connectivity.
2. Refer to `const DeviceRemapInfo` at the top of the file to see how Expert Wireless Trackball is supported there. Follow the pattern for new devices.

Note that single configuration support is determined using device ID only. So if a device uses a different vendor ID and its product ID,
conflicts with another existing device, the Device ID remapping mechanism has to be updated to check for vendor ID as well.

## GUI
GUI support for a device requires the following to be to be setup:
1. Name
2. ID
3. Views (Top-view and/or Side-view)
4. Button Definitions for each view
5. Images
    - Button mask images
    - Pointer & Scroll pane images
6. Tutor Page Info
    - Images
    - Help Text

All these information are defined in the file `gui/app/data/supported-devices.json`. Images - button, pointer & scroll as well as tutor images are stored
in the folder `app/images/trackball/<device-id>`. This mechanism allows adding support for any number of devices with minimum effort and of course
no changes to the code.

It's easier to describe how to add support for a new device, with an example. We can use Expert Wireless Trackball's definition for that:

```
{
    "id": 32792,
    "name": "Expert Mouse® Wireless Trackball",
    "family": "trackball",
    "topView": true,
    "sideView": false,
    "topButtons": [
        {
            "hoverIndex": 1,
            "mask": 4
        },
        {
            "hoverIndex": 2,
            "mask": 1
        },
        {
            "hoverIndex": 3,
            "mask": 3
        },
        {
            "hoverIndex": 4,
            "mask": 2
        },
        {
            "hoverIndex": 5,
            "mask": 8
        },
        {
            "hoverIndex": 6,
            "mask": 12
        }
    ],
    "scrollWheel": true,
    "connectionInterface": "bluetooth,usb",
    "tutor": {
        "usb": {
            "text": "Connect via Dongle",
            "slides": [
                {
                    "image": "tutor_usb1.jpg",
                    "hint": ""
                },
                {
                    "image": "tutor_usb2.jpg",
                    "hint": ""
                },
                {
                    "image": "tutor_usb3.jpg",
                    "hint": ""
                }
            ]
        },
        "bluetooth": {
            "text": "Connect via Bluetooth",
            "slides": [
                {
                    "image": "tutor_ble1.jpg",
                    "hint": ""
                },
                {
                    "image": "tutor_ble2.jpg",
                    "hint": "Click the Bluetooth tray icon on your screen, then Add a Bluetooth Device"
                },
                {
                    "image": "tutor_ble3.jpg",
                    "hint": ""
                },
                {
                    "image": "tutor_ble4.jpg",
                    "hint": ""
                }
            ]
        }
    }
}
```

- The `id` & `name` fields and their purpose ought to be self-explanatory. Note that on devices with multiple connection interfaces,
make sure to match the `DeviceRemapInfo` target device id to this device id.

- `family` describes the device family. In th earlier paragraph we mentioned that device images are stored in the folder `app/images/trackball/<device-id>`.
The `trackball` in this path comes from this family field value. The purpose of using the family is to allow introducing support for devices that belong
to a separate category and therefore require a different organization of its subfolder structure.

- `topView` & `sideView` field values indicate whether this device has to have both top-view and side-view in the GUI. Specifying `true` as its value for both
these fields would result in the view change button to be rendered on the top right corner of the GUI content area. Note that if either of these fields are
set to `true`, `topButtons` and/or `sideButtons`, for `topView` and & `sideView` arrays respectively, have to be initialized. In the case of Wireless Expert
Trackball, it only requires `topView` and therefore `sideView` is set to `false` and only `topButtons` is initialized.

- `scrollWheel` boolean field defines whether the device has a scroll wheel or not. If set to `false`, the Scrolling pane will be hidden for this device.

- `connectionInterface` field allows specifying the connection interfaces that the device supports. This affects the connection status displayed at the
bottom status pane of the device configuration page. Valid values for these are `bluetooth, usb` if the device supports both connection interfaces or
one of the two, if it only supports one connection interface (see definitions for other devices that only support single connection interface).

- `topButtons` defines the UI button index and the corresponding device button bitmask generated for its depress event. To understand this better
it's important know the sequence in which these buttons are defined by the UI/UX team. The following image shows this sequence.

    ![Button Sequence](button-nos.jpg "Button Sequence")

    For each required button in the image above, we have to define the corresponding device button bitmask.

    <br>This would associate the GUI button
    with the device button. With the image as a reference, you can see that in the example above, masks for all six buttons are defined. These masks correspond
    to the device button bitmask generated for that device.

    <br>Note that if a device only has two buttons, only two need to be defined. This can be
    any two depending on the device's button layout. If the mask for a button is not defined, it will be hidden.

- `sideButtons` follows the same definition scheme as `topButtons`, but for side view buttons. Again define only the buttons that are relevant to the device.

- `tutor` field defines the tutor pages to be shown for the device. These include tutor images & their message text (shown below the image). If there's no
message text for an image, define an empty string. Note that in the above example three slides are defined for `Connect via Dongle` and four slides for
`Connection via Bluetooth`.

There's no limit to the number of slides. If you want to improve the tutor page by defining additional slides, just add
new objects to the `slides` array with appropriate image & hint text for each. Note that the tutor images are stored under a device specific folder
(identified by the product ID) and therefore it won't conflict with the images for other devices.

Note that in the current source, tutor images for different devices have the same filename. This is not necessary as their names
are read from the `slides` array and therefore are fully customizable.

### GUI Hover Images
GUI, as per its design, uses different images to highlight the selected button, pointer or scroll wheel when the corresponding
action in it is selected by the user. These images are stored in the folder `gui/app/images/trackball/<deviceid>`. As you can
see these images are stored in individual folders named by the device USB product ID. This allows the design to support any
number of devices without requiring any code changes.

The image filename follows a standard convention, which allows support for new devices to be added quite easily.

This filename convention is documented in the table below.

 Filename | Description
----------|-------------
`product.png` | Product image displayed in Home page.
`product-top.png` | Product image displayed for Buttons configuration pane, top view
`product-side.png` | Product image displayed for Buttons configuration pane, side view (If device does not have a side view configration, this image is not necessary)
`product_hover_pointer.png` | Product image displayed in Pointer configuration pane
`product_hover_scroll.png` | Product image displayed in Scroll configuation pane (If scroll wheel for a device is absent, this image is not necessary)
`product_hover{n}.png` | Button hover image. `n` is the button bitmask in decimal for the specified button. So if a given button state is controlled by bit 3 (starting from bit 0), the corresponding hover image filename would be `product_hover8.png`. Another example is the Tilt Left/Right buttons. Since these buttons are controlled by bits 30 & 31, the corresponding hover image filenames are named, `product_hover1073741824.png` and `product_hover2147483648.png`, respectively.

# Localization
All the user facing components in the project are fully localizable with all
strings isoloated from the code into its own project specific string resource.
This section will briefly list the various components of the project that
requires localization and the technology used therein to achieve localization.
Note that localization support is added via industry standard technologies
and convention at the time of developing the software, ie, circa 2019.

## Components

### Common
* GUI

### Mac
* Preference Pane
* Installer
* Uninstaller

### Windows
* Download Notifier Service
* Installer

## GUI
An Electron app, localization in GUI is implemented using [i18next](https://www.i18next.com/).
All the strings to be localized are stored in the file translation.json located in `gui\app\locales\<lang>`.

#### How it works
When the app loads, it queries the system for the current locale and if the locale is one listed
as a supported locale in `AppConfig.languages` attempts to load the strings from the corresponding
folder.

Note that locales sometimes include country code too. So the locale code for Taiwan is `zh-Hant`.
In this case app would first try to find an exact match in `AppConfig.languages`. If a match is
not found app will try to find a match for the language code, which in this case is `zh`. This
mechanism allows the flexibility of providing a single translation file for all different dialects
of a language while also supporting region specific language files.

Note that if a matching translation file is not found, app will fall back to English.

#### Steps to add a new language:
1. Add the appropriate translation.json to the language specific folder
2. Add the language code to `AppConfig.languages[]` defined in `gui\src\app.config.ts`.

### Mac
#### Preference Pane
Preference pane has only one string to be localized -- the `Launch` button text. Being
a Cocoa app, it's UI is defined in a `.xib` file stored under locale specific folders.
To provide additional lanauges, create the `.lproj` folder for the required language and
translate the `Launch` button inside it (search and replace will make it clear).

You may refer to Apple's localization guide [here](https://developer.apple.com/library/archive/documentation/MacOSX/Conceptual/BPInternational/MaintaingYourOwnStringsFiles/MaintaingYourOwnStringsFiles.html) for more details.

Adding support for new languages is clearly documented [here](https://developer.apple.com/library/archive/documentation/MacOSX/Conceptual/BPInternational/InternationalizingYourUserInterface/InternationalizingYourUserInterface.html#//apple_ref/doc/uid/10000171i-CH3-SW2). (Refer to section
Enabling Base Internationalization)

#### Installer
The UI of the installer is driver by the OS and therefore it would display the OS Primary language as
set in System Preferences -> Prefered languages. However license agreement is provided by the installer
and therefore this can be localized.

To accommodate this the license file for each locale is stored separately in its own locale specifc
subfolder. These folders can be found at `setup/osx/src/Resources`. The folder names ought to be
self explanatory as to which locale each one is meant for. All these languages will be listed in the
lice agreement page, with the default being set to the OS primary locale language.

#### Uninstaller
Uninstaller is a pure Objective-C Cocoa app written using Xcode. This program only requires localization of
a handful of strings and all these have been defined in a `.lproj` file as per Cocoa programming guidelines.
To support a new language follow Apple's localization guide [here](https://developer.apple.com/library/archive/documentation/MacOSX/Conceptual/BPInternational/MaintaingYourOwnStringsFiles/MaintaingYourOwnStringsFiles.html).


Adding support for new languages is clearly documented [here](https://developer.apple.com/library/archive/documentation/MacOSX/Conceptual/BPInternational/InternationalizingYourUserInterface/InternationalizingYourUserInterface.html#//apple_ref/doc/uid/10000171i-CH3-SW2). (Refer to section
Enabling Base Internationalization)

### Windows
#### Download Notifier Service
Notifier service is a driver package component that displays a notification message to the user to download
KensingtonWorks package if it did not detect one.

The localizable element in this component is the notification message string.

This is a pure Win32 app written in C++ and the message string is loaded from the application resource file.
Follow standard Windows multiple language string resource definition techniques to support multiple languages.

#### Installer
Windows installer, an MSI file, is scripted using [WiX](https://wixtoolset.org/). WiX fully supports
localization of its UI elements as well as all its embedded strings.

The strings used in the installer itself, for messages are stored as as `.wxl` file for each locale.
So for English-US, there is `unified-en-US.wxl` and for Chinese Traditional (Taiwan), there
is `unified-zh-TW.wxl`.

Following this naming convention, you can support additional locales by creating a file for
the required locale code. You may refer to this [link](https://www.science.co.il/language/Locale-codes.php#definitions) for a list of Windows locale codes.

Note that there are two key identifiers here -- one is the LCID string (`en-US`, `zh-TW`, etc)
and the other is the LCID Decimal code (1033, 1028, respectively for the previous two). The `.wxl`
filename contains the LCID string in its filename and the file itself specifies the LCID decimal
value (Take a look inside `unified-en-US.wxl` or `unified-zh-TW.xml`).

**11/26 UPDATE**

Two new gulp tasks have been created to automate the process of creating & applying the language
transforms to the final MSI. These transforms are applied to the `en-US` MSI file.

There are two two distinct steps to make this work.

1. the WiX Visual Studio project has to include all the WiX language files that you want to
support. With this done, building the WiX project (`gulp buildSetup`) will result in the respective
MSI file being generated in `setup/windows/bin/x86/Release/<lang-tag>/` folder.

2. `gulpfile.js` contains two new tasks -- `generateLangTransforms` & `integrateLangTransforms`.
`generateLangTransforms` would iterate through all the languages defined in `LOCALIZATION_LANGS` array
declared at the top of `gulpfile.js` and generate the MSI transforms for each of these.
`integrateLangTransforms` would iterate through all the languages defined in `LOCALIZATION_LANGS` array
and integrate the respective language transform (`*.mst`) file into the the `en-US` MSI file.

So it's important that the the list of languages in the above array is kept in sync with the list of
WiX language files added to WiX Visual Studio project. Visual Studio WiX project can contain more
languages than what is listed in `LOCALIZATION_LANGS`, but not *vice-versa*.

Finally, these two new tasks have been integrated into `buildAll` command so that the whole process works
seamlessly without requiring any user intervention.

The downside to this is that the time required to complete `buildAll` increases linearly with the
number of languages support you add.

#### How MSI Multi-language support works

One important point: MSI installer, originally was designed to generate separate installer for each
locale. That is one installer for English-US, another for Chinese Traditional (Taiwan) and
so on. So by default the Visual Studio toolset for WiX would generate separeate installers for each
language string file added to the WiX project (`setup\windows\unified.wixproj`). These will be
generated to `setup\windows\bin\x86\Release\<LCID-string>`.

However some scenarios call for a single installer that embeds multiple languages and when user
starts the installer, the language that matches the system locale is automatically chosen. This
support was added later to Windows Installer through some addtional tools that is shipped
with the Windows SDK. The following section lists these steps. This is based on the steps
outlined [here](http://www.installsite.org/pages/en/msi/articles/embeddedlang/index.htm).

Steps To add support for a new language:
1. Create the WiX language file for the required locale by copying the English file. For
```
c:\trackballworks2\setup\windows> copy unified-en-US.wxl unified-zh-CN.wxl
```
2. Update it's embedded LCID code & decimal value. Translate it's contents.
3. Add the newly created file to `unified.wixproj` from Visual Studio. Now when you build the
setup using the build script command, it will create an installer in `setup\bin\x86\Release\zh-CN`.
4. Create an MSI language transform for the newly created installer(Transforms are `diff` databases
between two MSI files). This is done using the
WiX tool `torch.exe`.
```
c:\trackballworks2\setup\windows> torch -t language .\bin\x86\Release\en-US\kwsetup.msi .\bin\x86\Release\zh-CN\kwsetup.msi -out kwsetup-zh-CN.mst
```
5. You can add this file to the source repository for use later. Note that if the messages change
steps 2 to 5 have to be repeated to reflect the updates.
6. Use the following command to add the newly generated transform to the original installer.
```
c:\trackballworks2\setup\windows> wisubstg.vbs .\bin\x86\Release\en-US\kwsetup.msi .\kwsetup-zh-CN.mst 2052
```
Note how the LCID decimal is specified in the end, which matches the LCID string code.
`wisubstg.vbs` is a script from the Windows Installer SDK.

7. Update the Summary Information Stream of the MSI listing all supported languages of this package. Since the
MSI supports English-US, Chinese Traditional (Taiwan) & Chinese Simplified. So you have to use the following
command:
```
c:\trackballworks2\setup\windows> wilangid.vbs .\bin\x86\Release\en-US\kwsetup.msi Package 1033,1028,2052
```
`WiLangId.vbs` is a script from the Windows Installer SDK.

8. And you're done! `setup\windows\bin\x86\Release\en-US\kwsetup.msi` now includes support for three
locales -- English-US, Chinese Traditional (Taiwan) & Chinese Simplified.

The WiX documentation [here](https://wixtoolset.org/documentation/manual/v3/howtos/ui_and_localization/)
has details on the steps outlined above.
