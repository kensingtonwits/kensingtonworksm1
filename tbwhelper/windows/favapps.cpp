#include "stdafx.h"
#include <Shlobj.h>
#include <shellapi.h>
#include <shobjidl.h>
#include <shlguid.h>
#include <strsafe.h>
#include <set>
#include <Psapi.h>
#include <map>
#include <limits>

#define max(a,b) (((a) > (b)) ? (a) : (b))
#define min(a,b) (((a) < (b)) ? (a) : (b))
#include <gdiplus.h>
#pragma comment(lib, "gdiplus.lib")

#include <boost/log/trivial.hpp>
#include <boost/archive/iterators/base64_from_binary.hpp>
#include <boost/archive/iterators/transform_width.hpp>
#include <boost/algorithm/string.hpp>

#include "favapps.h"
#include "utils.h"

using namespace nlohmann;
namespace gp = Gdiplus;

typedef std::unordered_map<std::wstring, std::wstring> PROGRAMNAMESTABLE;
static PROGRAMNAMESTABLE s_programNames;
time_t s_lastCacheRefreshAt = 0;
// refresh time in seconds, set to once per day
const DWORD PROGRAMNAMES_CACHE_UPDATE_INTERVAL = 24 * 60 * 60;

struct App {
	std::string _program;
	std::string _name;
	std::string _icon;
	App(const char* program, const char* name)
		: _program(program), _name(name)
	{}

	int operator<(const App& rhs) const
	{
		return _program < rhs._program;
	}
};

static std::wstring getProgramName(const wchar_t* programBinaryPath);
static std::vector<std::wstring> getShortcutsFromFolder(const wchar_t* path, bool recurse);
static std::vector<std::wstring> getShortcutsFromCSIDL(int csidl);
static void dumpShortcuts(const char* header, std::vector<std::wstring>& files);
static HRESULT ResolveIt(HWND hwnd, LPCWSTR lpszLinkFile, LPWSTR lpszPath, int iPathBufferSize, LPWSTR lpszIconLocation, INT iIconBufferSize, INT* piIconIndex);
static std::wstring getLinkTarget(const wchar_t* lnkFile, std::wstring& iconLocation, int& iIconIndex);
static std::wstring getLinkName(const wchar_t* lnkFile);
static int GetEncoderClsid(const WCHAR* format, CLSID* pClsid);
static std::string getIconFromFile(const wchar_t* filePath, int index);
static void parseLinks(
	std::vector<std::wstring> const& linksToParse,
	std::set<App>& favApps,
	std::set<std::wstring> const& exclusionList,
	int maxAppCount,
	bool fDontParseIcon);
static std::wstring getAppDataFolder();

static std::wstring getAppDataFolder()
{
	PWSTR pszPath{ nullptr };
	if (::SHGetKnownFolderPath(
		FOLDERID_RoamingAppData,
		0,
		NULL,
		&pszPath) == S_OK)
	{
		std::wstring sPath(pszPath);
		::CoTaskMemFree(pszPath);
		//if (sPath.at(sPath.length() - 1) != TEXT('\\'))
		//	sPath.append(TEXT("\\"));
		return sPath;
	}
	return std::wstring();
}

static std::wstring getCommonStartMenuFolder()
{
	PWSTR pszPath{ nullptr };
	if (::SHGetKnownFolderPath(
		FOLDERID_CommonStartMenu,
		0,
		NULL,
		&pszPath) == S_OK)
	{
		std::wstring sPath(pszPath);
		::CoTaskMemFree(pszPath);
		//if (sPath.at(sPath.length() - 1) != TEXT('\\'))
		//	sPath.append(TEXT("\\"));
		return sPath;
	}
	return std::wstring();
}


std::string encode64(const std::string &val)
{
	using namespace boost::archive::iterators;
	using It = base64_from_binary<transform_width<std::string::const_iterator, 6, 8>>;
	auto tmp = std::string(It(std::begin(val)), It(std::end(val)));
	return tmp.append((3 - val.size() % 3) % 3, '=');
}

std::string toBase64(std::vector<uint8_t> data)
{
	const std::string base64_padding[] = { "", "==","=" };

	using namespace boost::archive::iterators;
	typedef std::vector<uint8_t>::const_iterator iterator_type;
	typedef base64_from_binary<transform_width<iterator_type, 6, 8> > base64_enc;
	std::stringstream ss;
	std::copy(base64_enc(data.begin()), base64_enc(data.end()), std::ostream_iterator<char>(ss));
	ss << base64_padding[data.size() % 3];
	return ss.str();
}

/*
 * Rebuilds the program name cache table that maps program binary full path to
 * it's friendly name as per the Start Menu entry created for it.
 */
void rebuildProgramNamesCache()
{
	BOOST_LOG_TRIVIAL(debug) << "Rebuilding program names cache...";

	std::set<App> apps;

	std::wstring userStartMenuFolder = getAppDataFolder();
	if (userStartMenuFolder.length() > 0)
	{
		userStartMenuFolder.append(L"\\Microsoft\\Windows\\Start Menu\\Programs");
		auto links = getShortcutsFromFolder(userStartMenuFolder.c_str(), true);
		parseLinks(links, apps, std::set<std::wstring>(), INT_MAX, true);
	}

	// common start menu folder
	auto links = getShortcutsFromFolder(getCommonStartMenuFolder().c_str(), true);
	parseLinks(links, apps, std::set<std::wstring>(), INT_MAX, true);

	s_programNames.clear();
	for (std::set<App>::const_iterator it=apps.begin(); it!=apps.end(); it++)
	{
		auto lwrFilename = tolower(it->_program);
		//BOOST_LOG_TRIVIAL(debug) << L"Inserting \"" << lwrFilename << "\" : \"" << toUTF16(it->_name) << "\" into name cache";
		s_programNames.insert(std::make_pair(toUTF16(lwrFilename), toUTF16(it->_name)));
	}
	s_lastCacheRefreshAt = std::time(nullptr);
}

/*
 * Given a progarm's full binary path, returns its friendly name by looking up
 * the program name cache that we maintain.
 *
 * If an entry does not exist there, returns the program's EXE file basename 
 * part as its name.
 */
static std::wstring getProgramName(const wchar_t* programBinaryPath)
{
	std::wstring lwrProgramBin = tolower(std::wstring(programBinaryPath));
	BOOST_LOG_TRIVIAL(debug) << "Looking up cache for the name of \"" << lwrProgramBin << "\"..";
	auto it = s_programNames.find(lwrProgramBin);
	if (it != s_programNames.end())
		return it->second;
	BOOST_LOG_TRIVIAL(debug) << "\tprogram binary not in name cache";

	std::wstring filename(programBinaryPath);
	auto lastBackSlash = filename.rfind(L'\\');
	auto name = filename.substr(lastBackSlash + 1);
	auto extensionPos = tolower(name).find(L".exe");
	if (extensionPos != std::wstring::npos)
	{
		name = name.substr(0, extensionPos);
		// camel case it
		name[0] = toupper(name.at(0));
		return name;
	}

	return std::wstring();
}

static std::vector<std::wstring> getShortcutsFromFolder(const wchar_t* path, bool recurse)
{
	std::wstring folderPath(path);
	if (folderPath.at(folderPath.length() - 1) != L'\\')
		folderPath += L"\\";

	std::vector<std::wstring> files;
#if 0
	#include <boost/filesystem.hpp>
	namespace fs = boost::filesystem;
	try
	{
		for (const auto & entry : fs::directory_iterator(wildCardPath)) {
			std::string const& filePath = entry.path().string();
			if (filePath.find(".lnk") != -1) {
				files.push_back(filePath);
			}
		}
	}
	catch (const std::exception&)
	{

	}
#else
	WIN32_FIND_DATAW ffd = { 0 };
	HANDLE hFind = INVALID_HANDLE_VALUE;
	DWORD dwError = 0;

	std::wstring wildCardPath(folderPath);
	wildCardPath.append(L"*");
	hFind = FindFirstFileW(wildCardPath.c_str(), &ffd);
	if (hFind != INVALID_HANDLE_VALUE)
	{
		do
		{
			if (!(ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
			{
				try
				{
					std::wstring filename(ffd.cFileName);
					if (filename.find(L".lnk") != std::wstring::npos)
					{
						std::wstring filePath(folderPath);
						filePath += ffd.cFileName;
						files.push_back(filePath);
					}
				}
				catch (std::exception& e)
				{
					BOOST_LOG_TRIVIAL(warning) << "Error while retrieving file name: " << e.what();
				}
			}
			else
			{
				if (recurse && ::wcscmp(ffd.cFileName, L".") != 0 && ::wcscmp(ffd.cFileName, L"..") != 0)
				{
					std::wstring subFolderPath = folderPath;
					subFolderPath.append(ffd.cFileName);
					auto subFolderLinks = getShortcutsFromFolder(subFolderPath.c_str(), recurse);
					files.insert(files.end(), subFolderLinks.begin(), subFolderLinks.end());
				}
			}
		} while (FindNextFileW(hFind, &ffd));
	}
	dwError = ::GetLastError();
	if (dwError != ERROR_NO_MORE_FILES)
	{
		BOOST_LOG_TRIVIAL(warning) << "Error enumerating files, rc: " << dwError;
	}

	::FindClose(hFind);
#endif

	return files;
}

static std::vector<std::wstring> getShortcutsFromCSIDL(int csidl)
{
	WCHAR workingPath[MAX_PATH * 2] = { 0 };
	if (SUCCEEDED(SHGetFolderPathW(
		NULL,
		csidl,
		NULL,
		SHGFP_TYPE_CURRENT,
		workingPath)))
	{
		return getShortcutsFromFolder(workingPath, false);
	}
	return std::vector<std::wstring>();
}

static void dumpShortcuts(const char* header, std::vector<std::wstring>& files)
{
	BOOST_LOG_TRIVIAL(debug) << header;
	for (auto file : files)
	{
		try
		{
			BOOST_LOG_TRIVIAL(debug) << file;
		}
		catch (const std::exception&)
		{
		}
	}
}

// From https://docs.microsoft.com/en-us/windows/win32/shell/links#resolving-a-shortcut
// ResolveIt - Uses the Shell's IShellLink and IPersistFile interfaces 
//             to retrieve the path and description from an existing shortcut. 
//
// Returns the result of calling the member functions of the interfaces. 
//
// Parameters:
// hwnd         - A handle to the parent window. The Shell uses this window to 
//                display a dialog box if it needs to prompt the user for more 
//                information while resolving the link.
// lpszLinkFile - Address of a buffer that contains the path of the link,
//                including the file name.
// lpszPath     - Address of a buffer that receives the path of the link 
//				  target, including the file name.
// lpszDesc     - Address of a buffer that receives the description of the 
//                Shell link, stored in the Comment field of the link
//                properties.

static HRESULT ResolveIt(HWND hwnd, LPCWSTR lpszLinkFile, LPWSTR lpszPath, int iPathBufferSize, LPWSTR lpszIconLocation, INT iIconBufferSize, INT* piIconIndex)
{
	HRESULT hres;
	IShellLink* psl;
	WCHAR szGotPath[MAX_PATH];
	WCHAR szDescription[MAX_PATH];
	WIN32_FIND_DATA wfd;

	*lpszPath = 0; // Assume failure 

	// Get a pointer to the IShellLink interface. It is assumed that CoInitialize
	// has already been called. 
	hres = CoCreateInstance(CLSID_ShellLink, NULL, CLSCTX_INPROC_SERVER, IID_IShellLink, (LPVOID*)&psl);
	if (SUCCEEDED(hres))
	{
		IPersistFile* ppf;

		// Get a pointer to the IPersistFile interface. 
		hres = psl->QueryInterface(IID_IPersistFile, (void**)&ppf);

		if (SUCCEEDED(hres))
		{
			WCHAR wsz[2048] = { 0 };
			::wcscpy_s(wsz, lpszLinkFile);

			// Add code here to check return value from MultiByteWideChar 
			// for success.

			// Load the shortcut. 
			hres = ppf->Load(wsz, STGM_READ);

			if (SUCCEEDED(hres))
			{
				// Resolve the link. 
				

				// no UI & timeout of 1 millisecond
				hres = psl->Resolve(hwnd, MAKELPARAM(SLR_NO_UI, 1));
				if (SUCCEEDED(hres))
				{
					// Get the path to the link target. 
					hres = psl->GetPath(szGotPath, MAX_PATH, (WIN32_FIND_DATA*)&wfd, 0);

					if (SUCCEEDED(hres))
					{
						// Get the description of the target. 
						hres = psl->GetDescription(szDescription, MAX_PATH);

						if (SUCCEEDED(hres))
						{
							hres = StringCbCopy(lpszPath, iPathBufferSize, szGotPath);
							if (SUCCEEDED(hres))
							{
								// Handle success
							}
							else
							{
								// Handle the error
							}

							// get icon location
							if (lpszIconLocation) {
								*piIconIndex = 0;
								lpszIconLocation[0] = L'\0';
								psl->GetIconLocation(lpszIconLocation, iIconBufferSize, piIconIndex);
							}
						}


					}
				}
			}

			// Release the pointer to the IPersistFile interface. 
			ppf->Release();
		}

		// Release the pointer to the IShellLink interface. 
		psl->Release();
	}
	return hres;
}

// Returns the target of a link file.
static std::wstring getLinkTarget(const wchar_t* lnkFile, std::wstring& iconLocation, int& iIconIndex)
{
	WCHAR szTarget[2048] = { 0 }, szIconLocation[2048] = { 0 };
	if (SUCCEEDED(ResolveIt(
		NULL,
		lnkFile,
		szTarget,
		sizeof(szTarget) / sizeof(szTarget[0]),
		szIconLocation,
		sizeof(szIconLocation) / sizeof(szIconLocation[0]),
		&iIconIndex
	)))
	{
		iconLocation = szIconLocation;
		return std::wstring(szTarget);
	}
	return std::wstring();
}

static std::wstring getLinkName(const wchar_t* lnkFile)
{
	std::wstring name;
	const wchar_t * pc = ::wcsrchr(lnkFile, L'\\');
	if (pc)
	{
		name = pc + 1;
		auto extPos = name.rfind(L".lnk");
		if (extPos != std::wstring::npos) {
			name = name.substr(0, extPos);
		}
	}
	return name;
}

// GDI+ code to get the encoder from its CSLID
// GDI+ should've been initialize!
static int GetEncoderClsid(const WCHAR* format, CLSID* pClsid)
{
	UINT  num = 0;          // number of image encoders
	UINT  size = 0;         // size of the image encoder array in bytes

	gp::ImageCodecInfo* pImageCodecInfo = NULL;

	gp::GetImageEncodersSize(&num, &size);
	if (size == 0)
		return -1;  // Failure

	pImageCodecInfo = (gp::ImageCodecInfo*)(malloc(size));
	if (pImageCodecInfo == NULL)
		return -1;  // Failure

	gp::GetImageEncoders(num, size, pImageCodecInfo);

	for (UINT j = 0; j < num; ++j)
	{
		if (wcscmp(pImageCodecInfo[j].MimeType, format) == 0)
		{
			*pClsid = pImageCodecInfo[j].Clsid;
			free(pImageCodecInfo);
			return j;  // Success
		}
	}

	free(pImageCodecInfo);
	return -1;  // Failure
}

// Extracts an icon at the given index from the given file
// and returns its content as base64 encoded string
static std::string getIconFromFile(const wchar_t* filePath, int index)
{
	//BOOST_LOG_TRIVIAL(debug) << L"Extracting icon from " << filePath;

	auto hIcon = ::ExtractIcon(NULL, filePath, (UINT)index);
	if (hIcon) {
		// convert the icon to 32x32
		std::unique_ptr<gp::Bitmap> bmp(gp::Bitmap::FromHICON(hIcon));
		//BOOST_LOG_TRIVIAL(debug) << "Icon size: " << bmp->GetWidth() << ", " << bmp->GetHeight();
		// Create a stream to store the PNG data for the bitmap
		IStream* pStream = ::SHCreateMemStream(NULL, 0);
		if (pStream)
		{
			CLSID encoderClsid;
			GetEncoderClsid(L"image/png", &encoderClsid);
			auto status = bmp->Save(pStream, &encoderClsid);
			if (status == gp::Ok)
			{
				STATSTG streamStat = { 0 };
				pStream->Stat(&streamStat, STATFLAG_DEFAULT);
				//BOOST_LOG_TRIVIAL(debug) << "Stream size " << streamStat.cbSize.LowPart;
				if (streamStat.cbSize.LowPart > 0)
				{
					std::vector<uint8_t> buf(streamStat.cbSize.LowPart);
					ULONG cbRead = 0;
					LARGE_INTEGER li = { 0 };
					pStream->Seek(li, STREAM_SEEK_SET, NULL);
					pStream->Read(&buf[0], streamStat.cbSize.LowPart, &cbRead);
					if (cbRead == streamStat.cbSize.LowPart)
					{
						// contentshave been read to our buffer!
						//BOOST_LOG_TRIVIAL(debug) << "Stream contents read to our buffer";
						return toBase64(buf);
					}
					else
					{

					}
				}
				else
				{
					//BOOST_LOG_TRIVIAL(debug) << "Icon size == 0";
				}
			}
			pStream->Release();
		}

		::DestroyIcon(hIcon);
	}

	return std::string();
}

void to_json(json& j, App const& pgm)
{
	j["program"] = pgm._program;
	j["name"] = pgm._name;
	j["icon"] = pgm._icon;
}

// not necessary as we won't er deserializing
void from_json(json const & j, App& pgm)
{
}

/*
 * Adds a system specific programs to the exclusion list so that they 
 * are filtered out of the final returned list.
 */
void addSystemProgramsToExclusionList(std::set<std::wstring> exclusionList)
{
	exclusionList.insert(L"c:\\windows\\system32\\control.exe");
	exclusionList.insert(L"c:\\windows\\system32\\rundll32.exe");
}

// From device file name to DOS filename
BOOL GetFsFileName(LPCTSTR lpDeviceFileName, std::wstring& fsFileName)
{
	BOOL rc = FALSE;

	TCHAR lpDeviceName[4096] = { 0 };
	TCHAR lpDrive[3] = _T("A:");

	// Iterating through the drive letters
	for (TCHAR actDrive = _T('A'); actDrive <= _T('Z'); actDrive++)
	{
		lpDrive[0] = actDrive;

		// Query the device for the drive letter
		if (QueryDosDevice(lpDrive, lpDeviceName, 0x1000) != 0)
		{
			// Network drive?
			if (_tcsnicmp(_T("\\Device\\LanmanRedirector\\"), lpDeviceName, 25) == 0)
			{
				//Mapped network drive 

				TCHAR cDriveLetter;
				DWORD dwParam;

				TCHAR lpSharedName[4096] = { 0 };

				if (_stscanf(lpDeviceName,
					_T("\\Device\\LanmanRedirector\\;%c:%d\\%s"),
					&cDriveLetter,
					&dwParam,
					lpSharedName) != 3)
					continue;

				_tcscpy(lpDeviceName, _T("\\Device\\LanmanRedirector\\"));
				_tcscat(lpDeviceName, lpSharedName);
			}

			// Is this the drive letter we are looking for?
			if (_tcsnicmp(lpDeviceName, lpDeviceFileName, _tcslen(lpDeviceName)) == 0)
			{
				fsFileName = lpDrive;
				fsFileName += (LPCTSTR)(lpDeviceFileName + _tcslen(lpDeviceName));

				rc = TRUE;

				break;
			}
		}
	}

	return rc;
}

bool isSystemProcess(const wchar_t* filename)
{
	const wchar_t* SYSTEM_PROCESSES[] = {
		L"applicationframehost.exe",
		L"explorer.exe",
		L"rundll32.exe",
		L"windows\\system32\\control.exe",
		L"tbwhelper.exe",
		L"kensingtonworks.exe"
	};
	auto lwrFilename = tolower(std::wstring(filename));
	for (size_t i = 0; i < sizeof(SYSTEM_PROCESSES)/sizeof(SYSTEM_PROCESSES[0]); i++)
	{
		if (lwrFilename.find(SYSTEM_PROCESSES[i]) != std::wstring::npos) {
			return true;
		}
	}
	return false;
}

struct WindowEnumeratorArgs {
	std::set<App>& _apps;
	std::set<std::wstring> const& _exclusionList;
	int _maxAppCount;
	WindowEnumeratorArgs(std::set<App>& apps, std::set<std::wstring> const& exclusionList, int maxCount)
		: _apps(apps), _exclusionList(exclusionList), _maxAppCount(maxCount)
	{}
};

BOOL CALLBACK EnumDesktopWindowProc(HWND hWnd, LPARAM lParam)
{
	WindowEnumeratorArgs* pArgs = reinterpret_cast<WindowEnumeratorArgs*>(lParam);
	if (pArgs->_apps.size() == pArgs->_maxAppCount)
		return FALSE;

	// filter 1 - skip windows which are not visible
	if (!::IsWindowVisible(hWnd))
		return TRUE;

	// filter 2 - skip windows which do not have a window text
	WCHAR szText[1024] = { 0 };
	::GetWindowText(hWnd, szText, sizeof(szText) / sizeof(szText[0]));
	if (::wcslen(szText) == 0)
		return TRUE;

	// filter 3 - skip all non-frame windows
	DWORD dwStyle = ::GetWindowLong(hWnd, GWL_STYLE);
	DWORD dwStyleEx = ::GetWindowLong(hWnd, GWL_EXSTYLE);
	if (!(dwStyleEx & WS_EX_APPWINDOW) && !(dwStyle & WS_OVERLAPPEDWINDOW))
		return TRUE;

	// filter 4 - skip all zero sized windows
	RECT rc; ::GetWindowRect(hWnd, &rc);
	if ((rc.right - rc.left) == 0 || (rc.bottom - rc.top) == 0)
		return TRUE;

	DWORD pid = 0;
	DWORD tid = ::GetWindowThreadProcessId(hWnd, &pid);
	GUITHREADINFO gui = { 0 };
	gui.cbSize = sizeof(gui);
	if (!::GetGUIThreadInfo(tid, &gui))
		return TRUE;

	HANDLE hProcess = OpenProcess(GENERIC_READ, FALSE, pid);
	if (hProcess)
	{
		WCHAR szImage[1024] = { 0 };
		if (::GetProcessImageFileName(hProcess, szImage, sizeof(szImage) / sizeof(szImage[0])) > 0)
		{
			std::wstring filename;
			GetFsFileName(szImage, filename);
			std::wstring lwrFilename = tolower(filename);
			// filter 4 - skip system processes
			if (isSystemProcess(lwrFilename.c_str()))
				return TRUE;

			if (pArgs->_exclusionList.find(lwrFilename) != pArgs->_exclusionList.end())
				return TRUE;

			//OutputDebugStringW(filename.c_str());
			//OutputDebugStringW(L"\n");
			std::wstring name = getProgramName(filename.c_str());
			/*
			auto lastBackSlash = filename.rfind(L'\\');
			auto name = filename.substr(lastBackSlash+1);
			auto extensionPos = tolower(name).find(L".exe");
			if (extensionPos != std::wstring::npos)
			{
				name = name.substr(0, extensionPos);
				// camel case it
				name[0] = toupper(name.at(0));
			}
			*/
			App app(toUTF8(filename).c_str(), toUTF8(name).c_str());
			app._icon = getIconFromFile(filename.c_str(), 0);
			if (app._icon.length() > 0)
			{
				BOOST_LOG_TRIVIAL(debug) << "Inserting active process " << app._program;
				pArgs->_apps.insert(app);
			}
		}
		::CloseHandle(hProcess);
	}
	return TRUE;
}

/*
 * Enumerates active frame windows and returns the process paths for each
   of them.
 */
void enumActiveFrameWindowApps(std::set<App>& apps, std::set<std::wstring> const& exclusionList, int maxAppCount)
{
	WindowEnumeratorArgs enumArgs(apps, exclusionList, maxAppCount);

	HDESK hDesk = OpenDesktop(L"default", 0, FALSE, GENERIC_READ);
	if (hDesk)
	{
		::EnumDesktopWindows(hDesk, EnumDesktopWindowProc, (LPARAM)&enumArgs);
		::CloseDesktop(hDesk);
	}
}

static void parseLinks(
	std::vector<std::wstring> const& linksToParse, 
	std::set<App>& favApps,
	std::set<std::wstring> const& exclusionList,
	int maxAppCount,
	bool fDontParseIcon)
{
	int index = (int)favApps.size();
	for (auto linkFile : linksToParse)
	{
		std::wstring iconLocation; int iIconIndex = 0;
		auto target = getLinkTarget(linkFile.c_str(), iconLocation, iIconIndex);
		//BOOST_LOG_TRIVIAL(debug) << L"Target for \"" << linkFile << L"\": " << target;
		auto itarget = tolower(target);
		std::wstring::size_type pos = itarget.find(L".exe");
		if (pos == (itarget.length() - 4)
			&& exclusionList.find(itarget) == exclusionList.end()
			&& !isSystemProcess(itarget.c_str())
			)
		{
			auto name = getLinkName(linkFile.c_str());
			App app(toUTF8(target).c_str(), toUTF8(name).c_str());
			if (!fDontParseIcon)
			{
				if (iconLocation.length() > 0 && iIconIndex != -1)
				{
					app._icon = getIconFromFile(iconLocation.c_str(), iIconIndex);
				}
				else
				{
					app._icon = getIconFromFile(target.c_str(), 0);
				}
				if (app._icon.length() > 0)
					favApps.insert(app);
			}
			else
			{
				favApps.insert(app);
			}

			if (++index >= maxAppCount)
				break;
		}
	}
}

static void convertLinkNamesToProgramNames(std::set<App>& favApps)
{
	// Sometimes app list can generate document shortcuts on the desktop. 
	// In this case the link name would be the document name or something
	// friendly that the user has given it. We need to change this to the
	// program's name, which is more relevant to our program.
	for (std::set<App>::iterator it=favApps.begin(); it!=favApps.end(); it++)
	{
		App& app = const_cast<App&>(*it);
		app._name = toUTF8(getProgramName(toUTF16(app._program).c_str()));
	}
}

/*
 * Returns a JSON object which is an array of program informations:
 *	{
 *		"program": <path>
 *		"name": <friendly name>
 *		"icon": <icon base64 encoded>
 *  }
 *
 */
nlohmann::json getFavApps(int maxAppCount, const std::set<std::string>& exclude)
{
	json jApps = json::array();

	::CoInitialize(NULL);

	gp::GdiplusStartupInput gdiplusStartupInput;
	ULONG_PTR gdiplusToken;
	gp::GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);

	// check if we should refresh the program names cache
	time_t now = std::time(nullptr);
	if (now != -1 && ((now-s_lastCacheRefreshAt) > PROGRAMNAMES_CACHE_UPDATE_INTERVAL))
	{
		rebuildProgramNamesCache();
	}

	// convert exclusion list to vector of strings
	std::set<std::wstring> exclusionList;
	addSystemProgramsToExclusionList(exclusionList);
	for (auto pgm : exclude)
	{
		exclusionList.insert(tolower(toUTF16(pgm)));
	}

	std::set<App> favApps; // use a set to prevent duplicate apps
	enumActiveFrameWindowApps(favApps, exclusionList, maxAppCount);

	if (favApps.size() < maxAppCount)
	{
		std::vector<std::wstring> linksToParse;
		auto links = getShortcutsFromCSIDL(CSIDL_DESKTOPDIRECTORY);
		//dumpShortcuts("User Desktop:-", links);
		parseLinks(links, favApps, exclusionList, maxAppCount, false);
	}

	// User start menu - without recursion
	std::wstring userStartMenuFolder = getAppDataFolder();
	if (userStartMenuFolder.length() > 0)
	{
		if (favApps.size() < maxAppCount)
		{
			userStartMenuFolder.append(L"\\Microsoft\\Windows\\Start Menu\\Programs");
			auto links = getShortcutsFromFolder(userStartMenuFolder.c_str(), false);
			parseLinks(links, favApps, exclusionList, maxAppCount, false);
		}
		// with recursion
		if (favApps.size() < maxAppCount)
		{
			auto links = getShortcutsFromFolder(userStartMenuFolder.c_str(), true);
			parseLinks(links, favApps, exclusionList, maxAppCount, false);
		}
	}

	// Common Start Menu - without recursion
	if (favApps.size() < maxAppCount)
	{
		auto links = getShortcutsFromFolder(getCommonStartMenuFolder().c_str(), false);
		//dumpShortcuts("Start Menu:-", links);
		parseLinks(links, favApps, exclusionList, maxAppCount, false);
	}

	// Common Start Menu - with recursion
	if (favApps.size() < maxAppCount)
	{
		auto links = getShortcutsFromFolder(getCommonStartMenuFolder().c_str(), true);
		//dumpShortcuts("Start Menu children:-", links);
		parseLinks(links, favApps, exclusionList, maxAppCount, false);
	}

	convertLinkNamesToProgramNames(favApps);
	for (auto app : favApps)
	{
		jApps.push_back(app);
	}

	gp::GdiplusShutdown(gdiplusToken);
	return jApps;
}
