#pragma once

#ifdef _WINDOWS
#include <windows.h>

// Some global variables
extern HINSTANCE g_hInstance;               // current instance
extern HWND g_hMainWnd;                     // main window
extern HWND g_hWndDeviceManager;			// device manager window;
extern wchar_t TBHELPER_WND_CLASS_NAME[];   // Window class name

#else

#pragma error("TODO")

#endif