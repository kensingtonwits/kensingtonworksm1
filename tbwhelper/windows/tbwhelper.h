#pragma once

#include "resource.h"

#define HELPERICON_ON						0
#define HELPERICON_OFF						1
#define HELPERICON_ERROR_WRONGKERNELVERSION	2
#define HELPERICON_ERROR_NOSETTINGS			3

void HelperForceReloadPreferences(void);
