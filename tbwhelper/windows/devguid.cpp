/*
 * devguid.cpp
 *
 *  Only purpose of this file is to create the global device GUID variables that
 *  are declared in TbwKernelAPI.h and are referred to in DeviceManager.cpp.
 *  Without this, the linker will throw an 'unresolved symbol' error.
 *
 *  Note that this file should not use precompiled headers and INITGUID macro
 *  has to be declared at the top before guiddef.h is included to convert the 
 *  DEFINE_GUID macros into actual variable definitions.
 */

#define INITGUID
#include <guiddef.h>

// {4F1FD8B4-05A8-4E91-B42C-8942B49AE606}
DEFINE_GUID(GUID_BUS_KNSTBW, 0x4f1fd8b4, 0x5a8, 0x4e91, 0xb4, 0x2c, 0x89, 0x42, 0xb4, 0x9a, 0xe6, 0x6);
#define TBW_DEVICE_ID L"{4F1FD8B4-05A8-4E91-B42C-8942B49AE606}\\KNSTBW\0"

// {1A629312-9256-4ED3-83CB-F5FE65F5E03A}
DEFINE_GUID(GUID_DEVINTERFACE_KNSTBW, 0x1a629312, 0x9256, 0x4ed3, 0x83, 0xcb, 0xf5, 0xfe, 0x65, 0xf5, 0xe0, 0x3a);

/*
Old tbwkern 1.x PDO device id
// {AF111662-3D80-43ac-B39B-D7B15ACB0B54}
DEFINE_GUID(GUID_BUS_KNSTBW, 0xaf111662, 0x3d80, 0x43ac, 0xb3, 0x9b, 0xd7, 0xb1, 0x5a, 0xcb, 0xb, 0x54);
#define TBW_DEVICE_ID L"{AF111662-3D80-43ac-B39B-D7B15ACB0B54}\\KNSTBW\0"


// {1D0785A7-8CF1-46d9-9643-6A3A99EB21A0}
DEFINE_GUID(GUID_DEVINTERFACE_KNSTBW, 0x1d0785a7, 0x8cf1, 0x46d9, 0x96, 0x43, 0x6a, 0x3a, 0x99, 0xeb, 0x21, 0xa0);
*/