#pragma once

/*
 * Returns a boolean indicating if another instance of this app exists.
 */
bool anotherInstanceExists();
