/*
    Class to migrate legacy TbwSettings.xml to new TbwSettings.json format.
*/
#pragma once

#include <map>
#include <string>

#include "json.hpp"
using namespace nlohmann;

#include "rapidxml-1.13/rapidxml.hpp"
namespace rx = rapidxml;
typedef rx::xml_node<TBCHAR> XMLNODE;

class Settings;
class DeviceConfig;

class SettingsMigrator {

    struct KeystrokeCommand {
        uint32_t modifiers;
        uint32_t repeat;
        std::string macro;
        std::string json;

        KeystrokeCommand()
            : modifiers(0), repeat(0), macro(), json()
        {}
    };
    typedef std::multimap<uint32_t, KeystrokeCommand> KEYSTROKECOMMANDS; 
    KEYSTROKECOMMANDS _keystrokeCommands;

public:
    SettingsMigrator();

    bool migrate();

private:
    void loadActionMacros();
    void migrateApplicationConfig(Settings&, XMLNODE* pNode);
    void migrateDeviceConfig(Settings&, const char* app, const char* appFilePath, XMLNODE* pNode);
    void migrateAction(DeviceConfig& config, XMLNODE* pDevNode, const TBCHAR* pszButtonName);
};
