#include "stdafx.h"
#include <tlhelp32.h>
#include <vdmdbg.h>
#include <psapi.h>

#include <sstream>
#include <iostream>
#include <functional>
#include <iomanip>

#include <boost/log/trivial.hpp>
#include <boost/algorithm/string.hpp>

#include "DeviceManager.h"
#include "Settings.h"
#include "App.h"
#include "utils.h"
#include "PlatformServices.h"
#include "tbwMshm.h"

HWND g_hWndDeviceManager = NULL;

#define DEVMGR_OBJECT_WINDOW_CLASS L"DeviceMgrMsgWindowClass"

// Test instrumentation support
#define TEST_INSTRUMENT_PRODUCT_NAME    L"Kensington Test Trackball"
#define TEST_INSTRUMENT_PRODUCT_ID      65535

// Message handler reloads settings
// wParam - Not used
// lParam - Not used
#define WM_RELOAD_SETTINGS          WM_USER + 0x0000

// WPARAM = button down mask
// LPARAM = button up mask
#define WM_EMULATE_BUTTON_ACTIVITY  WM_USER + 0x0001


// We only use one timer in the app. So strictly speaking this is not 
// necessary. But for sake of clarity we define it anyway.
const UINT_PTR TIMER_POLLFORCHANGES = 1000;
const UINT TIMER_POLLFORCHANGES_INTERVAL = 300;

static const wchar_t* LOG_TAG = L"DevMgr";

using namespace std;

class SettingsFileChangeMonitor {
    HANDLE _h;
    time_t _lastModifiedTime;
public:
    SettingsFileChangeMonitor()
        : _h(NULL)
        , _lastModifiedTime(0)
    {}
    ~SettingsFileChangeMonitor()
    {
        FindCloseChangeNotification(_h);
    }
    void start(DeviceManager* pMgr)
    {
        Settings::getFilename();
        _h = ::FindFirstChangeNotification(
            toUTF16(Settings::getFileFolder()).c_str(),
            FALSE,
            FILE_NOTIFY_CHANGE_LAST_WRITE);
        if (_h == INVALID_HANDLE_VALUE)
            throw std::exception("Error registering file change notification");

        _lastModifiedTime = getSettingsFileTimeStamp();

        if (!pMgr->AddWaitHandle(
            _h,
            std::bind(&SettingsFileChangeMonitor::handleChangeNotification,
                this)))
        {
            throw std::exception("Error binding overlapped io with completion handler");
        }

    }
    operator HANDLE() const
    {
        return _h;
    }

    // Notification change handler
    // Note that file change notification is registered on a folder and not 
    // on a file. So if user creates a new file in the same folder, the 
    // notification will be raised. To counter these scenarios, we check
    // modified time of the settings file against the last recorded time
    // to determine if TbwSettings.xml was indeed modified. And only if we
    // can confirm that the file as modified, we notify App instance of the
    // event which triggers the settings reload process.
    void handleChangeNotification()
    {
        auto mtime = getSettingsFileTimeStamp();
        if (mtime != _lastModifiedTime)
        {
            BOOST_LOG_TRIVIAL(info) << "Settings file change detected";
            DeviceManager::getInstance()->handleSettingsFileChange();
            _lastModifiedTime = mtime;
        }
        FindNextChangeNotification(_h);
    }

    // Returns the last modified time of the settings file if it exists.
    // It it does not exist returns 0;
    time_t getSettingsFileTimeStamp()
    {
        struct _stat stat = { 0 };
        if (::_stat64i32(Settings::getFileFullPath().c_str(), &stat) == 0)
            return stat.st_mtime;
        return 0;
    }
};

// Return executable filename given its process Id.
// If fStripPath is set to true, returns the executable filename after stripping
// the folder path where it's located.
static std::wstring GetProcessNameFromId(DWORD dwProcessId, BOOL fStrip)
{
    std::vector<WCHAR> buf(32768);
    DWORD dwBufLen = (DWORD)buf.size();

    CHandle hProcess(OpenProcess(
        PROCESS_QUERY_INFORMATION | READ_CONTROL,
        FALSE,
        dwProcessId));

    if (!hProcess)
        return L"";

    if (!::QueryFullProcessImageName(
        hProcess,
        0,
        &buf[0],
        &dwBufLen))
        return L"";

    std::wstring name(&buf[0]);
    if (fStrip)
    {
        auto backslash = name.rfind(L'\\');
        if (backslash != std::wstring::npos)
            name = name.substr(backslash + 1);
    }
    return name;
}

DeviceManager* DeviceManager::_instance = nullptr;

DeviceManager* DeviceManager::getInstance()
{
	if (nullptr == DeviceManager::_instance)
	{
        DeviceManager::_instance = new DeviceManager();
		//tbwMshm::tbwMshm_create(boost::detail::winapi::GetCurrentProcessId());
		tbwMshm::tbwMshm_setup(boost::detail::winapi::GetCurrentProcessId());
	}
    return DeviceManager::_instance;
}

void DeviceManager::start()
{
    _testMode = false;
    MsgWFMOHandler::Start();
    ::WaitForSingleObject(_initDone, INFINITE);
    if (!_initSuccess) {
        // initializeation failure, throw error
        throw std::runtime_error("Error initializing device manager");
    }
}

void DeviceManager::stop()
{
    MsgWFMOHandler::Stop();
    _devices.clear();
    delete this;
    _instance = nullptr;
}

DeviceManager::DeviceManager()
    : _hWnd(NULL)
    , _hPnPNotification(NULL)
    //, _changeMonitor(new SettingsFileChangeMonitor())
    , _hWndLastForeground(NULL)
    , _modifierStatus(0)
    , _testMode(false)
    , _initSuccess(false)
    , _initDone(TRUE, FALSE)
{
}

DeviceManager::~DeviceManager()
{
	BOOST_LOG_TRIVIAL(info) << "~DeviceManager()";
	//tbwMshm::tbwMshm_remove();
}

void DeviceManager::init(bool fTestMode/* = false*/)
{
    try
    {
        auto pServices = PlatformServices::getInstance();
        assert(pServices != nullptr);

        // register a dummy window class for 
        WNDCLASSEXW wcex = { 0 };
        wcex.cbSize = sizeof(WNDCLASSEX);
        wcex.lpfnWndProc = DeviceManager::_WndProc;
        wcex.lpszClassName = DEVMGR_OBJECT_WINDOW_CLASS;

        // TODO: need to signal error condition to invoking thread!!
        if (RegisterClassExW(&wcex))
        {
            _hWnd = CreateWindowW(
                DEVMGR_OBJECT_WINDOW_CLASS,
                L"",
                WS_OVERLAPPEDWINDOW,
                CW_USEDEFAULT, 0,
                320, 240,
                HWND_MESSAGE,
                nullptr,
                NULL,
                nullptr);
        }

        if (!_hWnd)
		{
            throw std::runtime_error("Error creating device manager object window");
        }

		g_hWndDeviceManager = _hWnd;

        _settings.reset(new Settings(true));

        // start the change monitor
        if (_changeMonitor)
            _changeMonitor->start(this);

        // enumerate devices
        enumerateAttachedDevices();

        DEV_BROADCAST_DEVICEINTERFACE dbi = { 0 };

        dbi.dbcc_size = sizeof(DEV_BROADCAST_DEVICEINTERFACE);
        dbi.dbcc_devicetype = DBT_DEVTYP_DEVICEINTERFACE;
        dbi.dbcc_classguid = GUID_DEVINTERFACE_KNSTBW;

        _hPnPNotification = RegisterDeviceNotification(
            _hWnd,
            &dbi,
            DEVICE_NOTIFY_WINDOW_HANDLE | DEVICE_NOTIFY_ALL_INTERFACE_CLASSES);

        // Start the timer to track changes in foreground process
        // and also state of modifier keys.
        SetTimer(_hWnd, TIMER_POLLFORCHANGES, TIMER_POLLFORCHANGES_INTERVAL, NULL);

        _initSuccess = true;
    }
    catch (const std::exception& e)
    {
        BOOST_LOG_TRIVIAL(error) << "Error initializing device manager, reason: " << e.what();
    }
    catch (...)
    {
        BOOST_LOG_TRIVIAL(error) << "Unknown error initializing device manager";
    }

    _initDone.Set();
#if 0
    if (fTestMode)
    {
        BOOST_LOG_TRIVIAL(debug)
            << "Test instrumentation mode ON";

        DEVICEPTR baseDevice(new TestDevice(this));
        Device* device = dynamic_cast<Device*>(baseDevice.get());
        device->init();
        _devices.push_back(baseDevice);

        App* pApp = App::getInstance();
        _settings->createDefaultDeviceConfigIfDoesntExist(device->id());

        // Getting a reference to a transient Setttings object in App might look
        // unsafe. But remember that our app runs on a single thread. So we don't
        // have to worry about the object being invalidated from another thread
        // owing to external factors such as changes to settings from the UI.
        //
        // When and if we make this a multithreaded app, then we we would have
        // wrap this method in a global critical section, which the App class
        // would also have to acquire to change its _settings member thereby
        // ensuring that App object & DeviceManager (and its Device children)
        // all refer to the same Settings object at all times.
        //
        // Also note that when _settings member is changed (or updated), App 
        // object would reinitialize the DeviceConfig member of all Device 
        // instances with the newly created object.
        device->setConfig(
            _settings->getDeviceConfig(
                getForegroundAppName().c_str(),
                device->id(),
                true
            ),
            true
        );
    }
#endif
}

void DeviceManager::enumerateAttachedDevices()
{
    HDEVINFO                            hardwareDeviceInfo;
    SP_INTERFACE_DEVICE_DATA            deviceInfoData = { 0 };
    ULONG                               i = 0;
    BOOL	                            done = FALSE;
    PSP_INTERFACE_DEVICE_DETAIL_DATA    DeviceData = NULL;

    hardwareDeviceInfo = SetupDiGetClassDevs(
        &GUID_DEVINTERFACE_KNSTBW,
        NULL,
        NULL, 
        (DIGCF_PRESENT | DIGCF_DEVICEINTERFACE));

    if (hardwareDeviceInfo == INVALID_HANDLE_VALUE)
        return;

    deviceInfoData.cbSize = sizeof(SP_INTERFACE_DEVICE_DATA);

    while (!done)
    {
        if (!SetupDiEnumDeviceInterfaces(
            hardwareDeviceInfo,
            0,
            &GUID_DEVINTERFACE_KNSTBW,
            i,
            &deviceInfoData))
            break;

        ULONG predictedLength = 0;
        ULONG requiredLength = 0;
        // the first one is a probe for size.
        if (!SetupDiGetDeviceInterfaceDetail(
            hardwareDeviceInfo,
            &deviceInfoData,
            NULL,
            0,
            &requiredLength,
            NULL) 
            && GetLastError() != ERROR_INSUFFICIENT_BUFFER)
            break;

        predictedLength = requiredLength;

        std::vector<BYTE> buf(predictedLength);
        PSP_INTERFACE_DEVICE_DETAIL_DATA pDeviceData = 
            reinterpret_cast<PSP_INTERFACE_DEVICE_DETAIL_DATA>(&buf[0]);
        pDeviceData->cbSize = sizeof(SP_INTERFACE_DEVICE_DETAIL_DATA);

        if (!SetupDiGetDeviceInterfaceDetail(
            hardwareDeviceInfo,
            &deviceInfoData,
            pDeviceData,
            predictedLength,
            &requiredLength,
            NULL))
            break;

        handleDeviceArrival(pDeviceData->DevicePath);
        i++;
    }

    SetupDiDestroyDeviceInfoList(hardwareDeviceInfo);

	json jresp = getDevices();
	std::string jstr = jresp.dump();
	tbwMshm::updateMshmDevices(jstr);
}

void DeviceManager::handleDeviceArrival(const wchar_t* lpszDevicePath)
{
    AutoLock l(m_sync);

    BOOST_LOG_TRIVIAL(info) << L"Device arrival: " << lpszDevicePath;
    try
    {
        DEVICEPTR baseDevice(new Device(this, lpszDevicePath));
        Device* device = dynamic_cast<Device*>(baseDevice.get());
        device->init();
        _devices.push_back(baseDevice);

        App* pApp = App::getInstance();

        _settings->createDefaultDeviceConfigIfDoesntExist(device->id());

        // Getting a reference to a transient Setttings object in App might look
        // unsafe. But remember that our app runs on a single thread. So we don't
        // have to worry about the object being invalidated from another thread
        // owing to external factors such as changes to settings from the UI.
        //
        // When and if we make this a multithreaded app, then we we would have
        // wrap this method in a global critical section, which the App class
        // would also have to acquire to change its _settings member thereby
        // ensuring that App object & DeviceManager (and its Device children)
        // all refer to the same Settings object at all times.
        //
        // Also note that when _settings member is changed (or updated), App 
        // object would reinitialize the DeviceConfig member of all Device 
        // instances with the newly created object.
        device->setConfig(
            _settings->getDeviceConfig(
                getForegroundAppName().c_str(),
                device->id(),
                true
                ),
            true
        );
    }
    catch (const std::exception& e)
    {
        BOOST_LOG_TRIVIAL(error) << "Error initializing Device, reason: "
            << e.what();
    }
}

void DeviceManager::handleDeviceDeparture(const wchar_t* lpszDevicePath)
{
    AutoLock l(m_sync);
    BOOST_LOG_TRIVIAL(info) << L"Device departure: " << lpszDevicePath;

    // find the device
    for (auto it=_devices.begin(); it!=_devices.end(); it++)
    {
        DEVICEPTR& baseDevice = *it;
        Device* device = dynamic_cast<Device*>(baseDevice.get());
        if (::_wcsicmp(device->fileName().c_str(), lpszDevicePath) == 0) {
            _devices.remove(baseDevice);
            break;
        }
    }

	json jresp = getDevices();
	std::string jstr = jresp.dump();
	tbwMshm::updateMshmDevices(jstr);

}

LRESULT CALLBACK DeviceManager::_WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    return DeviceManager::getInstance()->WndProc(hWnd, uMsg, wParam, lParam);
}

LRESULT DeviceManager::WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch (uMsg)
    {
    case WM_DEVICECHANGE:
        handleDeviceChangeMessage(wParam, lParam);
        return 0;

    case WM_TIMER:
        if (wParam == TIMER_POLLFORCHANGES)
        {
            checkForForegroundAppChange();
            checkForModifierKeyStatusChange();
			//&*&*&*G1_ADD
			tbwMshm::wHeartbeat();
			//&*&*&*G2_ADD
        }
        break;

    case WM_EMULATE_BUTTON_ACTIVITY:
        if (_testMode)
            handleEmulationMessage(wParam, lParam); // ONLY IN TEST MODE!
        return 0;

    case WM_RELOAD_SETTINGS:
        handleSettingsFileChange();
        break;

    default:
        break;
    }
    return DefWindowProc(hWnd, uMsg, wParam, lParam);
}

void DeviceManager::handleDeviceChangeMessage(WPARAM wParam, LPARAM lParam)
{
    switch (wParam)
    {
    case DBT_DEVICEARRIVAL:
        {
            DEV_BROADCAST_DEVNODE *pDBD = 
                reinterpret_cast<DEV_BROADCAST_DEVNODE*>(lParam);
            if (pDBD->dbcd_devicetype == DBT_DEVTYP_DEVICEINTERFACE)
            {
                DEV_BROADCAST_DEVICEINTERFACE *pDBDI = (DEV_BROADCAST_DEVICEINTERFACE *)lParam;
                if (::memcmp(&pDBDI->dbcc_classguid, &GUID_DEVINTERFACE_KNSTBW, sizeof(GUID)) == 0)
                {
                    handleDeviceArrival(pDBDI->dbcc_name);

					json jresp = getDevices();
					std::string jstr = jresp.dump();
					tbwMshm::updateMshmDevices(jstr);
 					std::cout << jstr << std::endl;
					BOOST_LOG_TRIVIAL(info) << jstr;
               }
            }
        }
        break;

    case DBT_DEVICEREMOVECOMPLETE:
        {
            DEV_BROADCAST_DEVNODE *pDBD =
                reinterpret_cast<DEV_BROADCAST_DEVNODE*>(lParam);
            if (pDBD->dbcd_devicetype == DBT_DEVTYP_DEVICEINTERFACE)
            {
                DEV_BROADCAST_DEVICEINTERFACE *pDBDI = (DEV_BROADCAST_DEVICEINTERFACE *)lParam;
                if (::memcmp(&pDBDI->dbcc_classguid, &GUID_DEVINTERFACE_KNSTBW, sizeof(GUID)) == 0)
                {
                    handleDeviceDeparture(pDBDI->dbcc_name);
                }
            }
        }
        break;
    }
}

void DeviceManager::OnBeginIOLoop()
{
    init(_testMode);
}

void DeviceManager::OnEndIOLoop(bool fGracefulExit)
{
    destroy();
}

void DeviceManager::destroy()
{
    if (_changeMonitor)
        delete _changeMonitor;
    _changeMonitor = nullptr;

    if (_hPnPNotification)
        UnregisterDeviceNotification(_hPnPNotification);
    _hPnPNotification = NULL;

    if (_hWnd)
        DestroyWindow(_hWnd);
    _hWnd = NULL;
    UnregisterClass(DEVMGR_OBJECT_WINDOW_CLASS, NULL);

    _settings.reset();
}

void DeviceManager::handleEmulationMessage(WPARAM ulButtonDownMask, LPARAM ulButtonUpMask)
{
    for (auto device : _devices) {
        if (device->id() == TEST_INSTRUMENT_PRODUCT_ID) {
            device->getConfig().handleButtonActivity(
                device.get(),
                (ULONG)ulButtonDownMask,
                (ULONG)ulButtonUpMask);
        }
    }
}

void DeviceManager::objLock()
{
    m_sync.Lock();
}
void DeviceManager::objUnlock()
{
    m_sync.Unlock();
}

void DeviceManager::emulateTestDeviceButtonActivity(ULONG ulButtonDownMask, ULONG ulButtonUpMask)
{
    // This could be called from any thread. Therefore we queue up a thread
    // message and let the DeviceManager workerthread handle the emulation
    // as if the event came from the device
    if (!_testMode)
        return;

    ::PostMessage(_hWnd, WM_EMULATE_BUTTON_ACTIVITY, ulButtonDownMask, ulButtonUpMask);
}

Device::Device(DeviceManager * pMgr, const wchar_t * lpszDeviceFileName)
    : DeviceBase(0, 0, "", 0)
    , _pManager(pMgr)
    , _deviceFileName(lpszDeviceFileName)
    , _deviceFile()
    , _eventOv(FALSE, FALSE)
    , _type(TBW_TYPE_UNKNOWN)
    , _driverBuild(0)
    , _usVersion(0)
//    , _pConfig(0)
{
    ::ZeroMemory(&_ov, sizeof(OVERLAPPED));
    ::ZeroMemory(&_kernData, sizeof(TBWKERNDATA));
}

Device::~Device()
{
    BOOST_LOG_TRIVIAL(debug) <<
        L"Releasing device ID: " << _id;
    auto pMgr = DeviceManager::getInstance();
    pMgr->RemoveWaitHandle(_eventOv);
}

void Device::init()
{
    if (_deviceFile.Create(
        _deviceFileName.c_str(),
        GENERIC_WRITE | GENERIC_READ,
        FILE_SHARE_WRITE | FILE_SHARE_READ,
        OPEN_EXISTING,
        FILE_ATTRIBUTE_NORMAL | FILE_FLAG_OVERLAPPED) != S_OK)
        throw std::runtime_error("Error opening the device");

    TRACKBALL_DRIVER_INFO driverInfo = { 0 };
    driverInfo.cbSize = sizeof(TRACKBALL_DRIVER_INFO);
    if (!getProperty(
        IOCTL_GETDDRIVERINFO,
        &driverInfo,
        sizeof(driverInfo)))
    {
        throw std::runtime_error("Error retrieving driver information");
    }
    BOOST_LOG_TRIVIAL(debug) << "Driver Info - struct size: " << driverInfo.cbSize << ", version: 0x" << std::hex << driverInfo.uVersion;

    TRACKBALL_DEVICE_INFO tdi = { 0 };
    // get device info 
    if (!getProperty(
        IOCTL_GETDEVICEINFO,
        &tdi,
        sizeof(tdi)))
    {
        throw std::runtime_error("Error retrieving device information from the driver");
    }

    // register ourselves as the helper app
    _productName = tdi.szName;
    _usVersion = tdi.usVersion;

	DeviceBase::init((uint16_t)tdi.usProductId, (uint16_t)tdi.usVersion, toUTF8(std::wstring(tdi.szName)).c_str());

    if (!setProperty(
        IOCTL_REGISTERHELPER,
        NULL,
        0))
    {
        throw std::runtime_error("Error register helper app with driver");
    }

	BOOST_LOG_TRIVIAL(info)
		<< "device arrived: " << name()
		<< ", id: " << id()
		<< ", realId: " << realId()
		<< ", firmwareVersion: " << std::hex << _firmwareVersion;

    // queue an overlapped read
    enqueueAsyncRead();

    // bind the overlapped completion status event to 
    // Device::handleDriverData through the App class's event 
    // notification mechanism.
    App* pApp = App::getInstance();
    if (!_pManager->AddWaitHandle(
        _eventOv,
        std::bind(&Device::handleDriverData, this)))
        throw std::exception("Error binding overlapped io with completion handler");
}
/*
uint16_t Device::id() const
{
    return _deviceId;
}

std::string Device::name() const
{
    std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t> convert;
    std::string dest = convert.to_bytes(_productName);
    return dest;
}

// Returns firmware version
uint16_t Device::version() const
{
    return (unsigned)_usVersion;
}
*/

void to_json(nlohmann::json& j, const DEVICEPTR& device)
{
    j = nlohmann::json{
        { "id", device->id() },
        { "name", device->name()  },
        { "version", device->version() }
    };
}
/*
void Device::setConfig(DeviceConfig& config, bool force)
{
    if (_pConfig != &config || force)
    {
        if (_pConfig != &config)
        {
            if (_pConfig)
                _pConfig->deactivate();
            _pConfig = &config;
            _pConfig->activate();
        }

        PlatformServices::getInstance()->configurePointer(
            _pConfig->pointerSpeed(),
            _pConfig->pointerAccelerationRate(),
            _pConfig->scrollSpeed()
        );

        setVertScrollParams(
            _pConfig->inertialScroll(),
            _pConfig->invertScroll()
        );

        auto buttonsCfg = getButtonsConfig();
        setProperty(
            IOCTL_SETBUTTONSCONFIG,
            &buttonsCfg,
            (DWORD)sizeof(TBWBUTTONSCONFIG)
        );
    }
}

/*
DeviceConfig & Device::getConfig() const
{
    return *_pConfig;
}
*/
void Device::setSlowPointer(bool enable)
{
    // enable/disable slow pointer
    uint32_t param = enable ? 1 : 0;

    if (!setProperty(
        IOCTL_SETSLOWPOINTER,
        &param,
        sizeof(param)))
    {
        throw std::exception("Error enabling slow pointer");
    }
}

bool Device::getSlowPointer()
{
    UINT32 enabled = 0;
    // get device type
    if (!getProperty(
        IOCTL_GETSLOWPOINTER,
        &enabled,
        sizeof(enabled)))
    {
        throw std::exception("Error quering driver slow pointer");
    }
    return enabled ? true : false;
}

void Device::setLockPointerAxis(bool enable)
{
    // enable/disable single axis movement
    uint32_t param = enable ? 1 : 0;

    if (!setProperty(
        IOCTL_SETSINGLEAXISMOVT,
        &param,
        sizeof(param)))
    {
        throw std::exception("Error enabling single axis movement");
    }
}

bool Device::getLockPointerAxis()
{
    UINT32 enabled = 0;
    // get device type
    if (!getProperty(
        IOCTL_GETSINGLEAXISMOVT,
        &enabled,
        sizeof(enabled)))
    {
        throw std::exception("Error quering driver single axis movement status");
    }
    return enabled ? true : false;
}

void Device::setVertScrollParams(bool inertial, bool invert)
{
    setScrollParams(SCROLL_WHEEL_VERT, inertial, invert);
}

void Device::getVertScrollParams(bool & inertial, bool & invert)
{
    auto sp = getScrollParams(SCROLL_WHEEL_VERT);
    inertial = sp.bInertialScroll;
    invert = sp.bInvertWheel;
}

void Device::setHorizScrollParams(bool inertial, bool invert)
{
    setScrollParams(SCROLL_WHEEL_HORZ, inertial, invert);
}

void Device::getHorizScrollParams(bool & inertial, bool & invert)
{
    auto sp = getScrollParams(SCROLL_WHEEL_HORZ);
    inertial = sp.bInertialScroll;
    invert = sp.bInvertWheel;
}

TBWBUTTONSCONFIG Device::getButtonsConfig()
{
    auto& actions = _pConfig->getActions();
    TBWBUTTONSCONFIG buttonsCfg = { 0 };
    buttonsCfg.cbSize = sizeof(buttonsCfg);
    buttonsCfg.chordMask = _pConfig->getChordsMask();
	//&*&*&*G1_ADD
	_pConfig->getChordMasks(buttonsCfg.chordMsks);
	//&*&*&*G2_ADD

    UINT32 index = 0;
    for (auto& a : actions)
    {
        ACTIONPTR& action = a.second;
        if (action->name() == "singleClick")
        {
            ActionSingleClick* pAction = dynamic_cast<ActionSingleClick*>(action.get());
            if (pAction->_modifiers == 0)
            {
                TBWBUTTONACTION& btnAction = buttonsCfg.buttons[index];
                btnAction.action = BUTTON_ACTION_CLICK;
                btnAction.buttonMask = a.first;
                btnAction.u.ClickActionParams.buttonMask = 0x01 << (pAction->_button - 1);
                index++;
            }
        }
        else if (action->name() == "scroll")
        {
            ActionScroll* pAction = dynamic_cast<ActionScroll*>(action.get());
            TBWBUTTONACTION& btnAction = buttonsCfg.buttons[index];
            btnAction.action = BUTTON_ACTION_SCROLL;
            btnAction.buttonMask = a.first;
            btnAction.u.ScrollActionParams.direction = pAction->_direction;
            btnAction.u.ScrollActionParams.lines = pAction->_lines;
            index++;
        }
    }
    buttonsCfg.nButtons = index;

#ifdef _DEBUG
    BOOST_LOG_TRIVIAL(debug) << "Button config - chordMask: 0x" << std::hex 
        << buttonsCfg.chordMask << ", nButtons: " << buttonsCfg.nButtons;
    for (size_t i = 0; i < buttonsCfg.nButtons; i++)
    {
        TBWBUTTONACTION& btnAction = buttonsCfg.buttons[i];
        BOOST_LOG_TRIVIAL(debug) << "\t" << i+1 << " buttonIn: 0x" << std::hex << btnAction.buttonMask;
        if (btnAction.action == BUTTON_ACTION_CLICK)
        {
            BOOST_LOG_TRIVIAL(debug) << "\tCLICK action, buttonOut: 0x" << std::hex << btnAction.u.ClickActionParams.buttonMask;
        }
        else if (btnAction.action == BUTTON_ACTION_SCROLL)
        {
            BOOST_LOG_TRIVIAL(debug) << "\tSCROLL action, direction: " << (int)btnAction.u.ScrollActionParams.direction
                << ", lines: " << (int)btnAction.u.ScrollActionParams.lines;
        }
    }
#endif

    return buttonsCfg;
}

void Device::updatePointerParams()
{
    PlatformServices::getInstance()->configurePointer(
        _pConfig->pointerSpeed(),
        _pConfig->pointerAcceleration() ? _pConfig->pointerAccelerationRate() : 0,
        _pConfig->scrollSpeed()
    );
}

void Device::updateScrollWheelParams()
{
	PlatformServices::getInstance()->configurePointer(
		_pConfig->pointerSpeed(),
		_pConfig->pointerAcceleration() ? _pConfig->pointerAccelerationRate() : 0,
		_pConfig->scrollSpeed()
	);
	setVertScrollParams(
        _pConfig->inertialScroll(),
        _pConfig->invertScroll()
    );
}

void Device::updateButtonsConfig()
{
    auto buttonsCfg = getButtonsConfig();
    setProperty(
        IOCTL_SETBUTTONSCONFIG,
        &buttonsCfg,
        (DWORD)sizeof(buttonsCfg)
    );
}

#ifdef TAU
void Device::emulateButtonClick(uint32_t buttonMask)
{
    TBWEMULATEDEVICEACTION action = { 0 };
    action.cbSize = sizeof(action);
    action.uAction = DA_BUTTONCLICK;
    //action.aParam[0] = (BYTE)buttonMask;
    //action.aParam[1] = 0x03;   // DOWN & UP

    action.u.ButtonClick.mask = buttonMask;
    action.u.ButtonClick.dir = 0x03;
    setProperty(
        IOCTL_EMULATEDEVICEACTION,
        &action,
        (DWORD)sizeof(action)
    );
/*
    ::PostMessage(
        _pManager->_hWnd,
        WM_EMULATE_BUTTON_ACTIVITY,
        (WPARAM)buttonMask,
        (LPARAM)0
    );
    ::PostMessage(
        _pManager->_hWnd,
        WM_EMULATE_BUTTON_ACTIVITY,
        (WPARAM)0,
        (LPARAM)buttonMask
    );
*/
}

void Device::emulateButtonPress(unsigned button, bool fDown)
{
    ::PostMessage(
        _pManager->_hWnd, 
        WM_EMULATE_BUTTON_ACTIVITY,
        (WPARAM)fDown ? button : 0,
        (LPARAM)fDown ? 0 : button
    );
}
#endif

bool Device::getProperty(DWORD dwCode, LPVOID lpParam, DWORD nParamSize)
{
    OVERLAPPED overlapped = { 0 };
    CEvent event(FALSE, FALSE);

    ZeroMemory(&overlapped, sizeof(overlapped));
    overlapped.hEvent = event; //  CreateEvent(NULL, FALSE, FALSE, NULL);

    if (!DeviceIoControl(
        _deviceFile,
        dwCode,
        NULL,
        0,
        lpParam,
        nParamSize,
        NULL,
        &overlapped) && GetLastError() != ERROR_IO_PENDING)
        return false;

    // wait for the overlapped I/O to finish
    if (WaitForSingleObject(event, INFINITE) == WAIT_OBJECT_0)
    {
        DWORD nBytesRead = 0;
        if (GetOverlappedResult(_deviceFile, &overlapped, &nBytesRead, TRUE))
        {
            return nParamSize == nBytesRead;
        }
    }

    return false;
}

bool Device::setProperty(DWORD dwCode, LPVOID lpParam, DWORD nParamSize)
{
    OVERLAPPED overlapped = { 0 };
    CEvent event(FALSE, FALSE);

    ZeroMemory(&overlapped, sizeof(overlapped));
    overlapped.hEvent = event; //  CreateEvent(NULL, FALSE, FALSE, NULL);

    DWORD nBytesReturned = 0;
    if (!DeviceIoControl(
        _deviceFile,
        dwCode,
        lpParam,
        nParamSize,
        NULL,
        0,
        NULL,
        &overlapped) && GetLastError() != ERROR_IO_PENDING)
        return false;

    // wait for the overlapped I/O to finish
    if (WaitForSingleObject(event, INFINITE) == WAIT_OBJECT_0)
    {
        DWORD nBytesWritten = 0;
        if (GetOverlappedResult(_deviceFile, &overlapped, &nBytesWritten, TRUE))
        {
            return nParamSize == nBytesWritten;
        }
    }

    return false;
}

void Device::handleDriverData()
{
    // acquire lock
    AutoLock l(_pManager->m_sync);

    // if GetOverlappedResult fails, it most probably is due to device being 
    // disconnected, which results in the pending overlapped Read to return
    // with failure.
    DWORD cb = 0;
    if (::GetOverlappedResult(_deviceFile, &_ov, &cb, TRUE))
    {
        //BOOST_LOG_TRIVIAL(info)
        //    << L"Driver data - length: " << _kernData.report_length
        //    << L", buttons: 0x" << hex << _kernData.buttons
        //    << L", status: 0x" << hex << _kernData.status;
        try
        {
            if (_kernData.status & TBWKERNDATA_STATUS_BUTTONACTIVITY)
                handleButtonActivity();

            if (_kernData.status & TBWKERNDATA_STATUS_NEWACTIVITY)
                handleNullActivity();

            // enqueue another read
            enqueueAsyncRead();
        }
        catch (...)
        {
            // TODO: error handling
        }
    }
}

inline void Device::handleButtonActivity()
{
    if (_pConfig)
    {
        _pConfig->handleButtonActivity(this, _kernData.buttonDownMask, _kernData.buttonUpMask);
    }
    else
    {
        BOOST_LOG_TRIVIAL(debug)
            << L"Device " << _deviceFileName
            << L" no config defined!";
    }
}

void Device::handleNullActivity()
{
}

void Device::enqueueAsyncRead()
{
    ::ZeroMemory(&_kernData, sizeof(_kernData));
    ::ZeroMemory(&_ov, sizeof(OVERLAPPED));
    _ov.hEvent = _eventOv;
    HRESULT hr = _deviceFile.Read(
        &_kernData,
        sizeof(_kernData),
        &_ov);
    if (hr != HRESULT_FROM_WIN32(ERROR_IO_PENDING))
    {
        throw std::exception("Error queuing async read to the driver");
    }
}

void Device::setScrollParams(SCROLL_WHEEL_TYPE wheel, bool inertial, bool invert)
{
    SCROLL_PARAMS sp = { 0 };
    sp.ScrollWheelType = wheel;
    sp.bInertialScroll = inertial ? 1 : 0;
    sp.bInvertWheel = invert ? 1 : 0;

    if (!setProperty(
        IOCTL_SETSCROLLPARAMS,
        &sp,
        sizeof(SCROLL_PARAMS))) {
        throw std::exception("Error setting vert inertial scroll status");
    }
}

SCROLL_PARAMS Device::getScrollParams(SCROLL_WHEEL_TYPE wheel)
{
    SCROLL_PARAMS sp = { 0 };
    sp.ScrollWheelType = wheel;

    if (!getProperty(
        IOCTL_GETSCROLLPARAMS,
        &sp,
        sizeof(SCROLL_PARAMS))) {
        throw std::exception("Error getting inertial scroll status");
    }

    return sp;
}

// Handler for notification from SettingsFileMonitor that it has been updated.
//
// Logic goes like this:
//
//  Reload the settings
//  Get foreground process name
//  Get matching device configuration for the foreground process
//  If the configuration is different from the currently active configuration, 
//      notify driver
//  else
//      do nothing
//
void DeviceManager::handleSettingsFileChange()
{
    AutoLock l(m_sync);

    BOOST_LOG_TRIVIAL(debug) << "Reloading settings";

    try
    {
        _settings->load(Settings::getFileFullPath().c_str());

        std::wstring activeApp = getForegroundAppName();

        // Find setting matching the device ID and current process filename
        // and pass it to the device.
        for (auto& baseDevice: _devices)
        {
            Device* device = dynamic_cast<Device*>(baseDevice.get());
            device->setConfig(
                _settings->getDeviceConfig(
                    getForegroundAppName().c_str(),
                    device->id(),
                    true
                ),
                true
            );
        }
    }
    catch (BadSettingsFile bsf)
    {
        // Settings::reload() can throw! Catch it we must, but it's not 
        // necessary that we do any special handling. Perhaps log a message?
        BOOST_LOG_TRIVIAL(error)
            << L"Error reloading the settings file";
    }
    catch (...)
    {
        BOOST_LOG_TRIVIAL(error)
            << L"Unknown error while reloading the settings & initing device";
    }
}

/*
Logic here is to first check if the current foregroup app has changed.
If it has changed, for each connected device, retrieve the matching setting
profile name.
Then compare this profile name, with the current profile that's in effect and
if they are different apply the new setting.
*/
void DeviceManager::checkForForegroundAppChange()
{
    AutoLock l(m_sync);

    HWND hWndForeground = ::GetForegroundWindow();
    if (!hWndForeground
        || hWndForeground == _hWndLastForeground)
        return;

    // foreground app has changed
    std::wstring activeApp = getForegroundAppName();
    if (activeApp.length() == 0)
        return;

    // Windows filenames are case sensitive -- so we normalize them to
    // lower case. Note that GUI would also have to do similar normalization
    // when creating application profiles.
    boost::to_lower(activeApp);

    BOOST_LOG_TRIVIAL(debug)
        << L"Foreground window has changed: "
        << activeApp;

    // Find setting matching the device ID and current process filename
    // and pass it to the device.
    for (auto& baseDevice: _devices)
    {
		Device* device = dynamic_cast<Device*>(baseDevice.get());
		tbstring appConfigName = _settings->getMatchingProfileName(
			device->id(),
			activeApp.c_str());

		if (appConfigName.length() > 0) {
			BOOST_LOG_TRIVIAL(debug)
				<< L"App config changed to: "
				<< appConfigName;
			device->setConfig(
				_settings->getDeviceConfig(
					appConfigName.c_str(),
					device->id(),
					true
				),
				false
			);
		}
    }

    _hWndLastForeground = hWndForeground;
}

std::wstring DeviceManager::getForegroundAppName()
{
    HWND hWndForeground = ::GetForegroundWindow();
    if (hWndForeground)
    {
        DWORD dwProcessId = 0;
        ::GetWindowThreadProcessId(hWndForeground, &dwProcessId);
        return GetProcessNameFromId(dwProcessId, false);
    }
    return std::wstring();
}

void DeviceManager::checkForModifierKeyStatusChange()
{
    AutoLock l(m_sync);

    USHORT status = 0;
    status |= (GetAsyncKeyState(VK_SHIFT) < 0) ? 0x01 : 0;
    status |= (GetAsyncKeyState(VK_CONTROL) < 0) ? 0x04 : 0;
    status |= (GetAsyncKeyState(VK_MENU) & 0x8000) ? 0x10 : 0;

    if (status != _modifierStatus)
    {
        /*
        BOOST_LOG_TRIVIAL(debug)
            << L"Modifier key status changed: 0x"
            << std::hex << std::setw(4) << std::setfill(L'0') << status;
        */
		BOOST_LOG_TRIVIAL(debug)
			<< L"Modifier key status changed: 0x"
			<< status;
        for (auto& baseDevice : _devices)
        {
            Device* device = dynamic_cast<Device*>(baseDevice.get());
            try
            {
				//&*&*&*G1_MOD, perform lock pinter axis before slowing pointer.
				auto lockPointerAxisEnabled = device->getLockPointerAxis();
				auto lockPointerAxisModifers = device->getConfig().getLockPointerAxisModifiersMask();
				if (lockPointerAxisModifers)
				{

					if (lockPointerAxisEnabled && !(status & lockPointerAxisModifers))
					{
						BOOST_LOG_TRIVIAL(debug) << "Disable lock pointer axis";
						device->setLockPointerAxis(false);
					}
					else if (!lockPointerAxisEnabled && (status ^ lockPointerAxisModifers) == 0)
					{
						BOOST_LOG_TRIVIAL(debug) << "Enable lock pointer axis";
						device->setLockPointerAxis(true);
					}
				}
				
                auto slowPointerEnabled = device->getSlowPointer();
				auto slowModifiers = device->getConfig().getSlowModifiersMask();
                if (slowModifiers)
                {
                    if (slowPointerEnabled && !(status & slowModifiers))
                    {
                        // enable slow pointer
                        BOOST_LOG_TRIVIAL(debug) << "Disable slow pointer";
                        device->setSlowPointer(false);
                    }
                    else if (!slowPointerEnabled && (status ^ slowModifiers) == 0)
                    {
                        // enable slow pointer
                        BOOST_LOG_TRIVIAL(debug) << "Enable slow pointer";
                        device->setSlowPointer(true);
                    }
                }

				//&*&*&*G1_ADD
				if (lockPointerAxisModifers && slowModifiers)
				{
					auto dualModifiers = slowModifiers | lockPointerAxisModifers;
					if ((status == dualModifiers) && (dualModifiers != slowModifiers) && (dualModifiers != lockPointerAxisModifers))
					{
						if (!lockPointerAxisEnabled)
						{
							BOOST_LOG_TRIVIAL(info) << "Enabling locked axis pointer movement";
							device->setLockPointerAxis(true);
						}
						if (!slowPointerEnabled)
						{
							BOOST_LOG_TRIVIAL(info) << "Enabling slow pointer";
							device->setSlowPointer(true);
						}
					}

				}

				//&*&*&*G2_ADD


            }
            catch (const std::exception& e)
            {
                e;
            }
        }

        _modifierStatus = status;
    }
}

TestDevice::TestDevice(DeviceManager* pManager)
    : Device(pManager, L"")
{
}

TestDevice::~TestDevice()
{
}

void TestDevice::init()
{
    _driverBuild = 2;
    _id = TEST_INSTRUMENT_PRODUCT_ID;
	_realId = TEST_INSTRUMENT_PRODUCT_ID;
    _usVersion = 0x0100;
    _productName = TEST_INSTRUMENT_PRODUCT_NAME;
}

bool TestDevice::getProperty(DWORD dwCode, LPVOID, DWORD)
{
    return true;
}

bool TestDevice::setProperty(DWORD dwCode, LPVOID, DWORD)
{
    return true;
}
