#pragma once

#include "wfmohandler.h"
#include "TrayIcon.h"
#include "Settings.h"

//class CommandServer;
class TbwHelper;

class App : public ITrayIconListener {

    static App* _instance;

protected:
    HINSTANCE   _hInstance;
    HWND        _hWnd;
    CTrayIcon*  _trayIcon;
    //CommandServer* _server;
    BOOL        _wtsSessionChangeRegd;
    TbwHelper*  _helper;

protected:
    App(); // throw(std::exception)
    virtual ~App();

    bool createUI();

    // window proc
    static LRESULT CALLBACK _WndProc(HWND, UINT, WPARAM, LPARAM);
    LRESULT WndProc(HWND, UINT, WPARAM, LPARAM);

    LRESULT OnCreate(HWND, WPARAM, LPARAM);
    LRESULT OnDestroy(HWND, WPARAM, LPARAM);
    LRESULT OnSessionChange(HWND, WPARAM, LPARAM);
    LRESULT OnPowerBroadcast(HWND, WPARAM, LPARAM);
    LRESULT OnOpenKensingtonWorks(HWND, WPARAM, LPARAM);

    // CTrayIcon event handlers. Not relevant when tbwhelper is running
    // as a background process.
    // virtual void OnTrayIcon
    virtual void OnTrayIconLButtonDblClk(CTrayIcon* pTrayIcon);
    virtual void OnTrayIconRButtonUp(CTrayIcon* pTrayIcon);

	std::wstring GetTrayIconLabel();
	void ShowTrayMenu(CTrayIcon* pTrayIcon);

    // Methods for testing the instrumentation device
    void testKeyStroke();
    void testSnippet();
    void testLaunch();
    void testOpenFolder();
    void testSequence();
    void testDoubleClick();
    void testTripleClick();
    void testChord1();
    void testChord2();
    void slowPointer();
    void singleAxisMovement();
    void inertialScroll();

public:
    // Returns the one and only instance of this class.
    static App* getInstance();

    // Returns a boolean indicating if an instance of tbwhelper is already 
    // running in this session.
    static bool instanceExists();

    // init the app, pass fTestMode = true to 
    // enable test instrumentation mode
    void init(bool fTestMode=false);

    // Destroy the app, to be called when App::run() exits, which happens
    // when the process terminates gracefully.
    void destroy();

    // The main IO loop which incorporates a message loop.
    // Essentially the core of the program.
    void run();

    HWND getWindow()
    {
        return _hWnd; 
    }

    void showWindow()
    {
        ::ShowWindow(getWindow(), SW_SHOW);
    }

    void hideWindow()
    {
        ::ShowWindow(getWindow(), SW_HIDE);
    }
};
