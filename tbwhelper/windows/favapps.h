#pragma once

#include "json.hpp"

/*
 * Build program binary path -> name cache
 */
void rebuildProgramNamesCache();

/*
 * Returns a JSON array of favorite apps. Each element of the array is of the 
   form:
   {
	"program": "<program-binary-path>",
	"name": "<program-name>",
	"icon": "<program-icon-as-based64-encoded-str>"
   }
 */
nlohmann::json getFavApps(int count, const std::set<std::string>&);
