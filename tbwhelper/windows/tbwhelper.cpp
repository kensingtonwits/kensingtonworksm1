// tbwhelper.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include <wtsapi32.h>
#include <set>

#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/sinks/text_file_backend.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/sinks/debug_output_backend.hpp>
#include <boost/log/sources/severity_logger.hpp>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>

#include <boost/locale.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/filesystem/fstream.hpp>

namespace logging = boost::log;
namespace src = boost::log::sources;
namespace sinks = boost::log::sinks;
namespace keywords = boost::log::keywords;

//#define _LEGACY_CODE    1

#include "tbwhelper.h"

#include "TrayIcon.h"
#include "globals.h"

#include "App.h"

#include "json.hpp"
#include "PlatformServices.h"

#include "Settings.h"
#include "Migrate.h"
#include "launchgui.h"
#include "favapps.h"

// Global Variables:
HINSTANCE g_hInstance = NULL;                   // current instance
HWND g_hMainWnd = NULL;                         // main window
wchar_t TBHELPER_WND_CLASS_NAME[] = L"TbwHelperWndClass";

// program options
struct ProgramOptions {
    bool _terminateRunning;
    logging::trivial::severity_level _logLevel;
    bool _logConsoleOutput;
	bool _launchGui;
	bool _parseFavApps;
    ProgramOptions()
        : _terminateRunning(false)
        , _logLevel(logging::trivial::info)
        , _logConsoleOutput(false)
		, _launchGui(false)
		, _parseFavApps(false)
    {}
};

// Forward declarations of functions included in this code module:
static ProgramOptions processCommandLine(const wchar_t** argv, int argc);
static void quitExistingApp();
static void initLog(logging::trivial::severity_level lvl, bool consoleOutput);
static void deinitLog();
static void migrateLegacySettings();
static void testSettings();

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

	// Create and install global locale
	std::locale::global(boost::locale::generator().generate(""));
	// Make boost.filesystem use it
	boost::filesystem::path::imbue(std::locale());

#ifdef _DEBUG
    // Get current flag
    int tmpFlag = _CrtSetDbgFlag(_CRTDBG_REPORT_FLAG);

    // Turn on leak-checking bit.
    tmpFlag |= _CRTDBG_LEAK_CHECK_DF;

    // Set flag to the new value.
    _CrtSetDbgFlag(tmpFlag);
#endif

    // Set current directory to executable's location.
    TCHAR szThisFile[1024] = { 0 };
    ::GetModuleFileName(NULL, szThisFile, sizeof(szThisFile) / sizeof(szThisFile[0]));
    boost::filesystem::path myfullname(szThisFile);
    boost::filesystem::path filepath = myfullname.remove_filename();
    ::SetCurrentDirectory(filepath.native().c_str());


    DWORD dwError, dwPriClass;

#if true // *&*&*&V1_20201127
    if (!SetPriorityClass(GetCurrentProcess(), PROCESS_MODE_BACKGROUND_BEGIN))
    {
        dwError = GetLastError();
        if (ERROR_PROCESS_MODE_ALREADY_BACKGROUND == dwError)
            OutputDebugStringW(TEXT("Already in background mode\n"));
        else 
            OutputDebugStringW(TEXT("Failed to enter background mode \n"));
            //_tprintf(TEXT("Failed to enter background mode (%d)\n"), dwError);
        goto Cleanup;
    }
    OutputDebugStringW(TEXT("--------------tbwHelper------------\n"));
    // Display priority class

    dwPriClass = GetPriorityClass(GetCurrentProcess());

    _tprintf(TEXT("Current priority class is 0x%x\n"), dwPriClass);

    //
    // Perform background work
    //
    ;

    if (!SetPriorityClass(GetCurrentProcess(), HIGH_PRIORITY_CLASS))
    {
        _tprintf(TEXT("Failed to end background mode (%d)\n"), GetLastError());
    }

Cleanup:
    // Clean up
#endif




    int retval = 0;
    try
    {
        int argc = 0;
        const wchar_t** argv = (const wchar_t**)::CommandLineToArgvW(
            ::GetCommandLineW(),
            &argc);

        auto po = processCommandLine(argv, argc);
        if (po._terminateRunning)
        {
            quitExistingApp();
            return 0;
        }

		if (po._launchGui)
		{
			launchGui();
			return 0;
		}

#if 0 //def _DEBUG
		if (po._parseFavApps)
		{
			initLog(po._logLevel, po._logConsoleOutput);
			std::set<std::string> exclude;
			auto apps = getFavApps(10, exclude);
			BOOST_LOG_TRIVIAL(debug) << "Fav apps: " << apps;
			deinitLog();
			return 0;
		}
#endif

		// Check for another instance of the app in this session
        if (App::instanceExists())
            return 2;

        initLog(po._logLevel, po._logConsoleOutput);

		/*
		{
			testSettings();
			deinitLog();
			return 0;
		}
		*/

        migrateLegacySettings();

		// build program names cache, which requires COM. So initialize it first.
		::CoInitialize(NULL);
		rebuildProgramNamesCache();

        App* pApp = App::getInstance();
        if (nullptr == pApp)
            return 3;

        pApp->init(false);
        pApp->run();
        pApp->destroy();

		::CoUninitialize();

        deinitLog();

        retval = 0;
    }
    catch (std::exception& e)
    {
        BOOST_LOG_TRIVIAL(error) << "Error starting the app, reason: " << e.what();
        retval = 1;
    }
    catch(...)
    {
        BOOST_LOG_TRIVIAL(error) << "Unknown error while starting the app";
        retval = 1;
    }

    if (retval != 0) {
        App* pApp = App::getInstance();
        pApp->destroy();
    }
    auto pServices = PlatformServices::getInstance();
    pServices->destroy();

    return retval;
}

// Throws exception if invalid arguments are passed!
ProgramOptions processCommandLine(const wchar_t** argv, int argc)
{
    namespace po = boost::program_options;
    try
    {
        ProgramOptions options;

        po::options_description desc("Options");
        desc.add_options()
            ("logLevel", po::value<int>(), "Log level (0-5, 0: maximum logging, 5: minimum logging)")
            ("quit", "Terminate running helper")
            ("dbgOutput", "Output log messages to OS console")
            ("verbose", "Enable maximum logging")
			("launchGui", "Launch GUI")
			("parseFavApps", "Parse Favorite Apps")
			;

        po::variables_map vm;
        po::store(po::parse_command_line<wchar_t>(argc, argv, desc), vm);
        po::notify(vm);

        if (vm.count("logLevel")) {
            auto logLevel = vm["logLevel"].as<int>();
            // sanitize int to logging::trivial::severity_level enum range
            logLevel = std::min(std::max(logLevel, (int)logging::trivial::trace), (int)logging::trivial::fatal);
            // convert sanitized value back to severity_level
            options._logLevel = (logging::trivial::severity_level)logLevel;
        }

        if (vm.count("dbgOutput")) {
            options._logConsoleOutput = true;
        }

        if (vm.count("verbose")) {
            options._logLevel = logging::trivial::trace;
        }

        if (vm.count("quit")) {
            options._terminateRunning = true;
        }

		if (vm.count("launchGui")) {
			options._launchGui = true;
		}

		if (vm.count("parseFavApps")) {
			options._parseFavApps = true;
		}

        return options;
    }
    catch (po::error& e)
    {
        std::cerr << e.what();
        throw;
    }
}

/*
 * Find the running instance of this app and quit it.
 */
void quitExistingApp()
{
#if _WINDOWS
    HWND hWnd = ::FindWindowW(TBHELPER_WND_CLASS_NAME, NULL);
    if (hWnd)
    {
        DWORD dwProcessId = 0;
        ::GetWindowThreadProcessId(hWnd, &dwProcessId);

        // Attach our input queue to the target window's thread before
        // posting WM_CLOSE message.
        ::AttachThreadInput(::GetCurrentThreadId(),
            ::GetWindowThreadProcessId(hWnd, NULL),
            TRUE);

        ::PostMessage(hWnd, WM_CLOSE, 0, 0);

        // Detach from the target window thread input queue.
        ::AttachThreadInput(::GetCurrentThreadId(),
            ::GetWindowThreadProcessId(hWnd, NULL),
            FALSE);

        // Now wait for the main tbwhelper process to terminate
        HANDLE hProcess = ::OpenProcess(
            SYNCHRONIZE,
            FALSE,
            dwProcessId);
        if (hProcess)
        {
            // We wait for 5 seconds for the process to terminate, which ought
            // to be more than sufficient. Also, we don't care about the return
            // value of this function as there isn't much we can do upon 
            // failure.
            ::WaitForSingleObject(hProcess, 5 * 1000);
            ::CloseHandle(hProcess);
        }
    }
#elif _OSX
#endif
}

// Initialize the log system (uses Boost::log)
void initLog(logging::trivial::severity_level lvl, bool consoleOutput)
{
    boost::filesystem::path logFilepath(PlatformServices::getInstance()->getLogFileFolder());
    logFilepath.append("tbwhelper_%2N.log");
    logging::add_file_log
    (
        keywords::file_name = logFilepath,
        keywords::rotation_size = 5 * 1024 * 1024,
        keywords::format = "%TimeStamp%: %ThreadID% %Message%",
        keywords::open_mode = std::ios::app,
        keywords::auto_flush = true
    );

    if (consoleOutput)
    {
#ifdef _WINDOWS
        typedef sinks::synchronous_sink< sinks::debug_output_backend > sink_t;
        boost::shared_ptr< logging::core > core = logging::core::get();
        // Create the sink. The backend requires synchronization in the frontend.
        boost::shared_ptr< sink_t > sink(new sink_t());
        namespace expr = boost::log::expressions;
        sink->set_formatter
        (
            expr::format("%1%\r\n")
//            % expr::format_date_time<boost::posix_time::ptime>("TimeStamp", "%Y-%m-%d %H:%M:%S")
//            % logging::trivial::severity
            % expr::smessage
        );
        core->add_sink(sink);
#else
#endif
    }

    logging::core::get()->set_filter
    (
        logging::trivial::severity >= lvl
    );

    logging::add_common_attributes();

    BOOST_LOG_TRIVIAL(error) << "Log initialized, logLevel: " << lvl;
}

void deinitLog()
{
    BOOST_LOG_TRIVIAL(error) << "Log deinitialized";
}

/*
 * If TbwSettings.json does not exist and TbwSettings.xml is found, migrate
 * TbwSettings.xml to TbwSettings.json.
 */
void migrateLegacySettings()
{
	try
	{
		if (!boost::filesystem::exists(Settings::getFileFullPath().c_str())
			&& boost::filesystem::exists(Settings::getLegacyFileFullPath().c_str()))
		{
			// TbwSettings.json does not exist and TbwSettings.xml does. Or user
			// has explicitly requested forced migration.
			SettingsMigrator sm;
			sm.migrate();
		}
	}
	catch (const std::exception&)
	{
		BOOST_LOG_TRIVIAL(warning) << "Error migrating old settings, using default settings..";
	}
}

// created to test Settings class object lifetime management
void testSettings()
{
	try
	{
		typedef std::unique_ptr<Settings> SETTINGSPTR;

		{
			SETTINGSPTR s3(new Settings(false));
			s3->createDefaultDeviceConfigIfDoesntExist(32792);
			auto& devConfig2 = s3->getDeviceConfig("*", 32792, true);
			BOOST_LOG_TRIVIAL(debug) << "Settings 3 devConfig.appname: " << devConfig2.settings()->getAppName(32792, "*");
		}
		/*
		SETTINGSPTR s1(Settings::createLoadingFile("c:\\tmp\\TbwSettings.json"));
		// check s1 contents
		auto devices = s1->getDevices("*");
		for (auto device : devices) {
			BOOST_LOG_TRIVIAL(debug) << "Device: " << device;
		}

		auto& devConfig1 = s1->getDeviceConfig("*", 32792, true);
		BOOST_LOG_TRIVIAL(debug) << "Settings 1 devConfig.appname: " << devConfig1.settings()->getAppName(32792, "*");

		SETTINGSPTR s2(Settings::createLoadingFile("c:\\tmp\\TbwSettings_empty.json"));
		s2->createDefaultDeviceConfigIfDoesntExist(32792);
		auto devices2 = s2->getDevices("*");
		for (auto device : devices2) {
			BOOST_LOG_TRIVIAL(debug) << "Device: " << device;
		}
		auto& devConfig2 = s2->getDeviceConfig("*", 32792, true);
		BOOST_LOG_TRIVIAL(debug) << "Settings 2 devConfig.appname: " << devConfig2.settings()->getAppName(32792, "*");

		// assign s2 to s1
		s1.reset(s2.release());
		auto& devConfig3 = s1->getDeviceConfig("*", 32792, true);
		BOOST_LOG_TRIVIAL(debug) << "Settings 1 devConfig.appname: " << devConfig3.settings()->getAppName(32792, "*");
		*/
	}
	catch (std::exception& e)
	{
		BOOST_LOG_TRIVIAL(debug) << "Exception: " << e.what();
	}
}
