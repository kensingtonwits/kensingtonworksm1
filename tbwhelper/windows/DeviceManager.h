/*
 * This is the primary abstraction for detecting and managing TrackballWorks
 * devices in the system.
 *
 * The model consists of two classes -- DeviceManager, which monitors the system
 * for connection/disconnection of Trackball devices and Device class, which is
 * instantiated/destroyed when a device is connected/disconnected.
 *
 * Device class provides thread-safe methods that allows communication with the
 * actual TrackballWorks device driver.
 *
 * Needless to say DeviceManager is envisaged as a singleton though this design
 * requirement is not enforced by implementation.
 */
#pragma once

#include <dbt.h>
#include <setupapi.h>
#include <memory>
#include <list>
#include <string>
#include <locale>
#include <codecvt>

#include "WFMOHandler.h"

#include "TbwKernelAPI.h"
#include "utils.h"
#include "json.hpp"

#include "IDeviceManager.h"
#include "DeviceManagerBase.h"

class Device;
class DeviceManager;

class Device : public DeviceBase {

protected:
    DeviceManager* _pManager;
    std::wstring _deviceFileName;
    CAtlFile _deviceFile;
    CEvent _eventOv; // for overlapped IO
    OVERLAPPED _ov;

    // device property
    INT32 _type;        // device type - legacy stuff, not relevant
    INT32 _driverBuild; // driver build - legacy stuff, not relevant
    USHORT _usVersion;  // firmware version

    // Kernel data returned by the driver. We queue a overlapped read on
    // this structure.
    TBWKERNDATA _kernData;

    //DeviceConfig* _pConfig;

    std::wstring _productName;

    // Hide these so that we can prevent copying by clients
    Device(Device const&) = delete;
    Device& operator=(Device const&) = delete;

public:
    Device(DeviceManager* pMgr, const wchar_t* lpszDeviceFileName);
    virtual ~Device();

    virtual void init();

    std::wstring const& fileName() const
    {
        return _deviceFileName;
    }

    // ITbwDevice virtual methods
    /*
    virtual uint16_t id() const;
    virtual std::string name() const;
    virtual uint16_t version() const;
    */
    // Returns legacy ID which is an enum defined in TBW 1.x
    unsigned legacyId() const
    {
        return _type;
    }

    //void setConfig(DeviceConfig& config, bool force=false);
    //DeviceConfig& getConfig() const;

    // enable/disable slow pointer movement
    void setSlowPointer(bool enable);
    // get current slow pointer enabled status
    bool getSlowPointer();

    // enable/disable locked axis movement
    void setLockPointerAxis(bool enable);
    // get current locked axis movement enabled status
    bool getLockPointerAxis();

    void setVertScrollParams(bool inertial, bool invert);
    void getVertScrollParams(bool& inertial, bool& invert);

    void setHorizScrollParams(bool inertial, bool invert);
    void getHorizScrollParams(bool& inertial, bool& invert);

    TBWBUTTONSCONFIG getButtonsConfig();

    // DeviceBase pure virtual methods
    virtual void updatePointerParams();
    virtual void updateScrollWheelParams();
    virtual void updateButtonsConfig();
#ifdef TAU
    virtual void emulateButtonClick(uint32_t buttonMask);
    // end DeviceBase pure virtual methods

    // for automated testing -- not used in actual production
    void emulateButtonPress(unsigned button, bool fDown);
#endif

protected:
    // get/set driver property (PDO ioctl wrappers)
    virtual bool getProperty(DWORD dwCode, LPVOID, DWORD);
    virtual bool setProperty(DWORD dwCode, LPVOID, DWORD);

    // handler for TBWKERNDATA sent by the driver. This data is sent through 
    // the _kernData member which we pass as the buffer to an overlapped
    // ReadFile.
    void handleDriverData();

    // handle device button activity
    void handleButtonActivity();

    // handle NULL activity which is a periodic report sent by the driver
    // after an interval following any kind of device interrupts resulting in
    // an HID report being generated.
    void handleNullActivity();

    // Enqueue's an async read on the device to receive TBWKERNDATA sent
    // by the driver.
    void enqueueAsyncRead();

    void setScrollParams(SCROLL_WHEEL_TYPE wheel, bool inertial, bool invert);
    SCROLL_PARAMS getScrollParams(SCROLL_WHEEL_TYPE wheel);
};

//typedef std::shared_ptr<Device> DEVICEPTR;

class TestDevice : public Device {

public:
    TestDevice(DeviceManager* pMgr);
    virtual ~TestDevice();

    virtual void init();
    virtual bool getProperty(DWORD dwCode, LPVOID, DWORD);
    virtual bool setProperty(DWORD dwCode, LPVOID, DWORD);
};

// serialize Device to json
void to_json(nlohmann::json& j, const DEVICEPTR& device);

class SettingsFileChangeMonitor;

class DeviceManager : public DeviceManagerBase, protected MsgWFMOHandler
{
    friend class Device;
    static DeviceManager* _instance;

    friend class SettingsFileChangeMonitor;

    // Message only window for getting device arrival notification from
    // PnP device manager. DeviceManager creates its own window for this,
    // thereby making this class an independent component within the app.
    HWND _hWnd;
    // PnP notification handle
    HDEVNOTIFY _hPnPNotification;

    // collection of all attached devices
    //typedef std::list<DEVICEPTR> DEVICEPTRLIST;
    //DEVICEPTRLIST _devices;

    //std::unique_ptr<Settings> _settings;
    SettingsFileChangeMonitor* _changeMonitor;
    HWND        _hWndLastForeground; // tracks last detected foreground window
    USHORT      _modifierStatus;
    tbstring     _curProfileName;

    bool        _testMode;

    bool        _initSuccess;
    CEvent      _initDone;

protected:
    DeviceManager();
    virtual ~DeviceManager();

    // enumerate any attached devices
    void enumerateAttachedDevices();
    
    // handle device arrival
    void handleDeviceArrival(const wchar_t* lpszDevicePath);

    // handle device removal
    void handleDeviceDeparture(const wchar_t* lpszDevicePath);

    // message window proc
    static LRESULT CALLBACK _WndProc(HWND, UINT, WPARAM, LPARAM);
    LRESULT WndProc(HWND, UINT, WPARAM, LPARAM);

    // Check if foreground process has changed. It does this by comparing the 
    // current foreground window against what we cached during the previous
    // call to this method.
    void checkForForegroundAppChange();

    // Returns the name of the executable file of the process whose window
    // is in the foreground receiving user input. If teh foreground window 
    // is non-deterministic, it returns an empty string.
    std::wstring getForegroundAppName();

    // Check if there's any change in modifier key status. As in the previous
    // method, this is done by checking the current modifier key status against
    // what we cached during the previous call to this method.
    void checkForModifierKeyStatusChange();

    // Settings file change handler
    void handleSettingsFileChange();

    // WM_DEVICECHANGE message handler
    void handleDeviceChangeMessage(WPARAM, LPARAM);

    virtual void OnBeginIOLoop();
    virtual void OnEndIOLoop(bool fGracefulExit);

    // initialize the device manager.
    void init(bool fTestMode = false);
    void destroy();

    void handleEmulationMessage(WPARAM, LPARAM);

public:
    static DeviceManager* getInstance();

    void start();
    void stop();

    // Override to use MsgWFMOHandler synchronization primitive
    virtual void objLock();
    virtual void objUnlock();

    // Test instrumentation support
    void emulateTestDeviceButtonActivity(
        ULONG ulButtonDownMask,
        ULONG ulButtonUpMask);
};
