#include "singleinstance.h"

#if _WINDOWS
#include <windows.h>
#include "globals.h"
#elif _OSX
#pragma error("TODO")
#endif
/*
* Returns a boolean indicating if another instance of this app is
* active.
*/
bool anotherInstanceExists()
{
#if _WINDOWS
    const wchar_t* INSTANCE_MUTEX_NAME =
        L"Global\\com.kensington.TrackballWorks.TbwHelper";

    g_hInstanceMutex = ::CreateMutex(
        NULL,
        TRUE,
        INSTANCE_MUTEX_NAME);

    if (g_hInstanceMutex != NULL && ::GetLastError() == ERROR_ALREADY_EXISTS) {
        ::CloseHandle(g_hInstanceMutex);
        g_hInstanceMutex = NULL;
        return true;
    }
#elif _OSX
#pragma error("TODO")
#endif

    return false;
}
