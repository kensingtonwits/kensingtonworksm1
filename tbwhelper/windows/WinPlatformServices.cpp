#include "stdafx.h"
#include <Shlobj.h>
#include <shellapi.h>
#include <vector>
#include <algorithm>

#include <boost/log/trivial.hpp>

#include "WinPlatformServices.h"

//#include "logfmwk.h"
//#include "App.h"
#include "Modifiers.h"
#include "utils.h"
#include "favapps.h"
#include "globals.h"
#include "DeviceManager.h"


#define VKKEYSCAN_SHIFT		0x0100U
#define VKKEYSCAN_CTRL		0x0200U
#define VKKEYSCAN_ALT		0x0400U
#define VKKEYSCAN_HANKAKU	0x0800U

// SPI_SETMOUSE expects an array of three elements as its pvParam argument.
// This array contains 4 of these sets of 3 values totalling 12 elements.
const int ACCELERATION_PROFILES[] = { 0, 0, 0, 10, 128, 3, 10, 64, 3, 4, 64, 4 };

static const wchar_t* LOG_TAG = L"WinPSvcs";

WinPlatformServices::WinPlatformServices()
{
}

WinPlatformServices::~WinPlatformServices()
{
}

void WinPlatformServices::postKeystroke(uint16_t usModifiers, uint16_t usKeyCode, bool fDown, bool fModifiersOnly)
{
    std::vector<INPUT> inputs;
    inputs.reserve(16);

    if (!usKeyCode && !usModifiers)
        return;


#ifdef _DEBUG
    BOOST_LOG_TRIVIAL(info)
        << "postKeystroke usModifiers: 0x" << std::hex << usModifiers
        << ", usKeyCode: " << usKeyCode 
        << ", dir: " << (fDown ? "DOWN" : "UP");
#endif


    // For release action, poke the actual key release event first
    // before poking input events for releasing the modifier keys.
    if (!fModifiersOnly && !fDown && usKeyCode)
    {
        INPUT in = { 0 };
        in.ki.wVk = usKeyCode;
        inputs.push_back(in);
	}

    if (usModifiers & WIN_K_SHIFT)
    {
        INPUT in = { 0 };
        in.ki.wVk = VK_SHIFT;
        inputs.push_back(in);
	}

    if (usModifiers & WIN_K_CTRL)
    {
        INPUT in = { 0 };
        in.ki.wVk = VK_CONTROL;
        inputs.push_back(in);
	}

    if (usModifiers & WIN_K_ALT)
    {
        INPUT in = { 0 };
        in.ki.wVk = VK_MENU;
        inputs.push_back(in);
	}

    if (usModifiers & WIN_K_WIN)
    {
        INPUT in = { 0 };
        in.ki.wVk = VK_LWIN;
        inputs.push_back(in);
	}

    if (usModifiers & WIN_K_APPS)
    {
        INPUT in = { 0 };
        in.ki.wVk = VK_APPS;
        inputs.push_back(in);
	}

    // For keydown events, the non-modifier key should be the
    // last input event. That is after all the modifier keys are
    // pressed down, the non-modifier key depress is emulated.
    if (!fModifiersOnly && fDown && usKeyCode)
    {
        INPUT in = { 0 };
        in.ki.wVk = usKeyCode;
        inputs.push_back(in);
	}

    for (auto& in : inputs)
    {
        in.ki.dwFlags = in.ki.dwFlags | (fDown ? 0 : KEYEVENTF_KEYUP);
        switch (in.ki.wVk)
        {
        case VK_END:
        case VK_HOME:
        case VK_PRIOR:
        case VK_NEXT:
       // case VK_SNAPSHOT:
        case VK_RIGHT:
        case VK_LEFT:
        case VK_UP:
        case VK_DOWN:
        case VK_APPS:
		case VK_DELETE:
//		case VK_OEM_MINUS:
//		case VK_OEM_PLUS:
		case VK_ADD:
		case VK_SUBTRACT:
		case VK_MULTIPLY:
		case VK_DIVIDE:
		case VK_DECIMAL:
		case VK_LWIN:
		case VK_RWIN:
		case VK_MENU:
		//case VK_RETURN:

//		case VK_LCONTROL:
		case VK_VOLUME_MUTE:
		case VK_VOLUME_DOWN:
		case VK_VOLUME_UP:
		case VK_MEDIA_NEXT_TRACK:
		case VK_MEDIA_PREV_TRACK:
		case VK_MEDIA_STOP:
		case VK_MEDIA_PLAY_PAUSE:
		case VK_BROWSER_BACK:
		case VK_BROWSER_FORWARD:
		case VK_BROWSER_REFRESH:
		case VK_BROWSER_STOP:
		case VK_BROWSER_SEARCH:
		case VK_BROWSER_FAVORITES:
		case VK_BROWSER_HOME:
            in.ki.dwFlags = in.ki.dwFlags | KEYEVENTF_EXTENDEDKEY;
            break;

        case 0x0A:
            in.ki.dwFlags = in.ki.dwFlags | KEYEVENTF_EXTENDEDKEY;
            in.ki.wVk = VK_RETURN;
            break;

        default:
            break;
        }

        // conver the virtual keycode to its equivalent keyboard scan code
        in.ki.wScan = (unsigned short)MapVirtualKeyEx(in.ki.wVk, MAPVK_VK_TO_VSC_EX, GetKeyboardLayout(0));
		//in.ki.wScan = (unsigned short)MapVirtualKeyEx(in.ki.wVk, MAPVK_VK_TO_VSC, 0);
		//in.ki.wVk = 0;
		in.ki.dwFlags |= KEYEVENTF_SCANCODE;
        in.type = INPUT_KEYBOARD;
		in.ki.time = GetTickCount();
		in.ki.dwExtraInfo = 0;

		#ifdef _DEBUG
		BOOST_LOG_TRIVIAL(info)
			<< "scancode: 0x" << std::hex << in.ki.wScan;
		#endif

    }

    if (inputs.size() > 0)
        SendInput((UINT)inputs.size(), &inputs[0], sizeof(INPUT));
}

// From Remy Lebeau's answer to a question on SO:
// https://stackoverflow.com/questions/31305404/sending-two-or-more-chars-using-sendinput/31307429#31307429
void SendInputString(const std::wstring &str)
{
	int len = str.length();
	if (len == 0) {
		BOOST_LOG_TRIVIAL(warning) << L"Snippet string length == 0";
		return;
	}
	//BOOST_LOG_TRIVIAL(debug) << L"Inserting snippet string: " << str;

	std::vector<INPUT> in(len * 2);
	ZeroMemory(&in[0], in.size() * sizeof(INPUT));

	int i = 0, idx = 0;
	while (i < len)
	{
		WORD ch = (WORD)str[i++];

		/*
		SendInput() caveat:-
		So that means if you want to send a Unicode codepoint that requires a 
		UTF-16 surrogate pair, you need 2 INPUTs, one for the high surrogate 
		and one for the low surrogate. That caveat is NOT mentioned in the 
		SendInput() documentation, but it is implied by the fact that the vScan 
		field is a 16bit WORD, and that KEYEVENTF_UNICODE events generate 
		WM_CHAR messages, which passes UTF-16 surrogate codeunits as separate
		messages.
		*/
		if ((ch < 0xD800) || (ch > 0xDFFF))
		{
			in[idx].type = INPUT_KEYBOARD;
			in[idx].ki.wScan = ch;
			in[idx].ki.dwFlags = KEYEVENTF_UNICODE;
			++idx;

			in[idx] = in[idx - 1];
			in[idx].ki.dwFlags |= KEYEVENTF_KEYUP;
			++idx;
		}
		else
		{
			in[idx].type = INPUT_KEYBOARD;
			in[idx].ki.wScan = ch;
			in[idx].ki.dwFlags = KEYEVENTF_UNICODE;
			++idx;

			in[idx].type = INPUT_KEYBOARD;
			in[idx].ki.wScan = (WORD)str[i++];
			in[idx].ki.dwFlags = KEYEVENTF_UNICODE;
			++idx;

			in[idx] = in[idx - 2];
			in[idx].ki.dwFlags |= KEYEVENTF_KEYUP;
			++idx;

			in[idx] = in[idx - 2];
			in[idx].ki.dwFlags |= KEYEVENTF_KEYUP;
			++idx;
		}
	}

	// Not sure if we need to AttachThreadInput the current input thread
	// before calling this. Would that make a difference?
	SendInput(in.size(), &in[0], sizeof(INPUT));
}

// Inserts a line of UTF8 text
static void insertLine(std::string const& utf8String)
{
	std::vector<WCHAR> buf(utf8String.length() * 4);
	int nLenWide = ::MultiByteToWideChar(
		CP_UTF8,
		0,
		(LPCSTR)(utf8String.c_str()),
		(int)(utf8String.size()),
		(LPWSTR)&buf[0],
		buf.size());
	if (nLenWide == 0) {
		BOOST_LOG_TRIVIAL(error) << "Error converting snippet line to UTF16, rc: " << ::GetLastError();
		return;
	}
	std::wstring str((wchar_t*)&buf[0], buf.size());
	SendInputString(str);
}

// Inserts a CRLF by poking VK_RETURN down/up
static void insertReturn()
{
	INPUT in[2];
	in[0].type = INPUT_KEYBOARD;
	in[0].ki.wVk = VK_RETURN;
	in[0].ki.dwFlags = 0;

	in[1].type = INPUT_KEYBOARD;
	in[1].ki.wVk = VK_RETURN;
	in[1].ki.dwFlags = KEYEVENTF_KEYUP;

	SendInput(2, in, sizeof(INPUT));
}

void WinPlatformServices::postString(const std::string& utf8String)
{
	try
	{
		size_t start = 0;
		while (start < utf8String.length()) {
			auto lfPos = utf8String.find('\n', start);
			auto line = utf8String.substr(start, lfPos - start);
			if (line.length())
			{
				BOOST_LOG_TRIVIAL(debug) << "Snippet line: " << line;
				insertLine(line);
			}
			start = lfPos;
			while (start < utf8String.length() && utf8String.at(start) == '\n') {
				BOOST_LOG_TRIVIAL(debug) << "Snippet insert LF";
				insertReturn();
				start++;
			}
		}

#if 0
		std::vector<WCHAR> buf(utf8String.length() * 4);
		int nLenWide = ::MultiByteToWideChar(
			CP_UTF8,
			0,
			(LPCSTR)(utf8String.c_str()),
			(int)(utf8String.size()),
			(LPWSTR)&buf[0],
			buf.size());
		if (nLenWide == 0) {
			BOOST_LOG_TRIVIAL(error) << "Error converting snippet string to UTF16, rc: " << ::GetLastError();
			return;
		}

		std::wstring str((wchar_t*)&buf[0], buf.size());
		SendInputString(str);
#endif
	}
	catch (const std::exception& e)
	{
		BOOST_LOG_TRIVIAL(error) << "Error converting snippet string to UTF16: " << e.what();
	}
}

void WinPlatformServices::postPointerClick(int button, bool fDown)
{
    static DWORD __downFlags[] = {
		0,
        MOUSEEVENTF_LEFTDOWN,
        MOUSEEVENTF_RIGHTDOWN,
		0,
        MOUSEEVENTF_MIDDLEDOWN
    };
    static DWORD __upFlags[] = {
		0,
        MOUSEEVENTF_LEFTUP,
        MOUSEEVENTF_RIGHTUP,
		0,
        MOUSEEVENTF_MIDDLEUP
    };

	if (button < 1 || button >= sizeof(__downFlags) / sizeof(__downFlags[0]))
		return;

	auto index = button; // button >= 1 && button <= sizeof(__downFlags) / sizeof(__downFlags[0]) ? button : 0;

    //BOOST_LOG_TRIVIAL(debug)
    //    << "postPointerClick - button index: " << index
    //    << ", dir: " << (fDown ? "DOWN" : "UP");
 
    POINT pos;
    GetCursorPos(&pos);

    INPUT input = { 0 };
    input.type = INPUT_MOUSE;
    input.mi.dwFlags = fDown ? __downFlags[index] : __upFlags[index];
    input.mi.dx = pos.x;
    input.mi.dy = pos.y;
    input.mi.time = GetTickCount();
    SendInput(1, &input, sizeof(INPUT));
}

void WinPlatformServices::configurePointer(int speed, int acceleration, unsigned scrollLines)
{
    /*
        SPI_GETMOUSESPEED

        Retrieves the current mouse speed. The mouse speed determines how far 
        the pointer will move based on the distance the mouse moves. The 
        pvParam parameter must point to an integer that receives a value which 
        ranges between 1 (slowest) and 20 (fastest). A value of 10 is the 
        default. The value can be set by an end-user using the mouse control 
        panel application or by an application using SPI_SETMOUSESPEED.

        SPI_SETMOUSESPEED

        Sets the current mouse speed. The pvParam parameter is an integer 
        between 1 (slowest) and 20 (fastest). A value of 10 is the default. 
        This value is typically set using the mouse control panel application.
    */
    int curSpeed = 0;
    SystemParametersInfo(SPI_GETMOUSESPEED, 0, (PVOID)&curSpeed, 0);
    //BOOST_LOG_TRIVIAL(debug) << "configurePointer - "
    //    << "speed: " << speed
    //    << ", acceleration: " << acceleration
    //    << ", scrollLines: " << scrollLines;

    // 'speed' is supplied in normalized scale of 1-100. Converted this to 
    // 1-20 as expected by SP_SETMOUSESPEED
	int newSpeed = (int)std::min((int)std::max(((float)speed / 100) * 20, (float)1), 20);
    //BOOST_LOG_TRIVIAL(debug) << "Setting SPI_SETMOUSESPEED - "
    //    << "current: " << curSpeed
    //    << ", new: " << newSpeed;
    if (curSpeed != newSpeed)
    {
        //BOOST_LOG_TRIVIAL(debug) << "Setting mouse speed";
        // Disable "warning C4312: 'type cast': conversion from 'int' to 'PVOID' of greater size"
        #pragma warning(push)
        #pragma warning(disable : 4312)
        SystemParametersInfo(SPI_SETMOUSESPEED, 0, (PVOID)newSpeed, 0);
        #pragma warning(pop)
    }

    /*
        SPI_GETMOUSE

        Retrieves the two mouse threshold values and the mouse acceleration.
        The pvParam parameter must point to an array of three integers that
        receives these values. See mouse_event for further information.

        SPI_SETMOUSE

        Sets the two mouse threshold values and the mouse acceleration. The 
        pvParam parameter must point to an array of three integers that 
        specifies these values. See mouse_event for further information.
    */
    int accelParam[3] = { 0 };
    SystemParametersInfo(SPI_GETMOUSE, 0, (PVOID)&accelParam, 0);

    // 'acceleration' is also supplied in the normalized scale of 1-100. 
    // Convert it to 0-3 so that we can index into ACCEL_PROFILE array.
	int accelParamIndex = (int)std::min(((float)acceleration / 100) * 3, (float)3);
    const int* pAccel = &ACCELERATION_PROFILES[accelParamIndex * 3];
    //BOOST_LOG_TRIVIAL(debug) << "Setting SPI_SETMOUSE - "
    //    << "current: " << accelParam[0]
    //    << ", " << accelParam[1]
    //    << ", " << accelParam[2]
    //    << ", new: "
    //    << *pAccel
    //    << ", " << *(pAccel + 1)
    //    << ", " << *(pAccel + 2);
    if (accelParam[0] != pAccel[0]
        || accelParam[1] != pAccel[1]
        || accelParam[2] != pAccel[2])
    {
        //BOOST_LOG_TRIVIAL(debug) << "Setting acceleration parameters";
        SystemParametersInfo(SPI_SETMOUSE, 0, (PVOID)pAccel, 0);
    }

    /*
        SPI_GETWHEELSCROLLLINES

        Retrieves the number of lines to scroll when the vertical mouse wheel
        is moved. The pvParam parameter must point to a UINT variable that
        receives the number of lines. The default value is 3.

        SPI_SETWHEELSCROLLLINES

        Sets the number of lines to scroll when the vertical mouse wheel is 
        moved. The number of lines is set from the uiParam parameter.
        
        The number of lines is the suggested number of lines to scroll when the 
        mouse wheel is rolled without using modifier keys. If the number is 0, 
        then no scrolling should occur. If the number of lines to scroll is 
        greater than the number of lines viewable, and in particular if it is 
        WHEEL_PAGESCROLL (#defined as UINT_MAX), the scroll operation should be 
        interpreted as clicking once in the page down or page up regions of the 
        scroll bar.
    */
    UINT lines = 0;
    SystemParametersInfo(SPI_GETWHEELSCROLLLINES, 0, (PVOID)&lines, 0);
    //BOOST_LOG_TRIVIAL(debug) << "Setting SPI_SETWHEELSCROLLLINES: "
    //    << "current: " << lines
    //    << ", new: " << scrollLines;
    if (lines != scrollLines)
    {
        //BOOST_LOG_TRIVIAL(debug) << "Setting wheel scroll lines";
        SystemParametersInfo(SPI_SETWHEELSCROLLLINES, scrollLines, NULL, 0);
    }

}

void WinPlatformServices::openResource(const std::string& resource, const std::vector<std::string>& args, int type)
{
    // TODO: move to WinPlatformServices.cpp
    CHAR workingPath[MAX_PATH] = { 0 };
    if (SUCCEEDED(SHGetFolderPathA(
        NULL,
        CSIDL_DESKTOPDIRECTORY,
        NULL,
        SHGFP_TYPE_CURRENT,
        workingPath)))
    {
        ShellExecuteA(NULL, NULL, resource.c_str(), NULL, workingPath, SW_SHOWNORMAL);
    }
}

std::string WinPlatformServices::getPreferencesFolder()
{
    PWSTR pszPath{ nullptr };
    if (::SHGetKnownFolderPath(
        FOLDERID_RoamingAppData,
        0,
        NULL,
        &pszPath) == S_OK)
    {
        std::wstring sPath(pszPath);
        ::CoTaskMemFree(pszPath);
        if (sPath.at(sPath.length() - 1) != TEXT('\\'))
            sPath.append(TEXT("\\"));
        sPath.append(TEXT("Kensington\\TrackballWorks"));
        return toUTF8(sPath);
    }
    return std::string();
}

std::string WinPlatformServices::getLogFileFolder()
{
    return getPreferencesFolder();
}

void maximizeFgWindow(nlohmann::json const& args)
{
    PostMessage(::GetForegroundWindow(), WM_SYSCOMMAND, SC_MAXIMIZE, 0);
}

void minimizeFgWindow(nlohmann::json const& args)
{
    PostMessage(::GetForegroundWindow(), WM_SYSCOMMAND, SC_MINIMIZE, 0);
}

void closeFgWindow(nlohmann::json const& args)
{
    PostMessage(::GetForegroundWindow(), WM_SYSCOMMAND, SC_CLOSE, 0);
}

void lockWorkstation(nlohmann::json const& args)
{
    ::LockWorkStation();
}

struct MinimizeAllButCurrentParams {
    DWORD dwPid;
    HWND hWnd;
    bool appwise;
};

static BOOL CALLBACK MinimizeAllButOneCallback(HWND hwnd, LPARAM lp)
{
    BOOL bMinimize = FALSE;
    MinimizeAllButCurrentParams *param =
        reinterpret_cast<MinimizeAllButCurrentParams*>(lp);

    if (IsWindowVisible(hwnd))
    {
        if (param->hWnd != hwnd)
        {
            if (param->appwise)
            {
                DWORD pid = 0;
                GetWindowThreadProcessId(hwnd, &pid);
                if (pid != param->dwPid)
                {
                    bMinimize = TRUE;
                }
            }
            else bMinimize = TRUE;
        }

        if (bMinimize)
            PostMessage(hwnd, WM_SYSCOMMAND, SC_MINIMIZE, 0);
    }

    return TRUE;
}

void minimizeAllButFgWindow(nlohmann::json const& args)
{
    MinimizeAllButCurrentParams param = { 0 };
    param.hWnd = ::GetForegroundWindow();
    ::GetWindowThreadProcessId(param.hWnd, &param.dwPid);
    param.appwise = true;
    ::EnumWindows(
        MinimizeAllButOneCallback,
        (LPARAM)&param);
}

void WinPlatformServices::system(std::string const& action, nlohmann::json const& addlArgs)
{
    typedef void(*PFN_SYSTEMACTIONHANDLER)(nlohmann::json const& args);
    struct ActionDispatchTable {
        const char* action;
        PFN_SYSTEMACTIONHANDLER pfnHandler;
    } handlers[] = {
        { "maximizeWindow", maximizeFgWindow },
        { "minimizeWindow", minimizeFgWindow },
        { "closeWindow", closeFgWindow },
        { "minimizeAll", minimizeAllButFgWindow },
        { "lockWorkstation", lockWorkstation },
    };

    bool actionTaken = false;
    for (auto elem : handlers)
    {
        if (action == elem.action)
        {
            elem.pfnHandler(addlArgs);
            actionTaken = true;
            break;
        }
    }

    if (!actionTaken)
    {
        BOOST_LOG_TRIVIAL(error) << "Could not found handler for system action '" << action << "'";
    }
}

nlohmann::json WinPlatformServices::getFavoriteApps(int count, const std::set<std::string>& exclude)
{
	return ::getFavApps(count, exclude);
}

std::string WinPlatformServices::showPopupMenu(std::vector<std::string> const& items, std::vector<std::string> const& labels, int timeOut/*=-1*/)
{
	// Create a popup menu first
	HMENU hMenu = CreatePopupMenu();
	if (!hMenu)
		return std::string();

	{
		std::string item("KensingtonWorks");
		MENUITEMINFOA mi = { 0 };
		mi.cbSize = sizeof(mi);
		mi.fMask = MIIM_STRING | MIIM_ID | MIIM_STATE;
		mi.wID = 1000;
		mi.fType = MFT_STRING;
		mi.fState = MF_DISABLED;
		mi.dwTypeData = (LPSTR)item.c_str();
		mi.cch = (UINT)item.length();
		::InsertMenuItemA(
			hMenu,
			0,
			TRUE,
			&mi
		);
	}

	{
		MENUITEMINFOA mi = { 0 };
		mi.cbSize = sizeof(mi);
		mi.fMask = MIIM_STRING | MIIM_ID;
		mi.wID = 1001;
		mi.fType = MFT_SEPARATOR;
		mi.fState = MFS_DISABLED;
		::InsertMenuItemA(
			hMenu,
			1,
			TRUE,
			&mi
		);
	}

	std::vector<std::string> actualItems, itemsWithIndex;
	//UINT index = 1;
	const size_t MAX_ENTRY_LENGTH = 128;
	for (size_t i = 0; i < items.size(); i++)
	{
		std::string const& entry = items[i];

		// skip over empty snippets
		if (entry.length() == 0)
			continue;

		std::string label = labels[i];
		if (label.length() == 0)
			label = entry;

		std::stringstream ss;
		//if (index == 10)
		//	ss << "1&0. ";
		//else
		//	ss << "&" << index << ". ";
		ss << label.substr(0, MAX_ENTRY_LENGTH);	// restrict width
		if (label.length() > MAX_ENTRY_LENGTH)
			ss << "..."; // ellipsis to indicate text has been clipped
		itemsWithIndex.push_back(ss.str());
		actualItems.push_back(entry);
		//index++;
		//if (index > 20)
		//	break;
	}

	UINT index = 1;
	for (auto entry: itemsWithIndex)
	{
		std::wstring wEntry = toUTF16(entry);
		MENUITEMINFOW mi = { 0 };
		mi.fMask = MIIM_STRING | MIIM_ID;
		mi.wID = index++;
		mi.fType = MFT_STRING;
		mi.dwTypeData = (LPWSTR)wEntry.c_str();
		mi.cch = (UINT)wEntry.length();
		mi.cbSize = sizeof(mi);
		::InsertMenuItemW(
			hMenu,
			1000, // number that exceeds menu item count ensures items are appended
			TRUE,
			&mi
		);
	}

	POINT pt = { 0 };
	::GetCursorPos(&pt);

	// Make device manager window the foreground window
	HWND hWndFg = GetForegroundWindow();
	::AttachThreadInput(
		GetCurrentThreadId(),
		GetWindowThreadProcessId(hWndFg, NULL), 
		TRUE);
	::SetForegroundWindow(g_hWndDeviceManager);

	// now display the menu
	UINT uSelected = (UINT)::TrackPopupMenu(
		hMenu,
		::GetSystemMetrics(SM_MENUDROPALIGNMENT) | TPM_TOPALIGN | TPM_RETURNCMD | TPM_RIGHTBUTTON,
		pt.x,
		pt.y,
		0,
		g_hWndDeviceManager,
		NULL
	);
	// Restore the original foreground window
	::SetForegroundWindow(hWndFg);
	::AttachThreadInput(
		GetCurrentThreadId(),
		GetWindowThreadProcessId(hWndFg, NULL),
		FALSE);

	DestroyMenu(hMenu);

	// Item indices start from 1, so subtract 1 from the selected item
	// index. If none was selected, this would return -1 as per the function
	// specification.
	if (uSelected >= 1) {
		return actualItems[uSelected - 1];
	}
	return std::string();
}

uint32_t WinPlatformServices::getCurPointerSpeed()
{
	int curSpeed = 50;
	// Mouse speed is in the range 1-20, with 10 being the normal 1x speed.
	if (SystemParametersInfo(SPI_GETMOUSESPEED, 0, (PVOID)&curSpeed, 0)) {
		// Value returned is 1-20. Convert it to 1-100 scale
		curSpeed *= 5;
	}
	return (uint32_t)curSpeed;
}

/*
 * 2019/10/12
 * There's no way to accurately determine acceleration rate in Windows as
 * the setting is specified as arbitrary threshold values for mouse dx,xy
 * values. If the dx or dy values exceed this threshold, they are doubled.
 * And they are doubled again, if the resultant value exceeds the second 
 * threshold. So we just check acceleration flag and if it's non-zero,
 * return a 'safe' arbitrary value as the current acceleration rate.
 *
 * In any case the value returned by this function is used to reflect to the
 * user the current pointer configuration state and it's not critical that
 * we reflect the accurate value.
 */
uint32_t WinPlatformServices::getCurPointerAccelerationRate()
{
	/*
	SPI_GETMOUSE

	Retrieves the two mouse threshold values and the mouse acceleration.
	The pvParam parameter must point to an array of three integers that
	receives these values. See mouse_event for further information.
	*/
	int accelParam[3] = { 0 };
	if (SystemParametersInfo(SPI_GETMOUSE, 0, (PVOID)&accelParam, 0)) {
		if (accelParam[2] > 0) {
			return 10; // arbitrary value!
		}
	}
	return 0;	// no acceleration
}

uint32_t WinPlatformServices::getCurScrollSpeed()
{
	UINT lines = 1;	// default to 1
	// If this call fails, we return default value 1.
	SystemParametersInfo(SPI_GETWHEELSCROLLLINES, 0, (PVOID)&lines, 0);
	return (uint32_t)lines;
}

bool WinPlatformServices::getScrollInvert()
{
	// Windows doesn't seem to have a SPI_GET* macro which
	// we can read for this setting. Moreover Win10 control panel doesn't even
	// have a scroll direction setting. So returning false.
	return false;
}

void WinPlatformServices::postScrollNavigation(ScrollDirection direction, int lines)
{
    uint16_t keyCode = 0;
    switch (direction) {
    case SD_UP: keyCode = VK_UP; break;
    case SD_DOWN: keyCode = VK_DOWN; break;
    case SD_LEFT: keyCode = VK_LEFT; break;
    case SD_RIGHT: keyCode = VK_RIGHT; break;
	default:
		return;
    }
    
    for (int i = 0; i < lines; i++)
    {
        postKeystroke(0, keyCode, true, false);
        postKeystroke(0, keyCode, false, false);
    }
}
