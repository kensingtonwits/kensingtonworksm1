//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by tbwhelper.rc
//
#define IDC_MYICON                      2
#define IDD_TBWHELPER_DIALOG            102
#define IDS_APP_TITLE                   103
#define IDD_ABOUTBOX                    103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDI_TBWHELPER                   107
#define IDI_SMALL                       108
#define IDC_TBWHELPER                   109
#define IDR_MAINFRAME                   128
#define IDI_ICON_GRAY                   129
#define IDI_ICON_ERROR                  130
#define IDI_ICON_NORMAL                 131
#define IDR_MENU1                       132
#define IDR_TRAYMENU                    132
#define ID_TEST_KEYSTROKE               32771
#define ID_TEST_SNIPPET                 32772
#define ID_TEST_LAUNCH                  32773
#define ID_TEST_OPENFOLDER              32774
#define ID_TEST_SEQUENCE                32775
#define ID_TEST_CHORD1                  32776
#define ID_TEST_CHORD2                  32777
#define ID_TEST_SLOWPOINTER             32778
#define ID_TEST_SINGLEAXISMOVEMENT      32779
#define ID_TEST_INERTIALSCROLL          32780
#define ID_TEST_WTSCONNECT              32781
#define ID_Menu                         32782
#define ID_OPEN_KENSINGTONWORKS         32783
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        133
#define _APS_NEXT_COMMAND_VALUE         32784
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
