#include "stdafx.h"
#include <functional>
#include <algorithm>
#include <iomanip>
#include <cassert>
#include <WtsApi32.h>

#include <boost/filesystem/path.hpp>

#include "App.h"
#include "resource.h"
#include "DeviceManager.h"
#include "utils.h"
#include "globals.h"


#ifdef COMMAND_MESSAGE_QUEUE
#include "CmdMsger.h"
#else
#include "CommandServer.h"
#endif

#include "Settings.h"
#include "launchgui.h"
#include "version.h"

#ifdef DEBUG
#include "PlatformServices.h"
#endif // DEBUG

#define SHOW_TRAYICON
#define HIDE_TRAYICON_WHEN_MAIN_WINDOW_IS_SHOWN     0

#define WM_OPEN_KENSINGTONWORKS     WM_USER + 100

#ifdef COMMAND_MESSAGE_QUEUE
	//CommandMsgHandler *CommandMsgHandler::m_instance = nullptr;
#endif

class TbwHelper {
    DeviceManager* _deviceManager;
#ifdef COMMAND_MESSAGE_QUEUE
//	std::unique_ptr<CommandMsgHandler> _cmdMsgHandler;
	//CommandMsgHandler *_cmdMsgHandler = nullptr;
#else
    std::unique_ptr<CommandServer> _httpServer;
#endif

public:
    TbwHelper()
        : _deviceManager(DeviceManager::getInstance())
#ifdef COMMAND_MESSAGE_QUEUE
		//, _cmdMsgHandler(new CommandMsgHandler(_deviceManager, true)) //false))
		//, _cmdMsgHandler(DeviceManager::getInstance(_deviceManager, true)) //false))
#else
        , _httpServer(new CommandServer(_deviceManager, false))
#endif
    {
        try
        {
            _deviceManager->start();
#ifdef COMMAND_MESSAGE_QUEUE
			CommandMsger::CommandMsger_init(_deviceManager, false);
			CommandMsger::CommandMsger_Start();
#else
            _httpServer->Start();
#endif
        }
        catch (const std::exception& e)
        {

			BOOST_LOG_TRIVIAL(info) << "Exception: " << e.what();
			std::cout << "Exception: " << e.what()  << "\r\n";
            // We only need to stop _deviceManager as if _httpServer failed, 
            // ctor() unwind semantics would delete the unique_ptr<> object.
            // So all that needs to be done is to DeviceManager::stop() to
            // release its resources.
            _deviceManager->stop();
            throw;
        }
    }
    ~TbwHelper()
    {
#ifdef COMMAND_MESSAGE_QUEUE
		CommandMsger::CommandMsger_Stop();
#else
		_httpServer->Stop();
#endif
		BOOST_LOG_TRIVIAL(info) << "~TbwHelper() " ;
		std::cout << " ~TbwHelper() \r\n";

		_deviceManager->stop(); // stop() would destroy the singleton!
    }
};

App* App::_instance = nullptr;

static const wchar_t* LOG_TAG = L"App";

// Message handler for about box.
INT_PTR CALLBACK AboutDlgProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}

// A simple RAII class to init log file's fullname including its path.
// We store log files in AppData/Roaming/Kensington/TrackballWorks, the same
// location where we keep application setings.
class LogFileFullName : public boost::filesystem::path {

    typedef boost::filesystem::path baseClass;
public:
    LogFileFullName() :
        baseClass(Settings::getFileFolder())
    {
        append("tbwhelper.log");
    }
};

App::App()
    : _hInstance(NULL)
    , _hWnd(NULL)
    , _trayIcon(nullptr)
    , _wtsSessionChangeRegd(FALSE)
    , _helper(nullptr)
{
}

App::~App()
{
}

void App::init(bool fTestMode/* = false*/)
{
    if (!createUI())
        return;
#if 0
    DeviceManager* pDevMgr = DeviceManager::getInstance();
    if (!pDevMgr)
        return;

    pDevMgr->start();

    _server = new CommandServer(pDevMgr, fTestMode);
    _server->Start();
#else
    _helper = new TbwHelper();
    _trayIcon->SetVisible(DeviceManager::getInstance()->trayIcon());
#endif
}

void App::run()
{
    MSG msg;
    while (::GetMessage(&msg, NULL, 0, 0)) {
        ::TranslateMessage(&msg);
        ::DispatchMessage(&msg);
    }
}

void App::testKeyStroke()
{
	auto devices = DeviceManager::getInstance()->getDevices();
}

void App::testSnippet()
{
    try
    {
        /*
        DeviceManager* pDevMgr = DeviceManager::getInstance();
        Settings& s1 = pDevMgr->getSettings();
        json j = s1;
        std::stringstream ss;
        ss << j << "\n";
        OutputDebugStringA(ss.str().c_str());

        Settings s2 = j.get<Settings>();

        assert(s1 == s2);
        */
    }
    catch (const std::exception& e)
    {
        OutputDebugStringA(e.what());
    }
}

void App::testLaunch()
{
}

void App::testOpenFolder()
{
}

void App::testSequence()
{
}

void App::testDoubleClick()
{
}

void App::testTripleClick()
{
}

void App::testChord1()
{
    DeviceManager* pDevMgr = DeviceManager::getInstance();
    if (pDevMgr)
    {
        pDevMgr->emulateTestDeviceButtonActivity(0x01, 0);
        pDevMgr->emulateTestDeviceButtonActivity(0x00, 0x01);
        Sleep(30);
        pDevMgr->emulateTestDeviceButtonActivity(0x02, 0);
        pDevMgr->emulateTestDeviceButtonActivity(0x00, 0x02);
    }
}

void App::testChord2()
{
    DeviceManager* pDevMgr = DeviceManager::getInstance();
    if (pDevMgr)
    {
        pDevMgr->emulateTestDeviceButtonActivity(0x08, 0x00);
        pDevMgr->emulateTestDeviceButtonActivity(0x00, 0x08);
        Sleep(60);
        pDevMgr->emulateTestDeviceButtonActivity(0x04, 0x00);
        pDevMgr->emulateTestDeviceButtonActivity(0x00, 0x04);
    }
}

void App::slowPointer()
{
}

void App::singleAxisMovement()
{
}

void App::inertialScroll()
{
}

bool App::createUI()
{
    //  Registers the window class.
    WNDCLASSEXW wcex = { 0 };

    HINSTANCE hInstance = ::GetModuleHandle(NULL);

    wcex.cbSize = sizeof(WNDCLASSEX);
    wcex.style = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc = App::_WndProc;
    wcex.cbClsExtra = 0;
    wcex.cbWndExtra = 0;
    wcex.hInstance = NULL;
    wcex.hIcon = LoadIcon(NULL, MAKEINTRESOURCE(IDI_TBWHELPER));
    wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
    wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_TBWHELPER);
    wcex.lpszClassName = TBHELPER_WND_CLASS_NAME;
    wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_TBWHELPER));

    if (!RegisterClassExW(&wcex))
        return false;

    TCHAR szTitle[256] = { 0 };
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, 256);
    HWND hWnd = CreateWindowW(
        TBHELPER_WND_CLASS_NAME, szTitle, WS_OVERLAPPEDWINDOW,
        CW_USEDEFAULT, 0, 320, 240, nullptr, nullptr, hInstance, nullptr);

    if (!hWnd)
        return false;

    _hWnd = hWnd;
    _hInstance = hInstance;
#ifdef _DEBUG
    ShowWindow(hWnd, SW_SHOW);
#else
    ShowWindow(hWnd, SW_HIDE);
#endif
    UpdateWindow(hWnd);

#ifdef SHOW_TRAYICON
    // create trayicon
    _trayIcon = new CTrayIcon(GetTrayIconLabel().c_str(),
        false,
        LoadIcon(NULL, MAKEINTRESOURCE(IDI_TBWHELPER)));

    _trayIcon->SetListener(this);
    _trayIcon->SetIcon(LoadIcon(hInstance, MAKEINTRESOURCE(IDI_TBWHELPER)));
#endif
    return true;
}

LRESULT CALLBACK App::_WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    return App::getInstance()->WndProc(hWnd, uMsg, wParam, lParam);
}

LRESULT App::WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    switch (uMsg)
    {
    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            // Parse the menu selections:
            switch (wmId)
            {
            case IDM_ABOUT:
                DialogBox(_hInstance, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, AboutDlgProc);
                break;

            case IDM_EXIT:
                DestroyWindow(hWnd);
                break;
#ifdef _DEBUG
            case ID_TEST_KEYSTROKE:
                testKeyStroke();
                break;

            case ID_TEST_SNIPPET:
                testSnippet();
                break;

            case ID_TEST_LAUNCH:
                testLaunch();
                break;

            case ID_TEST_OPENFOLDER:
                testOpenFolder();
                break;

            case ID_TEST_SEQUENCE:
                //testSequence();
#ifdef DEBUG
				{
					std::vector<std::string> menuItems;
					menuItems.push_back("Hello World 1");
					menuItems.push_back("Hello World 2");
					menuItems.push_back("Hello World 3");
					menuItems.push_back("Hello World 4");
					menuItems.push_back("Hello World 5");
					menuItems.push_back("Hello World 6");
					menuItems.push_back("Hello World 7");
					menuItems.push_back("Hello World 8");
					menuItems.push_back("Hello World 9");
					menuItems.push_back("Hello World 10 The quick brown fox jumps over the lazy dog The quick brown fox jumps over the lazy dog The quick brown fox jumps over the lazy dog");
					std::string selected = PlatformServices::getInstance()->showPopupMenu(menuItems, menuItems);
				}
#endif // DEBUG
	            break;

            case ID_TEST_CHORD1:
                testChord1();
                break;

            case ID_TEST_CHORD2:
                testChord2();
                break;

            case ID_TEST_SLOWPOINTER:
                slowPointer();
                break;

            case ID_TEST_SINGLEAXISMOVEMENT:
                singleAxisMovement();
                break;

            case ID_TEST_INERTIALSCROLL:
                inertialScroll();
                break;

            case ID_TEST_WTSCONNECT:
                try
                {
                    if (_helper)
                    {
						#ifndef COMMAND_MESSAGE_QUEUE
                        delete _helper; _helper = nullptr;
						#endif
                    }
                    else
                    {
                        _helper = new TbwHelper();
                    }
                }
                catch (const std::exception& e)
                {
                    BOOST_LOG_TRIVIAL(error) << "Exception: " << e.what();
                }
                break;
#endif
            default:
                return DefWindowProc(hWnd, uMsg, wParam, lParam);
            }
        }
        break;

    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: Add any drawing code that uses hdc here...
            EndPaint(hWnd, &ps);
        }
        break;

    case WM_SIZE:
        if (wParam == SIZE_MINIMIZED)
        {
#ifdef SHOW_TRAYICON
    #if HIDE_TRAYICON_WHEN_MAIN_WINDOW_IS_SHOWN  
            ShowWindow(hWnd, SW_HIDE);
            _trayIcon->SetVisible(true);
    #endif
#endif
            return 0;
        }
        break;

    case WM_CREATE:
        return OnCreate(hWnd, wParam, lParam);

    case WM_DESTROY:
        return OnDestroy(hWnd, wParam, lParam);

    case WM_WTSSESSION_CHANGE:
        return OnSessionChange(hWnd, wParam, lParam);

    case WM_POWERBROADCAST:
        return OnPowerBroadcast(hWnd, wParam, lParam);

    case WM_OPEN_KENSINGTONWORKS:
        return OnOpenKensingtonWorks(hWnd, wParam, lParam);

    default:
        return DefWindowProc(hWnd, uMsg, wParam, lParam);
    }
    return 0;
}

LRESULT App::OnCreate(HWND hWnd, WPARAM wParam, LPARAM lParam)
{
    // Register for session change notifications
    _wtsSessionChangeRegd = WTSRegisterSessionNotification(hWnd, NOTIFY_FOR_THIS_SESSION);
    return DefWindowProc(hWnd, WM_CREATE, wParam, lParam);
}

LRESULT App::OnDestroy(HWND h, WPARAM w, LPARAM l)
{
    if (_wtsSessionChangeRegd)
        ::WTSUnRegisterSessionNotification(h);
    PostQuitMessage(0);
    // Posting WM_QUIT does not result in MsgWaitForMultipleObjects()
    // getting signalled to handle it and therefore cause the message
    // loop to exit. So we set WFMOHandler's QUIT event to trigger
    // loop exit.
    //WFMOHandler::Stop();
    return 0; // DefWindowProc(h, WM_DESTROY, w, l);
}

LRESULT App::OnSessionChange(HWND hWnd, WPARAM wParam, LPARAM lParam)
{
 //   BOOST_LOG_TRIVIAL(debug) << "Session change, code: " << wParam
 //       << ", session id: " << lParam;

    switch (wParam)
    {
    case WTS_CONSOLE_CONNECT:
    //case WTS_REMOTE_CONNECT:
        // create TbwHelper object
        BOOST_LOG_TRIVIAL(debug) << "Session change, CONSOLE_CONNECT";
        try
        {
            if (!_helper)
                _helper = new TbwHelper();
        }
        catch (const std::exception& e)
        {
            BOOST_LOG_TRIVIAL(error) << "Session connection - error creating helper: " << e.what();
        }
        break;

    case WTS_CONSOLE_DISCONNECT:
    //case WTS_REMOTE_DISCONNECT:
        // destroy global TbwHelper object
        BOOST_LOG_TRIVIAL(debug) << "Session change, CONSOLE_DISCONNECT";
        try
        {
			#ifndef COMMAND_MESSAGE_QUEUE
			delete _helper; _helper = nullptr;
			#endif
		}
        catch (const std::exception& e)
        {
            BOOST_LOG_TRIVIAL(error) << "Session disconnection - error destroying helper: " << e.what();
        }
        break;
    }

    return 0l;
}

LRESULT App::OnPowerBroadcast(HWND h, WPARAM w, LPARAM l)
{
    switch (w)
    {
    case PBT_APMSUSPEND:
        BOOST_LOG_TRIVIAL(debug) << "Power SUSPEND";
        try
        {
			#ifndef COMMAND_MESSAGE_QUEUE
			delete _helper; _helper = nullptr;
			#endif
		}
        catch (const std::exception& e)
        {
            BOOST_LOG_TRIVIAL(error) << "Power SUSPEND - error destroying helper: " << e.what();
        }
        break;

    case PBT_APMRESUMEAUTOMATIC:
        BOOST_LOG_TRIVIAL(info) << "Power - RESUME";
        try
        {
            // we should only create it if we're the active session
            // That is in a multi user logged state, if the user system goes
            // into sleep and recovers, will this message be sent to both
            // the sessions?
            if (!_helper)
                _helper = new TbwHelper();
        }
        catch (const std::exception& e)
        {
            BOOST_LOG_TRIVIAL(info) << "Power RESUME - error creating helper: " << e.what();
        }
        break;

    default:
        break;
    }
    return 1L;
}

LRESULT App::OnOpenKensingtonWorks(HWND, WPARAM, LPARAM)
{
    // TODO: Start KensingtonWorks GUI
#ifdef _DEBUG
    OutputDebugStringA("Open KensingtonWorks GUI\r\n");
#endif
	launchGui();
    return 0;
}

void App::OnTrayIconLButtonDblClk(CTrayIcon * pTrayIcon)
{
    PostMessage(_hWnd, WM_OPEN_KENSINGTONWORKS, 0, 0);
}

void App::OnTrayIconRButtonUp(CTrayIcon * pTrayIcon)
{
	ShowTrayMenu(pTrayIcon);
}

std::wstring App::GetTrayIconLabel()
{
	std::wstring trayText(L"KensingtonWorks ");
	trayText.append(toUTF16(_PRODUCTVERSION_STR));
	auto pos = trayText.rfind(L'.');
	if (pos != std::wstring::npos) {
		trayText = trayText.substr(0, pos);
	}
	return trayText;
}

void App::ShowTrayMenu(CTrayIcon* pTrayIcon)
{
	POINT pt;
	if (GetCursorPos(&pt))
	{
		HMENU menu = CreatePopupMenu();
		AppendMenu(menu, MF_STRING, 3, GetTrayIconLabel().c_str());

		// Make ourselves the foreground window before calling TrackPopupMenu.
		// This ensures that if the user clicks anywhere outside of the popup
		// menu, the popup menu will be auto dismissed by the system.
		HWND hWndFg = GetForegroundWindow();
		::AttachThreadInput(
			GetCurrentThreadId(),
			GetWindowThreadProcessId(hWndFg, NULL),
			TRUE);
		::SetForegroundWindow(_hWnd);

		UINT cmd = TrackPopupMenu(
			menu,
			TPM_RETURNCMD | TPM_RIGHTBUTTON,
			pt.x,
			pt.y,
			0,
			_hWnd,
			NULL);
		if (cmd == 3)
		{
			PostMessage(_hWnd, WM_OPEN_KENSINGTONWORKS, 0, 0);
		}

		// restore the original foreground window
		::SetForegroundWindow(hWndFg);
		// Detach our input queue from the last foreground window input queue.
		::AttachThreadInput(
			GetCurrentThreadId(),
			GetWindowThreadProcessId(hWndFg, NULL),
			FALSE);
	}
}


App* App::getInstance()
{
    if (nullptr == App::_instance)
        App::_instance = new App();
    return App::_instance;
}

bool App::instanceExists()
{
    // Just check for the presence of tbwhelper main window class
    return ::FindWindow(TBHELPER_WND_CLASS_NAME, NULL) != NULL;
}

void App::destroy()
{
#if 0
    // stop CommandServer first
    if (_server)
    {
        _server->Stop();
        delete _server;
    }

    DeviceManager* pDevMgr = DeviceManager::getInstance();
    pDevMgr->stop();
#else
    delete _helper; _helper = nullptr;
#endif

    if (_trayIcon)
        delete _trayIcon;
    _trayIcon = nullptr;

    delete this;
}
