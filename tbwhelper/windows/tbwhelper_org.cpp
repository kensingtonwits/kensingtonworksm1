// tbwhelper.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include <wtsapi32.h>

//#define _LEGACY_CODE    1

#include "tbwhelper.h"


#include "TrayIcon.h"
#include "globals.h"
#include "cmdline.h"
#include "TbwPreferences.h"
#ifdef _LEGACY_CODE
#include "KernelClient.h"
#include "Player.h"
#include "appsens.h"
#endif

#include "App.h"
#include "logfmwk.h"

#define WM_TRAYNOTIFY		WM_USER+1024
#define WM_FSCHANGENOTIFY	WM_USER+1025
#define WM_PLAYEREVENT		WM_USER+1026
#define WM_UPDATETRAYICON	WM_USER+1027
#define WM_POSTEDACTION		WM_USER+1028
#define WM_RESTARTHELPER	WM_USER+1029

#define kTrayMenuDelay	2
#define kTimerPeriod	350
#define kTbwString	TEXT("Trackballsworks Helper")
#define kTbwGUIString TEXT("TrackballWorks")

#define kDeviceCountError -1L

#ifdef _LEGACY_CODE
// externs
unsigned long cSpecPref = -1;
unsigned long cGlobPref = -1;
HMODULE gResModule = NULL;

#define noCoreCounts		6
#define purgeCacheDivider	20
#define loadPrefsTimeWindow	1000

static BOOL restartHelper = FALSE;

static HWND hHelperWindow = NULL;
static DWORD lastFSChange = 0L;
static DWORD openingTrayMenuCount = 0;

static UINT shellHookMessage = 0;					// a value of 0 means that no Shell Hook has been set.
static BOOL waitingForShellHookMessage = TRUE;

static HWND helperHwnd = NULL;

static DWORD deviceCount = 0;
#endif

#define MAX_LOADSTRING 100
#define HIDE_TRAYICON_WHEN_MAIN_WINDOW_IS_SHOWN     1

// Global Variables:
HINSTANCE g_hInstance = NULL;                   // current instance
WCHAR szTitle[MAX_LOADSTRING] = { 0 };          // The title bar text
HWND g_hMainWnd = NULL;                         // main window
wchar_t WINDOW_CLASS_NAME[] = L"TbwHelperWndClass";

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);
void                HandleTrayIconMessage(CTrayIcon* pTrayIcon, UINT uMsg);

// TrayIcon
CTrayIcon g_TrayIcon(
    L"TrackballWorks Helper", 
    HIDE_TRAYICON_WHEN_MAIN_WINDOW_IS_SHOWN ? false : true, 
    LoadIcon(NULL, MAKEINTRESOURCE(IDI_TBWHELPER)));

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    int argc = 0;
    const wchar_t** argv = (const wchar_t**)::CommandLineToArgvW(
        ::GetCommandLineW(),
        &argc);

    if (!processCommandLine(argv, argc))
        return 1;

    // Check for another instance of the app in this session
    if (App::instanceExists())
        return 1;


#ifdef _LEGACY_CODE
    // Load configuration from registry
    PrefLoadUIConfiguration();

    // Check is set in autorun. Set if not.
    PrefCheckAutorun();

    TbwKernelClientInit();

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
        return FALSE;

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_TBWHELPER));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }
    // laod preferences
    if (!PrefLoad())
    {
        TCHAR *filename = PrefGetBackupConfigFilename();
        if (!PrefLoadAsXMLAlternateLocation(filename))
        {
            if (!PrefCreateNew(NULL))
                PrefInitDummyPrefDB();
        }
    }
    PrefResolveDoGlobalsAll();

    TCHAR actProc[kMaxFilename] = { 0 };
    unsigned long i = AppSensSelectNewPref(actProc, kMaxFilename);
    if (i != -1) TbwKernelClientChangePref(i);

#else
    App* pApp = App::getInstance();
    if (!pApp)
    {
        return 1;
    }

    pApp->run();
    pApp->destroy();
#endif

    return 0;
}

#ifdef _LEGACY_CODE
//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_TBWHELPER));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_TBWHELPER);
    wcex.lpszClassName  = WINDOW_CLASS_NAME;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_TBWHELPER));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   g_hInstance = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(WINDOW_CLASS_NAME, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, 320, 240, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   g_hMainWnd = hWnd;
   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   g_TrayIcon.SetListener(HandleTrayIconMessage);
   g_TrayIcon.SetIcon(LoadIcon(g_hInstance, MAKEINTRESOURCE(IDI_TBWHELPER)));

   return TRUE;
}

void OnTimerProc(HWND hWnd)
{
    /*
    // Handle the deferred tray menu.
    if (openingTrayMenuCount > 0L)
    {
        openingTrayMenuCount--;

        if (openingTrayMenuCount == 0) OnTrayIconEvent(hWnd, TrayMenuShow(hWnd));
    }
    */
    // Check error state
    if (deviceCount != kDeviceCountError)
    {
        DWORD currentCount = TbwKernelClientGetDeviceCount();

        if (currentCount != deviceCount)
        {
            deviceCount = currentCount;

            if (currentCount == 0) HelperUpdateIcon(HELPERICON_OFF);
            else HelperUpdateIcon(HELPERICON_ON);
        }
    }

    // Check if there's any pending notification for pref file change.
    if (lastFSChange != 0L)
    {
        if ((GetTickCount() - lastFSChange) > loadPrefsTimeWindow)
        {
            PrefLoadUIConfiguration();
            AppSensInvalidateCurrentPref();

            // TrayMenuHideOrShow(gUIConf.hideTrayIcon == FALSE, hWnd, WM_TRAYNOTIFY);

            if (TbwKernelClientLockReaders())
            {
                if (!PrefLoad())
                {
                    TCHAR *filename = PrefGetBackupConfigFilename();

                    if (!PrefLoadAsXMLAlternateLocation(filename))
                    {
                        if (!PrefCreateNew(NULL))
                        {
                            PrefInitDummyPrefDB();
                        }
                    }
                }

                PrefResolveDoGlobalsAll();

                TbwKernelClientUnlockReaders();
                lastFSChange = 0L;
            }
        }
    }

    {
        static int purgeCacheTimer = 0;

        if (purgeCacheTimer == 0)
        {
            purgeCacheTimer = purgeCacheDivider;

            AppSensPurgePidCache();
        }
        else purgeCacheTimer--;

        if (waitingForShellHookMessage)
        {
            if (gPrefsCount)
            {
                unsigned long i;
                TCHAR actProc[kMaxFilename];

                i = AppSensSelectNewPref(actProc, kMaxFilename);

                if (i != -1) TbwKernelClientChangePref(i);
            }
        }
    }

}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
    case WM_CREATE:
        // Init the filesystem notification
        PrefSetChangeMessage(hWnd, WM_FSCHANGENOTIFY);
        if (SetTimer(hWnd, 0, kTimerPeriod, NULL) == 0) PostQuitMessage(0);
        HelperForceReloadPreferences();	// Force a PrefLoad in the Timer handler.

                                        // Register for console session attach/detach notifications
        WTSRegisterSessionNotification(hWnd, NOTIFY_FOR_THIS_SESSION);

        // Init the player
        PlayerStart(hWnd, WM_PLAYEREVENT, kMaxTbwControls);

        // Connect to the driver
        TbwKernelClientConnect(hWnd); // vannes20180911 this is where we connect to kernel device
        break;

    case WM_WTSSESSION_CHANGE:
        switch (wParam)
        {
        case WTS_SESSION_LOGON:
        case WTS_CONSOLE_CONNECT:
            TbwKernelClientConnect(hWnd);
            break;

        case WTS_SESSION_LOGOFF:
        case WTS_CONSOLE_DISCONNECT:
            TbwKernelClientDisconnect();
            break;

        }
        break;

    case WM_DEVICECHANGE:
        TbwKernelClientDeviceChangeProcessMessage(message, wParam, lParam);
        break;

    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            // Parse the menu selections:
            switch (wmId)
            {
            case IDM_ABOUT:
                DialogBox(g_hInstance, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
                break;
            case IDM_EXIT:
                DestroyWindow(hWnd);
                break;
            default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: Add any drawing code that uses hdc here...
            EndPaint(hWnd, &ps);
        }
        break;
    case WM_SIZE:
        if (wParam == SIZE_MINIMIZED)
        {
#if HIDE_TRAYICON_WHEN_MAIN_WINDOW_IS_SHOWN
            g_TrayIcon.SetVisible(true);
#endif
            ShowWindow(hWnd, SW_HIDE);
            return 0;
        }
        break;

    case WM_PLAYEREVENT:
        // Dispatch the message back to the player
        PlayerTimedMessageHandler(message, wParam, lParam);
        break;

    case WM_POSTEDACTION:
        // Send the message to the Plugins Dispatcher
        // HelperPluginExecutePostedAction(wParam, lParam);
        break;

    case WM_TIMER:
        OnTimerProc(hWnd);
        break;

    case WM_FSCHANGENOTIFY:
        // Mark the last FS activity notified.
        HelperForceReloadPreferences();
        break;

    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}
#endif

void HandleTrayIconMessage(CTrayIcon* pTrayIcon, UINT uMsg)
{
    switch (uMsg)
    {
    case WM_LBUTTONUP:
#if HIDE_TRAYICON_WHEN_MAIN_WINDOW_IS_SHOWN
        pTrayIcon->SetVisible(false);
#endif
        ShowWindow(g_hMainWnd, SW_SHOW);
        if (IsIconic(g_hMainWnd))
            ShowWindow(g_hMainWnd, SW_RESTORE);
        SetForegroundWindow(g_hMainWnd);
        break;

    case WM_RBUTTONUP:
    {
        POINT pt;
        if (GetCursorPos(&pt))
        {
            HMENU menu = CreatePopupMenu();
            AppendMenu(menu, MF_STRING, 3, L"Exit program");
            UINT cmd = TrackPopupMenu(
                menu, 
                TPM_RETURNCMD | TPM_RIGHTBUTTON, 
                pt.x, 
                pt.y, 
                0, 
                g_hMainWnd, 
                NULL);
            if (cmd == 3)
            {
                PostMessage(g_hMainWnd, WM_CLOSE, 0, 0);
            }
        }
    }
    break;
    }
}

void HelperUpdateIcon(UCHAR disabled)
{
    UNREFERENCED_PARAMETER(disabled);
}

void HelperForceReloadPreferences()
{
#ifdef _LEGACY_CODE
    lastFSChange = GetTickCount();
#endif
}
