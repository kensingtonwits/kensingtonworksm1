/*
    LegacySettingsMigrator implementation.
*/

#include "stdafx.h"

#include <fstream>
#include <sstream>
#include <locale>
#include <codecvt>
#include <algorithm>
#include <functional>

#include <boost/filesystem.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/log/trivial.hpp>

#include "utils.h"
#include "Migrate.h"
#include "Settings.h"
#include "TbwSettings.h"
#include "Modifiers.h"
#include "jsonutils.h"

// A few methods to read an attribute value from an XMLNODE and return it
// converted to a specific type. If conversion fails or if the attribute
// itself does not exist, returns the default value.

// Read numeric attribute value
template<typename T>
T readNumericAttrT(XMLNODE* pNode, const TBCHAR* attrName, T const& def)
{
    T val = def;
    auto attr = pNode->first_attribute(attrName);
    if (attr)
    {
        try
        {
            val = to<T>(attr->value());
        }
        catch (...)
        {
        }
    }
    return val;
}

// Read string attribute value
static tbstring readStringAttr(XMLNODE* pNode, const TBCHAR* attrName, tbstring const& def)
{
    tbstring ret = def;
    auto attr = pNode->first_attribute(attrName);
    if (attr)
    {
        try
        {
            ret = totbstring(attr->value());
        }
        catch (...)
        {
        }
    }
    return ret;
}

// Read boolean attribute value
static bool readBoolAttr(XMLNODE* pNode, const TBCHAR* attrName, bool def)
{
    bool ret = def;
    auto attr = pNode->first_attribute(attrName);
    if (attr)
    {
        try
        {
            ret = to<bool>(attr->value());
        }
        catch (...)
        {
        }
    }
    return ret;
}

static std::string getActionNameFromAttrs(int command, int args, unsigned modifiers, int repeat)
{
    // Legacy kTBWCommand* enums to Action string
    static struct _ {
        int id;
        const char* name;
    } TBWCommandToActionName[] = {
        { kCommandUseGlobal, "useGlobal" },
        { kCommandNone, "none" },
        { kCommandSnippet, "snippet" },
        { kCommandMacro, "sequence" },
        { kCommandKeystroke, "keystroke" },
        { kCommandOpenItem, "open"  },
        { kCommandClick, "singleClick" },
        { kCommandRightClick, "singleClick" },
        { kCommandDoubleClick, "multiClick" },
        { kCommandRightDoubleClick, "multiClick" },
        { kCommandTripleClick, "multiClick" },
        { kCommandShowDesktop , "showDesktop" },
        { kCommandMinimizeWindow , "minimizeWindow" },
        { kCommandMaximizeWindow, "maximizeWindow" },
        { kCommandMinimizeAll, "minimizeAll" },
        { kCommandCloseWindow, "closeWindow" },
        { kCommandLockWorkstation, "lockWorkstation",  },
        { kCommandClickLock, "drag" },
    };

    // do special handling first
    if (command == kCommandClick) {
        if (repeat > 1)
            return "multiClick";
        return "singleClick";
    }
    if (command == kCommandKeystroke) {
        if (modifiers == WIN_K_WIN && args == 'D')
            return "showDesktop";
        return "keystroke";
    }
    for (auto& entry : TBWCommandToActionName) {
        if (entry.id == command)
            return entry.name;
    }
    throw std::runtime_error("Unknown command type");
}

// Given button name returns the equivalent bit mask. Button names
// are of the form 'Button1', 'Button2', ..., 'ButtonN'.
// Handles ButtonChord1 & ButtonChord2 as Button1 + Button2 = 0x03
// & ButtonChrod2 as Button3 + Button4 = 0x0C.
static uint32_t buttonNameToMask(const TBCHAR* buttonName)
{
    tbstring name(tolower(tbstring(buttonName)));
    try
    {
        // 'Button1....31'
        if (name.find(TEXT("buttonchord")) == 0)
        {
            auto chordNo = name.substr(tbstring(TEXT("buttonchord")).length());
            if (chordNo == TEXT("1"))
            {
                return 0x01 | 0x02;    // buttons 0x01 | 0x02
            }
            else if (chordNo == TEXT("2"))
            {
                return 0x04 | 0x08;
            }
        }
        else if (name.find(TEXT("button")) == 0)
        {
            auto buttonNo = name.substr(tbstring(TEXT("button")).length());
            auto button = to<uint32_t>(buttonNo);
            return 0x01 << (button - 1);
        }
    }
    catch (...)
    {
    }

    return 0;
}

SettingsMigrator::SettingsMigrator()
{
    loadActionMacros();
}

bool SettingsMigrator::migrate()
{
    tbistream fin(
        Settings::getLegacyFileFullPath().c_str(),
        std::ios::binary | std::ios::ate);

    // apply BOM-sensitive UTF-16 facet
    fin.imbue(std::locale(fin.getloc(),
        new std::codecvt_utf16<wchar_t, 0x10ffff, std::consume_header>));
    // fin.seekg(0, std::ios::end);
    auto size = fin.tellg();
    std::vector<TBCHAR> tmp(size);
    TBCHAR* buf = &tmp[0];
    //auto buf = std::make_unique<TBCHAR[]>(size);

    fin.seekg(0, std::ios::beg);        // seek back to beginning of the file
    fin.read(buf, size);          // read the entire file into the vector

    rx::xml_document<TBCHAR> doc;
    doc.parse<0>(buf);

    XMLNODE* root = doc.first_node(kTBWXMLSettings);
    if (!root)
        throw BadSettingsFile();

    Settings settings;

    // iterate all Application nodes under root
    XMLNODE* appNode = root->first_node(kTBWXMLApp);
    for (; appNode != nullptr; appNode = appNode->next_sibling())
    {
        try
        {
            auto appName = readStringAttr(appNode, kTBWXMLAppExe, TEXT("*"));
            BOOST_LOG_TRIVIAL(info) << "Loading from legacy application node: "
                << appName.c_str();
            migrateApplicationConfig(settings, appNode);
            //_apps.insert(std::make_pair(toUTF8(appName), ApplicationConfig(this, appNode)));
        }
        catch (...)
        {
            BOOST_LOG_TRIVIAL(error) << "Error loading from legacy application node";
        }
    }

    settings.save();
    return true;
}

void SettingsMigrator::loadActionMacros()
{
    boost::filesystem::path localFolder("./tbwactions.json");
        
    std::ifstream fin(toUTF8(localFolder.native()));
    json jdefmacros;
    fin >> jdefmacros;
	int nKeyStrokeCommands = 0;
    for (json::const_iterator it = jdefmacros.begin(); it != jdefmacros.end(); ++it)
    {
        json const& jCommand = it.value();
        try
        {
            std::string command;
            jCommand.at("command").get_to(command);

            if (command == "keystroke")
            {
				BOOST_LOG_TRIVIAL(debug) << it.key() << " : " << it.value();
				uint32_t keyCode = getFromStrValue<unsigned>(jCommand, "keyCode");

                KeystrokeCommand kc;
                kc.macro = it.key();
                std::string modifiers;
                jCommand.at("modifiers").get_to(modifiers);
                kc.modifiers = modifiersFromText(modifiers.c_str());
                if (jCommand.find("repeat") != jCommand.end())
                    jCommand.at("repeat").get_to(kc.repeat);
                kc.json = jCommand.dump();
                _keystrokeCommands.insert(std::make_pair(keyCode, kc));
				nKeyStrokeCommands++;
            }
        }
        catch (const std::exception& e)
        {
			BOOST_LOG_TRIVIAL(debug) << "Exception: " << e.what() << "\n";
        }
    }

	BOOST_LOG_TRIVIAL(debug) << "Total keystroke commands: " << nKeyStrokeCommands << ", multimap size: " << _keystrokeCommands.size();
}

void SettingsMigrator::migrateApplicationConfig(Settings& settings, XMLNODE* pNode)
{
    auto fileName = toUTF8(readStringAttr(pNode, kTBWXMLAppExe, TEXT("*")));
    auto name = toUTF8(readStringAttr(pNode, kTBWXMLAppName, TEXT("")));

	// TODO: Implement deviceId
    //settings.createApp(0, name.c_str(), fileName.c_str());

    XMLNODE* devNode = pNode->first_node(kTBWXMLDev);
    for (; devNode != nullptr; devNode = devNode->next_sibling())
    {
        try
        {
            migrateDeviceConfig(settings, name.c_str(), fileName.c_str(), devNode);
        }
        catch (...)
        {

        }
    }
}

void SettingsMigrator::migrateDeviceConfig(Settings& settings, const char* appName, const char* appFilePath, XMLNODE* pNode)
{
    const int TBW_1X_TYPE_MAX = 9;    // TBW 1.x device type enum values max
    unsigned id = readNumericAttrT(pNode, kTBWXMLDevId, 0);
    if (id == 0)
        throw std::runtime_error("invalid device id");

    // TbwSettings 1.0 stores device type enum values as the device id. As this
    // enum's max value is < 9 whereas Kensington USB product IDs are >= 1020, 
    // we can safely convert legacy device type enum values to USB product ID.
    if (id < TBW_1X_TYPE_MAX)
        id = LegacyDeviceTypeToProductID(id);

	settings.createDefaultDeviceConfigIfDoesntExist(id);

	// create the app, just in case it doesn't exist
	std::vector<std::string> apps = settings.getApps(id);
	bool appExists = false;
	for (auto& appId : apps) {
		if (appId == tolower(std::string(appFilePath))) {
			appExists = true;
			break;
		}
	}
	if (!appExists) {
		settings.createApp(id, appName, tolower(std::string(appFilePath)).c_str());
	}
    DeviceConfig& dc = settings.getDeviceConfig(appFilePath, id, false);

    // pointerSpeed and pointerAcceleration should default to old scale
    // default values -- that is 10 & 0 respectively, so that when we translate
    // it to new scale it falls to the correct value in 1..100 scale.
    uint32_t pointerSpeed = readNumericAttrT(pNode, kTBWXMLDevSpeed, 10);
    // pointerSpeed is expressed in 1..20 scale. Convert this to 1..100 scale
    pointerSpeed *= 5;
    pointerSpeed = std::max(std::min(pointerSpeed, 100u), 1u);
    dc.pointerSpeed(pointerSpeed);

    uint32_t pointerAcceleration = readNumericAttrT(pNode, kTBWXMLDevAcceleration, 0);
    // Acceleration is expressed in scale of 1..4. Convert this to 1..100 scale.
    pointerAcceleration *= 25;
    pointerAcceleration = std::max(std::min(pointerAcceleration, 100u), 1u);
    dc.pointerAccelerationRate(pointerAcceleration);

    //uint32_t scrollLines = readNumericAttrT(pNode, kTBWXMLDevScrollSpeed, 0u);
    //dc.scrollSpeed(scrollLines);

    bool scrollInvert = readBoolAttr(pNode, kTBWXMLDevScrollInvert, false);
    dc.invertScroll(scrollInvert);

    uint32_t snapModifiers = readNumericAttrT(pNode, kTBWXMLDevSnapModifiers, 0u);
    dc.lockedAxisModifiers(modifiersToText(snapModifiers).c_str());

    uint32_t slowModifiers = readNumericAttrT(pNode, kTBWXMLDevSlowModifiers, 0u);
    dc.slowPointerModifiers(modifiersToText(slowModifiers).c_str());
    //unsigned slowDivider = readNumericAttrT(pNode, kTBWXMLDevSlowDivider, 0);

    bool inertialScroll = readBoolAttr(pNode, kTBWXMLDevIScroll, false);
    dc.inertialScroll(inertialScroll);

    //unsigned inertialScrollTimer = readNumericAttrT(pNode, kTBWXMLDevIScrollTimer, 0);
    //unsigned inertialScrollNullSteps = readNumericAttrT(pNode, kTBWXMLDevIScrollSteps, 0);
    //unsigned inertialScrollFactor = readNumericAttrT(pNode, kTBWXMLDevIScrollFactor, 0);


    // load Decrement{X|Y|Z} & Increment{X|Y|Z} actions. 
    // These seem like legacy settings, but we load them anyway
    TBCHAR const* controlNames[] = {
        kTBWXMLCtrlDecX,
        kTBWXMLCtrlIncX,
        kTBWXMLCtrlDecY,
        kTBWXMLCtrlIncY,
        kTBWXMLCtrlDecZ,
        kTBWXMLCtrlIncZ,
        // HACK! Need to figure out a better way to handle chord definitions
        kTBWXMLCtrlButtonChord1,
        kTBWXMLCtrlButtonChord2,
        kTBWXMLCtrlButtonSnippets,
    };

    for (auto& controlName : controlNames) {
        migrateAction(dc, pNode, controlName);
    }

    // load button action settings
    for (int buttonNo = 1; buttonNo < kMaxTbwControls; buttonNo++)
    {
        tbstringstream ss;
        ss << TEXT("Button") << buttonNo;
        migrateAction(dc, pNode, ss.str().c_str());
    }
}

static bool isOpenPathURL(std::string const& path)
{
	std::string lwrPath = tolower(path);
	return lwrPath.find("http://") == 0
		|| lwrPath.find("ftp://") == 0
		|| lwrPath.find("www.") == 0;
}

static bool isOpenPathFolder(std::string const& path)
{
	std::string lwrPath = path;
	if (lwrPath.find(' ') != std::string::npos) {
		lwrPath.insert(0, "\"");
		lwrPath.append("\"");
	}
	if (::PathFileExistsA(lwrPath.c_str()))
	{
		DWORD dwAttr = ::GetFileAttributesA(lwrPath.c_str());
		return dwAttr & FILE_ATTRIBUTE_DIRECTORY;
	}
	return false;
}

static bool isOpenPathFile(std::string const& path)
{
	std::string lwrPath = path;
	if (lwrPath.find(' ') != std::string::npos) {
		lwrPath.insert(0, "\"");
		lwrPath.append("\"");
	}
	if (::PathFileExistsA(lwrPath.c_str()))
	{
		DWORD dwAttr = ::GetFileAttributesA(lwrPath.c_str());
		return !(dwAttr & FILE_ATTRIBUTE_DIRECTORY);
	}
	return false;
}

static bool isOpenPathProgram(std::string const& path)
{
	std::string lwrPath = tolower(path);
	return lwrPath.rfind(".exe") == (lwrPath.length() - 4);
}

void SettingsMigrator::migrateAction(DeviceConfig& dc, XMLNODE* pDevNode, const TBCHAR* pszButtonName)
{
    XMLNODE* buttonNode = pDevNode->first_node(pszButtonName);
    if (nullptr == buttonNode)
        return;

    auto actionNode = buttonNode->first_node(kTBWXMLAct);
    if (nullptr == actionNode)
        return;

    auto buttonMask = buttonNameToMask(pszButtonName);
    if (buttonMask == 0)
        return;

    uint32_t command = (uint32_t)readNumericAttrT(actionNode, kTBWXMLActCommand, 0u);
    int argument = (int)readNumericAttrT(actionNode, kTBWXMLActKeyCode, 0u);
    uint32_t modifiers = (uint32_t)readNumericAttrT(actionNode, kTBWXMLActModifiers, 0u);
    uint32_t repeat = (uint32_t)readNumericAttrT(actionNode, kTBWXMLActRepeat, 0u);

    std::string actionName = getActionNameFromAttrs(command, argument, modifiers, repeat);

	struct SpecialCommands {
		int argument;
		uint32_t modifiers;
		std::string macro;
	} specialCommands[] = {
		{ 70, 64, "system_showSystemSearch" },
		{ -77, 0, "media_playPause" },
		{ -78, 0, "media_stop" },
		{ -79, 0, "media_prevTrack" },
		{ -80, 0, "media_nextTrack" },
		{ -81, 0, "media_volumeUp" },
		{ -82, 0, "media_volumeDown" },
		{ -83, 0, "media_mute" },
		{ -87, 0, "browser_stop" },
		{ -88, 0, "browser_refresh" },
		{ -89, 0, "browser_forward" },
		{ -90, 0, "browser_back" }
	};
    if (actionName == "keystroke")
    {
		auto found = false;
		// first search through the special table
		for (auto& element : specialCommands)
		{
			if (element.argument == argument && element.modifiers == modifiers)
			{
				json macro = { { "command", element.macro } };
				dc.addCommand(buttonMask, macro);
				found = true;
				break;
			}
		}

		if (!found)
		{
			std::pair<KEYSTROKECOMMANDS::iterator, KEYSTROKECOMMANDS::iterator> result =
				_keystrokeCommands.equal_range(argument);

			for (auto it = result.first; it != result.second; it++)
			{
				KeystrokeCommand const& kc = it->second;
				if (kc.modifiers == modifiers)
				{
					// matching command macro found!
					json macro = { { "command", kc.macro } };
					dc.addCommand(buttonMask, macro);
					found = true;
					break;
				}
			}
		}

        if (!found)
        {
			json macro = { { "command", "noAction" } };
            dc.addCommand(buttonMask, macro);
        }
    }
	else if (actionName == "showDesktop")
	{
		json macro = { { "command", "desktop_showDesktop" } };
		dc.addCommand(buttonMask, macro);
	}
	else if (actionName == "lockWorkstation")
	{
		json macro = {{ "command", "system_lockScreen" }};
		dc.addCommand(buttonMask, macro);
	}
	else if (actionName == "maximizeWindow")
	{
		json macro = { { "command", "desktop_maximizeWindow" } };
		dc.addCommand(buttonMask, macro);
	}
	else if (actionName == "minimizeWindow")
	{
		json macro = { { "command", "desktop_minimizeWindow" } };
		dc.addCommand(buttonMask, macro);
	}
	else if (actionName == "closeWindow")
	{
		json macro = { { "command", "desktop_closeWindow" } };
		dc.addCommand(buttonMask, macro);
	}
	else if (actionName == "minimizeAll")
	{
		json macro = { { "command", "desktop_minimizeAll" } };
		dc.addCommand(buttonMask, macro);
	}
    else if (actionName == "singleClick")
    {
        // argument=<mouse button #>, starting from 1
        // Use this array to translate that into macro name
        const char* macroName[] = {
            "mouse_leftClick",
            "mouse_rightClick",
            "mouse_middleClick",
            "mouse_button4Click",
            "mouse_button5Click"
        };

        json macro = {
            { "command", macroName[argument-1] },
            { "params", {
                { "modifiers", modifiersToText(modifiers) },
                }
            }
        };
        dc.addCommand(buttonMask, macro);
    }
    else if (actionName == "multiClick")
    {
        // argument=<mouse button #>, starting from 1
        // Use this array to translate that into macro name
        const char* macroName[] = {
            "mouse_leftDoubleClick",
            "mouse_rightDoubleClick",
            "mouse_middleDoubleClick",
        };

        std::string macroCommand;
        if (repeat == 2)
            macroCommand = macroName[argument - 1];
        else if (repeat == 3)
            macroCommand = "mouse_tripleClick";

        json macro = {
            { "command", macroCommand },
            { "params", {
                { "modifiers", modifiersToText(modifiers) },
                }
            }
        };
        dc.addCommand(buttonMask, macro);
    }
	else if (actionName == "drag")
	{
		const char* macroName[] = {
			"mouse_leftDrag",
			"mouse_rightDrag",
			"mouse_middleDrag",
		};
		if (argument >= 1 && argument <= 3)
		{
			std::string macroCommand = macroName[argument - 1];
			json macro = {
				{ "command", macroCommand },
				{ "params", {
					{ "modifiers", modifiersToText(modifiers) },
					}
				}
			};
			dc.addCommand(buttonMask, macro);
		}
	}
    else if (actionName == "open")
    {
        auto name = toUTF8(readStringAttr(actionNode, kTBWXMLActName, TEXT("")));
        auto path = toUTF8(readStringAttr(actionNode, kTBWXMLActPath, TEXT("")));
		std::string command = "noAction";
		if (isOpenPathURL(path))
		{
			command = "launching_openURL";
		}
		else if (isOpenPathProgram(path))
		{
			command = "launching_openApplication";
		}
		else if (isOpenPathFolder(path.length() ? path : name))
		{
			command = "launching_openFolder";
			path = name;
			name = "";
		}
		else if (isOpenPathFile(path.length() ? path : name))
		{
			command = "launching_openFile";
			path = name;
			name = "";
		}
		if (command != "noAction")
		{
			json macro = {
				{ "command", command },
				{ "params", {
					{ "label", name },
					{ "path", path }
					}
				}
			};
			dc.addCommand(buttonMask, macro);
		}
		else
		{
			json macro = {
				{ "command", command }
			};
			dc.addCommand(buttonMask, macro);
		}
    }
    else if (actionName == "snippet")
    {
        auto pSnippetNode = actionNode->first_node(kTBWXMLSnippet);
        if (pSnippetNode)
        {
			if (argument >= 1)
			{
				auto title = toUTF8(readStringAttr(pSnippetNode, kTBWXMLTitle, TEXT("")));
				auto content = toUTF8(readStringAttr(pSnippetNode, kTBWXMLString, TEXT("")));
				json macro = {
					{ "command", "snippet" },
					{ "params", {
						{ "label", title },
						{ "content", content },
						{"index", argument - 1 }
						}
					}
				};
				dc.addCommand(buttonMask, macro);
			}
			else
			{
				// snippet menu
				json macro = { { "command", "snippetMenu" } };
				dc.addCommand(buttonMask, macro);
			}
        }
    }
    else if (actionName == "none")
    {
        json disable = {
            { "command", "noAction" }
        };
        dc.addCommand(buttonMask, disable);
    }
}
