#include "stdafx.h"
#include "launchgui.h"
#include <algorithm>

/*
 * Computes optimum GUI scaling factor based on primary display resolution.
 */
static float getScalingFactor()
{
	// get primary display work area
	RECT rcWA = { 0 };
	SystemParametersInfo(SPI_GETWORKAREA, 0, &rcWA, 0);
	if ((rcWA.right - rcWA.left) < 1280 || (rcWA.bottom - rcWA.top) < 800)
	{
		float xScaleFactor = ((float)(rcWA.right - rcWA.left) / 1300);
		float yScaleFactor = ((float)(rcWA.bottom - rcWA.top) / 800);

		return std::min(xScaleFactor, yScaleFactor);
	}
	return 0.0;
}

/*
 * Starts the GUI applying a scaling factor as per the primary display resolution.
 */
void launchGui()
{
	// Read the installation location
	WCHAR szGuiFullname[1024] = { 0 };
	::GetModuleFileNameW(NULL, szGuiFullname, sizeof(szGuiFullname) / sizeof(szGuiFullname[0]));
	WCHAR* pc = ::wcsrchr(szGuiFullname, L'\\');
	*(pc + 1) = L'\0';
	::wcscat_s(szGuiFullname, L"KensingtonWorks2.exe");

	WCHAR szCmd[1024] = { 0 };
	/*
	auto scaleFactor = getScalingFactor();
	if (scaleFactor && scaleFactor < 1.0)
	{
		swprintf_s(szCmd, L"\"%s\" --force-device-scale-factor=%f", szGuiFullname, scaleFactor);
	}
	else
	*/
	{
		swprintf_s(szCmd, L"\"%s\"", szGuiFullname);
	}
	//MessageBox(NULL, szCmd, L"Gui command line", MB_OK);

	STARTUPINFO si = { 0 };
	PROCESS_INFORMATION pi = { 0 };
	if (::CreateProcessW(
		NULL,
		szCmd,
		NULL,
		NULL,
		FALSE,
		DETACHED_PROCESS,
		NULL,
		NULL,
		&si,
		&pi)) {
		::CloseHandle(pi.hThread);
		::CloseHandle(pi.hProcess);
	}
}
