// !$*UTF8*$!
{
	archiveVersion = 1;
	classes = {
	};
	objectVersion = 46;
	objects = {

/* Begin PBXBuildFile section */
		472B2D37233B761F0068458A /* Cocoa.framework in Frameworks */ = {isa = PBXBuildFile; fileRef = 472B2D36233B761F0068458A /* Cocoa.framework */; };
		47A320FE233B6B01005BFAE9 /* KensingtonWorksAgent.icns in Resources */ = {isa = PBXBuildFile; fileRef = 47A320FD233B6B01005BFAE9 /* KensingtonWorksAgent.icns */; settings = {ASSET_TAGS = (KensingtonWorks, ); }; };
		47FFE57B233B0ECB008BD7F2 /* AppDelegate.cpp in Sources */ = {isa = PBXBuildFile; fileRef = 47FFE57A233B0ECB008BD7F2 /* AppDelegate.cpp */; };
		47FFE57E233B0ECB008BD7F2 /* main.cpp in Sources */ = {isa = PBXBuildFile; fileRef = 47FFE57D233B0ECB008BD7F2 /* main.cpp */; };
		47FFE580233B0ECB008BD7F2 /* Assets.xcassets in Resources */ = {isa = PBXBuildFile; fileRef = 47FFE57F233B0ECB008BD7F2 /* Assets.xcassets */; };
		47FFE583233B0ECB008BD7F2 /* MainMenu.xib in Resources */ = {isa = PBXBuildFile; fileRef = 47FFE581233B0ECB008BD7F2 /* MainMenu.xib */; };
/* End PBXBuildFile section */

/* Begin PBXFileReference section */
		472B2D36233B761F0068458A /* Cocoa.framework */ = {isa = PBXFileReference; lastKnownFileType = wrapper.framework; name = Cocoa.framework; path = System/Library/Frameworks/Cocoa.framework; sourceTree = SDKROOT; };
		47A320FD233B6B01005BFAE9 /* KensingtonWorksAgent.icns */ = {isa = PBXFileReference; lastKnownFileType = image.icns; path = KensingtonWorksAgent.icns; sourceTree = "<group>"; };
		47FFE576233B0ECA008BD7F2 /* KensingtonWorksAgent.app */ = {isa = PBXFileReference; explicitFileType = wrapper.application; includeInIndex = 0; path = KensingtonWorksAgent.app; sourceTree = BUILT_PRODUCTS_DIR; };
		47FFE579233B0ECB008BD7F2 /* AppDelegate.h */ = {isa = PBXFileReference; explicitFileType = sourcecode.cpp.h; path = AppDelegate.h; sourceTree = "<group>"; };
		47FFE57A233B0ECB008BD7F2 /* AppDelegate.cpp */ = {isa = PBXFileReference; explicitFileType = sourcecode.cpp.objcpp; path = AppDelegate.cpp; sourceTree = "<group>"; };
		47FFE57D233B0ECB008BD7F2 /* main.cpp */ = {isa = PBXFileReference; explicitFileType = sourcecode.cpp.objcpp; path = main.cpp; sourceTree = "<group>"; };
		47FFE57F233B0ECB008BD7F2 /* Assets.xcassets */ = {isa = PBXFileReference; lastKnownFileType = folder.assetcatalog; path = Assets.xcassets; sourceTree = "<group>"; };
		47FFE582233B0ECB008BD7F2 /* Base */ = {isa = PBXFileReference; lastKnownFileType = file.xib; name = Base; path = Base.lproj/MainMenu.xib; sourceTree = "<group>"; };
		47FFE584233B0ECB008BD7F2 /* Info.plist */ = {isa = PBXFileReference; lastKnownFileType = text.plist.xml; path = Info.plist; sourceTree = "<group>"; };
/* End PBXFileReference section */

/* Begin PBXFrameworksBuildPhase section */
		47FFE573233B0ECA008BD7F2 /* Frameworks */ = {
			isa = PBXFrameworksBuildPhase;
			buildActionMask = 2147483647;
			files = (
				472B2D37233B761F0068458A /* Cocoa.framework in Frameworks */,
			);
			runOnlyForDeploymentPostprocessing = 0;
		};
/* End PBXFrameworksBuildPhase section */

/* Begin PBXGroup section */
		472B2D35233B761F0068458A /* Frameworks */ = {
			isa = PBXGroup;
			children = (
				472B2D36233B761F0068458A /* Cocoa.framework */,
			);
			name = Frameworks;
			sourceTree = "<group>";
		};
		47FFE56D233B0ECA008BD7F2 = {
			isa = PBXGroup;
			children = (
				47FFE578233B0ECA008BD7F2 /* kgtwhelper */,
				47FFE577233B0ECA008BD7F2 /* Products */,
				472B2D35233B761F0068458A /* Frameworks */,
			);
			sourceTree = "<group>";
		};
		47FFE577233B0ECA008BD7F2 /* Products */ = {
			isa = PBXGroup;
			children = (
				47FFE576233B0ECA008BD7F2 /* KensingtonWorksAgent.app */,
			);
			name = Products;
			sourceTree = "<group>";
		};
		47FFE578233B0ECA008BD7F2 /* kgtwhelper */ = {
			isa = PBXGroup;
			children = (
				47FFE579233B0ECB008BD7F2 /* AppDelegate.h */,
				47FFE57A233B0ECB008BD7F2 /* AppDelegate.cpp */,
				47A320FD233B6B01005BFAE9 /* KensingtonWorksAgent.icns */,
				47FFE57F233B0ECB008BD7F2 /* Assets.xcassets */,
				47FFE581233B0ECB008BD7F2 /* MainMenu.xib */,
				47FFE584233B0ECB008BD7F2 /* Info.plist */,
				47FFE57C233B0ECB008BD7F2 /* Supporting Files */,
			);
			path = kgtwhelper;
			sourceTree = "<group>";
		};
		47FFE57C233B0ECB008BD7F2 /* Supporting Files */ = {
			isa = PBXGroup;
			children = (
				47FFE57D233B0ECB008BD7F2 /* main.cpp */,
			);
			name = "Supporting Files";
			sourceTree = "<group>";
		};
/* End PBXGroup section */

/* Begin PBXNativeTarget section */
		47FFE575233B0ECA008BD7F2 /* KensingtonWorksAgent */ = {
			isa = PBXNativeTarget;
			buildConfigurationList = 47FFE587233B0ECB008BD7F2 /* Build configuration list for PBXNativeTarget "KensingtonWorksAgent" */;
			buildPhases = (
				47FFE572233B0ECA008BD7F2 /* Sources */,
				47FFE573233B0ECA008BD7F2 /* Frameworks */,
				47FFE574233B0ECA008BD7F2 /* Resources */,
			);
			buildRules = (
			);
			dependencies = (
			);
			name = KensingtonWorksAgent;
			productName = kgtwhelper;
			productReference = 47FFE576233B0ECA008BD7F2 /* KensingtonWorksAgent.app */;
			productType = "com.apple.product-type.application";
		};
/* End PBXNativeTarget section */

/* Begin PBXProject section */
		47FFE56E233B0ECA008BD7F2 /* Project object */ = {
			isa = PBXProject;
			attributes = {
				KnownAssetTags = (
					KensingtonWorks,
				);
				LastUpgradeCheck = 0820;
				ORGANIZATIONNAME = smallpearl;
				TargetAttributes = {
					47FFE575233B0ECA008BD7F2 = {
						CreatedOnToolsVersion = 8.2.1;
						DevelopmentTeam = 38YBH9NQ69;
						ProvisioningStyle = Automatic;
					};
				};
			};
			buildConfigurationList = 47FFE571233B0ECA008BD7F2 /* Build configuration list for PBXProject "KensingtonWorksAgent" */;
			compatibilityVersion = "Xcode 3.2";
			developmentRegion = English;
			hasScannedForEncodings = 0;
			knownRegions = (
				en,
				Base,
			);
			mainGroup = 47FFE56D233B0ECA008BD7F2;
			productRefGroup = 47FFE577233B0ECA008BD7F2 /* Products */;
			projectDirPath = "";
			projectRoot = "";
			targets = (
				47FFE575233B0ECA008BD7F2 /* KensingtonWorksAgent */,
			);
		};
/* End PBXProject section */

/* Begin PBXResourcesBuildPhase section */
		47FFE574233B0ECA008BD7F2 /* Resources */ = {
			isa = PBXResourcesBuildPhase;
			buildActionMask = 2147483647;
			files = (
				47FFE580233B0ECB008BD7F2 /* Assets.xcassets in Resources */,
				47FFE583233B0ECB008BD7F2 /* MainMenu.xib in Resources */,
				47A320FE233B6B01005BFAE9 /* KensingtonWorksAgent.icns in Resources */,
			);
			runOnlyForDeploymentPostprocessing = 0;
		};
/* End PBXResourcesBuildPhase section */

/* Begin PBXSourcesBuildPhase section */
		47FFE572233B0ECA008BD7F2 /* Sources */ = {
			isa = PBXSourcesBuildPhase;
			buildActionMask = 2147483647;
			files = (
				47FFE57E233B0ECB008BD7F2 /* main.cpp in Sources */,
				47FFE57B233B0ECB008BD7F2 /* AppDelegate.cpp in Sources */,
			);
			runOnlyForDeploymentPostprocessing = 0;
		};
/* End PBXSourcesBuildPhase section */

/* Begin PBXVariantGroup section */
		47FFE581233B0ECB008BD7F2 /* MainMenu.xib */ = {
			isa = PBXVariantGroup;
			children = (
				47FFE582233B0ECB008BD7F2 /* Base */,
			);
			name = MainMenu.xib;
			sourceTree = "<group>";
		};
/* End PBXVariantGroup section */

/* Begin XCBuildConfiguration section */
		47FFE585233B0ECB008BD7F2 /* Debug */ = {
			isa = XCBuildConfiguration;
			buildSettings = {
				ALWAYS_SEARCH_USER_PATHS = NO;
				CLANG_ANALYZER_NONNULL = YES;
				CLANG_CXX_LANGUAGE_STANDARD = "gnu++0x";
				CLANG_CXX_LIBRARY = "libc++";
				CLANG_ENABLE_MODULES = YES;
				CLANG_ENABLE_OBJC_ARC = YES;
				CLANG_WARN_BOOL_CONVERSION = YES;
				CLANG_WARN_CONSTANT_CONVERSION = YES;
				CLANG_WARN_DIRECT_OBJC_ISA_USAGE = YES_ERROR;
				CLANG_WARN_DOCUMENTATION_COMMENTS = YES;
				CLANG_WARN_EMPTY_BODY = YES;
				CLANG_WARN_ENUM_CONVERSION = YES;
				CLANG_WARN_INFINITE_RECURSION = YES;
				CLANG_WARN_INT_CONVERSION = YES;
				CLANG_WARN_OBJC_ROOT_CLASS = YES_ERROR;
				CLANG_WARN_SUSPICIOUS_MOVE = YES;
				CLANG_WARN_UNREACHABLE_CODE = YES;
				CLANG_WARN__DUPLICATE_METHOD_MATCH = YES;
				CODE_SIGN_IDENTITY = "-";
				COPY_PHASE_STRIP = NO;
				DEBUG_INFORMATION_FORMAT = dwarf;
				ENABLE_STRICT_OBJC_MSGSEND = YES;
				ENABLE_TESTABILITY = YES;
				GCC_C_LANGUAGE_STANDARD = gnu99;
				GCC_DYNAMIC_NO_PIC = NO;
				GCC_NO_COMMON_BLOCKS = YES;
				GCC_OPTIMIZATION_LEVEL = 0;
				GCC_PREPROCESSOR_DEFINITIONS = (
					"DEBUG=1",
					"$(inherited)",
				);
				GCC_WARN_64_TO_32_BIT_CONVERSION = YES;
				GCC_WARN_ABOUT_RETURN_TYPE = YES_ERROR;
				GCC_WARN_UNDECLARED_SELECTOR = YES;
				GCC_WARN_UNINITIALIZED_AUTOS = YES_AGGRESSIVE;
				GCC_WARN_UNUSED_FUNCTION = YES;
				GCC_WARN_UNUSED_VARIABLE = YES;
				MACOSX_DEPLOYMENT_TARGET = 10.13;
				MTL_ENABLE_DEBUG_INFO = YES;
				ONLY_ACTIVE_ARCH = YES;
				SDKROOT = macosx;
			};
			name = Debug;
		};
		47FFE586233B0ECB008BD7F2 /* Release */ = {
			isa = XCBuildConfiguration;
			buildSettings = {
				ALWAYS_SEARCH_USER_PATHS = NO;
				CLANG_ANALYZER_NONNULL = YES;
				CLANG_CXX_LANGUAGE_STANDARD = "gnu++0x";
				CLANG_CXX_LIBRARY = "libc++";
				CLANG_ENABLE_MODULES = YES;
				CLANG_ENABLE_OBJC_ARC = YES;
				CLANG_WARN_BOOL_CONVERSION = YES;
				CLANG_WARN_CONSTANT_CONVERSION = YES;
				CLANG_WARN_DIRECT_OBJC_ISA_USAGE = YES_ERROR;
				CLANG_WARN_DOCUMENTATION_COMMENTS = YES;
				CLANG_WARN_EMPTY_BODY = YES;
				CLANG_WARN_ENUM_CONVERSION = YES;
				CLANG_WARN_INFINITE_RECURSION = YES;
				CLANG_WARN_INT_CONVERSION = YES;
				CLANG_WARN_OBJC_ROOT_CLASS = YES_ERROR;
				CLANG_WARN_SUSPICIOUS_MOVE = YES;
				CLANG_WARN_UNREACHABLE_CODE = YES;
				CLANG_WARN__DUPLICATE_METHOD_MATCH = YES;
				CODE_SIGN_IDENTITY = "-";
				COPY_PHASE_STRIP = NO;
				DEBUG_INFORMATION_FORMAT = "dwarf-with-dsym";
				ENABLE_NS_ASSERTIONS = NO;
				ENABLE_STRICT_OBJC_MSGSEND = YES;
				GCC_C_LANGUAGE_STANDARD = gnu99;
				GCC_NO_COMMON_BLOCKS = YES;
				GCC_WARN_64_TO_32_BIT_CONVERSION = YES;
				GCC_WARN_ABOUT_RETURN_TYPE = YES_ERROR;
				GCC_WARN_UNDECLARED_SELECTOR = YES;
				GCC_WARN_UNINITIALIZED_AUTOS = YES_AGGRESSIVE;
				GCC_WARN_UNUSED_FUNCTION = YES;
				GCC_WARN_UNUSED_VARIABLE = YES;
				MACOSX_DEPLOYMENT_TARGET = 10.13;
				MTL_ENABLE_DEBUG_INFO = NO;
				SDKROOT = macosx;
			};
			name = Release;
		};
		47FFE588233B0ECB008BD7F2 /* Debug */ = {
			isa = XCBuildConfiguration;
			buildSettings = {
				ASSETCATALOG_COMPILER_APPICON_NAME = AppIcon;
				CODE_SIGN_IDENTITY = "Mac Developer";
				COMBINE_HIDPI_IMAGES = YES;
				DEVELOPMENT_TEAM = 38YBH9NQ69;
				INFOPLIST_FILE = kgtwhelper/Info.plist;
				LD_RUNPATH_SEARCH_PATHS = "$(inherited) @executable_path/../Frameworks";
				PRODUCT_BUNDLE_IDENTIFIER = com.smallpearl.kensingtonworks2.helper;
				PRODUCT_NAME = "$(TARGET_NAME)";
			};
			name = Debug;
		};
		47FFE589233B0ECB008BD7F2 /* Release */ = {
			isa = XCBuildConfiguration;
			buildSettings = {
				ASSETCATALOG_COMPILER_APPICON_NAME = AppIcon;
				CODE_SIGN_IDENTITY = "Mac Developer";
				COMBINE_HIDPI_IMAGES = YES;
				DEVELOPMENT_TEAM = 38YBH9NQ69;
				INFOPLIST_FILE = kgtwhelper/Info.plist;
				LD_RUNPATH_SEARCH_PATHS = "$(inherited) @executable_path/../Frameworks";
				PRODUCT_BUNDLE_IDENTIFIER = com.smallpearl.kensingtonworks2.helper;
				PRODUCT_NAME = "$(TARGET_NAME)";
			};
			name = Release;
		};
/* End XCBuildConfiguration section */

/* Begin XCConfigurationList section */
		47FFE571233B0ECA008BD7F2 /* Build configuration list for PBXProject "KensingtonWorksAgent" */ = {
			isa = XCConfigurationList;
			buildConfigurations = (
				47FFE585233B0ECB008BD7F2 /* Debug */,
				47FFE586233B0ECB008BD7F2 /* Release */,
			);
			defaultConfigurationIsVisible = 0;
			defaultConfigurationName = Release;
		};
		47FFE587233B0ECB008BD7F2 /* Build configuration list for PBXNativeTarget "KensingtonWorksAgent" */ = {
			isa = XCConfigurationList;
			buildConfigurations = (
				47FFE588233B0ECB008BD7F2 /* Debug */,
				47FFE589233B0ECB008BD7F2 /* Release */,
			);
			defaultConfigurationIsVisible = 0;
			defaultConfigurationName = Release;
		};
/* End XCConfigurationList section */
	};
	rootObject = 47FFE56E233B0ECA008BD7F2 /* Project object */;
}
