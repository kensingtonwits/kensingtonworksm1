//
//  main.cpp
//  tbwhelper
//
//
#include "../../shared/stdafx.h" //*&*&*&V1_20201113
#include <assert.h>
#include <errno.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/sysctl.h>

#include <boost/log/core.hpp>
#include <boost/log/trivial.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/sinks/text_file_backend.hpp>
#include <boost/log/utility/setup/file.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>
#include <boost/log/sinks/debug_output_backend.hpp>
#include <boost/log/sources/severity_logger.hpp>
#include <boost/log/utility/setup/console.hpp>
#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>

#include <Cocoa/Cocoa.h>

#include "PlatformServices.h"
#include "DeviceManager.h"
//#include "CommandServer.h"

#ifdef COMMAND_MESSAGE_QUEUE
#include "../../shared/CmdMsger.h"
#else
#include "CommandServer.h"
#endif
#include "../../shared/tbwMshm.h"

#include "Settings.h"
#include "Migrate.h"
#import <IOKit/hidsystem/IOHIDLib.h>
#import  "AllowGuide.h"
#include <thread>

using namespace std;
namespace logging = boost::log;
namespace src = boost::log::sources;
namespace sinks = boost::log::sinks;
namespace keywords = boost::log::keywords;

CFRunLoopRef g_rlWorkerThread = NULL;
//@property (nonatomic, strong) PrivacyOpenGuide *privacyGuideWindowController;

// program options
struct ProgramOptions {
    bool _terminateRunning;
    logging::trivial::severity_level _logLevel;
    bool _logConsoleOutput;
    bool _install;
    ProgramOptions()
    : _terminateRunning(false)
#ifdef DEBUG
    , _logLevel(logging::trivial::trace)
    , _logConsoleOutput(true)
#else
    , _logLevel(logging::trivial::info)
    , _logConsoleOutput(false)
#endif
    , _install(false)
    {}
};

static ProgramOptions processCommandLine(const char** argv, int argc);
static int GetBSDProcessList(kinfo_proc **procList, size_t *procCount);
static pid_t getActiveHelperPid();
static void quitExistingApp();
static void initLog(logging::trivial::severity_level lvl, bool consoleOutput);
static void deinitLog();
static void migrateLegacySettings();
//static void handleTbwHelper();


@interface NSString (ShellExecution)
- (NSString*)runAsCommand;
@end

@implementation NSString (ShellExecution)

- (NSString*)runAsCommand {
    NSPipe* pipe = [NSPipe pipe];

    NSTask* task = [[NSTask alloc] init];
    [task setLaunchPath: @"/bin/sh"];
    [task setArguments:@[@"-c", [NSString stringWithFormat:@"%@", self]]];
    [task setStandardOutput:pipe];

    NSFileHandle* file = [pipe fileHandleForReading];
    [task launch];

    return [[NSString alloc] initWithData:[file readDataToEndOfFile] encoding:NSUTF8StringEncoding];
}
@end

// Throws exception if invalid arguments are passed!
ProgramOptions processCommandLine(const char** argv, int argc)
{
    namespace po = boost::program_options;
    try
    {
        ProgramOptions options;
        
        po::options_description desc("Options");
        desc.add_options()
        ("logLevel", po::value<int>(), "Log level (0-5, 0: maximum logging, 5: minimum logging)")
        ("quit", "Terminate running helper")
        ("dbgOutput", "Output log messages to OS console")
        ("verbose", "Enable maximum logging")
        ("install", "Perform post-installation initialization")
        ;
        
        po::variables_map vm;
        po::store(po::parse_command_line<char>(argc, argv, desc), vm);
        po::notify(vm);
        
        if (vm.count("logLevel")) {
            auto logLevel = vm["logLevel"].as<int>();
            // sanitize int to logging::trivial::severity_level enum range
            logLevel = std::min(std::max(logLevel, (int)logging::trivial::trace), (int)logging::trivial::fatal);
            // convert sanitized value back to severity_level
            options._logLevel = (logging::trivial::severity_level)logLevel;
        }
        
        if (vm.count("dbgOutput")) {
            options._logConsoleOutput = true;
        }
        
        if (vm.count("verbose")) {
            options._logLevel = logging::trivial::trace;
        }
        
        if (vm.count("quit")) {
            options._terminateRunning = true;
        }
        
        if (vm.count("install")) {
            options._install = true;
        }
        
        return options;
    }
    catch (po::error& e)
    {
        std::cerr << e.what();
        throw;
    }
}


/*
 * Returns a list of all BSD processes on the system.  This routine
 * allocates the list and puts it in *procList and a count of the
 * number of entries in *procCount.  You are responsible for freeing
 * this list (use "free" from System framework).
 * On success, the function returns 0.
 * On error, the function returns a BSD errno value.
 *
 * Copied as is from:
 * https://developer.apple.com/library/archive/qa/qa2001/qa1123.html
 */
static int GetBSDProcessList(kinfo_proc **procList, size_t *procCount)
{
    int                 err;
    kinfo_proc *        result;
    bool                done;
    
    // We only get processes that belong to the current user session.
    // This way we don't terminate tbwhelper process started in another user's
    // concurrent session.
    uid_t uid = geteuid();
    static const int    name[] = { CTL_KERN, KERN_PROC, KERN_PROC_UID, (int)uid, 0 };
    // Declaring name as const requires us to cast it when passing it to
    // sysctl because the prototype doesn't include the const modifier.
    size_t              length;
    
    assert( procList != NULL);
    assert(*procList == NULL);
    assert(procCount != NULL);
    
    *procCount = 0;
    
    // We start by calling sysctl with result == NULL and length == 0.
    // That will succeed, and set length to the appropriate length.
    // We then allocate a buffer of that size and call sysctl again
    // with that buffer.  If that succeeds, we're done.  If that fails
    // with ENOMEM, we have to throw away our buffer and loop.  Note
    // that the loop causes use to call sysctl with NULL again; this
    // is necessary because the ENOMEM failure case sets length to
    // the amount of data returned, not the amount of data that
    // could have been returned.
    
    result = NULL;
    done = false;
    do {
        assert(result == NULL);
        
        // Call sysctl with a NULL buffer.
        
        length = 0;
        err = sysctl( (int *) name, (sizeof(name) / sizeof(*name)) - 1,
                     NULL, &length,
                     NULL, 0);
        if (err == -1) {
            err = errno;
        }
        
        // Allocate an appropriately sized buffer based on the results
        // from the previous call.
        
        if (err == 0) {
            result = (kinfo_proc*)malloc(length);
            if (result == NULL) {
                err = ENOMEM;
            }
        }
        
        // Call sysctl again with the new buffer.  If we get an ENOMEM
        // error, toss away our buffer and start again.
        
        if (err == 0) {
            err = sysctl( (int *) name, (sizeof(name) / sizeof(*name)) - 1,
                         result, &length,
                         NULL, 0);
            if (err == -1) {
                err = errno;
            }
            if (err == 0) {
                done = true;
            } else if (err == ENOMEM) {
                assert(result != NULL);
                free(result);
                result = NULL;
                err = 0;
            }
        }
    } while (err == 0 && ! done);
    
    // Clean up and establish post conditions.
    
    if (err != 0 && result != NULL) {
        free(result);
        result = NULL;
    }
    *procList = result;
    if (err == 0) {
        *procCount = length / sizeof(kinfo_proc);
    }
    
    assert( (err == 0) == (*procList != NULL) );
    
    return err;
}

/*
 * Returns the pid of active tbwhelper process. If tbwhelper is not active
 * Returns 0
 */
pid_t getActiveHelperPid()
{
    kinfo_proc* procList = NULL;
    size_t procCount = 0;
    pid_t pidHelper = 0;

    if (GetBSDProcessList(&procList, &procCount) == 0) {
        for (size_t i=0; i<procCount; i++) {
            kinfo_proc* proc = &procList[i];
            if (::strcmp(proc->kp_proc.p_comm, "tbwhelper") == 0
                && getpid() != proc->kp_proc.p_pid) { // exclude ourselves!
                pidHelper = proc->kp_proc.p_pid;
                break;
            }
        }
        
        ::free((void*)procList);
    }
    return pidHelper;
}

/*
 * Find the process id of the other instance of tbwhelper and
 * send it a SIGHUP causing it to terminate.
 */
void quitExistingApp()
{
    pid_t pidHelper = getActiveHelperPid();
    if (pidHelper) {
        // send sighup to it
        kill(pidHelper, SIGHUP);
    }
}

// Initialize the log system (uses Boost::log)
void initLog(logging::trivial::severity_level lvl, bool consoleOutput)
{
    boost::filesystem::path logFilepath(PlatformServices::getInstance()->getLogFileFolder());
    logFilepath.append("tbwhelper_%2N.log");
    logging::add_file_log
    (
     keywords::file_name = logFilepath,
     keywords::rotation_size = 5 * 1024 * 1024,
     keywords::format = "%TimeStamp%: %ThreadID% %Message%",
     keywords::open_mode = std::ios::app,
     keywords::auto_flush = true
     );
    
    if (consoleOutput)
    {
        logging::add_console_log(std::cout, keywords::format = "%ThreadID% %Message%");
    }
    
    logging::core::get()->set_filter(logging::trivial::severity >= lvl);
    
    logging::add_common_attributes();
    
    BOOST_LOG_TRIVIAL(info) << "Log initialized";
}

void deinitLog()
{
    BOOST_LOG_TRIVIAL(info) << "Log deinitialized\n";
}

/*
 * If TbwSettings.json does not exist and TbwSettings.xml is found, migrate
 * TbwSettings.xml to TbwSettings.json.
 */
void migrateLegacySettings()
{
    try
    {
        if (!boost::filesystem::exists(Settings::getFileFullPath().c_str())
            && boost::filesystem::exists(Settings::getLegacyFileFullPath().c_str()))
        {
            // TbwSettings.json does not exist and TbwSettings.xml does. Or user
            // has explicitly requested forced migration.
            SettingsMigrator sm;
            sm.migrate();
        }
    }
    catch (const std::exception& e)
    {
        BOOST_LOG_TRIVIAL(warning) << "Error migrating old settings: \"" << e.what() << "\", using default settings..";
    }
}

// Checks if accessibility is enabled for this app.
// fPrompt:
//         YES - Force the authorization screen to appear.
//         NO  - Just get the the status of Accesibility.
bool isAccessibilityEnabled(bool fPrompt)
{
    // This prompts the user to enable Accebility for tbwhelper.
    // I had originally written this in Objective-C using bridging.
    // But this implementation is better as it's pure C.
    // From: https://stackoverflow.com/a/24863855/744769
    const void * keys[] = { kAXTrustedCheckOptionPrompt };
    const void * values[] = { fPrompt ? kCFBooleanTrue : kCFBooleanFalse };

    CFDictionaryRef options = CFDictionaryCreate(
            kCFAllocatorDefault,
            keys,
            values,
            sizeof(keys) / sizeof(*keys),
            &kCFCopyStringDictionaryKeyCallBacks,
            &kCFTypeDictionaryValueCallBacks);

    bool ret = AXIsProcessTrustedWithOptions(options) == YES;
    CFRelease(options);

    return ret;
}

#ifdef COMMAND_MESSAGE_QUEUE
	//CommandMsgHandler *CommandMsgHandler::m_instance = nullptr;
#endif

/**
 * A simple class to wrap DeviceManager & CommandServer into one object.
 */
class TBWHelper {
    DeviceManager* _deviceManager;
#ifdef COMMAND_MESSAGE_QUEUE
//	std::unique_ptr<CommandMsgHandler> _cmdMsgHandler;
    //CommandMsgHandler *_cmdMsgHandler = nullptr;
#else
    std::unique_ptr<CommandServer> _apiServer;
#endif

public:
    TBWHelper()
        : _deviceManager(nullptr)
#ifdef COMMAND_MESSAGE_QUEUE
		//, _cmdMsgHandler(new CommandMsgHandler(_deviceManager, true)) //false))
#else
#endif
    {
        _deviceManager = DeviceManager::getInstance();
        _deviceManager->start();
        //_apiServer.reset(new CommandServer(_deviceManager, false));
        //_apiServer->Start();
#ifdef COMMAND_MESSAGE_QUEUE
        CommandMsger::CommandMsger_init(_deviceManager, false);
        CommandMsger::CommandMsger_Start();
#endif
    }
    
    ~TBWHelper()
    {
        BOOST_LOG_TRIVIAL(info) << "~TbwHelper() " ;
		std::cout << " ~TbwHelper() \r\n";
#ifdef COMMAND_MESSAGE_QUEUE
		CommandMsger::CommandMsger_Stop();
#endif

        //if (_apiServer) {
        //    _apiServer->Stop();
        //    _apiServer.reset();
        //}
        
        if (_deviceManager) {
            _deviceManager->stop();
            DeviceManager::destroyInstance();
        }
    }
};

TBWHelper* g_helper = nullptr;

#ifdef FAST_USER_SWITCHING_TEST
int g_reinitCount = 0;
#endif

// system signal handler.
static void sig(int signal)
{
    std::cout << "Quit signal received" << std::endl;
    
    if (g_rlWorkerThread)
    {
#ifdef FAST_USER_SWITCHING_TEST
        // For DEV testing only!!
        //
        // This is for testing DeviceManager & CommandServer destroy/create
        // cycle that can happend during Fast User Switching. Particularly
        // we want to test that if all the resources allocated by these objects
        // are cleaned up properly when they are destroyed and if there are
        // any memory leaks.
        if (g_reinitCount++ < 10)
        {
            if (g_helper) {
                std::cout << "\tdestroying TBWHelper instance" << std::endl;
                delete g_helper; g_helper = nullptr;
            } else {
                std::cout << "\tcreating TBWHelper instance" << std::endl;
                g_helper = new TBWHelper();
            }
        }
        else
#endif
        {
            delete g_helper; g_helper = nullptr;
            deinitLog();

            [[NSApplication sharedApplication] terminate:nil];
            //CFRunLoopStop(g_rlWorkerThread);
        }
    }
}

// An ObjectiveC object to receive Fast User Switching notifications.
// Apple stopped supporting the pure C Carbon API for this. However
// the Cocoa (ObjectiveC) interface is being maintained. Therefore
// we have to resort to using ObjectiveC object to implement this.
// Thankfully the ObjC interoperability feature works great.
//
// Code borrowed from Apple's knowledge base here:
// https://developer.apple.com/library/archive/documentation/MacOSX/Conceptual/BPMultipleUsers/Concepts/UserSwitchNotifications.html#//apple_ref/doc/uid/20002210-101401
// Note that this article shows pure C code too, but the APIs
// necessary for that are not being exposed since 10.7 SDK.
// They probably are still being maintained, but it's a risk to
// use them.
@interface UserSwitchMonitor : NSObject
@end

@implementation UserSwitchMonitor

- (void)registerCallbacks {
    [[[NSWorkspace sharedWorkspace] notificationCenter]
        addObserver:self
        selector:@selector(didBecomeActive:)
        name:NSWorkspaceSessionDidBecomeActiveNotification
        object:nil];
    
    [[[NSWorkspace sharedWorkspace] notificationCenter]
        addObserver:self
        selector:@selector(didBecomeInactive:)
        name:NSWorkspaceSessionDidResignActiveNotification
        object:nil];
}

// Session became active callback
- (void) didBecomeActive:(NSNotification*) notification {
    BOOST_LOG_TRIVIAL(info) << "Session became active";
    
    try
    {
        if (!g_helper) {
            g_helper = new TBWHelper();
        }
    }
    catch(std::exception& e)
    {
        BOOST_LOG_TRIVIAL(error) << "\terror creating app: " << e.what();
    }
}

// Session became inactive callback.
- (void) didBecomeInactive:(NSNotification*) notification {
    BOOST_LOG_TRIVIAL(info) << "Session became inactive";
    
    try
    {
        delete g_helper;
        g_helper = nullptr;
    }
    catch(std::exception& e)
    {
        BOOST_LOG_TRIVIAL(error) << "\terror destroying app: " << e.what();
    }
}
@end

// Code taken from Apple sample @
// https://developer.apple.com/library/archive/samplecode/PreLoginAgents/Listings/PreLoginAgentCocoa_main_m.html
static void InstallHandleSIGTERMFromRunLoop(void)
    // This routine installs a SIGTERM handler that's called on the main thread, allowing
    // it to then call into Cocoa to quit the app.
{
    static dispatch_once_t   sOnceToken;
    static dispatch_source_t sSignalSource;
 
    dispatch_once(&sOnceToken, ^{
        signal(SIGTERM, SIG_IGN);
    
        sSignalSource = dispatch_source_create(DISPATCH_SOURCE_TYPE_SIGNAL, SIGTERM, 0, dispatch_get_main_queue());
        assert(sSignalSource != NULL);
        
        dispatch_source_set_event_handler(sSignalSource, ^{
            assert([NSThread isMainThread]);
            
            BOOST_LOG_TRIVIAL(info) << "SIGTERM received...";

            //[[LogManager sharedManager] logWithFormat:@"Got SIGTERM"];
            delete g_helper; g_helper = nullptr;
            deinitLog();

            [[NSApplication sharedApplication] terminate:nil];
        });
        
        dispatch_resume(sSignalSource);
    });
}

bool isInputMonitoringEnabled()
{
    bool bRet = false;
    //NSLog(@"=======isInputMonitoringEnabled start=======");
    @autoreleasepool {
        // Setup code that might create autoreleased objects goes here.
        if(@available(macos 10.15, *))
        {
            IOHIDAccessType theType = IOHIDCheckAccess(kIOHIDRequestTypeListenEvent);
            //NSLog(@"isInputMonitoringEnabled - IOHIDCheckAccess = %d", theType);
            switch (theType)
            {
                case kIOHIDAccessTypeGranted: // 0
                    NSLog(@"isInputMonitoringEnabled  - kIOHIDAccessTypeGranted");
                    bRet = true;
                    break;
                case kIOHIDAccessTypeDenied: // 1
                {
                    NSLog(@"isInputMonitoringEnabled  - kIOHIDAccessTypeDenied");
                    // denied
                    //NSString *urlString = @"x-apple.systempreferences:com.apple.preference.security?Privacy_ListenEvent";
                    //[[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:urlString]];
                    break;
                }
                case kIOHIDAccessTypeUnknown: // 2
                {
                    // 未彈過提示框
                    bool result = IOHIDRequestAccess(kIOHIDRequestTypeListenEvent);
                    NSLog(@"isInputMonitoringEnabled - IOHIDRequestAccess result = %d", result);
                    break;
                }
                default:
                    break;
            }
        }
    }
    //NSLog(@"=======isInputMonitoringEnabled end=======");
    return bRet;
}

// Check the installer is exited or not
// It doens't only check kensingtonworks install process exit, but also check installing other softwawre.
// We don't need to care of it too much because user is not always open installer.
bool isInstallerQuited()
{
#if FALSE // these way only works in English system...
    NSWorkspace *nsWorkspace=[NSWorkspace sharedWorkspace];
    NSArray *runningApplicationsArray = [nsWorkspace runningApplications];
     
    for (NSRunningApplication *nsApp in runningApplicationsArray) {
        if([nsApp.localizedName isEqualToString:@"Installer"]){
            NSLog(@"%@",nsApp.localizedName);
            return false;
        }
    }
#else
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *pathForFile = @"/tmp/kgtwInstaller";
    if ([fileManager fileExistsAtPath:pathForFile]){
        NSLog(@"[tbwhelper] the kgtwInstaller flag is now existing. We wont appear the allow dialog.");
        return false;
    }
#endif
    return true;
}

int main(int argc, const char * argv[])
{
    try
    {
        auto po = processCommandLine(argv, argc);
        if (po._terminateRunning)
        {
            quitExistingApp();
            return 0;
        }

        // vannes20200331 during installation we only want system guide user to do that.
        if (po._install)
        {  
            // vannes20200331 temperarily we dont leverage this return value. Should we use it afterward.
            //return isAccessibilityEnabled(true) ? 0 : 1;
            if(@available(macos 10.13, *)){ // accessibility trigger 
                if(isAccessibilityEnabled(false) == false){ // this tries to trigger system reminding user enable accessibility
                    ;
                }                
            }
            return 1; // if we return zero here the install process will be failed
        }
        
        // check if another instance os already running
        if (getActiveHelperPid() != 0)
        {
            // another instance is active
            return 1;
        }

        initLog(po._logLevel, po._logConsoleOutput);
        NSOperatingSystemVersion osVer = [[NSProcessInfo processInfo] operatingSystemVersion];
        int mainVer = (int)osVer.majorVersion;
        int subVer = (int)osVer.minorVersion;
        int minorVer = (int)osVer.patchVersion;
        
#define INPUT_MONITORING_NOT_ALLOWED 1
#define ACCESSIBILITY_NOT_ALLOWED    2
#define KEXT_NOT_ALLOWED             4
#define DRIVERKIT_NOT_ALLOWED        8
        
        // This value is added as binary mode(for example, 0,1,2,4,8...)
        // 1 means InputMonitoring not allowed
        // 2 means Accessibility not allowed.
        // 4 means Kext need to be allowed.
        int stateMode = 0;

        //if(@available(macos 10.13, *)){ // tbd: 這僅能代表API的使用方式不能代表可以跑的OS
            NSFileManager *fileManager = [NSFileManager defaultManager];
            NSString *pathForFile = @"/tmp/kgtwallow";
            if (![fileManager fileExistsAtPath:pathForFile]){
                NSLog(@"[tbwhelper] the flag is not existing. We will appear the allow dialog.");
                stateMode += KEXT_NOT_ALLOWED;
            }
        //}
        
        if(@available(macos 10.15, *)){
            if(isInputMonitoringEnabled() == false){
                NSLog(@"[tbwhelper] isInputMonitoringEnabled() returns false;");
                stateMode += INPUT_MONITORING_NOT_ALLOWED;
            }
        }
        if(@available(macos 10.13, *)){ // tbd: 這僅能代表API的使用方式不能代表可以跑的OS
            if (!isAccessibilityEnabled(false))
            {
                BOOST_LOG_TRIVIAL(warning)
                    << "Accessibility not enabled for TrackballWorks. Some features will not function properly";

                stateMode += ACCESSIBILITY_NOT_ALLOWED;
            }
        }
        if((mainVer == 10 && subVer >= 16) || mainVer >= 11) // only for Big Sur
        {
            NSString* tmpStr = [@"systemextensionsctl list | grep com.kensington.tbwDKDriver" runAsCommand];
            if([tmpStr containsString:(@"com.kensington.tbwDKDriver")])
            {
                if([tmpStr containsString:(@"activated waiting for user")]) // if there is not activated-enabled
                {
                    stateMode += DRIVERKIT_NOT_ALLOWED;
                }
            }
            else
            {
                BOOST_LOG_TRIVIAL(info) << "[TBWHelper] main() - It's impossible here...";
            }
        }
        
        bool bInstallerQuited = isInstallerQuited();
        // show AllowGuide
        if((stateMode > 0) && (stateMode != 4) && (bInstallerQuited)){
            NSLog(@"[tbwhelper]main::trying to load dialog.");
            AllowGuide *allowGuide = [[AllowGuide alloc] init];
            [allowGuide getParam:stateMode];
            [allowGuide.window center];
            [NSApp activateIgnoringOtherApps:YES];
            [allowGuide.window makeKeyAndOrderFront:nil];
            [allowGuide topMostMyself];
        }
        else{
            NSLog(@"[tbwhelper]main::No need to load AllowGuide.");
        }

        // If TbwSettings.json is not present and legacy settings file
        // is present, this will migrate legacy settings to TbwSettings.json.
        // If TbwSettings.json is present, it does nothing.
        migrateLegacySettings();
        
        // Actions macro data file is integral to the system. Therefore
        // verify that the data file exists and that its contents
        // are valid.
        //
        // To do this, simpliy instante the ActionsFactory object which
        // would raise an exception if the file does not exist, or if
        // its contents are invalid.
        std::unique_ptr<ActionFactory> af(new ActionFactory());
        af.reset();

        ProcessSerialNumber psn = { 0, kCurrentProcess };
        //kProcessTransformToBackgroundApplication = 2, /* functional in Mac OS X 10.7 and later */
        //kProcessTransformToUIElementApplication = 4 /* functional in Mac OS X 10.7 and later */
        TransformProcessType(&psn, kProcessTransformToBackgroundApplication);

        g_rlWorkerThread = CFRunLoopGetCurrent();
        
        g_helper = new TBWHelper();
        std::cout << "Press Ctrl+C to quit..\n";

        // This will track fast user switching and when current session
        // goes inactive, it will destroy the global TbwHelper object
        // releasing the device and TCP/IP port. When the session becomes
        // active again, it will recreate the App object, activating the
        // user's TBW configuration as well as enabling them to use the
        // GUI to configure it further.
        UserSwitchMonitor* o = [UserSwitchMonitor alloc];
        [o registerCallbacks];

        /* Console style signal handler is not useful for us as we're a Cocoa app.
        // setup signal handler
        signal(SIGINT, sig);
        signal(SIGHUP, sig);
        signal(SIGTERM, sig);
        */
        InstallHandleSIGTERMFromRunLoop();
        
        // sit in loop until SIGTERM is received
        //CFRunLoopRun();
        NSApplicationMain(argc, argv);
        
        o = nil;

        delete g_helper; g_helper = nullptr;
    }
    catch(std::exception& e)
    {
        BOOST_LOG_TRIVIAL(error) << "Error initializing program: " << e.what();
    }

    deinitLog();
    return 0;
}


//void handleTbwHelper(){
//    try  {
//        g_helper = new TBWHelper();
//        std::cout << "Press Ctrl+C to quit..\n";
//    } catch(std::exception& e) {
//        BOOST_LOG_TRIVIAL(error) << "Error initializing program: " << e.what();
//    }
//}
