//
//  AppDelegate.m
//  kgtwhelper
//
//

#import "AppDelegate.h"
#include "tbwMshm.h" // *&*&*&Va_20201204

TBWHelperApp::TBWHelperApp()
    : _name("KensingtonWorks"), _dummy(new DummyObject)
{
    NSLog(@"TBWHelper::TBWHelper");
}

TBWHelperApp::~TBWHelperApp()
{
    NSLog(@"TBWHelper::~TBWHelper");
}

@interface AppDelegate ()

@property (weak) IBOutlet NSWindow *window;
@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{
    // Insert code here to initialize your application
   // [self activateStatusMenu]; // vannes20200408 stop showing icon on menubar
    
    _helperApp = new TBWHelperApp();
    _launchTimer = [NSTimer scheduledTimerWithTimeInterval:0.3 // 300ms
        target:self
        selector:@selector(handleLaunchTimer:)
        userInfo:nil
        repeats:YES];
}


- (void)applicationWillTerminate:(NSNotification *)aNotification
{
    // Insert code here to tear down your application
    //delete _helperApp;
}

- (void)activateStatusMenu
{
    NSStatusBar *bar = [NSStatusBar systemStatusBar];
 
    theItem = [bar statusItemWithLength:NSSquareStatusItemLength];
    [[theItem button] setImage: [NSImage imageNamed:@"Status"]];
    [theItem setHighlightMode:YES];
    
    NSMenu* menu = [[NSMenu alloc] init];
    NSMenuItem* item = [[NSMenuItem alloc] init];
    [item setTitle:@"KensingtonWorks"];
    [item setAction:@selector(activateGUI)];
    [menu addItem: item];

    /*
    item = [[NSMenuItem alloc] init];
    [item setTitle:@"Quit"];
    [item setAction:@selector(quitApp)];
    [menu addItem: item];
    */
    
    [theItem setMenu:menu];
}

- (void) activateGUI
{
    if(![[NSWorkspace sharedWorkspace] launchApplication:@"KensingtonWorks "])
        NSLog(@"KensingtonWorks failed to launch");
}

- (void) quitApp
{
    [[NSApplication sharedApplication] stop:nil];
    //[NSApp terminate:self];
}
- (void) handleLaunchTimer:(NSTimer*)timer
{
    _launchTimer = nil;
    //NSLog(@"Launch Timer handler"); // it works
    tbwMshm::wHeartbeat();
}

@end
