#pragma once

/*
    Volume values range from 0.0 to 1.0
 */
float getVolume();
void setVolume(float);

// post special keys, such as media keys, etc into input queue. 
void HIDPostAuxKey(uint32_t key, bool down);
