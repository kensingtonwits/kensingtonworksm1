//
//  AppDelegate.h
//  kgtwhelper
//
//
#include <string>
#include <memory>

#import <Cocoa/Cocoa.h>

struct DummyObject {
    char buf[1000];
};

struct TBWHelperApp {
    std::string _name;
    std::unique_ptr<DummyObject> _dummy;
    
    TBWHelperApp();
    virtual ~TBWHelperApp();
};

@interface AppDelegate : NSObject <NSApplicationDelegate>
{
    NSStatusItem *theItem;
    TBWHelperApp* _helperApp;
    NSTimer* _launchTimer;

}
- (void) activateStatusMenu;
- (void) activateGUI;
- (void) quitApp;
- (void) handleLaunchTimer:(NSTimer*)timer;
@end
