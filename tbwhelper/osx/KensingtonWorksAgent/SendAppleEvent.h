#pragma once

#include <CoreServices/CoreServices.h>
#include <Carbon/Carbon.h>

uint32_t AEVerbToEventId(const char* verb);

// 
// Pass these constants from AERegistry.h to get the desired results:
//
//  kAERestart - Restart the system immediately.
//  kAEShutDown - Shutdown the system immediately.
//  kAEShowRestartDialog - Show restart confirmation dialog.
//  kAEShowShutdownDialog - Show shutdown confirmation dialog.
//  kAELogOut - Show logout confirmation dialog.
//  kAEReallyLogOut - Logout immediately.
//  kAESleep - Sleep computer
//
// Returns:
//  0 on no error. Error code otherwise.
OSStatus SendAppleEventToSystemProcess(AEEventID EventToSend, UInt32 process);

