
#include <iostream>
#include <iomanip>
#include <utility>

#include <libkern/OSAtomic.h>
#include <IOKit/IOKitLib.h>
#include <IOKit/IOMessage.h>
#include <CoreFoundation/CoreFoundation.h>
#include <Foundation/Foundation.h>
#include <Cocoa/Cocoa.h>
#include <Carbon/Carbon.h>

#include <boost/log/trivial.hpp>

#include "DeviceManager.h"
#include <SystemExtensions/SystemExtensions.h>
#include "Settings.h"
#include "PlatformServices.h"
#include "ModKeyListener.hpp"
#include "Modifiers.h"
#include "hidconfig.hpp"

#include "tbwMshm.h" // *&*&*&V1_20201123

#import <Cocoa/Cocoa.h>
#import <SystemExtensions/SystemExtensions.h>

// *&*&*&V1_20201201
//-(void)updateString
//{
//    //[textField setStringValue:@"new text"];
//    BOOST_LOG_TRIVIAL(info) << "========tbwHelper time start=========";
//
//    BOOST_LOG_TRIVIAL(info) << "========tbwHelper time end=========";
//}

// some convenience methods
#if 0
// This function is not used, but keeping it here for reference.
// Return device property, by default recursing into parents
static CFTypeRef getDeviceProperty(io_service_t service, const char* property, bool fRecurse=true)
{
    CFStringRef cfstrProperty = CFStringCreateWithCStringNoCopy(kCFAllocatorDefault,
                                                                property,
                                                                kCFStringEncodingASCII,
                                                                kCFAllocatorDefault);
    if (!cfstrProperty) {
        return NULL;
    }
    
    IOOptionBits options = 0;
    if (fRecurse) {
        options = kIORegistryIterateParents|kIORegistryIterateRecursively;
    }
    CFTypeRef cfValue = IORegistryEntrySearchCFProperty(service,
                                                        kIOServicePlane,
                                                        cfstrProperty,
                                                        kCFAllocatorDefault,
                                                        options);
    return cfValue;
}
#endif

static void throw_os_error(kern_return_t kr)
{
    BOOST_LOG_TRIVIAL(error) << "Kernel API error, kern_return_t: 0x"
        << std::hex << kr;
    throw OSError(kr);
}

DeviceManager* DeviceManager::_instance = nullptr;

DeviceManager* DeviceManager::getInstance()
{
    if (!DeviceManager::_instance) {
        BOOST_LOG_TRIVIAL(error) << "DeviceManager::getInstance - create new.";
        DeviceManager::_instance = new DeviceManager();
        //tbwMshm::tbwMshm_create(getpid()); //*&*&*&V1_20201123
        tbwMshm::tbwMshm_setup(getpid()); //*&*&*&V1_20201130
    }
    return _instance;
}

void DeviceManager::destroyInstance()
{
    delete _instance;
    _instance = nullptr;
}

DeviceManager::DeviceManager()
: _worker()
, _rlRef(0)
, _notificationPort(NULL)
, _iter(IO_OBJECT_NULL)
, _foregroundAppMonitor(nullptr)
, _foregroundAppBinary()
, _foregroundAppBundle()
, _foregroundPid(0)
, _rlSourceForegroundApp(nullptr)
, _rlSourceSettingsChange(nullptr)
, _modKeyListener()
, _inited(false)
{
    BOOST_LOG_TRIVIAL(info) << "DeviceManager::DeviceManager()";
}

DeviceManager::~DeviceManager()
{
    if (_foregroundAppMonitor)
        pqrs_osx_frontmost_application_monitor_terminate(&_foregroundAppMonitor);
    
    if (_notificationPort)
        IONotificationPortDestroy(_notificationPort);
    
    // Release the iterator
    if (_iter)
        IOObjectRelease(_iter);
    
    _devices.clear();
    //tbwMshm::tbwMshm_remove(); // *&*&*&V1_20201123
}

void DeviceManager::start()
{
    std::condition_variable cvInit;
    kern_return_t kr = KERN_SUCCESS;
    std::unique_lock<std::mutex> lk(_mutex);

    _worker = std::thread(DeviceManager::sMain, this, std::ref(cvInit), std::ref(kr));
    // Wait for the condition_variable will be signalled, which will
    // indicate that worker thread has completed initializeation.
    cvInit.wait(lk);

    if (kr != KERN_SUCCESS)
        throw std::runtime_error("Error initializing DeviceManager");
}

void DeviceManager::stop()
{
    CFRunLoopStop(_rlRef);
    _worker.join();
}

@interface SNTSystemExtensionDelegate : NSObject<OSSystemExtensionRequestDelegate>
@end

@implementation SNTSystemExtensionDelegate

#pragma mark OSSystemExtensionRequestDelegate

- (OSSystemExtensionReplacementAction)request:(OSSystemExtensionRequest *)request
                  actionForReplacingExtension:(OSSystemExtensionProperties *)old
                                withExtension:(OSSystemExtensionProperties *) _new
  API_AVAILABLE(macos(10.15)) {
  NSLog(@"tbwDKDriver SystemExtension \"%@\" request for replacement", request.identifier);
  return OSSystemExtensionReplacementActionReplace;
}

- (void)requestNeedsUserApproval:(OSSystemExtensionRequest *)request API_AVAILABLE(macos(10.15)) {
  NSLog(@"tbwDKDriver SystemExtension \"%@\" request needs user approval", request.identifier);
}

- (void)request:(OSSystemExtensionRequest *)request
    didFailWithError:(NSError *)error API_AVAILABLE(macos(10.15)) {
  NSLog(@"tbwDKDriver SystemExtension  \"%@\" request did fail: %@", request.identifier, error);
  exit((int)error.code);
}

- (void)request:(OSSystemExtensionRequest *)request
    didFinishWithResult:(OSSystemExtensionRequestResult)result API_AVAILABLE(macos(10.15)) {
  NSLog(@"tbwDKDriver SystemExtension \"%@\" request did finish: %ld", request.identifier, (long)result);
  exit(0);
}

@end


// *&*&*&V1_20200908
@interface NSString (ShellExecution)
- (NSString*)runAsCommand;
@end

@implementation NSString (ShellExecution)

- (NSString*)runAsCommand {
    NSPipe* pipe = [NSPipe pipe];

    NSTask* task = [[NSTask alloc] init];
    [task setLaunchPath: @"/bin/sh"];
    [task setArguments:@[@"-c", [NSString stringWithFormat:@"%@", self]]];
    [task setStandardOutput:pipe];

    NSFileHandle* file = [pipe fileHandleForReading];
    [task launch];

    return [[NSString alloc] initWithData:[file readDataToEndOfFile] encoding:NSUTF8StringEncoding];
}

@end

void DeviceManager::activate_tbwDKManager()
{ // 這裡再多想想有沒有漏掉什麼狀況
    int doActivateDriver = 0;
    bool justInstalled = false; // planned do-activate after restart..
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *pathForFile = @"/tmp/kgtwInstaller";
    if ([fileManager fileExistsAtPath:pathForFile]){
        NSLog(@"[tbwhelper] DeviceManager::activate_tbwDKManager() - the kgtwInstaller flag is now existing.");
        justInstalled = true;
    }

    NSString* tmpStr = [@"systemextensionsctl list | grep com.kensington.tbwDKDriver" runAsCommand];
    if([tmpStr containsString:(@"com.kensington.tbwDKDriver")]) // already has record
    {
        if(![tmpStr containsString:(@"activated enabled")]) // if there is not activated-enabled or just uninstall last version
        {
            doActivateDriver = 1;
        }
        else // has activated-enabled
        {
            if ([tmpStr containsString:(@"KensingtonWorks DriverKit driver 11")]) // Force upgrade...will do each start
            {
                doActivateDriver = 2;
            }
            else // old drtverkit driver
            {
                std::system("open -W '/Applications/.KensingtonWorks.app' --args --uninstall"); // 解除本來給10.15的安裝
                NSString* tmpStr1 = [@"systemextensionsctl list | grep com.kensington.tbwDKDriver" runAsCommand]; // 再查一次看使用者是否已解除安裝
                if(![tmpStr1 containsString:(@"activated enabled")]) // till there's no activated-enabled then do-activate
                {
                    doActivateDriver = 1;
                }
            }
        }
    }
    else // not activated yet
    {
        BOOST_LOG_TRIVIAL(info) << "DeviceManager::activate_tbwDKManager(): Not activated yet, status: "<< [tmpStr UTF8String];
        doActivateDriver = 1;
    }

    if(doActivateDriver == 1)
    {
        BOOST_LOG_TRIVIAL(info) << "DeviceManager::activate_tbwDKManager(): Activate driver now!!";
        //if(_BigSur) // do-activate
            std::system("open -W '/Applications/.KensingtonWorks-for-BigSur-above.app' --args --install");
        //else
        //    std::system("open -W /Applications/.KensingtonWorks.app --args --install");
    }
    else if(doActivateDriver == 2)
    {
        BOOST_LOG_TRIVIAL(info) << "DeviceManager::activate_tbwDKManager(): Force Activate driver now!!"; // Only for Big Sur
        std::system("open -W '/Applications/.KensingtonWorks-for-BigSur-above.app' --args --forceActivate");
    }
#if 0
    if([tmpStr containsString:(@"activated waiting to upgrade")]) // *&*&*&V1_20210129 這個是upgrade的狀況，但我不打算做upgrade...
    {
        // BOOST_LOG_TRIVIAL(info) << "DeviceManager::activate_tbwDKManager(): dont do anything"
        //                         << "because of activated waiting to upgrade: "<< [tmpStr UTF8String];
        // // 如果使用者一直沒activate 又升級到了macOS11
        // if(_BigSur)
        // {
        //     if([tmpStr containsString:(@"KensingtonWorks DriverKit driver 11")])
        //         ; // do nothing
        //     else
        //     {
        //         std::system("open -W /Applications/.KensingtonWorks.app --args --uninstall"); // 解除本來給10.15的安裝
        //         std::system("open -W /Applications/.KensingtonWorks-for-BigSur-above.app --args --install"); // 安裝給11用的
        //     }
        // }
    }
    else
    {
        // activate-it if nothing
        //if(![tmpStr containsString:(@"com.kensington.tbwDKDriver")] && !justInstalled )
        if(![tmpStr containsString:(@"com.kensington.tbwDKDriver")])
        {
            BOOST_LOG_TRIVIAL(info) << "DeviceManager::activate_tbwDKManager(): Not activated yet, status: "<< [tmpStr UTF8String];
            if(_BigSur)
                std::system("open -W '/Applications/.KensingtonWorks-for-BigSur-above.app' --args --install");
            else
                std::system("open -W /Applications/.KensingtonWorks.app --args --install");
        }
        else if ([tmpStr containsString:(@"terminated waiting to uninstall on reboot")]) // 看到被移除就試著activate(這不太可能發生，只有測試人員知道)
        {
            BOOST_LOG_TRIVIAL(info) << "DeviceManager::activate_tbwDKManager(): driver is just removed, status: "<< [tmpStr UTF8String];
            if(_BigSur)
                std::system("open -W '/Applications/.KensingtonWorks-for-BigSur-above.app' --args --install");
            else
                std::system("open -W /Applications/.KensingtonWorks.app --args --install");
        }
        else if ([tmpStr containsString:(@"activated waiting for user")]) // should not do anything but there is an exception case...
        {
            if(_BigSur)
            {
                if([tmpStr containsString:(@"KensingtonWorks DriverKit driver 11")])
                    ; // do nothing
                else
                {
                    std::system("open -W /Applications/.KensingtonWorks.app --args --uninstall"); // 解除本來給10.15的安裝
                    //std::system("open -W /Applications/.KensingtonWorks-for-BigSur-above.app --args --install"); // 安裝給11用的
                    tmpStr = [@"systemextensionsctl list | grep com.kensington.tbwDKDriver" runAsCommand]; // 再做一次
                    //if()
                }
                
            }
            else
            {

            }

        }
    }
#endif
}
// *&*&*&V1_20201120 This function is not-use because the driver-install-app will close itself after installation.
void DeviceManager::close_tbwDKManager()
{
    //[[[NSRunningApplication runningApplicationsWithBundleIdentifier:@"com.kensington.tbwdkmanager"] objectAtIndex:0] terminate];
#if 0
    OSStatus result = AEBuildAppleEvent( kCoreEventClass, kAEQuitApplication, typeProcessSerialNumber, &currentProcessPSN,
    sizeof(ProcessSerialNumber), kAutoGenerateReturnID, kAnyTransactionID, &tAppleEvent, &tAEBuildError,"");
    result = AESend( &tAppleEvent, &tReply, kAEAlwaysInteract+kAENoReply, kAENormalPriority, kNoTimeOut, nil, nil );
#else
    AppleEvent event = {typeNull, nil};
    const char *bundleIDString = [@"com.kensington.tbwdkmanager" UTF8String];

    OSStatus result = AEBuildAppleEvent(kCoreEventClass, kAEQuitApplication, typeApplicationBundleID, bundleIDString, strlen(bundleIDString), kAutoGenerateReturnID, kAnyTransactionID, &event, NULL, "");

    if (result == noErr) {
        result = AESendMessage(&event, NULL, kAEAlwaysInteract|kAENoReply, kAEDefaultTimeout);
        AEDisposeDesc(&event);
    }
    //return result == noErr;
#endif
}

// Active the DriverKit service ( only when DriverKit embedded in itself
void DeviceManager::activate()
{
    if (@available(macOS 10.15, *)) {
        @autoreleasepool {
          NSNumber *sysxOperation;
          sysxOperation = @(1);
            BOOST_LOG_TRIVIAL(info) << "DeviceManager::activate()+++++++++++++++++++++++++++++++++";
            if (sysxOperation) {
              BOOST_LOG_TRIVIAL(info) << "DeviceManager::activate() - IN sysxOperation";  
              NSString *e = @"com.kensington.tbwDKDriver";
              OSSystemExtensionRequest *req;
              dispatch_queue_t q = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0);
              if (sysxOperation.intValue == 1) {
                BOOST_LOG_TRIVIAL(info) << "Requesting SystemExtension activation";
                //NSLog(@"Requesting SystemExtension activation");
                req = [OSSystemExtensionRequest activationRequestForExtension:e queue:q];
              } else if (sysxOperation.intValue == 2) {
                  BOOST_LOG_TRIVIAL(info) << "DeviceManager::activate() - Requesting SystemExtension deactivation";
                NSLog(@"DeviceManager::activate() - Requesting SystemExtension deactivation");
                req = [OSSystemExtensionRequest deactivationRequestForExtension:e queue:q];
              }
              if (req) {
                BOOST_LOG_TRIVIAL(info) << "DeviceManager::activate() - in if-req";
                SNTSystemExtensionDelegate *ed = [[SNTSystemExtensionDelegate alloc] init];
                req.delegate = ed;
                [[OSSystemExtensionManager sharedManager] submitRequest:req];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, NSEC_PER_SEC * 5), q, ^{
#if 0//-----------------------------------------------------------Do nothing-------------------------------------------------------//
                    NSLog(@"extension request is dispatched");
                    DeviceManager::sHandleSecurityOpen(0);
#endif//-----------------------------------------------------------Do nothing-------------------------------------------------------//
                });
                [[NSRunLoop mainRunLoop] run];
//                });
               } else {
                   BOOST_LOG_TRIVIAL(info) << "cannot init SystemExention  request";
                   throw "cannot init SystemExention  request";
               }
            } else  {
                BOOST_LOG_TRIVIAL(warning)  << "this device's framework is under required  version";
            }
        }
        
    } else {
        BOOST_LOG_TRIVIAL(info)  << "this device's framework is under required version  10.15.4, do. not load tbwDKDriver";
    }
}

void deactivate()
{
    if (@available(macOS 10.15, *)) {
        OSSystemExtensionRequest *request = [OSSystemExtensionRequest deactivationRequestForExtension:@"com.kensington.tbwDKDriver" queue:dispatch_get_main_queue()];
        [[OSSystemExtensionManager sharedManager] submitRequest:request];
    } else {
          BOOST_LOG_TRIVIAL(warning)  << "this device's framework is under required  version, do not unload tbwDKDriver";
    }
}


//******* IMPLEMENTATION *******//

bool DeviceManager::init()
{
    CFDictionaryRef            matchingDict = NULL;
    CFRunLoopSourceRef        runLoopSource;
    CFRunLoopSourceContext  context = {0};
    kern_return_t            kr;
    int mainVer = 10;
    int subVer = 15;
    int minorVer = 7;

    try
    {
        NSOperatingSystemVersion osVer = [[NSProcessInfo processInfo] operatingSystemVersion];
        mainVer = (int)osVer.majorVersion;
        subVer = (int)osVer.minorVersion;
        minorVer = (int)osVer.patchVersion;

        BOOST_LOG_TRIVIAL(info) << "Current OS version:"<<mainVer<<"."<<subVer<<"."<<minorVer;
        
        if(mainVer >= 11 // Big Sur formal release
         || (mainVer == 10 && subVer >= 16) // *&*&*&Va_20200916, this should be Big Sur beta version
         )
        {
            _useDriverKit = true;
            _BigSur       = true;
        }
#if 0  // *&*&*&V1_20200903 test only
        else if(mainVer == 10 && subVer ==15 && minorVer >= 5)
#else
        else if(mainVer == 10 && subVer <= 15)
#endif
        {            
            NSFileManager *fm = [NSFileManager defaultManager];
            if (![fm fileExistsAtPath:@"/Library/Extensions/trackballworks2.kext"])
            {   //  沒有kext時尋找driverKit(debug only)
                BOOST_LOG_TRIVIAL(info) << "Using DriverKit!!";
                _useDriverKit = true;
            }
            else
            {
                BOOST_LOG_TRIVIAL(info) << "macOS 10.15.x and below. Using kext!!";
            }
        }

        if(_useDriverKit && _BigSur) // *&*&*&V1_20210428 if 10.15 activate manually(debug only)
        {
            BOOST_LOG_TRIVIAL(info) << "Taking DriverKit as handled driver.";
            this->activate_tbwDKManager();
        }
        // [Leo] 20200827
        char service [50];
        if(_useDriverKit) // *&*&*&V1_20200903
            strcpy(service, "tbwDKDriver");
        else strcpy(service, "TrackballWorks2Service");

        matchingDict = IOServiceNameMatching(service);
        if (!matchingDict) {
            goto bailout;
        }

        _notificationPort = IONotificationPortCreate(kIOMasterPortDefault);
        if (!_notificationPort) {
            goto bailout;
        }

        runLoopSource = IONotificationPortGetRunLoopSource(_notificationPort);
        CFRunLoopAddSource(CFRunLoopGetCurrent(), runLoopSource, kCFRunLoopDefaultMode);

        _rlRef = CFRunLoopGetCurrent();

        // create default config, if settings file is not found!
        _settings.reset(new Settings(true));
        
        context.info = this;
        context.perform = DeviceManager::sForegroundAppChanged;
        context.version = 0;
        _rlSourceForegroundApp = CFRunLoopSourceCreate(kCFAllocatorDefault, 0, &context);
        if (!_rlSourceForegroundApp)
            goto bailout;
        CFRunLoopAddSource(CFRunLoopGetCurrent(), _rlSourceForegroundApp, kCFRunLoopDefaultMode);

        context.info = this;
        context.perform = DeviceManager::sHandleSettingsChange;
        context.version = 0;
        _rlSourceSettingsChange = CFRunLoopSourceCreate(kCFAllocatorDefault, 0, &context);
        if (!_rlSourceSettingsChange)
            goto bailout;
        CFRunLoopAddSource(CFRunLoopGetCurrent(), _rlSourceSettingsChange, kCFRunLoopDefaultMode);

        // setup event tap to listen to change to modifier keys
        _modKeyListener.reset(
            new ModKeyListener(
                kCFRunLoopDefaultMode,
                DeviceManager::sOnModifierKeyStatusChange,
                this)
            );
        // _modKeyListener->enable();
        
        // setup foreground app monitor
        if (!pqrs_osx_frontmost_application_monitor_initialize(
                &_foregroundAppMonitor,
                DeviceManager::_handleFrontMostCallback,
                this)) {
            BOOST_LOG_TRIVIAL(error) << "Error initializing frontmost_application monitor";
            goto bailout;
        }

        kr = IOServiceAddMatchingNotification(_notificationPort,
                                              kIOFirstMatchNotification,
                                              matchingDict,
                                              DeviceManager::_onDeviceArrival,
                                              this,
                                              &_iter);

        onDeviceArrival(_iter, false);

        // *&*&*&V1_20201123
        json jresp = getDevices();
        std::string jstr = jresp.dump();
        tbwMshm::updateMshmDevices(jstr);
        
        // *&*&*&V1_20201201
        //[NSTimer scheduledTimerWithTimeInterval:5
        //                                 target:nil
        //                               selector:@selector(updateString)
        //                               userInfo:nil
        //                                repeats:YES];
        
        
        
        _inited.store(true);
                
        return true;
    }
    catch(std::exception& e)
    {
        BOOST_LOG_TRIVIAL(error) << "Error initializing device manager: " << e.what();
    }
    catch(...)
    {
        BOOST_LOG_TRIVIAL(error) << "Unknown error initializing device manager";
    }
bailout:
    if (_foregroundAppMonitor) pqrs_osx_frontmost_application_monitor_terminate(&_foregroundAppMonitor);
    _foregroundAppMonitor = nullptr;

    if (_rlSourceForegroundApp) {
        CFRunLoopRemoveSource(CFRunLoopGetCurrent(), _rlSourceForegroundApp, kCFRunLoopDefaultMode);
        CFRelease(_rlSourceForegroundApp);
    }
    _rlSourceForegroundApp = nullptr;

    _modKeyListener.reset();

    if (_rlSourceSettingsChange) {
        CFRunLoopRemoveSource(CFRunLoopGetCurrent(), _rlSourceSettingsChange, kCFRunLoopDefaultMode);
        CFRelease(_rlSourceForegroundApp);
    }
    _rlSourceSettingsChange = nullptr;

    _settings.reset();

    _rlRef = nullptr;
    if (_notificationPort) IONotificationPortDestroy(_notificationPort);
    _notificationPort = NULL;

    if (matchingDict) CFRelease(matchingDict);

    return false;
}

void DeviceManager::deinit()
{
    if (_foregroundAppMonitor) pqrs_osx_frontmost_application_monitor_terminate(&_foregroundAppMonitor);
    _foregroundAppMonitor = nullptr;

    if (_rlSourceForegroundApp) {
        CFRunLoopRemoveSource(CFRunLoopGetCurrent(), _rlSourceForegroundApp, kCFRunLoopDefaultMode);
        CFRelease(_rlSourceForegroundApp);
    }
    _rlSourceForegroundApp = nullptr;

    _modKeyListener.reset();

    if (_rlSourceSettingsChange) {
        CFRunLoopRemoveSource(CFRunLoopGetCurrent(), _rlSourceSettingsChange, kCFRunLoopDefaultMode);
        CFRelease(_rlSourceSettingsChange);
    }
    _rlSourceSettingsChange = nullptr;

    _settings.reset();

    _rlRef = nullptr;
    if (_notificationPort) IONotificationPortDestroy(_notificationPort);
    _notificationPort = NULL;
    //if(_useDriverKit) // *&*&*&Va_20200903
    //    deactivate();
}

// Iterates through the list of connected devices and returns the
// first device whose product id matches the given product id.
//
// TODO: what if there are two devices attached with the same
// product Id. Then we need to use the serial number as the
// identifying parameter.
Device* DeviceManager::getDevice(uint16_t productId)
{
    Device* ret = 0;
    
    for (DEVICEPTRLIST::iterator it=_devices.begin(); it!=_devices.end(); it++) {
        if ((*it)->id() == productId) {
            ret = dynamic_cast<Device*>((*it).get());
            break;
        }
    }
    return ret;
}

Device* DeviceManager::getFirstDevice()
{
    if (_devices.size() == 0)
        return NULL;
    
    return dynamic_cast<Device*>(_devices.front().get());
}

void DeviceManager::sMain(void* context, std::condition_variable& cv, kern_return_t& kr)
{
    reinterpret_cast<DeviceManager*>(context)->main(cv, kr);
}

void DeviceManager::main(std::condition_variable& cv, kern_return_t& kr)
{
    // worker thread body
    try {
  
        {
            std::lock_guard<std::mutex> lg(_mutex);
            kr = KERN_SUCCESS;

            // initialize
            if (!init()) {
                BOOST_LOG_TRIVIAL(error) << "Failed to init DeviceManager";
                kr = KERN_FAILURE;
            }
            
            cv.notify_one();
        }
        
        BOOST_LOG_TRIVIAL(info) << "Starting DeviceManager runloop";
        // start run loop
        CFRunLoopRun();
        BOOST_LOG_TRIVIAL(debug) << "DeviceManager runloop graceful exit";
        
        deinit();
        
    } catch (std::exception& e) {
        BOOST_LOG_TRIVIAL(error) << "DeviceManager worker - exception: " << e.what();
    } catch (...) {
        BOOST_LOG_TRIVIAL(error) << "DeviceManager worker - unknown exception";
    }
}

void DeviceManager::onDeviceRemoval(Device *driver)
{
    std::lock_guard<std::mutex> lg(_mutex);

    // find the Device object from our list
    for (DEVICEPTRLIST::iterator it=_devices.begin(); it!=_devices.end(); it++) {
        if (it->get() == driver) {
            
            BOOST_LOG_TRIVIAL(info) << "device removed: " << (*it)->name() << ", id: " << (*it)->id();
            
            _devices.erase(it);
        }
    }
    // *&*&*&V1_20201123
    json jresp = getDevices();
    std::string jstr = jresp.dump();
    tbwMshm::updateMshmDevices(jstr);

}

void DeviceManager::_onDeviceArrival(void *refCon, io_iterator_t iterator)
{
    assert(refCon);
    reinterpret_cast<DeviceManager*>(refCon)->onDeviceArrival(iterator, true);
}

void DeviceManager::onDeviceArrival(io_iterator_t iterator, bool acquireMutex)
{
    io_service_t		service = 0;
    BOOST_LOG_TRIVIAL(debug) << "Device arrival - onDeviceArrival------------------";
    if (acquireMutex)
        _mutex.lock();

    try {
        // Iterate over all matching objects
        while ((service = IOIteratorNext(iterator)) != 0)
        { // *&*&*&V1_20200903 called then device is plugged in
            try {
                BOOST_LOG_TRIVIAL(debug) << "Device arrival - instantiating device";
                DEVICEPTR device(new Device(this, service, this->_useDriverKit));
                _devices.push_back(device);
                
                _settings->createDefaultDeviceConfigIfDoesntExist(device->id());
                
                BOOST_LOG_TRIVIAL(debug) << "Device arrival - settings config";
                device->setConfig(
                    _settings->getDeviceConfig(
                        _foregroundAppBinary.c_str(),
                        device->id(),
                        true),
                    true);
                
                // *&*&*&V1_20201123
                json jresp = getDevices();
                std::string jstr = jresp.dump();
                tbwMshm::updateMshmDevices(jstr);
                std::cout << jstr << std::endl;
                BOOST_LOG_TRIVIAL(info) <<"-----in onDeviceArrival()-----"<< jstr;
                
            } catch (std::exception const& e) {
                BOOST_LOG_TRIVIAL(error) << "Error handling device arrival: " << e.what();
                IOObjectRelease(service);
            }
        }
    } catch (...) {
        if (acquireMutex)
        BOOST_LOG_TRIVIAL(debug) << "Device arrival - onDeviceArrival in catch----------";
            _mutex.unlock();
            throw;
    }
    
    // Remember to release mutex if acquired!
    if (acquireMutex)
        _mutex.unlock();
}

void DeviceManager::_handleFrontMostCallback(const char* bundle, const char* binary, uint32_t pid, void* context)
{
    reinterpret_cast<DeviceManager*>(context)->handleFrontMostCallback(bundle, binary, pid);
}

void DeviceManager::handleFrontMostCallback(const char* bundle, const char* binary, uint32_t pid)
{
    // notification comes in a separate thread! Cache the new foreground
    // app details, signal DeviceManager main thread and return.
    std::string binaryFilename(binary);
    std::string::size_type lastSlash = binaryFilename.rfind('/');
    if (lastSlash != std::string::npos)
        binaryFilename = binaryFilename.substr(lastSlash+1);
    
    BOOST_LOG_TRIVIAL(info)
        << "Foreground app changed - "
        << "binary: " << binaryFilename
        << ", bundle: " << bundle
        << ", pid: " << pid;
    
    if (!_inited.load()) {
        BOOST_LOG_TRIVIAL(info) << "\tskipping as class is still being initialized";
        return;
    }
    
    std::lock_guard<std::mutex> lg(_mutex);
    if (_foregroundAppBinary != binaryFilename || _foregroundAppBundle != bundle)
    {
        _foregroundPid = (uint64_t) pid;
        _foregroundAppBinary = binaryFilename;
        _foregroundAppBundle = bundle;
        
        CFRunLoopSourceSignal(_rlSourceForegroundApp);
        CFRunLoopWakeUp(_rlRef);
    }
}

// This (and the class method below) is the runloop handler for
// foreground-app-changed notification sent by the frontmost app
// change detector
void DeviceManager::sForegroundAppChanged(void* info)
{
    reinterpret_cast<DeviceManager*>(info)->foregroundAppChanged();
}

void DeviceManager::foregroundAppChanged()
{
    std::lock_guard<std::mutex> lg(_mutex);
    
    BOOST_LOG_TRIVIAL(info)
        << "foreground app changed - "
        << " binary: " << _foregroundAppBinary
        << ", bundle: " << _foregroundAppBundle
        << ", pid: " << _foregroundPid;

    std::string appConfigName = _foregroundAppBundle + "-" + _foregroundAppBinary;

    // vannes20200414 ToDo: Here I may launch the allow guide dialog
    
    
        BOOST_LOG_TRIVIAL(info)
            << L"\tapp config: " << appConfigName;

    // Find setting matching the device ID and current process filename
    // and pass it to the device.
    for (auto device: _devices)
    {
        device->setConfig(
            _settings->getDeviceConfig(
                appConfigName.c_str(),
                device->id(),
                true
            ),
            false
        );
    }

}

void DeviceManager::sHandleSettingsChange(void *info)
{
    reinterpret_cast<DeviceManager*>(info)->handleSettingsChange();
}

void DeviceManager::sHandleSecurityOpen(void *info)
{

    if (@available(macOS 10.15,  *)) {
        OSSystemExtensionRequest *request = [OSSystemExtensionRequest activationRequestForExtension:@"com.kensington.tbwDKDriver" queue:dispatch_get_main_queue()];
        SNTSystemExtensionDelegate *ed = [[SNTSystemExtensionDelegate alloc] init];
        request.delegate = ed;
        [[OSSystemExtensionManager sharedManager] submitRequest:request];
        [[OSSystemExtensionManager sharedManager] submitRequest:request];    BOOST_LOG_TRIVIAL(info) << "security open";
    }
}

void DeviceManager::handleSettingsChange()
{
    std::lock_guard<std::mutex> lg(_mutex);
    //tbwMshm::wHeartbeat(); 有呼叫到但沒用
    
    
    
    try
    {
        BOOST_LOG_TRIVIAL(debug) << "Settings file changed, reloading...";

        _settings->load(Settings::getFileFullPath().c_str());

        // Find setting matching the device ID and current process filename
        // and pass it to the device.
        for (auto& device: _devices)
        {
            device->setConfig(
                _settings->getDeviceConfig(
                    _foregroundAppBinary.c_str(),
                    device->id(),
                    true
                ),
                true
            );
        }
    }
    catch (BadSettingsFile bsf)
    {
        // Settings::reload() can throw! Catch it we must, but it's not 
        // necessary that we do any special handling. Perhaps log a message?
        BOOST_LOG_TRIVIAL(error)
            << L"Error reloading the settings file";
    }
    catch (...)
    {
        BOOST_LOG_TRIVIAL(error)
            << L"Unknown error while reloading the settings & initing device";
    }
}

void DeviceManager::sOnModifierKeyStatusChange(void* ref, uint32_t key, bool fDown)
{
    reinterpret_cast<DeviceManager*>(ref)->onModifierKeyStatusChange(key, fDown);
}

void DeviceManager::onModifierKeyStatusChange(uint32_t key, bool fDown)
{
    std::lock_guard<std::mutex> lg(_mutex);
    
    struct ModKeyDesc {
        uint32_t modKey;
        const char* name;
    } names[] = {
        { MAC_K_COMMAND, "Command" },
        { MAC_K_SHIFT, "Shift" },
        { MAC_K_OPTION, "Option" },
        { MAC_K_CTRL, "Ctrl" }
    };
    
    const char* keyName = nullptr;
    for (size_t i=0; i<sizeof(names)/sizeof(names[0]); i++) {
        if (names[i].modKey == key) {
            keyName = names[i].name;
            break;
        }
    }
    
    auto modifierStatus = _modKeyListener->getModifierStatus();

    //BOOST_LOG_TRIVIAL(debug)
    //    << "Modifier key: " << keyName
    //    << ", dir: " << (fDown ? "DOWN" : "UP")
    //    << ", modifier status: 0x" << std::hex << modifierStatus;
    
    
    for (auto& baseDevice: _devices)
    {
        try
        {
            Device* device = dynamic_cast<Device*>(baseDevice.get());

            auto lockedAxisModifiers = device->getConfig().getLockPointerAxisModifiersMask();
            bool fLockedAxisEnabled = device->isSingleAxisMovementEnabled();
            if (lockedAxisModifiers)
            {
                BOOST_LOG_TRIVIAL(debug) << "locked axis movmnt enabled, triger mod: 0x" << std::hex << lockedAxisModifiers;
                if (fLockedAxisEnabled && !(modifierStatus & lockedAxisModifiers))
                {
                    BOOST_LOG_TRIVIAL(info) << "Disabling locked axis pointer movement";
                    device->disableSingleAxisMovement();
                }
                else if (!fLockedAxisEnabled && (modifierStatus ^ lockedAxisModifiers) == 0)
                {
                    BOOST_LOG_TRIVIAL(info) << "Enabling locked axis pointer movement";
                    device->enableSingleAxisMovement();
                }
            }

            auto slowModifiers = device->getConfig().getSlowModifiersMask();
            bool fSlowPointerEnabled = device->isSlowPointerEnabled();

            if (slowModifiers)
            {
                BOOST_LOG_TRIVIAL(debug) << "slow pointer enabled, triger mod: 0x" << std::hex << slowModifiers;
                if (fSlowPointerEnabled && !(modifierStatus & slowModifiers))
                {
                    BOOST_LOG_TRIVIAL(info) << "Disabling slow pointer";
                    device->disableSlowPointer();
                }
                else if (!fSlowPointerEnabled && (modifierStatus ^ slowModifiers) == 0)
                {
                    BOOST_LOG_TRIVIAL(info) << "Enabling slow pointer";
                    device->enableSlowPointer();
                }
            }


            //&*&*&*G1_ADD
            if (slowModifiers && lockedAxisModifiers)
            {
                auto dualModifier = slowModifiers | lockedAxisModifiers;
                if ((modifierStatus == dualModifier) && (dualModifier != slowModifiers) && (dualModifier != lockedAxisModifiers))
                {
                    if (!fLockedAxisEnabled)
                    {
                        BOOST_LOG_TRIVIAL(info) << "Enabling locked axis pointer movement";
                        device->enableSingleAxisMovement();
                    }
                    if (!fSlowPointerEnabled)
                    {
                        BOOST_LOG_TRIVIAL(info) << "Enabling slow pointer";
                        device->enableSlowPointer();
                    }
                }

            }

            //&*&*&*G2_ADD
          
        } catch (std::exception& e) {
            BOOST_LOG_TRIVIAL(error) << "Error changing slow-pointer/locked-axis-movement status: " << e.what();
        }
    }
}

//------------ Device IMPLEMENTATION ------------//

class NotificationRegnException : public std::exception {
    
public:
    const char* what()
    { return "Error registering general notification on driver"; }
};

class MissingProductInfo : public std::exception {
public:
    const char* what()
    { return "Error retrieving product id/name information"; }
};

class DriverOpenError : public std::exception {
public:
    const char* what()
    { return "Error opening the driver"; }
};

class HelperRegistrationError : public std::exception {
public:
    const char* what()
    { return "Error registering as helper"; }
};

class AsyncRequestError : public std::exception {
public:
    const char* what()
    { return "Error queuing async IOCTL request"; }
};

Device::Device(DeviceManager* pMgr, io_service_t service, bool _useDriverKit)
: DeviceBase(0, 0, "", 0)
, _pDeviceMgr(pMgr)
, _service(service)
, _notification(IO_OBJECT_NULL)
, _connection(IO_OBJECT_NULL)
, _interfaceNumber(0)
{
    ::memset(&_event, 0, sizeof(_event));
    _Device_use_DriverKit = _useDriverKit; // *&*&*&Va_20200903
    // install a callback for driver state change notifications
    kern_return_t kr = ::IOServiceAddInterestNotification(pMgr->notificationPort(),
                                                          service,
                                                          kIOGeneralInterest,
                                                          Device::_onDeviceNotification,
                                                          this,
                                                          &_notification);
    if (kr != KERN_SUCCESS) {
        BOOST_LOG_TRIVIAL(error)
            << "Error registering for arrival of TrackballWorks2Service object, rc: 0x%08x" << kr;
        throw NotificationRegnException();
    }
    
    // open a connection to the driver
    kr = ::IOServiceOpen(service,
                         mach_task_self(),
                         0,
                         &_connection);
    if (kr != KERN_SUCCESS) {
        BOOST_LOG_TRIVIAL(error)
            << "Error opening the TrackballWorks2Service object, rc: 0x" << std::hex << kr;
        IOObjectRelease(_notification);
        throw DriverOpenError();
    }
    
    // TODO:
    TBWDEVICEINFO di = {0};
    BOOST_LOG_TRIVIAL(info)
    << "DeviceManger new - getDeviceInfo";
    if(_useDriverKit) // *&*&*&Va_20200903
    { 
        di.usProductId = 12345;
        di.usVenderId = 0x047D;
    }
    kr = getDeviceInfo(&di);
    if (kr != KERN_SUCCESS) {
        BOOST_LOG_TRIVIAL(error)
            << "Error querying device information, rc: 0x" << std::hex << kr;
        IOServiceClose(_connection);
        IOObjectRelease(_notification);
        throw HelperRegistrationError();
    }
    
    BOOST_LOG_TRIVIAL(info)
    << "device name - " << di.szName;

    
    kr = registerHelper();
    if (kr != KERN_SUCCESS) {
        BOOST_LOG_TRIVIAL(error)
            << "Error registering ourselves as the helper app, rc: 0x" << std::hex << kr;
        IOServiceClose(_connection);
        IOObjectRelease(_notification);
        throw HelperRegistrationError();
    }
    
    //&*&*&*G1_ADD
    if(_useDriverKit) // *&*&*&_Va_20200903
    {
        _asyncMemBlock = 0;
        mach_vm_size_t size = 0;
        kr = IOConnectMapMemory64(_connection, 0 /*memoryType*/, mach_task_self(), &_asyncMemBlock, &size, kIOMapAnywhere);
    
        if(kr != KERN_SUCCESS)
        {
            BOOST_LOG_TRIVIAL(error)
                << "Error create share memory for async events, rc: 0x" << std::hex << kr;
            //IOServiceClose(_connection);
            //IOObjectRelease(_notification);
            //throw HelperRegistrationError();
        }
    
    //test_1
        TBWDRIVEREVENT *pev = (TBWDRIVEREVENT *)_asyncMemBlock;
        BOOST_LOG_TRIVIAL(info)
    << "share memory for async events created 0x" << std::hex << pev->time
                << " adress 0x" << std::hex << _asyncMemBlock;
    pev->time = 0x5a5a;
    //test_2
        //&*&*&*G2_ADD
    }
    
    
    kr = readNextEventAsync();
    if (kr != KERN_SUCCESS) {
        BOOST_LOG_TRIVIAL(error)
            << "Error queuing aync read, rc: 0x" << std::hex << kr;
        ::IOServiceClose(_connection);
        IOObjectRelease(_notification);
        throw AsyncRequestError();
    }

    // initialize base class members
    DeviceBase::init((uint16_t)di.usProductId, (uint16_t)di.usVersion, di.szName);

    //_id = di.usProductId;
    //_firmwareVersion = di.usVersion;
    //_name = di.szName;
    
    BOOST_LOG_TRIVIAL(info)
        << "device arrived: " << name()
        << ", id: " << id()
        << ", realId: " << realId()
        << ", firmwareVersion: " << std::hex << _firmwareVersion;
}

Device::~Device()
{
    BOOST_LOG_TRIVIAL(info) << "~Device - id: " << id();
    
    deregisterHelper();
    
    ::IOServiceClose(_connection);
    ::IOObjectRelease(_notification);
    ::IOObjectRelease(_service);
}

void Device::enableSlowPointer()
{
    assert(_connection != IO_OBJECT_NULL);
    
    TBWSLOWPOINTERPARAMS args = { 1, 0 };
    auto ret = IOConnectCallStructMethod(_connection,
                                         kKWDriverSetSlowPointer,
                                         &args,
                                         sizeof(args),
                                         NULL,
                                         0);
    if (ret != KERN_SUCCESS)
        throw_os_error(ret);
    
}

void Device::disableSlowPointer()
{
    assert(_connection != IO_OBJECT_NULL);
    TBWSLOWPOINTERPARAMS args = { 0, 0 };
    auto ret = IOConnectCallStructMethod(_connection,
                                         kKWDriverSetSlowPointer,
                                         &args,
                                         sizeof(args),
                                         NULL,
                                         0);
    if (ret != KERN_SUCCESS)
        throw_os_error(ret);
    
}

bool Device::isSlowPointerEnabled()
{
    assert(_connection != IO_OBJECT_NULL);
    
    TBWSLOWPOINTERPARAMS args = { 0, 0 };
    size_t outLen = sizeof(args);
    auto ret = IOConnectCallStructMethod(_connection,
                                         kKWDriverGetSlowPointer,
                                         NULL,
                                         0,
                                         &args,
                                         &outLen);
    if (ret != KERN_SUCCESS)
        throw_os_error(ret);
    
    return args.enable != 0;
}

void Device::enableSingleAxisMovement()
{
    assert(_connection != IO_OBJECT_NULL);
    
    TBWSINGLEAXISMOVEMENTPARMS param = { 1, 0 };
    auto ret = IOConnectCallStructMethod(_connection,
                                         kKWDriverSetTBWSINGLEAXISMOVEMENTPARMS,
                                         &param,
                                         sizeof(param),
                                         NULL,
                                         0);
    if (ret != KERN_SUCCESS)
        throw_os_error(ret);
}

void Device::disableSingleAxisMovement()
{
    assert(_connection != IO_OBJECT_NULL);
    
    TBWSINGLEAXISMOVEMENTPARMS param = { 0, 0 };
    auto ret = IOConnectCallStructMethod(_connection,
                                         kKWDriverSetTBWSINGLEAXISMOVEMENTPARMS,
                                         &param,
                                         sizeof(param),
                                         NULL,
                                         NULL);
    if (ret != KERN_SUCCESS)
        throw_os_error(ret);
}

bool Device::isSingleAxisMovementEnabled()
{
    assert(_connection != IO_OBJECT_NULL);
    
    TBWSINGLEAXISMOVEMENTPARMS param = { 0, 0 };
    size_t outLen = sizeof(param);
    auto ret = IOConnectCallStructMethod(_connection,
                                         kKWDriverGetTBWSINGLEAXISMOVEMENTPARMS,
                                         NULL,
                                         0,
                                         &param,
                                         &outLen);
    if (ret != KERN_SUCCESS)
        throw_os_error(ret);
    
    return param.enable != 0;
}

void Device::onDeviceNotification(natural_t messageType, void* messageArgument)
{
    switch (messageType)
    {
        case kIOMessageServiceIsTerminated:
            _pDeviceMgr->onDeviceRemoval(this);
            break;
    }
}

void Device::_onDeviceNotification(void* refCon, io_service_t service, natural_t messageType, void* messageArgument)
{
    assert(refCon);
    reinterpret_cast<Device*>(refCon)->onDeviceNotification(messageType, messageArgument);
}

kern_return_t Device::readNextEventAsync()
{
    io_async_ref64_t    asyncRef;
    uint64_t            scalarIn[3];
    
    asyncRef[kIOAsyncCalloutFuncIndex] = (uint64_t)Device::sNextEventCallback;
    asyncRef[kIOAsyncCalloutRefconIndex] = (uint64_t)this;
    
    scalarIn[0] = (uint64_t) sizeof(TBWDRIVEREVENT);
    ::memset(&_event, 0, sizeof(TBWDRIVEREVENT));
    _event.cb = sizeof(TBWDRIVEREVENT);
    scalarIn[1] = (uint64_t)&_event;
    
    //&*&*&*G1_ADD
    if(this->_Device_use_DriverKit) // *&*&*&Va_20200903
    {
        //if(_asyncMemBlock != 0)
        //    scalarIn[2] = _asyncMemBlock;
    
        // for debug
        _event.time = 0x5a5a;
    
        if(_asyncMemBlock != 0)
        {
            TBWDRIVEREVENT *pev = (TBWDRIVEREVENT *) _asyncMemBlock;
            pev->time = 0x5a5a;
        }

        BOOST_LOG_TRIVIAL(info) << "readNextEventAsync - 0x05a5a "
        << " scalarIn[1]: " << std::hex << scalarIn[1];
    }    
    
    
    auto ret = IOConnectCallAsyncScalarMethod(
        _connection,
        kKWDriverGetNextEventAsync,
        IONotificationPortGetMachPort(_pDeviceMgr->notificationPort()),
        asyncRef,
        kIOAsyncCalloutCount,
        scalarIn,
        //this->_Device_use_DriverKit? 3 : 2,//3,  //2->3
        2,
        NULL,
        NULL);

    if (ret == kIOReturnSuccess) {
        BOOST_LOG_TRIVIAL(error) << "Async timer installed";
    } else {
        BOOST_LOG_TRIVIAL(error) << "Error installing async timer callback: " << ret;
    }
    return ret;
}

void Device::sNextEventCallback(void *refcon, IOReturn result)
{
    // BOOST_LOG_TRIVIAL(info) << "Device::timerCallback - result: " << result;
    if (refcon == (void*)0xdeadbeef) {
        BOOST_LOG_TRIVIAL(error) << "Device::nextEventCallback - stopping run loop";
        CFRunLoopStop(CFRunLoopGetMain());
    }
    Device* pDevice = reinterpret_cast<Device*>(refcon);
    pDevice->nextEventCallback(result);
}

void Device::nextEventCallback(IOReturn result)
{
    std::lock_guard<std::mutex> lg(_pDeviceMgr->_mutex);
    
    //&*&*&*G1_ADD
    if(this->_Device_use_DriverKit) // *&*&*&Va_20200903
    {
        if(_asyncMemBlock != 0)
            memcpy(&_event, (void *)_asyncMemBlock, sizeof(TBWDRIVEREVENT));
    }
    //&*&*&*G2_ADD

    BOOST_LOG_TRIVIAL(info) << "nextEventCallback -"
        << " buttonDownMask: " << std::hex << _event.buttonDownMask
        << ", buttonUpMask: " << std::hex << _event.buttonUpMask;
    
     if (_pConfig)
    {
        _pConfig->handleButtonActivity(this, _event.buttonDownMask, _event.buttonUpMask);
    }
    else
    {
        BOOST_LOG_TRIVIAL(warning)
            << L"Notification from kernel for: " << name() << L", but no config defined!";
    }
    
    readNextEventAsync();
}

kern_return_t Device::getDeviceInfo(TBWDEVICEINFO* pDevInfo)
{
    size_t cbDevInfo = sizeof(TBWDEVICEINFO);
    if(this->_Device_use_DriverKit) // *&*&*&_Va_20200903
    {
        return IOConnectCallStructMethod(
            _connection,
            kKWDriverGetDeviceInfo,
            pDevInfo,
            cbDevInfo,
            pDevInfo,
            &cbDevInfo);
    }
    else
    {
        return IOConnectCallStructMethod(
            _connection,
            kKWDriverGetDeviceInfo,
            NULL,
            0,
            pDevInfo,
            &cbDevInfo);
    }
}

kern_return_t Device::registerHelper()
{
    uint64_t scalarIn[1];
    scalarIn[0] = { (uint64_t)::getpid() };

    return IOConnectCallScalarMethod(
        _connection,
        kKWDriverRegisterHelper,
        scalarIn,
        1,
        NULL,
        NULL);
}

kern_return_t Device::deregisterHelper()
{
    return IOConnectCallScalarMethod(
        _connection,
        kKWDriverDeregisterHelper,
        NULL,
        0,
        NULL,
        NULL);
}

void Device::setButtonsConfig(TBWBUTTONSCONFIG *pConfig)
{
    assert(_connection != IO_OBJECT_NULL);
    auto ret = IOConnectCallStructMethod(
        _connection,
        kKWDriverSetButtonsConfig,
        pConfig,
        sizeof(TBWBUTTONSCONFIG),
        NULL,
        0);
    if (ret != KERN_SUCCESS)
        throw_os_error(ret);
}

TBWBUTTONSCONFIG Device::getButtonsConfig()
{
    auto& actions = _pConfig->getActions();
    TBWBUTTONSCONFIG buttonsCfg = { 0 };
    buttonsCfg.cbSize = sizeof(TBWBUTTONSCONFIG);
    buttonsCfg.chordMask = _pConfig->getChordsMask();

    _pConfig->getChordMasks(buttonsCfg.chordMsks); // vv0427

    uint32_t index = 0;
    for (auto& a : actions)
    {
        ACTIONPTR& action = a.second;
        if (action->name() == "singleClick")
        {
            ActionSingleClick* pAction = dynamic_cast<ActionSingleClick*>(action.get());
            //if (pAction->_modifiers == 0)
            {
                TBWBUTTONACTION& action = buttonsCfg.buttons[index];
                action.action = BUTTON_ACTION_CLICK;
                action.buttonMask = a.first;
                // pAction->_button is a sequential button number startingat 1.
                // That is 1 - left button, 2 - right button, 3 - middle button, etc.
                // To translate this into our device specific button bit mask we need
                // to subtract one from it and then use the value to generate the
                // bitmask.
                action.u.ClickActionParams.buttonMask = (0x01 << (pAction->_button - 1));
                action.u.ClickActionParams._modifiers = pAction->_modifiers;
                index++;
            }
        }
        else if (action->name() == "scroll")
        {
            ActionScroll* pAction = dynamic_cast<ActionScroll*>(action.get());
            TBWBUTTONACTION& action = buttonsCfg.buttons[index];
            action.action = BUTTON_ACTION_SCROLL;
            action.buttonMask = a.first;
            switch(pAction->_direction) {
            case SD_UP: action.u.ScrollActionParams.direction = SCROLL_DIRECTION_UP; break;
            case SD_DOWN: action.u.ScrollActionParams.direction = SCROLL_DIRECTION_DOWN; break;
            case SD_LEFT: action.u.ScrollActionParams.direction = SCROLL_DIRECTION_LEFT; break;
            case SD_RIGHT: action.u.ScrollActionParams.direction = SCROLL_DIRECTION_RIGHT; break;
            // *&*&*&V1_ADD_SCROLL_WITH_TRACK, 20200309
            case SD_TRACKBALL: action.u.ScrollActionParams.direction = SCROLL_DIRECTION_TRACK; break;
            // *&*&*&V2_ADD_SCROLL_WITH_TRACK, 20200309
            case SD_TRACKBALL_AUTO: action.u.ScrollActionParams.direction = SCROLL_DIRECTION_AUTO; break; // *&*&*&Va_0200518
            default: break;
            }
            action.u.ScrollActionParams.lines = pAction->_lines;
            index++;
        }
        else if (action->name() == "drag")
        {
            ActionDrag* pAction = dynamic_cast<ActionDrag*>(action.get());
            //if (pAction->_modifiers == 0)
            {
                TBWBUTTONACTION& action = buttonsCfg.buttons[index];
                action.action = BUTTON_ACTION_DRAG;
                action.buttonMask = a.first;
                // pAction->_button is a sequential button number startingat 1.
                // That is 1 - left button, 2 - right button, 3 - middle button, etc.
                // To translate this into our device specific button bit mask we need
                // to subtract one from it and then use the value to generate the
                // bitmask.
                action.u.ClickActionParams.buttonMask = (0x01 << (pAction->_button - 1));
                action.u.ClickActionParams._modifiers = pAction->_modifiers;
                index++;
            }
        }
    }
    buttonsCfg.nButtons = index;

#if 1
    BOOST_LOG_TRIVIAL(debug) << "Button config:-";
    BOOST_LOG_TRIVIAL(debug) << "\tchord Mask: 0x" << std::hex << buttonsCfg.chordMask
        << ", buttons: " << buttonsCfg.nButtons;
    for (size_t i = 0; i < buttonsCfg.nButtons; i++)
    {
        TBWBUTTONACTION& btnAction = buttonsCfg.buttons[i];
        BOOST_LOG_TRIVIAL(debug) << "\t" << i + 1
            << ", IN mask: " << std::hex << btnAction.buttonMask;
        if (btnAction.action == BUTTON_ACTION_CLICK)
        {
            BOOST_LOG_TRIVIAL(debug) << "\tCLICK - OUT mask: 0x"
                << std::hex << btnAction.u.ClickActionParams.buttonMask
                << "\tmodifier: 0x"
                << std::hex << btnAction.u.ClickActionParams._modifiers;
        }
        else if (btnAction.action == BUTTON_ACTION_SCROLL)
        {
            BOOST_LOG_TRIVIAL(debug) << "\tSCROLL - direction: " << btnAction.u.ScrollActionParams.direction
                << ", lines: " << btnAction.u.ScrollActionParams.lines;
        }
        else if (btnAction.action == BUTTON_ACTION_DRAG)
        {
            BOOST_LOG_TRIVIAL(debug) << "\tDRAG - OUT mask: 0x"
                << std::hex << btnAction.u.ClickActionParams.buttonMask
                << "\tmodifier: 0x"
                << std::hex << btnAction.u.ClickActionParams._modifiers;
        }
    }
#endif

    return buttonsCfg;
}

void Device::setScrollParams(int wheel, double speed, bool invert, bool inertial)
{
    TBWSCROLLWHEELPARAMS params = {0};
    params.wheel = WHEEL_VERT;
    params.speed = speed;
    params.invert = invert ? 1 : 0;
    params.inertial = inertial ? 1 : 0;
    
    BOOST_LOG_TRIVIAL(debug) << "setScrollParams - "
        << "speed: " << speed
        << ", invert: " << invert
        << ", inertial: " << inertial;
    auto ret = IOConnectCallStructMethod(
                                         _connection,
                                         kKWDriverSetScrollWheelParams,
                                         &params,
                                         sizeof(TBWSCROLLWHEELPARAMS),
                                         NULL,
                                         0);
    if (ret != KERN_SUCCESS)
        throw_os_error(ret);
}

void Device::getScrollParams(int wheel, double& speed, bool& invert, bool& inertial)
{
    TBWSCROLLWHEELPARAMS params = {0};
    params.wheel = wheel;
    size_t cbParams = sizeof(TBWSCROLLWHEELPARAMS);
    auto ret = IOConnectCallStructMethod(
                                         _connection,
                                         kKWDriverSetScrollWheelParams,
                                         NULL,
                                         0,
                                         &params,
                                         &cbParams);
    if (ret != KERN_SUCCESS)
        throw_os_error(ret);
    
    speed = params.speed;
    invert = params.invert ? true : false;
    inertial = params.inertial ? true : false;
}

void Device::setPointerParams(double speed, uint64_t acceleration)
{
    TBWPOINTERPARAMS pp = {0};
    pp.flags = POINTER_PARAM_SPEED;
    pp.speed = speed;

    BOOST_LOG_TRIVIAL(debug) << "setPointerParams - "
        << "speed: " << speed
        <<", acceleration: " << acceleration;
    auto ret = IOConnectCallStructMethod(
                                         _connection,
                                         kKWDriverSetPointerParams,
                                         &pp,
                                         sizeof(pp),
                                         NULL,
                                         0);
    if (ret != KERN_SUCCESS) {
        throw_os_error(ret);
    }
    // acceleration can be set by setting HIDPointerAcceleration
    // HID parameter
    setPointerAccelerationNormalized((uint32_t)acceleration);
}

void Device::getPointerParams(double& speed, uint64_t& acceleration)
{
    TBWPOINTERPARAMS pp = {0};
    pp.flags = POINTER_PARAM_SPEED;
    size_t cbParams = sizeof(pp);
    auto ret = IOConnectCallStructMethod(
                                         _connection,
                                         kKWDriverSetPointerParams,
                                         NULL,
                                         0,
                                         &pp,
                                         &cbParams);
    if (ret != KERN_SUCCESS)
        throw_os_error(ret);
    
    speed = pp.speed;
    acceleration = getPointerAccelerationNormalized();
    
    BOOST_LOG_TRIVIAL(debug) << "getPointerParams - "
        << "speed: " << speed
        <<", acceleration: " << acceleration;
}

bool Device::postKeystroke(uint32_t modifiers, uint32_t keyCode, bool fDown, bool fModifiersOnly)
{
    // Let the PlatformServices.Impl handle special keys
    if (modifiers & MAC_K_SPECIAL)
        return false;
    
    TBWPOSTKEYSTROKE pks = {0};

    uint64_t index = 0;

    if (!fModifiersOnly && !fDown) {
        pks.keystrokes[index].keyCode = keyCode;
        pks.keystrokes[index++].down = fDown;
    }
    
    if (modifiers & MAC_K_COMMAND) {
        pks.keystrokes[index].keyCode = kVK_Command;
        pks.keystrokes[index++].down = fDown;
    }
    if (modifiers & MAC_K_FN) {
        pks.keystrokes[index].keyCode = kVK_Function;
        pks.keystrokes[index++].down = fDown;
    }
    if (modifiers & MAC_K_CTRL) {
        pks.keystrokes[index].keyCode = kVK_Control;
        pks.keystrokes[index++].down = fDown;
    }
    if (modifiers & MAC_K_OPTION) {
        pks.keystrokes[index].keyCode = kVK_Option;
        pks.keystrokes[index++].down = fDown;
    }
    if (modifiers & MAC_K_SHIFT) {
        pks.keystrokes[index].keyCode = kVK_Shift;
        pks.keystrokes[index++].down = fDown;
    }

    if (!fModifiersOnly && fDown) {
        pks.keystrokes[index].keyCode = keyCode;
        pks.keystrokes[index++].down = fDown;
    }
    pks.count = index;
    
    BOOST_LOG_TRIVIAL(debug) << "posting keystrokes to driver, modifiers: 0x"
        << std::hex << modifiers
        << ", keyCode: " << std::hex << keyCode
        << ", total # keystrokes: " << index;
    
    auto kr = IOConnectCallStructMethod(
                                        _connection,
                                        kKWDriverPostKeystroke,
                                        &pks,
                                        sizeof(pks),
                                        NULL,
                                        NULL);
    
    if (kr != KERN_SUCCESS) {
        BOOST_LOG_TRIVIAL(error) << "error posting keystrokes to driver: " << kr;
        return false;
        //throw_os_error(kr);
    }

    return true;
}

bool Device::postPointerClick(uint32_t button, bool fDown)
{
    TBWPOSTPOINTERCLICK ppc = {0};
    if (fDown) {
        ppc.button = button;
    }
    
    BOOST_LOG_TRIVIAL(debug) << "posting pointer click to driver - button: "
        << std::hex << button
        << ", dir: " << (fDown ? "DN" : "UP");
    
    auto kr = IOConnectCallStructMethod(
                                        _connection,
                                        kKWDriverPostPointerClick,
                                        &ppc,
                                        sizeof(ppc),
                                        NULL,
                                        NULL);
    
    if (kr != KERN_SUCCESS) {
        BOOST_LOG_TRIVIAL(error) << "error posting pointer clicks to driver: " << kr;
        return false;
        //throw_os_error(kr);
    }

    return true;
}

void Device::updatePointerParams()
{
    // if this configuration has defined SlowPointer or LockedAxis
    // hotkeys, enable ModKeyListener
    if (_pConfig->getSlowModifiersMask() || _pConfig->getLockPointerAxisModifiersMask()) {
        BOOST_LOG_TRIVIAL(debug) << "Enabling modifier key listener";
        _pDeviceMgr->_modKeyListener->enable();
    } else {
        if (_pDeviceMgr->_modKeyListener->isEnabled()) {
            BOOST_LOG_TRIVIAL(debug) << "Disabling modifier key listener";
            _pDeviceMgr->_modKeyListener->disable();
        }
    }

    BOOST_LOG_TRIVIAL(debug) << "updatePointerParams - speed: " << _pConfig->pointerSpeed()
        << ", acceleration: " << _pConfig->pointerAcceleration()
        << ", acceleration rate: " << _pConfig->pointerAccelerationRate();
    // Convert settings pointer speed from 0-100 to
    // double value ranging from 0.1 ~ 2.0
    const double POINTER_SPEED[] = {
        0.2, 0.4, 0.6, 0.8, 0.9, 1.0, 1.5, 2.0, 2.5, 3.0, 4.0
    };
    size_t index = (size_t) std::round((double)_pConfig->pointerSpeed()/10.0);
    index = std::min(std::max(index, (size_t)0), (size_t)10);
    double pointerSpeed = POINTER_SPEED[index];
    //double pointerSpeed = (double)_pConfig->pointerSpeed() / 50.0;
    //pointerSpeed = std::min(std::max(pointerSpeed, 0.2), 2.0);

    // Convert pointer acceleration from 0-100 to
    // the range 0 - 196608 (this is MacOS range).
    uint64_t pointerAccelerationRate = _pConfig->pointerAcceleration() ? _pConfig->pointerAccelerationRate() : 0;
    setPointerParams(
        pointerSpeed,
        pointerAccelerationRate
        );
}

void Device::updateScrollWheelParams()
{
    // convert scroll speed from 0-100 scale to float 1.0 ~ 10.0
    double speed = 1.0;
    speed = (double)_pConfig->scrollSpeed() / 10.0;
    speed = std::min(std::max(speed, 1.0), 10.0);
    setScrollParams(
        WHEEL_VERT,
        speed,
        _pConfig->invertScroll(),
        _pConfig->inertialScroll()
        );
}

void Device::updateButtonsConfig()
{
    auto buttonsCfg = getButtonsConfig();
    setButtonsConfig(&buttonsCfg);
}

#ifdef TAU
void Device::emulateButtonClick(uint32_t mask)
{
    assert(_connection != IO_OBJECT_NULL);
    
    TBWEMULATEDEVICEACTION action = { 0 };
    action.cbSize = sizeof(action);
    action.uAction = DA_BUTTONCLICK;
    action.u.ButtonClick.mask = mask;
    auto ret = IOConnectCallStructMethod(_connection,
                                         kKWDriverEmulateDeviceAction,
                                         &action,
                                         sizeof(action),
                                         NULL,
                                         0);
    if (ret != KERN_SUCCESS)
        throw_os_error(ret);
}
#endif
