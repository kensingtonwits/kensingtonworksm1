#include "Migrate.h"
#include <map>
#include <string>
#include <vector>

#include <boost/filesystem.hpp>
namespace bfs = boost::filesystem;

#include "Plist.hpp"
#include "../../shared/Settings.h"

using namespace std;
/**
    TODO:
        migrate scroll speed?
        migrate pointer speed?
        verify all migrations
        snippets - populate it across all device/app configs
 */

const uint32_t NO_ACTION = 0;
const uint32_t MOUSE_AND_KEYBOARD_KEYBOARD_SHORTCUT = 10;
const uint32_t MOUSE_AND_KEYBOARD_CLICK = 1;
const uint32_t MOUSE_AND_KEYBOARD_RIGHT_CLICK = 2;
const uint32_t MOUSE_AND_KEYBOARD_MIDDLE_CLICK = 3;
const uint32_t MOUSE_AND_KEYBOARD_DRAG = 14;
const uint32_t MOUSE_AND_KEYBOARD_RIGHT_DRAG = 15;
const uint32_t MOUSE_AND_KEYBOARD_MIDDLE_DRAG = 16;
const uint32_t MOUSE_AND_KEYBOARD_DOUBLE_CLICK = 12;
const uint32_t MOUSE_AND_KEYBOARD_DOUBLE_RIGHT_CLICK = 17;
const uint32_t MOUSE_AND_KEYBOARD_DOUBLE_MIDDLE_CLICK = 18;
const uint32_t MOUSE_AND_KEYBOARD_TRIPLE_CLICK = 13;

/**
 * Utility function to expand ~ to its full path.
 */
bfs::path expandHomeFolder(bfs::path in) {
    if (in.size () < 1) return in;

    const char * home = getenv ("HOME");
    if (home == NULL) {
      //cerr << "error: HOME variable not set." << endl;
      throw std::invalid_argument ("error: HOME environment variable not set.");
    }

    string s = in.c_str ();
    if (s[0] == '~') {
      s = string(home) + s.substr (1, s.size () - 1);
      return bfs::path (s);
    } else {
      return in;
    }
}

SettingsMigrator::SettingsMigrator()
     /*: MigratorBase()*/
{

}

SettingsMigrator::~SettingsMigrator()
{

}

void SettingsMigrator::migrate()
{
    map<string, boost::any> dict;
    Plist::readPlist(Settings::getLegacyFileFullPath().c_str(), dict);
    
    const char* requiredDicts[] = {
        "devices", "snippets"
    };
    // first make sure the settings has all the required nodes
    for (auto& key: requiredDicts) {
        if (dict.find(key) == dict.end()) {
            return;
        }
    }

    // convert snippets into a vector of strings
    auto devices = dict.find("devices")->second;
    auto arSnippets = boost::any_cast<ANYARRAY>(dict.find("snippets")->second);
    for (auto const& item: arSnippets) {
        STRING2ANYTABLE snippetInfo = boost::any_cast<STRING2ANYTABLE>(item);
        auto label = boost::any_cast<string>(snippetInfo.find("name")->second);
        auto content = boost::any_cast<string>(snippetInfo.find("text")->second);
        _snippets.push_back({label, content});
    }

    // Make sure _snippets array has 10 elements!
    if (_snippets.size() < 10) {
        for (size_t i=_snippets.size(); i<=10; i++) {
            _snippets.push_back({"", ""});
        }
    }

    Settings settings;
    migrateDevices(devices, settings);
    
    settings.save();
}

void SettingsMigrator::migrateDevices(const boost::any& devices, Settings& settings)
{
    ANYARRAY deviceList = boost::any_cast<ANYARRAY>(devices);
    
    for (auto& device: deviceList) {
        STRING2ANYTABLE deviceSettings = boost::any_cast<STRING2ANYTABLE>(device);
        migrateDevice(deviceSettings, settings);
    }
}

void SettingsMigrator::migrateDevice(const STRING2ANYTABLE& deviceSettings, Settings& settings)
{
    const char* requiredDicts[] = {
        "target device", "buttons", "options", "target app", "settings name"
    };
    // first make sure the settings has all the required nodes
    for (auto& key: requiredDicts) {
        if (deviceSettings.find(key) == deviceSettings.end()) {
            return;
        }
    }

    try {
        const STRING2ANYTABLE& devInfo = boost::any_cast<STRING2ANYTABLE>(
            deviceSettings.find("target device")->second);
        const STRING2ANYTABLE& options = boost::any_cast<STRING2ANYTABLE>(
            deviceSettings.find("options")->second);
        const STRING2ANYTABLE& buttonsInfo = boost::any_cast<STRING2ANYTABLE>(
            deviceSettings.find("buttons")->second);
        const STRING2ANYTABLE& appInfo = boost::any_cast<STRING2ANYTABLE>(
            deviceSettings.find("target app")->second);
        const auto& appName = boost::any_cast<string>(
            deviceSettings.find("settings name")->second);

        // get device id
        if (devInfo.find("device product id") == devInfo.end()) {
            return;
        }
        auto deviceId = (unsigned) boost::any_cast<int64_t>(devInfo.find("device product id")->second);
        if (deviceId == 0) {   // ignore device id 0
            return;
        }
        BOOST_LOG_TRIVIAL(debug) << "Device Id = " << deviceId << ", app = " << appName;
        
        // create global app device config
        settings.createDefaultDeviceConfigIfDoesntExist(deviceId);

        // parse target app
        string appId = "*"; // All Applications by default
        string appBundleId = boost::any_cast<string>(appInfo.find("app bundle id")->second);
        string appExecutable = boost::any_cast<string>(appInfo.find("app executable")->second);
        
        if (appBundleId.length() > 0 && appExecutable.length() > 0) {
            appId = appBundleId + "-" + appExecutable;
            // create the app if it does not exist
            settings.createApp(deviceId, appName.c_str(), appId.c_str());
       }
 
       // migrate device options such as acceleration, inertial scroll, etc
       DeviceConfig& dc = settings.getDeviceConfig(appId.c_str(), deviceId, false);
       migrateDeviceOptions(options, dc);
       migrateDeviceButtons(buttonsInfo, dc);
       
       // store all snippets into device snippets table
       for (size_t i=0; i<_snippets.size() && i < 10; i++) {
            auto const& snippet = _snippets[i];
            dc.addSnippetString(snippet.content, snippet.label, (int)i);
       }
        
    } catch (std::exception const& e) {
      BOOST_LOG_TRIVIAL(warning) << "Exception while migrating device: \""<< e.what() << "\", ignoring device..";
    }
}

void SettingsMigrator::migrateDeviceOptions(const STRING2ANYTABLE& options, DeviceConfig& dc)
{
    /*
    Options dict looks like this
    {
        'accelerate' = True,
        // Pointer acceleration, ranges from 0.0 to 2.0
        'acceleration' = 1.0,
        'inertial scroll' = False,
        'invert wheel' = False,
        'invert x' = False,
        'invert y' = False,
        'jump' = False,
        'single' = False,
        'single keys' = 0,
        'slow' = False,
        'slow keys' = 0,
        // Pointer speed, ranges from 5000(slowest) - 80 (fastest)
        [5000, 4000, 3000, 2000, 1000, 900, 800, 700, 600, 500, 400, 360, 330, 300, 260, 230, 200, 160, 130, 100, 80]
        'speed' = 400,
        'swap xy' = False
     }
     
     Modifier Keys:
        256 = Cmd
        2048 = Option
        512 = Shift
        4096 = Ctrl
    */
    
    int64_t pointerSpeed = boost::any_cast<int64_t>(options.find("speed")->second);
    // Predefined pointer settings
    int64_t pointerSpeedValues[] = {
        5000, 4000, 3000, 2000, 1000, 900, 800, 700, 600, 500, 400, 360, 330, 300, 260, 230, 200, 160, 130, 100, 80
    };
    size_t nSpeedValues = sizeof(pointerSpeedValues)/sizeof(pointerSpeedValues[0]);
    size_t valueIndex = -1;
    for (size_t i=0; i<nSpeedValues; i++) {
        if (pointerSpeed == pointerSpeedValues[i]) {
            valueIndex = i;
            break;
        }
    }
    if (valueIndex != -1) {
        pointerSpeed = (int64_t)((100.0/nSpeedValues)*valueIndex);
    } else {
        // default the value to 50
        pointerSpeed = 50;
    }
    dc.pointerSpeed((uint32_t)pointerSpeed);

    bool accelerate = boost::any_cast<bool>(options.find("accelerate")->second);
    dc.pointerAcceleration(accelerate);
    double accelerationRate = boost::any_cast<double>(options.find("acceleration")->second);
    BOOST_LOG_TRIVIAL(debug) << "Accelerate = " << accelerate <<", rate = " << accelerationRate;
    // accelerationRate ranges from 0.0. ~ 2.0. Convert it to 0-100 scale
    dc.pointerAccelerationRate(accelerationRate*50);

    bool singleAxis = boost::any_cast<bool>(options.find("single")->second);
    if (singleAxis) {
        uint32_t singleAxisKeys = (uint32_t)boost::any_cast<int64_t>(options.find("single keys")->second);
        dc.lockedAxisModifiers(modifiersToText(singleAxisKeys).c_str());
    }
    bool slowPointer = boost::any_cast<bool>(options.find("slow")->second);
    if (slowPointer) {
        uint32_t slowPointerKeys = (uint32_t)boost::any_cast<int64_t>(options.find("slow keys")->second);
        dc.slowPointerModifiers(modifiersToText(slowPointerKeys).c_str());
    }

    // scroll settings
    // Note that scroll speed is stored as button 12 -> cnt value. This will be migrated
    // when button settings are migrated. Here we migrate the rest of the scroll settings.
    dc.inertialScroll(boost::any_cast<bool>(options.find("inertial scroll")->second));
    dc.invertScroll(boost::any_cast<bool>(options.find("invert wheel")->second));
}

void SettingsMigrator::migrateDeviceButtons(const STRING2ANYTABLE& buttons, DeviceConfig& dc)
{
    struct {
        const char* key;
        uint32_t mask;
    } keyToMask[] = {
        { "00", 0x01 },
        { "01", 0x02 },
        { "02", 0x04 },
        { "03", 0x08 },
        { "08", 0x03 },
        { "09", 0x0C },
    };
    
    for (auto const& entry: keyToMask) {
        if (buttons.find(entry.key) != buttons.end()) {
            migrateDeviceButton(
                boost::any_cast<STRING2ANYTABLE>(buttons.find(entry.key)->second),
                entry.mask,
                dc);
        }
    }
    
    // scroll speed is set as button 12 setting
    try {
        if (buttons.find("12") != buttons.end()) {
            auto values = boost::any_cast<STRING2ANYTABLE>(buttons.find("12")->second);
            uint32_t cmd = (uint32_t)boost::any_cast<int64_t>(values.find("cmd")->second);
            uint32_t cnt = (uint32_t)boost::any_cast<int64_t>(values.find("cnt")->second);
            if (cmd == 20 && (cnt >= 1 && cnt <= 10)) {
                dc.scrollSpeed(cnt*10);
            }
        }
    } catch (std::exception const& e) {
  
    }
}

void SettingsMigrator::migrateDeviceButton(const STRING2ANYTABLE& button, uint32_t buttonMask, DeviceConfig& dc)
{
    uint32_t cmd = (uint32_t)boost::any_cast<int64_t>(button.find("cmd")->second);
    /*
    typedef bool(SettingsMigrator::*PFN_MIGRATE_ACTION)(uint32_t, const STRING2ANYTABLE&, uint32_t, DeviceConfig&);
    PFN_MIGRATE_ACTION migrateFns[] = {
        &SettingsMigrator::migrateCustomShortcut,
        &SettingsMigrator::migratePointerClick,
        &SettingsMigrator::migrateOthers
    };
    */

    if (migrateCustomShortcut(cmd, button, buttonMask, dc))
        return;
    if (migratePointerClick(cmd, button, buttonMask, dc))
        return;
    if (migrateLaunching(cmd, button, buttonMask, dc))
        return;
    if (migrateSnippet(cmd, button, buttonMask, dc))
        return;
    if (migrateOthers(cmd, button, buttonMask, dc))
        return;
    
    json j = {
        { "command", "noAction" }
    };
    dc.setButtonAction2(buttonMask, j);
/*
    for(auto pfn: migrateFns) {
        if ((this->*pfn)(cmd, button, buttonMask, dc)) {
            break;
        }
    }
*/
}

struct {
    uint32_t cmd;
    const char* macro;
} commandToPredefinedMacro[] = {
    { NO_ACTION, "noAction" },
    { MOUSE_AND_KEYBOARD_CLICK, "mouse_leftClick" },
    { MOUSE_AND_KEYBOARD_RIGHT_CLICK, "mouse_rightClick" },
    { MOUSE_AND_KEYBOARD_MIDDLE_CLICK, "mouse_middleClick" },
    { MOUSE_AND_KEYBOARD_DOUBLE_CLICK, "mouse_leftDoubleClick" },
    { MOUSE_AND_KEYBOARD_TRIPLE_CLICK, "mouse_leftTripleClick" },
    { MOUSE_AND_KEYBOARD_DRAG, "mouse_leftDrag" },
    { MOUSE_AND_KEYBOARD_RIGHT_DRAG, "mouse_rightDrag" },
    { MOUSE_AND_KEYBOARD_MIDDLE_DRAG, "mouse_middleDrag" },
    { MOUSE_AND_KEYBOARD_DOUBLE_RIGHT_CLICK, "mouse_rightDoubleClick" },
    { MOUSE_AND_KEYBOARD_DOUBLE_MIDDLE_CLICK, "mouse_middleDoubleClick" },
    { 160, "snippet_snippetMenu" },
    { 180, "snippet_snippet" },
    { 181, "snippet_snippet" },
    { 182, "snippet_snippet" },
    { 183, "snippet_snippet" },
    { 184, "snippet_snippet" },
    { 185, "snippet_snippet" },
    { 186, "snippet_snippet" },
    { 187, "snippet_snippet" },
    { 188, "snippet_snippet" },
    { 189, "snippet_snippet" },
    { 20, "navigation_scrollUp" },
    { 21, "navigation_scrollDown" },
    { 22, "navigation_scrollLeft" },
    { 23, "navigation_scrollRight" },
    
    { 30, "browser_back" },
    { 31, "browser_forward" },
    { 32, "browser_stop" },
    { 33, "browser_refresh" },

    { 34, "navigation_pageUp" },
    { 35, "navigation_pageDown" },
    { 36, "navigation_home" },
    { 37, "navigation_end" },

    { 49, "editing_undo" },
    { 50, "editing_redo" },
    { 51, "editing_cut" },
    { 52, "editing_copy" },
    { 53, "editing_paste" },
    { 54, "editing_duplicate" },
    { 55, "editing_selectAll" },
    { 56, "editing_find" },
    { 74, "editing_increaseSize" },
    { 75, "editing_decreaseSize" },

    { 81, "media_eject" },
    { 85, "media_volumeDown" },
    { 86, "media_volumeUp" },

    { 90, "media_playPause" },
    { 91, "media_stop" },
    { 92, "media_previousTrack" },
    { 93, "media_nextTrack" },
    
    { 104, "missionControl_missionControl" },
    { 105, "missionControl_applicationWindows" },
    { 136, "missionControl_launchpad" },
    { 101, "missionControl_showDashboard" },
    { 106, "missionControl_showDesktop" },
    
    { 114, "application_showNextWindow" },
    { 57, "application_minimizeWindow" },
    { 46, "application_closeWindow" },
    { 40, "application_hideCurrent" },
    { 41, "application_hideOthers" },
    { 39, "application_preferences" },
    { 42, "application_quit" },
    
    // system
    { 123, "system_spotlight" },
    { 124, "system_showSpotlightWindow" },
    { 111, "system_switchWindow" },
    { 26, "system_switchApplication" },
    { 110, "system_focusOnDock" },
    { 109, "system_focusOnMenu" },
    { 100, "system_toggleDockHiding" },
    { 76, "system_showForceQuitDialog" },
    { 77, "system_showShutdownDialog" },
    { 59, "system_logoffImmediately" },
    { 38, "system_logoff" },
    { 78, "system_restart" },
    { 80, "system_sleep" },
    { 79, "system_shutdown" },
    
    { 82, "screen_increaseBrightness" },
    { 83, "screen_decreaseBrightness" },
    { 120, "screen_captureScreenToClipboard" },
    { 119, "screen_captureScreenToFile" },
    { 122, "screen_captureSelectionToClipboard" },
    { 121, "screen_captureSelectionToFile" },
    
    { 71, "launching_openApplication" },
    { 70, "launching_openFile" },
    { 72, "launching_openURL" },
};

static string legacyCommandCodeToMacro(uint32_t cmd)
{
    for(const auto& entry: commandToPredefinedMacro) {
        if (entry.cmd == cmd) {
            return entry.macro;
        }
    }
    return string();
}


bool SettingsMigrator::migrateCustomShortcut(uint32_t cmd, const STRING2ANYTABLE& params, uint32_t buttonMask, DeviceConfig& dc)
{
    if (cmd != MOUSE_AND_KEYBOARD_KEYBOARD_SHORTCUT) {
        return false;
    }
    
    std::string modifiers;
    uint32_t keyCode = (uint32_t)boost::any_cast<int64_t>(params.find("key")->second);
    if (params.find("mod") != params.end()) {
        modifiers = modifiersToText(
            (uint32_t)boost::any_cast<int64_t>(params.find("mod")->second)
        );
    }
    json j = {
        { "command", "keyboard_shortcut" },
        { "params", {
                { "keyCode", keyCode },
                { "modifiers", modifiers }
            }
        }
    };
    dc.setButtonAction2(buttonMask, j);
    return true;
}

bool SettingsMigrator::migratePointerClick(uint32_t cmd, const STRING2ANYTABLE& params, uint32_t buttonMask, DeviceConfig& dc)
{
    /*
    struct {
        uint32_t cmd;
        const char* macro;
    } cmdToMacro[] = {
        { MOUSE_AND_KEYBOARD_CLICK, "mouse_leftClick" },
        { MOUSE_AND_KEYBOARD_RIGHT_CLICK, "mouse_rightClick" },
        { MOUSE_AND_KEYBOARD_MIDDLE_CLICK, "mouse_middleClick" },
        { MOUSE_AND_KEYBOARD_DOUBLE_CLICK, "mouse_leftDoubleClick" },
        { MOUSE_AND_KEYBOARD_TRIPLE_CLICK, "mouse_leftTripleClick" },
        { MOUSE_AND_KEYBOARD_DRAG, "mouse_leftDrag" },
        { MOUSE_AND_KEYBOARD_RIGHT_DRAG, "mouse_rightDrag" },
        { MOUSE_AND_KEYBOARD_MIDDLE_DRAG, "mouse_middleDrag" },
        { MOUSE_AND_KEYBOARD_DOUBLE_RIGHT_CLICK, "mouse_rightDoubleClick" },
        { MOUSE_AND_KEYBOARD_DOUBLE_MIDDLE_CLICK, "mouse_middleDoubleClick" }
    };
    */
    if ((cmd >= MOUSE_AND_KEYBOARD_CLICK && cmd <= MOUSE_AND_KEYBOARD_MIDDLE_CLICK)
        || (cmd >= MOUSE_AND_KEYBOARD_DOUBLE_CLICK && cmd <= MOUSE_AND_KEYBOARD_DOUBLE_MIDDLE_CLICK)) {
        
        std::string modifiers;
        if (params.find("mod") != params.end()) {
            modifiers = modifiersToText(
                (uint32_t)boost::any_cast<int64_t>(params.find("mod")->second)
            );
        }
        json j = {
            { "command", legacyCommandCodeToMacro(cmd).c_str() },
            { "params", {
                    { "modifiers", modifiers }
                }
            }
        };
        
        dc.setButtonAction2(buttonMask, j);
        return true;
    }

    return false;
}

bool SettingsMigrator::migrateLaunching(uint32_t cmd, const STRING2ANYTABLE& button, uint32_t buttonMask, DeviceConfig& dc)
{
    if (cmd >= 70 && cmd <= 72) {
        /*
            { 71, "launching_openApplication" },
            { 70, "launching_openFile" },
            { 72, "launching_openURL" },
         */
        auto bundleId = boost::any_cast<string>(button.find("bndl")->second);
        auto str = boost::any_cast<string>(button.find("str")->second);
        if (cmd == 71) {
            // launching_openApplication
            json j = {
                { "command", "launching_openApplication" },
                { "params", {
                    { "label", str },
                    { "path", bundleId }
                    }
                }
            };
            dc.setButtonAction2(buttonMask, j);
        } else if (cmd == 72) {
            // launching_openURL
            json j = {
                { "command", "launching_openURL" },
                { "params", {
                    { "label", "" },
                    { "path", str }
                    }
                }
            };
            dc.setButtonAction2(buttonMask, j);
        } else {
            // distinguish between launching_openFile & launching_openFolder
            json j = {
                {"command", "noAction"}
            };
            try {
                auto expandedPath = expandHomeFolder(str);
                if (boost::filesystem::exists(expandedPath)) {
                    std::string macro = "launching_openFolder";
                    if (boost::filesystem::is_regular_file(expandedPath)) {
                        macro = "launching_openFile";
                    };
                    j = json({
                        { "command", macro },
                        { "params", {
                            { "label", "" },
                            { "path", expandedPath.c_str() }
                            }
                        }
                    });
                }
            } catch (std::exception&) {
            }
            dc.setButtonAction2(buttonMask, j);
        }
        return true;
    }
    
    return false;
}

bool SettingsMigrator::migrateSnippet(uint32_t cmd, const STRING2ANYTABLE& button, uint32_t buttonMask, DeviceConfig& dc)
{
    if (cmd >= 180 && cmd <= 189) { // snippet 1 to 10
        int index = cmd-180;
        Snippet const& snippet = _snippets[index];
        json j = {
            { "command", "snippet" },
            { "params", {
                { "index", index },
                { "label", snippet.label.c_str() },
                { "content", snippet.content.c_str() }
                }
            }
        };
        dc.setButtonAction2(buttonMask, j);
        return true;
    }
    return false;
}

bool SettingsMigrator::migrateOthers(uint32_t cmd, const STRING2ANYTABLE& params, uint32_t buttonMask, DeviceConfig& dc)
{
    json j = {
        { "command", legacyCommandCodeToMacro(cmd).c_str() }
    };
    dc.setButtonAction2(buttonMask, j);
    return true;
}
