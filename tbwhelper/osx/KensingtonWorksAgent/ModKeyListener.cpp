//
//  ModKeyListener.cpp
//  tbwhelper
//
//  Created by Hariharan Mahadevan on 2019/5/18.
//  Copyright © 2019 Kensington. All rights reserved.
//

#include "ModKeyListener.hpp"
#include <stdexcept>
#include <iomanip>

#include <boost/log/trivial.hpp>
#include "Modifiers.h"

#include <Carbon/Carbon.h>

static struct ScanCode2Modifier {
    uint16_t    scanCode;
    uint32_t    modKey;
} sc2mod[] = {
    { kVK_Shift, MAC_K_SHIFT },      // Left.Shift
    { kVK_Control, MAC_K_CTRL },       // Left.Ctrl
    { kVK_Option, MAC_K_OPTION },     // Left.Option
    { kVK_Command, MAC_K_COMMAND },    // Left.Command
    { kVK_RightCommand, MAC_K_COMMAND },    // Right.Command
    { kVK_RightOption, MAC_K_OPTION },     // Right.Option
    { kVK_RightShift, MAC_K_SHIFT },      // Right.Shift
    { kVK_RightControl, MAC_K_CTRL }        // Right.Ctrl
};

ModKeyListener::ModKeyListener(CFRunLoopMode rlMode, PFN_MODIFIER_KEY_STATUS_CHANGE pfnCallback, void* refCallback)
    : _eventTap(nullptr)
    , _rlSource(nullptr)
    , _rlMode()
    , _pfnCallback(pfnCallback)
    , _refCallback(refCallback)
    , _modifierStatus(0)
{
    // Create an event tap. We are interested in key presses.
    CGEventMask        eventMask;
    eventMask = ((1 << kCGEventKeyDown) | (1 << kCGEventKeyUp | (1 << kCGEventFlagsChanged)));

    _eventTap = CGEventTapCreate(
        kCGSessionEventTap,
        kCGHeadInsertEventTap,
        kCGEventTapOptionListenOnly,
        eventMask,
        ModKeyListener::sEventCallback,
        this);
    if (!_eventTap) {
        std::runtime_error("Failed to create event tap");
    }

    // Create a run loop source.
    _rlSource = CFMachPortCreateRunLoopSource(
        kCFAllocatorDefault,
        _eventTap,
        0);
    if (!_rlSource)
    {
        CFRelease(_eventTap);
        _eventTap = nullptr;
        std::runtime_error("Failed to create event tap runloop source");
    }

    // Add to the current run loop.
    CFRunLoopAddSource(
        CFRunLoopGetCurrent(),
        _rlSource,
        rlMode);
    
    _rlMode = rlMode;
    
    BOOST_LOG_TRIVIAL(info) << "Modifier key listener created";
}

ModKeyListener::~ModKeyListener()
{
    if (_eventTap)
    {
        if (isEnabled())
            disable();
        
        if (_rlSource)
            CFRunLoopRemoveSource(CFRunLoopGetCurrent(), _rlSource, _rlMode);
        // CFRelease(_rlSource);
        _rlSource = nullptr;
        // TODO: how to release event tap object?
    }
    
}

void ModKeyListener::enable()
{
    assert(_eventTap);
    if (!isEnabled())
        CGEventTapEnable(_eventTap, true);
}

void ModKeyListener::disable()
{
    assert(_eventTap);
    if (isEnabled())
        CGEventTapEnable(_eventTap, false);
}

bool ModKeyListener::isEnabled()
{
    assert(_eventTap);
    return CGEventTapIsEnabled(_eventTap);
}

CGEventRef ModKeyListener::sEventCallback(
    CGEventTapProxy proxy,
    CGEventType type,
    CGEventRef event,
    void *refcon)
{
    return reinterpret_cast<ModKeyListener*>(refcon)->eventCallback(
        proxy,
        type,
        event);
}

CGEventRef ModKeyListener::eventCallback(
    CGEventTapProxy proxy,
    CGEventType type,
    CGEventRef event)
{
    // The incoming keycode.
    if (type != kCGEventFlagsChanged) {
        // only handle 'modifier' keys
        return event;
    }

    CGKeyCode keycode = (CGKeyCode)CGEventGetIntegerValueField(
                                       event, kCGKeyboardEventKeycode);

    uint32_t modifier = 0;
    for (size_t i=0; i<sizeof(sc2mod)/sizeof(sc2mod[0]); i++) {
        if (sc2mod[i].scanCode == keycode) {
            modifier = sc2mod[i].modKey;
            break;
        }
    }

    BOOST_LOG_TRIVIAL(debug) << "ModKeyListener - modifier keyCode: "
        << std::hex << (uint32_t)keycode;

    if (modifier != 0) {
        _modifierStatus ^= modifier;
        _pfnCallback(_refCallback, modifier, _modifierStatus & modifier ? true : false);
    }
    
    // We must return the event for it to be useful.
    return event;
}
