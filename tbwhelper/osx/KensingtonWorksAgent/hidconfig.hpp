//
//  pointer.hpp
//  kwdaemon
//
//  Created by Hariharan Mahadevan on 2019/1/28.
//  Copyright © 2019 Kensington. All rights reserved.
//

#pragma once

#include <CoreFoundation/CoreFoundation.h>
#include <Foundation/Foundation.h>
#include <IOKit/IOKitLib.h>

io_connect_t getIOHIDConnection();
CFTypeRef getValue(const char* keyName, io_connect_t conn=IO_OBJECT_NULL);
bool setValue(const char* keyName, double value, io_connect_t conn=IO_OBJECT_NULL);

double getMouseSpeed();
bool setMouseSpeed(double accel);

uint32_t getMouseSpeedNormalized();
bool setMouseSpeedNormalized(uint32_t speed);

double getScrollingSpeed();
bool setScrollingSpeed(double newValue);

uint32_t getScrollingSpeedNormalized();
bool setScrollingSpeedNormalized(uint32_t newValue);

double getPointerAcceleration();
bool setPointerAcceleration(double newValue);

uint32_t getPointerAccelerationNormalized();
bool setPointerAccelerationNormalized(uint32_t newValue);
