/*
 * DarwinPlatformServices.cpp
 */
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <thread>
#include <memory>

#include <boost/filesystem.hpp>
#include <boost/log/trivial.hpp>

#include <Cocoa/Cocoa.h>
#include <IOKit/hidsystem/ev_keymap.h>

#include "DarwinPlatformServices.h"
#include "Modifiers.h"
#include "hidconfig.hpp"
#include "SendAppleEvent.h"

#include <boost/log/trivial.hpp>

#include "oshelper.h"
#include "favapps.h"

DarwinPlatformServices::DarwinPlatformServices()
{}

DarwinPlatformServices::~DarwinPlatformServices()
{
}

// TODO: needs testing when fModifiersOnly is set to true
void postKeystrokeImpl(uint16_t usModifiers, uint16_t keyCode, bool fDown, bool fModifiersOnly)
{
    usModifiers &= MAC_MODIDIFER_BITS;

    if (usModifiers & MAC_K_SPECIAL) {
        // special key, use HIDPostAuxKey
        BOOST_LOG_TRIVIAL(debug) << "posting special key: " << keyCode
            << ", dir: " << (fDown ? "DOWN" : "UP");
        HIDPostAuxKey(keyCode, fDown);
        return;
    }
    
    // standard keys
    struct modKeyToFlag {
        uint16_t mod;
        uint64_t flag;
    } m2f[] = {
        { MAC_K_COMMAND, kCGEventFlagMaskCommand },
        { MAC_K_SHIFT, kCGEventFlagMaskShift },
        { MAC_K_OPTION, kCGEventFlagMaskAlternate },
        { MAC_K_CTRL, kCGEventFlagMaskControl },
        { MAC_K_FN, kCGEventFlagMaskSecondaryFn }
    };

    bool flagsAreInitialized = false;
    uint64_t flags = 0;
    for (size_t i=0; i<sizeof(m2f)/sizeof(m2f[0]); i++)
    {
        if (usModifiers & m2f[i].mod)
        {
            flags |= m2f[i].flag;
            flagsAreInitialized = true;
        }
    }
    CGEventSourceRef source = NULL;
    source = CGEventSourceCreate(kCGEventSourceStateCombinedSessionState);
    CGEventRef cgEvent = CGEventCreateKeyboardEvent(source, keyCode, fDown);
    if (flagsAreInitialized)
        CGEventSetFlags(cgEvent, flags);
    CGEventPost(kCGAnnotatedSessionEventTap, cgEvent);
    
    if (cgEvent) CFRelease(cgEvent);
    if (source) CFRelease(source);

}

void DarwinPlatformServices::postKeystroke(
    uint16_t usModifiers, uint16_t keyCode, bool fDown, bool fModifiersOnly)
{
    postKeystrokeImpl(usModifiers, keyCode, fDown, fModifiersOnly);
}

static void postStringImpl(const std::string& str)
{
    // Code from https://www.mail-archive.com/cocoa-dev@lists.apple.com/msg23343.html
    //
    // A bit of history.
    //
    // At first I tried to do the same thing as what is being done in Windows,
    // which is iterate through the characters of the string and try to
    // convert each character to its equivalent scan code (by using a private
    // lookup table) and then poke it into the input queue as in the previous
    // method. But soon, realised that this would only work on English keyboards
    // as that is the only scancode map that I can create. Upon further research
    // I discovered the API CGEventKeyboardSetUnicodeString. Seraching for this
    // I stumbled upon the following code from the link above.
    //
    // This seems to work for all characters. I tested with European as well
    // as Chinese chacters an the code holds.
    CGEventSourceRef eventSource = CGEventSourceCreate(kCGEventSourceStateHIDSystemState);
    if (!eventSource) {
        BOOST_LOG_TRIVIAL(error) << "postString - error creating event source";
        return;
    }
    
    CGEventRef keyEventDown = CGEventCreateKeyboardEvent(eventSource, 0, true);
    if (!keyEventDown) {
        CFRelease(eventSource);
        BOOST_LOG_TRIVIAL(error) << "postString - error creating keyboard event";
        return;
    }
    
    @autoreleasepool {
        NSString* characters = [NSString stringWithUTF8String:str.c_str()];
        UniChar buffer;
        for (int i = 0; i < [characters length]; i++) {
            [characters getCharacters:&buffer range:NSMakeRange(i, 1)];
            if (buffer == L'\n') {
                //BOOST_LOG_TRIVIAL(debug) << "snippet - new line character";
                postKeystrokeImpl(0, kVK_Return, true, false);
                postKeystrokeImpl(0, kVK_Return, false, false);
            } else if (buffer == L'\t') {
                //BOOST_LOG_TRIVIAL(debug) << "snippet - tab character";
                postKeystrokeImpl(0, kVK_Tab, true, false);
                postKeystrokeImpl(0, kVK_Tab, false, false);
            } else {
                keyEventDown = CGEventCreateKeyboardEvent(eventSource, 1, true);
                if (keyEventDown)
                {
                    CGEventKeyboardSetUnicodeString(keyEventDown, 1, &buffer);
                    CGEventPost(kCGHIDEventTap, keyEventDown);
                    CFRelease(keyEventDown);
                }
            }
        }
    }
    CFRelease(eventSource);
}

void DarwinPlatformServices::postString(const std::string& str)
{
    postStringImpl(str);
}

void DarwinPlatformServices::postPointerClick(int buttonMask, bool fDown)
{
    CGEventType et;
    CGMouseButton mb;
    if (buttonMask == 0x01) {
        et = fDown ? kCGEventLeftMouseDown : kCGEventLeftMouseUp;
        mb = kCGMouseButtonLeft;
    } else if (buttonMask == 0x02) {
        et = fDown ? kCGEventRightMouseDown : kCGEventRightMouseUp;
        mb = kCGMouseButtonRight;
    } else {
        // Apple does not define user mode macros for buttons 3 and above
        return;
    }
    
    CGEventRef ourEvent = CGEventCreate(NULL);
    CGPoint point = CGEventGetLocation(ourEvent);
    CFRelease(ourEvent);

    CGEventRef clickEvent = CGEventCreateMouseEvent(
        NULL,
        et,
        point,
        mb
        );
    
    CGEventPost(kCGHIDEventTap, clickEvent);
    CFRelease(clickEvent);
}

void DarwinPlatformServices::configurePointer(int speed, int acceleration, unsigned scrollLines)
{
    // speed & scrollLines is to be given to the driver
    
    // acceleration in the scale 0~100 has to be mapped to
    // the range 0 - 196608 and set in the HID IORegistry.
}

static bool openProgram(const std::string& bin, std::vector<std::string> const& args)
{
    @autoreleasepool {
    
        id idBin = [NSString stringWithUTF8String:bin.c_str()];

        id nsstrings = [NSMutableArray new];
        std::for_each(args.begin(), args.end(), ^(std::string str) {
            id nsstr = [NSString stringWithUTF8String:str.c_str()];
            [nsstrings addObject:nsstr];
        });
        //NSLog(@"nsstrings: %@", nsstrings);
        
        [NSTask launchedTaskWithLaunchPath:idBin
            arguments:nsstrings];
        return true;
    }
}

/*
 *  type: 0-Unknown, 1-fileOrFolder, 2-Application, 3-URL
 */
void DarwinPlatformServices::openResource(const std::string& resource, const std::vector<std::string>& args, int type)
{
    @autoreleasepool {
        NSString *nsRes = [NSString stringWithCString:resource.c_str() encoding:NSUTF8StringEncoding];
        
        switch(type)
        {
        case 1:
            // fileOrFolder
            [[NSWorkspace sharedWorkspace] openFile:nsRes];
            break;
        case 2:
            // Application, can be absolute path to the app or bundle identifier
            if (boost::filesystem::exists(resource)) {
                // Absolute path specified
                [[NSWorkspace sharedWorkspace] launchApplication:nsRes];
            } else {
                // Try to resolve the bundle identifier. This would fail if it's not
                // a valid bundle id. If it succeeds, launch it.
                NSURL* url = [[NSWorkspace sharedWorkspace] URLForApplicationWithBundleIdentifier:nsRes];
                if (url) {
                    [[NSWorkspace sharedWorkspace] launchApplication:url.path];
                }
            }
            break;
        case 3:
            // open URL
            [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:nsRes]];
            break;
        case 4:
            // launch a task with arguments
            openProgram(resource, args);
            break;
        default:
            BOOST_LOG_TRIVIAL(warning) << "Unknown openResource type: " << type;
            break;
        }
    }
    /*
    std::string command;
    if (args.length() > 0)
    {
        command = "\"";
        command.append(resource);
        command.append(" ");
        command.append(args);
        command.append("\"");
    }
    else
    {
        command = "open \"";
        command.append(resource);
        command.append("\"");
    }
    
    BOOST_LOG_TRIVIAL(info) << "Opening resource: " << command;
    std::system(command.c_str());
    */
}

std::string DarwinPlatformServices::getPreferencesFolder()
{
    namespace fs = boost::filesystem;
    auto folder = fs::path(getenv("HOME"));
    folder.append("Library/Preferences");
    fs::create_directory(folder);
    return folder.c_str();
}

std::string DarwinPlatformServices::getLogFileFolder()
{
    namespace fs = boost::filesystem;
    auto logFolder = fs::path(getenv("HOME"));
    logFolder.append("Library/Logs");
    fs::create_directory(logFolder);
    return logFolder.c_str();
}

void shutdown(nlohmann::json const& args)
{
    SendAppleEventToSystemProcess(kAEShowShutdownDialog, kSystemProcess);
}

void restart(nlohmann::json const& args)
{
    SendAppleEventToSystemProcess(kAEShowRestartDialog, kSystemProcess);
}

void logoff(nlohmann::json const& args)
{
    SendAppleEventToSystemProcess(kAELogOut, kSystemProcess);
}

void logoffImmediately(nlohmann::json const& args)
{
    SendAppleEventToSystemProcess(kAEReallyLogOut, kSystemProcess);
}

void sleep(nlohmann::json const& args)
{
    SendAppleEventToSystemProcess(kAESleep, kSystemProcess);
}

void mediaPlayPause(nlohmann::json const&)
{
    HIDPostAuxKey(NX_KEYTYPE_PLAY, true);
    HIDPostAuxKey(NX_KEYTYPE_PLAY, false);
}

void mediaStop(nlohmann::json const&)
{
}

void mediaNextTrack(nlohmann::json const&)
{
    HIDPostAuxKey(NX_KEYTYPE_NEXT, true);
    HIDPostAuxKey(NX_KEYTYPE_NEXT, false);
}

void mediaPrevTrack(nlohmann::json const&)
{
    HIDPostAuxKey(NX_KEYTYPE_PREVIOUS, true);
    HIDPostAuxKey(NX_KEYTYPE_PREVIOUS, false);
}

void mediaVolumeDown(nlohmann::json const&)
{
    float volume = getVolume();
    if (volume > 0) {
        volume -= 0.1;
        volume = std::max(volume, 0.0f);
        setVolume(volume);
    }
}

void mediaVolumeUp(nlohmann::json const&)
{
    float volume = getVolume();
    if (volume > 0) {
        volume += 0.1;
        volume = std::min(volume, 1.0f);
        setVolume(volume);
    }
}

static void startLaunchpad(nlohmann::json const&)
{
    std::string program = "/Applications/Launchpad.app";
    if (!boost::filesystem::exists(program)) {
        program = "/System/Applications/Launchpad.app";
    }
    if (boost::filesystem::exists(program)) {
        @autoreleasepool {
            NSString *nsRes = [NSString stringWithCString:program.c_str() encoding:NSUTF8StringEncoding];
            [[NSWorkspace sharedWorkspace] launchApplication:nsRes];
        }
    } else {
        BOOST_LOG_TRIVIAL(warning) << "Could not resolve path: " << program;
    }
}

void DarwinPlatformServices::system(std::string const& action, nlohmann::json const& args)
{
    BOOST_LOG_TRIVIAL(debug) << "Triggering system action: '" << action << "'";

    typedef void(*PFN_SYSTEMACTIONHANDLER)(nlohmann::json const& args);
    struct ActionDispatchTable {
        const char* action;
        PFN_SYSTEMACTIONHANDLER pfnHandler;
    } handlers[] = {
        { "shutdown", shutdown },
        { "restart", restart },
        { "logoff", logoff },
        { "logoffImmediately", logoffImmediately },
        { "sleep", sleep },
        { "mediaPlayPause", mediaPlayPause },
        { "mediaStop", mediaStop },
        { "mediaNextTrack", mediaNextTrack },
        { "mediaPrevTrack", mediaPrevTrack },
        { "volumeUp", mediaVolumeUp },
        { "volumeDown", mediaVolumeDown },
        { "launchpad", startLaunchpad }
    };
    
    bool actionTaken = false;
    for (auto elem : handlers)
    {
        if (action == elem.action)
        {
            elem.pfnHandler(args);
            actionTaken = true;
            break;
        }
    }

    if (!actionTaken)
    {
        BOOST_LOG_TRIVIAL(error) << "Could not found handler for system action '" << action << "'";
    }
}

/*
struct App {
	std::string _program;
	std::string _name;
	std::string _icon;
	App(const char* program, const char* name, const char* iconBase64)
		: _program(program), _name(name), _icon(iconBase64)
	{}

	int operator<(const App& rhs) const
	{
		return _program < rhs._program;
	}
};
void to_json(json& j, App const& pgm)
{
	j["program"] = pgm._program;
	j["name"] = pgm._name;
	j["icon"] = pgm._icon;
}
*/
nlohmann::json DarwinPlatformServices::getFavoriteApps(int count, const std::set<std::string>& exclude)
{
    BOOST_LOG_TRIVIAL(info) << "DarwinPlatformServices::getFavoriteApps()+++++++++++++++++++";
/*
    nlohmann::json jApps = json::array();

    @autoreleasepool {
        NSArray *appsArray = [[NSWorkspace sharedWorkspace] runningApplications];

        for (NSRunningApplication *a  in appsArray) {
            //NSLog(@"Name: %@", [a localizedName]);

            const char* name = [[a localizedName] UTF8String];
            std::string program([[a bundleURL] fileSystemRepresentation]);

            // filter programs from /System folder
            if (program.find("/System/") != std::string::npos) {
                continue;
            }
            
            //NSLog(@"Icon: %@", [a icon]);
            NSRect rect;  // contains an origin, width, height
            rect = NSMakeRect(0, 0, 32, 32);
            //NSImageRep* imageRep = [[a icon] bestRepresentationForRect:rect context:nil hints:nil];
            //NSLog(@"Icon size: %@", imageRep);
            NSImage* image = [a icon];
            [image lockFocus];
            NSBitmapImageRep *bitmapRep = [[NSBitmapImageRep alloc] initWithFocusedViewRect:NSMakeRect(0, 0, 32, 32)];
            [image unlockFocus];
            NSData *imageData = [bitmapRep representationUsingType:NSPNGFileType properties:nil];;
            NSString *base64String = [imageData base64EncodedStringWithOptions:0];
            //NSLog(@"Icon image: %@", base64String);
            const char* icon = [base64String UTF8String];
            jApps.push_back(App(program.c_str(), name, icon));
        }
    }

    return jApps;
*/
    return ::getFavApps(count, exclude);
}


// Dummy View class used to receive Menu Events
@interface DummyView : NSView
{
    NSMenuItem* nsMenuItem;
}
- (void) OnMenuSelection:(id)sender;
- (NSMenuItem*)MenuItem;
@end

@implementation DummyView
- (NSMenuItem*)MenuItem
{
    return nsMenuItem;
}

- (void)OnMenuSelection:(id)sender
{
    nsMenuItem = sender;
}
@end

struct SnippetsMenuData {
    std::vector<std::string> _snippets;
    std::vector<std::string> _labels;
    int _selectedItemIndex;
    SnippetsMenuData(std::vector<std::string> const& items, std::vector<std::string> const& labels)
        : _snippets(), _labels(), _selectedItemIndex(-1)
    {
        // build snippets that we can display, which are the non-empty ones.
        const size_t MAX_ENTRY_LENGTH = 128;
        for (size_t i = 0; i < items.size(); i++)
        {
            std::string const& entry = items[i];

            // skip over empty snippets
            if (entry.length() == 0)
                continue;

            std::string label = labels[i];
            if (label.length() == 0)
                label = entry;

            std::stringstream ss;
            ss << label.substr(0, MAX_ENTRY_LENGTH);	// restrict width
            if (label.length() > MAX_ENTRY_LENGTH)
                ss << "..."; // ellipsis to indicate text has been clipped
            _labels.push_back(ss.str());
            _snippets.push_back(entry);
        }
    }
    bool isEmpty() {
        return _snippets.size() == 0;
    }
};

NSMenu* s_snippetsMenu = nullptr;

void snippetsMenuThread(SnippetsMenuData* pMenuData)
{
    BOOST_LOG_TRIVIAL(debug) << "SnippetsMenu thread - entry";

    if (s_snippetsMenu) {
        BOOST_LOG_TRIVIAL(debug) << "Snippets menu already active...";
        return;
    }
    
    /*
    BOOST_LOG_TRIVIAL(debug) << "Snippets";
    for (auto& item: pMenuData->_labels)
    {
        BOOST_LOG_TRIVIAL(debug) << "\t" << item;
    }
    */
    
    dispatch_async(dispatch_get_main_queue(), ^(void){
        @autoreleasepool {
        
            NSRect    graphicsRect;  // contains an origin, width, height
            graphicsRect = NSMakeRect(200, 200, 50, 100);


            [NSApplication sharedApplication];
                // Style flags:
            NSUInteger windowStyle = NSWindowStyleMaskTitled | NSWindowStyleMaskClosable | NSWindowStyleMaskResizable;
            
            // Window bounds (x, y, width, height).
            NSRect windowRect = NSMakeRect(0, 0, 0, 0);
            NSWindow * window = [[NSWindow alloc] initWithContentRect:windowRect
                                                  styleMask:windowStyle
                                                  backing:NSBackingStoreBuffered
                                                  defer:NO];
            if (window == nullptr) {
                BOOST_LOG_TRIVIAL(warning) << "snippets menu - error creating window";
                return;
            }
            // Window controller:
            NSWindowController * windowController = [[NSWindowController alloc] initWithWindow:window];
            if (windowController == nullptr) {
                BOOST_LOG_TRIVIAL(warning) << "snippets menu - error creating window controller";
                return;
            }

            DummyView *nsView = [[DummyView alloc] initWithFrame:graphicsRect];
            if (nsView == nullptr) {
                BOOST_LOG_TRIVIAL(warning) << "snippets menu - error creating view";
                return;
            }
            [window setContentView:nsView];

            //-----------------------------
            // Create Menu and Dummy View
            //-----------------------------
            NSMenu *nsMenu = [[NSMenu alloc] initWithTitle:@"Contextual Menu"];
            if (nsMenu == nullptr) {
                BOOST_LOG_TRIVIAL(warning) << "snippets menu - error creating menu";
                return;
            }
            [nsMenu setAutoenablesItems:NO];

            int index = 1;
            NSMenuItem* firstItem = nullptr;
            for (auto item: pMenuData->_labels)
            {
                if (item.length() == 0)
                    continue;
                
                @autoreleasepool {
                    NSString *menuLabel = [NSString stringWithCString:item.c_str() encoding:NSUTF8StringEncoding];
                    NSMenuItem* item = [nsMenu addItemWithTitle:menuLabel action:@selector(OnMenuSelection:) keyEquivalent:@""];
                    if (item) {
                        if (!firstItem) {
                            firstItem = item;
                        }
                        [item setTag:index];
                        [item setTarget:nsView];
                        [item setEnabled:YES];
                    } else {
                        BOOST_LOG_TRIVIAL(warning) << "snippets menu - error creating menu item";
                    }
                }
                index++;
            }

            if (firstItem)
            {
                //BOOST_LOG_TRIVIAL(debug) << "Showing snippets menu..";
                s_snippetsMenu = nsMenu;
                [nsMenu popUpMenuPositioningItem:firstItem atLocation:[NSEvent mouseLocation] inView:nil];
                //BOOST_LOG_TRIVIAL(debug) << "Snippets menu shown..";
                s_snippetsMenu = nullptr;
     
                int selectedIndex = (int)[[nsView MenuItem] tag];
                //BOOST_LOG_TRIVIAL(debug) << "Snippets menu selected item: " << selectedIndex;
                if (selectedIndex > 0) {
                    pMenuData->_selectedItemIndex = selectedIndex - 1;
                }
            }
            else
            {
                BOOST_LOG_TRIVIAL(warning) << "No snippets to display!";
            }
        } // end @autorelease pool

        if (pMenuData->_selectedItemIndex >= 0) {
            std::string snippet = pMenuData->_snippets[pMenuData->_selectedItemIndex];
            BOOST_LOG_TRIVIAL(debug) << "Selected snippet: " << snippet << std::endl;
            postStringImpl(snippet);
        }
        delete pMenuData;
    });

    BOOST_LOG_TRIVIAL(debug) << "SnippetsMenu thread - exit";
}

std::string DarwinPlatformServices::showPopupMenu(std::vector<std::string> const& items, std::vector<std::string> const& labels, int timeOut)
{
    std::unique_ptr<SnippetsMenuData> menuData(new SnippetsMenuData(items, labels));
    if (!menuData->isEmpty()) {
        std::thread t1(snippetsMenuThread, menuData.release());
        t1.detach();
    }
    return std::string();
}

uint32_t DarwinPlatformServices::getCurPointerSpeed()
{
    // In Mac, pointer speed is hte speed multiplier given to the
    // driver. Sice we calibrate 50 to be equal to multiplier 1.0,
    // return this as the default pointer speed.
    return 50;
}

uint32_t DarwinPlatformServices::getCurPointerAccelerationRate()
{
    // Return pointer acceleration rate from the OS.
    return getPointerAccelerationNormalized();
}

uint32_t DarwinPlatformServices::getCurScrollSpeed()
{
    // Scroll speed is the number of lines to scroll for every
    // single wheel movement. We calibrate the default to 1 line.
    //BOOST_LOG_TRIVIAL(debug) << "vannes20200807 getCurScrollSpeed: ";
    return 1;
}

bool DarwinPlatformServices::getScrollInvert()
{
    return [[[NSUserDefaults standardUserDefaults] objectForKey:@"com.apple.swipescrolldirection"] boolValue] ? true : false;
}

void DarwinPlatformServices::postScrollNavigation(ScrollDirection direction, int lines)
{
}
