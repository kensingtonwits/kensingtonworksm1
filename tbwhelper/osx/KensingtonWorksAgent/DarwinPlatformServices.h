/*
 * DarwinPlatformServices.h
 *
 *  Platform services for OSX
 */
#pragma once

#include <memory>

#include "../../shared/PlatformServices.h"

class DarwinPlatformServices : public PlatformServicesImpl {

public:
    DarwinPlatformServices();
    ~DarwinPlatformServices();
    
    virtual void postKeystroke(uint16_t usModifiers, uint16_t keyCode, bool fDown, bool fModifiersOnly);
    virtual void postString(const std::string& string);
    virtual void postPointerClick(int buttonMask, bool fDown);
    virtual void configurePointer(int speed, int acceleration, unsigned scrollLines);
    virtual void openResource(const std::string& resource, const std::vector<std::string>& args, int type);
    virtual std::string getPreferencesFolder();
    virtual std::string getLogFileFolder();
    virtual void system(std::string const& verb, nlohmann::json const& args);
   	virtual nlohmann::json getFavoriteApps(int count, const std::set<std::string>& exclude);
	virtual std::string showPopupMenu(std::vector<std::string> const& items, std::vector<std::string> const& labels, int timeOut = -1);
    virtual uint32_t getCurPointerSpeed();
    virtual uint32_t getCurPointerAccelerationRate();
    virtual uint32_t getCurScrollSpeed();
    virtual bool getScrollInvert();
    virtual void postScrollNavigation(ScrollDirection direction, int lines);
};
