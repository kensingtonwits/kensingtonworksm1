//
// Function Restart/Shutdown/Logout OSX
// Taken from https://developer.apple.com/library/archive/qa/qa1134/_index.html
//
//
#include "SendAppleEvent.h"


/**
 * Given a four letter AppleEvent string code, translates it into
 * the 32-bit numberic code that can be passed to 
 * SendAppleEventToSystemProcess.
 *
 * If the verb is not a 4 letter string, returns 0.
 */
uint32_t AEVerbToEventId(const char* verb)
{
    size_t len = strlen(verb);
    if (len != 4)
        return 0;
    
    // Note that we're on a little endian machine, so bytes need
    // to be swapped!
    unsigned eventId =
        (unsigned)verb[0] << 24
        | (unsigned)verb[1] << 16
        | (unsigned)verb[2] << 8
        | (unsigned)verb[3];
    
    return (uint32_t)eventId;
}

//
OSStatus SendAppleEventToSystemProcess(AEEventID EventToSend, UInt32 process)
{
    AEAddressDesc targetDesc;
    ProcessSerialNumber kPSNOfSystemProcess = { 0, process };
    AppleEvent eventReply = {typeNull, NULL};
    AppleEvent appleEventToSend = {typeNull, NULL};

    OSStatus error = noErr;

    error = AECreateDesc(typeProcessSerialNumber, &kPSNOfSystemProcess, 
                                            sizeof(kPSNOfSystemProcess), &targetDesc);

    if (error != noErr)
    {
        return(error);
    }

    error = AECreateAppleEvent(kCoreEventClass, EventToSend, &targetDesc, 
                   kAutoGenerateReturnID, kAnyTransactionID, &appleEventToSend);

    AEDisposeDesc(&targetDesc);
    if (error != noErr)
    {
        return(error);
    }

    //error = AESend(&appleEventToSend, &eventReply, kAENoReply, 
    //              kAENormalPriority, kAEDefaultTimeout, NULL, NULL);
    /*
     The AESendMessage function allows you to send Apple events without linking 
     to the entire Carbon framework, as required by AESend. Linking with Carbon 
     brings in the HIToolbox framework, which requires that your application 
     have a connection to the window server. Daemons and other applications that 
     have no interface but wish to send and receive Apple events can use the 
     following functions for working with Apple events at a lower level: 
     AESendMessage, AEGetRegisteredMachPort, AEDecodeMessage, and 
     AEProcessMessage. 

     https://developer.apple.com/documentation/coreservices/1442994-aesendmessage?language=objc
     */
    error = AESendMessage(&appleEventToSend, &eventReply, kAENoReply,
                  kAEDefaultTimeout);
    error = AESendMessage(&appleEventToSend, &eventReply, kAENoReply,
                  kAEDefaultTimeout);

    AEDisposeDesc(&appleEventToSend);
    if (error != noErr)
    {
        return(error);
    }

    AEDisposeDesc(&eventReply);

    return(error); 
}

