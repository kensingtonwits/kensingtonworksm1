#pragma once

#include <map>
#include <string>
#include <vector>

#include <boost/any.hpp>

#include "../../shared/MigratorBase.h"

class Settings;
class DeviceConfig;

class SettingsMigrator /*: public MigratorBase*/ {

    struct Snippet {
        std::string label;
        std::string content;
    };
    std::vector<Snippet> _snippets;
    
public:
    SettingsMigrator();
    ~SettingsMigrator();
    
    void migrate();
    
private:
    typedef std::map<std::string, boost::any> STRING2ANYTABLE;
    typedef std::vector<boost::any> ANYARRAY;

    void migrateDevices(const boost::any& devices, Settings& settings);
    void migrateDevice(const STRING2ANYTABLE& deviceSettings, Settings& settings);
    void migrateDeviceOptions(const STRING2ANYTABLE& options, DeviceConfig& dc);
    void migrateDeviceButtons(const STRING2ANYTABLE& buttons, DeviceConfig& dc);
    void migrateDeviceButton(const STRING2ANYTABLE& button, uint32_t buttonMask, DeviceConfig& dc);
    
    bool migrateCustomShortcut(uint32_t cmd, const STRING2ANYTABLE& button, uint32_t buttonMask, DeviceConfig& dc);
    bool migratePointerClick(uint32_t cmd, const STRING2ANYTABLE& button, uint32_t buttonMask, DeviceConfig& dc);
    bool migrateLaunching(uint32_t cmd, const STRING2ANYTABLE& button, uint32_t buttonMask, DeviceConfig& dc);
    bool migrateSnippet(uint32_t cmd, const STRING2ANYTABLE& button, uint32_t buttonMask, DeviceConfig& dc);
    bool migrateOthers(uint32_t cmd, const STRING2ANYTABLE& button, uint32_t buttonMask, DeviceConfig& dc);
};
