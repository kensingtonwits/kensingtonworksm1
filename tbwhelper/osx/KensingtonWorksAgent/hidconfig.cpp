//
//  pointer.cpp
//  kwdaemon
//
//  Created by Hariharan Mahadevan on 2019/1/28.
//  Copyright © 2019 Kensington. All rights reserved.
//

#include "hidconfig.hpp"
#include <iostream>
#include <algorithm>
#include <cmath>

#include <CoreFoundation/CoreFoundation.h>
#include <Foundation/Foundation.h>
#include <IOKit/IOKitLib.h>
#include <IOKit/hidsystem/IOHIDParameter.h>
#include <IOKit/hidsystem/IOHIDShared.h>

using namespace std;

/*
 Opens the HIDSystem IORegistry key and returns it.
 
 Returns IO_OBJECT_NULL on error.
 */
io_connect_t getIOHIDConnection()
{
    // Create a matching dictionary for the IOHIDSystem IORegistryEntry class instance
    CFMutableDictionaryRef hidDictRef = IOServiceMatching("IOHIDSystem");
    if (!hidDictRef) {
        return IO_OBJECT_NULL;
    }
    
    // Create an iterator for all IO Registry objects that match the dictionary
    // (There should only be one).
    // Note that IOServiceGetMatchingServices will consume the reference to
    // the dictionary that was made by IOServiceMatching. So we don't have to
    // release it ourselves.
    io_iterator_t iter;
    kern_return_t ret = IOServiceGetMatchingServices(kIOMasterPortDefault,
                                                     hidDictRef,
                                                     &iter);
    if (ret != KERN_SUCCESS) {
        return IO_OBJECT_NULL;
    }
    
    // Iterate over all matching objects
    io_object_t service;
    io_object_t hidSysParamConn = IO_OBJECT_NULL;
    while ((service = IOIteratorNext(iter)) != 0)
    {
        // There should be only one HIDSystem node in IO Registry
        ret = IOServiceOpen(service,
                            mach_task_self(),
                            kIOHIDParamConnectType,
                            &hidSysParamConn);
        IOObjectRelease(service);
        
        if (ret == kIOReturnSuccess && hidSysParamConn != IO_OBJECT_NULL) {
            // Found it!
            break;
        }
        
    }
    
    // Release the iterator
    IOObjectRelease(iter);
    
    return hidSysParamConn;
}

/*
 Returns the value for a key stored in I/O Registry under 
 an IOService node.
 
 Returns NULL on failure or the retrieved value as a CFType
 specialization.
 */
CFTypeRef getValue(const char* keyName, io_connect_t conn/*=IO_OBJECT_NULL*/)
{
    bool releaseIOObj = false;
    if (conn == IO_OBJECT_NULL) {

        conn = getIOHIDConnection();
        if (conn == IO_OBJECT_NULL)
            return NULL;
        
        releaseIOObj = true;
    }
    
    // CFString object that just points to our const char* argument
    // So there's no need to release it.
    CFStringRef strKey = CFStringCreateWithCStringNoCopy(
                                          kCFAllocatorDefault,
                                          keyName,
                                          kCFStringEncodingASCII,
                                          NULL);
    if (!strKey) {
        if (releaseIOObj)
            IOObjectRelease(conn);
        return NULL;
    }

    CFTypeRef value;
    kern_return_t ret = IOHIDCopyCFTypeParameter(conn,
                                                 strKey,
                                                 &value);
    // CFRelease(strKey);
    if (ret != kIOReturnSuccess || !value)
    {
        if (releaseIOObj)
            IOObjectRelease(conn);
        return NULL;
    }

    if (releaseIOObj)
        IOObjectRelease(conn);

    return value;
}

bool setValue(const char* keyName, double value, io_connect_t conn/*=IO_OBJECT_NULL*/)
{
    bool releaseIOObj = false;
    if (conn == IO_OBJECT_NULL) {
        
        conn = getIOHIDConnection();
        if (conn == IO_OBJECT_NULL)
            return NULL;
        
        releaseIOObj = true;
    }
    
    // CFString object that just points to our const char* argument
    // So there's no need to release it.
    CFStringRef strKey = CFStringCreateWithCStringNoCopy(
                                 kCFAllocatorDefault,
                                 keyName,
                                 kCFStringEncodingASCII,
                                 NULL);
    if (!strKey) {
        if (releaseIOObj)
            IOObjectRelease(conn);
        return NULL;
    }
    
    std::cout << "setValue - setting value for key: " << keyName << ", value: " << value << std::endl;
    
    kern_return_t ret = IOHIDSetCFTypeParameter(
                                conn,
                                strKey,
                                (__bridge CFTypeRef)@(value));
    if (ret != kIOReturnSuccess)
    {
        if (releaseIOObj)
            IOObjectRelease(conn);
        return false;
    }

    return true;
}

/*
 Returns the value of HID system parameter "HIDMouseAccelecation"
 */
double getMouseSpeed()
{
    NSLog(@"vannes20200807 getMouseSpeed");
    CFTypeRef value = getValue(kIOHIDMouseAccelerationTypeKey);
    if (value == NULL || CFGetTypeID(value) != CFNumberGetTypeID())
    {
        if (value)
            CFRelease(value);
        return -1;
    }
    
    NSNumber* accel = CFBridgingRelease(value);
    return accel.doubleValue;
}

/*
 Sets the value of HID system parameter "HIDMouseAcceleration"

 Value ranges from 0 < value < 196608. The 10 values that are set
 by System Preferences Mouse app for its various slider settings
 are:
 
 [0, 8192, 32768, 45056, 57344, 653556, 98304, 131072, 163840, 196608]
 */
bool setMouseSpeed(double accel)
{
    NSLog(@"vannes20200807 setMouseSpeed");
    return setValue(kIOHIDMouseAccelerationTypeKey, accel);
}

static uint32_t toNormalized(double value, double values[], size_t nValues)
{
    if (value <= 0)
        return 0;
    else if (value >= values[nValues-1])
        return 100;

    for (size_t i=0; i<nValues+1; i++) {
        if (value >= values[i] && value < values[i+1]) {
            return (uint32_t)(((double)100.0/nValues)*(i+1));
        }
    }
    
    uint32_t ret = (uint32_t)((value/(values[nValues-1]-values[0]))*value);
    return std::max(std::min(ret, (uint32_t)0), (uint32_t)100);
}

static double fromNormalized(uint32_t value, double values[], size_t nValues)
{
    double range = values[nValues-1] - values[0];
    double closest = ((value*range)/100);
    for (size_t i=0; i<nValues-1; i++) {
        if (closest >= values[i] && closest <= values[i+1]) {
            // we found the range
            if (closest-values[i] < values[i+1]-closest) {
                return values[i];
            } else {
                return values[i+1];
            }
        }
    }
    return values[nValues-1];
}

uint32_t getMouseSpeedNormalized()
{
    double speedValues[] = {
        0, 8192, 32768, 45056, 57344, 65536, 98304, 131072, 163840, 196608
    };
    size_t nValues = sizeof(speedValues)/sizeof(speedValues[0]);
    double speed = getMouseSpeed();
    if (speed <= speedValues[0])
        return 0;
    if (speed >= speedValues[nValues-1])
        return 100;

    uint32_t normalizedValue = 0;
    for (size_t i=0; i<nValues-1; i++) {
        if (speed >= speedValues[i] && speed < speedValues[i+1]) {
            if (speed-speedValues[i] < speedValues[i+1]-speed) {
                normalizedValue = (uint32_t)i*10;
            } else {
                normalizedValue = (uint32_t)(i+1)*10;
            }
            break;
        }
    }
    return normalizedValue;
}

bool setMouseSpeedNormalized(uint32_t speed)
{
    double speedValues[] = {
        0, 8192, 32768, 45056, 57344, 65536, 98304, 131072, 163840, 196608
    };
    size_t nValues = sizeof(speedValues)/sizeof(speedValues[0]);
    speed = std::min(speed, (uint32_t)100);
    size_t index = std::round((double)speed/10);
    index = std::min(std::max(index, (size_t)0), nValues-1);
    //std::cout << "Normalized mouse speed: " << speedValues[index] << std::endl;
    return setMouseSpeed(speedValues[index]);
}

/*
 * Scroll speed values as set by System Preferences fall in the range:
 *
 *  [0, 8192, 14090, 20480, 32768, 65536, 327680]
 */

double getScrollingSpeed()
{
    CFTypeRef value = getValue(kIOHIDMouseScrollAccelerationKey);
    if (value == NULL || CFGetTypeID(value) != CFNumberGetTypeID())
    {
        if (value)
            CFRelease(value);
        return -1;
    }
    
    NSNumber* accel = CFBridgingRelease(value);
    return accel.doubleValue;
}

bool setScrollingSpeed(double newValue)
{
    return setValue(kIOHIDMouseScrollAccelerationKey, newValue);
}

uint32_t getScrollingSpeedNormalized()
{
    double speedValues[] = {
        0, 8192, 14090, 20480, 32768, 65536, 327680
    };
    size_t nValues = sizeof(speedValues)/sizeof(speedValues[0]);
    double speed = getScrollingSpeed();
    if (speed <= speedValues[0])
        return 0;
    if (speed >= speedValues[nValues-1])
        return 100;

    uint32_t normalizedValue = 0;
    for (size_t i=0; i<nValues-1; i++) {
        if (speed >= speedValues[i] && speed < speedValues[i+1]) {
            if (speed-speedValues[i] < speedValues[i+1]-speed) {
                normalizedValue = (uint32_t)i*10;
            } else {
                normalizedValue = (uint32_t)(i+1)*10;
            }
            break;
        }
    }
    return normalizedValue;
}

bool setScrollingSpeedNormalized(uint32_t speed)
{
    double speedValues[] = {
        0, 8192, 14090, 20480, 32768, 65536, 327680
    };
    size_t nValues = sizeof(speedValues)/sizeof(speedValues[0]);
    speed = std::min(speed, (uint32_t)100);
    size_t index = std::round((double)speed/10);
    index = std::min(std::max(index, (size_t)0), nValues-1);
    //std::cout << "Normalized scroll speed: " << speedValues[index] << std::endl;
    return setScrollingSpeed(speedValues[index]);
}

/**
 * Value range:  [0, ..., 45056, ..., 196608]
 */
double getPointerAcceleration()
{
    CFTypeRef value = getValue(kIOHIDPointerAccelerationKey);
    if (value == NULL || CFGetTypeID(value) != CFNumberGetTypeID())
    {
        if (value)
            CFRelease(value);
        return -1;
    }
    
    NSNumber* accel = CFBridgingRelease(value);
    return accel.doubleValue;
}

bool setPointerAcceleration(double newValue)
{
    return setValue(kIOHIDPointerAccelerationKey, newValue);
}

uint32_t getPointerAccelerationNormalized()
{
    // NSLog(@"vannes20200807 getPointerAccelerationNormalized"); // it goes here each launch time
    double rate = getPointerAcceleration();
    return (uint32_t)((rate/196608.0)*100.0);
}

bool setPointerAccelerationNormalized(uint32_t newValue)
{
    newValue = std::min(newValue, (uint32_t)100);
    // AccelerationRate has to be whole number!
    double rate = std::round((((double)newValue/100.0) * 196608.0));
    return setPointerAcceleration((double)rate);
}
