
    #define VERSION_MAJOR           2
    #define VERSION_MINOR           2
    #define VERSION_PATCH           11
    #define _FILEVERSION            2,2,11,0
    #define _FILEVERSION_STR        "2.2.11.0"
    #define _PRODUCTVERSION         2,2,11,0
    #define _PRODUCTVERSION_STR     "2.2.11.0"
    