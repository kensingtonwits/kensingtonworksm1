/**
 * A class to track system power state changes.
 *
 * Incomplete implemetation. Power state notification messages have to be
 * implemented as overridable virtual methods so that client class can
 & override the class and implement its own handlers.
 *
 * Leaving code here so that should we need need to handle power
 * state change notifications, it can be included into the main code.
 *
 * Adapted from Apple's sample code here:
 *  https://developer.apple.com/library/archive/qa/qa1340/_index.html
 */
#pragma once

#include <ctype.h>

#include <mach/mach_port.h>
#include <mach/mach_interface.h>
#include <mach/mach_init.h>
 
#include <IOKit/pwr_mgt/IOPMLib.h>
#include <IOKit/IOMessage.h>

#include <boost/log/trivial.hpp>
 
class PowerStateMonitor {

    // a reference to the Root Power Domain IOService
    io_connect_t            _rootPort;
    // notification port allocated by IORegisterForSystemPower
    IONotificationPortRef   _notifyPortRef;
    // notifier object, used to deregister later
    io_object_t             _notifierObject;

public:
    PowerStateMonitor()
    {
        _rootPort = IORegisterForSystemPower(
            this,
            &_notifyPortRef,
            PowerStateMonitor::_MySleepCallBack,
            &_notifierObject);
        if ( _rootPort == 0 )
        {
            throw std::runtime_error("IORegisterForSystemPower failed\n");
        }
        
        // add the notification port to the application runloop
        CFRunLoopAddSource(CFRunLoopGetCurrent(),
            IONotificationPortGetRunLoopSource(_notifyPortRef),
            kCFRunLoopCommonModes);
    }
    
    ~PowerStateMonitor()
    {
        CFRunLoopRemoveSource(
            CFRunLoopGetCurrent(),
            IONotificationPortGetRunLoopSource(_notifyPortRef),
            kCFRunLoopCommonModes);
        IODeregisterForSystemPower(&_notifierObject);
        IOServiceClose(_rootPort);
        IONotificationPortDestroy(_notifyPortRef);
    }
    
    void SleepCallback(io_service_t service, natural_t messageType, void * messageArgument)
    {
        BOOST_LOG_TRIVIAL(debug) << "PowerStateMonitor - messageType: "
            << std::hex << (long unsigned int)messageType
            << ", messageArgument: " << std::hex << (long unsigned int)messageArgument;
     
        switch ( messageType )
        {
            case kIOMessageCanSystemSleep:
                /* Idle sleep is about to kick in. This message will not be sent for forced sleep.
                    Applications have a chance to prevent sleep by calling IOCancelPowerChange.
                    Most applications should not prevent idle sleep.
     
                    Power Management waits up to 30 seconds for you to either allow or deny idle
                    sleep. If you don't acknowledge this power change by calling either
                    IOAllowPowerChange or IOCancelPowerChange, the system will wait 30
                    seconds then go to sleep.
                */
     
                //Uncomment to cancel idle sleep
                //IOCancelPowerChange( _rootPort, (long)messageArgument );
                // we will allow idle sleep
                IOAllowPowerChange(_rootPort, (long)messageArgument );
                break;
     
            case kIOMessageSystemWillSleep:
                /* The system WILL go to sleep. If you do not call IOAllowPowerChange or
                    IOCancelPowerChange to acknowledge this message, sleep will be
                    delayed by 30 seconds.
     
                    NOTE: If you call IOCancelPowerChange to deny sleep it returns
                    kIOReturnSuccess, however the system WILL still go to sleep.
                */
     
                IOAllowPowerChange( _rootPort, (long)messageArgument );
                break;
     
            case kIOMessageSystemWillPowerOn:
                //System has started the wake up process...
                break;
     
            case kIOMessageSystemHasPoweredOn:
                //System has finished waking up...
            break;
     
            default:
                break;
     
        }
    }
    static void _MySleepCallBack( void * refCon, io_service_t service, natural_t messageType, void * messageArgument )
    {
        reinterpret_cast<PowerStateMonitor*>(refCon)->SleepCallback(service, messageType, messageArgument);
    }
};
