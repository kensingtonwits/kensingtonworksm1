#pragma once

#include "DeviceManagerBase.h"

#include <CoreFoundation/CoreFoundation.h>
#include <ApplicationServices/ApplicationServices.h>
#include <IOKit/IOKitLib.h>
#include <list>
#include <memory>
#include <string>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <atomic>

#include "frontmost.h"
#include "../../../driver/osx/driver/KWDriverInterface.h"

class OSError : public std::exception {
public:
    kern_return_t _ret; // API return code
    OSError(kern_return_t ret) : _ret(ret)
    {}
};

class Device;
class DeviceManager;
class ModKeyListener;

// User level abstraction on the kernel driver. Abstracts both DeviceUSB and
// DeviceBT into once class.
class Device : public DeviceBase {
    
protected:
    DeviceManager*  _pDeviceMgr;
    io_service_t    _service;
    io_object_t     _notification;
    io_connect_t    _connection;
    
    uint16_t        _interfaceNumber;
    
    TBWDRIVEREVENT  _event;
    
    mach_vm_address_t   _asyncMemBlock;  //*&*&*G1_ADD
    
protected:
    kern_return_t readNextEventAsync();
    static void sNextEventCallback(void* ref, IOReturn result);
    void nextEventCallback(IOReturn result);
    
    // These methods don't throw and reaturn KERN_* instead as they
    // are meant to be called from the ctor/dtor, body where we should ideally
    // should not be throwing exceptions.
    kern_return_t getDeviceInfo(TBWDEVICEINFO* pDevInfo);
    kern_return_t registerHelper();
    kern_return_t deregisterHelper();
    
    TBWBUTTONSCONFIG getButtonsConfig();
    void setButtonsConfig(TBWBUTTONSCONFIG* pConfg);

public:
    Device(DeviceManager* monitor, io_service_t service, bool _useDriverKit);
    ~Device();
    bool _Device_use_DriverKit = false;
    uint16_t interfaceNumber()
    { return _interfaceNumber; }

    /*
     * All these 'feature' methods throw OSError if the API call returns
     * any value other than KERN_SUCCESS. OSError._ret will have the kernel
     * return code.
     */

    void setScrollParams(int wheel, double speed, bool invert, bool inertial);
    void getScrollParams(int wheel, double& speed, bool& invert, bool& inertial);
    
    void setPointerParams(double speed, uint64_t acceleration);
    void getPointerParams(double& speed, uint64_t& acceleration);
    
    void enableSlowPointer();
    void disableSlowPointer();
    bool isSlowPointerEnabled();
    
    void enableSingleAxisMovement();
    void disableSingleAxisMovement();
    bool isSingleAxisMovementEnabled();
    
    double getPointerSpeed();
    void setPointerSpeed(double speed);
    
    void enablePointerAcceleration();
    void disablePointerAcceleration();
    bool isPointerAccelerationEnabled();

    // DeviceBase pure virtual methods
    bool postPointerClick(uint32_t buttonState, bool fDown);
    bool postKeystroke(uint32_t modifiers, uint32_t keyCode, bool fDown, bool fModifiersOnly);
    void updatePointerParams();
    void updateScrollWheelParams();
    void updateButtonsConfig();
#ifdef TAU
    void emulateButtonClick(uint32_t mask);
#endif
    static void _onDeviceNotification(void* refCon, io_service_t service, natural_t messageType, void* messageArgument);
    void onDeviceNotification(natural_t messageType, void* messageArgument);
};

// Class to monitor for new Device device arrival & removal
class DeviceManager : public DeviceManagerBase {
    
    friend class Device;
    bool    _useDriverKit = false; // *&*&*&Va_20200903 default is negative
    bool    _BigSur       = false; // V_20210108
    static DeviceManager* _instance;
    
protected:
    std::thread                 _worker;
    CFRunLoopRef                _rlRef;
    io_iterator_t               _iter = { 0 };
    IONotificationPortRef       _notificationPort = { NULL };
    
    
    void*                       _foregroundAppMonitor;
    std::string                 _foregroundAppBinary;
    std::string                 _foregroundAppBundle;
    uint64_t                    _foregroundPid;
    CFRunLoopSourceRef          _rlSourceForegroundApp;
    CFRunLoopSourceRef          _rlSourceSettingsChange;
    CFRunLoopSourceRef          _securityNotify;
    
    std::unique_ptr<ModKeyListener> _modKeyListener;

    // flag will be set to true when initialization is complete
    std::atomic_bool _inited;
    
public:
    static DeviceManager* getInstance();
    static void destroyInstance();
    void activate();
    void activate_tbwDKManager();
    void close_tbwDKManager();

    void start();
    void stop();
    
    // So that Device objects can use the same notification port
    IONotificationPortRef notificationPort()
    { return _notificationPort; }
    
    // Returns number of connected devices
    size_t numConnectedDevices()
    { return _devices.size(); }
    
    // Returns a device with the given product id
    Device* getDevice(uint16_t productId);
    
    // HACK! only for testing
    Device* getFirstDevice();
    
protected:
    DeviceManager();
    ~DeviceManager();
    
    // Initialize the object
    bool init();
    void deinit();

    // thread
    static void sMain(void* context, std::condition_variable& cv, kern_return_t& kr);
    void main(std::condition_variable& cv, kern_return_t& kr);
    
    // New driver arrival callback
    static void _onDeviceArrival(void* refCon, io_iterator_t iterator);
    void onDeviceArrival(io_iterator_t iter, bool acquireMutex);
    
    void onDeviceRemoval(Device* driver);

    static void _handleFrontMostCallback(const char* bundle, const char* binary, uint32_t pid, void* context);
    void handleFrontMostCallback(const char* bundle, const char* binary, uint32_t pid);
    
    static void sForegroundAppChanged(void* info);
    void foregroundAppChanged();
    
    static void sHandleSettingsChange(void* info);
    
    static void sHandleSecurityOpen(void *info);

    void handleSettingsChange();
    
    static void sOnModifierKeyStatusChange(void* ref, uint32_t key, bool Down);
    void onModifierKeyStatusChange(uint32_t key, bool Down);
};
