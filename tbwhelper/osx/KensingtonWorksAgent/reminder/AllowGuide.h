//
//  AllowGuide.h
//  KensingtonWorksHelper
//
//  Created by Kensington on 2020/3/18.
//  Copyright © 2020 kensington. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface AllowGuide : NSWindowController
{
    BOOL bInstalling;
    int  iBillBoardnBtnMode;
}

- (void)runModal;
- (void)topMostMyself;
- (void)setInstallFlagTrue;
- (void)getParam:(int8_t)iMode;// {
    //NSLog(@"%@", parameter);
//}

@property (nonatomic, strong) IBOutlet NSTextField *infoBillboard;
@property (nonatomic, strong) IBOutlet NSButton *checkButton;
//@property (nonatomic, strong) IBOutlet NSButton *lastStepButton;
@end

NS_ASSUME_NONNULL_END
