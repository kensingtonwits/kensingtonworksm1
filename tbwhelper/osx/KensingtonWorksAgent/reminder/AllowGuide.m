//
//  AllowGuide.m
//  KensingtonWorksHelper
//
//  Created by Kensington on 2020/3/18.
//  Copyright © 2020 kensington. All rights reserved.
//

#import "AllowGuide.h"
#include <stdio.h>
#include <CoreServices/CoreServices.h>
#include <Carbon/Carbon.h>
#import <IOKit/hidsystem/IOHIDLib.h>

// strings definition
//#define STRING_REBOOT_PROMPT @"Once the steps above have been completed, restart your computer to activate these changes."
#define STRING_REBOOT_PROMPT @"Security and Privacy Settings for KensingtonWorks are now completed."

#define STR_BTN_TITLE_ALLOW_DRIVER  @"Open “Security & Privacy”"
#define STR_BTN_TITLE_ACCESSIBILITY @"Open “Accessibility”"
#define STR_BTN_TITLE_INPUT_MONITOR @"Open “Input Monitoring”"
#define STR_BTN_TITLE_OPENING_FREEZ @"Next"
#define STR_BTN_TITLE_GOING_NEXT    @"Going next..."
//#define STR_BTN_TITLE_NEXT          @"Next"
#define STR_BTN_TITLE_NEXT          @"Save changes"
#define STR_BTN_TITLE_ACCE_NEXT     @"Save changes"
#define STR_BTN_TITLE_CLOSE         @"Finish"


@interface AllowGuide ()

@end

@implementation AllowGuide

BOOL allowOpened = NO;
BOOL inputOpened = NO;

- (BOOL)windowShouldClose:(id)sender {
    if(iBillBoardnBtnMode == 0)
        exit(1);
    else
        [self.window close];
    return NO;
}



- (BOOL)checkAccessibility:(BOOL)bPrompt; {
    BOOL accessibilityEnabled = NO;
    if(bPrompt == YES)
    {
        NSDictionary *options = @{(__bridge id)kAXTrustedCheckOptionPrompt: @YES};
        accessibilityEnabled = AXIsProcessTrustedWithOptions((CFDictionaryRef)options);
    }
    else
    {
        NSDictionary *options = @{(__bridge id)kAXTrustedCheckOptionPrompt: @NO};
        accessibilityEnabled = AXIsProcessTrustedWithOptions((CFDictionaryRef)options);
    }
    NSLog(@"AllowGuide: checkAccessibility return is %d", accessibilityEnabled);
    return accessibilityEnabled;
}

- (BOOL)checkInputMonitor {
    BOOL bRet = YES;
    if(@available(macos 10.15, *)){
        IOHIDAccessType theType = IOHIDCheckAccess(kIOHIDRequestTypeListenEvent);
        //NSLog(@"isInputMonitoringEnabled - IOHIDCheckAccess = %d", theType);
        switch (theType)
        {
            case kIOHIDAccessTypeGranted: // 0
                NSLog(@"isInputMonitoringEnabled  - kIOHIDAccessTypeGranted");
                bRet = YES;
                break;
            case kIOHIDAccessTypeDenied: // 1
            {
                NSLog(@"isInputMonitoringEnabled  - kIOHIDAccessTypeDenied");
                // denied
                bRet = NO;
                break;
            }
            case kIOHIDAccessTypeUnknown: // 2
            {
                bool result = IOHIDRequestAccess(kIOHIDRequestTypeListenEvent);
                NSLog(@"isInputMonitoringEnabled - IOHIDRequestAccess result = %d", result);
                bRet = NO;
                break;
            }
            default:
                break;
        }
    }
    return bRet;
}

- (void)getParam:(int8_t)iMode; {
    //NSLog(@"AllowGuide - iMode is:%d", iMode);
    iBillBoardnBtnMode = iMode;
    //NSLog(@"AllowGuide - iBillBoardnBtnMode is:%d", iMode);
}

- (void) windowDidLoad {
    [super windowDidLoad];
    NSLog(@"=========AllowGuide is launched here.============");
    // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
    
    switch(iBillBoardnBtnMode)
    {
        case 15:
        case 14:
        case 13:
        case 12:
        case 11:
        case 10:
        case 9:
        case 8:
        case 7:
        case 6:
        case 5: // allow driver first
            self.infoBillboard.stringValue = NSLocalizedString(@"allow_drv_required", @"allow_drv_required");
            self.checkButton.title = NSLocalizedString(@"btn_open_security", @"btn_open_security");;
            break;
        case 4: // special case - only when installing on macOS 10.15 above it will show
            self.infoBillboard.stringValue = NSLocalizedString(@"acce_required",@"acce_required");
            self.checkButton.title = NSLocalizedString(@"btn_open_acce",@"btn_open_acce");
            break;
        case 2: // accessibility is the 2nd priority
            self.infoBillboard.stringValue = NSLocalizedString(@"acce_required",@"acce_required");
            self.checkButton.title = NSLocalizedString(@"btn_open_acce",@"btn_open_acce");
            break;
        case 3:
        case 1:
            self.infoBillboard.stringValue = NSLocalizedString(@"inputM_required",@"inputM_required");
            self.checkButton.title = NSLocalizedString(@"btn_open_inputM",@"btn_open_inputM");
            break;
    }
    //NSTask *task = [[NSTask alloc] init];
    //task.launchPath = @"/usr/bin/kextutil";
    //task.arguments = @[@"-b", @"com.kensington.trackballworks2"];
    //[task launch];
    //task.standardOutput = pipe;    
    
}

#pragma mark - IBAction
// vannes20200323 let's use this function trying to change something!!
- (IBAction)restartBtnClicked:(NSButton *)sender
{
    NSLog(@"=========AllowGuide: Restart btn is clicked!============");
    // TBD: we need to re-check the acce & input monitoring status again when user trying to go next step.
    
    // go minor right to let Security window show up
    NSPoint pos;
    pos.x = [[NSScreen mainScreen] frame].size.width - 1.5 * [self.window frame].size.width;
    pos.y = [self.window frame].origin.y;
    [self.window setFrame:CGRectMake(pos.x, pos.y,
                                     [self.window frame].size.width , [self.window frame].size.height) display:YES];
    
    // Dont be top most
    [self.window setLevel:NSNormalWindowLevel];
    
    // Open options flow: Allow button-->Input Monitoring-->Accessibility
    switch (iBillBoardnBtnMode) {
        case 15:
        case 14:
        case 13:
        case 12:
        case 11:
        case 10:
        case 9:
        case 8:
        case 7:
        case 6:
        case 5:
        {
            if(allowOpened == NO){
                // Open General tab
                NSURL *URL = [NSURL URLWithString:@"x-apple.systempreferences:com.apple.preference.security?General"];
                [[NSWorkspace sharedWorkspace] openURL:URL];
                allowOpened = YES;

                [sender setTitle:NSLocalizedString(@"btn_open_frez",@"btn_open_frez")];
                [sender setEnabled: NO]; // disable button temperarily
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    [sender setEnabled: YES]; // enable the button
                });
            }
            else{
                if(iBillBoardnBtnMode >= 12)
                    iBillBoardnBtnMode -= 8;
                
                iBillBoardnBtnMode -= 4;
                [sender setTitle:NSLocalizedString(@"btn_open_frez",@"btn_open_frez")];

                // change text
                if(iBillBoardnBtnMode == 3 || iBillBoardnBtnMode == 1)
                {
                    self.infoBillboard.stringValue = NSLocalizedString(@"inputM_required",@"inputM_required");
                    [sender setTitle:NSLocalizedString(@"btn_open_inputM",@"btn_open_inputM")];
                }
                else if(iBillBoardnBtnMode == 2)
                {
                    self.infoBillboard.stringValue = NSLocalizedString(@"acce_required",@"acce_required");
                    [sender setTitle:NSLocalizedString(@"btn_open_acce",@"btn_open_acce")];
                }
            }
        }
            break;
        case 4:
            [self.window close];
            break;
        case 3:
        {
            if(![self checkInputMonitor]){ // we re-check it until user enable it correctly
                // Open Input Monitor option
                NSURL *URL = [NSURL URLWithString:@"x-apple.systempreferences:com.apple.preference.security?Privacy_ListenEvent"];
                [[NSWorkspace sharedWorkspace] openURL:URL];
                if(inputOpened == YES)
                    exit(1);
                [sender setTitle:NSLocalizedString(@"btn_open_frez",@"btn_open_frez")];
                [sender setEnabled: NO]; // disable button temperarily
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    //[sender setTitle:STR_BTN_TITLE_NEXT]; // Hint user go next check
                    [sender setTitle:NSLocalizedString(@"btn_next",@"btn_next")]; // Hint user go next check
                    [sender setEnabled: YES]; // enable the button
                });
                inputOpened = YES;
                return;
            }
            else // Input pass so we transfer to next stage
            {
#if FALSE
                exit(1); // vannes20200415
#else
                //[sender setTitle:STR_BTN_TITLE_GOING_NEXT]; // Fake check, actually we just change the billboard andbutton text
                [sender setTitle:NSLocalizedString(@"btn_open_frez",@"btn_open_frez")]; // Fake check, actually we just change the billboard andbutton text
                [sender setEnabled: NO]; // disable button temperarily
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    self.infoBillboard.stringValue = NSLocalizedString(@"acce_required",@"acce_required");
                    //[sender setTitle:STR_BTN_TITLE_ACCESSIBILITY];
                    [sender setTitle:NSLocalizedString(@"btn_open_acce",@"btn_open_acce")];
                    [sender setEnabled: YES]; // enable the button
                    iBillBoardnBtnMode -= 1;
                });
#endif
            }
        }
            break;
        case 2:
        {
            if(![self checkAccessibility:NO]){ // we re-check it until user enable it correctly
                // Open Accessibility option
                NSURL *URL = [NSURL URLWithString:@"x-apple.systempreferences:com.apple.preference.security?Privacy_Accessibility"];
                [[NSWorkspace sharedWorkspace] openURL:URL];
                //[sender setTitle:STR_BTN_TITLE_OPENING_FREEZ];
                [sender setTitle:NSLocalizedString(@"btn_open_frez",@"btn_open_frez")];
                [sender setEnabled: NO]; // disable button temperarily
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    //[sender setTitle:STR_BTN_TITLE_ACCE_NEXT]; // Hint user go next check
                    [sender setTitle:NSLocalizedString(@"btn_next",@"btn_next")]; // Hint user go next check
                    [sender setEnabled: YES]; // enable the button
                });
                return;
            }
            else // Acce pass so we transfer to next stage
            {
//#if TRUE
#if FALSE // vannes20200424 we want a finish message
                exit(1); // vannes20200415
#else
                //[sender setTitle:STR_BTN_TITLE_GOING_NEXT];
                //[sender setTitle:STR_BTN_TITLE_OPENING_FREEZ];
                [sender setTitle:NSLocalizedString(@"btn_open_frez",@"btn_open_frez")];
                [sender setEnabled: NO]; // disable button temperarily
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    //self.infoBillboard.stringValue = STRING_REBOOT_PROMPT;
                    self.infoBillboard.stringValue = NSLocalizedString(@"complete",@"complete");
                    //[sender setTitle:STR_BTN_TITLE_CLOSE];
                    [sender setTitle:NSLocalizedString(@"quit",@"quit")];
                    [sender setEnabled: YES]; // enable the button
                    iBillBoardnBtnMode -= 2;
                });
#endif
            }
        }
            break;
        case 1:
        {
            if(![self checkInputMonitor]){ // we re-check it until user enable it correctly
#if TRUE
                exit(1); // 20200415 if we choose later, quit myslef can satisfy activate inputM as well
#else
                // Open Input monitor option
                NSURL *URL = [NSURL URLWithString:@"x-apple.systempreferences:com.apple.preference.security?Privacy_ListenEvent"];
                [[NSWorkspace sharedWorkspace] openURL:URL];
                //[sender setTitle:STR_BTN_TITLE_OPENING_FREEZ];
                [sender setTitle:NSLocalizedString(@"btn_open_frez",@"btn_open_frez")];
                [sender setEnabled: NO]; // disable button temperarily
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    //[sender setTitle:STR_BTN_TITLE_NEXT]; // Hint user go next check
                    [sender setTitle:NSLocalizedString(@"btn_next",@"btn_next")]; // Hint user go next check
                    [sender setEnabled: YES]; // enable the button
                });
                return;
#endif
            }
            else // Acce pass so we transfer to next stage
            {
#if TRUE
                exit(1); // vannes20200415
#else
                //[sender setTitle:STR_BTN_TITLE_GOING_NEXT]; // Fake check, actually we just change the billboard andbutton text
                [sender setTitle:NSLocalizedString(@"btn_open_frez",@"btn_open_frez")]; // Fake check, actually we just change the billboard andbutton text
                [sender setEnabled: NO]; // disable button temperarily
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                    //self.infoBillboard.stringValue = STRING_REBOOT_PROMPT;
                    self.infoBillboard.stringValue = NSLocalizedString(@"complete",@"complete");;
                    //[sender setTitle:STR_BTN_TITLE_CLOSE];
                    [sender setTitle:NSLocalizedString(@"quit",@"quit")];
                    [sender setEnabled: YES]; // enable the button
                    iBillBoardnBtnMode -= 1;
                });
#endif
            }
        }
            break;
        case 0:
        {
            [self.window close];
            exit(1); // quit myself and system will re-launch me soon
        }
            break;
        //case -1:
            //[self.window close];
            // SendAppleEventToSystemProcess(kAERestart); //reboot your computer, don't do it temperaroly
            //break;
    }
}

#pragma mark - control function
- (void)runModal {
    NSLog(@"=========AllowGuide: runModal is called!============");
    [self showWindow:self];
}

- (NSString *)windowNibName {
    NSLog(@"=========AllowGuide: windowNibName is called!============");
    bInstalling = FALSE;
    return NSStringFromClass([AllowGuide class]);
}

- (void)topMostMyself{
    //NSLog(@"=========AllowGuide: topMostMyself is starting!============");
    [self.window makeKeyAndOrderFront:self];
    [self.window setLevel:NSStatusWindowLevel]; // This make itself always on top
    
    // vannes20200414 Here we set a flag in the tmp folder indicating that no more show this dialog again
    // after it's closed. This flag will be cleared by system on next launch. By this setting the allow driver
    // guide page will be only showned one time in each system running life.
    NSString *content = @"allow guide dialog is launched.";
    NSData *fileContents = [content dataUsingEncoding:NSUTF8StringEncoding];
    [[NSFileManager defaultManager] createFileAtPath:@"/tmp/kgtwallow"
                                    contents:fileContents
                                    attributes:nil];
    
}

- (void)setInstallFlagTrue {
    NSLog(@"=========AllowGuide: setInstallFlagTrue is called!============");
    bInstalling = TRUE;
}

// Reboot function
OSStatus SendAppleEventToSystemProcess(AEEventID EventToSend)
{
    AEAddressDesc targetDesc;
    static const ProcessSerialNumber kPSNOfSystemProcess = { 0, kSystemProcess };
    AppleEvent eventReply = {typeNull, NULL};
    AppleEvent appleEventToSend = {typeNull, NULL};
    
    OSStatus error = noErr;
    
    error = AECreateDesc(typeProcessSerialNumber, &kPSNOfSystemProcess,
                         sizeof(kPSNOfSystemProcess), &targetDesc);
    
    if (error != noErr)
    {
        return(error);
    }
    
    error = AECreateAppleEvent(kCoreEventClass, EventToSend, &targetDesc,
                               kAutoGenerateReturnID, kAnyTransactionID, &appleEventToSend);
    
    AEDisposeDesc(&targetDesc);
    if (error != noErr)
    {
        return(error);
    }
    
    error = AESend(&appleEventToSend, &eventReply, kAENoReply,
                   kAENormalPriority, kAEDefaultTimeout, NULL, NULL);
    
    AEDisposeDesc(&appleEventToSend);
    if (error != noErr)
    {
        return(error);
    }
    
    AEDisposeDesc(&eventReply);
    
    return(error);
}

@end
