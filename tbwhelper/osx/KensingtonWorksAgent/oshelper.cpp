#include <stdlib.h>
#include <string.h>

#include <CoreAudio/CoreAudio.h>
#include <AudioToolbox/AudioToolbox.h>
#include <AudioToolbox/AudioServices.h>
#include <AppKit/AppKit.h>
#include <CoreGraphics/CGEvent.h>

static AudioDeviceID getDefaultOutputDeviceID()
{
    AudioDeviceID outputDeviceID = kAudioObjectUnknown;

    // get output device device
    OSStatus status = noErr;
    AudioObjectPropertyAddress propertyAOPA;
    propertyAOPA.mScope = kAudioObjectPropertyScopeGlobal;
    propertyAOPA.mElement = kAudioObjectPropertyElementMaster;
    propertyAOPA.mSelector = kAudioHardwarePropertyDefaultOutputDevice;

    if (!AudioObjectHasProperty(
                kAudioObjectSystemObject, 
                &propertyAOPA))
    {
        printf("Cannot find default output device!");
        return outputDeviceID;
    }

    status = AudioObjectGetPropertyData(
            kAudioObjectSystemObject, 
            &propertyAOPA, 
            0, 
            NULL, 
            (UInt32[]){sizeof(AudioDeviceID)}, 
            &outputDeviceID);

    if (status != 0)
    {
        printf("Cannot find default output device!");
    }
    return outputDeviceID;
}

float getVolume()
{
    Float32 outputVolume;

    OSStatus status = noErr;
    AudioObjectPropertyAddress propertyAOPA;
    propertyAOPA.mElement = kAudioObjectPropertyElementMaster;
    propertyAOPA.mSelector = kAudioHardwareServiceDeviceProperty_VirtualMasterVolume;
    propertyAOPA.mScope = kAudioDevicePropertyScopeOutput;

    AudioDeviceID outputDeviceID = getDefaultOutputDeviceID();

    if (outputDeviceID == kAudioObjectUnknown)
    {
        printf("Unknown device");
        return 0.0;
    }

    if (!AudioObjectHasProperty(outputDeviceID, &propertyAOPA))
    {
        printf("No volume returned for device 0x%0x", outputDeviceID);
        return 0.0;
    }

    status = AudioObjectGetPropertyData(
            outputDeviceID, 
            &propertyAOPA, 
            0, 
            NULL, 
            (UInt32[]){sizeof(Float32)}, 
            &outputVolume);

    if (status)
    {
        printf("No volume returned for device 0x%0x", outputDeviceID);
        return 0.0;
    }

    if (outputVolume < 0.0 || outputVolume > 1.0) return 0.0;

    return outputVolume;
}

void setVolume(float volume)
{
    AudioDeviceID outputDeviceID;
    AudioObjectPropertyAddress propertyAddress = { 
        kAudioHardwareServiceDeviceProperty_VirtualMasterVolume, 
        kAudioDevicePropertyScopeOutput,
        kAudioObjectPropertyElementMaster 
    };

    outputDeviceID = getDefaultOutputDeviceID();

    // Deprecated API. Switch to AudioObjectSetPropertyData
    /*
    AudioHardwareServiceSetPropertyData(outputDeviceID, 
                                        &propertyAddress, 
                                        0, 
                                        NULL, 
                                        sizeof(Float32),
                                        &volume);
    */
    AudioObjectSetPropertyData(
            outputDeviceID,
            &propertyAddress,
            0,
            NULL,
            sizeof(Float32),
            &volume);
}

double GetDoubleTime()
{
    return [NSEvent doubleClickInterval];
}

/*
from https://stackoverflow.com/questions/11045814/emulate-media-key-press-on-mac
import Quartz

# NSEvent.h
NSSystemDefined = 14

# hidsystem/ev_keymap.h
NX_KEYTYPE_SOUND_UP = 0
NX_KEYTYPE_SOUND_DOWN = 1
NX_KEYTYPE_PLAY = 16
NX_KEYTYPE_NEXT = 17
NX_KEYTYPE_PREVIOUS = 18
NX_KEYTYPE_FAST = 19
NX_KEYTYPE_REWIND = 20

def HIDPostAuxKey(key):
    def doKey(down):
        ev = Quartz.NSEvent.otherEventWithType_location_modifierFlags_timestamp_windowNumber_context_subtype_data1_data2_(
            NSSystemDefined, # type
            (0,0), # location
            0xa00 if down else 0xb00, # flags
            0, # timestamp
            0, # window
            0, # ctx
            8, # subtype
            (key << 16) | ((0xa if down else 0xb) << 8), # data1
            -1 # data2
            )
        cev = ev.CGEvent()
        Quartz.CGEventPost(0, cev)
    doKey(True)
    doKey(False)

for _ in range(10):
    HIDPostAuxKey(NX_KEYTYPE_SOUND_UP)
HIDPostAuxKey(NX_KEYTYPE_PLAY)
*/
void HIDPostAuxKey(uint32_t key, bool down)
{
    @autoreleasepool {
    
        NSEvent* ev = [NSEvent otherEventWithType:NSEventTypeSystemDefined
                                location:NSZeroPoint
                           modifierFlags:(down ? 0xa00 : 0xb00)
                               timestamp:0
                            windowNumber:0
                                 context:nil
                                 subtype:8
                                   data1:(key << 16)| ((down ? 0xa : 0xb) << 8)
                                   data2:-1
                                   ];
        CGEventPost(kCGHIDEventTap, [ev CGEvent]);
    }
}

#if 0
int main(int argc, char* argv[])
{
    if (argc < 2) {
        printf("Please specificy volume to set as argument\n");
        return 1;
    }

    float volume = (float)atof(argv[1]);
    printf("Current volume: %f, setting volume to %f\n", getVolume(), volume);

    setVolume(volume);

    return 0;
}
#endif

