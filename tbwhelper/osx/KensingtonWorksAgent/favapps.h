#pragma once

#include <set>
#include <string>

#include "json.hpp"

nlohmann::json getFavApps(int maxAppCount, std::set<std::string> const& exclude);

