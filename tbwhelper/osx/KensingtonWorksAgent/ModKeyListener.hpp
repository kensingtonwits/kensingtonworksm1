//
//  ModKeyListener.hpp
//  tbwhelper
//
//  Created by Hariharan Mahadevan on 2019/5/18.
//  Copyright © 2019 Kensington. All rights reserved.
//

#ifndef ModKeyListener_hpp
#define ModKeyListener_hpp

#include <ApplicationServices/ApplicationServices.h>

typedef void (*PFN_MODIFIER_KEY_STATUS_CHANGE)(void* ref, uint32_t uKey, bool fDown);

class ModKeyListener {

protected:
    CFMachPortRef       _eventTap;
    CFRunLoopSourceRef  _rlSource;
    CFRunLoopMode       _rlMode;
    
    uint32_t            _modifierStatus;
    
    PFN_MODIFIER_KEY_STATUS_CHANGE
                        _pfnCallback;
    void*               _refCallback;
    
    static CGEventRef sEventCallback(
        CGEventTapProxy proxy,
        CGEventType type,
        CGEventRef event,
        void *refcon);
    CGEventRef eventCallback(
        CGEventTapProxy proxy,
        CGEventType type,
        CGEventRef event);

public:
    ModKeyListener() = delete;
    ModKeyListener(const ModKeyListener&) = delete;
    ModKeyListener& operator=(const ModKeyListener&) = delete;    

    ModKeyListener(CFRunLoopMode rlMode, PFN_MODIFIER_KEY_STATUS_CHANGE pfnCallback, void* refCallback);
    virtual ~ModKeyListener();
    
    void enable();
    void disable();
    bool isEnabled();
    
    uint32_t getModifierStatus()
    { return _modifierStatus; }
};

#endif /* ModKeyListener_hpp */
