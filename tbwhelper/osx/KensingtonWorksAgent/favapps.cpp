#include "favapps.h"

#include <memory>
#include <chrono>
#include <thread>
#include <atomic>
#include <boost/log/trivial.hpp>

#include <Cocoa/Cocoa.h>

using namespace nlohmann;


struct App {
	std::string _program;
	std::string _name;
	std::string _icon;
	App(const char* program, const char* name, const char* iconBase64)
		: _program(program), _name(name), _icon(iconBase64)
	{}
   
	int operator<(const App& rhs) const
	{
		return _program < rhs._program;
	}
};
void to_json(json& j, App const& pgm)
{
	j["program"] = pgm._program;
	j["name"] = pgm._name;
	j["icon"] = pgm._icon;
}

/**
 * Returns a boolean indicating if the supplied bundle path
 * or executable path is a system app.
 *
 * Pure C function so that it's callable from Obj-C
 */
static bool isSystemApp(const char* pszBundlePath)
{
    // All the standard system paths
    const char* sysPaths[] = {
        "/System", "/Library",
        "/usr", "/etc", "/bin", "/sbin", "/var", "/net", "/tmp", "/cores", "/private"
    };
    
    for (auto sysPath: sysPaths) {
        if (::strstr(pszBundlePath, sysPath) != nullptr)
            return true;
    }
    return false;
}

/**
 * Top level app exclusion routine
 *
 * Pure C function so that it's callable from Obj-C
 */
static bool isExcludedApp(const char* pszBundlePath)
{
    if (isSystemApp(pszBundlePath))
        return true;
    
    const char* selfBin[] = { "KensingtonWorks", "tbwhelper" };
    for (auto pgm: selfBin) {
        if (::strstr(pszBundlePath, pgm) != nullptr)
            return true;
    }
    
    const char* osHelpers[] = {
        "iTunesHelper.app"
    };
    for (auto pgm: osHelpers) {
        if (::strstr(pszBundlePath, pgm) != nullptr)
            return true;
    }
    
    // if pszBundlePath doesn't end with .app eclude that too
    auto len = ::strlen(pszBundlePath);
    if (::strcmp(&pszBundlePath[len-4], ".app") != 0) {
        return true;
    }
    
    return false;
}

/**
 * Returns the 32x32 icon image as a base64 encoded string.
 */
static std::string getAppIconFromImage(NSImage* image)
{
    [image lockFocus];
    NSBitmapImageRep *bitmapRep = [[NSBitmapImageRep alloc] initWithFocusedViewRect:NSMakeRect(0, 0, 32, 32)];
    [image unlockFocus];
    NSDictionary *imageProps = [NSDictionary dictionaryWithObject:[NSNumber numberWithFloat:1.0] forKey:NSImageCompressionFactor];
    NSData *imageData = [bitmapRep representationUsingType:NSPNGFileType properties:imageProps];;
    if (imageData) {
        NSString *base64String = [imageData base64EncodedStringWithOptions:0];
        //NSLog(@"Icon image: %@", base64String);
        const char* icon = [base64String UTF8String];
        return std::string(icon);
    }
    return std::string();
}

/**
 * Popuplates jApps with running applications.
 *
 * Returns number of apps added to jApps.
 */
static int getRunningApps(int maxAppCount, std::set<std::string> const& exclude, std::vector<App>& apps)
{
    int nAppsAdded = 0;
    
    @autoreleasepool {
        NSArray *appsArray = [[NSWorkspace sharedWorkspace] runningApplications];

        for (NSRunningApplication *a  in appsArray) {

            const char* name = [[a localizedName] UTF8String];
            const char* program = [[a bundleURL] fileSystemRepresentation];
            const char* bundle = [[a bundleIdentifier] UTF8String];
            const char* executable = [[[a executableURL] path] UTF8String];
            const char* pc = ::strrchr(executable, '/');
            if (pc) {
                executable = pc+1;
            }

            char programIdentifier[512] = {0};
            snprintf(programIdentifier, sizeof(programIdentifier), "%s-%s", bundle, executable);
            //NSLog(@"Name: %@, programIdentifier: %s", [a localizedName], programIdentifier);

            // filter programs from /System folder
            if (!name || !program) {
                //NSLog(@"getRunningApps - name|program == null");
                continue;
            }
            
             if (isExcludedApp(program)) {
                //NSLog(@"getRunningApps - isExcludedApp");
                continue;
            }

            // filter those from exclude list
            if (std::find(exclude.begin(), exclude.end(), std::string(programIdentifier)) != exclude.end()) {
                //NSLog(@"getRunningApps - program in exclude list");
                continue;
            }
            
            //NSLog(@"Icon: %@", [a icon]);
            NSRect rect;  // contains an origin, width, height
            rect = NSMakeRect(0, 0, 32, 32);
            NSImage* image = [a icon];
            if (!image) {
                //NSLog(@"getRunningApps - program has no icon");
                continue;
            }
            auto imageStr = getAppIconFromImage(image);
            if (imageStr.length() == 0) {
                //NSLog(@"getRunningApps - program icon is empty");
                continue;
            }
            
            apps.push_back(App(programIdentifier, name, getAppIconFromImage(image).c_str()));

            nAppsAdded++;
            if (nAppsAdded == maxAppCount)
                break;
        }
    }

    return nAppsAdded;
}

/**
 * This is the objective-c version of the App C++ class that is defined
 * at the top of this file. Since installed app enumeration has to be
 * done from Objective-C code that is run from the main program's thread,
 * we need a means to store the enumerated results in Obj-C native form.
 * This class servers that that purpose.
 *
 * It's essentially a container for three strings:
 *  - application's path
 *  - its name
 *  - it's 32x32 icon, encoded as a base64 string.
 */
@interface InstalledApp: NSObject
{
    NSString* _path;
    NSString* _name;
    NSString* _icon;
}
- (void)initValues:(NSString*)path nameStr:(NSString*)name iconStr:(NSString*)icon;
- (NSString*)path;
- (NSString*)name;
- (NSString*)icon;
@end

@implementation InstalledApp
-(void)initValues:(NSString*)path nameStr:(NSString*)name iconStr:(NSString*)icon
{
    _path = path;
    _name = name;
    _icon = icon;
}

- (NSString*)path
{
    return _path;
}

- (NSString*)name;
{
    return _name;
}

- (NSString*)icon;
{
    return _icon;
}
@end

/**
 * Class that enumerates the installed applications in the system.
 * It achieves this by using Metadata query using the query string
 *
 *  kMDItemContentType == 'com.apple.application-bundle'
 *
 * Key to using this class is that it's initiateSearch() method,
 * which starts the query, has to be executed from the application
 * main thread (usually the first thread created by the app upon
 * start).
 *
 * Also this thread should have an active message pump.
 *
 * Therefore, if this class is called from a worker thread, as is
 * the case in our program, it has to be called using the async
 * dispatch mechanism as below:
 *
 * dispatch_async(dispatch_get_main_queue(), ^(void){
 *       @autoreleasepool {
 *           [s_appEnum initiateSearch];
 *       }
 *   });
 *
 * Note that the object itself can be constructed from the worker
 * thread.
 *
 * Another caveat is the object lifetime. Since the method is executed
 * using async mechanism from a different thread, the caller must 
 * ensure that the object remains valid until the async operation
 * completes.
 *
 * In our code we achieve this by declaring the AppEnumerator object
 * as a global variable, which ensures that Obj-C automatic reference
 * counted garbage collector does not reclaim the object until, we
 * assign the global variable to nullptr.
 *
 * In a nutshell, a tricky piece of code that needs to be carefully
 * managed, if making any changes.
 *
 * Finally upon completion of the query, the results are stored
 * in _apps member, which is a mutable array of InstalledApp objects
 * declared earlier.
 */
@interface AppEnumerator : NSObject
{
    NSMetadataQuery* _query;
    long _maxCount;
    NSMutableArray* _exclude; // array of NSString
    NSMutableArray* _apps;
}
- (NSArray*)apps;
- (instancetype) init;

/**
 * Set the maximum number of apps to store in _apps.
 */
- (void) setMaxCount:(NSInteger)maxCount;

/**
 * Add app the exclusion list. Apps in this list will not be
 * added to the _apps array.
 */
- (void) excludeApp:(NSString*)appPath;

/**
 * Start search -- call from application main thread. See class comments
 * above.
 */
- (void) initiateSearch;

/**
 * Callback routine.
 */
- (void) gatherComplete:(id)sender;

/*
 * Returns a boolean indicating if the query is still active.
 * Caller can repeatedly call this method to determine when the
 * query has completed and start retrieving the enumerated app list.
 */
- (BOOL) isRunning;
@end

@implementation AppEnumerator
- (NSArray*)apps
{
    return _apps;
}

- (instancetype) init
{
    if (self = [super init]) {
        // custom initialization
        _maxCount = 10;
        _exclude = [[NSMutableArray alloc] init];
        _apps = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void) setMaxCount:(NSInteger)maxCount
{
    _maxCount = maxCount;
}

- (void) excludeApp:(NSString*)appPath
{
    [_exclude insertObject:appPath atIndex:[_exclude count]];
}

- (void) initiateSearch
{
    _query = [[NSMetadataQuery alloc] init];
    [_query setSearchScopes: @[@"/Applications"]];  // if you want to isolate to Applications
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"kMDItemContentType == 'com.apple.application-bundle'"];
    [_query setPredicate:pred];

    // Register for NSMetadataQueryDidFinishGatheringNotification here because you need that to
    // know when the query has completed
     [[NSNotificationCenter defaultCenter] addObserver:self
        selector:@selector(gatherComplete:)
        name:NSMetadataQueryDidFinishGatheringNotification
        object:nil];

    [_query startQuery];
}

- (void)gatherComplete:(id)sender
{
    //NSLog(@"Gather complete, results count: %lu", (unsigned long)[_query resultCount]);

    NSUInteger nAdded = 0;
    NSUInteger i=0;
    for (i=0; i < [_query resultCount] && nAdded < _maxCount; i++) {
        NSMetadataItem *theResult = [_query resultAtIndex:i];
        NSString *displayName = [theResult valueForAttribute:(NSString *)kMDItemDisplayName];
        NSString *appPath = [theResult valueForAttribute:(NSString *)kMDItemPath];
        NSImage* pImage = [[NSWorkspace sharedWorkspace] iconForFile:appPath];
        //NSLog(@"Enumerated app at %lu - name: %@, path: %@", i, displayName, appPath);

        NSBundle * appBundle = [NSBundle bundleWithPath:appPath];
        char programIdentifier[512] = {0};
        NSString* nsProgramIdentifier = nil;
        if (appBundle) {
            const char* bundle = [[appBundle bundleIdentifier] UTF8String];
            const char* executable = [[[appBundle executableURL] path] UTF8String];
            const char* pc = ::strrchr(executable, '/');
            if (pc) {
                executable = pc+1;
            }
            snprintf(programIdentifier, sizeof(programIdentifier), "%s-%s", bundle, executable);
            nsProgramIdentifier = [NSString stringWithCString:programIdentifier encoding:NSUTF8StringEncoding];
            //NSLog(@"\tprogramIdentifier: %@", nsProgramIdentifier);
        }
      
        // Internal exclusion check. Note that this is to be done with the app's path
        if (isExcludedApp([appPath UTF8String])) {
            //NSLog(@"Enumerated app %@ is std excluded app", appPath);
            continue;
        }

        // External exclusion list check. This is to be done with app identifier, which is
        // <bundle_id>-<executable>
        NSUInteger j = 0;
        if (nsProgramIdentifier) {
            for (j=0; j < [_exclude count]; j++) {
                NSString* excludeApp = [_exclude objectAtIndex:j];
                if ([nsProgramIdentifier isEqualToString:excludeApp]) {
                    //NSLog(@"Ignoring enumerated app %@ in exclude list", appPath);
                    break;
                }
            }
        }

        // we exhausted all the apps in the exclude list
        // means, it's safe to add it
        if (j == [_exclude count]) {
            //NSLog(@"\tAdding installed app %@", appPath);
            NSString* imageStr = [[NSString alloc] initWithUTF8String:getAppIconFromImage(pImage).c_str()];
            //NSLog(@"Image str: %s", imageStr);
            InstalledApp* pApp = [[InstalledApp alloc] init];
            [pApp initValues:nsProgramIdentifier nameStr:displayName iconStr:imageStr];
            [_apps insertObject:pApp atIndex:[_apps count]];
            nAdded++;
        }
    }
    
    [_query stopQuery];
}

- (BOOL)isRunning
{
    return ![_query isStopped];
}
@end

// Global instance, necessary to maintain object lifetime
// across async call duration.
AppEnumerator* s_appEnum = nullptr;

/**
 * Popuplates jApps with installed applications.
 *
 * Returns number of apps added to jApps.
 */
static int getInstalledApps(int maxAppCount, std::vector<std::string> const& exclude, std::vector<App>& apps)
{
    //BOOST_LOG_TRIVIAL(debug) << "Enumerating installed apps, max: " << maxAppCount;

    s_appEnum = [[AppEnumerator alloc] init];
    [s_appEnum setMaxCount:(NSInteger)maxAppCount];
    for (auto& app: exclude) {
        [s_appEnum excludeApp:[NSString stringWithCString: app.c_str() encoding: NSUTF8StringEncoding]];
    }
    dispatch_async(dispatch_get_main_queue(), ^(void){
        @autoreleasepool {
            [s_appEnum initiateSearch];
        }
    });

    // wait till app enumeration is done or timeout (5 seconds) happens
    //BOOST_LOG_TRIVIAL(debug) << "Waiting for installed app enumeration to be completed...";
    auto start = std::chrono::high_resolution_clock::now();
    while ([s_appEnum isRunning]) {
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
        std::chrono::duration<double, std::milli> elapsed =
            std::chrono::high_resolution_clock::now()-start;
        if (elapsed.count() > 5*1000) {
            BOOST_LOG_TRIVIAL(warning) << "AppEnumerator - maximum wait time exceeded, breaking";
            break;
        }
    }
    
    //BOOST_LOG_TRIVIAL(debug) << "Enumerated installed apps, adding it to JSON...";
    [[s_appEnum apps] enumerateObjectsUsingBlock:^(InstalledApp * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        //NSLog(@"Installed app - pgm: %@, name: %@", [obj path], [obj name]);
        apps.push_back(App([[obj path] UTF8String], [[obj name] UTF8String], [[obj icon] UTF8String]));
    }];
    //BOOST_LOG_TRIVIAL(debug) << "Setting s_appEnum to nullptr..";
    s_appEnum = nullptr;
    
    return maxAppCount;
}

nlohmann::json getFavApps(int maxAppCount, const std::set<std::string>& exclude)
{
    json jApps = json::array();
  
    std::vector<App> apps;
    int nApps = 0;
    nApps = getRunningApps(maxAppCount, exclude, apps);
    if (nApps < maxAppCount) {
        std::vector<std::string> ex(exclude.begin(), exclude.end());
        for (auto& app: apps) {
            ex.push_back(app._program);
        }
        getInstalledApps(maxAppCount-nApps, ex, apps);
    }

    BOOST_LOG_TRIVIAL(debug) << "App list:-";
    for (auto& app: apps) {
        BOOST_LOG_TRIVIAL(debug) << "\t" << app._name << ", pgm: " << app._program;// << ", icon: " << app._icon;
        jApps.push_back(app);
    }

    return jApps;
}

