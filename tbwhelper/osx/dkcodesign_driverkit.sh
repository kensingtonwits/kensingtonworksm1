#!/bin/bash

# Replace with your identity
readonly CODE_SIGN_IDENTITY=30BE8DC925C6C8744378B645995F308911C90E35

set -e # forbid command failure

#
# Sign com.kensington.tbwDKdriver.dext
#

#
# Sign KensingtonWorksHelper.app
#

ori_driver_build_fold="build/Release-driverkit/com.kensington.tbwDKDriver.dext"

app_sys_extension_fold=build/Release/KensingtonWorksHelper.app/Contents/Library/SystemExtensions
if [ ! -d "${app_sys_extension_fold}" ]; then
  mkdir -p "${app_sys_extension_fold}"
  cp -R "${ori_driver_build_fold}" "${app_sys_extension_fold}/"
else
  cp -R "${ori_driver_build_fold}" "${app_sys_extension_fold}/"
fi

# Embed provisioning profile
cp \
    tbw_DK_driver_profile-2.provisionprofile \
    "${app_sys_extension_fold}/com.kensington.tbwDKDriver.dext/embedded.provisionprofile"


# Sign
codesign \
    --sign $CODE_SIGN_IDENTITY \
    --entitlements tbwDKDriver/tbwDKDriver.entitlements \
    --options runtime \
    --verbose \
    --force \
    "${app_sys_extension_fold}/com.kensington.tbwDKDriver.dext"

# Embed provisioning profile
cp \
    KensingtonWorks_using_DriverKit.provisionprofile  \
    build/Release/KensingtonWorksHelper.app/Contents/embedded.provisionprofile

# Sign
codesign \
    --sign $CODE_SIGN_IDENTITY \
    --entitlements KensingtonWorksAgent/KensingtonWorksAgent.entitlements \
    --options runtime \
    --verbose \
    --force \
    build/Release/KensingtonWorksHelper.app
