#!/bin/bash

# Replace with your identity
readonly CODE_SIGN_IDENTITY=30BE8DC925C6C8744378B645995F308911C90E35

set -e # forbid command failure

#
# Sign KensingtonWorksHelper.app
#

# Embed provisioning profile
cp \
    KensingtonWorks_using_DriverKit.provisionprofile  \
    build/Debug/KensingtonWorksHelper.app/Contents/embedded.provisionprofile

# Sign
codesign \
    --sign $CODE_SIGN_IDENTITY \
    --entitlements KensingtonWorksAgent/KensingtonWorksAgent.entitlements \
    --options runtime \
    --verbose \
    --force \
    build/Debug/KensingtonWorksHelper.app
