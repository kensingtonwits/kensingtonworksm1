# replace old helper and activate new
launchctl unload /Library/LaunchAgents/com.kensington.trackballworks.plist
sudo rm -R /Applications/KensingtonWorks\ .app/Contents/Helper/KensingtonWorksHelper.app/
sudo cp -R build/Release/KensingtonWorksHelper.app /Applications/KensingtonWorks\ .app/Contents/Helper/
launchctl load /Library/LaunchAgents/com.kensington.trackballworks.plist