rm -f -R build/Debug/KensingtonWorksHelper.app
xcodebuild clean build CODE_SIGN_IDENTITY="" CODE_SIGNING_REQUIRED=NO -alltargets -configuration Debug build
bash dkcodesign-debug.sh
# replace old helper and activate new
launchctl unload /Library/LaunchAgents/com.kensington.trackballworks.plist
#sudo rm -R /Applications/KensingtonWorks\ .app/Contents/Helper/KensingtonWorksHelper.app/
sudo rm -R "/Library/Application Support/Kensington/KensingtonWorks2/KensingtonWorksHelper.app/"
#sudo cp -R build/Debug/KensingtonWorksHelper.app /Applications/KensingtonWorks\ .app/Contents/Helper/
sudo cp -R Build/Debug/KensingtonWorksHelper.app "/Library/Application Support/Kensington/KensingtonWorks2/"
launchctl load /Library/LaunchAgents/com.kensington.trackballworks.plist