# replace old helper and activate new
launchctl unload /Library/LaunchAgents/com.kensington.trackballworks.plist
sudo rm -R "/Library/Application Support/Kensington/KensingtonWorks2/KensingtonWorksHelper.app/"
sudo cp -R build/Release/KensingtonWorksHelper.app "/Library/Application Support/Kensington/KensingtonWorks2/"
launchctl load /Library/LaunchAgents/com.kensington.trackballworks.plist