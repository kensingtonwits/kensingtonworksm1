// Turn off precompiled header!
#include "stdafx.h"
#ifdef __APPLE__
#include <Carbon/Carbon.h>
#else
#include <windows.h>
#endif

#include <boost/tokenizer.hpp>
#include <boost/algorithm/string.hpp>

#include "Modifiers.h"

static struct ScanCode2Modifier {
    uint16_t    scanCode;
    uint32_t    modKey;
} sc2mod[] = {
#ifdef __APPLE__
    { kVK_Shift, MAC_K_SHIFT },      // Left.Shift
    { kVK_Control, MAC_K_CTRL },       // Left.Ctrl
    { kVK_Option, MAC_K_OPTION },     // Left.Option
    { kVK_Command, MAC_K_COMMAND },    // Left.Command
    { kVK_RightCommand, MAC_K_COMMAND },    // Right.Command
    { kVK_RightOption, MAC_K_OPTION },     // Right.Option
    { kVK_RightShift, MAC_K_SHIFT },      // Right.Shift
    { kVK_RightControl, MAC_K_CTRL }        // Right.Ctrl
#else
    { 0, 0 }
#endif
};

// OS specific scancode to TBW modifier
inline uint32_t ScanCodeToTBWModifier(uint16_t scanCode)
{
    for (size_t i=0; i<sizeof(sc2mod)/sizeof(sc2mod[0]); i++) {
        if (sc2mod[i].scanCode == scanCode)
            return sc2mod[i].modKey;
    }
    
    return 0;
}

// Convert from TBW modifier to OS specific scan code
inline uint16_t TBWModifierToScanCode(uint32_t modifier)
{
    for (size_t i=0; i<sizeof(sc2mod)/sizeof(sc2mod[0]); i++) {
        if (sc2mod[i].modKey == modifier)
            return sc2mod[i].scanCode;
    }
    
    return 0;
}

/*
#define WIN_K_SHIFT		0x01
#define WIN_K_APPS		0x02   
#define WIN_K_CTRL		0x04
#define WIN_K_ALT		0x10
#define WIN_K_ALTGR     WIN_K_CTRL|WIN_K_ALT
#define WIN_K_WIN		0x40
#define WIN_K_LWIN		0x40
#define WIN_K_RWIN		0x80

// Mac

#define MAC_MODIDIFER_BITS  0xFF00

#define MAC_K_COMMAND   0x0100  // = 256
#define MAC_K_SHIFT     0x0200  // = 512
#define MAC_K_OPTION    0x0800  // = 2048
#define MAC_K_CTRL      0x1000  // = 4096
*/

struct _ModifiersMap {
    uint32_t bitMask;
    const char* text;
} modifiersTable[] = {
#ifdef _WINDOWS
    { WIN_K_ALT, "alt" },
    { WIN_K_CTRL, "ctrl" },
    { WIN_K_SHIFT, "shift" },
    { WIN_K_WIN, "win" }
#elif __APPLE__
    { MAC_K_OPTION, "alt" },
    { MAC_K_COMMAND, "cmd" },
    { MAC_K_FN, "fn" },
    { MAC_K_CTRL, "ctrl" },
    { MAC_K_SHIFT, "shift" },
    { MAC_K_SPECIAL, "special" }
#endif
};

std::string modifiersToText(uint32_t modifiers)
{
    std::string sModifiers;
    
    for (auto& mapentry: modifiersTable)
    {
        if (modifiers & mapentry.bitMask)
        {
            if (sModifiers.length() > 0)
                sModifiers += ",";
            sModifiers += mapentry.text;
        }
    }
    
    return sModifiers;
}

uint32_t modifiersFromText(const char* pszModifiers)
{
    uint32_t modifiers = 0;
    boost::char_separator<char> sep(",");
    std::string sModifiers(pszModifiers);
    boost::tokenizer<boost::char_separator<char>> tokens(sModifiers, sep);
    for (auto& tok : tokens)
    {
        std::string token(tok);
        boost::trim(token);
        boost::to_lower(token);
        for (auto& mapEntry: modifiersTable)
        {
            if (token.compare(mapEntry.text) == 0)
            {
                modifiers |= mapEntry.bitMask;
            }
        }
    }
    
    return modifiers;
}

