﻿// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#ifdef _WINDOWS
#define NOMINMAX

#include "targetver.h"
// C RunTime Header Files
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>

// Windows Header Files:
#include <windows.h>
#include <winioctl.h>
#include <atlbase.h>
#include <atlfile.h>
#include <atlsync.h>

#include <malloc.h>
#include <memory.h>
#include <tchar.h>


// TODO: reference additional headers your program requires here
#include <shellapi.h>
#include <assert.h>

#include <map>
#include <string>
#include <vector>

#define COMMAND_MESSAGE_QUEUE

#else	// #ifdef _WINDOWS
// Mac
#define COMMAND_MESSAGE_QUEUE
#endif	// #ifdef _WINDOWS
