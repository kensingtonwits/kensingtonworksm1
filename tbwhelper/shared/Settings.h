/*
 * Settings.h
 *
 *  Cross platform abstraction of app setting objects. Class hierarchy
 *  in terms of parent-child container relationship, rather than C++ 
 *  inheritence, looks like:
 *
 *      Settings
 *          [ApplicationConfig]
 *              [DeviceConfig]
 *                  [ActionBase*] --> through shared_ptr<ActionBase>
 *
 *  Settings support serialization to/from TbwSettings.json. The location of
 *  this file is platform dependent and determined by the default application 
 *  preferences location for the platform, which is provided by PlatformServices
 *  singleton.
 *
 *  The top level Settings class is meant to be instantiated wherever necessary
 *  and as such does not provide any mechanism to make it multi-thread safe.
 */

/*

 25/6/2019
 
 Ultimately we should have a simplified command representation in the
 user settings file so that these can be translated at the platform level.
 
 Also, for each command, we should support label field, which can be assigned
 by the user.
 
 For Example:
 */
#pragma once

#include <string>
#include <map>
#include <memory>
#include <sstream>
#include <deque>
#include <mutex>
#include <set>

#include "json.hpp"
using namespace nlohmann;

#include "utils.h"
//#include "TbwSettings.h"
#include "IDeviceManager.h"
#include "ScrollDirection.h"

#define DEVICEAPPS

struct ActionBase;
class DeviceConfig;
class ApplicationConfig;
class Settings;

// prototype of ActionBase child class factory function
typedef ActionBase* (*PFN_CREATE_ACTION_OBJECT)();

struct ActionBase {
	DeviceConfig* _deviceCfg;
    std::string _commandName;

    ActionBase() = delete;
    ActionBase(const ActionBase&) = delete;

    ActionBase(const char* commandName)
        : _deviceCfg(nullptr)
		, _commandName(commandName)
    {}
	virtual ~ActionBase()
    {}

	DeviceConfig* deviceConfig()
	{
		return _deviceCfg;
	}

    const std::string& name()
    {
        return _commandName;
    }
    static void registerChild(const char* name, PFN_CREATE_ACTION_OBJECT pfnFactory);

    // serialize from/to json
    virtual void load(json const&);
    virtual void save(json&);
    virtual void take(ITbwDevice*, bool fDown) = 0;
};
typedef std::shared_ptr<ActionBase> ACTIONPTR;
typedef std::map<uint32_t, ACTIONPTR> BUTTONACTIONTABLE;

#define DECLARE_ACTION_CLASS(className, actionName) struct meta##className {\
                                                        meta##className() \
                                                        {\
                                                            ActionBase::registerChild(actionName, &meta##className::createInstance);\
                                                        }\
                                                        static ActionBase* createInstance() {\
                                                            return new className(); \
                                                        }\
                                                    };

#define REGISTER_ACTION_CLASS(className)    className::meta##className tag##className;

struct ActionNone : public ActionBase {

    DECLARE_ACTION_CLASS(ActionNone, "none")

    ActionNone()
        : ActionBase("none")
    {}
    virtual void take(ITbwDevice* pDevice, bool fDown)
    {}
};

// sequence consists of an array of keyCodes, to be poked in sequence
struct ActionSequence : public ActionBase {

    // one step of an ActionSequence command
    struct Step {
        int _modifiers;
        int _keyCode;
        int _repeat;
        int _delay;
        int _state;

        Step()
        {}
    };

    std::vector<Step> _steps;

    DECLARE_ACTION_CLASS(ActionSequence, "sequence")

    ActionSequence()
        : ActionBase("sequence")
        , _steps()
    {}

    virtual void load(json const&);
    virtual void save(json&);
    virtual void take(ITbwDevice* pDevice, bool fDown);
};

struct ActionOpenItem : public ActionBase {
    std::string _name;
    std::string _path;
    std::vector<std::string> _args;
    int _type;  // 0-Unknown, 1-fileOrFolder, 2-Application, 3-URL

    DECLARE_ACTION_CLASS(ActionOpenItem, "open")

    ActionOpenItem()
        : ActionBase("open")
        , _name()
        , _path()
        , _args()
        , _type(0)
    {}

    ActionOpenItem(std::string const& path)
        : ActionBase("open")
        , _name()
        , _path(path)
        , _args()
        , _type(0)
    {}

    virtual void load(json const&);
    virtual void save(json&);
    virtual void take(ITbwDevice* pDevice, bool fDown);
};

struct ActionSnippet : public ActionBase {

    std::string _title;
    std::string _content;
	int _index;

    DECLARE_ACTION_CLASS(ActionSnippet, "snippet")

    ActionSnippet()
        : ActionBase("snippet")
        , _title()
        , _content()
		, _index(-1)
    {}
    ActionSnippet(std::string const& content)
        : ActionBase("snippet")
        , _content(content)
		, _index(-1)
	{}
 
    virtual void load(json const& j);
    virtual void save(json&);
    virtual void take(ITbwDevice* pDevice, bool fDown);
};

struct ActionSnippetMenu : public ActionBase {

	DECLARE_ACTION_CLASS(ActionSnippetMenu, "snippetMenu")

	ActionSnippetMenu()
		: ActionBase("snippetMenu")
	{}

	virtual void load(json const& j);
	virtual void save(json&);
	virtual void take(ITbwDevice* pDevice, bool fDown);
};

struct ActionKeystroke : public ActionBase {
    unsigned _modifiers;
    unsigned _keyCode;
    unsigned _repeat;
    unsigned _delay;
	bool _syncUp;

    DECLARE_ACTION_CLASS(ActionKeystroke, "keystroke")

    ActionKeystroke()
        : ActionBase("keystroke")
        , _modifiers(0)
        , _keyCode(0)
        , _repeat(0)
        , _delay(0)
		, _syncUp(false)
    {}

    ActionKeystroke(unsigned modifiers, unsigned keyCode, bool syncUp)
        : ActionBase("keystroke")
        , _modifiers(modifiers)
        , _keyCode(keyCode)
        , _repeat(0)
        , _delay(0)
		, _syncUp(syncUp)
	{}

    virtual void load(json const&);
    virtual void save(json&);
    virtual void take(ITbwDevice* pDevice, bool fDown);
};

struct ActionSingleClick : public ActionBase {
    int _button;
    uint32_t _modifiers;

    DECLARE_ACTION_CLASS(ActionSingleClick, "singleClick")

    ActionSingleClick()
        : ActionBase("singleClick")
        , _button(0)
        , _modifiers(0)
    {}
    ActionSingleClick(int button, uint32_t modifiers)
        : ActionBase("singleClick")
        , _button(button)
        , _modifiers(modifiers)
    {}

    virtual void load(json const&);
    virtual void save(json&);
    virtual void take(ITbwDevice* pDevice, bool fDown);
};

struct ActionDrag : public ActionBase {
    int _button;
    uint32_t _modifiers;

    DECLARE_ACTION_CLASS(ActionDrag, "drag")

    ActionDrag()
        : ActionBase("drag")
        , _button(0)
        , _modifiers(0)
    {}

    ActionDrag(int button, uint32_t modifiers)
        : ActionBase("drag")
        , _button(button)
        , _modifiers(modifiers)
    {}

    virtual void load(json const&);
    virtual void save(json&);
    virtual void take(ITbwDevice* pDevice, bool fDown);
};

struct ActionMultiClick : public ActionBase {
    int _button;    // mouse button number 1 to 3
                    //  1 - left button
                    //  2 - right button
                    //  3 - middle button
    int _nClicks;   // # of times to click for this action x 2
    int _clickCounter;
    unsigned _timerId;
    uint32_t _modifiers;

    DECLARE_ACTION_CLASS(ActionMultiClick, "multiClick")

    ActionMultiClick()
        : ActionBase("multiClick")
        , _button(0)
        , _nClicks(0)
        , _clickCounter(0)
        , _timerId(0)
        , _modifiers(0)
    {}

    ActionMultiClick(int button, int clicks, uint32_t modifiers)
        : ActionBase("multiClick")
        , _button(button >= 1 && button <= 3 ? button : 1)
        , _nClicks(clicks*2)
        , _clickCounter(0)
        , _timerId(0)
        , _modifiers(modifiers)
    {}

    virtual void load(json const&);
    virtual void save(json&);
    virtual void take(ITbwDevice* pDevice, bool fDown);
    void doClick();
};

struct ActionShowDesktop : public ActionBase {

    DECLARE_ACTION_CLASS(ActionShowDesktop, "showDesktop")

    ActionShowDesktop()
        : ActionBase("showDesktop")
    {}
    virtual void take(ITbwDevice* pDevice, bool fDown);
};

struct ActionMinimizeWindow : public ActionBase {

    DECLARE_ACTION_CLASS(ActionMinimizeWindow, "minimizeWindow")

    ActionMinimizeWindow()
        : ActionBase("minimizeWindow")
    {}
    virtual void take(ITbwDevice* pDevice, bool fDown);
};

struct ActionMaximizeWindow : public ActionBase {

    DECLARE_ACTION_CLASS(ActionMaximizeWindow, "maximizeWindow")

    ActionMaximizeWindow()
        : ActionBase("maximizeWindow")
    {}
    virtual void take(ITbwDevice* pDevice, bool fDown);
};

struct ActionCloseWindow : public ActionBase {

    DECLARE_ACTION_CLASS(ActionCloseWindow, "closeWindow")

    ActionCloseWindow()
        : ActionBase("closeWindow")
    {}
    virtual void take(ITbwDevice* pDevice, bool fDown);
};

struct ActionMinimizeAll : public ActionBase {

    DECLARE_ACTION_CLASS(ActionMinimizeAll, "minimizeAll")

    ActionMinimizeAll()
        : ActionBase("minimizeAll")
    {}
    virtual void take(ITbwDevice* pDevice, bool fDown);
};

struct ActionLockWorkstation : public ActionBase {

    DECLARE_ACTION_CLASS(ActionLockWorkstation, "lockWorkstation")

    ActionLockWorkstation()
        : ActionBase("lockWorkstation")
    {}
    virtual void take(ITbwDevice* pDevice, bool fDown);
};

struct ActionUseGlobal : public ActionBase {
    DECLARE_ACTION_CLASS(ActionUseGlobal, "useGlobal")

    ActionUseGlobal()
        : ActionBase("useGlobal")
    {}
    virtual void take(ITbwDevice* pDevice, bool fDown);
};

struct ActionSystem : public ActionBase {
    DECLARE_ACTION_CLASS(ActionSystem, "system")
    
    /**
     * The specific system action to take.
     *
     * Supported actions are:
     *
     *  shutdown
     *  restart
     *  logout
     *  shutdownImmediately
     *  restartImmediately
     *  logoutImmediately
     *  sleep
     */
    std::string _action;
    nlohmann::json _args; // additional arguments
    
    ActionSystem()
        : ActionBase("system")
        , _action()
        , _args()
    {}

    virtual void load(json const&);
    virtual void save(json&);
    virtual void take(ITbwDevice* pDevice, bool fDown);
};

struct ActionScroll : public ActionBase {
    DECLARE_ACTION_CLASS(ActionScroll, "scroll")

    ScrollDirection _direction;
    int _lines;

    ActionScroll()
        : ActionBase("scroll")
        , _direction(SD_UNKNOWN)
        , _lines(1)
    {}

    virtual void load(json const&);
    virtual void save(json&);
    virtual void take(ITbwDevice* pDevice, bool fDown);
};

/*
 * Class to instantiate ActionBase specializations based on supplied JSON.
 */
class ActionFactory {

    typedef std::unordered_map<std::string, json> ACTIONMACROTABLE;
    ACTIONMACROTABLE _macros;

public:
    ActionFactory();
    ~ActionFactory();

    ACTIONPTR createActionHandler(DeviceConfig*, json const& j);
    static ACTIONPTR createActionHandlerFromRaw(DeviceConfig*, json const& j);

private:
    bool loadActions(std::string const& path);
};

class DeviceConfig {

    friend void to_json(nlohmann::json& j, DeviceConfig const& settings);
    friend void from_json(nlohmann::json const& j, DeviceConfig& settings);
    friend class ApplicationConfig;
    friend class Settings;

	//static unsigned s_count;

//public:
    ApplicationConfig* _ac;
    int _id;
    int _pointerSpeed;          // mouse speed
    bool _pointerAcceleration;
    int _pointerAccelerationRate;   // mouse acceleration
    uint32_t _snapModifiers;    // modifiers to trigger locked axis movement
    uint32_t _slowModifiers;    // modifiers to trigger slow pointer
    int _slowDivider;
    bool _inertialScroll;       // inertial scroll enabled flag
    bool _scrollInvert;
    int _scrollLines;
    int _inertialScrollTimer;
    int _inertialScrollFactor;
    int _inertialScrollNullSteps;
    BUTTONACTIONTABLE _actions;

    // bitmask consisting of all defined chords
    // So if two chords are defined, 0x03(button1 + button2) & 0xC 
    // (button3 + button4), this would be set to 0xF.
    uint32_t _chordsCombinedMask;
	//&*&*&*G1_ADD_20210413
	uint32_t _chordMasks[16];
	//&*&*&*G2_ADD_20210413

    bool _active; // .true. indicates this is the active configuration

    // Button mask and their command macros, stored as stringified json.
    typedef std::map<uint32_t, std::string> BUTTONCOMMANDTABLE;
    BUTTONCOMMANDTABLE _commands;
    
public:
	struct SnippetString {
		std::string content;
		std::string label;
		uint64_t savedAt;

		SnippetString()
			: content(), label()
		{}
		SnippetString(std::string const& c, std::string const& t)
			: content(c), label(t), savedAt(::time(nullptr))
		{}
		bool operator<(const SnippetString& rhs) const
		{
			return rhs.savedAt < savedAt; // keeps it reverse chrono order
		}
		friend void to_json(nlohmann::json& j, SnippetString const& ss);
		friend void from_json(nlohmann::json const& j, SnippetString& ss);
	};

private:
	std::vector<SnippetString> _snippets;

private:
    void loadDefaultActions();

    void buildActionHandlersFromCommands();

    // Timer routine that tests if the currently accumulated button depress
    // mask matches any defined chord masks. It there is no matching chord
    // actions, routine emulates the corresponding button clicks.
    void chordDetecterTimer();

    void computeChordsCombinedMask();

public:
	DeviceConfig(); // json serializer uses this
	DeviceConfig(ApplicationConfig* ac);
	DeviceConfig(const DeviceConfig& rhs);
	virtual ~DeviceConfig();

    void init(ApplicationConfig* ac);

	ApplicationConfig* applicationConfig()
	{
		return _ac;
	}

    std::string appName() const;
    
    Settings* settings() const;
    /*
    ACTIONPTR getButtonAction(unsigned long buttonMask);
    void setButtonAction(unsigned long buttonMask, ACTIONPTR const& action);
    */
    json getButtonAction2(uint32_t mask);
    void setButtonAction2(uint32_t mask, json const&);

    /*
    void activate();
    void deactivate();
    bool isActive();
    */

    // sends button pressed/released bitmask
    void handleButtonActivity(ITbwDevice*, uint32_t ulDownButtons, uint32_t ulUpButtons);

    // pointer features
    uint32_t pointerSpeed();
    void pointerSpeed(uint32_t);

    bool pointerAcceleration();
    void pointerAcceleration(bool enable);

    uint32_t pointerAccelerationRate();
    void pointerAccelerationRate(uint32_t rate);

    bool slowPointer();
    std::string slowPointerModifiers();
    void slowPointerModifiers(const char*);

    bool lockedAxis();
    std::string lockedAxisModifiers();      // GETTER
    void lockedAxisModifiers(const char*);  // SETTER

    // scroll features
    bool inertialScroll();
    void inertialScroll(bool fEnable);

    uint32_t scrollSpeed();
    void scrollSpeed(uint32_t);

    bool invertScroll();
    void invertScroll(bool fEnable);

    //
    uint32_t getSlowModifiersMask()
    {
        return _slowModifiers;
    }

    uint32_t getLockPointerAxisModifiersMask()
    {
        return _snapModifiers;
    }

    BUTTONACTIONTABLE& getActions()
    {
        return _actions;
    }

    uint32_t getChordsMask()
    {
        return _chordsCombinedMask;
    }

	//&*&*&*G1_ADD
	void getChordMasks(uint32_t chordMasks[])
	{
		memcpy(chordMasks, _chordMasks, 16 * sizeof(uint32_t));
	}
	//&*&*&*G2_ADD

    bool operator==(const DeviceConfig& rhs) const;

    void addCommand(uint32_t buttonMask, json const& json);

	// a bit of kludgy method to accumulate snippet strings
	void addSnippetString(std::string const& content, std::string const& title, int index);
	std::vector<SnippetString> const& getSnippetStrings() const;

	static DeviceConfig createCopyingCoreButtonClicks(const DeviceConfig& dc);
};

// Table of devices indexed by their USB product id
typedef std::map<unsigned, DeviceConfig> DEVICETABLE;

class ApplicationConfig {

    friend void to_json(nlohmann::json & j, const ApplicationConfig & ac);
    friend void from_json(const nlohmann::json & j, ApplicationConfig & ac);

    // declare as frient so that this function can call ApplicationConfig::init()
    friend void from_json(nlohmann::json const & j, Settings & settings);

    friend class Settings;

    Settings* _settings;
    std::string _appFileName;   // executable file name
    std::string _appName;	    // settings name
	DeviceConfig _config;

	//static unsigned s_count;

public:
	ApplicationConfig(); // json serializer requires this
	ApplicationConfig(Settings* s, const char* name); // we use this internally
	ApplicationConfig(const ApplicationConfig& rhs); // so that _config._ac can be properly inited

	virtual ~ApplicationConfig();

    void init(Settings* s);

    // Returns a boolean indicating if config for the given device id exists
    bool deviceConfigExists(unsigned deviceId);

    // Creates the default DeviceConfig for deviceId, if one does not exist.
    void createDefaultDeviceConfigIfDoesntExist(unsigned deviceId);

    void addDeviceConfig(DeviceConfig&& dc);

public:
    Settings* settings() const
    {
        return _settings;
    }
    const std::string& Filename() const
    {
        return _appFileName;
    }
    std::string name() const
    {
        return _appName;
    }
};

// Application information indexed by their executable name
typedef std::map<std::string, ApplicationConfig> APPLICATIONTABLE;

typedef std::map<unsigned, APPLICATIONTABLE> DEVICEAPPTABLE;

struct BadSettingsFile : public std::exception {
};

class Settings {

    friend void to_json(nlohmann::json& j, Settings const& settings);
    friend void from_json(nlohmann::json const& j, Settings& settings);

    friend class DeviceConfig;

#ifdef DEVICEAPPS
	DEVICEAPPTABLE _devices;
#else
    APPLICATIONTABLE _apps;
#endif
    std::string _platform;
    time_t _timestamp;
    unsigned _versionMajor;
    unsigned _versionMinor;
    bool _trayIcon;


    // Creates an empty TbwSettings.json with empty global app ('*') settings
    void createDefault();

    // load/save implementation that does not acquire the mutex
    void loadImpl(const char* filePath);
#ifdef _WINDOWS
    void loadImplw(const wchar_t* filePath);
#endif    
    void saveImpl();
#ifdef _WINDOWS
    void saveImplw();
#endif

public:
    Settings(bool fLoadFromFile=false);
    ~Settings();

    // Returns the full path of the folder where the settings file is stored
    static std::string getFileFolder();

    // Returns the full path of the legacy settings file - TbwSettings.xml.
    static std::string getLegacyFileFullPath();

    // Returns the settings filename - TbwSettings.xml
    static std::string getLegacyFilename();

    // Returns the full path of the settings file (TbwSettings.json)
    static std::string getFileFullPath();
#ifdef _WINDOWS    
    static std::wstring getFileFullPathW();
#endif
    // Returns the settings filename - TbwSettings.json
    static std::string getFilename();
#ifdef _WINDOWS
    static std::wstring getFilenameW();
#endif
    // Creates the platform specific preferences folder where TbwSettings.json
    // will be stored.
    static void createFolderIfDoesNotExist();

    // load(reload) settings from TbwSettings.json
    void load(const char* filePath);
#ifdef _WINDOWS    
    void loadW(const wchar_t* filePath);
#endif
    // Save settings to TbwSettings.json, stored in default folder,
    // which is App preferences folder within the user's home folder.
    void save();

    // Creates a default device config for appname '*', if one doesn't exist.
    // If a device config exists, does nothing.
    void createDefaultDeviceConfigIfDoesntExist(unsigned id);

    // Returns all the app identifiers for the given device Id 
	// for which configuration exists.
    //
    // Throws:
    //  None
    std::vector<std::string> getApps(unsigned deviceId);

    // Returns all the device identifiers for which configuration exists
    //
    // Parameters:
    //  const char* app - app profile to query.
    //
    // Throws:
    //  None
    std::vector<unsigned> getDevices(const char* app);

    // Returns the friendly name of an app identifier
    //
    // Params:
    //  identifier - a string identifier that is used to uniquely identify
    //               the app.
    //
    // Throws:
    //  std::exception if app with identifier is not found!
    std::string getAppName(unsigned deviceId, const char* identifier);
    
    // Creates application configuration entry for the given app name
    //
    // Params:
    //  name - a friendly anme
    //  identifier - a string identifier that is used to uniquely identify
    //               this program.
    //
    // Throws:
    //  std::exception if an app with identifier already exists
    void createApp(unsigned deviceId, const char* name, const char* identifier);
    
    // Delete an existing app profile uniquely identied by 'identifier'.
    //
    // Params:
    //  identifier - a string identifier that is used to uniquely identify
    //               the app.
    // Throws:
    //  std::exception if an app with 'identifier' does not exist
    void deleteApp(unsigned deviceId, const char* identifier);
    
    // Given a process name and device id, returns the matching DeviceConfig
    // If definitions don't exist, returns the default DeviceConfig.
    DeviceConfig& getDeviceConfig(const char* processFilename, unsigned deviceId, bool fUseGlobal);
    // wchar_t version of the same function
    DeviceConfig& getDeviceConfig(const wchar_t* processFilename, unsigned deviceId, bool fUseGlobal);

    // Given a process name, returns a matching Application profile name
    // If a dedicated device setting does not exist for the given process,
    // returns the global profile name ('*') as the matching profile name.
    std::string getMatchingProfileName(unsigned deviceId, char const* processFilename);
    // wchar_t version of the same function
    std::wstring getMatchingProfileName(unsigned deviceId, wchar_t const* processFilename);

    // A factory function that given an action code and its arguments, returns 
    // a corresponding ActionBase specialized class for the action.
    //
    // Parameters:
    //  args: JSON string containing the command 
    //
    // Returns:
    //  ACTIONPTR - Ref counted ActionBase smart pointer
    //
    // Throws:
    //  std::exception if the json data passed is not valid for the given
    //  command.
    //static ACTIONPTR createActionHandler(json const& j); // throw(std::exception)
    static json getActionHandler(const ACTIONPTR& action);
    
    unsigned versionMajor() { return _versionMajor; }
    unsigned versionMinor() { return _versionMinor; }
    std::string platform()  { return _platform; }
    time_t timeStamp()      { return _timestamp; }
    std::string timeStampAsUTCStr();

    bool trayIcon();
    void trayIcon(bool enable);

	static Settings* createLoadingFile(const char* filePath);
};
