/*
    HTTP Server that accepts test requests.
*/
#include "stdafx.h"
#include "CommandServer.h"
#include "json.hpp"
using namespace nlohmann;

#include "IDeviceManager.h"
#include "Settings.h"
#include "utils.h"
#include "PlatformServices.h"
#include "DeviceIdRemap.h"

#undef min
#undef max
#include <algorithm>
#include <chrono>
#include <set>

#include <boost/log/trivial.hpp>

#ifdef __APPLE__
#include "../osx/KensingtonWorksAgent/version.h"
#else
#include "./version.h"
#endif

// just a running number
#define API_VERSION             1

using namespace std;

template<typename T>
T getFromJson(const json & j, const char* key, T def)
{
    T value = def;
    if (j.find(key) != j.end()) {
        auto json_value = j.at(key);
        if (json_value.is_string())
            value = to<T>(json_value.get<std::string>());
        else
            value = json_value.get<T>();
    }
    return value;
}

void CommandServer::reportSuccess(shared_ptr<HttpServer::Response> response, nlohmann::json const & jresp)
{
    string jstr = jresp.dump();
    jstr.append("\n");
    
#ifdef _DEBUG
    BOOST_LOG_TRIVIAL(debug) << "\tresponse(success): " << jresp;
#endif

    *response
        << "HTTP/1.1 200 OK"
        << "\nContent-Length: " << jstr.length()
        << "\nContent-Type: application/json; charset=utf-8"
        << "\n\n"
        << jstr;
}

void CommandServer::reportException(shared_ptr<HttpServer::Response> response, exception const & e)
{
    json jresp;
    jresp["success"] = false;
    jresp["reason"] = e.what();

    BOOST_LOG_TRIVIAL(warning) << "\tresponse(failure): " << jresp;
    string jstr = jresp.dump();
    jstr.append("\n");
    *response << "HTTP/1.1 400 Bad Request"
        << "\nContent-Length: " << jstr.length()
        << "\nContent-Type: application/json; charset=utf-8"
        << "\n\n"
        << jstr;
}

void CommandServer::getCommonConfigQueryFields(std::shared_ptr<HttpServer::Request> request, uint16_t & device, std::string & app)
{
    auto query_fields = request->parse_query_string();
    for (auto &field : query_fields) {
		if (field.first == "device")
		{
			device = to<uint16_t>(field.second);
			//device = devidremap::getRemappedDeviceId(device); // allows test programs to refer to the device using the real device id
		}
		else if (field.first == "app")
		{
#ifdef _WINDOWS
			// Windows file names are case insensitive. So treat all filnames
			// to lowercase so that while we search for a matching process,
			// it'll match (that algorithm also converts process image filename 
			// to lowercase in windows)
			app = tolower(field.second);
#else
			app = field.second;
#endif

		}
    }

}

uint32_t CommandServer::getButtonQueryField(std::shared_ptr<HttpServer::Request> request)
{
    auto query_fields = request->parse_query_string();
    uint32_t button = 0;
    for (auto &field : query_fields) {
        if (field.first == "button")
            button = to<uint32_t>(field.second);
    }
    return button;
}

CommandServer::CommandServer(ITbwDeviceManager* pMgr, bool fTestMode/* = false*/)
    : _pDeviceMgr(pMgr)
    , _server()
    , _testMode(fTestMode)
{
#ifndef _DEBUG
    // address hardcoded to localhost on release builds
    _server.config.address = "127.0.0.1";
#endif
    _server.config.port = 9090;

    // Add resources using path-regex and method-string, and an anonymous function
    // POST-example for the path /string, responds the posted string
    _server.default_resource["GET"] = [this](
        shared_ptr<HttpServer::Response> response,
        shared_ptr<HttpServer::Request> request)
    {
        try
        {
            reportSuccess(response, json{ 
                { "apiVersion", API_VERSION },
                { "fileVersion", {
                    { "major", VERSION_MAJOR },
                    { "minor", VERSION_MINOR },
                    { "patch", VERSION_PATCH }
                    }
                    }
                }
            );
        }
        catch (const exception& e)
        {
            //BOOST_LOG_TRIVIAL(error) << "Error while executing requested test: " << e.what();
            reportException(response, e);
        }
    };

	/*
		Returns an array of frequently used apps. For each app the following information is
		returned:
			{
				'binary': <path to the executable/app>
				'icon': <base64 encoded 32x32 program image in PNG>
			}

		Request accepts two parameters:
			count := number, number of favorite apps to return. Max 20
	*/
	_server.resource["^/favapps$"]["GET"] = [this](
		shared_ptr<HttpServer::Response> response,
		shared_ptr<HttpServer::Request> request)
	{
		try
		{
			BOOST_LOG_TRIVIAL(debug) << "[HTTP] - get favorite apps";

			int count = 10;
			std::string exclude;
			uint16_t deviceId = 0;
			auto query_fields = request->parse_query_string();
			for (auto &field : query_fields) {
				if (field.first == "count")
				{
					count = to<uint16_t>(field.second);
					count = std::min(std::max(count, 1), 20);
				}
				else if (field.first == "device")
				{
					deviceId = to<uint16_t>(field.second);
				}
			}

			// add existing apps to exclusion list
			std::set<std::string> exclusionList;
			try
			{
				auto existingApps = _pDeviceMgr->getApps(deviceId)["apps"];
				for (auto& app : existingApps.items())
				{
					nlohmann::json& jApp = app.value();
					for (auto& el : jApp.items()) {
                        #ifdef _WINDOWS
                        std::string key = tolower(el.key());
                        #else
						std::string key = el.key();
                        #endif
						if (key != "*") {
							exclusionList.insert(key);
						}
					}
				}
			}
			catch (std::exception& e)
			{
			}
			reportSuccess(
				response,
				PlatformServices::getInstance()->getFavoriteApps(count, exclusionList)
			);
		}
		catch (const exception& e)
		{
			reportException(response, e);
		}
	};
	
	/*
        Returns an array of attached devices. Returned JSON has the following
        format:

        {
            "devices": [
                    {
                        "id": <product Id> [can be psuedo device id]
						"realId": <real product id> [USB/BLE SKU]
						"interface": "[{bluetooth|usb}[[,]{bluetooth|usb}]]"
                        "name": "<product Name>",
                        "version": <firmware version>,
                        "present": <boolean>
                    }
                ]
        }
    */
    _server.resource["^/devices$"]["GET"] = [this](
        shared_ptr<HttpServer::Response> response,
        shared_ptr<HttpServer::Request> request)
    {
        try
        {
            //BOOST_LOG_TRIVIAL(debug) << "[HTTP] - get connected devices";
            reportSuccess(response, _pDeviceMgr->getDevices());
        }
        catch (const exception& e)
        {
            reportException(response, e);
        }
    };

    _server.resource["^/config/apps$"]["GET"] = [this](
        shared_ptr<HttpServer::Response> response,
        shared_ptr<HttpServer::Request> request)
    {
        try
        {
            BOOST_LOG_TRIVIAL(debug) << "[HTTP] - get app configurations";

			uint16_t deviceId = 0; std::string app;
			getCommonConfigQueryFields(request, deviceId, app);

			reportSuccess(response, _pDeviceMgr->getApps(deviceId));
        }
        catch (const exception& e)
        {
            reportException(response, e);
        }
    };

    _server.resource["^/config/apps$"]["POST"] = [this](
        shared_ptr<HttpServer::Response> response,
        shared_ptr<HttpServer::Request> request)
    {
        try
        {
			uint16_t deviceId = 0; std::string app;
			getCommonConfigQueryFields(request, deviceId, app);
			stringstream ss(request->content.string());
            auto jreq = json::parse(ss);
            BOOST_LOG_TRIVIAL(debug) << "[HTTP] - create new app configuration"
                << ", json: " << jreq;
            _pDeviceMgr->createApp(deviceId, jreq);
            reportSuccess(response, _pDeviceMgr->getApps(deviceId));
        }
        catch (const exception& e)
        {
            reportException(response, e);
        }
    };

    _server.resource["^/config/apps$"]["DELETE"] = [this](
        shared_ptr<HttpServer::Response> response,
        shared_ptr<HttpServer::Request> request)
    {
        try
        {
			uint16_t deviceId = 0; std::string app;
			getCommonConfigQueryFields(request, deviceId, app);
			stringstream ss(request->content.string());
            auto jreq = json::parse(ss);
            BOOST_LOG_TRIVIAL(debug) << "[HTTP] - delete app configuration"
                << ", json: " << jreq;
            _pDeviceMgr->deleteApp(deviceId, jreq);
            reportSuccess(response, _pDeviceMgr->getApps(deviceId));
        }
        catch (const exception& e)
        {
            reportException(response, e);
        }
    };

#ifdef TAU

    // Emulates button press down and release on a connected device.
    // HTTP URL: /devices/<deviceid>/emulatebuttonclick/<button_mask>
    //            down and button up.
    _server.resource["^/devices/([0-9]+)/emulatebuttonclick/([0-9]+)$"]["POST"] = [this](
        shared_ptr<HttpServer::Response> response,
        shared_ptr<HttpServer::Request> request)
    {
        try
        {
            auto deviceId = to<uint32_t>(request->path_match[1].str());
            auto buttonMask = to<uint32_t>(request->path_match[2].str());

            BOOST_LOG_TRIVIAL(debug)
                << "[HTTP] Emulate button click - device: " << deviceId
                << ", buttonMask: " << std::hex << buttonMask;
            
            _pDeviceMgr->emulateButtonClick(deviceId, buttonMask);

            reportSuccess(response, json{ {"success", true} });
        }
        catch (const exception& e)
        {
            BOOST_LOG_TRIVIAL(error) << "Error while executing emulate-button-click: " << e.what();
            reportException(response, e);
        }
    };
    static const char* CONFIG_BASE = "^/config";
#endif // TAU

#if 0
	static const char* APP_NAME_PATTERN = "([a-zA-Z0-9%_\\-\\s\\.,\\*]+)";
    static const char* DEVICE_ID_PATTERN = "([0-9]+)";

    // configure button actions
    // /config/<app>/<device>/buttons/<button mask>
    std::string configButtonsPath = std::string(CONFIG_BASE)
        + "/" + APP_NAME_PATTERN
        + "/" + DEVICE_ID_PATTERN
        + "/" + "buttons/([0-9]+)"  // path_match[3]
        + "$";
    _server.resource[configButtonsPath]["GET"] = [this](
        shared_ptr<HttpServer::Response> response,
        shared_ptr<HttpServer::Request> request)
    {
        try
        {
            BOOST_LOG_TRIVIAL(debug)
                << "[HTTP] get button config - "
                << "app: " << request->path_match[1].str()
                << ", deviceId: " << to<uint16_t>(request->path_match[2].str())
                << ", buttonMask: " << to<uint32_t>(request->path_match[3].str());
            
            reportSuccess(
                response,
                _pDeviceMgr->getButtonAction(
                    request->path_match[1].str().c_str(),
                    to<uint16_t>(request->path_match[2].str()),
                    to<uint32_t>(request->path_match[3].str())
                    )
                );
        }
        catch (std::exception& e)
        {
            reportException(response, e);
        }
    };

    _server.resource[configButtonsPath]["POST"] = [this](
        shared_ptr<HttpServer::Response> response,
        shared_ptr<HttpServer::Request> request)
    {
        try
        {
            stringstream ss(request->content.string());
            BOOST_LOG_TRIVIAL(debug)
                << "[HTTP] set button config - "
                << "app: " << request->path_match[1].str()
                << ", deviceId: " << to<uint16_t>(request->path_match[2].str())
                << ", buttonMask: " << to<uint32_t>(request->path_match[3].str())
                << ", action: " << ss.str();
            
            _pDeviceMgr->setButtonAction(
                request->path_match[1].str().c_str(),
                to<uint16_t>(request->path_match[2].str()),
                to<uint32_t>(request->path_match[3].str()),
                json::parse(ss)
                );
            reportSuccess(response, json{ {"success", true} });
        }
        catch (std::exception& e)
        {
            reportException(response, e);
        }
    };

    // Configure pointer.
    // HTTP URL: /config/<app>/<device>/pointer
    // REST API arguments:
    //      accelerationRate=<numeric>, usually ranges between 1..100
    //      speed=<numeric>, usually ranges between 1..100
    //      slowModifiers=<numeric>, bitmask for modifier key combination to 
    //          activate slow pointer.
    //      lockedAxisModifiers=<numeric>, bitmask for modifier key 
    //          combination to activate locked axis movement.
    std::string configPointerPath = std::string(CONFIG_BASE)
        + "/" + APP_NAME_PATTERN
        + "/" + DEVICE_ID_PATTERN
        + "/" + "pointer"
        + "$";
    _server.resource[configPointerPath]["GET"] = [this](
        shared_ptr<HttpServer::Response> response,
        shared_ptr<HttpServer::Request> request)
    {
        try
        {
            BOOST_LOG_TRIVIAL(debug)
                << "[HTTP] get pointer config - "
                << "app: " << request->path_match[1].str()
                << ", deviceId: " << to<uint16_t>(request->path_match[2].str());

            reportSuccess(
                response,
                _pDeviceMgr->getPointerConfig(
                    request->path_match[1].str().c_str(),
                    to<uint16_t>(request->path_match[2].str()))
                );
        }
        catch (std::exception& e)
        {
            reportException(response, e);
        }
    };

    _server.resource[configPointerPath]["POST"] = [this](
        shared_ptr<HttpServer::Response> response,
        shared_ptr<HttpServer::Request> request)
    {
        try
        {
            stringstream ss(request->content.string());
            BOOST_LOG_TRIVIAL(debug)
                << "[HTTP] set pointer config - "
                << "app: " << request->path_match[1].str()
                << ", deviceId: " << to<uint16_t>(request->path_match[2].str())
                << ", config: " << ss.str();

            _pDeviceMgr->setPointerConfig(
                request->path_match[1].str().c_str(),
                to<uint16_t>(request->path_match[2].str()),
                json::parse(ss)
                );
            reportSuccess(response, json{{"success", true}});
        }
        catch (std::exception& e)
        {
            reportException(response, e);
        }
    };

    // Configure scroll
    // HTTP URL: /config/<app>/<device>/scroll
    // REST API arguments:
    //      speed=numeric, usually ranges between 1..100
    //      invert=<bool>
    //      inertial=bool
    std::string configScrollPath = std::string(CONFIG_BASE)
        + "/" + APP_NAME_PATTERN
        + "/" + DEVICE_ID_PATTERN
        + "/" + "scroll"
        + "$";
    _server.resource[configScrollPath]["GET"] = [this](
        shared_ptr<HttpServer::Response> response,
        shared_ptr<HttpServer::Request> request)
    {
        try
        {
            BOOST_LOG_TRIVIAL(debug)
                << "[HTTP] get scroll config - "
                << "app: " << request->path_match[1].str()
                << ", deviceId: " << to<uint16_t>(request->path_match[2].str());

            reportSuccess(
                response,
                _pDeviceMgr->getScrollWheelConfig(
                    request->path_match[1].str().c_str(),
                    to<uint16_t>(request->path_match[2].str())
                    )
                );
        }
        catch (std::exception& e)
        {
            reportException(response, e);
        }
    };

    _server.resource[configScrollPath]["POST"] = [this](
        shared_ptr<HttpServer::Response> response,
        shared_ptr<HttpServer::Request> request)
    {
        try
        {
            stringstream ss(request->content.string());
            BOOST_LOG_TRIVIAL(debug)
                << "[HTTP] set scroll config - "
                << "app: " << request->path_match[1].str()
                << ", deviceId: " << to<uint16_t>(request->path_match[2].str())
                << ", config: " << ss.str();

                _pDeviceMgr->setScrollWheelConfig(
                    request->path_match[1].str().c_str(),
                    to<uint16_t>(request->path_match[2].str()),
                    json::parse(ss)
                    );
            reportSuccess(response, json{{"success", true}});
        }
        catch (std::exception& e)
        {
            reportException(response, e);
        }
    };
#endif
    // Get entire config
    _server.resource["^/config$"]["GET"] = [this](
        shared_ptr<HttpServer::Response> response,
        shared_ptr<HttpServer::Request> request)
    {
        try
        {
            BOOST_LOG_TRIVIAL(debug) << "[HTTP] get entire settings";

            reportSuccess(
                response,
                _pDeviceMgr->getSettings()
            );
        }
        catch (std::exception& e)
        {
            reportException(response, e);
        }
    };

    // Set entire config
    _server.resource["^/config$"]["POST"] = [this](
        shared_ptr<HttpServer::Response> response,
        shared_ptr<HttpServer::Request> request)
    {
        try
        {
            stringstream ss(request->content.string());
            BOOST_LOG_TRIVIAL(info) << "[HTTP] set entire settings";
			BOOST_LOG_TRIVIAL(info) << request->content.string();

            _pDeviceMgr->setSettings(json::parse(ss));

            reportSuccess(
                response,
                _pDeviceMgr->getSettings()
            );
        }
        catch (std::exception& e)
        {
            reportException(response, e);
        }
    };

    // Get general configuration, such as trayIcon, timeStamp, etc
    _server.resource["^/config/general$"]["GET"] = [this](
        shared_ptr<HttpServer::Response> response,
        shared_ptr<HttpServer::Request> request)
    {
        try
        {
            BOOST_LOG_TRIVIAL(debug) << "[HTTP] get general config";

            reportSuccess(
                response,
                _pDeviceMgr->getGeneralConfig()
            );
        }
        catch (std::exception& e)
        {
            reportException(response, e);
        }
    };

    // Set general configuration
    _server.resource["^/config/general$"]["POST"] = [this](
        shared_ptr<HttpServer::Response> response,
        shared_ptr<HttpServer::Request> request)
    {
        try
        {
            stringstream ss(request->content.string());
            BOOST_LOG_TRIVIAL(debug) << "[HTTP] set general config";

            _pDeviceMgr->setGeneralConfig(json::parse(ss));

            reportSuccess(
                response,
                _pDeviceMgr->getGeneralConfig()
            );
        }
        catch (std::exception& e)
        {
            reportException(response, e);
        }
    };

    //  New API where app & device id are passed as request arguments
    _server.resource["^/config/buttons$"]["GET"] = [this](
        shared_ptr<HttpServer::Response> response,
        shared_ptr<HttpServer::Request> request)
    {
        try
        {
            std::string app; uint16_t deviceId = 0;
            getCommonConfigQueryFields(request, deviceId, app);
            uint32_t button = getButtonQueryField(request);

            BOOST_LOG_TRIVIAL(info)
                << "[HTTP] get button config - "
                << "app: " << app
                << ", deviceId: " << deviceId
                << ", buttonMask: " << std::hex << button;

            reportSuccess(
                response,
                _pDeviceMgr->getButtonAction(
                    app.c_str(),
                    deviceId,
                    button)
            );
        }
        catch (std::exception& e)
        {
            reportException(response, e);
        }
    };

    //  New API where app & device id are passed as request arguments
    _server.resource["^/config/buttons$"]["POST"] = [this](
        shared_ptr<HttpServer::Response> response,
        shared_ptr<HttpServer::Request> request)
    {
        try
        {
            stringstream ss(request->content.string());
            std::string app; uint16_t deviceId = 0;
            getCommonConfigQueryFields(request, deviceId, app);
            uint32_t button = getButtonQueryField(request);
            //if (button == 0)
            //    throw std::runtime_error("Invalid button parameter");

			BOOST_LOG_TRIVIAL(info)
				<< "[HTTP] set button config - "
				<< "app: " << app
				<< ", deviceId: " << deviceId
				<< ", buttonMask: " << std::hex << button;
				//<< " ss " << json::parse(ss);
            
            _pDeviceMgr->setButtonAction(
                app.c_str(),
                deviceId,
                button,
                json::parse(ss)
            );
            reportSuccess(
                response,
                _pDeviceMgr->getButtonAction(
                    app.c_str(),
                    deviceId,
                    button)
            );
        }
        catch (std::exception& e)
        {
            reportException(response, e);
        }
    };

    // Pointer configuration
    _server.resource["^/config/pointer$"]["GET"] = [this](
        shared_ptr<HttpServer::Response> response,
        shared_ptr<HttpServer::Request> request)
    {
        try
        {
            std::string app; uint16_t deviceId = 0;
            getCommonConfigQueryFields(request, deviceId, app);

            BOOST_LOG_TRIVIAL(info)
                << "[HTTP] get pointer config - "
                << "app: " << app << app.length()
                << ", deviceId: " << deviceId;

            reportSuccess(
                response,
                _pDeviceMgr->getPointerConfig(
                    app.c_str(),
                    deviceId)
            );
        }
        catch (std::exception& e)
        {
            reportException(response, e);
        }
    };

    _server.resource["^/config/pointer$"]["POST"] = [this](
        shared_ptr<HttpServer::Response> response,
        shared_ptr<HttpServer::Request> request)
    {
        try
        {
            stringstream ss(request->content.string());
            std::string app; uint16_t deviceId = 0;
            getCommonConfigQueryFields(request, deviceId, app);

            BOOST_LOG_TRIVIAL(debug)
                << "[HTTP] set pointer config - "
                << "app: " << app
                << ", deviceId: " << deviceId;

            _pDeviceMgr->setPointerConfig(
                app.c_str(),
                deviceId,
                json::parse(ss)
            );

            reportSuccess(
                response,
                _pDeviceMgr->getPointerConfig(
                    app.c_str(),
                    deviceId)
            );
        }
        catch (std::exception& e)
        {
            reportException(response, e);
        }
    };

    // Scroll configuration
    _server.resource["^/config/scroll$"]["GET"] = [this](
        shared_ptr<HttpServer::Response> response,
        shared_ptr<HttpServer::Request> request)
    {
        try
        {
            std::string app; uint16_t deviceId = 0;
            getCommonConfigQueryFields(request, deviceId, app);

            BOOST_LOG_TRIVIAL(debug)
                << "[HTTP] get scroll config - "
                << "app: " << app
                << ", deviceId: " << deviceId;

            reportSuccess(
                response,
                _pDeviceMgr->getScrollWheelConfig(
                    app.c_str(),
                    deviceId
                )
            );
        }
        catch (std::exception& e)
        {
            reportException(response, e);
        }
    };

    _server.resource["^/config/scroll$"]["POST"] = [this](
        shared_ptr<HttpServer::Response> response,
        shared_ptr<HttpServer::Request> request)
    {
        try
        {
            stringstream ss(request->content.string());
            std::string app; uint16_t deviceId = 0;
            getCommonConfigQueryFields(request, deviceId, app);

            BOOST_LOG_TRIVIAL(debug)
                << "[HTTP] set scroll config - "
                << "app: " << app
                << ", deviceId: " << deviceId;

            _pDeviceMgr->setScrollWheelConfig(
                app.c_str(),
                deviceId,
                json::parse(ss)
            );

            reportSuccess(
                response,
                _pDeviceMgr->getScrollWheelConfig(
                    app.c_str(),
                    deviceId
                )
            );
        }
        catch (std::exception& e)
        {
            reportException(response, e);
        }
    };

	// Get snippets database
	_server.resource["^/config/snippets"]["GET"] = [this](
		shared_ptr<HttpServer::Response> response,
		shared_ptr<HttpServer::Request> request)
	{
		try
		{
			std::string app; uint16_t deviceId = 0;
			getCommonConfigQueryFields(request, deviceId, app);

			BOOST_LOG_TRIVIAL(debug)
				<< "[HTTP] get snippets - "
				<< "app: " << app
				<< ", deviceId: " << deviceId;

			reportSuccess(
				response,
				_pDeviceMgr->getSnippets(
					app.c_str(),
					deviceId
				)
			);
		}
		catch (std::exception& e)
		{
			reportException(response, e);
		}
	};


    if (_testMode)
    {
    }
}

CommandServer::~CommandServer()
{
    Stop();
}

void CommandServer::Start()
{
    _server_thread = thread([this] {
        BOOST_LOG_TRIVIAL(debug) << "starting command server..";
        this->server().start();
    });
}

void CommandServer::Stop()
{
    if (IsRunning())
    {
        _server.stop();
        _server_thread.join();
        BOOST_LOG_TRIVIAL(debug) << "command server stopped";
    }
}
