/*
 OS dependent services, which is a singleton that exposes platform dependent
 services in a uniform interface to platform independent classes in Settings.h.
*/
#pragma once

#include <string>
#include <vector>
#include <set>

#include "json.hpp"
using namespace nlohmann;

#include "utils.h"
#include "IDeviceManager.h"
#include "ScrollDirection.h"

// ABC for implementation class
struct PlatformServicesImpl {
    virtual ~PlatformServicesImpl()
    {}

    virtual void postKeystroke(uint16_t usModifiers, uint16_t keyCode, bool fDown, bool fModifiersOnly) = 0;
    virtual void postString(const std::string& string) = 0;
    virtual void postPointerClick(int buttonMask, bool fDown) = 0;
    /*
        Configure the pointer parameters:

        Parameters:
            speed - pointer speed, expressed as value in the range 0-100
            acceleration - acceleration factor, expressed as a value in the 
                           range 0-100
            scrollLines - number of absolute lines to scroll per unit of scroll
    */
    virtual void configurePointer(int speed, int acceleration, unsigned scrollLines) = 0;

    /*
        Open a resource
    */
    virtual void openResource(const std::string& resource, const std::vector<std::string>& args, int type) = 0;

    virtual std::string getPreferencesFolder() = 0;
    
    virtual std::string getLogFileFolder() = 0;
    
    virtual void system(std::string const& verb, nlohmann::json const& addlArgs) = 0;

	virtual nlohmann::json getFavoriteApps(int count, const std::set<std::string>& exclude) = 0;

	virtual std::string showPopupMenu(std::vector<std::string> const& items, std::vector<std::string> const& labels, int timeOut = -1) = 0;
 
    /**
     * Return value in the range 1-100
     */
    virtual uint32_t getCurPointerSpeed() = 0;

    /**
     * Return value in the range 0-100. 0 indicates acceleration disabled.
     */
    virtual uint32_t getCurPointerAccelerationRate() = 0;
    
    /**
     * Return value in the range 1-100
     */
    virtual uint32_t getCurScrollSpeed() = 0;
    
    /**
     * Return scroll invert flag.
     *
     * True indicates scroll direction is inverted. That is
     * opposite to natural direction as set by the OS.
     */
     virtual bool getScrollInvert() = 0;

     virtual void postScrollNavigation(ScrollDirection direction, int lines) = 0;
};

class PlatformServices {
    static PlatformServices* _instance;

    PlatformServicesImpl* _impl;

    // device states
    PlatformServices();
    ~PlatformServices();

public:

    static PlatformServices* getInstance();

    void destroy(); // destroy the singleton instance

    /*
        Post a key depress or release action

        Parameters:
            usModifiers - Modifier key bit mask
            keyCode - key code
            fDown - true for key depress, false for release action
    */
    void postKeystroke(uint16_t usModifiers, uint16_t keyCode, bool fDown, bool fModifiersOnly);

    /*
        Post the keystrokes for an entire string. Handles uppercase
        in the string and emulates Shift key depress and release
        accordingly.

        Parameters:
            string - the string to post to the input queue.
    */
    void postString(const std::string& string);

    /*
        Post a mouse pointer button click.

        Parameters:
            button - bit mask of the button to click
                       bit 0 - Left button
                       bit 1 - right button
                       bit 2 - middle button
					   bit 3 - button 4 // if platform supports it
					   bit 4 - button 5	// if platform supports it
                Do not pass combined bitmasks for pressing more than one button! 
			fDown - true for key depress, false for release action
    */
    void postPointerClick(int buttonMask, bool fDown);

    /*
        Mouse configuration -- speed, acceleration & scroll lines
     */
    void configurePointer(int speed, int acceleration, unsigned scrollLines);

    /*
        Open a resource, which could be a website (given by its URL or an executable)
    */
    void openResource(const std::string& resource, const std::vector<std::string>& args, int type);

    /*
        Returns the folder where TrackballWorks user preferences are stored.
    */
    std::string getPreferencesFolder();
    
    /*
     *  Returns the folder where log files are to be stored.
     */
    std::string getLogFileFolder();
    
    /**
     * System action.
     */
    void system(std::string const& verb, nlohmann::json const& addlArgs);

	/*
	 * Return a JSON object array containing the list of installed apps that
	   the user tends to use most frequently. Each object is of the form:

		{
			'program': <program path>
			'name': <friendly name>
			'icon': base64 PNG image of the program icon
		}
	*/
	nlohmann::json getFavoriteApps(int count, const std::set<std::string>& exclude);

	/*
	 * Display a popup menu at the current cursor position with the strings
	 * given in the vector.
	 *
	 * Optional timeout argument makes the popup menu to be automatically
	 * dismissed when the timeout (in miliiseconds) is reached. This defaults 
	 * -1, that is infinite, meaning the popup is never dismissed until user
	 * selects an item.
	 *
	 * Returns the selected menu item as the string index in the vector 
	 * argument. Or -1 if user dismissed the menu without selecting any item
	 * or specified timeout occured.
	 */
	std::string showPopupMenu(std::vector<std::string> const& items, std::vector<std::string> const& labels, int timeOut=-1);
 
    /**
     * Returns the current pointer speed as set in the OS.
     *
     * Value is retuned in 1-100 scale.
     */
    uint32_t getCurPointerSpeed();

    /**
     * Returns the current pointer acceleration rate as set in the OS.
     *
     * Value is retuned in 0-100 scale. 0 indicates acceleration is
     * diasbled.
     */
    uint32_t getCurPointerAccelerationRate();

    /**
     * Returns the current scroll speed as set in the OS.
     *
     * Value is returned in 1-100 scale.
     */
    uint32_t getCurScrollSpeed();

    /**
     * Return scroll invert flag.
     *
     * True indicates scroll direction is inverted. That is
     * opposite to natural direction as set by the OS.
     */
     bool getScrollInvert();

     /**
      * Post scroll navigation event
      *
      * Parameters:
      *     direction - navigation direction
      *         1 - UP
      *         2 - DOWN
      *         3 - LEFT
      *         4 - RIGHT
      *     lines - number of lines to scroll
      *         1 - n
      */
     void postScrollNavigation(ScrollDirection direction, int lines);
};
