/*
 * Some convenient functions. Expected to be cross-compilable on both
 * Windows & OSX.
*/
#include "stdafx.h"
#include "utils.h"
#include <vector>
#include <cstdlib>

#ifdef __APPLE__
#include <dlfcn.h>
#endif

// Legacy Device Types, taken from TbwKernelAPI.h
// Better if consecutive numbers since this value can be used as an index
typedef enum {
    TBW_TYPE_UNKNOWN = 0,
    TBW_TYPE_EXPERT = 1,
    TBW_TYPE_ORBIT = 2,
    TBW_TYPE_EAGLE = 3,
    TBW_TYPE_SLIMBLADE_NOCHORD = 4,
    TBW_TYPE_SLIMBLADE = 5,
    TBW_TYPE_KESTREL = 6,
    TBW_TYPE_EXPERT_W = 7,
    TBW_TYPE_EXPERT_BLE = 8,
    TBW_TYPE_MAX = 9
} TBW_TYPE;

struct LegacyTypeToProductID {
    int type;
    unsigned id;
} _lt2pid[] = {
    { TBW_TYPE_EXPERT, 0x1020 },            // Expert Mouse Trackball
    { TBW_TYPE_ORBIT, 0x1022 },             // Orbit Optical
    { TBW_TYPE_EAGLE, 0x2048 },             // Orbit Trackball with Scroll Ring
    { TBW_TYPE_SLIMBLADE_NOCHORD, 0x2041 }, // SlimBlade Trackball
    { TBW_TYPE_SLIMBLADE, 0x2041 },         // SlimBlade Trackball
    { TBW_TYPE_KESTREL, 0x8002 },           // Orbit Wireless (Kestrel)
    { TBW_TYPE_EXPERT_W, 0x8018 },          // Expert Wireless Mouse
    { TBW_TYPE_EXPERT_BLE, 0x8019 }         // Expert Wireless Mouse (Bluetooth)
};

unsigned LegacyDeviceTypeToProductID(int type)
{
    for (size_t i = 0; i < sizeof(_lt2pid)/sizeof(_lt2pid[0]); i++)
    {
        if (_lt2pid[i].type == type)
            return _lt2pid[i].id;
    }
    return 0;
}

unsigned ProductIDToLegacyDeviceType(unsigned id)
{
    for (size_t i = 0; i < sizeof(_lt2pid) / sizeof(_lt2pid[0]); i++)
    {
        if (_lt2pid[i].id == id)
            return _lt2pid[i].type;
    }
    return 0;
}

tbstring totbstring(std::string const& src)
{
#ifdef _WINDOWS
    return toUTF16(src);
    /*
    std::vector<TBCHAR> buf(src.size() + 1);
    mbstate_t state = { 0 };
    const char* pszSrc = src.c_str();
    size_t ret = 0;
    mbsrtowcs_s(
        &ret,
        &buf[0],
        buf.size(),
        &pszSrc,
        src.size(),
        &state);
    return tbstring(&buf[0], src.size());
     */
#else
    return src;
#endif
}

tbstring totbstring(std::wstring const& src)
{
#ifdef _WINDOWS
    return src;
#else
    return toUTF8(src);
    /*
    std::vector<TBCHAR> buf(src.size()*4 + 1);
    size_t ret = 0;
    buf.resize(wcstombs_s(
        &ret,
        &buf[0],
        buf.size(),
        src.c_str(),
        src.size()));
    return tbstring(&buf[0], buf.size());
     */
#endif
}

unsigned numberOfSetBits(uint32_t i)
{
    i = i - ((i >> 1) & 0x55555555);
    i = (i & 0x33333333) + ((i >> 2) & 0x33333333);
    return (((i + (i >> 4)) & 0x0F0F0F0F) * 0x01010101) >> 24;
}

std::string getModuleFileFullname()
{
#ifdef _WINDOWS
	CHAR szFilename[1024] = { 0 };
    ::GetModuleFileNameA(NULL, szFilename, sizeof(szFilename)/sizeof(szFilename[0]));
    return std::string(szFilename);
#else
    Dl_info module_info;
    if (dladdr(reinterpret_cast<void*>(getModuleFileFullname), &module_info) == 0) {
        // Failed to find the symbol we asked for.
        return std::string();
    }
    return std::string(module_info.dli_fname);
#endif
}
