#pragma once

enum ScrollDirection {
    SD_UNKNOWN = 0,
    SD_UP,
    SD_DOWN,
    SD_LEFT,
    SD_RIGHT,
	SD_TRACKBALL,
	SD_TRACKBALL_AUTO
};

