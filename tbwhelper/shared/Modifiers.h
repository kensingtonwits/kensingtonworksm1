/*
Contains macro definitions for modifier keys.
*/
#pragma once

#include <string>
#include <cstdint>

// Windows
#define WIN_MODIFIER_BITS   0x00FF

#define WIN_K_SHIFT		0x01
#define WIN_K_APPS		0x02   
#define WIN_K_CTRL		0x04
#define WIN_K_ALT		0x10
#define WIN_K_ALTGR     WIN_K_CTRL|WIN_K_ALT
#define WIN_K_WIN		0x40
#define WIN_K_LWIN		0x40
#define WIN_K_RWIN		0x80

// Mac

#define MAC_MODIDIFER_BITS  0xFF00

#define MAC_K_COMMAND   0x0100  // = 256
#define MAC_K_SHIFT     0x0200  // = 512
#define MAC_K_OPTION    0x0800  // = 2048
#define MAC_K_CTRL      0x1000  // = 4096
#define MAC_K_FN        0x2000  // = 8192
#define MAC_K_SPECIAL   0x4000  // = 16484

// Functions to convert between OS specific modifier scan code
// and TBW private modifier bitmask definitions
inline uint32_t ScanCodeToTBWModifier(uint16_t scanCode);
inline uint16_t TBWModifierToScanCode(uint32_t modifier);

// Function translate modifiers to/from bitmask to textual representation
std::string modifiersToText(uint32_t modifiers);
uint32_t modifiersFromText(const char* pszModifiers);
