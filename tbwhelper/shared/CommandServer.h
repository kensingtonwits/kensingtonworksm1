#pragma once

#ifdef _WINDOWS
#ifndef _WIN32_WINNT
// We're building on Win10, which is what _WIN32_WINNT defaults to 
// if not defined. (We probably can also get away with 0x0601(Win7)).
#define _WIN32_WINNT 0x0A00
#endif
#else
// OSX
#endif

#undef max

#include <memory>

#include "server_http.hpp"
#include "json.hpp"
#include "IDeviceManager.h"

using HttpServer = SimpleWeb::Server<SimpleWeb::HTTP>;

class Settings;
class DeviceConfig;

/*
 * A single threaded HTTP server. Can handle only one request at a time.
 */
class CommandServer {

protected:
    ITbwDeviceManager* _pDeviceMgr;
    HttpServer _server;
    std::thread _server_thread;
    bool _testMode;

private:
    void reportSuccess(std::shared_ptr<HttpServer::Response> response, nlohmann::json const& json);
    void reportException(std::shared_ptr<HttpServer::Response> response, std::exception const& e);
    void getCommonConfigQueryFields(std::shared_ptr<HttpServer::Request> request, uint16_t& device, std::string& app);
    uint32_t getButtonQueryField(std::shared_ptr<HttpServer::Request> request);

public:
    CommandServer(ITbwDeviceManager* pMgr, bool fTestMode = false);
    virtual ~CommandServer();

    HttpServer& server() { return _server; }

    void Start();
    void Stop();

    bool IsRunning() { return _server_thread.native_handle() != nullptr; }
};
