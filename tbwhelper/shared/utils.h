/*
 * Some convenient functions. Expected to be cross-compilable on both
 * Windows & OSX.
*/

#pragma once

#include <string>
#include <sstream>
#include <locale>
// #include <codecvt>
#include <boost/locale/encoding_utf.hpp>

using boost::locale::conv::utf_to_utf;

// normalize character type to TBCHAR which gets mapped to the platform
// default representation.
#ifdef _WINDOWS
typedef wchar_t TBCHAR;
#else
typedef char TBCHAR;
#endif

#ifdef _WINDOWS
#ifdef _DEBUG
#define DBGOUT(_x_) OutputDebugStringA(_x_)
#define DBGOUTW(_x_) OutputDebugStringW(_x_)
#else
#define DBGOUT(_x_)
#endif
#endif

// STL types corresponding to our character type.
typedef std::basic_string<TBCHAR> tbstring;
using tbistream = std::basic_ifstream<TBCHAR, std::char_traits<TBCHAR>>;
using tbostream = std::basic_ofstream<TBCHAR, std::char_traits<TBCHAR>>;
using tbstringstream = std::basic_stringstream<TBCHAR, std::char_traits<TBCHAR>, std::allocator<TBCHAR>>;

// Convert legacy device type enum to its equivalent USB product ID
unsigned LegacyDeviceTypeToProductID(int type);

// Convert USB product ID to equivalent legacy device type enum
unsigned ProductIDToLegacyDeviceType(unsigned id);

// Convert a from UTF-16 widechar to UTF-8
inline std::string toUTF8(std::wstring const& str)
{
//#if 1
    return utf_to_utf<char>(str.c_str(), str.c_str() + str.size());
    //std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>, wchar_t> converter;
    //return converter.to_bytes(str);
//#else // the same meaning as above
//    std::wstring_convert<std::codecvt_utf8<wchar_t>> myconv;
//    return myconv.to_bytes(str);
//#endif
}

inline std::string toUTF8(std::string const& str)
{
    return str;
}

inline std::wstring toUTF16(std::string const& str)
{
    return utf_to_utf<wchar_t>(str.c_str(), str.c_str() + str.size());
    //std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>, wchar_t> converter;
    //return converter.from_bytes(src);
}

//
inline std::string toUTF8String(std::string const& src)
{
    return src;
}

// A generic fn to convert & return strings in lowercase
template<typename T>
std::basic_string<T> tolower(const std::basic_string<T>& src)
{
    std::basic_string<T> lwr;;
    lwr.reserve(src.size() + 1);
    for (auto ch : src)
        lwr.push_back(tolower(ch));
    return lwr;
}

// Simple template to convert from string to desired type.
template<typename T>
T to(std::string const& str)
{
    std::stringstream ss(str);
    T val;
    ss >> val;
    return val;
}

template<>
inline std::string to(std::string const& str)
{
	return str;
}

// Same as above, but for wstring
template<typename T>
T to(std::wstring const& str)
{
    std::wstringstream ss(str);
    T val;
    ss >> val;
    return val;
}


template<>
inline std::wstring to(std::wstring const& str)
{
	return str;
}

// specialization for to<bool> from string
template<> inline bool to<bool>(std::string const& str)
{
    std::string copyStr = tolower(str);
    if (copyStr.compare("false") == 0 || copyStr.compare("0") == 0)
        return false;
    return true;
}

// specialization for to<bool> from wstring
template<> inline bool to<bool>(std::wstring const& str)
{
    std::wstring copyStr = tolower(str);
    if (copyStr.compare(L"false") == 0 || copyStr.compare(L"0") == 0)
        return false;
    return true;
}

// convert from STL string types to our platform sensitive type.
tbstring totbstring(std::string const& src);
tbstring totbstring(std::wstring const& src);

// returns number of bits set in a 32-bit unsigned integer
unsigned numberOfSetBits(uint32_t i);

// Returns module's filename, including its path
std::string getModuleFileFullname();
