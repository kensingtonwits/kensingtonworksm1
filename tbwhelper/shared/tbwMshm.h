#pragma once
#ifdef _WIN32 // *&*&*&V1_20201029
#ifndef _WINDOWS
#define _WINDOWS
#endif
#endif

#ifdef __APPLE__
// OSX
#include <pthread.h>
#else
#include <thread>
#endif

#include <boost/log/trivial.hpp>
#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/interprocess/sync/named_mutex.hpp>
#include <boost/interprocess/sync/named_condition.hpp>
#include <boost/interprocess/containers/string.hpp>
#include <boost/process.hpp>
#include <boost/date_time.hpp>
#include <iostream>

#ifdef _WINDOWS
#include <boost/detail/winapi/get_current_process.hpp>
#endif


using namespace std;
namespace bipc = boost::interprocess;


#define MSHMEM_SIZE		20480
#define MSHMEM_NAME			"tbwmshm"
#define MSHMEM_MUTEX_NAME			"tbwmshmmtex"
#define MSHMEM_CNDVAR_NAME			"tbwmshmcndvar"
#define MSHMEM_FHEADER		"header"
#define MSHMEM_FPID			"pid"
#define MSHMEM_FHBEAT		"heartbeat"
#define MSHMEM_FTAILER		"tailer"
#define MSHMEM_FTBWDEVICES	"tbwdevices"

typedef bipc::allocator<char, bipc::managed_shared_memory::segment_manager> CharAllocator;
typedef bipc::basic_string<char, std::char_traits<char>, CharAllocator> tbw_String;


/*
 * 
 */
namespace tbwMshm 
{
	enum
	{
		HEADER = 1,
		PID,
		HEARTBEAT,
		TAILER,
		TBWDEVICES,
	};

	bool tbwMshm_setup(uint32_t pid);

	bool tbwMshm_create(uint32_t pid);

	bool tbwMshm_open(uint32_t *pid);
	
	bool tbwMshm_remove();

	bool updateMshmDevices(std::string strDevices);

	std::string getMshmDevices();

	bool writeMshmElement(std::string element, const void *pvalue);
	bool readMshmElement(std::string element, void *pvalue);
	void wHeartbeat();
	int64_t rHeartbeat();

	bool is_Helper_Running();


};