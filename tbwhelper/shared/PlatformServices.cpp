
#include "stdafx.h"
#include "PlatformServices.h"
#ifdef _WINDOWS
#include "WinPlatformServices.h"
#else
#include "DarwinPlatformServices.h"
#endif

PlatformServices* PlatformServices::_instance = nullptr;

PlatformServices * PlatformServices::getInstance()
{
    if (!_instance)
        _instance = new PlatformServices();
    return _instance;
}

PlatformServices::PlatformServices()
    : _impl(nullptr)
{
#ifdef _WINDOWS
    _impl = new WinPlatformServices();
#else
    _impl = new DarwinPlatformServices();
#endif
}

PlatformServices::~PlatformServices()
{
    delete _impl;
}

void PlatformServices::destroy()
{
    delete _instance; _instance = nullptr;
}

void PlatformServices::postKeystroke(uint16_t usModifiers, uint16_t usKeyCode, bool fDown, bool fModifiersOnly)
{
    _impl->postKeystroke(usModifiers, usKeyCode, fDown, fModifiersOnly);
}

void PlatformServices::postString(const std::string& string)
{
    _impl->postString(string);
}

void PlatformServices::postPointerClick(int button, bool fDown)
{
    _impl->postPointerClick(button, fDown);
}

void PlatformServices::configurePointer(int speed, int acceleration, unsigned scrollLines)
{
    _impl->configurePointer(speed, acceleration, scrollLines);
}

void PlatformServices::openResource(const std::string& resource, const std::vector<std::string>& args, int type)
{
    _impl->openResource(resource, args, type);
}

std::string PlatformServices::getPreferencesFolder()
{
    return _impl->getPreferencesFolder();
}

std::string PlatformServices::getLogFileFolder()
{
    return _impl->getLogFileFolder();
}

void PlatformServices::system(std::string const& verb, nlohmann::json const& addlArgs)
{
    return _impl->system(verb, addlArgs);
}

nlohmann::json PlatformServices::getFavoriteApps(int count, const std::set<std::string>& exclude)
{
	return _impl->getFavoriteApps(count, exclude);
}

std::string PlatformServices::showPopupMenu(std::vector<std::string> const& items, std::vector<std::string> const& labels, int timeOut/*=-1*/)
{
	return _impl->showPopupMenu(items, labels, timeOut);
}

uint32_t PlatformServices::getCurPointerSpeed()
{
    return _impl->getCurPointerSpeed();
}

uint32_t PlatformServices::getCurPointerAccelerationRate()
{
    return _impl->getCurPointerAccelerationRate();
}

uint32_t PlatformServices::getCurScrollSpeed()
{
    return _impl->getCurScrollSpeed();
}

bool PlatformServices::getScrollInvert()
{
    return _impl->getScrollInvert();
}

void PlatformServices::postScrollNavigation(ScrollDirection direction, int lines)
{
    _impl->postScrollNavigation(direction, lines);
}