/*
 * TbwSettings.h
 *
 *  Header file contains code (mostly preprocessor macros) copied from legacy
 *  TbwPreferences.h. A new file was created instead of directly importing 
 *  the legacy TbwPrefernces.h so that this code can be ported to OSX. (Legacy
 *  file is Windows specific)
 *
 *  If you need better proof that the contents and legacy import -- see the
 *  embedded hard tabs!
 */
#pragma once

// TbwSettings.xml string literals & other macros copied from TbwPrefernces.h

#ifndef TEXT

#ifdef _WINDOWS
#define TEXT(x) L##x
#else
#define TEXT(x) x
#endif

#endif

// string definitions for TbwSettings.xml configuration format
#define kTBWXMLSettings			TEXT("TbwSettings")
#define kTBWXMLSettingsVersion	TEXT("version")
#define kTBWXMLApp				TEXT("Application")
#define kTBWXMLAppActive		TEXT("active")
#define kTBWXMLAppExe			TEXT("executable")
#define kTBWXMLAppName			TEXT("name")
#define kTBWXMLAppVersion		TEXT("versioninfo")
#define kTBWXMLAppProductName	TEXT("productname")
#define kTBWXMLDev				TEXT("Device")
#define kTBWXMLDevId			TEXT("id")
#define kTBWXMLDevSnapToDefault	TEXT("snapToDefault")
#define kTBWXMLDevScrollSpeed	TEXT("scrollingSpeed")
#define kTBWXMLDevScrollInvert	TEXT("scrollingInvert")
#define kTBWXMLDevAcceleration	TEXT("acceleration")
#define kTBWXMLDevSpeed 		TEXT("speed")
#define kTBWXMLDevSnapModifiers	TEXT("snapModifiers")
#define kTBWXMLDevSlowModifiers	TEXT("slowDownModifiers")
#define kTBWXMLDevSlowDivider	TEXT("slowDownDivider")
#define kTBWXMLDevIScroll 		TEXT("inertialscroll")
#define kTBWXMLDevIScrollTimer	TEXT("inertialscrolltimer")
#define kTBWXMLDevIScrollSteps	TEXT("inertialscrollmaxnullsteps")
#define kTBWXMLDevIScrollFactor	TEXT("inertialscrollfactor")
#define kTBWXMLAct				TEXT("Action")
#define kTBWXMLActName			TEXT("name")
#define kTBWXMLActDisplayed		TEXT("description")
#define kTBWXMLActCommand		TEXT("command")
#define kTBWXMLActModifiers		TEXT("modifiers")
#define kTBWXMLActKeyCode		TEXT("argument")
#define kTBWXMLActRepeat		TEXT("repeat")
#define kTBWXMLActDelay			TEXT("delay")
#define kTBWXMLActPath			TEXT("path")
#define kTBWXMLActParameters	TEXT("parameters")
#define kTBWXMLActState			TEXT("state")
#define kTBWXMLCtrlDecX			TEXT("DecrementX")
#define kTBWXMLCtrlIncX			TEXT("IncrementX")
#define kTBWXMLCtrlDecY			TEXT("DecrementY")
#define kTBWXMLCtrlIncY			TEXT("IncrementY")
#define kTBWXMLCtrlDecZ			TEXT("DecrementWheel")
#define kTBWXMLCtrlIncZ			TEXT("IncrementWheel")
#define kTBWXMLCtrlButton1		TEXT("Button1")
#define kTBWXMLCtrlButton2		TEXT("Button2")
#define kTBWXMLCtrlButton3		TEXT("Button3")
#define kTBWXMLCtrlButton4		TEXT("Button4")
#define kTBWXMLCtrlButtonChord1	TEXT("ButtonChord1")
#define kTBWXMLCtrlButtonChord2	TEXT("ButtonChord2")
#define kTBWXMLCtrlButtonSnippets	TEXT("SnippetsDB")
#define kTBWXMLCmdUnassigned	TEXT("CommandUnassigned")
#define kTBWXMLCmdUseGlobal		TEXT("CommandDefault")
#define kTBWXMLCmdTypeKeystroke	TEXT("CommandKeystroke")
#define kTBWXMLCmdShortcut		TEXT("CommandShortcut")
#define kTBWXMLCmdSpecialKey	TEXT("CommandSpecialKey")
#define kTBWXMLCmdScroll		TEXT("CommandScroll")
#define kTBWXMLCmqWindowAction	TEXT("CommandWindowAction")
#define kTBWXMLCmdOpenSpecial	TEXT("CommandOpenSpecialItem")
#define kTBWXMLCmdOpenItem		TEXT("CommandOpenItem")
#define kTBWXMLCmdMacro			TEXT("CommandMacro")
#define kTBWXMLCmdSnippet		TEXT("CommandSnippet")
#define kTBWXMLModShift			TEXT("ModifierShift")
#define kTBWXMLModControl		TEXT("ModifierControl")
#define kTBWXMLModAlt			TEXT("ModifierAlt")
#define kTBWXMLModWindows		TEXT("ModifierWindows")
#define kTBWXMLScrollUp			TEXT("ScrollUp")
#define kTBWXMLScrollDown		TEXT("ScrollDown")
#define kTBWXMLScrollLeft		TEXT("ScrollLeft")
#define kTBWXMLScrollRight		TEXT("ScrollRight")
#define kTBWXMLSnippet			TEXT("Snippet")
#define kTBWXMLStep				TEXT("Step")
#define kTBWXMLString			TEXT("string")
#define kTBWXMLTitle			TEXT("label")
#define kTBWXMLId				TEXT("id")

//==============================================================================
// command IDs:
enum {
    kCommandUseSystem = 0,
    kCommandUseGlobal = 4,
    kCommandNone,
    kCommandSendToClient,

    kCommandSnippet1,
    kCommandSnippet2,
    kCommandSnippet3,
    kCommandSnippet4,
    kCommandSnippet5,
    kCommandSnippet6,
    kCommandSnippet7,
    kCommandSnippet8,
    kCommandSnippet9,
    kCommandSnippet10,

    kCommandModifiers = 31,	// (native) use modifiers
    kCommandKeystroke = 32,	// (native) use argument with virtual key code, modifiers, repeat and delay
    kCommandTypeChar,
    kCommandOfficeShortcut,

    kCommandRightClickLock,
    kCommandMiddleClickLock,
    kCommandMiddleClick,
    kCommandButton4,
    kCommandButton5,
    kCommandRightDoubleClick,
    kCommandMiddleDoubleClick,
    kCommandClick = 48,	// (native) use argument for the click, modifiers, repeat and delay
    kCommandRightClick,
    kCommandDoubleClick,
    kCommandTripleClick,
    kCommandClickLock = 52,	// (native) use argument for the click, modifiers.
    kCommandWheel = 53,	// (native) keyCode for number of ticks and direction, modifiers, repeat, delay
    kCommandWheelH = 54,   // (native)
    kCommandMove,

    kCommandMacro = 60,	// (native) sequence, no arguments see examples
    kCommandSnippet = 61,	// (native) snippet list, no arguments see examples
    kCommandPause = 62,	// (native) pause - meant to be used in macros. Delay for the length in ms

    kCommandOpenItem = 64,	// (native) open document/executable. use Path to specify the item
    kCommandOpenSelf,
    kCommandOpenApp,
    kCommandOpenExecute,
    kCommandOpenExplorer,
    kCommandOpenMail,
    kCommandOpenWeb,
    kCommandOpenUrl,

    kCommandOpenFavorites = 100,	// (native)
    kCommandOpenDocuments,				// (native)
    kCommandOpenPictures,				// (native)
    kCommandOpenMusic,					// (native)
    kCommandOpenMovies,					// (native)
    kCommandOpenFonts,					// (native)
    kCommandOpenAdminTools,				// (native)
    kCommandOpenHistory,				// (native)
    kCommandOpenNetwork,				// (native)
    kCommandOpenTaskManager,
    kCommandOpenExplorerWindow,

    kCommandRun,
    kCommandLockWorkstation,			// (native)
    kCommandSearch,

    kCommandMute = 128,
    kCommandVolumeDown,
    kCommandVolumeUp,
    kCommandPreviousTrack,
    kCommandNextTrack,
    kCommandPlayPause,
    kCommandMediaStop,
    kCommandEject,

    kCommandBack = 160,
    kCommandForward,
    kCommandStop,
    kCommandReload,

    kCommandNew = 176,
    kCommandOpen,
    kCommandSave,
    kCommandClose,
    kCommandPrint,
    kCommandUndo,
    kCommandRedo,
    kCommandQuit,
    kCommandCut,
    kCommandCopy,
    kCommandPaste,
    kCommandSelectAll,
    kCommandGetInfo,
    kCommandFind,
    kCommandHelp,

    kCommandReply,
    kCommandFwd,
    kCommandSend,
    kCommandSpell,
    kCommandRefresh,

    kCommandShowDesktop,
    kCommandMinimizeWindow,		// (native)
    kCommandMaximizeWindow,		// (native)
    kCommandCloseWindow,		// (native)
    kCommandShowNextWindow,
    kCommandMinimizeAll,		// (native)

    kCommandLogOff,				// (native)
    kCommandSwitchKbdLayout,	// cmd-space is useful in J version
    kCommandCaptureScreen,
    kCommandCaptureSelection,
    kCommandJapaneseEisu,		// Japanese "Eisu" key (charCode $10, keyCode $66)
    kCommandJapaneseKana,		// Japanese "Kana" key (charCode $10, keyCode $68)

    kCommandScrollUp = 224,
    kCommandScrollDown,
    kCommandScrollLeft,
    kCommandScrollRight,
    kCommandPageUp,
    kCommandPageDown,
    kCommandPageLeft,
    kCommandPageRight,
    kCommandAccelerateUp,
    kCommandAccelerateDown,
    kCommandAccelerateLeft,
    kCommandAccelerateRight,
    kCommandPageHome,
    kCommandPageEnd,
    kCommandEndOfScrolls,

    kCommandSnapWindowLeft,
    kCommandSnapWindowRight,
    kCommandSnapWindowTop,
    kCommandSnapWindowBottom,
    kCommandModernUITaskView,
    kCommandVirtualDesktopNew,
    kCommandVirtualDesktopClose,
    kCommandVirtualDesktopSwitchLeft,
    kCommandVirtualDesktopSwitchRight
};

#define kMaxTbwControls				32
