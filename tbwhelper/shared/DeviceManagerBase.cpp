/*
DeviceManagerBase implementation.
*/
#include "stdafx.h"
#include "DeviceManagerBase.h"
#include "Settings.h"
#include <boost/log/trivial.hpp>
#include <boost/lexical_cast.hpp>
#include "Modifiers.h"

using namespace devidremap;

class SyncAutoLock {
    DeviceManagerBase* _pMgr;
public:
    SyncAutoLock(DeviceManagerBase* p) : _pMgr(p) { _pMgr->objUnlock(); }
    SyncAutoLock() { _pMgr->objUnlock(); }
};

DeviceManagerBase::~DeviceManagerBase()
{}

nlohmann::json DeviceManagerBase::getSettings()
{
    SyncAutoLock lg(this);

    nlohmann::json j;
    j = *_settings.get();
    return j;
}

void DeviceManagerBase::setSettings(const nlohmann::json & j)
{
    SyncAutoLock lg(this);

	// enumerate all connected devices and store their currently active setting
    // names (which is actually the app name). We need to do this as resetting
    // _settings object with new JSON data will destroy all the AppConfig &
    // DeviceConfig members of Settings. Therefore all DeviceConfig reference
    // held by Device object itself will be invalidated.
    //
    // Therefore we need a means to retrieve the current app name from the newly
    // stored setting. And we do that by looking up this table.
    std::map<uint16_t, std::string> deviceApps;
    for (auto& device : _devices)
    {
        auto& dc = device->getConfig();
        deviceApps.insert(std::make_pair(device->id(), dc.appName()));
    }

    // Note that this would invalidate Device::_pConfig member!
	std::unique_ptr<Settings> newSettings(new Settings(false));
	j.get_to(*newSettings.get());

	_settings.reset(newSettings.release());

	// Create default configuration for devices if no setting exists
	// in the restored settings.
	for (auto& device : _devices)
	{
		_settings->createDefaultDeviceConfigIfDoesntExist(device->id());
	}

	// Since Device::_pConfig has been invalidated, we need to update the
    // DeviceConfig for all Device objects for new settings.
    for (auto& device : _devices)
    {
		// Look up the last used app name from the cache that we created earlier
        const auto& appName = deviceApps.find(device->id())->second;
        device->setConfig(
            _settings->getDeviceConfig(
                appName.c_str(),
                device->id(),
                true
            ),
            true
        );
    }
	_settings->save();
}

nlohmann::json DeviceManagerBase::getGeneralConfig()
{
    SyncAutoLock lg(this);
    nlohmann::json j;

    j["timeStamp"] = _settings->timeStampAsUTCStr();
    j["trayIcon"] = _settings->trayIcon();

    return j;
}

void DeviceManagerBase::setGeneralConfig(const nlohmann::json& j)
{
    SyncAutoLock lg(this);

    if (j.find("trayIcon") != j.end()) {
        bool trayIcon = false;
        j.at("trayIcon").get_to(trayIcon);
        if (trayIcon != _settings->trayIcon()) {
            _settings->trayIcon(trayIcon);
            _settings->save();
        }
    }
}

nlohmann::json DeviceManagerBase::getDevices()
{
	BOOST_LOG_TRIVIAL(debug) << "DeviceManagerBase::getDevices - 0";
    SyncAutoLock lg(this);

    auto deviceIds = _settings->getDevices("*");

	BOOST_LOG_TRIVIAL(debug) << "DeviceManagerBase::getDevices - 1";


    json jDevices = json::array();
    for (auto deviceId : deviceIds)
    {
        bool present = false;
		//uint16_t realDeviceId = 0;
        std::string name = "";
		std::string realIds = "";
        uint16_t version = 0;
		std::string iface;
        for (auto& device : _devices)
        {
            if (device->id() == (uint16_t)deviceId)
            {
				if (iface.length() > 0)
					iface += ",";
				if (realIds.length() > 0)
					realIds += ",";
				iface += connectionInterfaceToStr(device->connectionInterface());
				realIds += boost::lexical_cast<std::string>(device->realId());
				//realDeviceId = device->realId();
                name = device->name();
                version = device->version();
                present = true;
	BOOST_LOG_TRIVIAL(debug) << " . " << name << " . ";
            }
        }
        jDevices.push_back(json{
            {"id", deviceId },
			{"realId", realIds },
			{"interface", iface },
            {"name", name },
            {"version", version },
            {"present", present}
        });
    }
    return json{
        { "devices", jDevices }
    };
}

nlohmann::json DeviceManagerBase::getApps(unsigned deviceId)
{
    SyncAutoLock lg(this);

    auto apps = _settings->getApps(deviceId);

    json ret = nlohmann::json::array();
    for (auto app : apps) {
        ret.push_back(json{
            { app, _settings->getAppName(deviceId, app.c_str()) }
            });
    }
    return json{ { "apps", ret } };
}

void DeviceManagerBase::createApp(unsigned deviceId, const nlohmann::json& jApp)
{
    SyncAutoLock lg(this);
    _settings->createApp(
		deviceId,
        jApp.at("name").get<std::string>().c_str(),
        jApp.at("identifier").get<std::string>().c_str()
    );
}

void DeviceManagerBase::deleteApp(unsigned deviceId, const nlohmann::json& jApp)
{
    SyncAutoLock lg(this);

    std::string identifier = jApp.at("identifier").get<std::string>();
    if (identifier == "*")
        throw std::runtime_error("Global app profile cannot be deleted");

    _settings->deleteApp(deviceId, identifier.c_str());
}

nlohmann::json DeviceManagerBase::getPointerConfig(const char* app, uint16_t deviceId)
{
    SyncAutoLock lg(this);

    auto& deviceCfg = _settings->getDeviceConfig(app, deviceId, false);
    return json{
        { "speed", deviceCfg.pointerSpeed() },
        { "acceleration", deviceCfg.pointerAcceleration() },
        { "accelerationRate", deviceCfg.pointerAccelerationRate() },
        { "slowModifiers", deviceCfg.slowPointerModifiers() },
        { "lockedAxisModifiers", deviceCfg.lockedAxisModifiers() }
    };
}

void DeviceManagerBase::setPointerConfig(const char* app, uint16_t deviceId, const nlohmann::json& j)
{
    SyncAutoLock lg(this);

    BOOST_LOG_TRIVIAL(debug) << "DeviceManagerBase::setPointerConfig -"
        << " app: " << app
        << ", device: " << deviceId
        << ", config: " << j;

    auto& deviceCfg = _settings->getDeviceConfig(app, deviceId, false);

    if (j.find("speed") != j.end())
        deviceCfg.pointerSpeed(j.at("speed").get<uint32_t>());

    if (j.find("acceleration") != j.end())
        deviceCfg.pointerAcceleration(j.at("acceleration").get<bool>());

    if (j.find("accelerationRate") != j.end())
        deviceCfg.pointerAccelerationRate(j.at("accelerationRate").get<uint32_t>());

    if (j.find("slowModifiers") != j.end())
        deviceCfg.slowPointerModifiers(j.at("slowModifiers").get<std::string>().c_str());

    if (j.find("lockedAxisModifiers") != j.end())
        deviceCfg.lockedAxisModifiers(j.at("lockedAxisModifiers").get<std::string>().c_str());

    _settings->save();

    for (auto& device : _devices)
    {
        if (device->id() == deviceId && device->getConfig().appName() == deviceCfg.appName())
        {
            device->updatePointerParams();
        }
    }
}

nlohmann::json DeviceManagerBase::getScrollWheelConfig(const char* app, uint16_t deviceId)
{
    SyncAutoLock lg(this);

    auto& deviceCfg = _settings->getDeviceConfig(app, deviceId, false);
    return json{
        { "wheel", "vert" },
        { "speed", deviceCfg.scrollSpeed() },
        { "invert", deviceCfg.invertScroll() },
        { "inertial", deviceCfg.inertialScroll() }
    };
}

void DeviceManagerBase::setScrollWheelConfig(const char* app, uint16_t deviceId, const nlohmann::json& j)
{
    SyncAutoLock lg(this);

    BOOST_LOG_TRIVIAL(debug) << "DeviceManagerBase::setScrollWheelConfig -"
        << " app: " << app
        << ", device: " << deviceId
        << ", config: " << j;

    auto& deviceCfg = _settings->getDeviceConfig(app, deviceId, false);

    if (j.find("speed") != j.end())
        deviceCfg.scrollSpeed(j.at("speed").get<uint32_t>());
    if (j.find("invert") != j.end())
        deviceCfg.invertScroll(j.at("invert").get<bool>());
    if (j.find("inertial") != j.end())
        deviceCfg.inertialScroll(j.at("inertial").get<bool>());

    _settings->save();

    for (auto& device : _devices)
    {
        if (device->id() == deviceId && device->getConfig().appName() == deviceCfg.appName())
        {
			BOOST_LOG_TRIVIAL(debug) << "Updating scroll params of real device id: " << device->realId();
            device->updateScrollWheelParams();
        }
    }
}

nlohmann::json DeviceManagerBase::getButtonAction(const char* app, uint16_t deviceId, uint32_t buttonMask)
{
    SyncAutoLock lg(this);

    BOOST_LOG_TRIVIAL(info) << "DeviceManagerBase::getButtonAction -"
        << " app: " << app
        << ", device: " << deviceId
        << ", buttonMask: " << buttonMask;

    auto& deviceCfg = _settings->getDeviceConfig(app, deviceId, false);
    /*
    try {
        return Settings::getActionHandler(deviceCfg.getButtonAction(buttonMask));
    } catch (std::exception) {
    }
    */

    // return the actual JSON
    return deviceCfg.getButtonAction2(buttonMask);
}

void DeviceManagerBase::setButtonAction(const char* app, uint16_t deviceId, uint32_t buttonMask, const nlohmann::json& action)
{
    SyncAutoLock lg(this);

    BOOST_LOG_TRIVIAL(info) << "DeviceManagerBase::setButtonAction -"
        << " app: " << app
        << ", device: " << deviceId
        << ", buttonMask: " << buttonMask
        << ", action: " << action;

    auto& deviceCfg = _settings->getDeviceConfig(app, deviceId, false);
    /*
    try {
        ActionFactory af;
        deviceCfg.setButtonAction(
            buttonMask,
            af.createActionHandler(action)
        );
    } catch(std::exception) {
    }
    */

    // serializing to ACTIONPTR failed, try to save it as a menmonic
    deviceCfg.setButtonAction2(buttonMask, action);

    _settings->save();

    for (auto& device : _devices)
    {
        if (device->id() == deviceId && device->getConfig().appName() == deviceCfg.appName())
        {
            device->updateButtonsConfig();
        }
    }
}

bool DeviceManagerBase::trayIcon()
{
    bool enable = false;
    {
        SyncAutoLock lg(this);
        enable = _settings->trayIcon();
    }
    return enable;
}

void DeviceManagerBase::trayIcon(bool enable)
{
    SyncAutoLock lg(this);
    if (enable != _settings->trayIcon()) {
        _settings->trayIcon(enable);
    }
}

#ifdef TAU
void DeviceManagerBase::emulateButtonClick(uint16_t deviceId, uint32_t buttonMask)
{
    SyncAutoLock lg(this);

    auto device = getDevice(deviceId);
    if (!device)
        throw std::runtime_error("Device not found!");

    device->emulateButtonClick(buttonMask);
}
#endif

nlohmann::json DeviceManagerBase::getSnippets(const char* app, uint16_t deviceId)
{
	SyncAutoLock lg(this);

	BOOST_LOG_TRIVIAL(debug) << "DeviceManagerBase::getSnippets -"
		<< " app: " << app
		<< ", device: " << deviceId;

	auto& deviceCfg = _settings->getDeviceConfig(app, deviceId, false);
	auto snippets = deviceCfg.getSnippetStrings();
	json jSnippets = json::array();

	for (auto& it: snippets)
	{
		//DeviceConfig::SnippetString& ss = it;
		jSnippets.push_back(it);
	}
	return jSnippets;
}

void DeviceManagerBase::objLock()
{
    _mutex.lock();
}

void DeviceManagerBase::objUnlock()
{
    _mutex.unlock();
}

void DeviceBase::init(uint16_t id, uint16_t firmwareVersion, const char * name)
{
	_id = getRemappedDeviceId(id);;
	_realId = id;
	_interface = getConnectionInterface(id);
	_firmwareVersion = firmwareVersion;
	_name = name;
}

// internal method -- mutex should've been acquired!
DeviceBase * DeviceManagerBase::getDevice(uint16_t productId)
{
    DeviceBase* ret = nullptr;

    for (DEVICEPTRLIST::iterator it = _devices.begin(); it != _devices.end(); it++) {
        if ((*it)->id() == productId) {
            ret = (*it).get();
            break;
        }
    }
    return ret;
}

void DeviceBase::setConfig(DeviceConfig& config, bool force)
{
    if (_pConfig != &config || force)
    {
        BOOST_LOG_TRIVIAL(debug) << "DeviceBase::setConfig - setting new config";

        if (_pConfig != &config)
        {
            //if (_pConfig)
            //    _pConfig->deactivate();
            _pConfig = &config;
            //_pConfig->activate();
        }

        try
        {
            updatePointerParams();
            updateScrollWheelParams();
            updateButtonsConfig();
        }
        catch (std::exception& e)
        {
            // TODO: review exception handling
            BOOST_LOG_TRIVIAL(error) << "Error settings buttons config: " << e.what();
        }
    }
    else
    {
        BOOST_LOG_TRIVIAL(debug) << "Device::setConfig - same config as before, not setting";
    }
}

DeviceConfig & DeviceBase::getConfig() const
{
    return *_pConfig;
}

bool DeviceBase::postKeystroke(uint32_t modifiers, uint32_t keyCode, bool fDown, bool fModifiersOnly)
{
    return false;
}

bool DeviceBase::postPointerClick(uint32_t button, bool fDown)
{
    return false;
}

bool DeviceBase::postScrollNavigation(ScrollDirection direction, int lines)
{
    return false;
}
