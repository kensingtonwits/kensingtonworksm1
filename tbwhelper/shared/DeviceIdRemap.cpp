#include "stdafx.h"
#include "DeviceIdRemap.h"
#include <string>

namespace devidremap {

struct DeviceRemapInfo {
	uint16_t realId;
	uint16_t pseudoId;
	ConnectionInterface iface;
};

/*
 * Update this table for new devices that support dual connection modes.
 *
 * The device id reporting scheme that we use is that Bluetooth device ids
 * are reported as USB device ids. However, the connection interface field
 * is initialized as per the device connection interface.
 */
const DeviceRemapInfo DEVICE_ID_REMAP_TABLE[] = {
	{ 32793, 32792, ConnectionInterface::INTERFACE_BLUETOOTH },	// Expert Wireless Mouse BT
	{ 64, 32859, ConnectionInterface::INTERFACE_BLUETOOTH },	// Ergo Wireless Mouse BT
	{ 32873, 32872, ConnectionInterface::INTERFACE_BLUETOOTH }, 	// Ergo Vertical Trackball BT
	{ 32935, 32934, ConnectionInterface::INTERFACE_BLUETOOTH }, 	// Orbit Wireless Trackball with scroll ring
	{ 32936, 32934, ConnectionInterface::INTERFACE_BLUETOOTH } 	// Orbit Wireless Trackball with scroll ring
};

/*
 * Returns the psuedo device id for devices whose id is remapped to
 * another device id. If the given id need not be remapped, returns
 * it without remapping.
 */
uint16_t getRemappedDeviceId(uint16_t realId)
{
	for (auto& remap : DEVICE_ID_REMAP_TABLE) {
		if (realId == remap.realId) {
			return remap.pseudoId;
		}
	}

	return realId;
}

/*
 * Returns the connection interface for the given device id. Note that the
 * device id has to be the real device id.
 */
ConnectionInterface getConnectionInterface(uint16_t realId)
{
	for (auto& remap : DEVICE_ID_REMAP_TABLE) {
		if (realId == remap.realId) {
			return remap.iface;
		}
	}
	return ConnectionInterface::INTERFACE_USB;
}

std::string connectionInterfaceToStr(ConnectionInterface iface)
{
	if (iface == ConnectionInterface::INTERFACE_USB)
		return "usb";
	else if (iface == ConnectionInterface::INTERFACE_BLUETOOTH)
		return "bluetooth";

	return "unknown";
}

} // using namespace devidremap
