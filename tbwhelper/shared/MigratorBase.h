#pragma once

#include <fstream>
#include <map>
#include <string>
#include <boost/filesystem.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/log/trivial.hpp>

#include "json.hpp"
using namespace nlohmann;

#include "Modifiers.h"
#include "utils.h"
#include "jsonutils.h"

/**
 * Base class for settings migrator. Class abstracts behavior that is
 * common to both Windows & Mac.
 */
class MigratorBase {

protected:
    struct KeystrokeCommand {
        uint32_t modifiers;
        uint32_t repeat;
        std::string macro;
        std::string json;

        KeystrokeCommand()
            : modifiers(0), repeat(0), macro(), json()
        {}
    };
    typedef std::multimap<uint32_t, KeystrokeCommand> KEYSTROKECOMMANDS;
    KEYSTROKECOMMANDS _keystrokeCommands;
    
protected:
    MigratorBase()
    {
        buildKeystrokeToCommandLookupTable();
    }
    virtual ~MigratorBase()
    {}
    
    void buildKeystrokeToCommandLookupTable()
    {
        #ifdef _WINDOWS
        boost::filesystem::path localFolder("./tbwactions.json");
        #else
        #ifdef DEBUG
        // This allows running the program from anywhere.
        boost::filesystem::path localFolder("/tmp/tbwactions.json");
        #else
        boost::filesystem::path localFolder("../Resources/tbwactions.json");
        #endif
        #endif
        
        std::ifstream fin(toUTF8(localFolder.native()));
        json jdefmacros;
        fin >> jdefmacros;
        int nKeyStrokeCommands = 0;
        for (json::const_iterator it = jdefmacros.begin(); it != jdefmacros.end(); ++it)
        {
            json const& jCommand = it.value();
            try
            {
                std::string command;
                jCommand.at("command").get_to(command);

                if (command == "keystroke")
                {
                    BOOST_LOG_TRIVIAL(debug) << it.key() << " : " << it.value();
                    uint32_t keyCode = getFromStrValue<unsigned>(jCommand, "keyCode");

                    KeystrokeCommand kc;
                    kc.macro = it.key();
                    std::string modifiers;
                    jCommand.at("modifiers").get_to(modifiers);
                    kc.modifiers = modifiersFromText(modifiers.c_str());
                    if (jCommand.find("repeat") != jCommand.end())
                        jCommand.at("repeat").get_to(kc.repeat);
                    kc.json = jCommand.dump();
                    _keystrokeCommands.insert(std::make_pair(keyCode, kc));
                    nKeyStrokeCommands++;
                }
            }
            catch (const std::exception& e)
            {
                BOOST_LOG_TRIVIAL(debug) << "Exception: " << e.what() << "\n";
            }
        }

        BOOST_LOG_TRIVIAL(debug) << "Total keystroke commands: " << nKeyStrokeCommands << ", multimap size: " << _keystrokeCommands.size();

    }
};
