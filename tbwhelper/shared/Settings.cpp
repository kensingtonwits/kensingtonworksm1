/*
 * Settings.cpp
 *
 *  Definition counterpart of Settings.h.
 */
#include "stdafx.h"
#include "Settings.h"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <locale>
#include <codecvt>
#include <functional>
#include <thread>
#include <ctime>
#include <memory>
#include <algorithm>
#include <boost/filesystem.hpp>
#include <boost/tokenizer.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/log/trivial.hpp>

#include "Settings.h"
#include "PlatformServices.h"
#include "Modifiers.h"
#include "jsonutils.h"

const size_t MAX_SNIPPETS_COUNT = 10;

#if 0
// Begin Player.c
static BASEACTION ModifiersAction;
//static TIMERTHREADCONTEXT ModifiersContext;
static UINT ModifiersTimer = 0;
static WORD CurrentModificatorStatus = 0;
static WORD FutureModificatorStatus = 0;
static BOOL SyncMode = FALSE;
static CRITICAL_SECTION PlayerCS;

void DoModifiers(BOOL nocompress, BOOL enforce, BOOL numlock)
{
    int PostedModifiersUp = 0;

    if (nocompress)
    {
        if (numlock)
        {
            if ((CurrentModificatorStatus == 0) && (FutureModificatorStatus != 0)) PressNumLock(TRUE);
        }
        PostedModifiersUp = PostKeystroke(CurrentModificatorStatus, 0, FALSE);
        PostKeystroke(FutureModificatorStatus, 0, TRUE);
    } // if(compress)
    else
    {
        if (numlock)
        {
            if ((CurrentModificatorStatus == 0) && (CurrentModificatorStatus != FutureModificatorStatus)) PressNumLock(TRUE);
        }
        PostedModifiersUp = PostKeystroke(CurrentModificatorStatus & ~FutureModificatorStatus, 0, FALSE);
        PostKeystroke(FutureModificatorStatus & ~CurrentModificatorStatus, 0, TRUE);
    }

    CurrentModificatorStatus = FutureModificatorStatus;

    if (numlock)
    {
        if ((CurrentModificatorStatus == 0) && PostedModifiersUp) PressNumLock(FALSE);
    }

}
// End Player.c imports
#endif

//const wchar_t *GetWC(const char *c)
//{
//    const size_t cSize = strlen(c) + 1;
//    wchar_t* wc = new wchar_t[cSize];
//    mbstowcs(wc, c, cSize);
//
//    return wc;
//}

/*
 * A simple class to backup the original Settings.xml file
 * which will get restored in the class destructor. The restoration
 * can be controlled by 'releasing' the temp file, which will be
 * used when the rest of the operations on the file is completed
 * successfully.
*/
#if 1
class AutoSettingsBackup {

    boost::filesystem::path _temp;
    bool _restore;

public:
    AutoSettingsBackup()
        : _temp()
        , _restore(true)
    {
        _temp = boost::filesystem::temp_directory_path();
        _temp /= boost::filesystem::unique_path();
        // copy the original settings to temporary file.
        boost::filesystem::copy_file(boost::filesystem::path(
            Settings::getFileFullPath()), _temp);
    }
    ~AutoSettingsBackup()
    {
        // restore the backup to the original
        if (_restore)
            boost::filesystem::copy_file(_temp, boost::filesystem::path(
                Settings::getFileFullPath()));

        // erase the backup settings file
        boost::filesystem::remove(_temp);
    }
    void DoNotRestore() // do not restore
    {
        _restore = false;
    }
};
#endif

static std::map<std::string, PFN_CREATE_ACTION_OBJECT> s_actionFactoryFns;

// Returns an initialized ActionBase specialized class object corresponding
// to the given command id in arg 1.
//
// Throws exception if kTBWCommand is not a recognized value.
static ACTIONPTR getActionHandlerFromName(std::string const& actionName) // throw(std::exception)
{
    // from actionName be able to construct corresponding ActionBase derived class.
    if (s_actionFactoryFns.find(actionName) != s_actionFactoryFns.end()) {
        PFN_CREATE_ACTION_OBJECT pfn = s_actionFactoryFns[actionName];
        return ACTIONPTR(pfn());
    }
    throw std::runtime_error("Unknown action name");
}

void ActionBase::registerChild(const char* name, PFN_CREATE_ACTION_OBJECT pfnFactory)
{
    s_actionFactoryFns[name] = pfnFactory;
}

void ActionBase::load(json const&)
{
}

void ActionBase::save(json& j)
{
}

REGISTER_ACTION_CLASS(ActionNone)

REGISTER_ACTION_CLASS(ActionSequence)

/*
 Parse a sequence step. XML fragment:
    <Step command="32" modifiers="0" argument="65" repeat="0" delay="0" state="0"/>
*/
/*
void ActionSequence::Step::load(XMLNODE* pNode)
{
    _modifiers = readNumericAttrT(pNode, kTBWXMLActModifiers, 0);
    _keyCode = readNumericAttrT(pNode, kTBWXMLActKeyCode, 0);
    _repeat = readNumericAttrT(pNode, kTBWXMLActRepeat, 0);
    _delay = readNumericAttrT(pNode, kTBWXMLActDelay, 0);
    _state = readNumericAttrT(pNode, kTBWXMLActState, 0);
}
*/

void ActionSequence::load(json const& j)
{
    j;
}

void ActionSequence::save(json& j)
{
}

/*
 Sequence actions are a sequence of keystrokes issued linearly with a
 delay between each command. The XML fragment for this looks like:

    <Button4>
        <Action description="34" command="60" name="Office Sequence">
            <Step command="31" modifiers="16"/>
            <Step command="32" argument="65"/>
            <Step command="32" argument="83"/>
            <Step command="32" argument="68"/>
            <Step command="32" argument="70"/>
        </Action>
    </Button4>

*/
/*
void ActionSequence::load(XMLNODE * pNode)
{
    XMLNODE* pStep = pNode->first_node(kTBWXMLStep);
    while (pStep) {
        _steps.push_back(Step(pStep));
        pStep = pStep->next_sibling(kTBWXMLStep);
    }
}
*/

void ActionSequence::take(ITbwDevice* pDevice, bool fDown)
{
}

REGISTER_ACTION_CLASS(ActionKeystroke)

void ActionKeystroke::load(json const& j)
{
    _keyCode = getFromStrValue<unsigned>(j, "keyCode");
    std::string modifiers = getFromStrValueOptional(j, "modifiers", std::string());
    _modifiers = modifiersFromText(modifiers.c_str());
	// syncUp flag
	_syncUp = getFromStrValueOptional(j, "syncUp", false);
}

void ActionKeystroke::save(json& j)
{
    j["modifiers"] = modifiersToText(_modifiers);
    j["keyCode"] = _keyCode;
}

void ActionKeystroke::take(ITbwDevice* pDevice, bool fDown)
{
	BOOST_LOG_TRIVIAL(debug)
		<< "Poke keystroke: modifiers: 0x"
		<< std::hex << _modifiers
		<< ", scancode: 0x" << _keyCode
		<< ", syncUp: " << std::boolalpha << _syncUp;

	if (_syncUp & !fDown) {
		//BOOST_LOG_TRIVIAL(debug) << "\tIgnoring UP as syncUp == true";
		return;
	}

    if (!pDevice->postKeystroke((uint32_t)_modifiers, (uint32_t)_keyCode, fDown, false))
        PlatformServices::getInstance()->postKeystroke(_modifiers, _keyCode, fDown, false);

	if (_syncUp) {
		// send the UP keystroke synchronously.
		BOOST_LOG_TRIVIAL(debug) << "\tpoking UP event synchronously";
		if (!pDevice->postKeystroke((uint32_t)_modifiers, (uint32_t)_keyCode, false, false))
			PlatformServices::getInstance()->postKeystroke(_modifiers, _keyCode, false, false);
	}
}

REGISTER_ACTION_CLASS(ActionOpenItem)

void ActionOpenItem::load(json const& j)
{
    _name = getFromStrValueOptional(j, "name", std::string());
    j.at("path").get_to(_path);
    _type = getFromStrValueOptional(j, "type", 0);
    if (j.find("args") != j.end()) {
        j.at("args").get_to(_args);
    }
}

void ActionOpenItem::save(json& j)
{
    j["name"] = _name;
    j["path"] = _path;
    j["args"] = _args;
    j["type"] = _type;
}

// C++ template to print vector container elements
template <typename T>
std::ostream& operator<<(std::ostream& os, const std::vector<T>& v)
{
    os << "[";
    for (int i = 0; i < v.size(); ++i) {
        os << v[i];
        if (i != v.size() - 1)
            os << ", ";
    }
    os << "]\n";
    return os;
}

void ActionOpenItem::take(ITbwDevice* pDevice, bool fDown)
{
	BOOST_LOG_TRIVIAL(debug)
		<< "Open item action - path: " << _path
		<< ", type: " << _type;

	if (fDown)
    {
        // delegate to platform specific module
        PlatformServices::getInstance()->openResource(_path, _args, _type);
    }
}

REGISTER_ACTION_CLASS(ActionSnippet)

void ActionSnippet::load(json const& j)
{
    _title = getFromStrValueOptional(j, "label", std::string());
	_index = getFromStrValueOptional(j, "index", -1);
	j.at("content").get_to(_content);
}

void ActionSnippet::save(json& j)
{
    j["label"] = _title;
    j["content"] = _content;
	j["index"] = _index;
}

void ActionSnippet::take(ITbwDevice* pDevice, bool fDown)
{
	if (_content.length() == 0 || !fDown) {
		if (_content.length() == 0) {
			BOOST_LOG_TRIVIAL(info)
				<< "Snippet action - content.length == 0";
		}
		return;
	}

    BOOST_LOG_TRIVIAL(debug)
        << "Snippet action - snippet: "
        << _content;

    // TODO: normalize to UTF-8?
    PlatformServices::getInstance()->postString(_content);
}

REGISTER_ACTION_CLASS(ActionSnippetMenu)

void ActionSnippetMenu::load(json const& j)
{
}

void ActionSnippetMenu::save(json& j)
{
}

void ActionSnippetMenu::take(ITbwDevice* pDevice, bool fDown)
{
	if (!fDown)
		return;

	BOOST_LOG_TRIVIAL(debug)
		<< "Snippet menu action";

	// TODO: build vector of snippet strings & their titles
	std::vector<std::string> items, labels;
	auto const& snippets = this->deviceConfig()->getSnippetStrings();
	for (auto const& ss : snippets)
	{
		items.push_back(ss.content);
		labels.push_back(ss.label);
	}

	// 2. request platform services to display a popup menu
	std::string selected = PlatformServices::getInstance()->showPopupMenu(items, labels);

	// 3. Return value from this call is the index of the selected item
	if (selected.length() > 0) { // != -1 && selected < items.size()) {
		// 4. Poke the item
		PlatformServices::getInstance()->postString(selected.c_str());
	}
}

REGISTER_ACTION_CLASS(ActionSingleClick)

void ActionSingleClick::load(json const& j)
{
    j.at("button").get_to(_button);
    std::string modifiers = getFromStrValueOptional(j, "modifiers", std::string());
    _modifiers = modifiersFromText(modifiers.c_str());
}

void ActionSingleClick::save(json& j)
{
    j["button"] = _button;
    j["modifiers"] = modifiersToText(_modifiers);
}

void ActionSingleClick::take(ITbwDevice* pDevice, bool fDown)
{
    BOOST_LOG_TRIVIAL(debug)
        << "Single click action - button: " << _button
        << ", modifiers: " << _modifiers;

	// Single click events without modifiers are handled in the driver.
	// By right these events should not reach user mode. In case they do
	// (#147 [Win] Resuming from sleep, the left button of the trackball becomes to right button.),
	// ignore them.
	if (_modifiers == 0) {
		BOOST_LOG_TRIVIAL(debug) << "\tmodifiers == 0, ignoring";
		return;
	}

    // Modifier keys for button down event should be posted BEFORE the
    // pointer event.
    if (_modifiers && fDown) {
        if (!pDevice->postKeystroke(_modifiers, 0, fDown, true))
            PlatformServices::getInstance()->postKeystroke(_modifiers, 0, fDown, true);
    }

	uint32_t buttonMask = 0x01 << (_button - 1);
	if (!pDevice->postPointerClick(buttonMask, fDown))
        PlatformServices::getInstance()->postPointerClick(buttonMask, fDown);

    // modifier keys for button up event should be posted AFTER the
    // pointer event.
    if (_modifiers && !fDown) {
        if (!pDevice->postKeystroke(_modifiers, 0, fDown, true))
            PlatformServices::getInstance()->postKeystroke(_modifiers, 0, fDown, true);
    }
}

REGISTER_ACTION_CLASS(ActionDrag)

void ActionDrag::load(json const& j)
{
    j.at("button").get_to(_button);
    std::string modifiers = getFromStrValueOptional(j, "modifiers", std::string());
    _modifiers = modifiersFromText(modifiers.c_str());
}

void ActionDrag::save(json& j)
{
    j["button"] = _button;
}

void ActionDrag::take(ITbwDevice* pDevice, bool fDown)
{
	BOOST_LOG_TRIVIAL(debug)
		<< "Drag action - button: " << _button;

	static bool btoogle = true;

	// only send button down event, which triggers a begin-drag op
	if (fDown) {
		if (_modifiers) {
			// Modifier keys down
			if (!pDevice->postKeystroke(_modifiers, 0, btoogle, true))
				PlatformServices::getInstance()->postKeystroke(_modifiers, 0, btoogle, true);
		}

		uint32_t buttonMask = 0x01 << (_button - 1);
		if (!pDevice->postPointerClick(buttonMask, btoogle))
			PlatformServices::getInstance()->postPointerClick(buttonMask, btoogle);

		btoogle = !btoogle;

	}
}

REGISTER_ACTION_CLASS(ActionMultiClick)

void ActionMultiClick::load(json const& j)
{
    j.at("button").get_to(_button);
    j.at("clicks").get_to(_nClicks);
    std::string modifiers = getFromStrValueOptional(j, "modifiers", std::string());
    _modifiers = modifiersFromText(modifiers.c_str());
}

void ActionMultiClick::save(json& j)
{
    j["button"] = _button;
    j["clicks"] = _nClicks;
    j["modifiers"] = modifiersToText(_modifiers);
}

void ActionMultiClick::take(ITbwDevice* pDevice, bool fDown)
{
	BOOST_LOG_TRIVIAL(debug)
		<< "Multi click - button: " << _button
		<< ", clicks: " << _nClicks
		<< ", modifiers: " << _modifiers;

	if (fDown) {
#if 0
        doClick();
#else
        /*
            _nClicks = 2

            i = 0, i % 2 = 0
                DOWN
            i = 1, i % 2 = 1
                UP
            i = 2, i % 2 == 0
                DOWN
            i = 3, i % 2 == 1
                UP
        */
        if (_modifiers) {
            // Modifier keys down
            if (!pDevice->postKeystroke(_modifiers, 0, true, true))
                PlatformServices::getInstance()->postKeystroke(_modifiers, 0, true, true);
        }
        uint32_t buttonMask = 0x01 << (_button-1);
        for (size_t i = 0; i < _nClicks*2 - 1; i++)
        {
            if (!pDevice->postPointerClick(buttonMask, (i % 2) == 0))
                PlatformServices::getInstance()->postPointerClick(
                    buttonMask, (i % 2) == 0);
        }
        //if (_modifiers) {
        //    // Modifier keys up
        //    if (!pDevice->postKeystroke(_modifiers, 0, false, true))
        //        PlatformServices::getInstance()->postKeystroke(_modifiers, 0, false, true);
        //}
#endif
    }
	else
	{
		uint32_t buttonMask = 0x01 << (_button - 1);
		if (!pDevice->postPointerClick(buttonMask, false))
			PlatformServices::getInstance()->postPointerClick(
				buttonMask, false);
		if (_modifiers) {
			// Modifier keys up
			if (!pDevice->postKeystroke(_modifiers, 0, false, true))
				PlatformServices::getInstance()->postKeystroke(_modifiers, 0, false, true);
		}
	}
}

void ActionMultiClick::doClick()
{
#if 0
    BOOST_LOG_TRIVIAL(debug)
        << "MutliClick - clicks: " << _nClicks
        << ", clickCounter: " << _clickCounter
        << ", timerId: " << _timerId
        << ", clock tick: " << ::GetTickCount();

    bool fDown = _clickCounter % 2 == 0;
    PlatformServices::getInstance()->postPointerClick(_button, fDown);
    _clickCounter++;
    auto mgr = DeviceManager::getInstance();
    if (_clickCounter < _nClicks*2)
    {
        if (_timerId == 0)
        {
            _timerId = mgr->AddTimer(
                MULTI_CLICK_INTER_CLICK_INTERVAL,
                true,
                std::bind(&ActionMultiClick::doClick, this));
        }
    }
    else
    {
        if (_timerId != 0)
        {
            mgr->RemoveTimer(_timerId);
            _timerId = 0;
            _clickCounter = 0;
        }
    }
#endif

}

REGISTER_ACTION_CLASS(ActionUseGlobal)

void ActionUseGlobal::take(ITbwDevice* pDevice, bool fDown)
{
}

REGISTER_ACTION_CLASS(ActionSystem)

void ActionSystem::take(ITbwDevice *pDevice, bool fDown)
{
	BOOST_LOG_TRIVIAL(debug)
		<< "System action - action: " << _action;

    if (!fDown)
        return;

    PlatformServices::getInstance()->system(_action, _args);
}

void ActionSystem::load(json const& j)
{
    j.at("action").get_to(_action);
    if (j.find("params") != j.end())
        _args = j["params"];
    /*
    for (json::const_iterator it = _args.begin(); it != _args.end(); ++it)
    {
        BOOST_LOG_TRIVIAL(debug) << it.key() << " : " << it.value();
    }
    */
}

void ActionSystem::save(json& j)
{
    j["action"] = _action;
    j["params"] = _args;
}

REGISTER_ACTION_CLASS(ActionScroll)

void ActionScroll::load(json const& j)
{
    std::string direction;
    j.at("direction").get_to(direction);
    direction = tolower(direction);
    if (direction == "left")
        _direction = SD_LEFT;
    else if (direction == "right")
        _direction = SD_RIGHT;
    else if (direction == "up")
        _direction = SD_UP;
    else if (direction == "down")
        _direction = SD_DOWN;
	else if (direction == "trackball")
		_direction = SD_TRACKBALL;
	else if (direction == "trackauto")
		_direction = SD_TRACKBALL_AUTO;

	_lines = getFromStrValueOptional(j, "lines", 1);
    _lines = std::min(std::max(_lines, 1), 100);
}

void ActionScroll::save(json& j)
{
    if (_direction == SD_LEFT)
        j["direction"] = "left";
    else if (_direction == SD_RIGHT)
        j["direction"] = "right";
    else if (_direction == SD_UP)
        j["direction"] = "up";
    else if (_direction == SD_DOWN)
        j["direction"] = "down";
	else if (_direction == SD_TRACKBALL)
		j["direction"] = "trackball";
	else if (_direction == SD_TRACKBALL_AUTO)
		j["direction"] = "trackauto";
	else
        j["direction"] = "unknown";
    j["lines"] = _lines;
}

void ActionScroll::take(ITbwDevice* pDevice, bool fDown)
{
    if (fDown) {
        //BOOST_LOG_TRIVIAL(info) << "ActionScroll::take - direction: " << _direction << ", lines: " << _lines;
        if (!pDevice->postScrollNavigation(_direction, _lines))
			PlatformServices::getInstance()->postScrollNavigation(_direction, _lines);
	}
}

class MissingActionMacrosDataFile : public std::exception {
public:
    virtual const char* what() const noexcept
    { return "Missing action macros data file"; }
};

class ActionFactoryInitError : public std::exception {
public:
    virtual const char* what() const noexcept
    { return "Error initializing action factory"; }
};

ActionFactory::ActionFactory()
    : _macros()
{
//#ifdef DEBUG
//    // This allows running the program from anywhere.
//    boost::filesystem::path dataFile("/tmp/tbwactions.json");
//#else
    // In production, tbwaction.json has to be in the same location
    // as tbwhelper binary.
    boost::filesystem::path dataFile(getModuleFileFullname());
#ifdef _WINDOWS
    dataFile.remove_filename().append("tbwactions.json");
#else
    dataFile.remove_filename().append("../Resources/tbwactions.json");
#endif
    //BOOST_LOG_TRIVIAL(debug) << "Actions data file: " << dataFile;
//#endif
    if (!boost::filesystem::exists(dataFile))
        throw MissingActionMacrosDataFile();

    if (!loadActions(toUTF8(dataFile.native())))
        throw ActionFactoryInitError(); // grave error, should stop program from running!
}

ActionFactory::~ActionFactory()
{
}

ACTIONPTR ActionFactory::createActionHandler(DeviceConfig* dc, json const& j)
{
    ACTIONPTR action;

    //BOOST_LOG_TRIVIAL(debug) << "createAction from: " << j << "\n";
    std::string command;
    j.at("command").get_to(command);
    json params = {};
    auto paramsIt = j.find("params");
    if (paramsIt != j.end())
        params = j["params"];

    try
    {
        auto it = _macros.find(command);
		//BOOST_LOG_TRIVIAL(info) << "it->second: " << it->second << "\n";
        if (it != _macros.end())
        {
			params = merge(it->second, params);
            //BOOST_LOG_TRIVIAL(debug) << "merged params: " << params << "\n";
            action = ActionFactory::createActionHandlerFromRaw(dc, params);
        }
    }
    catch (const std::exception&)
    {
    }

    if (!action)
    {
        try
        {
            action = ActionFactory::createActionHandlerFromRaw(dc, j);
        }
        catch (const std::exception&)
        {
        }
    }

    if (!action)
    {
        BOOST_LOG_TRIVIAL(error) << "Error creating action handler for command: " << command
            << ", args: " << params;
        throw std::runtime_error("Error creating action handler!");
    }
    return action;
}

bool ActionFactory::loadActions(std::string const& path)
{
    try
    {
        std::ifstream fin(path);
        json jdefmacros;
        fin >> jdefmacros;
        for (json::const_iterator it = jdefmacros.begin(); it != jdefmacros.end(); ++it)
        {
            //BOOST_LOG_TRIVIAL(debug) << it.key() << " : " << it.value();
            _macros[it.key()] = it.value();
        }
        return true;
    }
    catch (const std::exception& e)
    {
        BOOST_LOG_TRIVIAL(error) << "Error loading action macros definition file: " << e.what();
    }
    return false;
}

// can throw std::exception!
ACTIONPTR ActionFactory::createActionHandlerFromRaw(DeviceConfig* deviceConfig, json const& j)
{
	std::string command; j.at("command").get_to(command);
    ACTIONPTR action = getActionHandlerFromName(command);
    action->load(j);
	action->_deviceCfg = deviceConfig;

	// special handling for snippets
	if (deviceConfig
		&& deviceConfig->applicationConfig()
		&& deviceConfig->applicationConfig()->settings()
		&& command == "snippet")
	{
		ActionSnippet* pSnippet = dynamic_cast<ActionSnippet*>(action.operator->());
		if (pSnippet) {
			deviceConfig->addSnippetString(pSnippet->_content, pSnippet->_title, pSnippet->_index);
		}
	}

    return action;
}

//unsigned DeviceConfig::s_count = 0;

DeviceConfig::DeviceConfig()
	: _ac(nullptr)
	, _id(0)
	, _pointerSpeed(PlatformServices::getInstance()->getCurPointerSpeed()) // 10 in old scale
	, _pointerAcceleration(PlatformServices::getInstance()->getCurPointerAccelerationRate() > 0)
	, _pointerAccelerationRate(PlatformServices::getInstance()->getCurPointerAccelerationRate())   // 0 acceleration
	, _snapModifiers(0)
	, _slowModifiers(0)
	, _slowDivider(0)
	, _inertialScroll(false)
	, _scrollInvert(PlatformServices::getInstance()->getScrollInvert())
	, _scrollLines(PlatformServices::getInstance()->getCurScrollSpeed())
	, _inertialScrollTimer(0)
	, _inertialScrollFactor(0)
	, _inertialScrollNullSteps(0)
	, _actions()
	, _chordsCombinedMask(0)
{
	//s_count++;
	//BOOST_LOG_TRIVIAL(debug) << "DeviceConfig(def ctor): 0x" << this << ", class count: " << s_count;
	_snippets.resize(MAX_SNIPPETS_COUNT);
}

DeviceConfig::DeviceConfig(ApplicationConfig* ac)
	: _ac(ac)
	, _id(0)
    , _pointerSpeed(PlatformServices::getInstance()->getCurPointerSpeed()) // 10 in old scale
    , _pointerAcceleration(PlatformServices::getInstance()->getCurPointerAccelerationRate() > 0)
    , _pointerAccelerationRate(PlatformServices::getInstance()->getCurPointerAccelerationRate())   // 0 acceleration
	, _snapModifiers(0)
	, _slowModifiers(0)
	, _slowDivider(0)
	, _inertialScroll(false)
    , _scrollInvert(PlatformServices::getInstance()->getScrollInvert())
    , _scrollLines(PlatformServices::getInstance()->getCurScrollSpeed())
	, _inertialScrollTimer(0)
	, _inertialScrollFactor(0)
	, _inertialScrollNullSteps(0)
	, _commands()
	, _actions()
	, _chordsCombinedMask(0)
{
	//s_count++;
	//BOOST_LOG_TRIVIAL(debug) << "DeviceConfig: 0x" << this << ", class count: " << s_count;
	_snippets.resize(MAX_SNIPPETS_COUNT);
	loadDefaultActions();
}

DeviceConfig::DeviceConfig(const DeviceConfig& rhs)
	: _ac(rhs._ac)
	, _id(rhs._id)
	, _pointerSpeed(rhs._pointerSpeed) // 10 in old scale
	, _pointerAcceleration(rhs._pointerAcceleration)
	, _pointerAccelerationRate(rhs._pointerAccelerationRate)   // 0 acceleration
	, _snapModifiers(rhs._snapModifiers)
	, _slowModifiers(rhs._slowModifiers)
	, _slowDivider(rhs._slowDivider)
	, _inertialScroll(rhs._inertialScroll)
	, _scrollInvert(rhs._scrollInvert)
	, _scrollLines(rhs._scrollLines)
	, _inertialScrollTimer(rhs._inertialScrollTimer)
	, _inertialScrollFactor(rhs._inertialScrollFactor)
	, _inertialScrollNullSteps(rhs._inertialScrollNullSteps)
	, _commands(rhs._commands)
	, _actions()
	, _chordsCombinedMask(rhs._chordsCombinedMask)
	, _snippets(rhs._snippets)
{
	//s_count++;
	//BOOST_LOG_TRIVIAL(debug) << "DeviceConfig(copy ctor): 0x" << this;
	buildActionHandlersFromCommands();
	//&*&*&*G1_ADD
	memcpy(_chordMasks, rhs._chordMasks, 16 * sizeof(uint32_t));
	//&*&*&*G2_ADD

}

DeviceConfig::~DeviceConfig()
{
	//s_count--;
	//BOOST_LOG_TRIVIAL(debug) << "~DeviceConfig: 0x" << this << ", class count: " << s_count;
}

void DeviceConfig::loadDefaultActions()
{
    // load default actions for Left Click & Right Click
    _commands[0x01] = json({
        { "command", "mouse_leftClick" },
        { "params", {} }
    }).dump();

    _commands[0x02] = json({
        { "command", "mouse_rightClick" },
        { "params", {} }
    }).dump();

	_commands[0x04] = json({
		{ "command", "mouse_middleClick" },
		{ "params", {} }
	}).dump();

	_commands[0x08] = json({
		{ "command", "mouse_button4Click" },
		{ "params", {} }
	}).dump();

	_commands[0x10] = json({
		{ "command", "mouse_button5Click" },
		{ "params", {} }
	}).dump();

    _commands[0x40000000] = json({
        { "command", "navigation_scrollLeft" },
        { "params", {} }
        }).dump();

    _commands[0x80000000] = json({
        { "command", "navigation_scrollRight" },
        { "params", {} }
        }).dump();

    buildActionHandlersFromCommands();
}

void DeviceConfig::buildActionHandlersFromCommands()
{
    // convert commands into Action
    _actions.clear();
    ActionFactory af;
    for (auto it = _commands.begin(); it != _commands.end(); it++)
    {
        try
        {
            _actions[it->first] = af.createActionHandler(
				this,
				json::parse(it->second));
        }
        catch (const std::exception&)
        {
            BOOST_LOG_TRIVIAL(error) << "Error converting '" << it->first
                << "' into action object";
        }
    }
}

void DeviceConfig::handleButtonActivity(ITbwDevice* pDevice, uint32_t ulDownButtons, uint32_t ulUpButtons)
{
    BOOST_LOG_TRIVIAL(debug)
        << "Button event: DOWN: " << std::hex << ulDownButtons
        << " UP: " << std::hex << ulUpButtons;

    if (ulDownButtons)
    {
        auto it = _actions.find(ulDownButtons);
        if (it != _actions.end())
        {
            it->second->take(pDevice, true);
        }
        else
        {
            BOOST_LOG_TRIVIAL(debug)
                << "Button down event: no actions found for the buttons";
        }
    }

    if (ulUpButtons)
    {
        // If there're still any up events left, handle them
        auto it = _actions.find(ulUpButtons);
        if (it != _actions.end())
        {
            it->second->take(pDevice, false);
        }
        else
        {
            BOOST_LOG_TRIVIAL(debug)
                << "Button up event: no actions found for the buttons";
        }
    }
}

/*
ACTIONPTR DeviceConfig::getButtonAction(unsigned long buttonMask)
{
    if (_actions.find(buttonMask) != _actions.end()) {
        return _actions[buttonMask];
    }
    return std::make_shared<ActionNone>();
}

void DeviceConfig::setButtonAction(unsigned long buttonMask, ACTIONPTR const & action)
{
    _actions[buttonMask] = action;
    if (numberOfSetBits(buttonMask) > 1)
    {
        computeChordsCombinedMask();
    }
}
*/

json DeviceConfig::getButtonAction2(uint32_t mask)
{
    json j;
    if (mask)   // Get command for this button
    {
        auto it = _commands.find(mask);
        if (it != _commands.end())
        {
            j = json::parse(it->second);
        }
    }
    else // Get all commands for this device
    {
        for (auto it : _commands)
        {
            std::stringstream ss;
            ss << it.first;
            // Commands are stored as stringified JSON. So we need to convert
            // it to JSON so that it can be stored in the JSON object that we
            // return.
            j[ss.str()] = json::parse(it.second);
        }
    }
    return j;
}

void DeviceConfig::setButtonAction2(uint32_t mask, json const& j)
{
    ActionFactory af;
    if (mask)
    {
        _actions[mask] = af.createActionHandler(this, j);
        _commands[mask] = j.dump();
        if (numberOfSetBits(mask) > 1)
        {
            computeChordsCombinedMask();
        }
    }
    else
    {
        // json is an array of button settings to set multiple buttons
        for (auto it=j.begin(); it!=j.end(); it++)
        {
            uint32_t mask = to<uint32_t>(it.key());
            auto& jAction = it.value();
            _actions[mask] = af.createActionHandler(this, jAction);
            _commands[mask] = jAction.dump();
        }
    }
}

void DeviceConfig::computeChordsCombinedMask()
{
    _chordsCombinedMask = 0;
	//&*&*&*G1_ADD
	int i;
	memset(_chordMasks, 0, 16 * sizeof(uint32_t));
	i = 0;
	//&*&*&*G2_ADD
    for (auto& action : _actions)
    {
        if (numberOfSetBits(action.first) > 1 && action.second->name() != "none")
        {
            _chordsCombinedMask |= action.first;
			//&*&*&*G1_ADD
			_chordMasks[i] = action.first;
			BOOST_LOG_TRIVIAL(debug) << "_chordMasks[" << i << "] = " << _chordMasks[i];
			i++;
			//&*&*&*G2_ADD
        }
    }
}

void DeviceConfig::init(ApplicationConfig * ac)
{
    _ac = ac;
	for (auto action : _actions)
		action.second->_deviceCfg = this;
}

std::string DeviceConfig::appName() const
{
    return _ac->name();
}

Settings* DeviceConfig::settings() const
{
    return _ac->settings();
}

/*
void DeviceConfig::activate()
{
    if (!_active)
    {
        // set mouse speed, acceleration, scroll lines
        _active = true;
    }
}

void DeviceConfig::deactivate()
{
    _active = false;
}

bool DeviceConfig::isActive()
{
    return _active;
}
*/

uint32_t DeviceConfig::pointerSpeed()
{
    return _pointerSpeed;
}

void DeviceConfig::pointerSpeed(uint32_t speed)
{
    _pointerSpeed = speed;
    /*
    if (isActive())
    {
        // TODO: update SytemParametersInfo
        PlatformServices::getInstance()->configurePointer(
            _pointerSpeed,
            _pointerAccelerationRate,
            _scrollLines);
    }
    */
}

bool DeviceConfig::pointerAcceleration()
{
    return _pointerAcceleration;
}

void DeviceConfig::pointerAcceleration(bool enable)
{
    _pointerAcceleration = enable;
}

uint32_t DeviceConfig::pointerAccelerationRate()
{
    return (uint32_t)_pointerAccelerationRate;
}

void DeviceConfig::pointerAccelerationRate(uint32_t rate)
{
    if (rate != _pointerAccelerationRate) {
        _pointerAccelerationRate = rate;
        /*
        if (isActive())
        {
            // TODO: update SytemParametersInfo
            PlatformServices::getInstance()->configurePointer(
                _pointerSpeed,
                _pointerAccelerationRate,
                _scrollLines);
        }
        */
    }
}

bool DeviceConfig::slowPointer()
{
    return _slowModifiers != 0;
}

std::string DeviceConfig::slowPointerModifiers()
{
    return modifiersToText(_slowModifiers);
}

void DeviceConfig::slowPointerModifiers(const char* modifiers)
{
    _slowModifiers = modifiersFromText(modifiers);
}

bool DeviceConfig::lockedAxis()
{
    return _snapModifiers != 0;
}

std::string DeviceConfig::lockedAxisModifiers()
{
    return modifiersToText(_snapModifiers);
}

void DeviceConfig::lockedAxisModifiers(const char* modifiers)
{
    _snapModifiers = modifiersFromText(modifiers);
}

bool DeviceConfig::inertialScroll()
{
    return _inertialScroll;
}

void DeviceConfig::inertialScroll(bool fEnable)
{
    if (_inertialScroll != fEnable)
    {
        _inertialScroll = fEnable;
        /*
        auto devices = DeviceManager::getInstance()->devices(_id);
        for (auto& device : devices)
        {
            try
            {
                device->setVertScrollParams(_inertialScroll, _scrollInvert);
            }
            catch (...)
            {
            }
        }
        */
    }
}

uint32_t DeviceConfig::scrollSpeed()
{
    return _scrollLines;
}

void DeviceConfig::scrollSpeed(uint32_t lines)
{
    if (lines != _scrollLines)
    {
        _scrollLines = lines < 1 ? 1 : lines;
        /*
        PlatformServices::getInstance()->configurePointer(
            _pointerSpeed,
            _pointerAccelerationRate,
            _scrollLines);
        */
    }
}

bool DeviceConfig::invertScroll()
{
    return _scrollInvert;
}

void DeviceConfig::invertScroll(bool fEnable)
{
    if (_scrollInvert != fEnable)
    {
        _scrollInvert = fEnable;
        /*
        auto devices = DeviceManager::getInstance()->devices(_id);
        for (auto& device : devices)
        {
            try
            {
                device->setVertScrollParams(_scrollInvert, _scrollInvert);
            }
            catch (...)
            {

            }
        }
        */
    }
}

bool DeviceConfig::operator==(const DeviceConfig & rhs) const
{
    return _id == rhs._id
        && _pointerSpeed == rhs._pointerSpeed
        && _pointerAccelerationRate == rhs._pointerAccelerationRate
        && _snapModifiers == rhs._snapModifiers
        && _slowModifiers == rhs._slowModifiers
        && _slowDivider == rhs._slowDivider
        && _inertialScroll == rhs._inertialScroll
        && _scrollInvert == rhs._scrollInvert
        && _scrollLines == rhs._scrollLines
        && _inertialScrollTimer == rhs._inertialScrollTimer
        && _inertialScrollFactor == rhs._inertialScrollFactor
        && _inertialScrollNullSteps == rhs._inertialScrollNullSteps;

        // TODO: implement ACTIONPTR comparator and use it to compare
        // _action with rhs._actions
}

void DeviceConfig::addCommand(uint32_t buttonMask, json const & json)
{
    _commands[buttonMask] = json.dump();
}


void DeviceConfig::addSnippetString(std::string const & snippet, std::string const& title, int index)
{
	//if (_snippets.size() >= MAX_SNIPPETS_COUNT) {
	//	// remove oldest
	//	_snippets.erase(_snippets.begin()+_snippets.size()-1);
	//}
	// _snippets.push_back(SnippetString(snippet, title));
	// keep it in reverse chornological order (see operator<())
	//std::sort(_snippets.begin(), _snippets.end());
	if (index >= 0 && index < MAX_SNIPPETS_COUNT) {
		_snippets[index] = SnippetString(snippet, title);
	}
}

std::vector<DeviceConfig::SnippetString> const& DeviceConfig::getSnippetStrings() const
{
	return _snippets;
}

/**
 * Creates a copy of DeviceConfig, while copying button mappings with pure
 * single click actions (that is single click actions without modifiers).
 */
DeviceConfig DeviceConfig::createCopyingCoreButtonClicks(const DeviceConfig & dc)
{
	DeviceConfig ret(dc);
	ret._ac = dc._ac;
	ret._snippets.clear();
	ret._snippets.resize(MAX_SNIPPETS_COUNT);

	auto& commands = ret._commands;
	ActionFactory af;
	for (auto it = commands.begin(); it != commands.end(); it++) {
		auto j = json::parse(it->second);
		ACTIONPTR action = af.createActionHandler(nullptr, j);
		ActionSingleClick *pAction = dynamic_cast<ActionSingleClick*>(action.operator->());
		if (!pAction || pAction->_modifiers != 0) {
			commands.erase(it);
			it = commands.begin();
			continue;
		}
	}
	ret.buildActionHandlersFromCommands();

	return ret;
}

void to_json(nlohmann::json& j, DeviceConfig::SnippetString const& ss)
{
	j["label"] = ss.label;
	j["content"] = ss.content;
	//j["savedAt"] = ss.savedAt;
}

void from_json(nlohmann::json const& j, DeviceConfig::SnippetString& ss)
{
	ss.label = getFromStrValueOptional(j, "label", std::string());
	j.at("content").get_to(ss.content);
	//j.at("savedAt").get_to(ss.savedAt);
}

// function is deliberately named with uppercase JSON to avoid ambiguity between
// this & the to_json(std::shared_ptr<>) in json.hpp.
void to_json(json& j, ACTIONPTR const& action)
{
    j["command"] = action->_commandName;
    action->save(j);
}

void from_json(json const & j, ACTIONPTR& action)
{
    action = ActionFactory::createActionHandlerFromRaw(nullptr, j);
}

void to_json(nlohmann::json & j, const DeviceConfig & dc)
{
    j = json{
        {"id", dc._id },
        { "speed", dc._pointerSpeed },
		{ "snippets", dc._snippets },
        { "acceleration", dc._pointerAcceleration },
        { "accelerationRate", dc._pointerAccelerationRate },
        { "slowModifiers", modifiersToText(dc._slowModifiers) },
        { "slowDivider", dc._slowDivider },
        { "snapModifiers", modifiersToText(dc._snapModifiers) },
        { "inertialScroll", dc._inertialScroll },
        { "scrollInvert", dc._scrollInvert },
        { "scrollLines", dc._scrollLines },
        { "inertialScrollTimer", dc._inertialScrollTimer },
        { "inertialScrollFactor", dc._inertialScrollFactor },
        { "inertialScrollNullSteps", dc._inertialScrollNullSteps },
    };

    // Since we store the button's commands as stringified JSON, we first
    // convert it into JSON and store into a temporary table which is what
    // we save to the file. This ensures that the JSON file stored looks
    // like a real JSON file rather than the stringified version.
    // In from_json() we do the reverse.
    std::map<uint32_t, json> temp;
    for (auto it : dc._commands)
        temp.insert(std::make_pair(it.first, json::parse(it.second)));
    j["commands"] = temp;
}

void from_json(nlohmann::json const & j, DeviceConfig & dc)
{
    j.at("id").get_to(dc._id);
    j.at("speed").get_to(dc._pointerSpeed);
    j.at("acceleration").get_to(dc._pointerAcceleration);
    j.at("accelerationRate").get_to(dc._pointerAccelerationRate);
    std::string modifiers;
    j.at("slowModifiers").get_to(modifiers);
    dc._slowModifiers = modifiersFromText(modifiers.c_str());
    j.at("slowDivider").get_to(dc._slowDivider);
    modifiers = "";
    j.at("snapModifiers").get_to(modifiers);
    dc._snapModifiers = modifiersFromText(modifiers.c_str());
    j.at("inertialScroll").get_to(dc._inertialScroll);
    j.at("scrollInvert").get_to(dc._scrollInvert);
    j.at("scrollLines").get_to(dc._scrollLines);
    j.at("inertialScrollTimer").get_to(dc._inertialScrollTimer);
    j.at("inertialScrollFactor").get_to(dc._inertialScrollFactor);
    j.at("inertialScrollNullSteps").get_to(dc._inertialScrollNullSteps);

    // normaize values so that they fall within the min-max range
    dc._scrollLines = dc._scrollLines < 1 ? 1 : dc._scrollLines;    // scrollLines

    // Json deserializing for std::map uses map.emplace() method which
    // inserts a new element only if it doesn't already exist. Since
    // DeviceConfig will have the default left-click & right-click action
    // handlers placed for buttons 1 & 2, any remapping of these keys by the
    // user will never get properly deserialized.
    //
    // We solve this by clearing the action map before deserializing the map.

    // Serialize into a temporary table and then convert into stringified
    // JSON. See to_json() above for explanation.
    std::map<uint32_t, json> temp;
    j.at("commands").get_to(temp);
    dc._commands.clear();
    for (auto it : temp)
        dc._commands.insert(std::make_pair(it.first, it.second.dump()));
    dc.buildActionHandlersFromCommands();

	// load snippet strings
	if (j.find("snippets") != j.end()) {
		dc._snippets.clear();
		j.at("snippets").get_to(dc._snippets);
		dc._snippets.resize(MAX_SNIPPETS_COUNT);
		//std::sort(dc._snippets.begin(), dc._snippets.end());
	}

    // calculate _chordsCombinedMask
    dc.computeChordsCombinedMask();
}

//unsigned ApplicationConfig::s_count = 0;

ApplicationConfig::ApplicationConfig()
	: _settings(nullptr)
	, _appFileName()
	, _appName()
	, _config(this)
{
	//s_count++;
	//BOOST_LOG_TRIVIAL(debug) << "ApplicationConfig(def ctor): 0x" << this << ", count: " << s_count;
}

ApplicationConfig::ApplicationConfig(Settings* s, const char* name)
	: _settings(s)
	, _appFileName()
	, _appName(name)
	, _config(this)
{
	assert(name != nullptr);
	//s_count++;
	//BOOST_LOG_TRIVIAL(debug) << "ApplicationConfig: 0x" << this << ", count: " << s_count;
}

ApplicationConfig::ApplicationConfig(const ApplicationConfig& rhs)
	: _settings(rhs._settings)
	, _appFileName(rhs._appFileName)
	, _appName(rhs._appName)
	, _config(rhs._config)
{
	_config.init(this);
	//s_count++;
	//BOOST_LOG_TRIVIAL(debug) << "ApplicationConfig(copy ctor): 0x" << this << ", count: " << s_count;
}


ApplicationConfig::~ApplicationConfig()
{
	//s_count--;
	//BOOST_LOG_TRIVIAL(debug) << "~ApplicationConfig: 0x" << this << ", count: " << s_count;
}


void ApplicationConfig::init(Settings * s)
{
    _settings = s;
#ifdef DEVICEAPPS
	_config.init(this);
#else
    for (auto& device : _devices) {
        device.second.init(this);
    }
#endif
}

// Assumes that Settings mutex has been acquired
bool ApplicationConfig::deviceConfigExists(unsigned deviceId)
{
    assert(_settings != nullptr);
#ifdef DEVICEAPPS
	return true;
#else
    return _devices.find(deviceId) != _devices.end();
#endif
}

// Assumes that Settings mutex has been acquired
void ApplicationConfig::createDefaultDeviceConfigIfDoesntExist(unsigned deviceId)
{
    assert(_settings != nullptr);
#ifdef DEVICEAPPS
	_config._ac = this;
#else
    if (_devices.find(deviceId) == _devices.end())
    {
        // _devices[deviceId] =
        DeviceConfig dc(this);
        _devices[deviceId] = std::move(dc);
    }
#endif
}

void ApplicationConfig::addDeviceConfig(DeviceConfig&& dc)
{
#ifndef DEVICEAPPS
    _devices[dc._id] = std::move(dc);
#endif
}

void to_json(nlohmann::json & j, const ApplicationConfig & appConfig)
{
    j["name"] = appConfig._appName;
    j["fileName"] = appConfig._appFileName;
#ifdef DEVICEAPPS
	j["config"] = appConfig._config;
#else
	j["devices"] = appConfig._devices;
#endif
}

void from_json(const nlohmann::json & j, ApplicationConfig & ac)
{
    j.at("name").get_to(ac._appName);
    j.at("fileName").get_to(ac._appFileName);
#ifdef DEVICEAPPS
	j.at("config").get_to(ac._config);
	ac._config.init(&ac);
#else
    j.at("devices").get_to(ac._devices);
	for (auto& device : ac._devices) {
		device.second.init(&ac);
	}
#endif
}
Settings::Settings(bool fLoadFromFile/*=false*/)
#ifndef DEVICEAPPS
	: _apps()
#else
	: _devices()
#endif
#ifdef _WINDOWS
    , _platform("windows")
#else
    , _platform("darwin")
#endif
    , _timestamp(0)
    , _versionMajor(2)
    , _versionMinor(0)
    , _trayIcon(true)
{
	if (fLoadFromFile)
	{
		try
		{
			createFolderIfDoesNotExist();
			if (boost::filesystem::exists(Settings::getFileFullPath())) // it's okay
			{
				// *&*&*&v1_20200417 Windows doesn't fully support standard string 
#ifdef _WINDOWS
				loadW(Settings::getFileFullPathW().c_str());
#else
				load(Settings::getFileFullPath().c_str()); // failed on windows if there is non-English path
#endif
			}
			else
			{
				BOOST_LOG_TRIVIAL(info) << "Settings file not found, creating empty config";
				createDefault();
			}
		}
		catch (json::exception)
		{
			BOOST_LOG_TRIVIAL(warning) << "Error loading settings, creating empty config";
			createDefault();
		}
		catch (...)
		{
			throw;
		}
	}
}

Settings::~Settings()
{
}

// Creates the folder - Kensington/TrackballWorks -- if it does not exist.
// This is where TbwSettings.xml is expected to be stored.
void Settings::createFolderIfDoesNotExist()
{
    auto folder = getFileFolder();
    if (!boost::filesystem::exists(folder.c_str()))
    {
        if (!boost::filesystem::create_directories(folder.c_str()))
            throw std::runtime_error("Error creating settings folder");
    }
}

std::string Settings::getFileFolder()
{
    return PlatformServices::getInstance()->getPreferencesFolder();
}

std::string Settings::getLegacyFileFullPath()
{
    boost::filesystem::path loc(Settings::getFileFolder());
    loc.append(Settings::getLegacyFilename());
    return toUTF8(loc.c_str());
}

std::string Settings::getLegacyFilename()
{
#ifdef _WINDOWS
    return "TbwSettings.xml";
#else
    return "com.kensington.trackballworks.settings.plist";
#endif
}

//----------------------------------------------------------------------//
//std::string wideToMultiByte(std::wstring const & wideString)
//{
//    std::string ret;
//    std::string buff(MB_CUR_MAX, '\0');
//
//    for (wchar_t const & wc : wideString)
//    {
//        int mbCharLen = std::wctomb(&buff[0], wc);
//
//        if (mbCharLen < 1) { break; }
//
//        for (int i = 0; i < mbCharLen; ++i)
//        {
//            ret += buff[i];
//        }
//    }
//
//    return ret;
//}
//----------------------------------------------------------------------//

//inline std::string toUTF8(const std::wstring& ws)
//{
//    return lcv::utf_to_utf<char>(ws);
//}

std::string Settings::getFileFullPath()
{
    namespace fs = boost::filesystem;
    auto filePath = fs::path(Settings::getFileFolder());
    filePath.append(Settings::getFilename());
    return toUTF8(filePath.c_str());
}
#ifdef _WINDOWS
std::wstring Settings::getFileFullPathW()
{
    namespace fs = boost::filesystem;
    auto filePath = fs::path(Settings::getFileFolder());
    filePath.append(Settings::getFilenameW());
    return filePath.c_str();
}
#endif
std::string Settings::getFilename()
{
    return "TbwSettings.json";
}
#ifdef _WINDOWS
std::wstring Settings::getFilenameW()
{
    return L"TbwSettings.json";
}
#endif

// Typically called from the constructor when loading results
// in an exception.
// Since it's called from the ctor we don't acquire the mutex, as other threads
// should not access the Settings methods before it's fully constructed.
void Settings::createDefault()
{
#ifndef DEVICEAPPS
    // create "*" application config
    _apps.clear();
    _apps.insert(std::make_pair("*", ApplicationConfig(this, "*")));
#endif
#ifdef _WINDOWS
	saveImplw();
#else
    saveImpl();
#endif
}

void Settings::createDefaultDeviceConfigIfDoesntExist(unsigned deviceId)
{
    bool saveReqd = false;
#ifdef DEVICEAPPS
	auto deviceIt = _devices.find(deviceId);
	if (deviceIt == _devices.end()) {
		// have to create default 'global app' configuration
		//ApplicationConfig ac(this, "*");
		//ac.init(this);
		APPLICATIONTABLE apptable;
		apptable.insert(std::make_pair(std::string("*"), ApplicationConfig(this, "*")));
		_devices.insert(std::make_pair(deviceId, apptable));
		//auto& app = _devices.find(deviceId)->second;
		//auto& appConfig = app["*"];
		//appConfig.createDefaultDeviceConfigIfDoesntExist(deviceId);
		saveReqd = true;
	}
	APPLICATIONTABLE const& app = _devices.find(deviceId)->second;
	for (APPLICATIONTABLE::const_iterator it=app.begin(); it!= app.end(); it++)
	{
		const ApplicationConfig& ac = it->second;
		const DeviceConfig& dc = ac._config;
		assert(dc.settings() == this);
	}
#else
	// Global app config must exist!
	for (auto& ac : _apps)
	{
		if (!ac.second.deviceConfigExists(deviceId)) {
			ac.second.createDefaultDeviceConfigIfDoesntExist(deviceId);
			saveReqd = true;
		}
	}
#endif
    if (saveReqd)
#ifdef _WINDOWS  
        saveImplw();
#else
        saveImpl();
#endif
    /*
    APPLICATIONTABLE::iterator it = _apps.find("*");
    assert(it != _apps.end());
    if (!it->second.deviceConfigExists(deviceId))
    {
        it->second.createDefaultDeviceConfigIfDoesntExist(deviceId);
        save();
    }
    */
}

// TODO: Implement deviceId
std::vector<std::string> Settings::getApps(unsigned deviceId)
{
    std::vector<std::string> ret;
#ifdef DEVICEAPPS
	auto it = _devices.find(deviceId);
	if (it != _devices.end()) {
		auto& apps = it->second;
		for (auto& app : apps) {
			ret.push_back(app.first);
		}
	}
#else
    for (auto& app: _apps)
        apps.push_back(app.first);
#endif
    return ret;
}

// TODO: Implement deviceId
std::vector<unsigned> Settings::getDevices(const char * app)
{
    std::vector<unsigned> devices;
#ifdef DEVICEAPPS
	for (auto const& it : _devices) {
		devices.push_back(it.first);
	}
#else
    auto it = _apps.find(app ? app : "*");
    if (it != _apps.end())
    {
        for (auto&& device: it->second._devices)
        {
            devices.push_back(device.first);
        }
    }
#endif
    return devices;
}

// TODO: Implement deviceId
std::string Settings::getAppName(unsigned deviceId, const char* identifier)
{
#ifdef DEVICEAPPS
	auto deviceIt = _devices.find(deviceId);
	if (deviceIt == _devices.end()) {
		throw std::runtime_error("Device does not exist");
	}
	auto& apps = deviceIt->second;
#else
	auto& apps = _apps;
#endif
	auto it = apps.find(identifier);
    if (it == apps.end()) {
        throw std::runtime_error("Application profile does not exist");
    }
    return it->second.name();
}

// TODO: Implement deviceId
void Settings::createApp(unsigned deviceId, const char* appName, const char* identifier)
{
	std::string sAppId(identifier);
#ifdef _WINDOWS
	// Windows file names are case insensitive. So normalize it to lower case.
	sAppId = tolower(sAppId);
#endif

#ifdef DEVICEAPPS
	auto deviceIt = _devices.find(deviceId);
	if (deviceIt == _devices.end()) {
		throw std::runtime_error("Device does not exist");
	}
	auto& apps = deviceIt->second;
#else
	auto& apps = _apps;
#endif

	BOOST_LOG_TRIVIAL(debug) << "Settings::createApp - "
        << "appName: " << appName
        << ", binaryPath: " << sAppId;

    if (apps.find(sAppId) != apps.end()) {
        BOOST_LOG_TRIVIAL(debug) << "\tconfiguration already exists";
        throw std::runtime_error("Application configuration already exists");
    }

	// Replicate global config
	ApplicationConfig appConfig = apps.find("*")->second;
	appConfig._config = DeviceConfig::createCopyingCoreButtonClicks(appConfig._config);
	appConfig._appName = appName;

    // create ApplicationConfig entry for the app
    auto ret = apps.insert(std::make_pair(sAppId, appConfig/*ApplicationConfig(this, appName)*/));

#ifndef DEVICEAPPS
	auto& newAppConfig = ret.first->second;
    // create default DeviceConfig entry for all devices in the global('*') app config
    APPLICATIONTABLE::iterator it = apps.find("*");
    for (auto& device: it->second._devices)
    {
        newAppConfig.createDefaultDeviceConfigIfDoesntExist(device.first);
    }
#endif

#ifdef _WINDOWS
	saveImplw();
#else
    saveImpl();
#endif
}

// TODO: Implement deviceId
void Settings::deleteApp(unsigned deviceId, const char* identifier)
{
	std::string sAppId(identifier);
#ifdef _WINDOWS
	// Windows file names are case insensitive. So normalize it to lower case.
	sAppId = tolower(sAppId);
#endif

	if (sAppId == "*") {
		throw std::runtime_error("Cannot delete global application profile");
	}

#ifdef DEVICEAPPS
	auto deviceIt = _devices.find(deviceId);
	if (deviceIt == _devices.end()) {
		throw std::runtime_error("Device does not exist");
	}
	auto& apps = deviceIt->second;
#else
	auto& apps = _apps;
#endif

    BOOST_LOG_TRIVIAL(debug) << "Settings::deleteApp - "
        << ", identifier: " << sAppId;

    auto it = apps.find(sAppId);
    if (it == apps.end()) {
        BOOST_LOG_TRIVIAL(debug) << "\tapp not found";
        throw std::runtime_error("Application profile does not exist");
    }

    apps.erase(it);
#ifdef _WINDOWS
	saveImplw();
#else
    saveImpl();
#endif 
}

// load() implementation without acquiring mutex lock
void Settings::loadImpl(const char* filePath)
{
#ifndef DEVICEAPPS
    _apps.clear();
#endif
    std::ifstream fin(filePath);
    json j;
    fin >> j; // *&*&*&v1_20210417 not supported on Windows UTF16 path
    //json j = json::parse(fin);
    j.get_to(*this);
}
#ifdef _WINDOWS
void Settings::loadImplw(const wchar_t* filePath)
{
    #ifndef DEVICEAPPS
    _apps.clear();
    #endif
    std::ifstream fin(filePath);
    json j;
    fin >> j;
    j.get_to(*this);
}
#endif //_WINDOWS
// save() implementation without acquiring mutex lock
void Settings::saveImpl()
{
    std::ofstream fout(
        Settings::getFileFullPath().c_str());
    std::time(&_timestamp);
    json j = *this;
    fout << std::setw(2) << j;
}
#ifdef _WINDOWS
void Settings::saveImplw()
{
    std::ofstream fout(
        Settings::getFileFullPathW().c_str());
    std::time(&_timestamp);
    json j = *this;
    fout << std::setw(2) << j;
}
#endif
DeviceConfig& Settings::getDeviceConfig(const wchar_t* processFilename, unsigned deviceId, bool fUseGlobal)
{
    auto app = toUTF8(std::wstring(processFilename ? processFilename : L"*"));
    return getDeviceConfig(app.c_str(), deviceId, fUseGlobal);
}

DeviceConfig& Settings::getDeviceConfig(const char* processFilename, unsigned deviceId, bool fUseGlobal)
{
    auto application = std::string(processFilename ? processFilename : "*");

	auto deviceIt = _devices.find(deviceId);
	if (deviceIt == _devices.end()) {
		throw std::runtime_error("Setting for device does not exist");
	}
	const APPLICATIONTABLE& apps = deviceIt->second;

    auto it = apps.end();
    size_t iterCount = 0;

    /*
        At an initial glance, using a do-while loop to search through a table
        of app -> device-id entries might seem strange. But there's a reason
        for doing this.

        When fUseGlobal flag is specified, meaning that if a perfectly matching
        DeviceConfig entry for the given app-name & device-id combo is not
        found, we should return the DeviceConfig entry from the global app
        ('*') table. Thi will always succeed as an entry is guaranteed to be
        present for the given device id in the global app table. (A default
        DeviceConfig entry is created when the device is connected and tbwhelper
        detects it, if one was not already loaded while deserializing
        TbwSettings.json).

        Strictly speaking iterCount is not necessary, but we still add it as a
        safeguard against endless mindless looping. In other words, I'm being
        'kiasu' (if you're reading this, go figure what that means :-)).
     */
    do
    {
        it = apps.find(application);
        if (it == apps.end() && fUseGlobal)
        {
            it = apps.find("*");
        }

        if (it != apps.end())
        {
            const ApplicationConfig& app = it->second;
#ifdef DEVICEAPPS
			return const_cast<DeviceConfig&>(app._config);
#else
            DEVICETABLE::iterator itDevice = app._devices.find(deviceId);
            if (itDevice != app._devices.end())
            {
                // found a matching Device definition!
                return itDevice->second;
            }
#endif
		}

        // Search again through the global app table.
        application = "*";
    } while (fUseGlobal && it != apps.end() && ++iterCount < 2);

    throw std::runtime_error("Matching config for give process/device name not found!");
}

// Given a process name, returns a matching Application profile name
// If a dedicated device setting does not exist for the given process,
// returns the global profile name ('*') as the matching profile name.
std::string Settings::getMatchingProfileName(unsigned deviceId, char const * processFilename)
{
	std::string sAppId = processFilename;
#ifdef _WINDOWS
	// Windows file names are case insensitive. So normalize it to lower case.
	sAppId = tolower(sAppId);
#endif

#ifdef DEVICEAPPS
	auto itDeviceInfo = _devices.find(deviceId);
	if (itDeviceInfo == _devices.end()) {
		//throw std::runtime_error("Matching config for given device not found");
		return std::string();
	}
	auto& apps = itDeviceInfo->second;
#else
	auto& apps = _apps;
#endif

	APPLICATIONTABLE::iterator it = apps.find(sAppId);
	if (it != apps.end())
        return processFilename;

    // return global profile name
    return std::string("*");
}

std::wstring Settings::getMatchingProfileName(unsigned deviceId, wchar_t const * processFilename)
{
    auto app = toUTF8((processFilename ? processFilename : L"*"));
    return toUTF16(getMatchingProfileName(deviceId, app.c_str()));
}

/*
ACTIONPTR Settings::createActionHandler(json const& j)
{
    ACTIONPTR action = getActionHandlerFromName(j["command"]);
    action->load(j);
    return action;
}
*/
json Settings::getActionHandler(const ACTIONPTR& action)
{
    json j = action;
    return j;
}


// Return file timestamp in the format YYYY-MM-DDTHH:MM:SS.SSSZ
// which is a understood by Javascript.
std::string Settings::timeStampAsUTCStr()
{
#ifdef _WINDOWS
    #pragma warning(push)
    #pragma warning(disable: 4996)
#endif
    struct tm *t = gmtime(&_timestamp);

    char buf[128] = { 0 };
    std::strftime(buf, sizeof(buf), "%Y-%m-%dT%H-%M-%S.000Z", t);
#ifdef _WINDOWS
    #pragma warning(pop)
#endif
    return std::string(buf);
}

bool Settings::trayIcon()
{
    return _trayIcon;
}

void Settings::trayIcon(bool enable)
{
    _trayIcon = enable;
}

// Mutex should've been acquired
void Settings::save()
{
#ifdef _WINDOWS
	saveImplw();
#else
    saveImpl();
#endif
}

// Reload settings from TbwSettings.json
// Mutex should've been acquired
void Settings::load(const char* filePath)
{
	assert(filePath != nullptr);
    loadImpl(filePath);
}
#ifdef _WINDOWS
void Settings::loadW(const wchar_t* filePath)
{
    assert(filePath != nullptr);
    loadImplw(filePath);
}
#endif

void to_json(nlohmann::json & j, Settings const & settings)
{
#ifdef DEVICEAPPS
	j["devices"] = settings._devices;
#else
	j["apps"] = settings._apps;
#endif
    j["versionMajor"] = settings._versionMajor;
    j["versionMinor"] = settings._versionMinor;
    j["platform"] = settings._platform;
    j["timestamp"] = settings._timestamp;
    j["trayIcon"] = settings._trayIcon;
}

void from_json(nlohmann::json const & j, Settings & settings)
{
    std::string platform;
    j.at("platform").get_to(platform);
    if (platform != settings.platform())
        throw std::runtime_error("Platform mismatch");

    unsigned versionMajor = 0, versionMinor = 0;
    j.at("versionMajor").get_to(versionMajor);
    j.at("versionMinor").get_to(versionMinor);

    if (versionMajor > settings.versionMajor() ||
        (versionMajor == settings.versionMajor() && versionMinor > settings.versionMinor()))
        throw std::runtime_error("Cannot load from a newer settings version");

    //j.at("versionMajor").get_to(settings._versionMajor);
    //j.at("versionMinor").get_to(settings._versionMinor);
    //j.at("platform").get_to(settings._platform);
    j.at("timestamp").get_to(settings._timestamp);

	settings._trayIcon = getFromStrValueOptional<bool>(j, "trayIcon", true);
#ifdef DEVICEAPPS
	settings._devices.clear();
	j.at("devices").get_to(settings._devices);
	for (auto& deviceIt : settings._devices) {
		for (auto& app : deviceIt.second) {
			app.second.init(&settings);
		}
	}
#else
	settings._apps.clear();
	j.at("apps").get_to(settings._apps);
    for (auto& app : settings._apps)
        app.second.init(&settings);
#endif
}

/**
 * Create a Settings object loading contents from the given file.
 *
 * Throws std::exception upon file access or file parse error.
 */
Settings* Settings::createLoadingFile(const char* filePath)
{
	std::unique_ptr<Settings> settings(new Settings(false));
	settings->load(filePath);
	return settings.release();
}
