#pragma once

#include <cstdint>
#include <string>

namespace devidremap {

enum ConnectionInterface {
	INTERFACE_UNKNOWN = 0,
	INTERFACE_USB,
	INTERFACE_BLUETOOTH
};

uint16_t getRemappedDeviceId(uint16_t realId);
ConnectionInterface getConnectionInterface(uint16_t realId);
std::string connectionInterfaceToStr(ConnectionInterface iface);

}
