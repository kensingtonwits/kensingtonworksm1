/*
 * Implements IDeviceManager interface
 */
#pragma once
#include "IDeviceManager.h"
#include <list>
#include <memory>
#include <mutex>
#include "json.hpp"
#include "DeviceIdRemap.h"
#include "ScrollDirection.h"

class Settings;
class DeviceConfig;

// Device instances should derive from this class, which
// implements the ITbwDevice interface.
class DeviceBase : public ITbwDevice {

protected:
    uint16_t _id;
	uint16_t _realId;
	uint16_t _firmwareVersion;
    std::string _name;
    DeviceConfig* _pConfig;
	devidremap::ConnectionInterface _interface;

    DeviceBase() = delete;
    DeviceBase(const DeviceBase&) = delete;
    DeviceBase& operator=(DeviceBase const&) = delete;

public:
    DeviceBase(uint16_t id, uint16_t firmwareVersion, const char* name, uint16_t realId)
        : _id(id), _realId(realId), _firmwareVersion(firmwareVersion), _name(name), 
		_pConfig(nullptr), _interface(devidremap::INTERFACE_UNKNOWN)
    {}
    virtual ~DeviceBase()
    {}

	void init(uint16_t id, uint16_t firmwareVersion, const char* name);
    virtual uint16_t id() const { return _id; }
	virtual uint16_t realId() const { return _realId; }
	virtual std::string name() const { return _name; }
    virtual uint16_t version() const { return _firmwareVersion; }
	virtual devidremap::ConnectionInterface connectionInterface() { return _interface; }

    virtual void setConfig(DeviceConfig& config, bool force);
    virtual DeviceConfig& getConfig() const;
    virtual bool postKeystroke(uint32_t modifiers, uint32_t keyCode, bool fDown, bool fModifiersOnly);
    virtual bool postPointerClick(uint32_t button, bool fDown);
    virtual bool postScrollNavigation(ScrollDirection direction, int lines);

    virtual void updatePointerParams() = 0;
    virtual void updateScrollWheelParams() = 0;
    virtual void updateButtonsConfig() = 0;
#ifdef TAU
    virtual void emulateButtonClick(uint32_t mask) = 0;
#endif
};
typedef std::shared_ptr<DeviceBase> DEVICEPTR;

// Base class for DeviceManager, which implements the
// ITbwDeviceManager interface.
class DeviceManagerBase : public ITbwDeviceManager {

protected:
    std::mutex  _mutex;
    std::unique_ptr<Settings> _settings;
    typedef std::list<DEVICEPTR> DEVICEPTRLIST;
    DEVICEPTRLIST _devices;

public:
    virtual ~DeviceManagerBase();

    // get/set the entire settings
    virtual nlohmann::json getSettings();
    virtual void setSettings(const nlohmann::json&);

    // Get/set general application wide settings
    virtual nlohmann::json getGeneralConfig();
    virtual void setGeneralConfig(const nlohmann::json&);

    // Returns all the available devices
    virtual nlohmann::json getDevices();

    // Returns all the configured app identifiers & its friendly name.
    virtual nlohmann::json getApps(unsigned deviceId);

    // Create a new app
    virtual void createApp(unsigned deviceId, const nlohmann::json& app);

    // Delete an existing app with the given identifier
    virtual void deleteApp(unsigned deviceId, const nlohmann::json& app);

    // get/set pointer configuration
    virtual nlohmann::json getPointerConfig(const char* app, uint16_t deviceId);
    virtual void setPointerConfig(const char* app, uint16_t deviceId, const nlohmann::json&);

    // get/set scroll wheel configuration
    virtual nlohmann::json getScrollWheelConfig(const char* app, uint16_t deviceId);
    virtual void setScrollWheelConfig(const char* app, uint16_t deviceId, const nlohmann::json&);

    // get/set button action
    virtual nlohmann::json getButtonAction(const char* app, uint16_t deviceId, uint32_t buttonMask);
    virtual void setButtonAction(const char* app, uint16_t deviceId, uint32_t buttonMask, const nlohmann::json& action);

    virtual bool trayIcon();
    virtual void trayIcon(bool enable);

    // Automated testing interface
#ifdef TAU
    virtual void emulateButtonClick(uint16_t deviceId, uint32_t buttonMask);
#endif

	// Return current snippet array for the given app & device id
	nlohmann::json getSnippets(const char* app, uint16_t deviceId);

    // For all its methods, DeviceManagerBase implementation uses these
    // two methods to place locks on the object. Default implementation of
    // these use the _mutex.lock/unlock methods. In Windows, we use native
    // CRITICAL_SECTION object that is provided by the underlying 
    // MsgWFMOHandler. So Windows implemenation of DeviceManager can override
    // these methods and implement object locks using its native
    // CRITICAL_SECTION object.
    virtual void objLock();
    virtual void objUnlock();

protected:
    DeviceBase* getDevice(uint16_t productId);
};
