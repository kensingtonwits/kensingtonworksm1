#include "stdafx.h"
#include "tbwMshm.h"

namespace tbwMshm
{
	std::map<std::string, int> mshmMap =
	{ { MSHMEM_FHEADER, HEADER },
		{ MSHMEM_FPID, PID },
		{ MSHMEM_FHBEAT, HEARTBEAT},
		{ MSHMEM_FTAILER, TAILER},
		{ MSHMEM_FTBWDEVICES, TBWDEVICES},
	};
#ifdef __APPLE__
	boost::gregorian::date dateStart(1970, 1, 1);
	static boost::posix_time::ptime const epoch(dateStart);
#else
	static boost::posix_time::ptime const epoch(boost::gregorian::date::date(1970, 1, 1));
#endif
	bipc::named_mutex		*mshm_mutex; 
	bipc::named_condition	*mshm_cv; 

	std::uint32_t	mpid = 0;
	boost::interprocess::managed_shared_memory *mshm_segment = nullptr;
	//static tbw_String *mshm_tbwDevives;
	//static std::pair<uint32_t *, managed_shared_memory::size_type> mheader;

	bool tbwMshm_setup(uint32_t pid)
	{
		bool bcreated = false;
		BOOST_LOG_TRIVIAL(info) << "tbwMshm_setup .." ;
		try
		{
			// for debug  //boost::interprocess::shared_memory_object::remove(MSHMEM_NAME);
			mshm_segment = new boost::interprocess::managed_shared_memory(bipc::create_only, MSHMEM_NAME, MSHMEM_SIZE);
			bcreated = true;
		}
		catch (boost::interprocess::interprocess_exception& ex)
		{
			BOOST_LOG_TRIVIAL(info) << "mshm_segment err : " << ex.what();

			if (ex.get_error_code() == boost::interprocess::already_exists_error)
			{
				mshm_segment = new boost::interprocess::managed_shared_memory(bipc::open_or_create, MSHMEM_NAME, MSHMEM_SIZE);
				BOOST_LOG_TRIVIAL(info) << "mshm_segment opened" << std::endl;
			}
			else
			{
				std::cout << " shared memory access exception : " << ex.what() << "\r\n";
				BOOST_LOG_TRIVIAL(info) << " shared memory access exception : " << ex.what() << std::endl;
				return false;
			}
		}

		if (bcreated)
		{
			BOOST_LOG_TRIVIAL(info) << "mshm_segment created. ";
			uint32_t *header = mshm_segment->construct<uint32_t>
				(MSHMEM_FHEADER)  //name of the object
				(0x5A5A5A5A);
			uint32_t *hpid = mshm_segment->construct<uint32_t>
				(MSHMEM_FPID)  //name of the object
				(pid);
			int64_t *hbeat = mshm_segment->construct<int64_t>
				(MSHMEM_FHBEAT)  //name of the object
				//((boost::posix_time::microsec_clock::universal_time() - epoch).total_milliseconds());
				(0);
			uint32_t *tailer = mshm_segment->construct<uint32_t>
				(MSHMEM_FTAILER)  //name of the object
				(0xA5A5A5A5);

			tbw_String* tbwDevives = mshm_segment->construct<tbw_String>(MSHMEM_FTBWDEVICES)(
				"Message to other process",
				mshm_segment->get_segment_manager());

			//std::strcpy(tbwDevives->data(), "new message : Devices list 1234567890---------");
			BOOST_LOG_TRIVIAL(info) << " pid: " << pid;
			BOOST_LOG_TRIVIAL(info) << "X m_pid: " << *hpid ;
			BOOST_LOG_TRIVIAL(info) << "X heartbeat: " << *hbeat;
			BOOST_LOG_TRIVIAL(info) << "X helper: " << *tbwDevives << std::endl;
		}
		
		return true;

		
	}



	bool tbwMshm_create(uint32_t pid)
	{
		try
		{
			BOOST_LOG_TRIVIAL(info) << "mshm_segment init " << mshm_segment << std::endl;

			mpid = pid;
			if (mshm_segment == nullptr)
			{
				BOOST_LOG_TRIVIAL(info) << "mshm_segment init";


				//Remove shared memory on construction and destruction			
				boost::interprocess::shared_memory_object::remove(MSHMEM_NAME);
				mshm_segment = new boost::interprocess::managed_shared_memory(bipc::open_or_create, MSHMEM_NAME, MSHMEM_SIZE);

			}
			else
			{
				BOOST_LOG_TRIVIAL(info) << "mshm_segment has been created.";
				return true;
			}
			if (mshm_segment != nullptr)
			{
				BOOST_LOG_TRIVIAL(info) << "mshm_segment created. ";
				uint32_t *header = mshm_segment->construct<uint32_t>
					(MSHMEM_FHEADER)  //name of the object
					(0x5A5A5A5A);
				uint32_t *hpid = mshm_segment->construct<uint32_t>
					(MSHMEM_FPID)  //name of the object
					(pid);
				int64_t *hbeat = mshm_segment->construct<int64_t>
					(MSHMEM_FHBEAT)  //name of the object
					((boost::posix_time::microsec_clock::universal_time() - epoch).total_milliseconds());
				uint32_t *tailer = mshm_segment->construct<uint32_t>
					(MSHMEM_FTAILER)  //name of the object
					(0xA5A5A5A5);

				tbw_String* tbwDevives = mshm_segment->construct<tbw_String>(MSHMEM_FTBWDEVICES)(
					"Message to other process",
					mshm_segment->get_segment_manager());

				std::strcpy(tbwDevives->data(), "new message : Devices list 1234567890---------");
				BOOST_LOG_TRIVIAL(info) << " pid: " << pid;
				BOOST_LOG_TRIVIAL(info) << "X m_pid: " << *hpid << std::endl;
				BOOST_LOG_TRIVIAL(info) << "X helper: " << *tbwDevives << std::endl;
			}
			else
				return false;
		}
		catch (std::exception ex)
		{
			std::cout << " shared memory access exception : " << ex.what() << "\r\n";
			return false;
		}

		return true;
	}

	bool tbwMshm_open(uint32_t *pid)
	{
		*pid = 0;
		try
		{

			if (mshm_segment == nullptr)
			{
				BOOST_LOG_TRIVIAL(info) << "mshm_segment open";
				std::cout << "managed_shared_memory{ open_only," << MSHMEM_NAME << " } \r\n";
				mshm_segment = new boost::interprocess::managed_shared_memory(bipc::open_only, MSHMEM_NAME);
				std::cout << " --- opened \r\n";
			}
			if (mshm_segment != nullptr)
			{
				std::pair<uint32_t *, bipc::managed_shared_memory::size_type> memheader = mshm_segment->find<uint32_t>(MSHMEM_FHEADER);

				if (memheader.second > 0) // length
					std::cout << " header " << *(memheader.first) << std::endl;
				//Find the object
				std::pair<uint32_t *, bipc::managed_shared_memory::size_type> mempid = mshm_segment->find<uint32_t>(MSHMEM_FPID);
				//Length should be 1
				//if (res.second != 1) return 1;
				if (mempid.second > 0)
				{
					*pid = *mempid.first;
					mpid = *pid;
					std::cout << " pid " << *(mempid.first) << std::endl;

					//Just keep in mind that zombie pids will be reported as running
					if (!is_Helper_Running())
					{
						std::cout << "Helper is not running !" << std::endl;
						*pid = 0;
					}
				}

				//Find the array constructed from iterators
				std::pair<uint32_t *, bipc::managed_shared_memory::size_type> memtailer = mshm_segment->find<uint32_t>(MSHMEM_FTAILER);
				if (memtailer.second > 0)
					std::cout << " tailer " << *(memtailer.first) << std::endl;

				std::pair<tbw_String *, bipc::managed_shared_memory::size_type> memDevices = mshm_segment->find<tbw_String>(MSHMEM_FTBWDEVICES);
				std::cout << "got " << memDevices.second << " strings " << endl;
				if (memDevices.second > 0)
					std::cout << "first string is->" << memDevices.first->data() << "\r\n";
				//BOOST_LOG_TRIVIAL(info) << " m_pid: " << m_pid;
				//BOOST_LOG_TRIVIAL(info) << "C m_pid: " << res.first;

				return true;
			}
		}
		catch (std::exception ex)
		{
			std::cout << " shared memory access exception : " << ex.what() << "\r\n";
		}
		return false;
	}

	bool tbwMshm_remove()
	{
		mshm_segment = nullptr;
		return bipc::shared_memory_object::remove(MSHMEM_NAME);
	}

	bool writeMshmElement(std::string element, const void *pvalue)
	{
		if (mshm_segment != nullptr)
		{
			std::map<std::string, int>::iterator iter;
			iter = mshmMap.find(element);
			if (iter == mshmMap.end())
				return false;
			try
			{
				switch (iter->second)
				{
					case HEADER:
					case TAILER:
					case PID:
					{
						std::pair<uint32_t *, bipc::managed_shared_memory::size_type> pel = mshm_segment->find<uint32_t>(element.data());
						*pel.first = *(uint32_t *)pvalue;
					}
						break;
					case HEARTBEAT:
					{
						std::pair<int64_t *, bipc::managed_shared_memory::size_type> pel = mshm_segment->find<int64_t>(element.data());
						*pel.first = *(int64_t *)pvalue;
					}
						break;
					case TBWDEVICES:
					{
						std::pair<tbw_String *, bipc::managed_shared_memory::size_type> p = mshm_segment->find<tbw_String>(MSHMEM_FTBWDEVICES);
						std::strcpy(p.first->data(), ((std::string*)pvalue)->data());
					}
						break;
					default:
						return false;
						break;
				}
				return true;
			}
			catch (std::exception ex)
			{
				std::cout << " shared memory access exception : " << ex.what() << "\r\n";
			}
		}

		return false;
	}

	bool readMshmElement(std::string element, void *pvalue)
	{
		if (mshm_segment != nullptr)
		{
			std::map<std::string, int>::iterator iter;
			iter = mshmMap.find(element);
			if (iter == mshmMap.end())
				return false;
			try
			{
				switch (iter->second)
				{
					case HEADER:
					case TAILER:
					case PID:
					{
						std::pair<uint32_t *, bipc::managed_shared_memory::size_type> pel = mshm_segment->find<uint32_t>(element.data());
						*(uint32_t *)pvalue = *pel.first;
					}
					break;
					case HEARTBEAT:
					{
						std::pair<int64_t *, bipc::managed_shared_memory::size_type> pel = mshm_segment->find<int64_t>(element.data());
						*(int64_t *)pvalue = *pel.first;
					}
					break;
					case TBWDEVICES:
					{
						std::pair<tbw_String *, bipc::managed_shared_memory::size_type> p = mshm_segment->find<tbw_String>(MSHMEM_FTBWDEVICES);
						//std::strcpy(((std::string*)pvalue)->, p.first->data());
						*(std::string*)pvalue = p.first->data();
					}
					break;
					default:
						return false;
						break;
				}
				return true;
			}
			catch (std::exception ex)
			{
				std::cout << " shared memory access exception : " << ex.what() << "\r\n";
			}
		}

		return false;
	}


	bool updateMshmDevices(std::string strDevices)
	{
		try
		{
			if (mshm_segment != nullptr)
			{
				std::pair<tbw_String *, bipc::managed_shared_memory::size_type> p = mshm_segment->find<tbw_String>(MSHMEM_FTBWDEVICES);
				BOOST_LOG_TRIVIAL(info) << "tbwDevives1: " << p.first->c_str() << std::endl;
				std::strcpy(p.first->data(), strDevices.data());
				BOOST_LOG_TRIVIAL(info) << "tbwDevives2: " << getMshmDevices() << std::endl;
				return true;
			}
		}
		catch (std::exception ex)
		{
			std::cout << " shared memory access exception : " << ex.what() << "\r\n";
		}
		return false;
	}

	std::string getMshmDevices()
	{
		try
		{
			if (mshm_segment != nullptr)
			{
				std::pair<tbw_String *, bipc::managed_shared_memory::size_type> p = mshm_segment->find<tbw_String>(MSHMEM_FTBWDEVICES);
				//BOOST_LOG_TRIVIAL(info) << "tbwDevives: " << p.first->c_str() << std::endl;
				return std::string(p.first->c_str());
			}
		}
		catch (std::exception ex)
		{
			std::cout << " getMshmDevices memory access exception : " << ex.what() << "\r\n";
		}
		return std::string("");
	}

	void wHeartbeat()
	{
		int64_t beat = (boost::posix_time::microsec_clock::universal_time() - epoch).total_milliseconds();
		writeMshmElement(MSHMEM_FHBEAT, &beat);
		
		//BOOST_LOG_TRIVIAL(debug) << "wHeartbeat : " << beat;

	}

	int64_t rHeartbeat()
	{
		int64_t beat = 0;

		readMshmElement(MSHMEM_FHBEAT, &beat);

		//BOOST_LOG_TRIVIAL(info) << "rHeartbeat : " << beat;

		return beat;

	}

	bool is_Helper_Running() 
	{
		//Just keep in mind that zombie pids will be reported as running
		if (mpid <= 0) {
			return false;
		}
		#ifdef _WINDOWS
		HANDLE handle = OpenProcess(SYNCHRONIZE, false, mpid);
		if (!handle) {
			return false;
		}
		else {
			CloseHandle(handle);
			return true;
		}
		#else //defined(PLATFORM_ENVIRONMENT_UNIX)
		if (kill(mpid, 0) == -1) {
			return false;
		}
		else {
			return true;
		}
#endif
	}

} // namespace


