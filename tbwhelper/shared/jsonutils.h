
#pragma once

#include "json.hpp"


/*
    Gets a key's value from the a json, converting it to the specified type,
    if the stored value is string. If it is of the same type as the specified
    type (template argument), then it attempts an implicit conversion using
    json library's implicit conversion mechanism.

    If the specified key does not exist, throws an exception.

    Note that since this function template parameter is only used as a return
    type, the function would not take part of template type deduction.
    Consequently, it has to be used by specifying the required type explicitly.

    For instance, to get an unsigned value:

        json j;
        unsigned uValue = getFromStrValue<unsigned>(j, "keyName");
*/
template<typename T>
T getFromStrValue(const nlohmann::json& j, const std::string& key)
{
    auto json_value = j.at(key);
    if (json_value.is_string())
        return to<T>(json_value.get<std::string>());
    return json_value.get<T>();
}

/*
    Same as above, but the key's presence in the supplied json is optional. 
    If it's not found, returns the default value specified as 'def' argument
    (unlike the previous function, which throws an exception).

    Can be used as:

        json j;
        unsigned uValue = getFromStrValueOptional(j, "keyName", 0u);

*/
template<typename T>
T getFromStrValueOptional(const nlohmann::json& j, const std::string& key, const T& def)
{
    if (j.find(key) == j.end())
        return def;

    auto json_value = j.at(key);
    if (json_value.is_string())
        return to<T>(json_value.get<std::string>());
    return json_value.get<T>();
}

/*
    Merge two json objects returning the new merged object.
    Note that this merge is different from the official JSON standards merge,
    which depends on JSON patch files. For that json library natively provides
    a method (json::merge_patch).

    This function rather combines the keys in a & b generating a new JSON
    object with all the keys. Like a superset of two subsets.
 */
inline json merge(const json &a, const json &b)
{
    json result = a.flatten();
    json tmp = b.flatten();
    for (auto it = tmp.begin(); it != tmp.end(); ++it)
        result[it.key()] = it.value();
    return result.unflatten();
}
