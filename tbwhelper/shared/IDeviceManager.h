/*
 * IDeviceManager.h
 *
 *  Interfaces to be implemented by platform specific DeviceManager class.
 *  CommandServer, the HTTP server that will provide configuration services to
 *  the GUI will use these methods to implement its features.
 */
#pragma once
#define TAU // *&*&*&Va_20200901
#include <string>
#include <memory>
#include <vector>
#include "json.hpp"
#include "ScrollDirection.h"

 // Each connected device object instance should implement this interface
struct ITbwDevice {

    // Return USB device id
    virtual uint16_t id() const = 0;

    // Return device name as in the USB firmware
    virtual std::string name() const = 0;

    // Return firmware version
    virtual uint16_t version() const = 0;

    // Post a keystroke into the system input queue
    //
    // Parameters:
    //  uint32_t modifiers - modifiers mask as defined in Modifiers.h
    //  uint32_t keyCode - platform specific keycode
    //
    // Returns:
    //  bool - True if the implementaion handled the request and an event
    //      was indeed posted to the system input queue. False if it was
    //      not handled.
    virtual bool postKeystroke(uint32_t modifiers, uint32_t keyCode, bool fDown, bool fModifiersOnly) = 0;

    // Post a mouse click event into the system input queue
    //
    // Parameters:
    //  uint32_t button - 0 based button index.
    //  bool fDown      - True for button depress, False for button release.
    //
    // Returns:
    //  bool - True if the implementaion handled the request and an event
    //      was indeed posted to the system input queue. False if it was
    //      not handled.
    virtual bool postPointerClick(uint32_t button, bool fDown) = 0;

    // Post scroll navigation event
    //
    // Parameters:
    //  direction - direction of navigation
    //  lines - units to scroll
    //
    // Returns:
    //  bool - True if the implementaion handled the request and an event
    //      was indeed posted to the system input queue. False if it was
    //      not handled.
    virtual bool postScrollNavigation(ScrollDirection direction, int lines) = 0;
};
typedef std::shared_ptr<ITbwDevice> TBWDEVICEPTR;


// List of connected devices
struct TBDevice {
    uint16_t deviceId;
    std::string name;
    uint16_t firmwareVersion;
};

inline void to_json(nlohmann::json & j, TBDevice const & device)
{
    j["id"] = device.deviceId;
    j["name"] = device.name;
    j["firmwareVersion"] = device.firmwareVersion;
}

inline void from_json(nlohmann::json const & j, TBDevice & device)
{
    j.at("id").get_to(device.deviceId);
    j.at("name").get_to(device.name);
    j.at("firmwareVersion").get_to(device.firmwareVersion);
}

// Device manager that keeps track of devices, instantiating IDevice object
// upon connection and releasing them upon device removal.
struct ITbwDeviceManager {

    virtual nlohmann::json getSettings() = 0;
    virtual void setSettings(const nlohmann::json&) = 0;

    virtual nlohmann::json getGeneralConfig() = 0;
    virtual void setGeneralConfig(const nlohmann::json&) = 0;

    // Returns all the available devices
    virtual nlohmann::json getDevices() = 0;

    /*
    Returns all the configured app profiles.
     
    JSON Format:-

        {
            "apps": [
                { "*": "*" },
                { "<identifier>": "<friendly-name>" }
                ...
                ]
        }

        App with identifier "*" is the global app profile.
    */
    virtual nlohmann::json getApps(unsigned deviceId) = 0;

    /*
    Create a new application profile.

    JSON Format:-

     {
        "identifier": "<identifier>",
        "name": "<friendly-name>"
     }
     */
    virtual void createApp(unsigned deviceId, const nlohmann::json& app) = 0;

    /*
    Delete an existing app profile.

    JSON Format:-

    {
        "identifier": "<profile-identifier>"
    }
    */
    virtual void deleteApp(unsigned deviceId, const nlohmann::json& app) = 0;

    /*
    Get pointer configuration for the given device under the given application
    profile.

    Params:
        app - application profile
        deviceId - 16-bit USB device Id.

    Returns JSON:
        
        {
            "speed": <speed>,
            "acceleration": <pointer-acceleration>,
            "slowModifiers": <key-combination-mask>,
            "lockedAxisModifiers": <key-combination-mask>
        }

        <speed>: Numeber. A value in the range 0-100.
        <acceleration>: Number, A value in the range 0-100. 0 indicates
            acceleration is disabled.
        <key-combination-mask>: Number. Modifier hotkey combination for 
            activating slow pointer movement.
    */
    virtual nlohmann::json getPointerConfig(const char* app, uint16_t deviceId) = 0;

    /*
    Set pointer configuration for the given device under the given application
    profile.

    Params:
        app - application profile
        deviceId - 16-bit USB device Id.
        config: A JSON of the following format:
            {
                "speed": <speed>,
                "acceleration": <pointer-acceleration>,
                "slowModifiers": <key-combination-mask>,
                "lockedAxisModifiers": <key-combination-mask>
            }

            <speed>: Numeber. A value in the range 0-100.
            <acceleration>: Number, A value in the range 0-100. 0 indicates
                acceleration is disabled.
            <key-combination-mask>: Number. Modifier hotkey combination for
                activating slow pointer movement.
    */
    virtual void setPointerConfig(const char* app, uint16_t deviceId, const nlohmann::json& config) = 0;
    
    /*
    Get scroll wheel configuration for the given device under the given 
    application profile.

    Params:
        app - application profile
        deviceId - 16-bit USB device Id.

    Returns JSON:

        {
            "speed": <speed>,
            "inertial" <boolean>,
            "invert": <boolean>
        }

        <speed>: Numeber. A value in the range 0-100.
        <boolean>: true|false. Indicates if the feature is ON or OFF.
    */
    virtual nlohmann::json getScrollWheelConfig(const char* app, uint16_t deviceId) = 0;

    /*
    Set scroll wheel configuration for the given device under the given
    application profile.

    Params:
        app - application profile
        deviceId - 16-bit USB device Id.
        config: A JSON of the following format:
            {
                "speed": <speed>,
                "inertial" <boolean>,
                "invert": <boolean>
            }

            <speed>: Numeber. A value in the range 0-100.
            <boolean>: true|false. Indicates if the feature is ON or OFF.
    */
    virtual void setScrollWheelConfig(const char* app, uint16_t deviceId, const nlohmann::json&) = 0;

    // get/set button action
    /*
    Get action mapping for the given button mask, for the given application
    profile and device id.

    Params:
        app - application profile
        deviceId - 16-bit USB device Id.
        buttonMask - the buttonMask of the action being queried.

    Returns JSON:
        {
            "command": "{none|singleClick|keystroke|open}"
            <action specific parameters>
        }
    */
    virtual nlohmann::json getButtonAction(const char* app, uint16_t deviceId, uint32_t buttonMask) = 0;

    /*
    Set action mapping for the given button mask, for the given application
    profile and device id.

    Params:
        app - application profile
        deviceId - 16-bit USB device Id.
        buttonMask - the buttonMask of the action being set.
        action - JSON of the format:
            {
                "command": "{none|singleClick|keystroke|open}"
                <action specific parameters>
            }
    */
    virtual void setButtonAction(const char* app, uint16_t deviceId, uint32_t buttonMask, const nlohmann::json& action) = 0;
#ifdef TAU    
    virtual void emulateButtonClick(uint16_t deviceId, uint32_t buttonMask) = 0;
#endif
	/*
	Get scroll wheel configuration for the given device under the given
	application profile.

	Params:
		app - application identifier
		deviceId - device Id.

	Returns JSON of 10 snippets (current max snippet count):

		[
			{
				"content": <snippet text>,
				"label" <snippet label>,
			},
			{
				"content": <snippet text>,
				"label" <snippet label>,
			},
			...
			{
				"content": <snippet text>,
				"label" <snippet label>,
			}
		]

		<speed>: Numeber. A value in the range 0-100.
		<boolean>: true|false. Indicates if the feature is ON or OFF.
	*/
	virtual nlohmann::json getSnippets(const char* app, uint16_t deviceId) = 0;

};
