// rmlegacy.cpp
//  A stub to detect & launch legacy TrackballWorks uninstaller silently.
//

#include "targetver.h"
#include <iostream>
#include <windows.h>
#include <atlbase.h>
#include <string>
#include <ShlObj.h>

#include "remlegacy.h"

#ifdef _WIN64
const LPCTSTR REG_KEY_UNINSTALL = _T("SOFTWARE\\WOW6432Node\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\{004C4695-1C46-4d7e-A48E-FEF6A5AD32C4}");
#else
const LPCTSTR REG_KEY_UNINSTALL = _T("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\{004C4695-1C46-4d7e-A48E-FEF6A5AD32C4}");
#endif


// Returns a boolean indicating if legacy TBW is installed
std::wstring getLegacyTbwUninstaller()
{
    std::wstring unInstaller;

    // Read the UninstallString from HKLM
    CRegKey regKey;
    if (regKey.Open(
        HKEY_LOCAL_MACHINE,
        REG_KEY_UNINSTALL) == ERROR_SUCCESS) {

        TCHAR szUninstall[4096] = { 0 };
        DWORD cbUninstall = sizeof(szUninstall) / sizeof(szUninstall[0]);
        if (regKey.QueryStringValue(L"UninstallString", szUninstall, &cbUninstall) == ERROR_SUCCESS) {
            unInstaller = szUninstall;
            auto sep = unInstaller.find(L".exe");
            unInstaller = unInstaller.substr(0, sep + 4);
        }
    }
    return unInstaller;
}

std::wstring getPreferencesFolder()
{
    std::wstring sPath;
    PWSTR pszPath{ nullptr };
    if (::SHGetKnownFolderPath(
        FOLDERID_RoamingAppData,
        0,
        NULL,
        &pszPath) == S_OK)
    {
        sPath = pszPath;
        ::CoTaskMemFree(pszPath);
        if (sPath.at(sPath.length() - 1) != TEXT('\\'))
            sPath.append(TEXT("\\"));
        sPath.append(TEXT("Kensington\\TrackballWorks"));
    }
    return sPath;
}

// Backup settings to TbwSettings.xml.bak
void backupSettings()
{
    std::wstring settings = getPreferencesFolder();
    std::wstring orgSettings = settings + L"\\TbwSettings.xml";
    std::wstring newSettings = settings + L"\\TbwSettings.xml.bak";
    ::CopyFileW(orgSettings.c_str(), newSettings.c_str(), FALSE);
}

// Restore settings from TbwSettings.xml.bak to TbwSettings.xml
void restoreSettings()
{
    std::wstring settings = getPreferencesFolder();
    std::wstring orgSettings = settings + L"\\TbwSettings.xml.bak";
    std::wstring newSettings = settings + L"\\TbwSettings.xml";
    ::CopyFileW(orgSettings.c_str(), newSettings.c_str(), FALSE);
}

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.
    auto uninstaller = getLegacyTbwUninstaller();
    if (uninstaller.length() == 0) {
        // Legacy TBW not found!
        std::cerr << "Legacy TBW not found!\n";
        return 0;
    }

    if (uninstaller.at(0) != L'"') {
        uninstaller.insert(0, L"\"");
    }

    if (uninstaller.at(uninstaller.length() - 1) != L'\"') {
        uninstaller.append(L"\"");
    }
    // append the silent uninstaller option
    uninstaller.append(L" -s");

    // execute the uninstaller and wait for it to finish
    STARTUPINFO si = { 0 };
    si.cb = sizeof(si);
    PROCESS_INFORMATION pi = { 0 };
    std::wcout << "Legacy uninstaller: " << uninstaller << std::endl;
    if (::CreateProcessW(
        NULL,
        (LPWSTR)uninstaller.c_str(),
        NULL,
        NULL,
        TRUE,
        0,
        NULL,
        NULL,
        &si,
        &pi)) {

        auto start = ::GetTickCount();

        const DWORD MAX_TIMEOUT = 10 * 1000; // 10 seconds
        DWORD dwRet = 0;
        do {
            dwRet = ::WaitForSingleObject(pi.hProcess, 10);
        } while (((::GetTickCount() - start) < MAX_TIMEOUT) && dwRet != WAIT_OBJECT_0);

        ::CloseHandle(pi.hProcess);
        ::CloseHandle(pi.hThread);

        if (dwRet != WAIT_OBJECT_0) {
            std::cerr << "Legacy TBW uninstaller did not terminate gracefully!\n";
        }
        else {
            std::cerr << "Legacy TBW uninstaller terminated gracefully!\n";
        }
        return dwRet == WAIT_OBJECT_0 ? 0 : 1;
    }
    else {
        std::cerr << "Error starting legacy TBW uninstaller!\n";
    }

    return 1;   // failed
}
