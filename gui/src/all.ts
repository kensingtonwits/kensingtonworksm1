import * as $ from "jquery";
import { app, shell, remote, ipcRenderer } from "electron";
import { PRODUCT_VERSION } from "./version";
import { UpdateStatus } from "./update";
import * as fs from "fs";
import { loadString, getTemplateFilePath, getMainWindow } from "./helper";
import { showPopup, hidePopup, hideCoverBox, hideAsyncPopup, isPopupActive } from "./popups";
import { UserCancellation } from "./errors";
import axios from 'axios';
import { setPreferredLanguage, getPreferredLanguage } from "./i18n";

// timeout all network operations by 60 seconds
axios.defaults.timeout = 60*1000;   // 60 seconds

/**
 * Class encapsulates the home page logic. Most of the init* function methods
 * are actually code written by Begonia provided as part of all.js & index.js.
 * I just abstracted them as individual blocks (while preserving the
 * original comments), gave them a name that somewhat matches their role
 * (based on what I could interpret from the original code) and call it from
 * the constructor mimicking their page initialization logic.
 *
 * Once the initialization is complete, our own logic kicks in. The primary
 * logic is detection of devices from the helper.
 */
export function initDimensions(autoDismissLoader = true): void {
    // Window 相關設定
    var w: any = window,
        win = $(window),
        ww,
        wh,
        ws; // 取得Window設定值

    var windowSetting = function windowSetting() {
        ww = win.innerWidth();
        wh = win.innerHeight();
        ws = win.scrollTop();
    };

    windowSetting(); // ----------------------------------- Window 相關監測
    // window on scroll use javascript
    // Reference: https://stackoverflow.com/a/10915048
    // http://dev.w3.org/2006/webapi/DOM-Level-3-Events/html/DOM3-Events.html#event-type-scroll

    function onScrollEventHandler(ev: any) { }

    function onResizeEventHandler(ev: any) {
        windowSetting();
    }

    w.addEventListener('scroll', () => {
    }, false);

    w.addEventListener('resize', () => {
        windowSetting();
    }, false);

    /*
    // the original w.addEventListener('load', ...) is commented out
    w.addEventListener('load', () => {
        // from function loaderRemove() in original code
        $('.loader').addClass('loaded');
        $('html').addClass('loaded');
        setTimeout(function () {
            $('.loader').remove();
        }, 3000);
        $('.loader').fadeOut('2000', function () {
            $(this).remove();
        });
    }, false);
    */

    if (autoDismissLoader) {
        setTimeout(function () {
            // $('.loader').remove();
            $('.loader').fadeOut('300', function () {
                $(this).remove();
            });
        }, 200); // window load
    }
}

export function initScrollbar(): void
{
    var scroll_w = $('#scroll_w').width();
    $('.scrollbar').mCustomScrollbar({
        theme: "dark",
        // 設定捲軸樣式
        setWidth: scroll_w,
        // 設定寬度
        setHeight: 768 - 109.5 // 設定高度

    });
    $('.app_list_more').mCustomScrollbar({
        theme: "dark",
        // 設定捲軸樣式
        setWidth: scroll_w,
        // 設定寬度
        setHeight: 768 - 109.5 // 設定高度

    });
    $('.re_left').mCustomScrollbar({
        theme: "dark",
        // 設定捲軸樣式
        setWidth: 182,
        // 設定寬度
        setHeight: 422.5 // 設定高度

    });
    $('.re_right').mCustomScrollbar({
        theme: "dark",
        // 設定捲軸樣式
        setWidth: 262,
        // 設定寬度
        setHeight: 422.5 // 設定高度

    });
}

function loadTemplates() {
    let templates = [
        ['id_templ_slot_app_menu_hamburger', 'app-menu-hamburger.html'],
        ['id_templ_slot_app_menu', 'app-menu.html'],
        ['id_templ_slot_popups', 'popups.html']
    ];
    templates.forEach(elem => {
        try {
            let slot = document.getElementById(elem[0]);
            if (slot) {
                let templ = fs.readFileSync(getTemplateFilePath(elem[1])).toString();
                slot.outerHTML = templ;
            }
        } catch (error) {
            // Ignore errors
        }
    })
}

export function initMenu(): void {
    // Since on Mac we're showing the OS native titlebar, hide the
    // window minimize & close buttons.
    if (process.platform == 'darwin') {
        $('header div.rightbutton').addClass('is-hidden');
    }
    loadTemplates();
    updateProductVersion();
    setupUpdateMechanism();

    $(document).on('mouseenter', '.right_menu , .menu_list', function () {
        if (!isPopupActive()) {
            $('.menu_list').addClass('hover');
        }
    });
    $(document).on('mouseleave', '.headerbottom , .menu_list', function () {
        $('.menu_list').removeClass('hover');
    });
    $(document).on('mouseenter', '.h_content.app , .app_list , .removeapp', function () {
        if (!isPopupActive()) {
            $('.app_list').addClass('hover');
            $('.listcover').addClass('hover');
        }
    });
    $(document).on('mouseenter', '.app_list_more', function () {
        $('.app_list_more').addClass('hover');
        $('.listcover').addClass('hover');
    });
    $(document).on('mouseleave', '.headerbottom , .app_list , .app_list_more', function () {
        $('.app_list').removeClass('hover');
        $('.app_list_more').removeClass('hover');
        $('.listcover').removeClass('hover');
    });
    // $(document).on('mouseleave', '.headerbottom , .app_list', function () {
    //     $('.app_list').removeClass('hover');
    //     $('.listcover').removeClass('hover');
    // });
    if ($('footer .footercontent .name').height() > 30) {
        $('footer .footercontent .name').addClass('padd_height');
    }

    var scroll_w = $('#scroll_w').width();
    $('.scrollbar').mCustomScrollbar({
        theme: "dark",
        // 設定捲軸樣式
        setWidth: scroll_w,
        // 設定寬度
        setHeight: 768 - 109.5 // 設定高度

    });
    $('.re_left').mCustomScrollbar({
        theme: "dark",
        // 設定捲軸樣式
        setWidth: 182,
        // 設定寬度
        setHeight: 422.5 // 設定高度

    });
    $('.re_right').mCustomScrollbar({
        theme: "dark",
        // 設定捲軸樣式
        setWidth: 262,
        // 設定寬度
        setHeight: 422.5 // 設定高度

    });
    $('.regulate_center.scroll').mCustomScrollbar({
        theme: "dark",
        // 設定捲軸樣式
        setWidth: 445,
        // 設定寬度
        setHeight: 422.5 // 設定高度

    });
}


export function setupGlobalUIHandlers(): void
{
    // Application window titlebar close button
    $('.close').on('click', () => {
        window.close();
        if (process.platform !== 'darwin') app.quit()
    });
    // Application window titlebar minimize button
    $('.narrow').on('click', () => {
        remote.BrowserWindow.getFocusedWindow().minimize();
    });
    // Handle's popup's 'Close'/'Cancel' button to close the popup
    // and the underlying 'modal' coverbox.
    $(document).on('click', '.button.cancel', function (event) {
        hideCoverBox();
        hideAsyncPopup(event, false, new UserCancellation());
    });
    // Visit www.kensington.com (actually, opens browser with the
    // target element's href value)
    $(".visit-kensington-web").on("click", (ev) => {
        ev.preventDefault();
        shell.openExternal($(ev.target).attr("href"));
    });
    // Update popup activator
    $("#id_update_available").on('click', (event) => {
        showPopup('id_popup_updateapp');
    });
    // About popup's 'Check For Updates' handler
    $("#id_popup_about_check_for_updates").click((event) => {
        // Tell main process to check for updates. It will generate an event
        // handler for which will reflect the status in UI appropriately.
        let $checkForUpdatesBtn = $("#id_popup_about_check_for_updates");
        if ($checkForUpdatesBtn.data('action') == 'check') {
            ipcRenderer.send('check-for-update');
            setUpdateStatusMessage(loadString('Checking for updates'));
        } else {
            hidePopup('id_popup_about');
            showPopup('id_popup_updateapp');
        }
    });
    // Update popup's 'Download' button handler
    $("#id_popup_updateapp_download").click((event) => {
        $("#id_popup_updateapp").removeClass('click');
        let packageInfo = $('#id_popup_updateapp').data('package-info');
        //console.log('Update package info:', packageInfo);
        ipcRenderer.send('download-update', packageInfo);
        setDownloadProgressStatus(0, 0, 0);
        showPopup('id_popup_download');
    });
    // Download popup's 'Cancel' handler
    $("#id_popup_download_cancel").click((event) => {
        ipcRenderer.send('cancel-download-update');
    });
    // About popup activator
    $("#id_about").click(event => {
        showPopup('id_popup_about');
    });
    $("#id_language").click(event => {
        let pl = getPreferredLanguage();
        if (pl == '') {
            pl = 'sys';
        }
        $("#id_select_language").val(pl);
        showPopup("id_popup_language");
    })
    $("#id_change_language").click(event => {
        let pl = $("#id_select_language").val().toString();
        console.log('Selected language:', pl);
        setPreferredLanguage(pl);
        hidePopup("id_popup_language");

        let windows = remote.BrowserWindow.getAllWindows();
        let mainWindow = windows && windows.length > 0 ? windows[0] : null;
        if (mainWindow) {
            mainWindow.reload();
        }
    });
}

/**
 *
 * @param numberOfBytes Number of bytes to humanize
 */
function humanizeSize(numberOfBytes: number): string {
    let sizeUnit = 'B'; // Bytes
    if (numberOfBytes > 1024*1024) {
        numberOfBytes = Math.floor((numberOfBytes/(1024*1024)));
        sizeUnit = 'MB';
    } else if (numberOfBytes > 1024) {
        numberOfBytes = Math.floor((numberOfBytes/(1024)));
        sizeUnit = 'KB';
    }
    return `${numberOfBytes}${sizeUnit}`;
}

/**
 *
 * @param total Total number of bytes for the download
 * @param received Received bytes
 * @param progress Program, in the range 0..1
 */
function setDownloadProgressStatus(total: number, received: number, progress: number) {
    // humanize the size values
    if (total > 0) {
        $("#id_popup_download_status_size").text(`${humanizeSize(received)} / ${humanizeSize(total)}`);
    } else {
        $("#id_popup_download_status_size").text('0 / 0');
    }
    $("#id_popup_download_status_progress").attr({style: `width:${progress*100}%;`})
}

export function updateProductVersion() {
    let version = `v${PRODUCT_VERSION.major}.${PRODUCT_VERSION.minor}.${PRODUCT_VERSION.patch}`;
    if (PRODUCT_VERSION.hasOwnProperty('beta') && PRODUCT_VERSION.beta) {
        version += `βeta`
    }
    $("h6.version").text(version);
}

/**
 * Update UI elements to reflect no updates available.
 */
function handleNoUpdatesAvailable() {
    // Update About box status text anyway
    setUpdateStatusMessage(loadString('KensingtonWorks is up to date'));
    // set Update About box button text to 'Check For Updates'
    let $checkForUpdatesBtn = $("#id_popup_about_check_for_updates");
    $checkForUpdatesBtn.attr('value', loadString('Check for Updates'))
    $checkForUpdatesBtn.data('action', 'check');

    // Hide the 'Update Available' menu item
    let $updateMenuItem = $("#id_update_available");
    $updateMenuItem.addClass('is-hidden');
    // Remove the dot from the hamburger menu
    $($("#id_right_menu svg circle")[0]).remove();
}

/**
 * Handle updates-available received.
 */
function handleUpdatesAvailable(status: UpdateStatus) {
    if (!status.available) {
        return;
    }

    setUpdateStatusMessage(loadString('update_available'));
    // set Update About box button text to 'Go to Updates'
    let $checkForUpdatesBtn = $("#id_popup_about_check_for_updates");
    $checkForUpdatesBtn.attr('value', loadString('btn_GotoUpdates'))
    $checkForUpdatesBtn.data('action', 'download');

    // update is available
    let $updateMenuItem = $("#id_update_available");
    // Enable the Update Available menu item (it's hidden by default)
    $updateMenuItem.removeClass('is-hidden');
    // add <i class="listicon be-icon be-icon-dot"></i> to the menu item
    $updateMenuItem.append($('<i class="listicon be-icon be-icon-dot"></i>'));
    // add the dot to the menu item
    let menuHtml = $("#id_right_menu svg").html();
    menuHtml += '<circle style="opacity:1;" fill="#BB3030" cx="8.847" cy="14.08" r="8.741"></circle>';
    $("#id_right_menu svg").html(menuHtml);

    //  1. Update the 'update' popup to reflect availability
    $("#id_popup_updateapp_version").text(
        loadString('update_info_version', {version: status.version}));
    $("#id_popup_updateapp_published").text(
        loadString('update_info_published', {published_on: status.pubishedDate}));
    $("#id_popup_updateapp_size").text(
        loadString('update_info_size', {size: humanizeSize(status.size)}));
    $("#id_popup_updateapp").data('package-info', status);

    //  2. Set correct data attributes with updated package details
    $("#id_popup_updateapp div.no_update_available").addClass('is-hidden');
    $("#id_popup_updateapp div.update_available").removeClass('is-hidden');
}

function setUpdateStatusMessage(msg: string) {
    let $statusMsg = $("#id_popup_about_update_status");
    $statusMsg.text(msg);
    $statusMsg.removeClass('is-hidden');
}

/**
 * To be called from renderer
 */
export function setupUpdateMechanism() {

    ipcRenderer.on("no-update-available", handleNoUpdatesAvailable);

    // Setup handler to paint the menu item to reflect update availability
    ipcRenderer.on('update-available', (event: any, status: UpdateStatus) => {
        //console.log('Update available, status:', status);
        // Check-for-updates generated by program during startup
        handleUpdatesAvailable(status);
    });

    // Main process notifies that download of the update package has started
    ipcRenderer.on("update-download-started", (event: any, item: any) => {
        //console.log('update-download-started, item', item);
        $("#id_popup_download").data('download-item', item);
    });

    ipcRenderer.on("update-download-finished", (event: any) => {
        // { filename: <>, savePath:<>, totalBytes: <> }
        let downloadItem: any = $("#id_popup_download").data('download-item');
        if (downloadItem) {
            console.log('update-download-finished, shellExecute: ',
                downloadItem.savePath);
            $('.coverbox').removeClass('click');
            $("#id_popup_download").removeClass('click');
            ipcRenderer.send('open-update-package', downloadItem.savePath);
            $("#id_popup_download").data('download-item', null);

            // Since we downloaded and started the update process,
            // mark UI elements as no updates available
            handleNoUpdatesAvailable();
        }
    });

    ipcRenderer.on('update-download-cancelled', () => {
        $('#id_popup_download').removeClass('click');
        $('.coverbox').removeClass('click');
    });

    ipcRenderer.on('update-download-progress', (event: any, args: any) => {
        //console.log('Update progress:', args.progress);
        // { filename: <>, savePath:<>, totalBytes: <> }
        let downloadItem: any = $("#id_popup_download").data('download-item');
        if (downloadItem) {
            let total = downloadItem.totalBytes;
            let received = total * args.progress;
            setDownloadProgressStatus(total, received, args.progress);
        }
    });

    // Default check-for-updates during program startup
    ipcRenderer.send('check-for-update');
}