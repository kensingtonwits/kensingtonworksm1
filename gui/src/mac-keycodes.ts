/**
 * Define a table that maps HTML keycodes to Apple KeyCodes
 */

const WIN_KEYCODES: any = {
    "Digit1": 	49,
    "Digit2": 	50,
    "Digit3": 	51,
    "Digit4": 	52,
    "Digit5": 	53,
    "Digit6": 	54,
    "Digit7": 	55,
    "Digit8": 	56,
    "Digit9": 	57,
    "Digit0": 	48,
    "KeyA": 	65,
    "KeyB": 	66,
    "KeyC": 	67,
    "KeyD": 	68,
    "KeyE": 	69,
    "KeyF": 	70,
    "KeyG": 	71,
    "KeyH": 	72,
    "KeyI": 	73,
    "KeyJ": 	74,
    "KeyK": 	75,
    "KeyL": 	76,
    "KeyM": 	77,
    "KeyN": 	78,
    "KeyO": 	79,
    "KeyP": 	80,
    "KeyQ": 	81,
    "KeyR": 	82,
    "KeyS": 	83,
    "KeyT": 	84,
    "KeyU": 	85,
    "KeyV": 	86,
    "KeyW": 	87,
    "KeyX": 	88,
    "KeyY": 	89,
    "KeyZ": 	90,
    "Comma": 	188,
    "Period": 	190,
    "Semicolon": 	186,
    "Quote": 	222,
    "BracketLeft": 	219,
    "BracketRight": 	221,
    "Backquote": 	192,
    "Backslash": 	220,
    "Minus": 	189,
    "Equal": 	187,
    // "IntlRo": 	189,
    // "IntlYen": 	0,
    // "ContextMenu": 	93,
    "Enter": 	13,
    "Space": 32,
    "Backspace": 8,
    "Tab": 	9,
    "Delete": 	46,
    "End": 	35,
    "Home": 	36,
    "Insert": 	45,
    "PageDown": 	34,
    "PageUp": 	33,
    "ArrowDown": 	40,
    "ArrowLeft": 	37,
    "ArrowRight": 	39,
    "ArrowUp": 	38,
    "Escape": 	27,
    "PrintScreen": 	44,
    "ScrollLock": 	145,
    "Pause": 19,
    "Ctrl": 17,
    "Win": 91,
    "Win ": 92,
    "Shift": 16,
    "Alt": 12,
    "F1": 	112,
    "F2": 	113,
    "F3": 	114,
    "F4": 	115,
    "F5": 	116,
    "F6": 	117,
    "F7": 	118,
    "F8": 	119,
    "F9": 	120,
    "F10": 	121,
    "F11": 122,
    "F12": 123,
    "F13": 124,
    "F14": 125,
    "F15": 126,
    "F16": 127,
    "F17": 128,
    "F18": 129,
    "F19": 130,
    "F20": 131,
    "F21": 132,
    "F22": 133,
    "F23": 134,
    "F24": 135,
    "Numpad0": 	96,
    "Numpad1": 	97,
    "Numpad2": 	98,
    "Numpad3": 	99,
    "Numpad4": 	100,
    "Numpad5": 	101,
    "Numpad6": 	102,
    "Numpad7": 	103,
    "Numpad8": 	104,
    "Numpad9": 	105,
    "NumpadAdd": 	107,
    "NumpadDecimal": 	110,
    "NumpadDivide": 	111,
    "NumpadEnter": 	13,
    "NumpadEqual": 	61,
    "NumpadMultiply": 	106,
    "NumpadSubtract": 	109
};

// Mac Keycodes indexed by HTML key.code
const MAC_KEYCODES: any = {
    "KeyA":	0x00,
    "KeyS":	0x01,
    "KeyD":	0x02,
    "KeyF":	0x03,
    "KeyH":	0x04,
    "KeyG":	0x05,
    "KeyZ":	0x06,
    "KeyX":	0x07,
    "KeyC":	0x08,
    "KeyV":	0x09,
    "KeyB":	0x0B,
    "KeyQ":	0x0C,
    "KeyW":	0x0D,
    "KeyE":	0x0E,
    "KeyR":	0x0F,
    "KeyY":	0x10,
    "KeyT":	0x11,
    "Digit1":	0x12,
    "Digit2":	0x13,
    "Digit3":	0x14,
    "Digit4":	0x15,
    "Digit6":	0x16,
    "Digit5":	0x17,
    "Equal":	0x18,
    "Digit9":	0x19,
    "Digit7":	0x1A,
    "Minus":	0x1B,
    "Digit8":	0x1C,
    "Digit0":	0x1D,
    "BracketRight":	0x1E,
    "KeyO":	0x1F,
    "KeyU":	0x20,
    "BracketLeft":	0x21,
    "KeyI":	0x22,
    "KeyP":	0x23,
    "KeyL":	0x25,
    "KeyJ":	0x26,
    "Quote":	0x27,
    "KeyK":	0x28,
    "Semicolon":	0x29,
    "Backslash":	0x2A,
    "Comma":	0x2B,
    "Slash":	0x2C,
    "KeyN":	0x2D,
    "KeyM":	0x2E,
    "Period":	0x2F,
    "Backquote":	0x32,
    "NumpadDecimal":	0x41,
    "NumpadMultiply":	0x43,
    "NumpadAdd":	0x45,
    "NumpadClear":	0x47,
    "NumpadDivide":	0x4B,
    "NumpadEnter":	0x4C,
    "NumpadSubtract":	0x4E,
    "NumpadEqual":	0x51,
    "Numpad0":	0x52,
    "Numpad1":	0x53,
    "Numpad2":	0x54,
    "Numpad3":	0x55,
    "Numpad4":	0x56,
    "Numpad5":	0x57,
    "Numpad6":	0x58,
    "Numpad7":	0x59,
    "Numpad8":	0x5B,
    "Numpad9":	0x5C,
    "Return":	0x24,
    "Tab":	0x30,
    "Space":	0x31,
    "Delete": 0x75,
    "Backspace": 0x33,
    "Escape":	0x35,
    // "Meta":	0x37,
    // "Shift":	0x38,
    // "CapsLock":	0x39,
    // "Alt":	0x3A,
    // "Control":	0x3B,
    // "MetaRight":	0x36,
    // "ShiftRight":	0x3C,
    // "AltRight":	0x3D,
    // "ControlRight":	0x3E,
    // "Function":	0x3F,
    "F17":	0x40,
    "VolumeUp":	0x48,
    "VolumeDown":	0x49,
    "Mute":	0x4A,
    "F18":	0x4F,
    "F19":	0x50,
    "F20":	0x5A,
    "F5":	0x60,
    "F6":	0x61,
    "F7":	0x62,
    "F3":	0x63,
    "F8":	0x64,
    "F9":	0x65,
    "F11":	0x67,
    "F13":	0x69,
    "F16":	0x6A,
    "F14":	0x6B,
    "F10":	0x6D,
    "F12":	0x6F,
    "F15":	0x71,
    "Help":	0x72,
    "Home":	0x73,
    "PageUp":	0x74,
    "ForwardDelete":	0x75,
    "F4":	0x76,
    "End":	0x77,
    "F2":	0x78,
    "PageDown":	0x79,
    "F1":	0x7A,
    "ArrowLeft":	0x7B,
    "ArrowRight": 0x7C,
    "ArrowDown": 0x7D,
    "ArrowUp": 0x7E,
    "DownArrow":	0x7D,
    "UpArrow":	0x7E
};

const HTMLCODES_TO_CHAR: any = {
    'Digit1': '1',
    'Digit2': '2',
    'Digit3': '3',
    'Digit4': '4',
    'Digit5': '5',
    'Digit6': '6',
    'Digit7': '7',
    'Digit8': '8',
    'Digit9': '9',
    'Digit0': '0',
    'KeyA': 'A',
    'KeyB': 'B',
    'KeyC': 'C',
    'KeyD': 'D',
    'KeyE': 'E',
    'KeyF': 'F',
    'KeyG': 'G',
    'KeyH': 'H',
    'KeyI': 'I',
    'KeyJ': 'J',
    'KeyK': 'K',
    'KeyL': 'L',
    'KeyM': 'M',
    'KeyN': 'N',
    'KeyO': 'O',
    'KeyP': 'P',
    'KeyQ': 'Q',
    'KeyR': 'R',
    'KeyS': 'S',
    'KeyT': 'T',
    'KeyU': 'U',
    'KeyV': 'V',
    'KeyW': 'W',
    'KeyX': 'X',
    'KeyY': 'Y',
    'KeyZ': 'Z',
    "Comma": 	",",
    "Period": 	".",
    "Semicolon": 	";",
    "Quote": 	"'",
    "BracketLeft": 	"{",
    "BracketRight": 	"}",
    "Backquote": 	"`",
    "Backslash": 	"\\",
    "Minus": 	"-",
    "Equal": 	"=",
    "IntlRo": 	"",
    "IntlYen": 	"",
    "ContextMenu": 	"",
    "Enter": 	"",
    "Space": 	"Space",
    "Tab": 	"Tab",
    "Delete": 	"Del",
    "End": 	"",
    "Home": 	"",
    "Insert": 	"",
    "PageDown": 	"",
    "PageUp": 	"",
    "ArrowDown": 	"",
    "ArrowLeft": 	"",
    "ArrowRight": 	"",
    "ArrowUp": 	"",
    "Escape": 	"",
    "PrintScreen": 	"",
    "ScrollLock": 	"",
    "Pause": 	"",
    'F1': 'F1',
    'F2': 'F2',
    'F3': 'F3',
    'F4': 'F4',
    'F5': 'F5',
    'F6': 'F6',
    'F7': 'F7',
    'F8': 'F8',
    'F9': 'F9',
    "F10": 	'F10',
    "F11": 'F11',
    "F12": 'F12',
    "F13": 'F13',
    "F14": 'F14',
    "F15": 'F15',
    "F16": 'F16',
    "F17": 'F17',
    "F18": 'F18',
    "F19": 'F19',
    "F20": 'F20',
    "F21": 'F21',
    "F22": 'F22',
    "F23": 'F23',
    "F24": 'F24',
    "Numpad0": 	'',
    "Numpad1": 	'',
    "Numpad2": 	'',
    "Numpad3": 	'',
    "Numpad4": 	'',
    "Numpad5": 	'',
    "Numpad6": 	'',
    "Numpad7": 	'',
    "Numpad8": 	'',
    "Numpad9": 	'',
    "NumpadAdd": 	'+',
    "NumpadDecimal": 	'.',
    "NumpadDivide": 	'/',
    "NumpadEnter": 	'',
    "NumpadEqual": 	'=',
    "NumpadMultiply": 	'*',
    "NumpadSubtract": 	'-'
};

/**
  * Returns the Apple keyboard keyCode for the given HTML KeyCode
  * 
  * @param htmlCode string string code from browser
  * @param htmlKeyCode number keyCode which from browser
  */
export function getKeyCodeFromHTMLCode(htmlCode: string, htmlKeyCode: number) : number {
    //console.log('getKeyCodeFromHTMLCode - htmlCode:', htmlCode, ", htmlKeyCode:", htmlKeyCode);
    if (process.platform == 'darwin') {
        if (MAC_KEYCODES.hasOwnProperty(htmlCode)) {
            return MAC_KEYCODES[htmlCode];
        }
        return -1; // -1 indicates not found! ignore the key
    }

    // For other platforms, return the htmlKeyCode
    return htmlKeyCode;
}

/**
 * Returns HTML keycode for the given Apple KeyCode
 *
 * @param appleKeyCode Apple KeyCode
 */
export function getHTMLCodeFromKeyCode(keyCode: number) : string {
    //console.log('getHTMLCodeFromKeyCode - keyCode:', keyCode);
    if (process.platform == 'darwin') {
        for (let htmlCode in MAC_KEYCODES) {
            if (MAC_KEYCODES[htmlCode] == keyCode) {
                return htmlCode;
            }
        }
    } else {
        for (let htmlCode in WIN_KEYCODES) {
            if (WIN_KEYCODES[htmlCode] == keyCode) {
                return htmlCode;
            }
        }
    }
    return '';
}

export function isValidShortcutKey(htmlCode: string) {
    if (process.platform == 'darwin') {
        if (MAC_KEYCODES.hasOwnProperty(htmlCode)) {
            return true;
        }
    } else {
        if (WIN_KEYCODES.hasOwnProperty(htmlCode)) {
            return true;
        }
   }
   return false;
}