import { remote, ipcRenderer } from "electron";
import * as $ from "jquery";
import { initI18n } from "./i18n";
import "foundation-sites";
var Swiper = require('swiper');
import * as mscroll from "malihu-custom-scrollbar-plugin";
import {
    initDimensions, initMenu, initScrollbar, setupGlobalUIHandlers, updateProductVersion
} from "./all";
import {
    getUrlParam, SupportedDevice, getSupportedDeviceInfo, localizeStrings, initI18Funcs, loadString
} from "./helper";
import { TrackballDevice } from "./tbw";
import { initBackupRestoreHandlers } from "./BackupRestoreHandlers";

let _di: SupportedDevice = null;
let _t: any = null; // i18next.t function


/**
 * Initializes switper for the device's tutor specification in
 * supported-devices.json.
 */
function loadDeviceTutorImages() {

    // Expected to have $text & $index variables
    let CONNECTION_MODE_TEMPL = $("#id_template_connection_mode_buttons").html();
    // Variables: $index
    let SLIDE_WRAPPER_TEMPL = $("#id_template_connection_mode_slide_wrapper").html();
    // Variables: $image & $hint
    let SLIDE_TEMPL = $("#id_template_connection_mode_slide").html();

    let index = 1;
    for (const mode in _di.tutor) {
        let modeDetails = _di.tutor[mode];
        let buttonHtml = CONNECTION_MODE_TEMPL
            .replace(/\$family/g, _di.family)
            .replace(/\$id/g, _di.id.toString())
            .replace(/\$text/g, loadString(modeDetails.text))
            .replace(/\$index/g, index.toString());
        $("#id_connection_mode_buttons").append(buttonHtml);

        let slideWrapperHtml = SLIDE_WRAPPER_TEMPL
            .replace(/\$index/g, index.toString());
        let $slideWrapper = $(slideWrapperHtml);
        modeDetails['slides'].forEach((slide: any) => {
            let slideHtml = SLIDE_TEMPL
            .replace(/\$family/g, _di.family)
            .replace(/\$id/g, _di.id.toString())
            .replace(/\$image/g, slide.image)
            .replace(/\$hint/g, slide.hint.length ? loadString(slide.hint) : "");
            $slideWrapper.find('div.swiper-wrapper').append(slideHtml);
        })
        $("section.device_area").append($slideWrapper);
        index++;
    }
}

/**
 * Initialize the swiper for the tutor images.
 */
function initSwiper() {

    // Setup Swiper for all of the connection modes. We just cycle through
    // 'n' number of connection modes until we don't find a corresponding
    // <div class="selectedcontent'n'"..> element.
    for (let index = 1; index < 10; index++) {
        if ($(`.selectedcontent${index}`).length == 0) {
            break;
        }

        var swiper = new Swiper(`.selectedcontent${index}`, {
            speed: 1500,
            autoplay: false,
            slidesPerView: 1,
            centeredSlides: true,
            loop: false, //循環撥放
            spaceBetween: 0,
            slidesPerGroup: 1,
            autoHeight: true,
            watchOverflow: true,
            simulateTouch: true,
            effect: "slide", //Could be "slide", "fade", "cube", "coverflow" or "flip"
            pagination: {
                el: `.selectedcontent${index} .swiper-pagination`,
                clickable: true,
            },
            navigation: {
                nextEl: `.selectedcontent${index} .swiper_next`,
                prevEl: `.selectedcontent${index} .swiper_prev`,
            }
        });
    }

    if ($('.selected_tabcell .scrolltab').length == 1) {
        $('.selected_tabcell').addClass('one');
    }
    else if ($('.selected_tabcell .scrolltab').length == 2) {
        $('.selected_tabcell').addClass('two');
    }
    else if ($('.selected_tabcell .scrolltab').length == 3) {
        $('.selected_tabcell').addClass('three');
    }
    $(document).on('click', '.scrolltab', function () {
        var click = $(this).data('click');
        $('.scrolltab').removeClass('hover');
        $(this).addClass('hover');
        $('.selectedcontent').removeClass('hover');
        $('.selectedcontent[data-click=' + click + ']').addClass('hover');
    });

    // Select the first connection mode. Has to be done from a timer as
    // Swiper needs to initialize itself first.
    setTimeout(() => {
        if ($("#id_connection_mode_buttons .scrolltab").length > 0) {
            $($("#id_connection_mode_buttons .scrolltab")[0]).click();
        }
        /**
         * Let the user's decide if they want to hide this solitary button
        if ($("#id_connection_mode_buttons .scrolltab").length == 1) {
            $("#id_connection_mode_buttons").hide();
        }
        */
    }, 200);
}

function initPage() {
    $(document).foundation(); //IE old version tip
    mscroll($);
    initDimensions();
    initMenu();
    setupGlobalUIHandlers();
    updateProductVersion();
    initBackupRestoreHandlers();

    let productid = parseInt(getUrlParam("device", 0));
    _di = getSupportedDeviceInfo(productid);

    loadString('connect_your_device_now', {devicename:_di.name});
    $("h5.devicetitle").html(`Connect your ${_di.name} now!`);

    loadDeviceTutorImages();

    initSwiper();

    // wait for the device to arrive
    // Setup handler for new device arrival notification sent by main process.
    ipcRenderer.on('device-arrival', (event: any, device: TrackballDevice) => {
        //console.log('device-arrival message');
        if (device._id == _di.id && device._present) {
            // the device we're waiting for has arrived!
            //console.log(`Device ${_di.name} arrived, going  to its config page`);
            ipcRenderer.send("goto-device-config", device._id);
        }
    });

    ipcRenderer.on('device-status-change', (event: any, device: TrackballDevice) => {
        //console.log('device-arrival message');
        if (device._id == _di.id && device._present) {
            // the device we're waiting for has arrived!
            //console.log(`Device ${_di.name} connected, going to its config page`);
            ipcRenderer.send("goto-device-config", device._id);
        }
    });

    // get all connected devices
    let kwConfig = remote.getGlobal('kwConfig');
    if (kwConfig) {
        let devices: Map<number, TrackballDevice> = kwConfig.devices;
        //console.log('Connected devices: ', devices.size);
        devices.forEach((device, key, map) => {
            if (device._id == _di.id && device._present) {
                //console.log(`Device ${_di.name} arrived, going back home`);
                ipcRenderer.send("goto-device-config", device._id);
            }
        });
    } else {
        console.warn('Error retrieving shared object from global');
    }

}

ipcRenderer.on('helper-missing', (event: any) => {
    //console.log('Helper has gone missing!')
    ipcRenderer.send("goto-home");
});
  
$(document).ready(() => {
    initI18n()
    .then((t) => {
      // store the translation function globally so that we can use
      // whenever we need it.
      initI18Funcs(t);

      try {
        initPage();
        localizeStrings();
      } catch (error) {
          console.log('init error: ', error);
      }

    }).catch((err) => {
        console.log("Error initializing i18next library, err: ", err);
    });
})