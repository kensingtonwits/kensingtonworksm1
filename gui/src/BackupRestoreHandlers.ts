/**
 * Handlers for cloud related UI widgets.
 *
 * To use, call initCloudUIHandlers() from page init routine. Obviously, has
 * to be called when document is fully loaded.
 */
import * as $ from "jquery";
import { remote, ipcRenderer } from "electron";
import { TrackballWorks } from "./tbw";
import { loadString } from "./helper";
import {
    getCloudBackupProvider, getCloudUserInfo, backupToCloud,
    restoreFromCloud, eraseCloudBackupProvider, setCloudBackupProvider, getAutoSaveChangesToCloud, setAutoSaveChangesToCloud
} from "./BackupRestore";
import {
    showCoverBox, showAlertMessage, showBusyPopup, showPopup,
    hideCoverBox, hideBusyPopup, hidePopup, showPopupAsync, getAsyncPopupPromiseCallbacks
} from "./popups";
import * as fs from "fs";
import { googleSignInWithPopup } from "./gdrive";
import { microsoftSignInWithPopup } from "./onedrive";
import { UserCancellation } from "./errors";

/**
 * If cloud binding exists show the 'boundok' popup. Else
 * show the 'bind_to_cloud' popup.
 */
function bindToCloudToolbarButtonHandler() {
    let cloudBackupProvider = getCloudBackupProvider();
    if (!cloudBackupProvider) {
        showBindToCloudPopup();
    } else {
        showCloudBindingStatus();
    }
}

/**
 * Show the bind to cloud popup, listing Google Drive & Microsoft OneDrive
 */
function showBindToCloudPopup() {
    showPopup('id_popup_bind_to_cloud');
}

/**
 * Shows the current cloud binding status popup
 */
function showCloudBindingStatus() {
    let cloudBackupProvider = getCloudBackupProvider();
    let cloudUserInfo = getCloudUserInfo();
    let $boundOkPopup = $("#id_popup_boundok");
    $boundOkPopup.find('div.drive_allwidth').addClass('is-hidden');
    $boundOkPopup.find(`div.drive_allwidth.${cloudBackupProvider}`).removeClass('is-hidden');
    $boundOkPopup.find(`div.drive_allwidth.${cloudBackupProvider} div.email`).text(cloudUserInfo.email);
    // restore current auto save status
    let autoSave = getAutoSaveChangesToCloud();
    $('#color-input-red').prop('checked', autoSave);

    showPopup('id_popup_boundok');
}

/**
 * Auto save settings to cloud
 */
function handlerSyncSettingsToCloud() {
    let autoSave = $('#color-input-red').prop('checked');
    setAutoSaveChangesToCloud(autoSave);
}

/**
 *
 */
async function showBindToCloudPopupAsync() {
    return new Promise((resolve, reject) => {
        showPopupAsync('id_popup_bind_to_cloud', true, resolve, reject);
    });
}

/**
 * Update the text message in the confirmation popup to include the preferred
 * cloud backup provider names & account.
 */
function showUnbindConfirmationPopup() {
    let $unbindPopup = $($("#id_popup_unbind")[0]);
    let cloudBackupProvider = getCloudBackupProvider();
    let serviceProvider = cloudBackupProvider == "google" ? 'Google Drive' : 'Microsoft OneDrive';
    loadString('unbind_confirm_prompt', {service_provider: serviceProvider});
    $unbindPopup.find('div.popupsubtitle').text(loadString('unbind_confirm_prompt', {service_provider: serviceProvider}));
    $unbindPopup.find('div.popupword').text(loadString('unbind_confirm_message', {service_provider: serviceProvider}));
    hidePopup('id_popup_boundok');
    showPopup('id_popup_unbind');
}

/**
 * Updates the application toolbar with current cloud account binding details.
 */
function updateCloudUserInfoBar() {
    let userInfo = getCloudUserInfo();
    if (userInfo) {
        $("#id_bind_to_cloud p").text(userInfo.email);
        $("#id_bind_to_cloud p").attr('title', userInfo.email)
    } else {
        $("#id_bind_to_cloud p").text(loadString('bind_to_cloud_title'));
        $("#id_bind_to_cloud p").attr('title', '');
    }
}

/**
 * Updates the Backup/Restore popups such that the cloud part of the popup
 * is set to either Unbound/Google/Microsoft depending on current cloud binding
 * status.
 */
function updateBackupRestorePopupForCloudBinding(popupId: string) {
    let $popup = $($(`#${popupId}`)[0]);
    let provider = getCloudBackupProvider();
    let userInfo = getCloudUserInfo();
    // hide everything first
    $popup.find('div.frombox.unbound').addClass('is-hidden');
    $popup.find('div.frombox.google').addClass('is-hidden');
    $popup.find('div.frombox.microsoft').addClass('is-hidden');
    if (userInfo) {
        $popup.find(`div.frombox.${provider}`).removeClass('is-hidden');
        $popup.find(`div.frombox.${provider} div.email`).text(userInfo.email);
    } else {
        $popup.find('div.frombox.unbound').removeClass('is-hidden');
    }
}

/**
 * Signin to selected could provider and save the relevant info to offline
 * storage.
 *
 * @param provider google|microsoft
 */
function handlerSignInToProvider(provider: 'google'|'microsoft') {
    let promise = getAsyncPopupPromiseCallbacks('id_popup_bind_to_cloud');
    hidePopup('id_popup_bind_to_cloud');
    showBusyPopup();
    signInToCloudAsync(provider)
    .then(() => {
        // Update header bar to reflect current signed in user account info
        setCloudBackupProvider(provider);
        updateCloudUserInfoBar();
        if (promise) {
            promise.resolve();
        } else {
            showCloudBindingStatus();
        }
    }).catch((err: Error) => {
        if (promise) {
            // propagate error to higher level await routine
            // which would catch it and display the error
            promise.reject(err);
        } else {
            if (!(err instanceof UserCancellation)) {
                showAlertMessage(
                    loadString("Error"),
                    loadString('error_binding_to_cloud_provider'));
            }
        }
    }).finally(() => {
        hideBusyPopup();
    });
}

/**
 * Shows the specified provider signin popup and waits for user to complete
 * the signin.
 *
 * @param provider 'google' or 'microsoft'
 * @returns Promise<token> - promise with the access token upon valid
 * signin. Upon reject, and Error object which contains the error information.
 */
async function signInToCloudAsync(provider: 'google'|'microsoft') {
    if (provider == 'google') {
        await googleSignInWithPopup();
    } else if (provider == 'microsoft') {
        await microsoftSignInWithPopup();
    }
}

/**
 * Backup the settings to a file
 */
function backupToFileHandler() {
    hidePopup('id_popup_backup');

    TrackballWorks.getConfig()
    .then(settings => {
        remote.dialog.showSaveDialog(remote.getCurrentWindow(), {
            defaultPath: remote.app.getPath('documents'),
            title: 'KensingtonWorks',
            filters: [
                { name: loadString('Settings Backup Files'), extensions: ['json'] },
                { name: loadString('All Files'), extensions: ['*'] }
            ]
        })
        .then(({canceled, filePath}) => {
            if (!canceled && filePath) {
                try {
                    //console.log('Save as:', file);
                    fs.writeFileSync(
                        filePath,
                        JSON.stringify(settings, null, 2));
                    showAlertMessage(
                        'KensingtonWorks',
                        loadString('settings_backed_up'),
                        '<i class="popupicon be-icon be-icon-file"></i>'
                        );
                } catch (error) {
                    showAlertMessage(
                        'KensingtonWorks',
                        loadString('error_backing_up_settings')
                        );
                }
            }
        });
    }).catch(err => {
        showAlertMessage(
            'KensingtonWorks',
            loadString('error_backing_up_settings')
            );
    }).finally(() => {
        hideCoverBox();
    });
}

/**
 *
 */
async function getCloudBackupProviderAsync() {
    let provider = getCloudBackupProvider();
    if (!provider) {
        await showBindToCloudPopupAsync();
    }
}

/**
 * Backup current settings to the selected could backup provider.
 * If a provider has not been chosen, transfers control to the binding popup.
 */
function backupToCloudHandler() {
    hidePopup('id_popup_backup');
    // let provider = getCloudBackupProvider();
    // if (!provider) {
    //     //showCloudBindingPopup();
    //     // TODO: will it come here after user selects a provider and signs in?
    //     return;
    // }

    getCloudBackupProviderAsync()
    .then(() => {
        showBusyPopup();
        return backupToCloud()
    }).then(() => {
        showAlertMessage(
            'KensingtonWorks',
            loadString('settings_backed_up'),
            '<i class="popupicon be-icon be-icon-cloud"></i>'
            );
    }).catch((err) => {
        if (!(err instanceof UserCancellation)) {
            showAlertMessage(
                'KensingtonWorks',
                loadString('error_backing_up_settings_to_cloud')
            );
        }
    }).finally(() => {
        hideBusyPopup();
        hideCoverBox();
    });
}

/**
 *
 * @param err Typically Javascript Error object.
 */
function handleRestoreError(err: any, cloudRestore: boolean) {
    // detect platform mismatch errors
    if (err['isAxiosError']
        && err.response.data['reason']
        && err.response.data['reason'] == "Platform mismatch") {
        showAlertMessage(
            'KensingtonWorks',
            loadString('error_restoring_settings_platform_mismatch')
        );
    } else {
        // skip over UserCancellation that can happen during cloud restore
        if (!(err instanceof UserCancellation)) {
            showAlertMessage(
                'KensingtonWorks',
                loadString(cloudRestore ?
                    'error_restoring_settings_from_cloud' : 'error_restoring_settings')
            );
        }
    }
}

/**
 * Restore settings from a PC file.
 */
function restoreFromFileHandler() {
    hidePopup('id_popup_restore');
    hideCoverBox();

    remote.dialog.showOpenDialog(remote.getCurrentWindow(), {
        defaultPath: remote.app.getPath('documents'),
        title: 'KensingtonWorks',
        properties: ['openFile'],
        filters: [
            { name: loadString('Settings Backup Files'), extensions: ['json'] },
            { name: loadString('All Files'), extensions: ['*'] }
        ]
    })
    .then(({canceled, filePaths}) => {
        if (!canceled && filePaths) {
            // TODO: plugin TrackballWorks.setConfig()
            //console.log('Restore from file:', file);
            try {
                let settings = JSON.parse(
                    fs.readFileSync(filePaths[0]).toString());
                TrackballWorks.setConfig(settings)
                .then(() => {
                    // Send a reresh-settings to main process, which will
                    // emit the same message to renderer process. We need
                    // this circuitous way of handling things as restoring
                    // of settings is done from an independent module because
                    // of the UI. If it were handled from the main process
                    // we wouldn't have need this.
                    ipcRenderer.send('refresh-settings');
                    showAlertMessage(
                        'KensingtonWorks',
                        loadString('settings_restored'),
                        '<i class="popupicon be-icon be-icon-file" > </i>'
                    );
                }).catch((err) => {
                    handleRestoreError(err, false);
                });
            } catch (error) {
                //console.log('Error parsing the JSON file:', error);
                showAlertMessage(
                    'KensingtonWorks',
                    loadString('error_restoring_settings')
                );
            }
        }
    });
}

/**
 * Restore settings from selected cloud provider
 */
function restoreFromCloudHandler() {
    hidePopup('id_popup_restore');
    // let provider = getCloudBackupProvider();
    // if (!provider) {
    //     //showCloudBindingPopup();
    //     return;
    // }
    getCloudBackupProviderAsync()
    .then(() => {
        // Note that coverbox is still visible
        showBusyPopup();
        return restoreFromCloud()
    }).then(() => {
        ipcRenderer.send('refresh-settings');
        showAlertMessage(
            'KensingtonWorks',
            loadString('settings_restored'),
            '<i class="popupicon be-icon be-icon-cloud"></i>'
        );
    }).catch(err => {
        //console.log('Error restoring settings from google:', err);
        handleRestoreError(err, true);
    }).finally(() => {
        hideBusyPopup();
        hideCoverBox();
    });
}

/**
 * Unbind from cloud - remove all cloud binding information from offline
 * settings.
 */
function handlerUnbindCloud() {
    hidePopup('id_popup_unbind');
    showBusyPopup();
    eraseCloudBackupProvider()
    .then(() => {
    }).catch(err => {
        // display error message popup
        showAlertMessage(loadString('Unbind Error'), loadString('Error unbinding from your cloud account'));
    }).finally(() => {
        hideBusyPopup();
        hideCoverBox();
        updateCloudUserInfoBar();
    })
}

/**
 * Sets up all the UI handlers for Backup/Restore related UI widgets.
 */
export function initBackupRestoreHandlers() {
    $("#id_bind_to_cloud").click((event) => {
        bindToCloudToolbarButtonHandler();
    });
    $("#id_popup_bind_to_cloud_google").click(event => {
        handlerSignInToProvider('google');
    });
    $("#id_popup_bind_to_cloud_microsoft").click(event => {
        handlerSignInToProvider('microsoft');
    });
    // Restore Setting popup activator
    $("#id_restore_setting").on('click', (event) => {
        showCoverBox();
        updateBackupRestorePopupForCloudBinding('id_popup_restore');
        showPopup('id_popup_restore');
    });
    // Backup Setting popup activator
    $("#id_backup_setting").on('click', (event) => {
        showCoverBox();
        updateBackupRestorePopupForCloudBinding('id_popup_backup');
        showPopup('id_popup_backup');
    });
    $("#id_popup_boundok_unbind").click(event => {
        event.preventDefault();
        showUnbindConfirmationPopup();
    });
    $("#id_unbind_cloud").click(event => {
        handlerUnbindCloud();
    });
    // Backup settings to a file
    $('#id_backup_to_file').click(event => {
        event.preventDefault();
        backupToFileHandler();
    });
    // Backup settings to Google Drive
    $("#id_popup_backup div.backup_to_cloud").click((event) => {
        event.preventDefault();
        backupToCloudHandler();
    });
    // Restore settings from file
    $('#id_restore_from_file').click(event => {
        event.preventDefault();
        restoreFromFileHandler();
    });
    // Restore settings from Google Drive
    $("#id_popup_restore div.restore_from_cloud").click((event) => {
        event.preventDefault();
        restoreFromCloudHandler();
    });
    $("#color-input-red").click(handlerSyncSettingsToCloud);
    updateCloudUserInfoBar();
}
