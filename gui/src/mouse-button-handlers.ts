import * as $ from 'jquery';
import { MenuItemHandler } from "./menu-item-handler";
import { getCommandLabel } from "./helper";
import { modifierButtonsFromStr, modifierButtonsToStr } from "./modifier-buttons";

class MouseButtonActionHandlers implements MenuItemHandler {
    loadFn(button: number, command: string, params: any, selectedRadio: any) {
        let modifiers = '';
        if (params && params.hasOwnProperty('modifiers')) {
          modifiers = params.modifiers;
        }
        modifierButtonsFromStr(modifiers, selectedRadio);
        if (params && params.hasOwnProperty('label')
            && params.label) {
            $(selectedRadio).find('input[name=\'label\']').val(params.label);
        }
    };
    validatorFn(command: string) {
        return true;
    };
    storeFn(app:string, button: number, command: string, element: any) {
        let label = $(element).find('div.set_content input[name=\'label\']').val();
        return {
            modifiers: modifierButtonsToStr(element),
            label: label ? label : ''
        };
    };
    labelFn(command: string, params: any, t: (val:string) => string) {
        if (params
            && params.hasOwnProperty('label')
            && params.label) {
            return params.label;
        }
        return getCommandLabel(command);
    };
}

export function bindMouseButtonActionHandlers(t: any, actionHandlers: Map<RegExp, MenuItemHandler>) {
    actionHandlers.set(new RegExp('mouse_'), new MouseButtonActionHandlers());
}
