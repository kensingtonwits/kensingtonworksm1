import * as fs from "fs";
import * as electron from "electron";
import * as jsrender from "jsrender";
import * as path from 'path';
import * as $ from "jquery";
const exec = require('child_process').exec
import * as os from 'os';

const cmdservice = require('bindings')('commandServer.node');

export enum request_code {
    REQUEST_UNKNOWN,
    REQUEST_WHO,
    REQUEST_GET_DEFAULT_BUTTONS_CONFIG,
    REQUEST_GET_BUTTON,
    REQUEST_GET_ALL_BUTTONS,
    REQUEST_SET_BUTTON,
    REQUEST_SET_ALL_BUTTONS,
    REQUEST_GET_DEFAULT_POINTER_CONFIG,
    REQUEST_GET_POINTER_CONFIG,
    REQUEST_SET_POINTER_CONFIG,
    REQUEST_GET_DEFAULT_SCROLL_CONFIG,
    REQUEST_GET_SCROLL_CONFIG,
    REQUEST_SET_SCROLL_CONFIG,
    REQUEST_GET_SNIPPETS,
    REQUEST_GET_APPS,
    REQUEST_CREATE_APP,
    REQUEST_DELETE_APP,
    REQUEST_GET_DEVICES,
    REQUEST_GET_CONFIGS,
    REQUEST_SET_CONFIGS,
    REQUEST_GET_FAVORITE_APPS,

}


let _env: any = null;

// localization function
let _t: (s: string, options?: any) => string = null;

/**
 * Returns the URL parameters as an object with properties.
 */
export function getUrlParams(): any
{
    let vars: any = {};
    let href = window.location.href;
    let parts = href.replace(
        /[?&]+([^=&]+)=([^&]*)/gi,
        (m,key,value): string => {
            vars[key] = value;
            return ""
        }
    );
    return vars;
}

/**
 * @function isDev Returns true if app is running in dev environment.
 * @returns {boolean}
 */
export function isDev(): boolean {
    let app = electron.app || electron.remote.app;
    // for testing only
    //return app.getAppPath().search('tsgui') != -1;
    return app.isPackaged == false;
}

/**
 * @property {function} isProd Returns true if app is running in prod environment.
 * @returns {boolean}
 */
export function isProd(): boolean {
    let app = electron.app || electron.remote.app;
    //return app.getAppPath().search('tsgui') == -1;
    return app.isPackaged;
}

export function loadTemplate(filePath: string): JsViews.Template {
    let templateRaw = fs.readFileSync(getTemplateFilePath(filePath));
    return jsrender.templates(templateRaw.toString());
}

export function loadTemplateString(filePath: string): string {
    let templateRaw = fs.readFileSync(getTemplateFilePath(filePath));
    //let doc = new DOMParser().parseFromString(templateRaw.toString(), 'text/xml');
    //return doc.body;
    if (templateRaw) {
        return templateRaw.toString();
    }
    return "";
}

export function getTemplateFilePath(templateFile: string): string {
    let filePath = '';
    if (templateFile.startsWith('/')) {
        templateFile = templateFile.substr(1);
    }

    filePath =  path.join(__dirname, 'templates', templateFile);
    //console.log('Template file path:', filePath);

    return filePath;
}

export function getDataFilePath(dataFile: string): string {
    let filePath = '';
    if (dataFile.startsWith('/')) {
        dataFile = dataFile.substr(1);
    }

    filePath = path.join(__dirname, 'data', dataFile);
    //console.log('Data file path:', filePath);
    return filePath;
}

/**
 * Returns the value of the specified URL parameter or the default value
 * if the parameter is not in the URL argument list..
 *
 * @param parameter string the URL parameter name to query
 * @param defaultvalue any default value for the parameter.
 */
export function getUrlParam(parameter: string, defaultvalue: any): any
{
    let urlparameter = defaultvalue;
    if(window.location.href.indexOf(parameter) > -1) {
        urlparameter = getUrlParams()[parameter];
    }
    return urlparameter;
}

// Data for each supported device that is stored in supperted-devices.json
// is of the following format.
export interface ButtonInfo {
    hoverIndex: number;
    mask: number;
}
export interface ScrollDirImages {
    normal: string,
    reverse: string
};

export interface SupportedDevice {
    id: number;
    name: string;
    family: string;
    topView: boolean;
    sideView: boolean;
    topButtons: ButtonInfo[];
    sideButtons: ButtonInfo[];
    scrollWheel: boolean;
    scrollDirImages?: ScrollDirImages;
    connectionInterface: string;
    defaultSettings: any;
    tutor: any;
}

export function getAppFolder(): string {
    return './app';
}
/**
 * Returns the array of supported devices by reading the relevant data file.
 */
export function getSupportedDevices(): SupportedDevice[] {
    let sDevicesRaw = fs.readFileSync(getDataFilePath('supported-devices.json'));
    let sDevices: SupportedDevice[] = JSON.parse(sDevicesRaw.toString());
    return sDevices;
}

/**
 * Given a product id, returns its name as defined in supported-devices.json.
 *
 * @param id product id
 */
export function getSupportedDeviceInfo(id: number): SupportedDevice {
    let sDevices = getSupportedDevices();
    let sd: SupportedDevice = null;
    for (let index = 0; index < sDevices.length; index++) {
        const element = sDevices[index];
        if (element.id == id) {
            sd = element;
            break;
        }
    }
    return sd;
}

//&*&*&*G1_ADD
/**
 * Given a product id, returns its name as defined in supported-devices.json.
 *
 * @param id product id
 */
export function initCmdService(): boolean {
    const num = cmdservice.StartService(100);
    console.log('StartService: ', num);

    let response = cmdservice.SendRequest(request_code.REQUEST_WHO, "2b14d5e");
    console.log('SendRequest: ', response);

    return true;}

export function sendCmdServiceRequest(request: number, app: string, devid: number, button: number,
    cmd: string, params: string): string {
    //console.log('SendRequest : ', request, ' button ', button, ' command ', cmd, 'params ', params);
    //console.log(' . ');
    let response = cmdservice.SendRequest(request, app, devid, button, cmd, params);
    //console.log('SendRequest response: ', response);
    return response;
}
//&*&*&*G2_ADD

let _commandLabels: Map<string, string> = null;
/**
 * Returns the command label for the given command-name. Returns the command
 * itself if a matching entry is not found in the command-labels translation
 * table.
 *
 * @param command string The command name, whose label is being queried
 */
export function getCommandLabel(command: string): string {
    /*
    if (!_commandLabels) {
        //console.log('Loading command labels...');
        _commandLabels = new Map<string, string>();
        let labelsRaw = fs.readFileSync(getDataFilePath('command-labels.json'));
        let labels: any = JSON.parse(labelsRaw.toString());
        for (const key in labels) {
        _commandLabels.set(key, labels[key]);
        }
    }
    return _commandLabels.has(command) ? _commandLabels.get(command) : command;
    */
   return loadString(command);
}

/**
 * Function localizes all strings in the page.
 *
 * @param t function The i18next function to retrieve the string value given
 * a token for the currently selected language.
 */
export function localizeStrings() {
    $("input.i18text").attr("value", (index, i18token) => {
        let i18Str = loadString(i18token);
        return i18Str;
    })
    $(".i18text").each((index, element) => {
        let jElem = $(element);
        let token = jElem.text();
        if (token) {
          jElem.text(loadString(token));
        }
        if (jElem.attr('title') != null) {
            let title = jElem.attr('title');
            if (title) {
                jElem.attr({title: loadString(title)});
            }
        }
    });
}

export function getEnv() {
    if (_env == null) {
        try {
            let filePath = path.join(__dirname, 'env.json');
            let envRaw = fs.readFileSync(filePath);
            _env = JSON.parse(envRaw.toString())
        } catch (error) {
            console.log('Error reading env.json');
        }
    }
    return _env;
}

export function initI18Funcs(t:any) {
    _t = t;
}

/**
 * Return the string with the given id in the current UI language.
 *
 * @param str The string to localize to current UI language
 */
export function loadString(str: string, options: any = {}): string {
    if (_t) {
        return _t(str, options);
    }
    return str;
}

/**
 * Code to detect if a process is running by using system process list
 * command. Command itself platform dependent and detected upon runtime.
 *
 * @param win windows process name
 * @param mac Mac process name
 * @param linux Linux process name
 */
export function isProcessRunning(win: string, mac: string, linux: string){
    return new Promise(function(resolve, reject){
        const plat = process.platform
        const cmd = plat == 'win32' ? 'tasklist' : (plat == 'darwin' ? 'ps -ax | grep ' + mac : (plat == 'linux' ? 'ps -A' : ''))
        const proc = plat == 'win32' ? win : (plat == 'darwin' ? mac : (plat == 'linux' ? linux : ''))
        if(cmd === '' || proc === ''){
            resolve(false)
        }
        exec(cmd, function(err: any, stdout: any, stderr: any) {
            resolve(stdout.toLowerCase().indexOf(proc.toLowerCase()) > -1)
        })
    })
}

/**
 * A message box that can be called from both main & renderer processes.
 *
 * @param msg string The message string
 * @param type string Message type {error|warning|info}
 */
export function safeMessageBox(msg: string, type: string) {
    let windows = null;
    if (process && process.type == 'renderer') {
        windows = electron.remote.BrowserWindow.getAllWindows();
        return electron.remote.dialog.showMessageBox(
            windows.length > 0 ? windows[0] : null, {
                message: msg,
                type: type
            }
        );
    } else {
        windows = electron.BrowserWindow.getAllWindows();
        return electron.dialog.showMessageBox(
            windows.length > 0 ? windows[0] : null, {
                message: msg,
                type: type
            }
        );
    }
}

/**
 * Returns a filename that suffixes the OS platform such that when settings
 * backed up to the could, windows/mac settings can be differentiated.
 */
export function getCloudBackupFilename() {
    return `settings_${os.platform()}.json`;
}

/**
 * Dump all session cookies.
 */
export function dumpCookies() {
    // Query all cookies.
    electron.remote.session.defaultSession.cookies.get({})
        .then((cookies) => {
            console.log(cookies)
        }).catch((error) => {
            console.log(error)
        });
}

/**
 * Clears all cookies from session cookie store.
 */
export async function clearCookies() {
   electron.remote.session.defaultSession.clearStorageData({
       storages: ['cookies']
   });
}

/**
 * Returns main app window object or null
 */
export function getMainWindow(): electron.BrowserWindow {
    let windows = electron.BrowserWindow.getAllWindows();
    return windows && windows.length > 0 ? windows[0] : null;
  }
