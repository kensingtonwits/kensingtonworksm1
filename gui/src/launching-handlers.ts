import { remote } from "electron";
import * as $ from 'jquery';
import { MenuItemHandler } from "./menu-item-handler";
import { loadString } from "./helper";

// common function to store App/Folder/App parameters
function storeFileFolderApplication(app:string, button: number, command: string, element: any): any {
    let label = $(element).find('input[name=\'label\']').val();
    return {
        path: $(element).find('div.set_content p.set_word').text(),
        label: label ? label : ''
    }
}

function getLabel(command: string, params: any): string {
    let label = '';
    if (params && params.hasOwnProperty('label')) {
        label = params.label;
    }
    if (!label) {
        switch (command) {
            case 'launching_openFile':
                label = 'Open File';
                break;
            case 'launching_openFolder':
                label = 'Open Folder';
                break;
            case 'launching_openApplication':
                label = 'Open Application';
                break;
            case 'launching_openURL':
                label = params.path;
                break;
        }
    }
    return label;
}

class FileActionHandlers implements MenuItemHandler {
    loadFn(button: number, command: string, params: any, selectedRadio: any) {

    };
    validatorFn(command: string) {
        let file = $("#id-launch-file-choose").next("p.set_word").text();
        return file.length > 0;
    };
    storeFn(app:string, button: number, command: string, element: any) {
        return storeFileFolderApplication(app, button, command, element);
    };
    labelFn(command: string, params: any, t: (val:string) => string) {
        return getLabel(command, params);
    };
}

class FolderActionHandlers implements MenuItemHandler {
    loadFn(button: number, command: string, params: any, selectedRadio: any) {

    };
    validatorFn(command: string) {
        let file = $("#id-launch-folder-choose").next("p.set_word").text();
        return file.length > 0;
    };
    storeFn(app:string, button: number, command: string, element: any) {
        return storeFileFolderApplication(app, button, command, element);
    };
    labelFn(command: string, params: any, t: (val:string) => string) {
        return getLabel(command, params);
    };
}

class AppActionHandlers implements MenuItemHandler {
    loadFn(button: number, command: string, params: any, selectedRadio: any) {

    };
    validatorFn(command: string) {
        let file = $("#id-launch-application-choose").next("p.set_word").text();
        return file.length > 0;
    };
    storeFn(app:string, button: number, command: string, element: any) {
        return storeFileFolderApplication(app, button, command, element);
    };
    labelFn(command: string, params: any, t: (val:string) => string) {
        return getLabel(command, params);
    };
}

class URLActionHandlers implements MenuItemHandler {
    loadFn(button: number, command: string, params: any, selectedRadio: any) {
        $("#id-launch-url-choose").val(params.path);
        let label = params.hasOwnProperty('label') ? params.label : '';
        $("#id-launch-url-choose").next('input[name=\'label\']').val(label);
    };
    validatorFn(command: string) {
        let url = $("#id-launch-url-choose").val().toString();
        // Regex to validate URL pattern
        var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
            '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
            '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
            '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
            '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
            '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
        return !!pattern.test(url);
    };
    storeFn(app:string, button: number, command: string, element: any) {
        let label = $(element).find('input[name=\'label\']').val();
        return {
            path: $("#id-launch-url-choose").val().toString(),
            label: label ? label : ''
        }
    };
    labelFn(command: string, params: any, t: (val:string) => string) {
        return getLabel(command, params);
    };
}

export function bindLaunchingHandlers(t: any, actionHandlers: Map<RegExp, MenuItemHandler>) {
  // launch file choose
  $("#id-launch-file-choose").click((ev) => {
    remote.dialog.showOpenDialog(remote.getCurrentWindow(), {
      filters: [
        { name: 'All Files', extensions: ['*'] }
      ],
      properties: ['openFile'],
    })
    .then(({canceled, filePaths}) => {
      if (!canceled && filePaths) {
        //console.log('Selected file:', filePaths[0]);
        $(ev.target).next("p.set_word").text(filePaths[0]);
      }
    });
  });

  // launch folder choose
  $("#id-launch-folder-choose").click((ev) => {
    remote.dialog.showOpenDialog(remote.getCurrentWindow(), {
      properties: ['openDirectory', 'treatPackageAsDirectory'],
    })
    .then(({canceled, filePaths}) => {
      if (!canceled && filePaths) {
        //console.log('Selected folder:', filePaths);
        $(ev.target).next("p.set_word").text(filePaths[0]);
      }
    });
  });

  // launch application choose
  $("#id-launch-application-choose").click((ev) => {
    let programFilter = [
      { name: loadString('Executable Files'), extensions: ['exe'] }
    ];
    if (process.platform == 'darwin') {
      programFilter = [
        { name: loadString('Applications'), extensions: ['app'] }
      ];
    };
    remote.dialog.showOpenDialog(remote.getCurrentWindow(), {
      filters: programFilter,
      properties: ['openFile']
    })
    .then(({canceled, filePaths}) => {
      if (!canceled && filePaths) {
        //console.log('Selected program:', filePaths);
        $(ev.target).next("p.set_word").text(filePaths[0]);
      }
    });
  });

  actionHandlers.set(new RegExp('launching_openFile'), new FileActionHandlers());
  actionHandlers.set(new RegExp('launching_openFolder'), new FolderActionHandlers());
  actionHandlers.set(new RegExp('launching_openApplication'), new AppActionHandlers());
  actionHandlers.set(new RegExp('launching_openURL'), new URLActionHandlers());
}
