// Homepage primary script

import { remote, ipcRenderer, shell } from "electron";
import * as $ from "jquery";
import { initI18n } from "./i18n";
import "foundation-sites";
import * as mscroll from "malihu-custom-scrollbar-plugin";
const Swiper = require("swiper");
import { TrackballDevice } from "./tbw";
import {
    initDimensions, initMenu, initScrollbar, setupGlobalUIHandlers
} from "./all";
import {
    SupportedDevice, getSupportedDevices, getSupportedDeviceInfo,
    loadTemplate, initI18Funcs, localizeStrings
} from "./helper";
import { initBackupRestoreHandlers } from "./BackupRestoreHandlers";

//&*&*&*G1_ADD
const electron = require('electron');
const console = require('console');
//&*&*&*G2_ADD


// keeps track of all enumerated devices
let _deviceList : TrackballDevice[] = [];
let _swiperAvailDeviceList: any = null;
let _swiperAddDeviceList: any = null;
let _addDeviceIndexes = new Map<number, number>();

function initSwiper(): void {

    _swiperAvailDeviceList = new Swiper('.devicecycle .swiper-container', {
        speed: 1200,
        autoplay: false,
        slidesPerView: 3,
        centeredSlides: true,
        loop: false, //循環撥放
        spaceBetween: 0,
        slidesPerGroup: 1,
        autoHeight: true,
        watchOverflow: true,
        simulateTouch: true,
        effect: "slide", //Could be "slide", "fade", "cube", "coverflow" or "flip"
        pagination: {
            el: '.devicecycle .swiper-pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '.devicecycle .swiper_next',
            prevEl: '.devicecycle .swiper_prev',
        }
    });
    _swiperAddDeviceList = new Swiper('.device_addcycle .swiper-container', {
        speed: 1200,
        autoplay: false,
        slidesPerView: 5,
        centeredSlides: true,
        loop: false, //循環撥放
        spaceBetween: 0,
        slidesPerGroup: 1,
        calculateHeight: true,
        autoHeight: true,
        watchOverflow: true,
        simulateTouch: true,
        effect: "slide", //Could be "slide", "fade", "cube", "coverflow" or "flip"
        pagination: {
            el: '.device_addcycle .swiper-pagination',
            clickable: true,
        },
        navigation: {
            nextEl: '.device_addcycle .swiper_next',
            prevEl: '.device_addcycle .swiper_prev',
        }
    });
    $(document).on('click', '.button.cancel', function () {
        $('.coverbox').removeClass('click');
        $('.popup').removeClass('click');
    });
}

/**
 * Appends all supported device information to the 'Add Devices' swiper
 * container. This is done before the Swiper is initialized so that
 * the alignment of the 'VIEW ALL' button below it comes out nice.
 */
function initAddDevicesList() {
    let sDevices: SupportedDevice[] = getSupportedDevices();
    let slides = new Array<string>();
    if (_swiperAddDeviceList) {
        _swiperAddDeviceList.removeAllSlides();
    }
    _addDeviceIndexes.clear();
    sDevices.forEach((val, index, ar) => {
        //console.log("Supported devices - id:", val.id, "name:", val.name);
        let tmpl = loadTemplate('add-device.html');
        let html = tmpl.render(val);
        //console.log(html);
        slides.push(html);
        //console.log('AddDeviceList - device:', val.id, ', index:', index);
        _addDeviceIndexes.set(val.id, index);
        if (_swiperAddDeviceList) {
            _swiperAddDeviceList.appendSlide(html);
        } else {
            $(".sp_add_device .swiper-wrapper").append(html);
        }
    })
    if (_swiperAddDeviceList) {
        _swiperAddDeviceList.update();
    }
}

/**
 * when a new device arrives, it's transferred from Add Devices list to
 * the Available Devices list. As a part of this, we remove the slide item from
 * Add Devices swiper and add it to Available Devices swiper. Once we do this
 * we need to rebuild the Add Devices swiper item indices so that when another
 * device arrives, we can remove the relevant slide item by its correct index
 * value in Swiper control.
 */
function removeDeviceFromAddDevicesList(id: number) {
    let slideToRemove = _addDeviceIndexes.get(id);
    _swiperAddDeviceList.removeSlide(slideToRemove);
    _swiperAddDeviceList.update();
    _swiperAddDeviceList.updateAutoHeight();

    // update _addDeviceIndexes Map so that it
    let localIndexes = new Map<number, number>();
    _addDeviceIndexes.forEach((value, key) => {
        if (key != id) {
            localIndexes.set(key, value > slideToRemove ? value-1 : value);
        }
    })
    _addDeviceIndexes = localIndexes;
    // console.log('Updated addDevicesIndexes:-');
    // _addDeviceIndexes.forEach((value, key) => {
    //     console.log('\tdevice:', key, ', index:', value);
    // });
}

function initPage(): void {
    $(document).foundation(); //IE old version tip
    mscroll($);

    // Initialize keeping the loader image in place. Loader SVG image will be
    // hidden when we detect Helper at the end of the page init sequence.
    initDimensions(false);

    initMenu();
    initScrollbar();

    // Add all supported devices stored in the device list JSON
    // to 'Add Devices' list. Note that we do this before we initialize
    // the Swiper control as this would help position the 'VIEW ALL' button
    // below the swiper correctly without partially obscuring the Swiper
    // contents.
    initAddDevicesList();
    initSwiper();
    setupGlobalUIHandlers();
    initBackupRestoreHandlers();

    // updateProductVersion();
    // setupUpdateMechanism();

    // get all connected devices
    let kwConfig = remote.getGlobal('kwConfig');

    if (kwConfig && kwConfig.helperDetected) {
        let devices: Map<number, TrackballDevice> = kwConfig.devices;
        if (devices.size > 0) {
            console.log('Connected devices: ', devices.size);
            devices.forEach((device, key, map) => {
                //console.log('Reporting arrival of enumed global device list');
                handleDeviceArrival(device);
            });
        } else {
            hideLoading();
        }
    } else {
        console.warn('Error retrieving shared object from global');
    }

    // Setup handler for new device arrival notification sent by main process.
    ipcRenderer.on('device-arrival', (event: any, device: TrackballDevice) => {
        console.log('device-arrival message');
        let tbDevice = new TrackballDevice(
            device._id, device._realId, device._name,
            device._version, device._present, device._connectionInterface
        );
        handleDeviceArrival(tbDevice);
    });

    ipcRenderer.on('device-status-change', (event: any, device: TrackballDevice) => {
        let devTitle = $(`div.swiper-slide[data-device-id='${device._id}'] h6.devicename`);
        console.log('device-status-change message- ', device._name);
        if (devTitle.length) {
            if (device._present) {
                $(devTitle).addClass('connected');
            } else {
                $(devTitle).removeClass('connected');
            }
        }
    });
}

/**
 * Handles device arrival. We need to do the following:
 *
 *  1. Add the device to the 'Select to start' list
 *  2. If more than 3 devices is present in our device list, enable the
 *     Swiper arrow heads
 *
 * @param device TrackballDevice representing the device
 */
function handleDeviceArrival(device: TrackballDevice) {
    console.log('handleDeviceArrival - device: ', device._id);
    let sd = getSupportedDeviceInfo(device.id());
    if (!sd) {
        console.log(`Device with id ${device.id()} not supported!`);
        hideLoading();
        return;
    }
    let deviceCountBefore = _deviceList.length;
    _deviceList.push(device);
    let tmpl = loadTemplate('select-product.html');
    let html = tmpl.render({
        id: device.id(),
        name: sd.name,
        family: sd.family,
        nameClass: device.isPresent() ? "connected": ""
    });
    //console.log(html);
    _swiperAvailDeviceList.appendSlide(html);
    _swiperAvailDeviceList.update();
    _swiperAvailDeviceList.slideNext();

    if (deviceCountBefore == 0) {
        // Disable 'No connected devices' and enable the 'Select to start'
        $(".no_device").addClass("is-hidden");
        $(".sp_device_list").removeClass("is-hidden");
    }

    // remove the device from Add Device Swiper
    //console.log('handleDeviceArrival - device:', device.id(), ', add device index:', _addDeviceIndexes.get(device.id()));
    removeDeviceFromAddDevicesList(device.id());

    // incase we were in helperDetected == false state, hide the loading indicator
    hideLoading();
}

function showLoading() {
    $('div.loader').show();
}

function hideLoading() {
    $('div.loader').hide();
}

// Handler for helper-missing notification sent by main process.
ipcRenderer.on('helper-missing', (event: any) => {
    //console.log('Helper has gone missing!')
    _swiperAvailDeviceList.removeAllSlides();
    initAddDevicesList();
    showLoading();
});

// Handler for helper-deleteced notification sent by main process.
ipcRenderer.on('helper-detected', (event: any) => {
    //console.log('Helper detected!')
    hideLoading();
});

$(document).ready(() => {

    initI18n()
    .then((t) => {
        // @ts-ignore
        // $("#greet").html(t('greeting'));
        //let tt = new Foundation.Tooltip($('.has-tip'));
        initI18Funcs(t);

        try {
            initPage();
            localizeStrings();
        } catch (error) {
            console.log('init error: ', error);
        }

    }).catch((err) => {
        console.log("Error initializing i18next library, err: ", err);
    });
});
