export interface CloudUserInfo {
    name: string,
    email: string,
};
