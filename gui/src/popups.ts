/**
 * Popup management functions
 */
import * as $ from "jquery";
import { UserCancellation } from "./errors";

 /**
 * Show the coverbox div that makes popups above it behave like a modal dialog.
 */
export function showCoverBox() {
    $("#id_coverbox").addClass('click');
}

export function isPopupActive() {
    return $('div.popup.click').length > 0;
}
/**
 * Hide the coverbox div that makes popups above it behave like a modal dialog.
 */
export function hideCoverBox() {
    $("#id_coverbox").removeClass('click');
}

export function showPopup(idPopup: string, withCoverBox = true) {
    if (!isPopupActive()) {
        if (withCoverBox) { showCoverBox(); }
        $(`#${idPopup}`).addClass('click');
    }
}

function hidePopupImpl($popup: JQuery<HTMLElement>, withCoverBox: boolean, settlePromise = false, error: Error = null) {
    $popup.removeClass('click');
    if (withCoverBox) { hideCoverBox(); }
    if (settlePromise) {
        let promise: any = $popup.data('promise');
        // remove the promise object so that next invocation can be without promise
        $popup.data('promise', null);
        if (promise) {
            if (!error) {
                promise.resolve()
            } else {
                promise.reject(error);
            }
        }
    }
}

/**
 * Hide a popup by its id, or an event on one of its child elements.
 *
 * @param idPopup string Popup id
 * @param withCoverBox
 * @param error
 */
export function hidePopup(idPopup: string | Event | JQuery.ClickEvent, withCoverBox = true) {
    if (typeof idPopup == "string") {
        hidePopupImpl($(`#${idPopup}`), true, false);
    } else {
        let element = event.target as HTMLElement;
        let popup = $(element).parents('div.popup');
        hidePopupImpl($(popup[0]), withCoverBox, false);
    }
}

/**
 * Hide a popup by its id, or an event on one of its child elements.
 *
 * @param idPopup string Popup id
 * @param withCoverBox
 * @param error
 */
export function hideAsyncPopup(idPopup: string | Event | JQuery.ClickEvent, withCoverBox = true, error: Error = null) {
    if (typeof idPopup == "string") {
        hidePopupImpl($(`#${idPopup}`), true, true, error);
    } else {
        let element = event.target as HTMLElement;
        let popup = $(element).parents('div.popup');
        hidePopupImpl($(popup[0]), withCoverBox, true, error);
    }
}

export function showBusyPopup() {
    showCoverBox();
    showPopup('id_popup_busy');
    // $("#id_popup_busy").addClass('click');
}

export function hideBusyPopup() {
    hidePopup('id_popup_busy');
    hideCoverBox();
    //$("#id_popup_busy").removeClass('click');
}

/**
 * Show alert message
 * @param title message box title
 * @param message message text
 * @param icon message icon, optional
 */
export function showAlertMessage(title: string, message: string, icon?: string) {
    $('#id_popup_alert .popupsubtitle').text(title);
    $('#id_popup_alert .popupword').text(message);
    if (icon) {
        $('#id_popup_alert .alerticon').html(icon);
    } else {
        $('#id_popup_alert .alerticon').html('<i class="popupicon be-icon be-icon-prompt"></i>');
    }
    showCoverBox();
    $('#id_popup_alert').addClass('click');
}

/**
 * Hide alert box
 */
export function hideAlertMessage() {
    // replace with default placeholders
    $('#id_popup_alert .popupsubtitle').text('Alert Title');
    $('#id_popup_alert .popupword').text('Alert message');
    $('#id_popup_alert .alerticon').html('<i class="popupicon be-icon be-icon-prompt"></i>');
    $('#id_popup_alert').removeClass('click');
    hideCoverBox();
}

/**
 * Show popup with async completion. Promise is settled when the user either
 * dismisses the popup with Cancel/Close, (promise is rejected) or when the
 * intended action is taken (promise is resolved).
 *
 * @param idPopup string popup id, without the leading '#'
 * @param withCoverBox boolean Whether 'modal' coverbox should be activated
 * @param resolve callback Promise resolve callback function
 * @param reject callback Promise reject callback function
 */
export async function showPopupAsync(idPopup: string, withCoverBox: boolean,
    resolve: (value?: unknown) => void, reject: (reason?: any) => void) {
    if (withCoverBox) { showCoverBox(); }
    let $popup = $(`#${idPopup}`);
    // Set the data-promise to { resolve: res_cb, reject: rej_cb }
    // So that hidePopup can resolve or reject it as the case may
    $popup.data('promise', { resolve: resolve, reject: reject });
    $(`#${idPopup}`).addClass('click');
}

/**
 * Returns the promise resolve, reject callbacks associate with an async popup.
 *
 * @param idPopup string Popup id
 */
export function getAsyncPopupPromiseCallbacks(idPopup: string): { resolve: (value?: unknown) => void, reject: (reason?: any) => void } {
    let $popup = $(`#${idPopup}`);
    return $popup.data('promise');
}