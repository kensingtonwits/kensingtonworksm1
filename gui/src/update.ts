/**
 * Implements core of the update mechansim. Exports one function,
 * initUpdateMechanism(), which is to be called from the main process.
 *
 * Registers the following ipc messages:
 *  - check-for-update
 *      Args:
 *          None
 *      Sends an HTTP request to the update server querying the version info
 *      for the latest package matching current OS & bitness.
 *      Generates one of the following two messages to renderer:
 *        - update-available
 *        - no-update-avaialble
 *
 *  - download-update
 *      Args:
 *        UpdateStatus object (see update-available)
 *      Renderer process can send this message with an UpdateStatus object
 *      to begin downloading the latest update package. As soon as the
 *      download starts, an 'update-download-started' message will be sent
 *      to the renderer process.
 *
 *  - cancel-download-update
 *      Cancel an ongoing update download process. If no update is ongoing,
 *      does nothing.
 *      Args:
 *        None
 *
 *  - open-update-package
 *      This message executes the package with the shell.openExternal() API
 *      kick starting the update process. The installation package is expected
 *      to detect KensingtonWorks and terminate it as part of its installation
 *      workflow.
 *      Args:
 *        Absolute path to the downloaded package to open for starting the
 *        update process.
 *
 *
 * Sends the following IPC messages to renderer process:
 *  - update-available
 *      Indicates that an update is available. Renderer process can reflect
 *      the package information in a suitable UI and prompt user whether they
 *      want to go ahead with downloading the package. To trigger download,
 *      send 'download-update' message to the main process.
 *      Args:
 *        UpdateStatus object
 *
 *  - no-update-available
 *      Indicates that there are no new updates available and the user is
 *      using the latest version of the package.
 *      Args:
 *        None
 *
 *  - update-download-started
 *      Indicates that downloading of the update package has started.
 *      Args:
 *        Object of the following prototype:
 *        {
 *          filename: /filename of the package to download/
 *          savePath: /Absolute path where the package will be saved to/
 *          totalBytes: /Total size of the package/
 *        }
 *
 *  - update-download-progress
 *      Updates the renderer process on download progress. Renderer can use
 *      this message to update an appropriate UI widget reflecting download
 *      progress to the user.
 *      Args:
 *        Number, download progress value in the range 0..1. Multiply by
 *        100 to get percentage.
 *
 *  - update-download-finished
 *      Indicates that download of the update package is complete.
 *      Args:
 *        None
 *
 *  - update-download-cancelled
 *      Indicates that update in progress has indeed been cancelled.
 *      Args:
 *        None
 */
import { ipcMain, BrowserWindow, DownloadItem, shell } from "electron";
import { PRODUCT_VERSION } from "./version";
import { download } from "electron-dl";
import * as os from "os";
import axios from 'axios';
import * as path from 'path';
import * as fs from "fs";
import { getEnv } from "./helper";

// const GET_LATEST_PACKAGE_INFO_URL = 'https://kensington.com/siteassets/product-support/';
//const GET_LATEST_PACKAGE_INFO_URL = 'https://smallpearl.com/media/';
const GET_LATEST_PACKAGE_INFO_URL = 'https://accoblobstorageus.blob.core.windows.net/software/version/KensingtonWorks2/';

let _curDownloadItem: DownloadItem = null;

function getPackageInfoFilename() : string {
  let platform = 'osx';
  let bit = 'x64'
  if (process.platform == 'win32') {
    platform = 'win';
    if (os.arch() == 'ia32') {
      bit = 'x86';
    }
  }
  return `kgtw_${platform}.json`;
}

/**
 * Check with the update check server if a new version is available. First
 * checks if the file kgtw_{win32|darwin}_{x64|x86}.json is present in the
 * local folder. If it is, then returns its contents. If it's not present,
 * or its contents were not a valid JSON file (couldn't be parsed), queries
 * the update server for update information.
 *
 * Generates renderer IPC event: 'update-available' if update is available
 *  {
 *      packageInfo: any
 *  }
 *
 */
function checkForNewVersion(): Promise<any> {
  return new Promise((res, rej) => {
    // Look for the file kgtw_${platform}_${bit}.json file in the application
    // folder. If it's present, read it and return its contents. This is for
    // testing update with a dummy server.
    let packageFilename = getPackageInfoFilename();
    let filePath = '';
    let env = getEnv();
    if (env && env.name == 'production') {
      filePath = path.join(__dirname, "..", "..", "..", packageFilename);
    } else {
      filePath = path.join(__dirname, "..", packageFilename);
    }
    try {
      //console.log('checkForNewVersion looking for local file:', filePath);
      let packageInfoRaw = fs.readFileSync(filePath);
      let data = JSON.parse(packageInfoRaw.toString());
      //console.log('checkForNewVersion local file found, data:', data);
      return res(data);
    } catch (error) {
      //console.log('checkForNewVersion error parsing local file:', error);
    }

    // If we don't get a response within 10 seconds we can assume that there's
    // something wrong with the server and therefore no updates are available.
    return axios.get(
      `${GET_LATEST_PACKAGE_INFO_URL}${packageFilename}`,
      { timeout: 10*1000}
      )
      .then((response) => {
        //console.log('checkForNewVersion - data:', response.data);
        res(response.data);
      }).catch(err => {
        rej(err);
      });
    })
}

/**
 * Returns the active BrowserWindow object or null if none detected.
 */
function getMainWindow(): BrowserWindow {
  let windows = BrowserWindow.getAllWindows();
  return windows && windows.length > 0 ? windows[0] : null;
}

function onDownloadStarted(item: DownloadItem) {
  //console.log('Package download started:', item.getFilename(), item.getTotalBytes());
  _curDownloadItem = item;
  let mainWindow = getMainWindow();
  if (mainWindow) {
    mainWindow.webContents.send("update-download-started", {
      filename: item.getFilename(),
      savePath: item.getSavePath(),
      totalBytes: item.getTotalBytes()
    });
  }
}

function onDownloadProgress(progress: number) {
  //console.log('Package download progress:', progress);
  let mainWindow = getMainWindow();
  if (mainWindow) {
    mainWindow.webContents.send("update-download-progress", {
      progress: progress
    });
  }
}

function onDownloadCancel(item: DownloadItem) {
  //console.log('Download cancelled');
  _curDownloadItem = null;
  let mainWindow = getMainWindow();
  if (mainWindow) {
    mainWindow.webContents.send("update-download-cancelled");
  }
}

/**
 * check-for-update return object
 */
export class UpdateStatus {
  constructor(
    public available: boolean,
    public url: string,
    public version: string,
    public size: number,
    public pubishedDate: string,
    public err?: Error) {
  }
}

/**
 * Initialize the update manager.
 *
 * This function essentially sets up the various main process IPC handlers
 * as documented at the top of this file.
 *
 * Intended to be called from main process as electron-dl package, that this
 * file uses, has to be run from the main process.
 */
export function initUpdateManager() {

  /**
   * IPC to check for new version availability
   */
  ipcMain.on('check-for-update', async (event: any) => {
      //console.log('check-for-update');
      if (_curDownloadItem == null) {
        checkForNewVersion()
        .then((pkgInfo) => {
          //console.log('check-for-update - pkgInfo:', pkgInfo);
          
          let mainWindow = getMainWindow();
          try {
            let latestVersion = `${pkgInfo.version_major}.${pkgInfo.version_minor}.${pkgInfo.version_build}`;
            let size = pkgInfo.size;
            let publishedDate = pkgInfo.published_date.split('T')[0];
            //let ourVersion = `${PRODUCT_VERSION.major}.${PRODUCT_VERSION.minor}.${PRODUCT_VERSION.patch}`;
            //console.log(`check-for-update - Our ver: ${ourVersion} latest version: ${latestVersion}`);

            if (pkgInfo.version_major > PRODUCT_VERSION.major
              || (pkgInfo.version_major == PRODUCT_VERSION.major
                  && pkgInfo.version_minor > PRODUCT_VERSION.minor)
              || (pkgInfo.version_major == PRODUCT_VERSION.major
                  && pkgInfo.version_minor == PRODUCT_VERSION.minor
                  && pkgInfo.version_build > PRODUCT_VERSION.patch)) {

              if (mainWindow) {
                mainWindow.webContents.send("update-available", new UpdateStatus(
                  true,
                  pkgInfo.download_url,
                  latestVersion,
                  size,
                  publishedDate
                ));
              }
            } else {
              // no updates available
              if (mainWindow) {
                mainWindow.webContents.send("no-update-available");
              }
            }
          } catch (error) {
            console.log('Error parsing package info received from server:', error);
          }
        }).catch((err) => {
          //console.log('check-for-update - Error quering update:', err);
          let mainWindow = getMainWindow();
          if (mainWindow) {
              mainWindow.webContents.send("update-available", new UpdateStatus(
              false, '', '', 0, '', err
            ));
          }
        });
      } else {
        //console.log('Not starting update as curDownloadItem != null');
      }
  });

  /**
   * Download the specified package from server
   */
  ipcMain.on('download-update', (event: any, info: UpdateStatus) => {
      //console.log('download-update');
      // For testing only!
      //let url = "https://www.kensington.com/siteassets/product-support/KensingtonWorks/KensingtonWorksa-1.2.zip";
      let url = info.url;
      download(
        BrowserWindow.getFocusedWindow(),
        url,
        {
          onStarted: onDownloadStarted,
          onProgress: onDownloadProgress,
          onCancel: onDownloadCancel,
        }).then((file) => {
          //console.log('Package downloaded to:', file.getFilename());
          _curDownloadItem = null;
          let mainWindow = getMainWindow();
          if (mainWindow) {
            mainWindow.webContents.send("update-download-finished");
          }
        })
  });

  ipcMain.on('cancel-download-update', (event: any) => {
    //console.log('cancel-download-update, package:');
    if (_curDownloadItem) {
      _curDownloadItem.cancel();
      //_curDownloadItem = null;
    } else {
      try {
        let mainWindow = getMainWindow();
        if (mainWindow) {
          mainWindow.webContents.send("update-download-cancelled");
        }
      } catch (error) {
      }
    }
  });

  /**
   * Fork the downloaded package to start the installation process.
   */
  ipcMain.on('open-update-package', (event: any, pkg: any) => {
    //console.log('open-update-package, package:', pkg);
    // Do *NOT* refer to _curDownloadItem here!!
    if (process.platform == 'darwin') {
      shell.openPath(pkg)
      .then(() => {})
      .catch((err) => {
        console.log('Error opening the update package:', err);
      });
    } else {
      shell.openExternal(pkg)
      .then(() => {})
      .catch((err) => {
        console.log('Error opening the update package:', err);
      });
    }
});
}
