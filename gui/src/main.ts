import { app, BrowserWindow, ipcMain, globalShortcut, protocol, Menu, shell } from "electron";
import * as path from "path";
import { initI18n } from "./i18n";
import { TrackballWorks, TrackballDevice } from "./tbw";

import {
  getSupportedDeviceInfo, getEnv, isProcessRunning, safeMessageBox,
  initI18Funcs, loadString } from "./helper";
import { initUpdateManager } from "./update";
import { fstat } from "fs";
//&*&*&*G1_ADD
const electron = require('electron');
//&*&*&*G2_ADD

let mainWindow: Electron.BrowserWindow;
let connectedDevices: Map<Number, Number> = new Map();

const DEFAULT_WINDOW_WIDTH = 1280;
const DEFAULT_WINDOW_HEIGHT = 768;
const DEVICE_POLL_INTERVAL = 1500;  // 1.5 seconds

// if (process.platform === 'win32') {
//   app.commandLine.appendSwitch('high-dpi-support', 'true')
//   app.commandLine.appendSwitch('force-device-scale-factor', "0.96");
// }

app.allowRendererProcessReuse = false

// disable pinch to zoom
app.commandLine.appendSwitch('disable-pinch');

//&*&*&*G1_ADD
//app.commandLine.appendSwitch("--disable-http-cache");
//&*&*&*G2_ADD

/**
 * Query list of devices from helper nad update the global variable
 * where we maintain list of all device settings.
 */
function enumerateDevices() : Promise<void>
{
  return TrackballWorks.getDevices()
  .then((devices) => {
    //console.log('devices: ', devices);
    if (devices.length == 0) {
      //console.warn('No devices attached');
      // If helper was missing and it's been detected, send the message
      // 'helper-detected' to the renderer process. Home page would use this
      // message to hide the 'Loading' status indicator.
      //
      // Note that need to do this only when there are no devices present.
      //
      // If devices are present, we'll be generating 'device-arrival' messages
      // to the renderer handler for which would implicitly hide the 'loading'
      // status indicator. 
      if (!global.kwConfig.helperDetected) {
        global.kwConfig.helperDetected = true;
        if (mainWindow) {
          mainWindow.webContents.send('helper-detected');
        }
      }
    } else {
      //console.log(`${devices.length} devices attached`);
      global.kwConfig.helperDetected = true;
      let curDevices: Map<Number, TrackballDevice> = global.kwConfig.devices;
        let newDevices = new Array<TrackballDevice>();
        let newarrivedDevices = new Array<Number>();
        devices.forEach((val: TrackballDevice, index: number, arr: any[]) => {
          //Checking connected devices
          if (val.connectionInterface() != '') {
              if (!connectedDevices.has(val.id())) {
                  connectedDevices.set(val.id(), index);
                  newarrivedDevices.push(val.id());
                  //console.log(`Found device connected - id: ${val.id()}, name: ${val.name()}`);
              }             
          }

        if (!curDevices.has(val.id())) {
          //console.log(`New device attached - id: ${val.id()}, name: ${val.name()}`);
          curDevices.set(val.id(), val);
          newDevices.push(val);
        } else {
          // Device exists. Check if it's present/interface status has changed.
          // If it has, update the new status and generate
          // 'device-status-change' message.
          let existingDevice: TrackballDevice = curDevices.get(val.id())
          if (existingDevice.connectionInterface() != val.connectionInterface()) {
            //console.log(`Connection interface has changed - old iface: ${existingDevice.connectionInterface()}, new iface: ${val.connectionInterface()}`);
            existingDevice._present = val._present;
            existingDevice._connectionInterface = val._connectionInterface;
            if (val.connectionInterface() == '') {
                connectedDevices.delete(val.id());
                console.log(`Connected device removed: ${val.id()}`); 
            }
            if (mainWindow) {
              mainWindow.webContents.send("device-status-change", existingDevice);
            }
          }
        }
      });

        if (mainWindow) {
            if (curDevices.size == 1 && curDevices.size == newDevices.length) {
                // If we detected a single device, go to its configuration page.
                let val = curDevices.values().next().value;
                gotoDeviceConfigPage(val.id());
            } else {
                // More than one device, generate the IPC message. If renderer
                // is in home page, it'll handle it accordingly.
                newDevices.forEach((val) => {
                    mainWindow.webContents.send("device-arrival", val);
                });

                if (newarrivedDevices.length == 1)
                {
                    let dev = curDevices.get(newarrivedDevices[0]);
                    console.log(`single new connected device - id: ${dev.id()}, name: ${dev.name()}`);
                    let url = mainWindow.webContents.getURL();
                    //let str = 'trackball_overview.html\?device=' + (dev.id()).toString();
                    let devstr = 'device=' + (dev.id()).toString();
                    //check if current page is the trackball's overview page (config. page)
                    if ((url.search('trackball_overview.html') < 0) || (url.search(devstr) < 0))
                    {
                        gotoDeviceConfigPage(dev.id()); // *&*&*&V1_20201208 要在開頭想辦法走到這裡來
                    }
                }

            }
        }
        else {
            // mainWindow isn't ready, reset connectedDevices.
            connectedDevices.clear();
        }

    }
  }).catch((err) => {
    if (err.hasOwnProperty('code') && err.code == 'ECONNREFUSED') {
      // Helper process is missing or something else has gone wrong!
      if (mainWindow) {
        mainWindow.webContents.send('helper-missing');
      }
      global.kwConfig.helperDetected = false;
      global.kwConfig.devices.clear(); //new Map<Number, TrackballDevice>()  
    }
    //console.warn('Error querying devices: ', err);
  });
}

/**
 * Navigate to device's configuration page
 * @param deviceId number, device's id
 */
function gotoDeviceConfigPage(deviceId: number): boolean {
  //console.log('gotoDeviceConfigPage, deviceId: ', deviceId);
  let sd = getSupportedDeviceInfo(deviceId);
  if (sd) {
    // Kludge -- this URL scheme is present in select-product.html as well.
    // So if we ever change the URL path, we have to update both places!
    let url = `file:///${__dirname}/${sd.family}_overview.html?device=${deviceId}`;
    mainWindow.loadURL(url);
    return true;
  }
  return false;
}

/**
 * Navigate to device's tutor page
 * @param deviceId number, device's id
 */
function gotoDeviceTutorPage(deviceId: number) {
  //console.log('gotoDeviceTutorPage, deviceId: ', deviceId);
  let url = `file:///${__dirname}/tutor.html?device=${deviceId}`;
  mainWindow.loadURL(url);
}

/**
 * Navigate to application home page.
 */
function gotoHome() {
  mainWindow.loadFile(path.join(__dirname, "home.html"));
}

/**
 * Navigate to home or to the single trackball's config page.
 */
function gotoHomeOrSingleTrackballConfigPage() {
  let devices = global.kwConfig.devices;
  let goHome = true;
  if (devices.size == 1) {
    let device: TrackballDevice = devices.values().next().value;
    goHome = gotoDeviceConfigPage(device.id()) == false;
  }
  
  if (goHome) {
    mainWindow.loadFile(path.join(__dirname, "home.html"));
  }
}

/**
 * Should be self explanatory -- sets up global variables accessible through
 */
function setupGlobals() {
    // @ts-ignore
    let config: KWConfig = {
      someVar: 100,
      helperDetected: true,
      devices: new Map<Number, TrackballDevice>()
    };
    global.kwConfig = config;
}

/**
 * Creates the one and only one Browser window, initializes it and loads
 * home.html into it. If only one device is detected, navigates to the device
 * configuration page.
 */
function createWindow() {

  // Create the browser window.

    //&*&*&*G1_ADD
    //clear cache
    const fs = require('fs');
    //var chromeCacheDir = path.join(app.getPath('userData'), 'Cache');
    var chromeCacheDir = app.getPath('userData');
    //console.log(`chromeCacheDir: ${chromeCacheDir}`);
    if (fs.existsSync(chromeCacheDir)) {
        var files = fs.readdirSync(chromeCacheDir);
        for (var i = 0; i < files.length; i++) {
            if (files[i] != 'Preferences')
                continue;
            var filename = path.join(chromeCacheDir, files[i]);
            if (fs.existsSync(filename)) {
                try {
                    var stats = fs.statSync(filename);
                    if (!stats.isDirectory()) {
                        fs.unlinkSync(filename);
                        //console.log(`file: ${filename}`);
                    }
                }
                catch (e) {
                    console.log(e);
                }
            }
        }
    }
    //&*&*&*G2_ADD

  // Calculate optimal width & height based on user specified zoom factor.
  // If you change this value to something else, be sure to erase the file
  // '~\AppData\Roaming\kensingtonworks\Preferences' for the new value to
  // take effect. Reason is Chrome engine caches the zoom factor and seems to
  // build graphics sprites for the various pages to speed up rendering.
  // This relies on user not changing the zoom factor with every run of the
  // program.
  //
  // Or this could be a bug that Chrome needs to fix.
    //&*&*&*G1_MOD
    //let winZoomFactor = 0.70;
    //let winWidth = Math.round(DEFAULT_WINDOW_WIDTH * winZoomFactor);
    //let winHeight = Math.round(DEFAULT_WINDOW_HEIGHT * winZoomFactor);
    var screenElectron = electron.screen;

    // Get mouse cursor absolute position
    const { x, y } = screenElectron.getCursorScreenPoint();
    // Find the display where the mouse cursor will be
    //const currentDisplay = screenElectron.getDisplayNearestPoint({ x, y });
    const currentDisplay = screenElectron.getPrimaryDisplay();

    let factor = 0.9;
    let wr = Math.min(currentDisplay.size.width * factor, DEFAULT_WINDOW_WIDTH * 1.0) / DEFAULT_WINDOW_WIDTH;
    //let hr = currentDisplay.size.height / (DEFAULT_WINDOW_HEIGHT *1.0);
    let hr = Math.min(currentDisplay.size.height * factor, DEFAULT_WINDOW_HEIGHT * 1.0) / DEFAULT_WINDOW_HEIGHT;
    let winZoomFactor = Math.min(wr, hr);
    let winWidth = Math.ceil(winZoomFactor * DEFAULT_WINDOW_WIDTH);
    let winHeight = Math.round(winZoomFactor * DEFAULT_WINDOW_HEIGHT);
    let contentZoomFactor = winZoomFactor;
   
    console.log(`Screen.w: ${currentDisplay.size.width}, Screen.h: ${currentDisplay.size.height}, Scale: ${currentDisplay.scaleFactor * 100}%`);
    //&*&*&*G2_MOD
    console.log(`Width: ${winWidth}, Height: ${winHeight}, wzfactor: ${winZoomFactor}, czfactor: ${contentZoomFactor}`);
    let windowOptions: Electron.BrowserWindowConstructorOptions = {
    width: winWidth,
    height: winHeight,
    x: currentDisplay.workArea.x + 50,
    y: currentDisplay.workArea.y + 50,
    frame: false,
    //transparent: true,
    //zoomToPageWidth: true,
    resizable: false,
    show: false,
    webPreferences: {
        nodeIntegration: true,
        zoomFactor: contentZoomFactor,   //winZoomFactor
        contextIsolation: false,
        enableRemoteModule: true,
    }
  };

  if (process.platform == 'darwin') {
    windowOptions.frame = true;
    windowOptions.useContentSize = true;
  }

  mainWindow = new BrowserWindow(windowOptions);
  //&*&*&*G1_ADD
    // Set window position to that display coordinates
    //mainWindow.setPosition(currentDisplay.workArea.x, currentDisplay.workArea.y);
    mainWindow.center();
  //&*&*&*G2_ADD

  // sets up update IPC handlers
  initUpdateManager();  

  if (process.platform === 'darwin') {
    const template: any = [
      {
        label: app.getName(),
        submenu: [{ role: 'about' }, { type: 'separator' }, { role: 'hide' }, { role: 'hideothers' }, { role: 'unhide' }, { type: 'separator' }, { role: 'quit' }]
      },
      {
        label: 'Edit',
        submenu: [{ role: 'undo' }, { role: 'redo' }, { type: 'separator' }, { role: 'cut' }, { role: 'copy' }, { role: 'paste' }, { role: 'selectall' }]
      },
      {
        role: 'window',
        submenu: [{ role: 'minimize' }, { role: 'close' }]
      }
    ];
    Menu.setApplicationMenu(Menu.buildFromTemplate(template));
  } else {
    Menu.setApplicationMenu(null)
  }
  // poll for new device arrivals every DEVICE_POLL_INTERVAL milliseconds
  setInterval(() => {
    enumerateDevices()
    .then(() =>{})
    .catch(()=>{});
  }, DEVICE_POLL_INTERVAL);

  // Emitted when the window is closed.
  mainWindow.on("closed", () => {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null;
  });

  // disable standard window behaviors
  //mainWindow.setResizable(false);
  mainWindow.setMaximizable(false);
  mainWindow.setFullScreenable(false);

  /*
   GLOBAL SHORTCUTS ARE SESSION WIDE AND WOULD PREVENT KENSINGTONWORKS FROM
   SUCCESSFULLY POKING THE KEYSTROKES INTO THE INPUT QUEUE. SO WE CANT USE
   THAT FOR THIS PROGRAM.

   WE HAVE TO FIGURE OUT SOME OTHER WAY TO DISABLE THE CHROMIUM SHORTCUTS.

  // Disable Cmd/Ctrl+W(close window)
  globalShortcut.register('CmdOrCtrl+W', () => {});

  // Disable Cmd/Ctrl+R(reload), -- only on production
  if (env && env.name == 'production') {
    globalShortcut.register('CmdOrCtrl+R', () => {});
    // disable all dev tools shortcuts
    let devToolsShortcuts = [
      'Ctrl+Shift+I',
      'F12',
      'Ctrl+Shift+J',
      'Ctrl+Shift+C'
    ]
    if (process.platform == "darwin") {
      devToolsShortcuts = [
        'Cmd+Alt+I',
        'Cmd+Alt+J',
        'Cmd+Alt+C',
        'Cmd+Shift+C'
      ]
    }
    devToolsShortcuts.forEach(shortcut => {
      globalShortcut.register(shortcut, () => {});
    });
  }
  */

  let webContents = mainWindow.webContents;
  // disable zooming
  webContents.on('did-finish-load', () => {
    // Commented out as this conflicts with the zoomFactor webPrefernces
    // argument used in 'new BrowserWindow()'.
    //webContents.setZoomFactor(1.0);
    //webContents.setVisualZoomLevelLimits(1.0, 2.0);
    //webContents.setLayoutZoomLevelLimits(1.0, 2.0);
  });

  gotoHomeOrSingleTrackballConfigPage();

    //&*&*&*G1_MOD
    // mainWindow.show();
    mainWindow.once("ready-to-show", () => {
        mainWindow.webContents.setZoomFactor(contentZoomFactor);
        mainWindow.show();     // must be called here 
        // Open the DevTools
        let env = getEnv();
        if (env && env.name == 'development') {
          mainWindow.webContents.openDevTools();
        }
        mainWindow.webContents.openDevTools({ mode: 'detach' });
    });
    //&*&*&*G1_MOD

  if (process.platform == 'win32') {
    // Trap iOS redirect URL so that it doesn't show the Windows native
    // Open With Application dialog box.
    // TODO: We may have to enable this code for OSX too.
    protocol.registerHttpProtocol(
        'com.kensington.kensingtonworks',
        (req, cb) => {
            // do nothing;
            //console.log('Google Redirect URL:', req.url);
        });
  }

}

function initApp() {
  
  //&*&*&*G1_ADD
    if (process.platform == 'win32') {
        var apppath = path.dirname(app.getPath('exe'));
        apppath = path.join(apppath, "/tbwhelper.exe");
        const fs = require('fs');
        try {
            if (fs.existsSync(apppath)) {
                //shell.openPath("C:\\Program Files (x86)\\Kensington\\KensingtonWorks2\\tbwhelper.exe");
                shell.openPath(apppath)
                .then(() => {
                  console.log("The file of tbwhelper.exe exists.");
                })
                .catch((err: Error) => {
                  safeMessageBox(err.message, 'error');
                  app.quit();
                })
                .finally(() => {
                  console.log("node: ", process.versions.node);
                  console.log("node nodules: ", process.versions.modules);
                  console.log("node electron: ", process.versions.electron);
                  
                  initAppAfterShellOpen();
                });
                return;
            } else {
                console.log(apppath + ' does not exist.');
            }
        } catch (err) {
            console.error(err);
        }

        console.log("node: ", process.versions.node);
        console.log("node nodules: ", process.versions.modules);
        console.log("node electron: ", process.versions.electron);
    }
    
    initAppAfterShellOpen();
}

function initAppAfterShellOpen() {
  //&*&*&*G2_ADD
  initI18n()
  .then(t => {
    initI18Funcs(t);
    /*
    return isProcessRunning('tbwhelper.exe', 'KensingtonWorksAgent', 'KensingtonWorksAgent')
  }).then(running => {
    if (!running) {
      return Promise.reject(Error(loadString('HelperNotRunning')));
    }
    */
    setupGlobals();
    return enumerateDevices();
  }).then(() => {
    createWindow();
  }).catch((err: Error) => {
    safeMessageBox(err.message, 'error');
    app.quit();
  });
}


    // *&*&*&V1_20201026 try javascript addon here...
    //console.log('----------------load c++ addon--------------------');
    //console.log("node: ", process.versions.node); 
    //console.log("node nodules: ", process.versions.modules);
    //console.log("node electron: ", process.versions.electron);
    //TrackballWorks.initCmdService();
   // var addon = require('bindings')('commandServer.node')
   // const num = addon.Btn1Set(100); // the name must be identical with the exported name of library
   // console.log('----------------load c++ addon get back-----------');
    //const num = addon.process('somefile.txt');
   // console.log(num);
    //console.log('This should be eight:', addon.add(3, 5))






// for some reason  requestSingleInstanceLock() causes the app to crash
// during startup on Mac. So the solution is to use this on Windows
// and on Mac add <key>LSMultipleInstancesProhibited<key><true/> to the 
// app Info.plist to prevent multiple instances.
if (process.platform != 'darwin') {
  // Prevent multiple instance. Also if a second instance is launched,
  // bring the window of the existing instance to the foreground.
  const gotTheLock = app.requestSingleInstanceLock();
  if (!gotTheLock) {
    //console.log('Another instance already exists!');
    app.quit();
  } else {
    app.on('second-instance', (event, commandLine, workingDirectory) => {
      if (mainWindow) {
        if (mainWindow.isMinimized()) mainWindow.restore()
        mainWindow.focus()
      }
    })

    // This method will be called when Electron has finished
    // initialization and is ready to create browser windows.
    // Some APIs can only be used after this event occurs.
    app.on("ready", initApp);
  }
} else {
  app.on("ready", initApp);
}

// Quit when all windows are closed.
app.on("window-all-closed", () => {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", () => {
  // On OS X it"s common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow();
  }
});

/**
 * Update from renderer process that settings has been restored.
 * which informs the renderer to refresh its settings. If a trackball
 * overview page is active, it will refresh its button bindings and
 * pointer & scroll setting values in this message handler.
 */
ipcMain.on('refresh-settings', (event: any, pkg: any) => {
  //console.log('refresh-settings');
  let windows = BrowserWindow.getAllWindows();
  let mainWindow = windows && windows.length > 0 ? windows[0] : null;
  if (mainWindow) {
      mainWindow.webContents.send('refresh-settings');
  }
});

// Page navigation IPC handlers

// IPC message to load home page
ipcMain.on("goto-home", () => {
  gotoHome();
});

// IPC message to go to device config page
ipcMain.on("goto-device-config", (event: any, deviceId: number) => {
  gotoDeviceConfigPage(deviceId);
});

// IPC message to go to device tutor page
ipcMain.on("goto-device-tutor", (event: any, deviceId: number) => {
  gotoDeviceTutorPage(deviceId);
});

// end page navigation IPC handlers

// In this file you can include the rest of your app"s specific main process
// code. You can also put them in separate files and require them here.
