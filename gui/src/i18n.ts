/** Initialize the i18 subsystem */
import * as path from "path";
import * as i18next from "i18next";
declare var i18nextBackend: any;
var i18nextBackend = require('i18next-node-fs-backend');
import { AppConfig } from "./app.config";
import { remote, app } from "electron";
import * as fs from "fs";
import * as settings from "electron-settings";
import { getEnv } from "./helper";

function languageSelectorPlugin() {
    let preferredLanguage = '';
    if (settings.has("preferredLanguage")) {
        preferredLanguage = settings.get('preferredLanguage').toString();;
    }
    return {
        type: 'languageDetector',
        async: false, // If this is set to true, your detect function receives a callback function that you should call with your language, useful to retrieve your language stored in AsyncStorage for example
        init: function(services: any, detectorOptions: any, i18nextOptions: any) {
          /* use services and options */
        },
        detect: function(callback: any) { // You'll receive a callback if you passed async true
          /* return detected language */
          // callback('de'); if you used the async flag
          //console.log('languageSelector.detect - language:', preferredLanguage);
          return preferredLanguage;
        },
        cacheUserLanguage: function(lng: string) {
          /* cache language */
          //console.log('languageSelector.cacheUserLanguage');
        }
    }
}

// initialize i18next
export function initI18n(localeRoot: string = null) : Promise<void>
{
    let root = __dirname;
    if (localeRoot) {
        root = path.join(root, localeRoot);
    }

    let preferredLanguage = '';
    if (settings.has("preferredLanguage")) {
        preferredLanguage = settings.get('preferredLanguage').toString();;
    }

    let env = getEnv();
    let saveMissing = false;
    if (env && env.name == 'development') {
        saveMissing = true;
    }

    if (!preferredLanguage || preferredLanguage == 'sys') {
        console.log('Preferred language not set, detecting from system locale');
        let locale = ''; 
        if (process && process.type == 'renderer') {
            locale = remote.app.getLocale();
        } else {
            locale = app.getLocale();
        }
        console.log('App locale:', locale);
        // If specific dialect of a language is listed in supported
        // languages, set it as the current language.
        if (AppConfig.languages.indexOf(locale) > -1) {
            console.log('\tLocale is supported!');
            preferredLanguage = locale;
        } else {
            // Remove the country code from language selector and
            // set it as the preferred language. If this language is not
            // supported, app will fallback to English.
            //console.log('\tLocale NOT supported!');
            preferredLanguage = locale.split('-')[0];
        }
    } else {
        //console.log('Preferred language set to:', preferredLanguage);
    }

    // determine locale path
    //&*&*&*G1_MOD
    //let loadPath = path.join(root, '/locales/{{lng}}/{{ns}}.json');
    //let addPath = path.join('/locales/{{lng}}/{{ns}}.missing.json');
    console.log('Preferred language set to:', preferredLanguage);
    let loadPath = path.join(root, '/locales/', preferredLanguage, '/translation.json');
    let addPath = path.join(root, '/locales/', preferredLanguage, '/translation.missing.json');
    //&*&*&*G2_MOD
    try {
        // see if external locale path is defined. If it is use that as the path.
        // We only check for the fallback language strings file
        //&*&*&*G1_MOD
        //fs.readFileSync(path.join(root, '../../locales/en/translation.json'));
        fs.readFileSync(path.join(root, '/locales/en/translation.json'));
        //&*&*&*G2_MOD
        // External locales folder found, change loadPath & addPath to point to
        // that.
        //console.log('External "locales" folder exists, using that..');
        //&*&*&*G1_MOD
        //loadPath = path.join(root, '../../locales/{{lng}}/{{ns}}.json');
        //addPath = path.join(root, '../../locales/{{lng}}/{{ns}}.missing.json');
        loadPath = path.join(root, '/locales/', preferredLanguage, '/translation.json');
        addPath = path.join(root, '/locales/', preferredLanguage, '/translation.missing.json');
        //&*&*&*G2_MOD
    } catch (error) {
        console.log('Error loading en/translation.json from external "locales", using built-in.');
    }
    console.log('initI18n - language:', preferredLanguage, ', loadPath:', loadPath);
    // i18next library has bad typings. So tell tsc to ignore the type mismatch.
    // @ts-ignore
    return i18next
        // @ts-ignore
        .use(i18nextBackend)
        // @ts-ignore
        .use(languageSelectorPlugin())
        // @ts-ignore
        .init({
            backend: {
                // path where resources get loaded from
                loadPath: loadPath,
                // path to post missing resources
                addPath: addPath,
                // jsonIndent to use when storing json files
                jsonIndent: 2,
            },
            interpolation: {
                escapeValue: false
            },
            debug: false,
            saveMissing: saveMissing,
            fallbackLng: 'en',
            whitelist: AppConfig.languages,
        });
}

export function getPreferredLanguage() {
    let preferredLanguage = '';
    if (settings.has("preferredLanguage")) {
        preferredLanguage = settings.get('preferredLanguage').toString();;
    }
    return preferredLanguage;
}

export function setPreferredLanguage(lang: string) {
    if (AppConfig.languages.indexOf(lang) > -1 || lang == 'sys') {
        console.log("Setting preferred language to:", lang);
        //console.log('\tLocale is supported!');
        settings.set('preferredLanguage', lang);
    } else {
        console.warn("Invalid language specified for preferred language");
    }
}
