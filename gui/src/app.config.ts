/*
module.exports = {
    platform: process.platform,
    port: process.env.PORT ? process.env.PORT : 3000,
    title: 'Hello World!',
    languages: ['en', 'el'],
    fallbackLang: 'en',
    namespace: 'translation'
};
*/
export class AppConfig {
    static platform = 'win32';
    static port = 3000;
    static title = 'Hello World';
    static languages: string[] = [
        'en', 'de', 'fr', 'nl', 'es', 'it', 'pt', 'dk',
        'no', 'se', 'fi', 'ee', 'lv', 'lt', 'pl', 'cz',
        'sz', 'hu', 'ro', 'ru', 'ua', 'kz', 'tr', 'gr',
        'ar', 'zh-TW', 'zh-CN', 'kr',
    ];
    static fallbackLang: string = 'en';
    static namespace = 'translation';
}
