/**
 *
 */
export interface MenuItemHandler {
    loadFn: (button: number, command: string, params: any, selectedRadio: any) => void;
    validatorFn: (command: string) => boolean,
    storeFn: (app:string, button: number, command: string, element: any) => {},
    labelFn: (command: string, params: any, t: (val:string) => string) => string;
};
