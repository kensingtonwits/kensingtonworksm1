// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.

import * as $ from "jquery";
import * as process from 'process';
import { initI18n } from "./i18n";

$(document).ready(() => {

    $('#chrome-version').text(process.versions.chrome);
    $('#electron-version').text(process.versions.electron);
    $('#node-version').text(process.versions.node);

    initI18n()
        .then((t) => {
            // @ts-ignore
            $("#greet").html(t('greeting'));
        }).catch((err) => {
            console.log("Error initializing i18next library, err: ", err);
        });
});
