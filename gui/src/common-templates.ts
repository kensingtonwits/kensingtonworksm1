/**
 * HTML template fragments that can be used for all product families.
 */
import * as os from "os";

let META_KEY_TEMPLATE = '';
let ALT_LABEL = 'Alt';
let ALT_CODE = 'alt'
if (os.platform() == "win32") {
    META_KEY_TEMPLATE = `
<div class="keysbox win">
  <label>
    <input class="choose" type="radio" name="keys">
    <p class="choosename">Win</p>
  </label>
</div>
`;
  ALT_LABEL = 'Alt';
  ALT_CODE = 'alt';
} else {
    META_KEY_TEMPLATE = `
<div class="keysbox cmd">
  <label>
    <input class="choose" type="radio" name="keys">
    <p class="choosename">Cmd</p>
  </label>
</div>
`
  ALT_LABEL = 'Opt';
  ALT_CODE = 'alt';
}

// Modifier key template. Note that depending on the platform
// this template adds a Win or Cmd key
export const MODIFIER_KEY_TEMPLATE = `
<p class="set_title">Modifier Keys</p>
<div class="modifier_keys">
  ${META_KEY_TEMPLATE}
  <div class="keysbox ${ALT_CODE}">
    <label>
      <input class="choose" type="radio" name="keys">
      <p class="choosename">${ALT_LABEL}</p>
    </label>
  </div>
  <div class="keysbox shift">
    <label>
      <input class="choose" type="radio" name="keys">
      <p class="choosename">Shift</p>
    </label>
  </div>
  <div class="keysbox ctrl">
    <label>
      <input class="choose" type="radio" name="keys">
      <p class="choosename">Ctrl</p>
    </label>
  </div>
</div>
<p class="set_word"></p>
`;

// Label template
export const LABEL_TEMPLATE = `
<p class="set_title">Label</p>
<input class="text" type="text" name='label' placeholder="" value="">
`;