import * as $ from 'jquery';
import { MenuItemHandler } from "./menu-item-handler";
import { keyCodeNameMap } from "./keycodenames";
import { modifierButtonsFromStr, modifierButtonsToStr } from "./modifier-buttons";
import { getKeyCodeFromHTMLCode, getHTMLCodeFromKeyCode, isValidShortcutKey } from "./mac-keycodes";
//&*&*&*G1_ADD
const electron = require('electron');
const console = require('console');
//&*&*&*G2_ADD

// prototype of the data-shortcut that we maintain for the shortcut key
interface ShortcutKeyParams {
    keyCode: number,
    modifiers: string,
    code: string
};

// For eg
/**
 * Converts keyboard shortcut to its equivalent string representation.
 * For eg.:- given keyCode = 65, modifiers = 'ctrl', returns
 *  'Ctrl+A' (keyCode 65 is A character on ANSI keyboard).
 *
 * @param keyCode number The scancode for the keyboard key
 * @param modifiers string modifier combination such as 'alt,ctrl,shift,cmd,win'
 */
function keyboardShortCutToString(keyParams: ShortcutKeyParams): string {
    let str = '';
    keyParams.modifiers.split(',').forEach((val, index, ar) => {
        if (val) {
            str += str.length > 0 ? '+' : '';
            // Darwin we have to map 'alt' to 'opt'. Underlying helper still uses 'alt'
            // to refer to 'Option' key in Mac, but user wants to see 'Opt' So we hard code
            // it here.
            if (process.platform == 'darwin' && val == 'alt') {
                val = 'opt';
            }
            str += val[0].toUpperCase() + val.substr(1);
        }
    })
    // convert keyCode to its equivalent string
    str += str.length > 0 ? '+' : '';
    if (keyParams.keyCode >= 0) {
        str += keyParams.code; //keyCodeNameMap[keyCode];
    }
    return str;
}

class KeyboardShortcutActionHandlers implements MenuItemHandler {
    loadFn(button: number, command: string, params: any, selectedRadio: any) {
        // load the shortcut label & key itself into the relevant input fields
        console.log('Shortcut params: ', params);
        let $shortcutKey = $('#id-shortcut-key');

        params.code = getHTMLCodeFromKeyCode(params.keyCode);
        //console.log()

        $shortcutKey.data('shortcut', params);
        $shortcutKey.val(
            keyboardShortCutToString(params)
        );
        let label = '';
        if (params && params.hasOwnProperty('label')
            && params.label) {
            label = params.label;
        }
        $(selectedRadio).find('input[name=\'label\']').val(label);
        // Set the state of corresponding modifier buttons
        let modKeysComponent = $shortcutKey.parent().find(
            'div.modifier_keys')[0];
        modifierButtonsFromStr(params.modifiers, modKeysComponent);
    };
    validatorFn(command: string) {
        let shortcut = $('#id-shortcut-key').data('shortcut');
        if (shortcut
            && shortcut.hasOwnProperty('keyCode')
            && shortcut.hasOwnProperty('modifiers')
            && shortcut.keyCode >= 0) {
            return true;
        }
        return false;
    };
    storeFn(app:string, button: number, command: string, element: any) {
        let params: ShortcutKeyParams = $('#id-shortcut-key').data('shortcut');
        // translate HTML keyCode to Apple KeyCode
        params.keyCode = getKeyCodeFromHTMLCode(params.code, params.keyCode);

        let label = $(element).find('input[name=\'label\']').val();
        let helperParams = {
            keyCode: params.keyCode,
            label:  label ? label : '',
            modifiers: params.modifiers
        }
        return helperParams;
    };
    labelFn(command: string, params: any, t: (val:string) => string) {
        // if user has specified a label, return it.
        if (params
            && params.hasOwnProperty('label')
            && params.label) {
            return params.label;
        }
        params.code = getHTMLCodeFromKeyCode(params.keyCode);
        return keyboardShortCutToString(params);
    };
}

export function bindKeyboardShortcutActionHandlers(t: any, actionHandlers: Map<RegExp, MenuItemHandler>) {

    /**
     * Form current modifier key combination status from the respective button
     * toggle state & update id-shortcut-key data-shortcut. Together with
     * the modifier key status & the key pressed, if any, create a friendly
     * label and and update the shortcut entry field to reflect this.
     */
    function updateShortcutModifiersAndLabel() {
        let keyData: ShortcutKeyParams = { // this is the data we send to tbwhelper
            keyCode: -1,
            modifiers: "",
            code: ""
        };
        let $shortcutKey = $('#id-shortcut-key');
        if ($shortcutKey.data('shortcut')) {
            keyData = $shortcutKey.data('shortcut');
        }
        let modKeysComponent = $shortcutKey.parent().find(
            'div.modifier_keys')[0];
        keyData.modifiers = modifierButtonsToStr(modKeysComponent);
        let label = keyboardShortCutToString(keyData);
        $shortcutKey.val(label);
        $shortcutKey.data('shortcut', keyData);
        // console.log('Shortcut key:', keyData, ', label:', label);
    }

    // 'hotkey' sensor mechanism for keyboard shortcut entry field
    $("#id-shortcut-key").on("keydown", (e) => {
        e.preventDefault();

        console.log('KeyCode:', e.keyCode, ", code:", (<any>e).code);
        let keyData: ShortcutKeyParams = { // this is the data we send to tbwhelper
            keyCode: -1,
            modifiers: "",
            code: ""
        };
        let $shortcutKey = $(e.target);
        if ($shortcutKey.data('shortcut')) {
            keyData = $shortcutKey.data('shortcut');
        }
        if (!isValidShortcutKey((<any>e).code)) {
            if (e.keyCode == 91 || e.keyCode == 17 || e.keyCode == 92 || e.keyCode == 16 || e.keyCode == 44 || e.keyCode == 13)
            { ;}
            else
                return;
        }
        /*
        let keysToExclude = [
            "NUMLOCK", "SCROLLLOCK", "PAUSE", "CONTEXTMENU", "CAPSLOCK", "ENTER",
            "BACKSPACE",
            "Control", "Shift", "Alt", "Meta"
        ];
        let isExcludedKey = keysToExclude.find((element) => {
            return element.localeCompare(e.key, undefined, { sensitivity: 'accent' }) === 0;
        });
        // We can use unicode characters to display some keys such as ARROWLEFT,
        // ARROWRIGHT, etc
        if (!isExcludedKey) {
            keyData.keyCode = e.keyCode;
            keyData.code = (<any>e).code;
        }
        */
        keyData.keyCode = e.keyCode;
        keyData.code = (<any>e).code;

        //console.log('KeyData:', keyData.keyCode, ", code:", keyData.code);

        $(e.target).data('shortcut', keyData);
        updateShortcutModifiersAndLabel();
    });

    $("#id-shortcut-key").parent().on('click', 'div.modifier_keys div.keysbox', (event) => {
        // Weird code, but is necessary. Reason is we have to let the default
        // click handlers for the modifier key buttons to be executed first
        // so that it'll set the 'click' attribute to the relevant button.
        // It's this 'click' css style that we look for in deciding whether
        // a modifier key is required or not. So calling updateShortcutLabel
        // via setTimeout ensures that the default handlers are executed and
        // 'updateShortcutLabel' will be executed when event loop has processed
        // all pending user input events.
        //
        // Note that this is a single call timer function. Not setInterval()!
        setTimeout(() => updateShortcutModifiersAndLabel(), 0);
    });

    actionHandlers.set(new RegExp('keyboard_shortcut'), new KeyboardShortcutActionHandlers());
}
