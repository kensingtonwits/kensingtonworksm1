import { remote, ipcRenderer, app } from "electron";
import * as $ from "jquery";
import * as os from "os";
import * as path from "path";
import { initI18n } from "./i18n";
import "foundation-sites";
import * as mscroll from "malihu-custom-scrollbar-plugin";
import { TrackballDevice, TrackballWorks } from "./tbw";
import {
  initDimensions, initMenu, initScrollbar, setupGlobalUIHandlers,
  updateProductVersion
} from "./all";
import {
  getUrlParam, SupportedDevice, getSupportedDeviceInfo, loadTemplate,
  localizeStrings,
  initI18Funcs,
  loadString,
  loadTemplateString
} from "./helper";
import { MenuItemHandler } from "./menu-item-handler";
import { bindLaunchingHandlers } from "./launching-handlers";
import { bindKeyboardShortcutActionHandlers } from "./keyboard-shortcut-handler";
import { bindGenericActionHandlers } from "./generic-handler";
import { bindSnippetActionHandlers } from "./snippet-handler";
import { bindMouseButtonActionHandlers } from "./mouse-button-handlers";
import { bindScrollActionHandler } from "./scroll-handler";
import { MODIFIER_KEY_TEMPLATE, LABEL_TEMPLATE } from "./common-templates";
import { initBackupRestoreHandlers } from "./BackupRestoreHandlers";
import { showAlertMessage } from "./popups";
import { execSync } from "child_process";

//&*&*&*G1_ADD
const electron = require('electron');
const console = require('console');
//&*&*&*G2_ADD


// Path to locate the product button div area. We add an id to the div to make
// it faster to locate it. (Browser does this natively as it maintains a table
// for elements with an id). jQuery internally uses document.querySelectorAll()
// which would look up this table first if an id selector is specified.
// Refer to https://learn.jquery.com/performance/optimize-selectors/#id-based-selectors
const PRODUCTBUTTON_PATH = '#id-productbutton';

namespace Trackball {

function setupTrackballConfigHandlers() {
    $(document).on('mouseenter', '.pb', function(){
        var hover = $(this).data('hover');
        $(this).addClass('hover');
        $('.productimg[data-hover=' + hover + ']').addClass('hover');
    });
    $(document).on('mouseleave', '.pb', function(){
        var hover = $(this).data('hover');
        $(this).removeClass('hover');
        $('.productimg[data-hover=' + hover + ']').removeClass('hover');
    });
    $(document).on('click', '.pb', function(){
      if($(this).hasClass('click') == false){
        $('.pb').removeClass('click');
        $(this).addClass('click');
        $('.content_main').addClass('open');
        $('.content_regulate').addClass('open');
      }else{
        $('.pb').removeClass('click');
        $('.content_main').removeClass('open');
        $('.content_regulate').removeClass('open');
      }
    });
    $(document).on('click', '.trov_rightcontent.buttonbox .content_regulate .regulate_top .re_close', function(){
        $('.pb').removeClass('click');
        $('.content_main').removeClass('open');
        $('.content_regulate').removeClass('open');
    });
    $(document).on('click', '.trov_rightcontent .content_regulate .regulate_top .re_close', function(){
      $('.trov_button[data-button="button1"').click();
    });
    $(document).on('click', '.dir_button', function(){
      var click = $(this).data('click');
      $('.dir_button').removeClass('click');
      $(this).addClass('click');
      $('.trov_rightcontent.buttonbox .content_main .productarea').removeClass('click');
      $('.trov_rightcontent.buttonbox .content_main .productarea[data-click=' + click + ']').addClass('click');
    });
    $(document).on('click', '.radiobox label.chooselabel', function(){
      $('.radiobox').removeClass('click');
      $(this).parent('.radiobox').addClass('click');
    });

    // handler for buttons pane modifier key button toggle switches
    $(document).on('click', '.set_content .modifier_keys .keysbox label', function(ev){
      // $('.keysbox').removeClass('click');
      // $(this).parent('.keysbox').addClass('click');
      if ($(this).parent('.keysbox').hasClass('click') == false) {
        $(this).parent('.keysbox').addClass('click');
        $(this).children('input').prop('checked', true);
      } else {
        $(this).parent('.keysbox').removeClass('click');
        $(this).children('input').prop('checked', false);
      }
      ev.preventDefault();
    });

    // handler for pointer pane modifier key button toggle switches
    $(document).on('click', '.checkarea.cont .checkbox .modifier_keys .keysbox label', function(event){
      if($(this).parent('.keysbox').hasClass('click') == false){
        $(this).parent('.keysbox').addClass('click');
        $(this).children('input').prop('checked', true);
      }else{
        $(this).parent('.keysbox').removeClass('click');
        $(this).children('input').prop('checked', false);
      }
      event.preventDefault();
    });
    $(document).on('click', '.content_box , .searchbtn', function(){
      var set = $(this).data('set');
      $('.content_box').removeClass('click');
      $(this).addClass('click');
      $('.re_right').removeClass('click');
      $('.re_right[data-set=' + set + ']').addClass('click');
    });
    // restore button settings to defaults
    $("#id-reset-all").on('click', resetAllHandler);
    $("#id-restore-defaults-buttons").on('click', resetButtonsHandler);
    $("#id-cancel-restore-defaults").on('click', cancelResetDefaultsPopup);

    $(document).on('click', '.trov_button', function(event){

      // Prevent endless loop of click events that can happen
      // when click is called on an already clicked item.
      let curPane = $('div.trov_button.click');
      if (curPane.length) {
        let $curPane = $($('div.trov_button.click')[0]);
        let $clickedPane = $($(event.target).parents('div.trov_button')[0]);
        if ($curPane.data('pane') == $clickedPane.data('pane')) {
          event.preventDefault();
          return;
        }
      }
      var button = $(this).data('button');
      var pane = $(this).data('pane');
      $('.trov_button').removeClass('click');
      $(this).addClass('click');
      $('.trov_rightcontent').removeClass('click');
      $('.trov_rightcontent[data-button=' + button + ']').addClass('click');
      if (pane == 'buttons') {
        // for button assignments, we just need to close the Action menu.
        $('.pb').removeClass('click');
        $('.content_main').removeClass('open');
        $('.content_regulate').removeClass('open');
      } else if (pane == 'pointer') {
        loadPointerConfig();
      } else if (pane == 'scrolling') {
        loadScrollConfig();
      }
    });
    $(document).on('click', '.checkbox', function(){
      if($(this).children('.checkstyle').hasClass('click') == false){
        $(this).children('.checkstyle').addClass('click');
        $(this).prev('input').click();
        $(this).children('.checkstyle').addClass('click');
        $(this).parent('.checkarea').next('.checkarea.cont').addClass('click');
      }else{
        $(this).children('.checkstyle').removeClass('click');
        $(this).parent('.checkarea').next('.checkarea.cont').removeClass('click');
        $(this).prev('input').click();
      }
    });
    $(document).on('click', '.reversebox.left', function(){
      $('.reversebox').removeClass('click');
      $(this).addClass('click');
      $('.reversebox.reverse').addClass('click');
    });
    $(document).on('click', '.reversebox.right', function(){
      $('.reversebox').removeClass('click');
      $(this).addClass('click');
      $('.reversebox.normal').addClass('click');
    });
    $(document).on("keydown", (e) => {
      if (e.keyCode == 9) {
        //console.log('Blocking TAB key input in buttonbox');
        e.preventDefault();
      }
    });
}

// Returns the TrackballDevice object matching the given product id
function getTrackballDevice(id: number) : TrackballDevice {
  let kwConfig = remote.getGlobal('kwConfig');
  if (kwConfig) {
      let devices: Map<number, TrackballDevice> = kwConfig.devices;
      return devices.get(id);
  }
  throw Error("Device not found!");
}

// page global TrackballDevice instance.
let _device: TrackballDevice = null;
let _curApp: string = "";
let _di: SupportedDevice = null;
let _t: any = null; // i18next.t function

// This is a map of command names to their corresponding validator functions
// Each validator function is supposed to return a boolean indicating if the
// user input for the corresponding command are complete and valid. If the
// function returns false, Save would fail.
//
// During Save the map of validator functions are searched for a matching
// command name and if one is found, the function is executed and further
// searching for matching functions are not done.
let _menuItemActionHandlers = new Map<RegExp, MenuItemHandler>();

/**
 * Given a command name, returns a matching MenuItemActionHandler object.
 * @param command string Command name
 */
function getMenuItemActionHandler(command: string): MenuItemHandler {
  let actionHandlers: MenuItemHandler = null;
  for (const key of _menuItemActionHandlers.keys()) {
    if (key.test(command)) {
      return _menuItemActionHandlers.get(key)
    }
  }
  return null;
}

/**
 * Returns the number of buttons assigned with the given command macro.
 * @param command string The command macro to search for.
 */
function numButtonsWithCommand(command: string, params:any = {}): number {
  let count = 0;
  $(`${PRODUCTBUTTON_PATH} .pb`).each((index, element) => {
    let buttonData = $(element).data();
    if (buttonData.hasOwnProperty('command')
      && buttonData.command == command) {

      // KLUDGE for handling left_click modifiers
      let paramModifiers = '';
      if (params.hasOwnProperty('modifiers')) {
        paramModifiers = params.modifiers ? params.modifiers : '';
      }
      let buttonModifiers = '';
      if (buttonData.hasOwnProperty('params')
        && buttonData.params
        && buttonData.params.hasOwnProperty('modifiers')) {
        buttonModifiers = buttonData.params.modifiers;
      }
      if (paramModifiers == buttonModifiers) {
        count += 1;
      }
    }
  });
  return count;
}

/**
 *
 * @param hoverIndex number Hover index used by the GUI to distinguish buttons
 * @param button number Button mask
 * @param command string Command string
 * @param params any Optional command parameters
 */
function setButtonLabelAndData(hoverIndex: number, button: number, command: string, params: any) {
  //console.log(`setButtonLabelAndData - hi: ${hoverIndex}, button: ${button}, command: ${command}`);
  // convert command macro to its equivalent string representation

  let ah = getMenuItemActionHandler(command);
  let commandLabel = ah.labelFn(command, params, _t);
  //console.log('commandLabel:', commandLabel);
  let jbutton = $(`${PRODUCTBUTTON_PATH} .pb.hover${hoverIndex}[data-button-mask="${button}"]`);
  jbutton.find('span.s_name').text(commandLabel);
  jbutton.find('span.lock').text(commandLabel);
  jbutton.data({command: command, params: params});
  let data = jbutton.data();
  //console.log('button data: ', JSON.stringify(data));
}

/**
 * Set/clear button's programmable lock message, depending on if there'is
 * 1 or more buttons set to mouse_leftClick command.
 */
function setLeftClickLockMessage(): void {
  let leftClickButtons = numButtonsWithCommand("mouse_leftClick");
  //console.log('# of Left Click assignments:', leftClickButtons);

  $(`${PRODUCTBUTTON_PATH} .pb`).each((index, element) => {
    let jbutton = $(element);
    let buttonCommand = jbutton.data('command');
    let params = jbutton.data('params');
    if (leftClickButtons > 1) {
      // remove any <p class='lockword'></p> children
      let lockMsgElem = jbutton.find('p.lockword');
      if (lockMsgElem.length) {
        lockMsgElem.remove();
      }
    } else if (buttonCommand == 'mouse_leftClick'
      && (!params || !params.hasOwnProperty('modifiers') || !params.modifiers)) {
      let lockMsg = loadString('leftRightClick_lockword');
      jbutton.find('.hover_center').append(`<p class='lockword'>${lockMsg}<p>`);
    } else {
      // remove any lockword, if one is present
      jbutton.find('.hover_center p.lockword').remove();
    }
  });
}

// loads all the apps and appends each to the app list menu
function loadApps() {
  _device.getApps()
  .then(apps => {
    let appList: any[] = apps.apps;
    appList.forEach((value, index, array) => {
      for (const key in value) {
        if (value.hasOwnProperty(key) && key != '*') {
          const name = value[key];
          addAppToMenu(name, key);
        }
      }
    });
  })
}

/**
 * Load snippets and update the snippet menu items
 */
function loadSnippets() {
  _device.getSnippets(_curApp)
  .then((data: any) => {
    updateSnippetMenuItems(data);
  }).catch(err => {
    // TODO: error handling
    console.log('Error getting snippets: ', err);
  });
}

/**
 * Loads action assignments of all buttons for the current device and
 * initializes the relevant UI widget for each.
 */
function loadButtonAssignments() {
  _device.getButton(_curApp, 0)
  .then((data: any) => {
    updateButtonAssignmentsUI(data);
  }).catch(err => {
    // TODO: error handling
    console.log('Error getting button assigments: ', err);
  })
}

/**
 * Load snippet menu items with current snippets list.
 *
 * @param snippets snippets array, each of which is of the form:
 *  { content: '', label: '' }
 */
function updateSnippetMenuItems(snippets: Array<any>) {
  //console.log('updateSnippets:', snippets);
  snippets.forEach((snippet, index) => {
    try {
      let $snippetRadio = $($(`.re_right[data-set='set2'] div.radiobox[data-index='${index}']`)[0]);
      $snippetRadio.find('textarea').val(snippet.content);
      // <input class="text" type="text" name="label" placeholder="" value="">
      $snippetRadio.find('input[name="label"]').val(snippet.label);
    } catch (error) {
      console.log('updateSnippets error:', error);
    }
  });
}

/**
 * Shows the given button assignments on the relevant UI element.
 *
 * @param btnAssignments any Button assignments object where each
 *    button's action assignment object is indexed by its mask.
 */
function updateButtonAssignmentsUI(btnAssignments: any) {
  // for each button defined in the device info, find the assigned action
  // and display it
  // TODO: check for sideview impact
  function _updateButton(btnAssignments: any, hoverIndex: number, buttonMask: number) {
    let assignDefault = true;
    let action = Reflect.get(btnAssignments, buttonMask);
    if (action) {
      //console.log('assignment exists for ', buttonMask);
      // assignment exists
      const element = btnAssignments[buttonMask];
      let command: string = "";
      let params: any = null;
      if (element.hasOwnProperty('command')) {
        command = element.command;
      }
      if (element.hasOwnProperty('params')) {
        params = element.params;
      }
      if (command) {
        setButtonLabelAndData(hoverIndex, buttonMask, command, params);
        assignDefault = false;
      }
    }

    if (assignDefault) {
      // no assignment, default to No Action
      //console.log('no assignment exists for ', buttonMask);
      setButtonLabelAndData(hoverIndex, buttonMask, "noAction", null);
    }
  }
  if (_di.topView) {

  }
  if (_di.topView && _di.hasOwnProperty('topButtons')) {
    _di.topButtons.forEach((val, index, ar) => {
      _updateButton(btnAssignments, val.hoverIndex, val.mask);
    })
  }

  if (_di.sideView && _di.hasOwnProperty('sideButtons')) {
    _di.sideButtons.forEach((val, index, ar) => {
      _updateButton(btnAssignments, val.hoverIndex, val.mask);
    });
  }
  setLeftClickLockMessage();
}

/**
 * Checks/unchecks the specified checkbox control.
 *
 * @param id string id of the checkbox input control, without the leading '#'
 * @param checked boolean checked state boolean flag
 */
function setCheckBoxState(id: string, checked: boolean): void {
  let jqId = `#${id}`;
  $(jqId).prop("checked", checked);
  if (checked) {
    $(jqId).next('.checkbox').find('.checkstyle').addClass('click');
    $(jqId).parent().next().addClass('click');
  } else {
    $(jqId).next('.checkbox').find('.checkstyle').removeClass('click');
    $(jqId).parent().next().removeClass('click');
  }
}

/**
 * Returns the trigger modifier key combination for Slow Pointer & Locked
 * Axis movement features. Returned string will be of the form:
 *    "alt,ctrl,cmd,shift"
 * depending on the keys selected.
 *
 * @param idPrefix string The input id prefix string to which different modifier
 * keys are appened to determine the toggle state of each modifier key.
 */
function getModifierKeyString(idPrefix: string): string {
  let modifiers = "";
  let keys = [ "alt", "ctrl", "shift", "cmd" ];

  keys.forEach((val, index, ar) => {
    let id = `#${idPrefix}-${val}`;
    if ($(id).is(":checked")) {
      if (modifiers.length > 0) {
        modifiers += ',';
      }
      modifiers += val;
    }
  });
  return modifiers;
}

function setModifiersKeyButtonState(idPrefix: string, modKeys: string): void {
  let keys = [ "alt", "ctrl", "shift", "cmd" ];
  keys.forEach((val, index, ar) => {
    let id = `#${idPrefix}-${val}`;
    // set the checked state for the checkbox input control
    let checked = modKeys.search(val) != -1;
    $(id).prop('checked', checked);
    // set the selected state for the corresponding div
    if (checked) {
      $(id).closest(".keysbox").addClass('click');
    } else {
      $(id).closest(".keysbox").removeClass('click');
    }
  });
}

function loadPointerConfig() {
  _device.getPointerConfig(_curApp)
  .then((data: any) => {
    console.log('Pointer config:', data);
    updatePointerConfigUI(data);
  }).catch(err => {
    // TODO: error handling
    console.log('Error retrieving pointer config:', err);
  })
}

/**
 * Update UI elements for the given pointer config.
 */
function updatePointerConfigUI(pointerConfig: any) {
  $("#pointer-speed-slider").val(pointerConfig.speed/10);
  setCheckBoxState("pointer-accel-checkbox", pointerConfig.acceleration);
  $("#pointer-accel-slider").val(pointerConfig.accelerationRate/10);

  // reflect single axis movement modifier key status
  //data.lockedAxisModifiers = "shift,ctrl"; // for testing
  setCheckBoxState('single-axis-checkbox', pointerConfig.lockedAxisModifiers.length > 0);
  setModifiersKeyButtonState("single-axis-key", pointerConfig.lockedAxisModifiers);

  // reflect slow pointer modifier key state
  //data.slowModifiers = "alt,shift"; // for testing
  setCheckBoxState('slow-pointer-checkbox', pointerConfig.slowModifiers.length > 0);
  setModifiersKeyButtonState("slow-pointer-key", pointerConfig.slowModifiers);
}

function loadScrollConfig() {
  _device.getScrollConfig(_curApp)
  .then((data: any) => {
    //console.log("Scroll config:", data);
    updateScrollConfigUI(data);
  }).catch((err) => {
    // TODO: error handling
    console.log('Error retrieving scroll config:', err);
  })
}

function updateScrollConfigUI(config: any) {
  $("#scroll-speed-slider").val(config.speed);
  setCheckBoxState('inertial-scroll-checkbox', config.inertial);
  // data.invert = true; // for testing only
  if (config.invert) {
    $("#normal-scroll-radio").removeClass('click');
    $("#reverse-scroll-radio").addClass('click');
  } else {
    $("#reverse-scroll-radio").removeClass('click');
    $("#normal-scroll-radio").addClass('click');
  }
}

/**
 * Clears any current button menu selections.
 */
function clearButtonActionMenuSelections() {
  $('div.regulate_center div.radiobox.click').removeClass('click');
  $('div.regulate_center div.re_right.click').removeClass('click')
  $('div.re_left div.content_box.click').removeClass('click');
}

/**
 * Given a deepest level menu item, function selects the item and all its
 * parent menu items until the root menu item corresponding to the specified
 * deepest level menu item.
 *
 * @param finalMenuItem element The menu item element corresponding to the
 * last level in the menu hierarchy.
 */
function selectButtonActionMenuItem(finalMenuItem: any) {
  // Select the relevant radiobox, its parent div.re_right and its parent menu
  // div.content_box elements.
  //let finalRadioBox = $(`div.regulate_center div.re_right div.radiobox[data-command='${command}']`);
  let finalRadioBox = $(finalMenuItem);
  finalRadioBox.addClass('click');
  let reRightElem = finalRadioBox.closest('div.re_right');
  reRightElem.addClass('click');
  let setId = reRightElem.data('set');
  finalRadioBox.closest('div.regulate_center').find(`div.content_box[data-set="${setId}"]`).addClass('click');
}

/**
 * Close the trackball button assignment menu
 */
function closeButtonActionMenu() {
  $("#id-close-button-action-menu").click();
}

/**
 * Update the menu for the selected button's action binding
 *
 * @param button HTMLElement HTML element of the selected button.
 */
function syncButtonActionMenu(button: any)
{
  let jButton = $(button);
  let command: string = jButton.data('command');
  let params: any = jButton.data('params');
  //console.log('syncButtonActionMenu:', command, params);

  let finalRadioBox = null;
  if (command == 'snippet') {
    finalRadioBox = $(`div.regulate_center div.re_right div.radiobox[data-command='${command}'][data-index='${params.index}']`)[0];
  } else {
    finalRadioBox = $(`div.regulate_center div.re_right div.radiobox[data-command='${command}']`)[0];
  }
  clearButtonActionMenuSelections();
  selectButtonActionMenuItem(finalRadioBox);

  let ah = getMenuItemActionHandler(command);
  if (ah) {
    ah.loadFn(button, command, jButton.data('params'), finalRadioBox);
  }
}

/**
 * If the given app is different from the given app, sets the given app as the
 * current application.
 *
 * Update the relevant UI widget to reflect the current app friendly name.
 *
 * @param name string Application friendly name
 * @param appId string Application identifier
 */
function setCurrentApp(name: string, appId: string, force: boolean = false) {
  //console.log(`Select app ${name}: ${appId}`);
  if (appId != _curApp || force) {
    _curApp = appId;
    // set the title to selected app name
    $('div.h_content.app p').text(name);

    loadButtonAssignments();
    loadPointerConfig();
    loadScrollConfig();
    loadSnippets();
  }
}

/**
 * Selects All Applications from app_list.
 *
 * Does this by triggering a 'click' on the corresponding app_list item.
 */
function selectAllApplications() {
  //$("p[data-app-id='*']").click();
  $("div.app_list #mCSB_1_container p.appname[data-app-id='*']").click();
}

// hides the app list menu
function hideAppList() {
  $('.app_list').removeClass('hover');
  $('.listcover').removeClass('hover');
}

function hideFavAppList() {
  $('.app_list_more').removeClass('hover');
  $('.listcover').removeClass('hover');
}

/**
 * Select an executable (or .app in macOS) and returns its full path
 */
function getApplication() {
  let app = '';
  let options: any = null;
  if (os.platform() == "win32") {
    options = {
      defaultPath: "c:\\Program Files",
      filters: [
        { name: loadString('Executable Files'), extensions: ['exe'] }
      ],
      properties: ['openFile'],
    }
  } else { // darwin
    options = {
      defaultPath: "/Applications",
      filters: [
        { name: loadString('Applications'), extensions: ['app'] }
      ],
      properties: ['openFile']
    }
  }
  let files = remote.dialog.showOpenDialogSync(remote.getCurrentWindow(), options)
  if (files && files.length) {
    // Don't allow KensingtonWorks programs to be added to the application
    // profile list.
    let appPath = remote.app.getAppPath();
    if (appPath.indexOf('app.asar') != -1) {
      let pathComps = path.dirname(appPath).split(path.sep);
      pathComps.pop();
      pathComps.pop();
      appPath = path.join(...pathComps);
    }
    //console.log('Add app:', files[0], ', app path:', appPath);
    if (files[0].indexOf(appPath) != -1) {
      showAlertMessage(
        'KensingtonWorks',
        loadString('error_cannot_add_kensingtonworks_programs')
      );
      return '';
    }
    app = files[0]
  }
  return app;
}

/**
 * Returns a friendly name for the app
 * @param fullPath string full path to the application or EXE
 */
function getAppName(fullPath: string) {
  let parsedPath = path.parse(fullPath);
  let name = parsedPath.name;
  name.toLocaleLowerCase();
  if (os.platform() == "win32") {
    let lastExe = name.search('/\.exe$/');
    if (lastExe != -1) {
      name = name.substr(0, lastExe)
    }
  } else {
    let lastApp = name.search('/\.app$/');
    name.replace('.app', '');
    if (lastApp != -1) {
      name = name.substr(0, lastApp)
    }
  }
  return name.charAt(0).toLocaleUpperCase() + name.substr(1);
}

function addAppToMenu(name: string, fullPath: string) {
  let appMenuItem =
  `<div class="marked_content unadd">
    <p class="appname" data-app-id='${fullPath}'>${name}</p><i class="marktrash be-icon be-icon-trash"></i>
   </div>`;

   $("div.app_list #mCSB_1_container").append(appMenuItem);
   $(".scrollbar").mCustomScrollbar("update");
}

/**
 * Removes an app from the app list menu
 * @param appId string the app identifier, which is its full path
 */
function removeAppFromMenu(appId: string) {
  // $(`p[data-app-id='${appId}']`) does not work. So we have to use the
  // filter function to find the matching menu entry.
  let menuElem = $(`div.app_list #mCSB_1_container [data-app-id]`).filter((index, element) => {
    if ($(element).data("app-id") == appId) {
      return true;
    };
    return false;
  }).parent();
  menuElem.remove();
  //$(`div.app_list #mCSB_1_container p[data-app-id='${appId}']`).parent().remove();
  $(".scrollbar").mCustomScrollbar("update");
}

/**
 * 'click' handler for device button
 */
function deviceButtonSelectHandler(event: Event) {
  /**
   * We have to do two things here:-
   *
   * 1. If the user is trying to remap a default mouse button and if another
   *    button is not mapped to the same action, stop the user from
   *    remapping.
   * 2. If remapping is allowed, select the menu item corresponding to
   *    to the current action mapping.
   */
  if ($(event.target).closest('.pb').find('p.lockword').length != 0) {
    // 1. Is it a mouse default button and if so is it okay to remap?
    event.stopPropagation();
    $(event.target).closest('.pb').addClass('click');
    setTimeout(() => {
      $(event.target).closest('.pb').removeClass('click');
    }, 1200);
    return;
  }

  // 2. Synchronize the menu to select the item corresponding to the
  // current button action
  syncButtonActionMenu($(event.target).closest('.pb'));
}

/**
 * 'click' handler for Save settings <input> element for the Button config
 * pane.
 */
function saveButtonSettingsHandler(ev: Event) {
  ev.preventDefault();
  let selectedRadioBox = $('.re_right .radiobox.click')[0];
  let command: string = $($('.re_right .radiobox.click')[0]).data('command');
  if (!command) {
    return;
  }

  let actionHandlers = getMenuItemActionHandler(command);
  if (!actionHandlers || !actionHandlers.validatorFn(command)) {
    //console.log('Not valid input!');
    // TODO: how to reflect error?
    return;
  }

  let jButton = $($(`${PRODUCTBUTTON_PATH} .pb.click`)[0]);
  let button = parseInt(jButton.data('buttonMask'));
  let hover = parseInt(jButton.attr('class').split("hover")[1]);

  let params = actionHandlers.storeFn(_curApp, button, command, selectedRadioBox);
  //console.log('command: ', command, ", hoverIndex:", hover, ", button:", button, ', params:', params);
  _device.setButton(_curApp, button, command, params)
    .then((res: any) => {
      //console.log('Result: ', res);
      setButtonLabelAndData(hover, button, res.command, res.params);
      closeButtonActionMenu();
      jButton.removeClass('click');
      setLeftClickLockMessage();
    }).catch((err: any) => {
      console.warn('Error: ', err);
    });
}

/**
 * 'click' handler for Save settings <input> element for the Pointer config
 * pane.
 */
function savePointerSettingsHandler(ev: Event) {
  ev.preventDefault();
  let lockedAxisModifiers = '';
  if ($("#single-axis-checkbox").prop('checked')) {
    lockedAxisModifiers = getModifierKeyString('single-axis-key');
  }
  let slowPointerModifiers = '';
  if ($("#slow-pointer-checkbox").prop('checked')) {
    slowPointerModifiers = getModifierKeyString('slow-pointer-key');
  }
  let pointerConfig = {
    speed: parseInt(<string>$("#pointer-speed-slider").val())*10,
    acceleration: $("#pointer-accel-checkbox").prop("checked"),
    accelerationRate: parseInt(<string>$("#pointer-accel-slider").val())*10,
    lockedAxisModifiers: lockedAxisModifiers,
    slowModifiers: slowPointerModifiers
  };
  //console.log('Save pointer config:', pointerConfig);
  // TODO: show busy indicator
  _device.setPointerConfig(_curApp, pointerConfig)
  .then((config) => {
    //console.log('Pointer config updated:', config);
  }).catch((error) => {
    // TODO: what to do here?
    console.log('Error setting pointer config:', error);
  });
  $('.trov_button[data-button="button1"').click();
}

/**
 * 'click' handler for Save settings <input> element for the Scroll config
 * pane.
 */
function saveScrollSettingsHandler(ev: Event) {
  ev.preventDefault();
  let invert = $("#reverse-scroll-radio.click").length == 1;
  let scrollConfig = {
    speed: parseInt(<string>$("#scroll-speed-slider").val()),
    inertial: $("#inertial-scroll-checkbox").prop("checked"),
    invert: invert
  };
  //console.log('Save scroll config:', scrollConfig);
  // TODO: display busy indicator
  _device.setScrollConfig(_curApp, scrollConfig)
  .then((config) => {
    //console.log('Scroll config updated:', config);
  }).catch((error) => {
    // TODO: what to do here?
    console.log('Error setting scroll config:', error);
  });
  $('.trov_button[data-button="button1"').click();
}

/**
 * Given the full path to an application, returns its identifier as
 * <bundleId>-<Executable> after parsing its Info.plist.
 *
 * @param appPath Application full path
 *
 * Only applicable to OSX.
 * Assumes that /usr/libexec/PlistBuddy is available in the system.
 */
function getMacAppIdentifierFromPath(appPath: string) {  
  let bundleIdCmd = `/usr/libexec/PlistBuddy -c 'Print CFBundleIdentifier' "${appPath}/Contents/Info.plist"`;
  let executableCmd = `/usr/libexec/PlistBuddy -c 'Print CFBundleExecutable' "${appPath}/Contents/Info.plist"`;
  let bundleId = execSync(bundleIdCmd).toString();
  bundleId = bundleId.split('\n')[0]
  let executable = execSync(executableCmd).toString();
  executable = executable.split('\n')[0];
  //console.log('BundleId:', bundleId, ', executable:', executable);
  return `${bundleId}-${executable}`;
}

/**
 * Given the full path to an application, returns its name as
 * set in its Info.plist
 *
 * @param appPath Application full path
 *
 * Only applicable to OSX.
 * Assumes that /usr/libexec/PlistBuddy is available in the system.
 */
function getMacAppNameFromPath(appPath: string) {
  let nameCmd = `/usr/libexec/PlistBuddy -c 'Print CFBundleName' "${appPath}/Contents/Info.plist"`;
  let name = execSync(nameCmd).toString();
  name = name.split('\n')[0]
  //console.log('Name:', name);
  return name;
}

/**
 * 'click' handler for new application button (+ button) in the app list.
 */
function newApplicationHandler() {
  let appPath = getApplication();
  let appName = getAppName(appPath);
  if (appPath) {
    // In Mac, application identifier is <bundle_id>-<Executable>. So we
    // need to retrieve this information from the selected app path.
    if (process.platform == 'darwin') {
      let identifier = getMacAppIdentifierFromPath(appPath);
      let name = getMacAppNameFromPath(appPath);
      if (identifier) { appPath = identifier; }
      if (name) { appName = name };
    }
    _device.createApp(appName, appPath)
    .then((apps) => {
      addAppToMenu(appName, appPath);
      hideAppList();
      setCurrentApp(appName, appPath);
    }).catch((error) => {
      // TODO: error handling
      console.log('Error creating new app:', error);
    });
  }
}

/**
 * UI handler for selecting an app from app list.
 */
function selectAppHandler(event: Event) {
  let appId = $(event.target).data("app-id");
  setCurrentApp($(event.target).text(), appId);
  hideAppList();
}

/**
 * UI handler for deleting an app from the app list.
 * This just shows the remove app confirmation popup.
 */
function removeAppHandler(event: Event) {
  let appName = $(event.target).prev("p").text();
  let appId = $(event.target).prev("p").data('app-id');
  //console.log('Delete app:', appName);

  $("div.removeapp input[type='submit']").data('app-id', appId);
  // update the remove confirmation popup message for the select app
  $('.removeapp div.popupsubtitle').text(loadString('deleteAppPrompt', {app: appName}));
  // show remove confirmation popup
  $('.removeapp').addClass('click');
  $('.coverbox').addClass('click');
}

/**
 * UI handler for delete app confirmation popup.
 * This actually deletes the app from the device config.
 */
function actualRemoveAppHandler(event: Event) {
  let appId = $(event.target).data('app-id');
  $(event.target).data('app-id', '');
  _device.deleteApp(appId)
  .then(() => {
    removeAppFromMenu(appId);
    if (_curApp === appId) {
      selectAllApplications();
    }
    hideAppList();
  }).catch(error => {
    // TODO: display error to user
    console.log('Error deleting app:', error);
  }).finally(() => {
    $('.removeapp').removeClass('click');
    $('.coverbox').removeClass('click');
  });
}

function showRestoreDefaultsPopup() {
  $('.restoreall').addClass('click');
  $('.coverbox').addClass('click');
}

function hideRestoreDefaultsPopup() {
  $('.coverbox').removeClass('click');
  $('.restoreall').removeClass('click');
}

function setRestoreDefaultsPromptMessage(message: string) {
  $('.restoreall div.popupsubtitle').text(message);
}

function cancelResetDefaultsPopup(event: Event) {
  $('.restoreall input[type=\'submit\']').off('click');
  hideRestoreDefaultsPopup();
}

function resetAllHandler(event: Event) {
  setRestoreDefaultsPromptMessage(loadString("resetAllPrompt"));
  $('.restoreall input[type=\'submit\']').on('click', actualResetAllHandler);
  showRestoreDefaultsPopup();
}

function actualResetAllHandler(event: Event) {
  // TODO: reset all settings
  Promise.all([
    _device.setAllButtons(_curApp, _device.getDefaultButtonsConfig()),
    _device.setPointerConfig(_curApp, _device.getDefaultPointerConfig()),
    _device.setScrollConfig(_curApp, _device.getDefaultScrollConfig())
  ]).then((results) => {
    updateButtonAssignmentsUI(results[0]);
    updatePointerConfigUI(results[1]);
    updateScrollConfigUI(results[2]);
  }).catch((error) => {

  })
  hideRestoreDefaultsPopup();
  closeButtonActionMenu();
}

function resetButtonsHandler(event: Event) {
  setRestoreDefaultsPromptMessage(loadString("resetButtonsPrompt"));
  $('.restoreall input[type=\'submit\']').on('click', actualResetButtonsHandler);
  showRestoreDefaultsPopup();
}

function actualResetButtonsHandler(event: Event) {
  $('.restoreall input[type=\'submit\']').off('click');
  let jButton = $($(`${PRODUCTBUTTON_PATH} .pb.click`)[0]);
  let button = parseInt(jButton.data('buttonMask'));
  let buttonClasses = jButton.attr('class').split(' ');
  let hoverClass = '';
  buttonClasses.forEach((val) => {
    if (val.startsWith('hover')) {
      hoverClass = val;
    }
  })
  let defaultButtonConfig = null;
  let hoverIndex = 0;
  if (hoverClass) {
    let hoverIndexStr = hoverClass.substr("hover".length);
    if (hoverIndexStr.length) {
      hoverIndex = parseInt(hoverIndexStr);
      let defaultButtonsConfig = _device.getDefaultButtonsConfig();
      if (defaultButtonsConfig.hasOwnProperty(button.toString())) {
        defaultButtonConfig = _device.getDefaultButtonsConfig()[button.toString()];
      }
    }
  }
  // let hover = parseInt(jButton.attr('class').split("hover")[1]);
  //let defaultButtonConfig = _device.getDefaultButtonsConfig()[button.toString()];

  _device.setButton(_curApp, button, defaultButtonConfig.command, defaultButtonConfig.params)
  .then((res: any) => {
    setButtonLabelAndData(hoverIndex, button, res.command, res.params);
    closeButtonActionMenu();
    jButton.removeClass('click');
    setLeftClickLockMessage();
  }).catch((error) => {
    console.log('Restore defaults error:', error);
    // TODO: how to show error
  }).finally(() => {
    hideRestoreDefaultsPopup();
    closeButtonActionMenu();
  });
}

function resetPointerSettingsHandler(event: Event) {
  setRestoreDefaultsPromptMessage(loadString("resetPointerSettingsPrompt"));
  $('.restoreall input[type=\'submit\']').on('click', actualResetPointerSettingsHandler);
  showRestoreDefaultsPopup();
}

function actualResetPointerSettingsHandler(event: Event) {
  $('.restoreall input[type=\'submit\']').off('click');
  _device.setPointerConfig(_curApp, _device.getDefaultPointerConfig())
  .then((newConfig) => {
    //console.log('new pointer config:', newConfig);
    updatePointerConfigUI(newConfig);
  }).catch((error) => {
    // TODO: how to show error
  });
  hideRestoreDefaultsPopup();
}

function resetScrollSettingsHandler(event: Event) {
  setRestoreDefaultsPromptMessage(loadString("resetScrollSettingsPrompt"));
  $('.restoreall input[type=\'submit\']').on('click', actualResetScrollSettingsHandler);
  showRestoreDefaultsPopup();
}

function actualResetScrollSettingsHandler(event: Event) {
  $('.restoreall input[type=\'submit\']').off('click');
  _device.setScrollConfig(_curApp, _device.getDefaultScrollConfig())
  .then((newConfig) => {
    //console.log('new scroll config:', newConfig);
    updateScrollConfigUI(newConfig);
  }).catch((error) => {
    // TODO: how to show error
  });
  hideRestoreDefaultsPopup();
}

function initPage() {
  $(document).foundation(); //IE old version tip
  mscroll($);
  initDimensions();
  initMenu();

  // Load button action menu template corresponding to the platform.
  // Should do this before the scrollbars are setup.
  $("#id-template-button-action-menu").replaceWith(
    loadTemplateString(`trackball/button-menu-${process.platform}.html`)
  );

  initScrollbar();
  setupGlobalUIHandlers();
  setupTrackballConfigHandlers();
  updateProductVersion();
  initBackupRestoreHandlers();

  // populate modifier keys & label templates
  $("div[name='mod-keys-placeholder']").replaceWith(MODIFIER_KEY_TEMPLATE);
  $("div[name='label-placeholder']").replaceWith(LABEL_TEMPLATE);

  bindLaunchingHandlers(_t, _menuItemActionHandlers);  // launch file/folder/program/url handlers
  bindKeyboardShortcutActionHandlers(_t, _menuItemActionHandlers);
  bindSnippetActionHandlers(_t, _menuItemActionHandlers);
  bindMouseButtonActionHandlers(_t, _menuItemActionHandlers);
  bindScrollActionHandler(_t, _menuItemActionHandlers);
  bindGenericActionHandlers(_t, _menuItemActionHandlers); // has to be the last
}

/**
 * Depending on the support device information for the device, draw
 * the appropriate bluetooth/usb status icons.
 *
 * Actually, code now removes the icons as the HTML will have all the
 * relevant icons. So we just retain the icons which reflect the device's
 * supported connection interfaces.
 */
function drawSupportedConnectionIntefaceIcons() {
  if (_di.connectionInterface.indexOf('bluetooth') == -1) {
    $("#id_status_bluetooth").remove();
  }
  if (_di.connectionInterface.indexOf('usb') == -1) {
    $("#id_status_usb").remove();
  }
}

async function buildFavoriteAppListMenu() {
  let favAppList = await TrackballWorks.getFavoriteApps(_device.id());
  //let favAppList = await _device.getFavoriteApps(_device.id());
  let $favMenu = $('div.app_list_more div.scrollbarlist');
  $favMenu.html('');
  for (let index = 0; index < favAppList.length; index++) {
    const element = favAppList[index];
    /*
      Element is an object of the form
      {
        icon: <icon URLEncoded>,
        name: <name>,
        program: <full path to program>
      }
    */
    let menuItem = `
    <div class="marked_content" data-program="${element.program}" data-name="${element.name}">
      <div class="markedicon">
        <img src="data:image/png;base64,${element.icon}" alt="">
      </div>
      <p class="appname">${element.name}</p>
    </div>`;
    $favMenu.append(menuItem);
  }
}

/**
 * 'click' handler for new application button (+ button) in the app list.
 */
function newApplicationFromFavMenuHandler(event: any) {
  let $menuItem = $($(event.target).parents('div.marked_content')[0])
  let appPath = $menuItem.data('program');
  let appName = $menuItem.data('name');
  //console.log('App name: ', appName, 'app path:', appPath);
  if (appPath) {
    _device.createApp(appName, appPath)
    .then((apps) => {
      addAppToMenu(appName, appPath);
      hideFavAppList();
      setCurrentApp(appName, appPath);
    }).catch((error) => {
      // TODO: error handling
      console.log('Error creating new app:', error);
    });
  }
}

/**
 * Configure the page for device with the given device id
 * @param deviceId number
 */
function initPageForDevice(deviceId: number) {
  _device = getTrackballDevice(deviceId);
  _di = getSupportedDeviceInfo(deviceId);

  // set product name
  $(".footercontent .name").text(_di.name);
  drawSupportedConnectionIntefaceIcons();

  // initialize Buttons template
  let tmpl = loadTemplate('trackball/content-buttons.html');
  let html = tmpl.render(_di);
  // console.log(html);
  $(".content_main").html(html);

  // initialize Pointer area
  tmpl = loadTemplate('trackball/content-pointer.html');
  let di = JSON.parse(JSON.stringify(_di));
  di.altLabel = 'Alt';
  di.platform = process.platform;
  if (process.platform == 'darwin') {
    di.altLabel = 'Opt';
  }

  html = tmpl.render(di);
  $(".trov_rightcontent[data-button='button2']").html(html);

  // initialize Scrolling area
  if (_di.scrollWheel) {
    tmpl = loadTemplate('trackball/content-scrolling.html');
    html = tmpl.render(_di);
    $(".trov_rightcontent[data-button='button3']").html(html);

    if (_di.hasOwnProperty('scrollDirImages')) {
      if (_di.scrollDirImages.hasOwnProperty('normal')) {
        $('div.reversebox.normal').attr('style', `background-image: url(images/trackball/${_di.id}/${_di.scrollDirImages.normal})`)
      }
      if (_di.scrollDirImages.hasOwnProperty('reverse')) {
        $('div.reversebox.reverse').attr('style', `background-image: url(images/trackball/${_di.id}/${_di.scrollDirImages.reverse})`)
      }
    }
  } else {
    $(".trov_button[data-button='button3']").addClass('is-hidden');
  }

  loadApps();

  // Device button configuration rounted rectangle click handler
  $(`${PRODUCTBUTTON_PATH} .pb`).on('click', deviceButtonSelectHandler);
  // Button action save settings handler
  $('#save-button-settings').on('click', saveButtonSettingsHandler);
  // Pointer setttings Save button handler
  $('#save-pointer-settings').on('click', savePointerSettingsHandler);
  // Scroll setttings Save button handler
  $("#save-scroll-settings").on('click', saveScrollSettingsHandler);
  // Add application handler, that shows the frequently used app list menu
  $("#id_add_application").on('click', (event) => {
    buildFavoriteAppListMenu()
    .then(() => {
      $('.app_list').removeClass('hover');
      $('.app_list_more').addClass('hover');
      $('.listcover').addClass('click');
    });
  });
  $(document).on('click', "div.app_list_more div.marked_content", (event) => {
    newApplicationFromFavMenuHandler(event);
  });
  // Close the 'Current app list' menu
  $('#id_close_freq_used_app_list').click((event) => {
    $('.app_list_more').removeClass('hover');
    $('.app_list').addClass('hover');
    $('.listcover').addClass('click');
  });
  $('#id_add_application_via_shell_open').click((event) => {
    $('.app_list_more').removeClass('hover');
    $('.listcover').removeClass('click');
    newApplicationHandler();
  });
  // handler for selecting an app
  $('body').on('click', 'div.app_list #mCSB_1_container p.appname', selectAppHandler);
  // handler for deleting an app from the app list
  $('div.app_list').on('click', '#mCSB_1_container .be-icon-trash', removeAppHandler);
  // handler for delete app confirmation popup submit button
  $('div.removeapp input[type=\'submit\']').on('click', actualRemoveAppHandler);
  // Restore Defaults handlers
  // pointer settings
  $("#id-restore-defaults-pointer").on('click', resetPointerSettingsHandler);
  // scroll settings
  $("#id-restore-defaults-scroll").on('click', resetScrollSettingsHandler);

  //console.log("Default buttons config for device:", getDefaultButtonSettings(deviceId));
  updateStatus();

  $("#id_status_goto_tutor").click((event) => {
    event.preventDefault();
    ipcRenderer.send("goto-device-tutor", _device.id());
  });

}

/**
 * Essentially removes all the jquery click handlers that that are bound
 * to UI elements that are dynamically added to the page by rendering templates
 * with device specific properties such as id, name, etc.
 */
function deinitPageForDevice(deviceId: number) {
  $(`${PRODUCTBUTTON_PATH} .pb`).off('click');
  // Button action save settings handler
  $('#save-button-settings').off('click');
  // Pointer setttings Save button handler
  $('#save-pointer-settings').off('click');
  // Scroll setttings Save button handler
  $("#save-scroll-settings").off('click');
  // New application handler
  $("#id-new-application").off('click');
  // handler for selecting an app
  $('body').off('click', 'div.app_list #mCSB_1_container p.appname');
  // handler for deleting an app from the app list
  $('div.app_list').off('click', '#mCSB_1_container .be-icon-trash');
  // handler for delete app confirmation popup submit button
  $('div.removeapp input[type=\'submit\']').off('click');
  // pointer settings restore defaults button
  $("#id-restore-defaults-pointer").off('click');
  // scroll settings restore defaults button
  $("#id-restore-defaults-scroll").off('click');
}

/**
 * Update status icons to reflect device status.
 */
function updateStatus() {
  const CONNECTED_COLOR = '#0096ff';
  const DISCONNECTED_COLOR = 'gray';
  $("#id_status_usb").css({
    color: _device.connectionInterface().indexOf('usb') == -1 ?
    DISCONNECTED_COLOR : CONNECTED_COLOR
  });
  $("#id_status_bluetooth").css({
    color: _device.connectionInterface().indexOf('bluetooth') == -1 ?
    DISCONNECTED_COLOR : CONNECTED_COLOR
  });

  if (_device.connectionInterface()) {
    $('#id_status_goto_tutor').hide();
  } else {
    $('#id_status_goto_tutor').show();
  }
}

/**
 * Sent to inform trackball config to reload all settings.
 * We can achieve this by emulating user selecting All Applications
 * from the applications list.
 */
ipcRenderer.on('refresh-settings', (event: any) => {
  //console.log('Trackball Overview - refresh-settings');
  try {
    let $allApp = $($("div.app_list #mCSB_1_container p.appname[data-app-id='*']")[0]);
    setCurrentApp($allApp.text(), $allApp.data("app-id"), true);
  } catch (error) {
    console.log("Error refreshing page settings:", error);
  }
});

// Handler for helper-missing notification sent by main process.
ipcRenderer.on('helper-missing', (event: any) => {
  //console.log('Helper has gone missing!')
  ipcRenderer.send("goto-home");
});

$(document).ready(() => {

    initI18n()
    .then((t) => {
      // store the translation function globally so that we can use
      // whenever we need it.
      initI18Funcs(t);

      try {
          initPage();

          let deviceId = parseInt(getUrlParam("device", 0));
          initPageForDevice(deviceId);
          localizeStrings();

          // This would cause the all the relevant UI elements to be updated
          // for All Applications config, which is the default when we enter the
          // device configuration UI.
          selectAllApplications();

          ipcRenderer.on('device-status-change', (event: any, device: TrackballDevice) => {
            //console.log('device-arrival message');
            if (device._id == _device.id()) {
              // disconnected device has arrived. Update status icons
              updateStatus();
            }
          })

        } catch (error) {
          // TODO: error handling
          console.log('init error: ', error);
        }

    }).catch((err) => {
        // TODO: error handling
        console.log("Error initializing i18next library, err: ", err);
    });
});

} // namespace Trackball