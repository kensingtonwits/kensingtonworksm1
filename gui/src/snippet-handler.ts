import * as $ from 'jquery';
import { MenuItemHandler } from "./menu-item-handler";
import { getCommandLabel } from "./helper";

class SnippetActionHandlers implements MenuItemHandler {
    loadFn(button: number, command: string, params: any, selectedRadio: any) {
        $(selectedRadio).find('textarea').val('');
        $(selectedRadio).find('input[name=\'label\']').val('');
        if (params && params.hasOwnProperty('content') && params.content) {
            $(selectedRadio).find('textarea').val(params.content);
        }
        if (params && params.hasOwnProperty('label') && params.label) {
            $(selectedRadio).find('input[name=\'label\']').val(params.label);
        }
    };
    validatorFn(command: string) {
        return true;
    };
    storeFn(app:string, button: number, command: string, selectedRadio: any) {
        let label = $(selectedRadio).find('input[name=\'label\']').val();
        let index = $(selectedRadio).data('index');
        return {
            content: $(selectedRadio).find('textarea').val(),
            label: label ? label : '',
            index: index
        }
    };
    labelFn(command: string, params: any, t: (val:string) => string) {
        if (params
            && params.hasOwnProperty('label')
            && params.label) {
            return params.label;
        }
        if (params
            && params.hasOwnProperty('content')
            && params.content) {
            return params.content;
        }
        return getCommandLabel(command);
    };
}

export function bindSnippetActionHandlers(t: any, actionHandlers: Map<RegExp, MenuItemHandler>) {
    actionHandlers.set(new RegExp('snippet'), new SnippetActionHandlers());
}
