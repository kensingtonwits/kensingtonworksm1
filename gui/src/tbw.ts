import { isDev, getMainWindow } from './helper';
import { getSupportedDeviceInfo } from "./helper";
import { request_code, sendCmdServiceRequest, initCmdService } from "./helper";
import axios from "axios";

//const cmdservice = require('bindings')('commandServer.node');



export class TrackballDevice {
    // id: number;
    // realId: number
    // interface: string
    // name: string;
    // version: number;
    // present: boolean;

    constructor(public _id: number, public _realId: number,
        public _name: string,
        public _version: number, public _present: boolean,
        public _connectionInterface: string)
    {
    }

    id(): number {
        return this._id;
    }

    name(): string {
        return this._name;
    }

    version(): number {
        return this._version;
    }

    isPresent(): boolean {
        return this._present;
    }

    connectionInterface(): string {
        return this._connectionInterface;
    }

    public async getButton(app: string, button: number): Promise<string> {
        //let url = TrackballWorks.helperBaseUrl() + `/config/buttons?app=${app}&device=${this._id}&button=${button}`;
        //console.log('getButton :', url);
        //let response = await axios.get(url);
        //console.log('getButton response :', response.data);
        //return response.data;
        let request = request_code.REQUEST_GET_BUTTON;
        let resp = await sendCmdServiceRequest(request, app/*.replace(/\\/g, "\\\\")*/, this._id, button, '', '');
        if (resp.includes("FALSE")) {
            //return JSON.parse("{}");
            resp = "{}";
        }
        var jsonobj = JSON.parse(resp); 
        console.log('getButton response :', jsonobj);
        return jsonobj;
    }

    /**
     * Sets the given buttons action binding.
     *
     * @param app string Application identifier
     * @param button number
     * @param command string Command macro
     * @param params any Optional command specific parameters.
     * @returns Promise<any> Upon resolve the promise returns the bound
     * action macro and its command specific parameters, if any.
     */
    public async setButton(app: string, button: number, command: string, params: any): Promise<any> {
        //let url = TrackballWorks.helperBaseUrl() + `/config/buttons?app=${app}&device=${this._id}&button=${button}`;
        //let response = await axios.post(url, {
        //    "command": command,
        //    "params": params
        //});
        //console.log('setButton : ', url, " ", command, " ", params);
        //console.log('setButton response:', response.data);
        //return response.data;
        //
        console.log('setButton : ', app);
        let request = request_code.REQUEST_SET_ALL_BUTTONS;
        let strcfg: string = "{\"command\": \"" + command + "\", \"params\": " + JSON.stringify(params) + "}";
        let resp = await sendCmdServiceRequest(request, app/*.replace(/\\/g, "\\\\")*/, this._id, button, "", strcfg);
        if (resp.includes("FALSE")) {
            resp = "{}";
        }
        var jsonobj = JSON.parse(resp);       
        console.log('setButton response :', jsonobj);
        return jsonobj;
        //
    }

    /**
     * Returns the configuration for all buttons for the given application
     * context.
     *
     * @param app string Application identifier
     */
    public async getAllButtons(app: string): Promise<any> {
        //let url = TrackballWorks.helperBaseUrl() + `/config/buttons?app=${app}&device=${this._id}&button=0`;
        //let response = await axios.get(url);
        //console.log('getAllButtons response:', response.data);
        //return response.data;

        let request = request_code.REQUEST_GET_ALL_BUTTONS;
        let resp = await sendCmdServiceRequest(request, app/*.replace(/\\/g, "\\\\")*/, this._id, 0, '', '');
        if (resp.includes("FALSE")) {
            //return JSON.parse("{}");
            resp = "{}";
        }
        var jsonobj = JSON.parse(resp);
        console.log('getAllButtons response :', jsonobj);
        return jsonobj;
    }

    /**
     * Sets the configuration of all buttons for the give device and application
     * context.
     *
     * @param app string Application identifier
     * @param config All button configuration as a JSON
     * @returns any Configuration of all buttons as a JSON
     */
    public async setAllButtons(app: string, config: any): Promise<any> {
        //let url = TrackballWorks.helperBaseUrl() + `/config/buttons?app=${app}&device=${this._id}&button=0`;
        //let response = await axios.post(url, config);
        //console.log('setAllButtons response:', response.data);
        //return response.data;

        let request = request_code.REQUEST_SET_ALL_BUTTONS;
        let resp = await sendCmdServiceRequest(request, app/*.replace(/\\/g, "\\\\")*/, this._id, 0, "", JSON.stringify(config));
        if (resp.includes("FALSE")) {
            //return config;
            resp = "{}";
        }
        var jsonobj = JSON.parse(resp);
        console.log('setAllButtons response :', jsonobj);
        return jsonobj;   
    }

    /**
     * Get pointer configuration for the given app.
     *
     * @param app string Application identifier
     */
    public async getPointerConfig(app: string): Promise<any> {
        //let url = TrackballWorks.helperBaseUrl() + `/config/pointer?app=${app}&device=${this._id}`;
        //let response = await axios.get(url);
        //console.log('getPointerConfig response:', response.data);
        //return response.data;

        let request = request_code.REQUEST_GET_POINTER_CONFIG;
        let resp = await sendCmdServiceRequest(request, app/*.replace(/\\/g, "\\\\")*/, this._id, 0, '', '');
         if (resp.includes("FALSE")) {
            //return JSON.parse("{}");
            resp = "{}";
        }
        var jsonobj = JSON.parse(resp);
        console.log('getPointerConfig response :', jsonobj);
        return jsonobj;
    }

    /**
     * Set pointer configuration of the given app.
     *
     * @param app string App identifier
     * @param config any Pointer configuration data
     */
    public async setPointerConfig(app: string, config: any): Promise<any> {
        //let url = TrackballWorks.helperBaseUrl() + `/config/pointer?app=${app}&device=${this._id}`;
        //let response = await axios.post(url, config);
        //console.log('setPointerConfig response:', response.data);
        //return response.data;

        let request = request_code.REQUEST_SET_POINTER_CONFIG;
        let resp = await sendCmdServiceRequest(request, app/*.replace(/\\/g, "\\\\")*/, this._id, 0, "", JSON.stringify(config));
        if (resp.includes("FALSE")) {
            //return config;
            resp = "{}";
        }
        var jsonobj = JSON.parse(resp);
        console.log('setPointerConfig response :', jsonobj);
        return jsonobj;
    }

    /**
     * Get scroll configuration for the given app
     *
     * @param app string App identifier
     */
    public async getScrollConfig(app: string): Promise<any> {
        //let url = TrackballWorks.helperBaseUrl() + `/config/scroll?app=${app}&device=${this._id}`;
        //let response = await axios.get(url);
        //console.log('getScrollConfig response:', response.data);
        //return response.data;

        let request = request_code.REQUEST_GET_SCROLL_CONFIG;
        let resp = await sendCmdServiceRequest(request, app/*.replace(/\\/g, "\\\\")*/, this._id, 0, '', '');
        var jsonobj = JSON.parse(resp);
        if (resp.includes("FALSE")) {
            //return JSON.parse("{}");
            resp = "{}";
        }
        console.log('getScrollConfig response :', jsonobj);
        return jsonobj;
    }

    /**
     * Set the scroll configuration for the given app
     *
     * @param app string App identifier
     * @param config any Scroll configuration
     */
    public async setScrollConfig(app: string, config: any): Promise<any> {
        //let url = TrackballWorks.helperBaseUrl() + `/config/scroll?app=${app}&device=${this._id}`;
        //let response = await axios.post(url, config);
        //console.log('setScrollConfig response:', response.data);
        //return response.data;

        let request = request_code.REQUEST_SET_SCROLL_CONFIG;
        let resp = await sendCmdServiceRequest(request, app/*.replace(/\\/g, "\\\\")*/, this._id, 0, "", JSON.stringify(config));
        if (resp.includes("FALSE")) {
            //return config;
            resp = "{}";
        }
        var jsonobj = JSON.parse(resp);
        console.log('setScrollConfig response :', jsonobj);
        return jsonobj;
    }

    /**
     * Get snippets list for the given app
     *
     * @param app application identifier
     */
    public async getSnippets(app: string): Promise<any> {
        //let url = TrackballWorks.helperBaseUrl() + `/config/snippets?app=${app}&device=${this._id}`;
        //let response = await axios.get(url);
        //console.log('getSnippets response:', response.data);
        //return response.data;

        let request = request_code.REQUEST_GET_SNIPPETS;
        let resp = await sendCmdServiceRequest(request, app/*.replace(/\\/g, "\\\\")*/, this._id, 0, '', '');
        if (resp.includes("FALSE")) {
            //return JSON.parse("{}");
            resp = "{}";
        }
        //console.log('getSnippets response :', resp);
        var jsonobj = JSON.parse(resp);
        
        return jsonobj;
    }

    public async getApps(): Promise<any> {
        //let url = TrackballWorks.helperBaseUrl() + `/config/apps?device=${this._id}`;
        //const response = await axios.get(url);
        //console.log('getApps: ', response.data);
        //return response.data;

        let request = request_code.REQUEST_GET_APPS;
        let resp = await sendCmdServiceRequest(request, '*', this._id, 0, '', '');
        if (resp.includes("FALSE")) {
            //return JSON.parse("{}");
            resp = "{}";
        }
        var jsonobj = JSON.parse(resp);
        console.log('getApps response :', jsonobj);
        return jsonobj;
    }

    public async createApp(name: string, path: string): Promise<any> {
        //let url = TrackballWorks.helperBaseUrl() + `/config/apps?device=${this._id}`;
        //const response = await axios.post(url, {
        //    name: name,
        //    identifier: path
        //});
        //console.log('createApp response:', response.data);
        //return response.data;

        let request = request_code.REQUEST_CREATE_APP;
        //let jpath = path.replace(/\\/g, "\\\\");
        //jpath.;
        let strapp: string = "{ \"name\": \"" + name + "\",  \"identifier\": \"" + path.replace(/\\/g, "\\\\") + "\"}";
        let resp = await sendCmdServiceRequest(request, "", this._id, 0, "", strapp);
        if (resp.includes("FALSE")) {
            //return JSON.parse(strapp);
            resp = "{}";
        }
        var jsonobj = JSON.parse(resp);
        console.log('createApp response :', jsonobj);
        return jsonobj;

    }

    public async deleteApp(path: string): Promise<any> {
        //let url = TrackballWorks.helperBaseUrl() + `/config/apps?device=${this._id}`;
        //const response = await axios.delete(url, { data: { identifier: path }});
        //console.log('deleteApp response:', response.data);
        //return response.data;

        let request = request_code.REQUEST_DELETE_APP;
        let strapp: string = "{ \"identifier\": \"" + path.replace(/\\/g, "\\\\") + "\"}";
        let resp = await sendCmdServiceRequest(request, "", this._id, 0, "", strapp);
        if (resp.includes("FALSE")) {
            //return JSON.parse(strapp);
            resp = "{}";
        }
        var jsonobj = JSON.parse(resp);
        console.log('deleteApp response :', jsonobj);
        return jsonobj;

    }

    // default buttons config
    getDefaultButtonsConfig(): any {
        let di = getSupportedDeviceInfo(this.id());
        if (di.hasOwnProperty('defaultSettings')
            && di.defaultSettings
            && Object.keys(di.defaultSettings).length > 0
            && di.defaultSettings.hasOwnProperty('buttons')) {
            return di.defaultSettings.buttons;
        }

        let defaultSettings: any = {};
        const standardButtonKeys: any = {
            "1": { "command": "mouse_leftClick", "params": {} },
            "2": { "command": "mouse_rightClick", "params": {} },
            "4": { "command": "mouse_middleClick", "params": {} },
            "8": { "command": "mouse_button4Click", "params": {} },
            "16": { "command": "mouse_button5Click", "params": {} },
            "1073741824": { "command": "navigation_scrollLeft", "params": {} },
            "2147483648": { "command": "navigation_scrollRight", "params": {} }
        };
        const noActionCommand: any = {
            "command": "noAction", "params": {}
        };

        if (di.topView && di.hasOwnProperty('topButtons')) {
            for (let index = 0; index <di.topButtons.length; index++) {
                const element = di.topButtons[index];
                defaultSettings[element.mask] = standardButtonKeys.hasOwnProperty(element.mask) ?
                    standardButtonKeys[element.mask] : noActionCommand;
            }
        }
        if (di.sideView && di.hasOwnProperty('sideButtons')) {
            for (let index = 0; index <di.sideButtons.length; index++) {
                const element = di.sideButtons[index];
                defaultSettings[element.mask] = standardButtonKeys.hasOwnProperty(element.mask) ?
                    standardButtonKeys[element.mask] : noActionCommand;
            }
        }
        return defaultSettings;
    }

    getDefaultPointerConfig(): any {
        // These settings match the default values set in tbwhelper's
        // Settings.cpp DeviceConfig::DeviceConfig ctor
        return {
            "speed": 50,
            "acceleration": false,
            "accelerationRate": 0,
            "lockedAxisModifiers": "",
            "slowModifiers": ""
        }
    }

    getDefaultScrollConfig(): any {
        // These settings match the default values set in tbwhelper's
        // Settings.cpp DeviceConfig::DeviceConfig ctor
        return {
            "speed": 1,
            "inertial": false,
            "invert": false
        }
    }


    

};

export class TrackballWorks { 

    /**
     * @property {function} helperBaseUrl Returns TBW helper process base URL.
     * @returns {String}
     */
    public static helperBaseUrl(): string {
        let baseUrl = 'http://localhost:9090'; // Default URL
        if (isDev()) {
            // check for helper-url command line argument
            /**@type {Array} */
            /*
            let args = remote.process.argv.slice(2);
            console.log('helperBaseUrl - args: ', args);
            args.forEach((val: string, index: number, array: any[]) => {
                if (val.startsWith("helper-url") || val.startsWith("helperurl")) {
                    let args = val.split("=");
                    if (args.length > 1) {
                        baseUrl = args[1];
                    }
                }
            });
            */
        }
        return baseUrl;
    }

    /**
     * Returns trackball devices registered with TrackballWorks.
     * @returns
     */

    public static async getDevices(): Promise<TrackballDevice[]> {
        //let url = this.helperBaseUrl() + '/devices';
        //const response = await axios.get(url);
        //console.log('TBW devices:', response.data);
        //let tbDevices: TrackballDevice[] = [];
        //response.data.devices.forEach((device: any) => {
        //    tbDevices.push(new TrackballDevice(
        //        device.id,
        //        device.realId,
       //         device.name,
       //         device.version,
       //         device.present,
      //          device.interface));
      //  });
      //  return tbDevices;

        //console.log('getDevices ');
        let tbDevices: TrackballDevice[] = [];
        let request = request_code.REQUEST_GET_DEVICES;
        let resp = await sendCmdServiceRequest(request, '', 0, 0, '', '');
        if (resp.localeCompare("FALSE") == 0) {
            console.log('getDevices response FALSE');
            return tbDevices;
        }
        var jsonobj = JSON.parse(resp);
        //console.log('getDevices response :', jsonobj);
        jsonobj.devices.forEach((device: any) => {
            tbDevices.push(new TrackballDevice(
                device.id,
                device.realId,
                device.name,
                device.version,
                device.present,
                device.interface));
        });
        return tbDevices;

    }

    /**
     * Get the entire tbw settings
     */
    // move this function to class TrackballDevice
    
    static async getConfig(): Promise<string> {
        //let url = `${this.helperBaseUrl()}/config`;
        //console.log("getConfig", url);
        //const response = await axios.get(url);
        //console.log("getConfig ", response.data);
        //return response.data;

        let request = request_code.REQUEST_GET_CONFIGS;
        let resp = await sendCmdServiceRequest(request, '', 0, 0, '', '');
        if (resp.includes("FALSE")) {
            //return JSON.parse("{}");
            resp = "{}";
        }
        var jsonobj = JSON.parse(resp);
        console.log('getConfig response :', jsonobj);
        return jsonobj;

    }
    
    /**
     * Set the entire config
     */
    static async setConfig(settings: any) {
        //let url = `${this.helperBaseUrl()}/config`;
        //const response = await axios.post(url, settings);
        //return response.data;

        let request = request_code.REQUEST_SET_CONFIGS;
        let resp = await sendCmdServiceRequest(request, "", 0, 0, "", JSON.stringify(settings));
        if (resp.includes("FALSE")) {
            return settings;
        }
        var jsonobj = JSON.parse(resp);
        console.log('setConfig response :', jsonobj);
        return jsonobj;
    }

    /**
     * Returns an array of favorite apps
     */
    // move this function to class TrackballDevice
    
    static async getFavoriteApps(deviceId: number) {

        //console.log("getFavoriteApps : ", deviceId);
        //let url = `${this.helperBaseUrl()}/favapps?device=${deviceId}&count=9`;
        //const response = await axios.get(url);
        //return response.data;

        console.log("getFavoriteApps : ", deviceId);
        let request = request_code.REQUEST_GET_FAVORITE_APPS;
        let resp = await sendCmdServiceRequest(request, '', deviceId, 0, '', '');
        if (resp.includes("FALSE")) {
            //return JSON.parse("{}");
            resp = "{}";
        }
        var jsonobj = JSON.parse(resp);
        //console.log('getFavoriteApps response :', resp);
        return jsonobj;


    }
    
}


// Axios request interceptor to detect configuration changes
axios.interceptors.request.use(function (config) {
    // Do something before request is sent
    //console.log('HTTP Interceptor - url:', config.url);
    // Configuration URL that we need to intercept to detect changes to
    // settings. Note that the trailing '/' is VERY important as without it
    // even Restore Setting (both from PC file as well as cloud) POST request
    // will also be wrongly detected as a settings change.
    // Since all individual settings API URL endpoints are a sub namespace
    // within the '/config' namespace, using the trailing '/' we can
    // accurately detect configuration changes.
    let configUrl = `${TrackballWorks.helperBaseUrl()}/config/`;
    if ((config.method == 'post' || config.method == 'put'
        || config.method == 'delete')
        && config.url.indexOf(configUrl) != -1) {
        let window = getMainWindow();
        if (window) {
            window.webContents.send('device-settings-update');
        }
    }
    return config;
  }, function (error) {
    // Do something with request error
    return Promise.reject(error);
  });
