#pragma once

#include <boost/serialization/string.hpp>
#include <boost/serialization/access.hpp>
#include <map>

using namespace std;

enum
{
	REQUEST_UNKNOWN,
	REQUEST_WHO,
	REQUEST_GET_DEFAULT_BUTTONS_CONFIG,
	REQUEST_GET_BUTTON,
	REQUEST_GET_ALL_BUTTONS,
	REQUEST_SET_BUTTON,
	REQUEST_SET_ALL_BUTTONS,
	REQUEST_GET_DEFAULT_POINTER_CONFIG,
	REQUEST_GET_POINTER_CONFIG,
	REQUEST_SET_POINTER_CONFIG,
	REQUEST_GET_DEFAULT_SCROLL_CONFIG,
	REQUEST_GET_SCROLL_CONFIG,
	REQUEST_SET_SCROLL_CONFIG,
	REQUEST_GET_SNIPPETS,
	REQUEST_GET_APPS,
	REQUEST_CREATE_APP,
	REQUEST_DELETE_APP,
	REQUEST_GET_DEVICES,
	REQUEST_GET_CONFIGS,
	REQUEST_SET_CONFIGS,
	REQUEST_GET_FAVORITE_APPS,

};

const std::map<std::string, int> mapOfCmd = {
		{"Unknown", REQUEST_UNKNOWN},
		{"WhoAmI", REQUEST_WHO },
		{"Get Button", REQUEST_GET_BUTTON},
		{"Set Button", REQUEST_SET_BUTTON}
};

#define ARCH_FLAGS (boost::archive::no_codecvt | boost::archive::no_tracking)

class cmdMsg
{
private:
	friend class boost::serialization::access;

	template<class Archive>
	void serialize(Archive & ar, const unsigned int version)
	{
		ar & cid;
		ar & deviceId;
		ar & processid;
		ar & button;
		ar & request;
		ar & cmd;
		ar & app;
		ar & params;
		ar & response;
	};

public:
	std::uint16_t cid;	
	std::uint16_t deviceId;
	std::uint16_t processid;
	std::uint32_t button;  // 1, 2, .., 0 for all buttons
	std::uint16_t request;
	std::string cmd;
	std::string app;
	std::string params;
	std::string	response;


	cmdMsg(std::uint16_t i = 0, std::string n = "", std::string p = "")
		: cid(i), cmd(n), app(p), params(p)
	{};

};