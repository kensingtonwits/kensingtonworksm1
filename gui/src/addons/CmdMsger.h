#pragma once
#ifdef _WIN32 // *&*&*&V1_20201029
#ifndef _WINDOWS
#define _WINDOWS
#endif
#endif
#ifdef _WINDOWS
#ifndef _WIN32_WINNT
// We're building on Win10, which is what _WIN32_WINNT defaults to 
// if not defined. (We probably can also get away with 0x0601(Win7)).
#define _WIN32_WINNT 0x0A00
#endif
#else
// OSX
#include <pthread.h>
#endif

#define IS_USER_HELPER	false

#if(IS_USER_HELPER)
#define IS_JAVASCRIPT_ON	false
#else
#define IS_JAVASCRIPT_ON	true
#endif


#if(IS_USER_HELPER)
#include "IDeviceManager.h"
#endif
#if(IS_JAVASCRIPT_ON)
#include <nan.h>
#endif
#include <boost/interprocess/ipc/message_queue.hpp>
#include <boost/process.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <thread>
#include <set>
#include "cmdMsg.h"
#include <boost/log/trivial.hpp>
#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/interprocess/containers/string.hpp>
#include <iostream>
#ifdef _WINDOWS
#include <boost/detail/winapi/get_current_process.hpp>
#endif
#include "tbwMshm.h"

using namespace std;
using namespace boost::interprocess;

#define MSG_RECEIVE_TIMEOUT			2000
#define MSG_RECEIVE_LONG_TIMEOUT	10000
#define MSG_SEND_TIMEOUT			2000
#define MSGQUEUE_PREFIX			"tbwcmdmsgQueue"
#define MAX_QUEUE_NUMBER		20
#define MAX_QUEUE_SIZE			512000	//102400

#define DEFAULT_RESPONSE		"{}"


/*
 * 
 */
namespace CommandMsger {

#if(IS_USER_HELPER)
	bool CommandMsger_init(ITbwDeviceManager* pMgr, bool bTestMode);

	std::set<std::string> getExistApps(uint16_t deviceid);
#else
	bool CommandMsger_init(bool bTestMode);
	//void call_helper();
	bool is_helper_running();
	bool CommandMsgque_reset();
#endif

	bool CommandMsger_Start();
	void CommandMsger_Stop();
	bool CommandMsgque_setup(uint32_t pid);

	uint32_t msg_receive(uint32_t tmout);
	uint32_t msg_send(uint32_t tmout);
	void receive(uint32_t tmout, uint32_t *ret);
	void send(uint32_t tmout, uint32_t *ret);

	std::string sendRequest(uint16_t request, std::string app, uint16_t devid, uint32_t button, std::string cmd, std::string params);
	void running();
	//void check_MsgQueue();
	//bool is_msgque_connected();
	//bool getMsgQueue(bool bcreate);
	void clearQueue();
	//bool setupQueue(uint32_t seed);
	void resetQueue();

	bool IsRunning();
} // namespace