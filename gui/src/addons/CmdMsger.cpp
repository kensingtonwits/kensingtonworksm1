#include "CmdMsger.h"
#ifdef _WINDOWS
	#include <boost/detail/winapi/get_current_process.hpp>
#endif
#include <boost/range.hpp>
#include <boost/date_time.hpp>
#include <boost/chrono.hpp>
#include <boost/log/trivial.hpp>

#if(IS_USER_HELPER)
#include "json.hpp"
#include "utils.h"
#include "PlatformServices.h"
#endif
#include "tbwMshm.h"

#ifdef __APPLE__
#include "../../../tbwhelper/osx/KensingtonWorksAgent/version.h"
#else
//#include "./version.h"
#endif

// just a running number
#define API_VERSION             1

using namespace std;
namespace bipc = boost::interprocess;
namespace bfs = boost::filesystem;
using namespace boost::posix_time;
#if(IS_USER_HELPER)
using namespace nlohmann;
#endif

//boost::filesystem::path que_dir(bfs::temp_directory_path().append("/.msgque"));
#ifdef __APPLE__
boost::gregorian::date dateStart(1970, 1, 1);
static boost::posix_time::ptime const epoch(dateStart);
#else
static boost::posix_time::ptime const epoch(boost::gregorian::date::date(1970, 1, 1));
#endif
namespace CommandMsger
{
#if(IS_USER_HELPER)
	ITbwDeviceManager* _pDeviceMgr;
	std::thread			_working_thread;
#else
	//std::thread			_monitor_thread;
	std::mutex			_reset_mutex;
	bipc::named_mutex	_monitor_mutex(bipc::open_or_create,"monitor_mutex");
	bipc::interprocess_condition  _cond_msgque_check;
	bool	_bhelper_running = false;
#endif
	bipc::named_mutex	_helper_mutex(bipc::open_or_create,"helper_mutex");
	bipc::interprocess_condition  _cond_helper_check;

	std::thread			_msg_thread;
	std::timed_mutex	_msg_mutex;
	std::mutex			_send_mutex;
	std::string	_Qname1;
	std::string	_Qname2;
	std::string	_sQname;
	std::string	_rQname;
	bipc::message_queue* _msg_sQueue = nullptr;
	bipc::message_queue* _msg_rQueue = nullptr;
	uint32_t	_pid = 0;
	std::thread::id	_tid;
	uint32_t	_mshm_pid = 0;
	cmdMsg		_smsg;
	cmdMsg		_rmsg;
	bool		_inited = false;
	bool		_binservice = false;
	bool		_mshm_ready = false;
	bool		_msgque_ready = false;
	//bool		_msgque_connected = false;
	bool		_testMode = false;
	//int64_t		_heartbeat0 = 0;
	//int64_t		_heartbeat = 0;


#if(IS_USER_HELPER)
	bool CommandMsger_init(ITbwDeviceManager* pMgr, bool bTestMode)
	{
		_testMode = bTestMode;
		_pDeviceMgr = pMgr;
		BOOST_LOG_TRIVIAL(info) << "CommandMsger init";
		if (!_inited)
		{
			#ifdef __APPLE__
			_pid = getpid();
			#else
			_pid = boost::detail::winapi::GetCurrentProcessId();
			#endif
			// shared memory init and get MSHMEM_FPID
			if (tbwMshm::tbwMshm_setup(_pid))
			{
				//_mshm_ready = tbwMshm::readMshmElement(MSHMEM_FPID, &_mshm_pid);
				_mshm_ready = tbwMshm::writeMshmElement(MSHMEM_FPID, &_pid);
				int64_t _heartbeat0 = (boost::posix_time::microsec_clock::universal_time() - epoch).total_milliseconds();
				_mshm_ready = tbwMshm::writeMshmElement(MSHMEM_FHBEAT, &_heartbeat0);
				_inited = _mshm_ready;
				BOOST_LOG_TRIVIAL(info) << "CommandMsger inite : " << _inited;
				std::cout << "CommandMsger inite : " << _inited << " \r\n";
			}
		}

		return _inited;
	}
#else
	bool CommandMsger_init(bool bTestMode)
	{
		_tid = std::this_thread::get_id();
		BOOST_LOG_TRIVIAL(info) << _tid << " > CommandMsger init";

		_testMode = bTestMode;
		if (!_inited)
		{
			_inited = false;
			BOOST_LOG_TRIVIAL(info) << "CommandMsger init..";
			std::cout << "CommandMsger init.. \r\n";
			#ifdef __APPLE__
			_pid = getpid();
			#else
			_pid = boost::detail::winapi::GetCurrentProcessId();
			#endif
			// shared memory init and get MSHMEM_FPID
			if (tbwMshm::tbwMshm_setup(_pid))
			{
				_mshm_ready = tbwMshm::readMshmElement(MSHMEM_FPID, &_mshm_pid);
				//_mshm_ready = tbwMshm::readMshmElement(MSHMEM_FHBEAT, &_heartbeat0);
				_inited = _mshm_ready;
				BOOST_LOG_TRIVIAL(info) << "CommandMsger inite : " << _inited;
				std::cout << "CommandMsger inite : " << _inited << " \r\n";
				// start monitor thread
				//_monitor_thread = std::thread(&running);
				if (!_bhelper_running)
				{
					// let GUI cmd msg queue start after helper runnung
					CommandMsgque_reset();
				}

			}
		}

		return _inited;
	}
#endif

	bool CommandMsger_Start()
	{
		if (!_inited)
		{
			BOOST_LOG_TRIVIAL(info) << "CommandMsger is not started!" << " \r\n";
			return false;
		}
		BOOST_LOG_TRIVIAL(info) << "CommandMsger_Start";

#if(IS_USER_HELPER)
		if (!CommandMsgque_setup(_pid))
		{
			return false;
		}
		_smsg.cid = 0;
		if (!_binservice)
		{
			BOOST_LOG_TRIVIAL(info) << "_working_thread starts running!";
			_binservice = true;
			_working_thread = thread(&running);
		}
#else
		if (!is_helper_running())
		{
			return false;
		}
		if (!CommandMsgque_setup(_mshm_pid))
		{
			return false;
		}
		//std::cout << "heartbeat : " << tbwMshm::rHeartbeat() << std::endl;
		_smsg.cid = 0;
		_binservice = true;
#endif

		return true;
	}

	bool CommandMsgque_setup(uint32_t seed)
	{
		std::thread::id tid = std::this_thread::get_id();

		BOOST_LOG_TRIVIAL(info) <<tid << "/" << _tid << " > CommandMsgque_setup(" << seed << ")";

		_msgque_ready = false;
		int r1, r2, r3;
		srand(seed);
		for (int i = 0; i <= rand() % 10; i++)
			r1 = rand();
		for (int i = 0; i <= rand() % 10; i++)
			r2 = rand();
		for (int i = 0; i <= rand() % 10; i++)
			r3 = rand();

		std::string name1, name2;

		try
		{
#if(IS_USER_HELPER)
			_rQname = std::string(MSGQUEUE_PREFIX) + std::string("g2h") + std::to_string(r1 / 2 + r2 / 2);
			_sQname = std::string(MSGQUEUE_PREFIX) + std::string("h2g") + std::to_string(r2 / 2 + r3 / 2);
			bipc::message_queue::remove(_rQname.data());
			bipc::message_queue::remove(_sQname.data());
#else
			_sQname = std::string(MSGQUEUE_PREFIX) + std::string("g2h") + std::to_string(r1 / 2 + r2 / 2);
			_rQname = std::string(MSGQUEUE_PREFIX) + std::string("h2g") + std::to_string(r2 / 2 + r3 / 2);
#endif
			// don't use create_only
			_msg_rQueue = new message_queue(open_or_create, _rQname.data(), MAX_QUEUE_NUMBER, MAX_QUEUE_SIZE);// sizeof(cmdMsg));
			_msg_sQueue = new message_queue(open_or_create, _sQname.data(), MAX_QUEUE_NUMBER, MAX_QUEUE_SIZE);// sizeof(cmdMsg));
			BOOST_LOG_TRIVIAL(info) << "msg queue (" << MAX_QUEUE_NUMBER << ", " << MAX_QUEUE_SIZE << " < " << _rQname << " > created/opened";
			_msgque_ready = true;
			_smsg.cid = 0;

		}
		catch (boost::interprocess::interprocess_exception& ex)
		{
			BOOST_LOG_TRIVIAL(info) << "new message_queue err : " << ex.what();
			std::cout << "new message_queue err : " << ex.what();
			_msgque_ready = false;
		}

		return _msgque_ready;
	}



#if(!IS_USER_HELPER)

	bool is_helper_running()
	{
		if (!_mshm_ready)
			return false;

		int64_t hbeat0 = tbwMshm::rHeartbeat();
		int64_t beat = (boost::posix_time::microsec_clock::universal_time() - epoch).total_milliseconds();
		if ((beat - hbeat0) > 1000)
		{
			//BOOST_LOG_TRIVIAL(info) << "helper is not running..";
			_bhelper_running = false;
			return false;
		}

		//BOOST_LOG_TRIVIAL(info) << "helper is running..";
		_bhelper_running = true;

		return true;

	}


#if 0
	bool is_msgque_connected()
	{
		if (!_msgque_ready || (_msg_sQueue == nullptr))
			return false;

		std::string response;

		int64_t hbeat0 = tbwMshm::rHeartbeat();
		int64_t beat = (boost::posix_time::microsec_clock::universal_time() - epoch).total_milliseconds();
		if ((beat - hbeat0) > 10000)
		{
			BOOST_LOG_TRIVIAL(info) << "helper is not running..";
			_msgque_connected = false;
			return false;
		}

		BOOST_LOG_TRIVIAL(info) << "helper is running..";

		clearQueue();

		for (int i = 0; i < 2; i++)
		{
			// check msg queues
			BOOST_LOG_TRIVIAL(info) << "send who am I.. " << i;
			response = sendRequest(REQUEST_WHO, "*", 0, 0, "", "");
			if (response.compare("WhoAmI") == 0)
			{
				std::cout << "WhoAmI check OK \r\n";
				_msgque_connected = true;
				return true;; // check OK	
			}

		}

		return false;

	}
#endif

	void resetQueue()
	{
		std::cout << "resetQueue\r\n";
		//_inited = false;
		try
		{

			if (_msg_sQueue)
			{
				_msg_sQueue->remove(_Qname1.data());
				_msg_sQueue = nullptr;
			}
			if (_msg_rQueue)
			{
				_msg_rQueue->remove(_Qname2.data());
				_msg_rQueue = nullptr;
			}
		}
		catch (boost::interprocess::interprocess_exception& ex)
		{
			BOOST_LOG_TRIVIAL(info) << "resetQueue err : " << ex.what();
			std::cout << "resetQueue err : " << ex.what();
		}

		_msgque_ready = false;

	}


#endif


	void CommandMsger_Stop()
	{
#if(IS_USER_HELPER)
		if (_binservice)
		{
			_working_thread.join();
			_binservice = false;
			_pDeviceMgr = nullptr;
			BOOST_LOG_TRIVIAL(info) << "command message queue stopped";
		}
		tbwMshm::tbwMshm_remove();
#endif
	}


#if(IS_USER_HELPER)

	void running()
	{
		BOOST_LOG_TRIVIAL(info) << "starting command message queue..";
		json jresp, jsoncmd, jsonpara;
		std::string cmdstr(""), parastr("");
		std::string jstr = "", str;
		uint32_t errcode;

		_binservice = true;

		do
		{
			try
			{
				if (_msg_sQueue == nullptr)
				{
					CommandMsgque_setup(_pid);
					continue;
				}

				if (_pDeviceMgr == nullptr)
				{
					BOOST_LOG_TRIVIAL(info) << "_pDeviceMgr == nullptr";
					goto Exit;
				}


				if ((errcode = msg_receive(0)) != no_error)
				{
					BOOST_LOG_TRIVIAL(info) << "msg_receive(0) err : " << errcode;
				}

				BOOST_LOG_TRIVIAL(info) << "message #" << _rmsg.cid << ": request " << _rmsg.request << " received";
				//BOOST_LOG_TRIVIAL(info) << "app " << _rmsg.app;
				//BOOST_LOG_TRIVIAL(info) << "command " << _rmsg.cmd << "params " << _rmsg.params;

				_smsg.request = _rmsg.request;
				_smsg.cid = _rmsg.cid;
				_smsg.deviceId = _rmsg.deviceId;
				_smsg.response = DEFAULT_RESPONSE;

#ifdef _WINDOWS
				// Windows file names are case insensitive. So treat all filnames
				// to lowercase so that while we search for a matching process,
				// it'll match (that algorithm also converts process image filename 
				// to lowercase in windows)
				if (_rmsg.app.length() > 0)
				{
					_rmsg.app = tolower(_rmsg.app);
				}

#else
				//app = field.second;
				//if (_rmsg.app.length() > 0)
				//{
				//	_rmsg.app = tolower(_rmsg.app);
				//}
#endif

				switch (_rmsg.request)
				{
				case REQUEST_WHO:
					_smsg.response = "WhoAmI";
					BOOST_LOG_TRIVIAL(info) << "message: " << _smsg.response << " sent";
					msg_send(MSG_SEND_TIMEOUT);
					break;
				case REQUEST_GET_DEVICES:
					//BOOST_LOG_TRIVIAL(info) << "REQUEST_GET_DEVICES: ";
					jresp = _pDeviceMgr->getDevices();
					jstr = jresp.dump();
					//jstr.append("\n");
					_smsg.response = jstr;
					msg_send(MSG_SEND_TIMEOUT);
					//BOOST_LOG_TRIVIAL(info) << "response: " << _smsg.response;
					break;

				case REQUEST_SET_CONFIGS:
					BOOST_LOG_TRIVIAL(info) << "REQUEST_SET_SETTINGS: ";
					//BOOST_LOG_TRIVIAL(info) << _rmsg.params;
					_pDeviceMgr->setSettings(json::parse(_rmsg.params));
					//break;
				case REQUEST_GET_CONFIGS:
					//BOOST_LOG_TRIVIAL(info) << "REQUEST_GET_SETTINGS: ";
					jresp = _pDeviceMgr->getSettings();
					jstr = jresp.dump();
					//jstr.append("\n");
					_smsg.response = jstr;
					msg_send(MSG_SEND_TIMEOUT);
					//BOOST_LOG_TRIVIAL(info) << "response: " << _smsg.response;
					break;
				case REQUEST_GET_FAVORITE_APPS:
					BOOST_LOG_TRIVIAL(info) << "REQUEST_GET_FAVORITE_APPS ";
					jresp = PlatformServices::getInstance()->getFavoriteApps(9,
						getExistApps(_rmsg.deviceId));
					jstr = jresp.dump();
					//jstr.append("\n");
					_smsg.response = jstr;
					msg_send(MSG_SEND_TIMEOUT);
					//BOOST_LOG_TRIVIAL(info) << "REQUEST_GET_FAVORITE_APPS " << jstr;
					BOOST_LOG_TRIVIAL(info) << "REQUEST_GET_FAVORITE_APPS sent!";// << jstr;
					break;
				case REQUEST_SET_BUTTON:
				case REQUEST_SET_ALL_BUTTONS:
					_pDeviceMgr->setButtonAction(
						_rmsg.app.c_str(),
						_rmsg.deviceId,
						_rmsg.button,
						json::parse(_rmsg.params));
				case REQUEST_GET_BUTTON:
					BOOST_LOG_TRIVIAL(info) << "REQUEST_GET_BUTTON: " << _rmsg.button;
					jresp = _pDeviceMgr->getButtonAction(
						_rmsg.app.c_str(),
						_rmsg.deviceId,
						_rmsg.button);
					jstr = jresp.dump();
					//jstr.append("\n");
					_smsg.response = jstr;
					msg_send(MSG_SEND_TIMEOUT);
					BOOST_LOG_TRIVIAL(info) << "response: " << _smsg.response;
					break;
				case REQUEST_SET_POINTER_CONFIG:
					_pDeviceMgr->setPointerConfig(
						_rmsg.app.c_str(),
						_rmsg.deviceId,
						json::parse(_rmsg.params));

				case REQUEST_GET_POINTER_CONFIG:
					jresp = _pDeviceMgr->getPointerConfig(
						_rmsg.app.c_str(),
						_rmsg.deviceId);
					jstr = jresp.dump();
					//jstr.append("\n");
					_smsg.response = jstr;
					msg_send(MSG_SEND_TIMEOUT);
					BOOST_LOG_TRIVIAL(info) << "pointer config: " << jstr;
					break;
				case REQUEST_SET_SCROLL_CONFIG:
					_pDeviceMgr->setScrollWheelConfig(
						_rmsg.app.c_str(), _rmsg.deviceId,
						json::parse(_rmsg.params));
				case REQUEST_GET_SCROLL_CONFIG:
					jresp = _pDeviceMgr->getScrollWheelConfig(
						_rmsg.app.c_str(), _rmsg.deviceId);
					jstr = jresp.dump();
					//jstr.append("\n");
					_smsg.response = jstr;
					msg_send(MSG_SEND_TIMEOUT);
					BOOST_LOG_TRIVIAL(info) << "scroll config: " << jstr;
					break;
				case REQUEST_GET_SNIPPETS:
					jresp = _pDeviceMgr->getSnippets(
						_rmsg.app.c_str(), _rmsg.deviceId);
					jstr = jresp.dump();
					//jstr.append("\n");
					_smsg.response = jstr;
					msg_send(MSG_SEND_TIMEOUT);
					BOOST_LOG_TRIVIAL(info) << "snippets: " << jstr;
					break;
				case REQUEST_CREATE_APP:
				case REQUEST_DELETE_APP:
					if (_rmsg.request == REQUEST_CREATE_APP)
					{
						BOOST_LOG_TRIVIAL(info) << "REQUEST_CREATE_APP " << _rmsg.params << _rmsg.deviceId;
						_pDeviceMgr->createApp(_rmsg.deviceId,
							json::parse(_rmsg.params));
					}
					else
					{
						BOOST_LOG_TRIVIAL(info) << "REQUEST_DELETE_APP " << _rmsg.params << _rmsg.deviceId;
						_pDeviceMgr->deleteApp(_rmsg.deviceId,
							json::parse(_rmsg.params));
					}

				case REQUEST_GET_APPS:
					jresp = _pDeviceMgr->getApps(_rmsg.deviceId);
					jstr = jresp.dump();
					//jstr.append("\n");
					_smsg.response = jstr;
					msg_send(MSG_SEND_TIMEOUT);
					BOOST_LOG_TRIVIAL(info) << "Apps: " << jstr;
					break;
				default:
					//_smsg.button;
					_smsg.request = REQUEST_UNKNOWN;
					msg_send(MSG_SEND_TIMEOUT);
					BOOST_LOG_TRIVIAL(info) << "request " << "REQUEST_UNKNOWN" << " received";
					break;
				}

			}
			catch (boost::interprocess::interprocess_exception& ex)
			{
				BOOST_LOG_TRIVIAL(info) << "CommandMsger::running() IPC err: " << ex.what();
				//_smsg.response = DEFAULT_RESPONSE;
				//msg_send(MSG_SEND_TIMEOUT);
			}
			catch (std::exception ex)
			{
				BOOST_LOG_TRIVIAL(info) << "CommandMsger::running() err: " << ex.what();
				_smsg.response = DEFAULT_RESPONSE;
				msg_send(MSG_SEND_TIMEOUT);
			}


		} while (true);

	Exit:
		_binservice = false;
		return;

	}

	std::set<std::string> getExistApps(uint16_t deviceid)
	{
		// add existing apps to exclusion list
		std::set<std::string> exclusionList;
		try
		{
			auto existingApps = _pDeviceMgr->getApps(deviceid)["apps"];
			for (auto& app : existingApps.items())
			{
				nlohmann::json& jApp = app.value();
				for (auto& el : jApp.items()) {
#ifdef _WINDOWS
					std::string key = tolower(el.key());
#else
					std::string key = el.key();
#endif
					if (key != "*") {
						exclusionList.insert(key);
					}
				}
			}
		}
		catch (std::exception& e)
		{
			std::cout << "getExistApps err : " << e.what() << std::endl;
		}

		return exclusionList;
	}

#else

	bool CommandMsgque_reset()
	{
		std::lock_guard<std::mutex> lock(_reset_mutex);
		//BOOST_LOG_TRIVIAL(info) << _tid << " > CommandMsgque_reset()";
		bool boldstste = _bhelper_running;
		if (is_helper_running())
		{
			if (!boldstste)
			{
				if (tbwMshm::readMshmElement(MSHMEM_FPID, &_mshm_pid))
				{
					if (CommandMsgque_setup(_mshm_pid))
					{
						BOOST_LOG_TRIVIAL(info) << _tid << " > CommandMsgque_reset OK!";
						return true;
					}
				}
				else
				{
					CommandMsger_init(_testMode);
				}
			}
			else
				return true;
			
		}
		else
		{
			#ifdef _WINDOWS
			//boost::filesystem::path fname("C:\\Program Files(x86)\\Kensington\\KensingtonWorks2\\tbwhelper.exe");
			//	boost::filesystem::current_path().append("/tbwhelper.exe"));
			//char fname[MAX_PATH] = "\"C:\\Program Files(x86)\\Kensington\\KensingtonWorks2\\tbwhelper.exe\"";
			//GetCurrentDirectory(MAX_PATH, fname);
			//strcat(fname, "\\tbwhelper.exe");
			//BOOST_LOG_TRIVIAL(info) << fname;
			//ShellExecute(NULL, "open", fname, NULL, NULL, SW_SHOWDEFAULT);
			//boost::process::exe(fname.c_str());
			//system(fname);
			#endif
		}

		return false;
	}

	void running()
	{
		uint16_t cnt = 0;
		std::thread::id tid = std::this_thread::get_id();
		do
		{
			try
			{
				//if(cnt++ % 20 == 0)
				//	BOOST_LOG_TRIVIAL(info) << tid << " > monitoring...(" << _bhelper_running << ")";
				if (!_bhelper_running)
				{
					// let GUI cmd msg queue start after helper runnung
					CommandMsgque_reset();
				}

				//bipc::scoped_lock<bipc::named_mutex> lock(_monitor_mutex);
				//_cond_msgque_check.wait(lock);

				std::this_thread::sleep_for(std::chrono::seconds(2));

			}
			catch (std::exception& ex)
			{
				BOOST_LOG_TRIVIAL(info) << "monitor error : " << ex.what();

			}
		
		} while (1);


	}
#endif


	std::string sendRequest(uint16_t request, std::string app, uint16_t devid, uint32_t button, std::string cmd, std::string params)
	{
		std::lock_guard<std::mutex> lock(_send_mutex);
		#if(!IS_USER_HELPER)
		if (!_msgque_ready)	// && (request != REQUEST_WHO))
		{
			BOOST_LOG_TRIVIAL(info) << " _msgque_ready == false";
			goto failExit;
		}
		if (request == REQUEST_GET_DEVICES)
		{
			//GUI will periodically update device info.
			if (!_bhelper_running)
			{
				CommandMsgque_reset();
			}
			return tbwMshm::getMshmDevices();
		}
		#endif

		try
		{
			_smsg.request = request;
			_smsg.app = app;
			_smsg.deviceId = devid;
			_smsg.button = button;
			_smsg.cmd = cmd;
			_smsg.params = params;
			_smsg.cid++;

			//BOOST_LOG_TRIVIAL(info) << "sendRequest sending..";
			BOOST_LOG_TRIVIAL(info) << _tid << " >  #" << _smsg.cid << " msg_send - " << request << std::endl;

			if (msg_send(MSG_SEND_TIMEOUT) == no_error)
			{
				uint16_t rectout = MSG_RECEIVE_TIMEOUT;
				switch (request)
				{
					case REQUEST_GET_DEVICES:
					case REQUEST_GET_CONFIGS:
					case REQUEST_SET_CONFIGS:
					case REQUEST_GET_SNIPPETS:
					case REQUEST_GET_FAVORITE_APPS:
						rectout = MSG_RECEIVE_LONG_TIMEOUT;
						break;
				}
				if (msg_receive(rectout) != no_error)
				{
					std::cout << request << " sendRequest MSG_RECEIVE_TIMEOUT \r\n";
					goto failExit;
				}
				BOOST_LOG_TRIVIAL(info) << "msg #" << _rmsg.cid << " - " << _smsg.cid << " msg_receive - cmd " << _smsg.cmd << " \r\n";
				if (_rmsg.cid == _smsg.cid)
				{
					_smsg.cmd = _rmsg.cmd;

					//BOOST_LOG_TRIVIAL(info) << "response : " << _rmsg.response;
					return _rmsg.response;
				}

			}
			else
			{
				BOOST_LOG_TRIVIAL(info) << request << " sendRequest MSG_SEND_TIMEOUT \r\n";
				goto failExit;

			}
		}
		catch (boost::interprocess::interprocess_exception& ex)
		{
			ex.get_error_code();
			BOOST_LOG_TRIVIAL(info) << "error : " << ex.what();

		}

	failExit:
		#if(!IS_USER_HELPER)
		//BOOST_LOG_TRIVIAL(info) << "_cond_msgque_check.notify_all()";
		//_cond_msgque_check.notify_all();
		_bhelper_running = false;
		CommandMsgque_reset();
		#endif
		return std::string("FALSE");
	}

	void clearQueue()
	{
		for (int i = 0; i < 20; i++)
		{
			if (msg_receive(100) != no_error)
			{
				return;
			}
		}
	}

/*
	bool getMsgQueue(bool bcreate)
	{
		if (_pid <= 0)
		{
			return false;
		}
		if (setupQueue(_mshm_pid))
		{
			//_msgque_ready = true;
			//_msgque_ready = checkQueues(3);
			return true;
		}

		return false;
				
	}
*/
	bool setupQueue(uint32_t seed)
	{
		std::cout << "setupQueue <seed: " << seed << "> \r\n";

		bool ret = false;
		int r1, r2, r3;
		srand(seed);
		for (int i = 0; i <= rand() % 10; i++)
			r1 = rand();
		for (int i = 0; i <= rand() % 10; i++)
			r2 = rand();
		for (int i = 0; i <= rand() % 10; i++)
			r3 = rand();

		std::string name1, name2;

		_Qname1 = std::string(MSGQUEUE_PREFIX) + std::string("g2h") + std::to_string(r1 / 2 + r2 / 2);
		_Qname2 = std::string(MSGQUEUE_PREFIX) + std::string("h2g") + std::to_string(r2 / 2 + r3 / 2);

		try
		{
#if(IS_USER_HELPER)
			{
				// don't use create_only
				_msg_rQueue = new message_queue(open_or_create, _Qname1.data(), MAX_QUEUE_NUMBER, MAX_QUEUE_SIZE);// sizeof(cmdMsg));
				_msg_sQueue = new message_queue(open_or_create, _Qname2.data(), MAX_QUEUE_NUMBER, MAX_QUEUE_SIZE);// sizeof(cmdMsg));
				//BOOST_LOG_TRIVIAL(info) << "msg queue (" << MAX_QUEUE_NUMBER << ", " << MAX_QUEUE_SIZE << " < " << _Qname1 << " > created";
				ret = true;
			}
#else
			{
				_msg_rQueue = new message_queue(open_only, _Qname2.data());
				_msg_sQueue = new message_queue(open_only, _Qname1.data());
				//BOOST_LOG_TRIVIAL(info) << "msg queue < " << _Qname2 << " > opened";
				std::cout << "msg queue < " << _Qname2 << " > opened.\r\n";
				ret = true;
			}
#endif
		}
		catch (boost::interprocess::interprocess_exception& ex)
		{
			BOOST_LOG_TRIVIAL(info) << "new message_queue err : " << ex.what();
			std::cout << "new message_queue err : " << ex.what();
			ret = false;
		}

		return ret;
	}


	uint32_t msg_receive(uint32_t tmout)
	{
		uint32_t retcode;

		if (tmout <= 0)
		{
			receive(tmout, &retcode);
			return retcode;
		}

		std::thread			msg_thread;
		msg_thread = thread(&receive, tmout, &retcode);
		if (_msg_mutex.try_lock_for(std::chrono::milliseconds(tmout + 20)))
		{
			_msg_mutex.unlock();
			//return retcode;
		}
		else
		{

#ifdef _WINDOWS
			TerminateThread(msg_thread.native_handle(), 0);
#else
			pthread_cancel(msg_thread.native_handle());
			//pthread_kill(msg_thread.native_handle());
#endif
			_msg_mutex.unlock();
			retcode = timeout_when_locking_error;
		}
		msg_thread.join();
		return retcode;
	}

	uint32_t msg_send(uint32_t tmout)
	{
		uint32_t retcode = no_error;

		if (tmout <= 0)
		{
			send(tmout, &retcode);
			return retcode;
		}

		std::thread			msg_thread;

		msg_thread = thread(&send, tmout, &retcode);
		if (_msg_mutex.try_lock_for(std::chrono::milliseconds(tmout + 20)))
		{
			_msg_mutex.unlock();
			//return retcode;
		}
		else
		{
#ifdef _WINDOWS
			TerminateThread(msg_thread.native_handle(), 0);
#else
			pthread_cancel(msg_thread.native_handle());
			//pthread_kill(msg_thread.native_handle());
#endif
			_msg_mutex.unlock();
			retcode = timeout_when_locking_error;
		}

		msg_thread.join();
		return retcode;
	}


	void receive(uint32_t tmout, uint32_t *retcode)
	{
		//_msg_mutex.lock();
		std::lock_guard<std::timed_mutex> lock(_msg_mutex);

		if (_msg_rQueue == nullptr)
		{
			*retcode = not_found_error;
			goto exit;
		}


		try
		{
			message_queue::size_type recvd_size;
			unsigned int priority;
			std::stringstream iss;
			std::string serialized_string;
			serialized_string.resize(MAX_QUEUE_SIZE);
			if (tmout <= 0)
			{
				_msg_rQueue->receive(&serialized_string[0], MAX_QUEUE_SIZE, recvd_size, priority);
				*retcode = no_error;
			}
			else
			{
				if (_msg_rQueue->timed_receive(&serialized_string[0], MAX_QUEUE_SIZE, recvd_size, priority,
					ptime(boost::posix_time::microsec_clock::universal_time()) + milliseconds(tmout)))
					//ptime(boost::posix_time::microsec_clock::local_time()) + milliseconds(tmout)))
				{
					*retcode = no_error;
				}
				else
				{
					*retcode = io_error;
				}

			}

			if (*retcode == no_error)
			{
				iss << serialized_string;

				boost::archive::text_iarchive ia(iss, ARCH_FLAGS);
				ia >> _rmsg;

				BOOST_LOG_TRIVIAL(info) << _tid << " > msg queue " << recvd_size << " bytes received ";
				//std::cout << "msg queue " << recvd_size << " bytes received \r\n";
			}


		}
		catch (boost::interprocess::interprocess_exception& ex)
		{
			*retcode = ex.get_error_code();
			goto exit;
		}
	exit:
		_msg_mutex.unlock();
		return;
	}


	void send(uint32_t tmout, uint32_t *retcode)
	{
		//_msg_mutex.lock();
		std::lock_guard<std::timed_mutex> lock(_msg_mutex);

		if (_msg_rQueue == nullptr)
		{
			*retcode = not_found_error;
			goto exit;
		}


		try
		{
			std::stringstream oss;
			boost::archive::text_oarchive oa(oss, ARCH_FLAGS);
			oa << _smsg;

			std::string serialized_string(oss.str());

			if (tmout <= 0)
			{
				_msg_sQueue->send(serialized_string.data(), serialized_string.size(), 0);
				*retcode = no_error;
			}
			else
			{
				if (_msg_sQueue->timed_send(serialized_string.data(), serialized_string.size(), 0,
					ptime(boost::posix_time::microsec_clock::universal_time()) + milliseconds(tmout)))
					//ptime(boost::posix_time::microsec_clock::local_time()) + milliseconds(tmout)))
				{
					*retcode = no_error;
				}

			}

		}
		catch (boost::interprocess::interprocess_exception& ex)
		{
			*retcode = ex.get_error_code();
			goto exit;

		}
	exit:

		//_msg_mutex.unlock();
		return;
	}

	bool IsRunning() 
	{ 
		return _binservice; 
	}

} // namespace CommandMsger
//-----------------------------------End of porting----------------------------


#if(IS_JAVASCRIPT_ON)

static std::mutex _addon_mutex;

void startService(const Nan::FunctionCallbackInfo<v8::Value>& info)
{
	if (!CommandMsger::_binservice)
	{
		#ifdef __APPLE__
		CommandMsger::CommandMsger_init(getpid());
		#else
		CommandMsger::CommandMsger_init(boost::detail::winapi::GetCurrentProcessId());
		#endif
		//CommandMsger::CommandMsger_Start();
	}

    
    double arg0 = info[0]->NumberValue(Nan::GetCurrentContext()).FromJust();
    v8::Local<v8::Number> num = Nan::New(arg0 + 1);
    info.GetReturnValue().Set(num);
}

void sendRequest(const Nan::FunctionCallbackInfo<v8::Value>& info)
{	
	_addon_mutex.lock();

	uint16_t	request, deviceid; 
	uint32_t	button;
	std::string		app(""), cmd(""), params(""), response("");

	//std::cout << "commandService: ";
	//std::lock_guard<std::mutex> lock(_mutex);
	v8::Isolate* isolate = info.GetIsolate();

	if (info[0]->IsNumber())
	{
		request = info[0]->NumberValue(Nan::GetCurrentContext()).FromJust();
	}
	if (info[1]->IsString())
	{
		// from v8 to cpp			
		v8::String::Utf8Value str(isolate, info[1]);
		app = *str;
	}
	if (info[2]->IsNumber())
	{
		deviceid = info[2]->NumberValue(Nan::GetCurrentContext()).FromJust();
	}
	if (info[3]->IsNumber())
	{
		button = info[3]->NumberValue(Nan::GetCurrentContext()).FromJust();
	}
	if (info[4]->IsString())
	{
		// from v8 to cpp			
		v8::String::Utf8Value str(isolate, info[4]);
		cmd = *str;
	}
	if (info[5]->IsString())
	{
		// from v8 to cpp			
		v8::String::Utf8Value str(isolate, info[5]);
		params = *str;
	}
	//std::cout << "send request " << request; //<< ", " << app << ", " 
	//				<< deviceid << ", " << button << ", "
	//				<< cmd << ", " << params <<  " >\r\n";
	response = CommandMsger::sendRequest(request, app, deviceid, button, cmd, params);
	
Exit:
	//std::cout << " => response " << request << " \r\n";
	// cpp to v8
	v8::Local<v8::String> v8String = v8::String::NewFromUtf8(isolate, response.c_str(), v8::NewStringType::kNormal).ToLocalChecked();
	info.GetReturnValue().Set(v8String);
	// or
	//v8::Local<v8::String> res = Nan::New("This is a thing.").ToLocalChecked();
	//info.GetReturnValue().Set(res);
	_addon_mutex.unlock();
}

void Init(v8::Local<v8::Object> exports) {

	std::cout << " addon Init(). \r\n";
    exports->Set(Nan::GetCurrentContext(),
		         Nan::New("StartService").ToLocalChecked(), // the exported name
                 Nan::New<v8::FunctionTemplate>(startService)->GetFunction(Nan::GetCurrentContext()).ToLocalChecked());
	exports->Set(Nan::GetCurrentContext(),
		         Nan::New("SendRequest").ToLocalChecked(), // the exported name
		         Nan::New<v8::FunctionTemplate>(sendRequest)->GetFunction(Nan::GetCurrentContext()).ToLocalChecked());
#ifdef __APPLE__
	CommandMsger::CommandMsger_init(getpid());
#else
	CommandMsger::CommandMsger_init(boost::detail::winapi::GetCurrentProcessId());
#endif
	//CommandMsger::CommandMsger_Start();
	
}

NODE_MODULE(addon, Init)

#endif