import * as $ from 'jquery';
import { MenuItemHandler } from "./menu-item-handler";
import { getCommandLabel } from "./helper";

class GenericActionHandlers implements MenuItemHandler {
    loadFn(button: number, command: string, params: any, selectedRadio: any) {

    };
    validatorFn(command: string) {
        return true;
    };
    storeFn(app:string, button: number, command: string, element: any) {
        return {};
    };
    labelFn(command: string, params: any, t: (val:string) => string) {
        if (params
            && params.hasOwnProperty('label')
            && params.label) {
            return params.label;
        }
        return getCommandLabel(command);
    };
}

export function bindGenericActionHandlers(t: any, actionHandlers: Map<RegExp, MenuItemHandler>) {
    actionHandlers.set(new RegExp('.*'), new GenericActionHandlers());
}
