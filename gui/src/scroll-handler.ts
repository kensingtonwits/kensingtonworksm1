import * as $ from 'jquery';
import { MenuItemHandler } from "./menu-item-handler";
import { getCommandLabel } from "./helper";

class ScrollActionHandler implements MenuItemHandler {
    loadFn(button: number, command: string, params: any, selectedRadio: any) {
        if ($(selectedRadio).find('input').length > 0
            && params
            && params.hasOwnProperty('lines')
            && params.lines) {
            $(selectedRadio).find('input').val(params.lines);
        }
    };
    validatorFn(command: string) {
        return true;
    };
    storeFn(app:string, button: number, command: string, selectedRadio: any) {
        if ($(selectedRadio).find('input').length > 0) {
            return {
                lines: $(selectedRadio).find('input').val()
            }
        }
        return {};
    };
    labelFn(command: string, params: any, t: (val:string) => string) {
        return getCommandLabel(command);
    };
}

export function bindScrollActionHandler(t: any, actionHandlers: Map<RegExp, MenuItemHandler>) {
    actionHandlers.set(new RegExp('navigation_scroll'), new ScrollActionHandler());
}
