/**
 * Cloud backup/restore functions
 */
import * as settings from "electron-settings";
import { saveSettingsToGoogle, restoreSettingsFromGoogle, getGoogleUserInfo, removeGoogleUserInfo } from "./gdrive";
import { saveSettingsToOneDrive, restoreSettingsFromOneDrive, getMicrosoftUserInfo, removeMicrosoftUserInfo } from "./onedrive";
import { ipcRenderer } from "electron";
import { loadString } from "./helper";

let _autoSaveTimerId: any = null;

// A basic mechanism to auto-save settings changes to cloud storage.
ipcRenderer.on('device-settings-update', () => {
    let provider = getCloudBackupProvider();
    let cloudUserInfo = getCloudUserInfo();
    if (getAutoSaveChangesToCloud() && provider && cloudUserInfo) {
        //console.log('BackupRestore - device settings updated')
        if (!_autoSaveTimerId) {
            _autoSaveTimerId = setTimeout(() => {
                //console.log('BackupRestore - sync timeout expired')
                backupToCloud()
                .then(() => {
                    if (getNotifyAutoSaveChangesToCloud()) {
                        let notification = new Notification('KensingtonWorks', {
                            body: loadString('auto_save_settings_success_msg')
                        });
                    }
                }).catch(err => {
                    if (getNotifyAutoSaveChangesToCloud()) {
                        let notification = new Notification('KensingtonWorks', {
                            body: loadString('auto_save_settings_failure_msg')
                        });
                    }
                    console.log('Error auto backing up settings to cloud:', err);
                }).finally(() => {
                    clearTimeout(_autoSaveTimerId);
                    _autoSaveTimerId = null;
                });

            }, 5000);
        }
    }
});

 /**
  * Returns one of 'google' or 'onedrive' if user has selected
  * either as their online backup storage provider. Or null if
  * user has not selected any.
  */
 export function getCloudBackupProvider() : "google" | "microsoft" {
    let cloudBackupProvider = null;
    try {
        if (settings.has('cloudBackup')) {
            let prevBackup: any = settings.get('cloudBackup');
            if (prevBackup) {
                cloudBackupProvider = prevBackup.provider;
            }
        }
    } catch (error) {
    }
    return cloudBackupProvider;
}

/**
 * Sets the preferred cloud storage provider as either of
 * Google Drive or Microsoft OneDrive.
 *
 * @param provider string {'google'|'onedrive'}
 */
export function setCloudBackupProvider(provider: "google" | "microsoft") {
    settings.set('cloudBackup', {
        provider: provider
    });
}

/**
 * Removes current cloud backup provider.
 */
export async function eraseCloudBackupProvider() {
    try {
        let provider = getCloudBackupProvider();
        if (settings.has('cloudBackup')) {
            settings.delete('cloudBackup');
        }
        if (provider == 'google') {
            return await removeGoogleUserInfo();
        } else if (provider == 'microsoft') {
            return await removeMicrosoftUserInfo();
        }
    } catch (error) {
    }
}

/**
 * Returns cloud backup provider specific user info.
 */
export function getCloudUserInfo() {
    let provider = getCloudBackupProvider();
    if (provider == 'google') {
        return getGoogleUserInfo();
    } else if (provider == 'microsoft') {
        return getMicrosoftUserInfo();
    }
    return null;
}

/**
 * Backup current settings to preferred cloud storage service.
 */
export async function backupToCloud() {
    let provider = getCloudBackupProvider();
    if (provider == 'google') {
        return await saveSettingsToGoogle();
    } else if (provider == 'microsoft') {
        return await saveSettingsToOneDrive();
    }
    throw Error('Preferred cloud storage service not set')
}

/**
 * Restore settings from preferred cloud storage service.
 */
export async function restoreFromCloud() {
    let provider = getCloudBackupProvider();
    if (provider == 'google') {
        return await restoreSettingsFromGoogle();
    } else if (provider == 'microsoft') {
        return await restoreSettingsFromOneDrive();
    }
    throw Error('Preferred cloud storage service not set')
}

export function getAutoSaveChangesToCloud(): boolean {
    if (settings.has('autoSaveChangesToCloud')) {
        return <boolean>settings.get("autoSaveChangesToCloud");
    }
    return false;
}

export function setAutoSaveChangesToCloud(enable: boolean) {
    settings.set("autoSaveChangesToCloud", enable);
}

export function getNotifyAutoSaveChangesToCloud(): boolean {
    if (settings.has('notifyAutoSaveChangesToCloud')) {
        return <boolean>settings.get("notifyAutoSaveChangesToCloud");
    }
    return false;
}

export function setNotifyAutoSaveChangesToCloud(enable: boolean) {
    settings.set("notifyAutoSaveChangesToCloud", enable);
}
