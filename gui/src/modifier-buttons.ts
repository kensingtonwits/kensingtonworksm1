import * as $ from 'jquery';

/**
 * Handler for Modifier button component, which presents user with
 * a set of toggle buttons, each for one modifier button specific for the
 * platform. The two functions allow these buttons state to be converted
 * into their string form representation that can be consumed by TbwSettings
 * and vice versa. That is converting the string form modifier button
 * combination string into button toggle states.
 */

/**
 * Converts modifier button selected state into modifier key combination
 * string of the form:
 *    "alt,ctrl,shift,win,cmd"
 * @param element any parent html element containing the modifier keys
 */
export function modifierButtonsToStr(element: any): string {
    let modifiers = '';
    let jElem = $(element);
    jElem.find('div.keysbox.click').each((index, keysbox) => {
        modifiers += modifiers.length > 0 ? ',' : '';
        let styles = $(keysbox).attr('class');
        styles = styles.replace('keysbox', '').replace('click', '');
        modifiers += styles.trim();
    });
    return modifiers;
}

/**
 * Sets the state of the buttons representing the modifier keys to match
 * the modifier
 * @param modifiers string Modifier key combination string of the form:
 *    "alt,ctrl,shift,win,cmd" depending on the keys selected.
 * @param element any the parent html element under which the modifer key
 *    buttons whose state is to be set is located.
 */
export function modifierButtonsFromStr(modifiers: string, element: any): void {
    let jElem = $(element);
    // first clear the selected state of all key buttons
    jElem.find('div.keysbox').removeClass('click');
    jElem.find(`div.keysbox input`).prop('checked', false);
    modifiers.split(',').forEach((val, index, ar) => {
      if (val.length) {
        jElem.find(`div.keysbox.${val}`).addClass('click');
        jElem.find(`div.keysbox.${val} input`).prop('checked', true);
      }
    });
}