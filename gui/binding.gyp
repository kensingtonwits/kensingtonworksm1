{
  "targets": [
    {
      "target_name": "commandServer",
      "sources": [ "src/addons/CmdMsger.cpp", "src/addons/tbwMshm.cpp" ],
      "conditions": [
        ["OS==\"win\"", {
            'configurations': {
                'Debug': {
                    "include_dirs": [
                        'C:/boost_1_67_0',
                        "<!(node -e \"require('nan')\")"
                    ],
                    'defines': [
                        '_WIN32_WINNT=0x0601'
                    ],
                    'msvs_disabled_warnings': [ 4244,4005,4506,4345,4804,4805 ],
                    'msvs_settings': {
                        'VCLinkerTool': {
                            'AdditionalLibraryDirectories': ['C:/boost_1_67_0/stage/lib'],
                        },
                        "VCCLCompilerTool": {
                            "ExceptionHandling": 1,
                            'RuntimeTypeInfo': 'true'
                        }
                    }
                },
                'Release': {
                    "include_dirs": [
                        'C:/boost_1_67_0',
                        "<!(node -e \"require('nan')\")"
                    ],
                    'defines': [
                        '_WIN32_WINNT=0x0601'
                    ],
                    'msvs_disabled_warnings': [ 4244,4005,4506,4345,4804,4805 ],
                    'msvs_settings': {
                        'VCLinkerTool': {
                            'AdditionalLibraryDirectories': ['C:/boost_1_67_0/stage/lib'],
                        },
                        "VCCLCompilerTool": {
                            "ExceptionHandling": 1,
                            'RuntimeTypeInfo': 'true'
                        }
                    }
                }
            }
        },
        ],
        ["OS==\"mac\"", {
            "include_dirs": [
                '/usr/local/include',
                "<!(node -e \"require('nan')\")"
            ],
            "libraries": [
                "/usr/local/lib/libboost_system.a",
                "/usr/local/lib/libboost_filesystem.a",
                "/usr/local/lib/libboost_log_setup.a",
                "/usr/local/lib/libboost_log.a",
                "/usr/local/lib/libboost_program_options.a",
                "/usr/local/lib/libboost_thread.a",
                "/usr/local/lib/libboost_serialization.a",
            ],
            "cflags_cc!": [ "-fno-rtti", "-fno-exceptions" ],
            "cflags!": [ "-fno-exceptions" ],
            "xcode_settings": {
                        'OTHER_CPLUSPLUSFLAGS' : ['-std=c++14','-stdlib=libc++', '-v'],
                        'OTHER_LDFLAGS': ['-stdlib=libc++'],
                        'MACOSX_DEPLOYMENT_TARGET': '10.11',
                        'GCC_ENABLE_CPP_EXCEPTIONS': 'YES',
                        "GCC_ENABLE_CPP_RTTI": "YES"
            }
        },
        ],
      ],
    }
  ]
}