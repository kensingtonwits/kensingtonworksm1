# Download necessary npm nodules
echo "Now download necessary modules..."
npm i
# Rebuild for node-gyp addon
echo ""
echo "Download finished. Rebuild electron component for node-gyp..."
./node_modules/.bin/electron-rebuild

# Change Visual Studio settings
echo ""
echo "Electron-rebuild finished."
echo "Don't worry it fails, We'll change VS2017 settings and rebuild commandServer.node again right now."
$currentPath = pwd
# Change to work directory
chdir build
# Get xml content
$file=gi .\commandServer.vcxproj
[Xml]$XmlContent = Get-Content .\commandServer.vcxproj
$xmlContent.Project.ItemDefinitionGroup.ClCompile[0].RuntimeTypeInfo = "true"
$xmlContent.Project.ItemDefinitionGroup.ClCompile[1].RuntimeTypeInfo = "true"
$XmlContent.save($file.FullName)
# Change back to upper directory
chdir $currentPath

# Addon rebuild
node-gyp build

# Copy binary to Release folders
echo ""
echo "Rebuild commandServer.node finished. Copying binary to Release folder..."
if (-not (Test-Path './Release/')){
    mkdir Release
}
cp ./build/Release/commandServer.node ./Release/
#cp ./icon/TrackballWorks.ico ./build/
echo ""
echo "npm modules & commandServer.node is ready for ya!"