#!/bin/bash

# Download necessary npm nodules
echo "Now download necessary modules..."
npm i
# Rebuild for node-gyp addon
echo ""
echo "Download finished. Rebuild electron component for node-gyp..."
./node_modules/.bin/electron-rebuild

echo ""
echo "Rebuild commandServer.node finished. Copying binary to Release folder..."
if [ ! -d "Release" ]; then
    mkdir "Release"
fi
cp ./build/Release/commandServer.node ./Release/

echo ""
echo "npm modules & commandServer.node is ready for ya!"